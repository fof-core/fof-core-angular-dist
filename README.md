# Core

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.2.0.

## New project 

````
$ ng generate application fof-angular-users
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use?
  CSS
> SCSS   [ https://sass-lang.com/documentation/syntax#scss                ] 
````

```bash
# install the main lib
npm install git+ssh://git@bitbucket.org/fof-core/fof-core-angular-dist.git
or npm install git+https://bitbucket.org/fof-core/fof-core-angular-dist.git
# run the following script from the root directory
.\node_modules\@fof-angular\core\ressources\postinstall.sh

# the script will automatize the following 

ng add @angular/material 
# ? Choose a prebuilt theme name, or "custom" for a custom theme: Custom
# ? Set up global Angular Material typography styles? Yes 
# ? Set up browser animations for Angular Material? Yes
npm install @ngx-translate/core --save
npm install @ngx-translate/http-loader --save
# - replace content of browserslist
# - add the proxy-conf.json file at the root directory
```

1. change the app.component.html content for the following one

```html
<company-master-page></company-master-page>
```

2. replace the start script in package.json by 
`"start": "ng serve --host 0.0.0.0 --proxy-config proxy-conf.json"`

3. update environment 
````typescript
export const environment = {
  production: false,
  //add the following line
  apiPath: 'api'
}
````

3. add the 404 route in the main app-routing.module.ts file
````typescript
//...
import { FofAuthGuard } from '@fof-angular/core'
import { NotFoundComponent } from '@your-company-angular/template'

const routes: Routes = [  
  { path: '**', component: NotFoundComponent, data: { title: 'Vous êtes perdu' }, canActivate: [FofAuthGuard]}
]
//...
````

4. import the fof-core module into the main module (AppModule) and ensure AppRoutingModule is in the last position
````typescript
// ...
import { IfofConfig } from '@fof-angular/core'
import { TemplateModule } from '@your-company-angular/template'
import { environment } from '../environments/environment'

// your custom actions for access management
export enum eP {  
  invoiceCreate = 'invoiceCreate',
  invoiceUpdate = 'invoiceUpdate', 
  invoiceRead = 'invoiceRead',
  invoiceDelete = 'invoiceDelete'   
}

// see comments in the source code for detail
const appConfig: IfofConfig = {
  appName: {
    long: 'My app',
    technical: 'my-app'
  },
  mainTitle: {
    long: 'Invoice application',
    short: 'Invoice App'
  },
  environment: environment,
  permissions: eP,
  // your custom main navigation 
  navigationMain: [
    { link: 'settings', label: 'A propos' },
    { link: 'feature-list', label: 'Fonctionnalités' },
    { link: 'examples', label: 'Examples' }    
  ]
}

@NgModule({  
  imports: [    
    TemplateModule.forRoot(appConfig), // <-- add that one
    AppRoutingModule // <-- must be the last one in the list (for catching ** routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

````

5. Change /index.html 

````html
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>FofAngularAppTest</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="manifest" href="manifest.webmanifest">
  <meta name="theme-color" content="#1976d2">
  <link rel="icon" type="image/png" href="./assets/logo-small.png"> -->
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/manifest/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/manifest/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/manifest/favicon-16x16.png">
  <link rel="manifest" href="/assets/manifest/site.webmanifest">
  <link rel="mask-icon" href="/assets/manifest/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="/assets/manifest/favicon.ico">
  <meta name="apple-mobile-web-app-title" content="fof angular app test">
  <meta name="application-name" content="fof angular app test">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="/assets/manifest/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <style type="text/css">
    body, html {
      height: 100%;
    }
    .app-loading {
      position: relative;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      height: 100%;
      background-color: #025AA8;
    }

    .logo {
      /* margin-top: 10px;      */
      /* width: 100px;
      height: 100px; */
      width: 115px;
      height: 70px;
      background: url(./assets/logo-small.png) center center no-repeat;
      background-size: cover;
    }

    .app-loading .spinner {
      height: 200px;
      width: 200px;
      animation: rotate 2s linear infinite;
      transform-origin: center center;
      position: absolute;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      margin: auto;
    }

    .app-loading .spinner .path {
      stroke-dasharray: 1, 200;
      stroke-dashoffset: 0;
      animation: dash 1.5s ease-in-out infinite;
      stroke-linecap: round;
      stroke: #ddd;
    }

    @keyframes rotate {
      100% {
        transform: rotate(360deg);
      }
    }

    @keyframes dash {
      0% {
        stroke-dasharray: 1, 200;
        stroke-dashoffset: 0;
      }
      50% {
        stroke-dasharray: 89, 200;
        stroke-dashoffset: -35px;
      }
      100% {
        stroke-dasharray: 89, 200;
        stroke-dashoffset: -124px;
      }
    }
  </style>
</head>
<body>  
  <app-root>
    <div class="app-loading">
      <div class="logo">
        <svg class="spinner" viewBox="25 25 50 50">
          <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
      </div>
    </div>
  </app-root>
  <noscript>Please enable JavaScript to continue using this application.</noscript>
</body>
</html>
````

6. Copy all stuff furnished by your company in assests directory
e.g. icon, manifest, logo etc...

7. import your company styles in 
````scss
@import '../../company-angular/template/src/theming';
@import '../../company-angular/template/src/sass/styles-variables.scss';
````

8. You can now start the app with `npm start`
nb. don't forget the run the backend. See @fof-core/nestjs

## Code scaffolding

Run `ng generate component component-name --project core` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project core`.
> Note: Don't forget to add `--project core` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build core` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build core`, go to the dist folder `cd dist/core` and run `npm publish`.

## Running unit tests

Run `ng test core` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@nestjsx/crud-request'), require('@angular/common/http'), require('rxjs'), require('@angular/router'), require('rxjs/operators'), require('@angular/common'), require('@angular/cdk/tree'), require('@angular/material/tree'), require('@angular/material/button'), require('@angular/material/checkbox'), require('@angular/material/icon'), require('@angular/material/card'), require('@angular/material/chips'), require('@angular/material/dialog'), require('@angular/material/divider'), require('@angular/material/expansion'), require('@angular/material/input'), require('@angular/material/list'), require('@angular/material/paginator'), require('@angular/material/progress-spinner'), require('@angular/material/sort'), require('@angular/material/table'), require('@angular/forms'), require('@angular/material/form-field'), require('@angular/cdk/layout'), require('@ngx-translate/core'), require('@angular/animations'), require('@ngx-translate/http-loader')) :
    typeof define === 'function' && define.amd ? define('@fof-angular/core', ['exports', '@angular/core', '@nestjsx/crud-request', '@angular/common/http', 'rxjs', '@angular/router', 'rxjs/operators', '@angular/common', '@angular/cdk/tree', '@angular/material/tree', '@angular/material/button', '@angular/material/checkbox', '@angular/material/icon', '@angular/material/card', '@angular/material/chips', '@angular/material/dialog', '@angular/material/divider', '@angular/material/expansion', '@angular/material/input', '@angular/material/list', '@angular/material/paginator', '@angular/material/progress-spinner', '@angular/material/sort', '@angular/material/table', '@angular/forms', '@angular/material/form-field', '@angular/cdk/layout', '@ngx-translate/core', '@angular/animations', '@ngx-translate/http-loader'], factory) :
    (global = global || self, factory((global['fof-angular'] = global['fof-angular'] || {}, global['fof-angular'].core = {}), global.ng.core, global['@nestjsx/crud-request'], global.ng.common.http, global.rxjs, global.ng.router, global.rxjs.operators, global.ng.common, global.ng.cdk.tree, global.ng.material.tree, global.ng.material.button, global.ng.material.checkbox, global.ng.material.icon, global.ng.material.card, global.ng.material.chips, global.ng.material.dialog, global.ng.material.divider, global.ng.material.expansion, global.ng.material.input, global.ng.material.list, global.ng.material.paginator, global.ng.material.progressSpinner, global.ng.material.sort, global.ng.material.table, global.ng.forms, global.ng.material.formField, global.ng.cdk.layout, global['@ngx-translate/core'], global.ng.animations, global['@ngx-translate/http-loader']));
}(this, (function (exports, core, crudRequest, http, rxjs, router, operators, common, tree, tree$1, button, checkbox, icon, card, chips, dialog, divider, expansion, input, list, paginator, progressSpinner, sort, table, forms, formField, layout, core$1, animations, httpLoader) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    (function (eAuth) {
        eAuth["basic"] = "basic";
        eAuth["windows"] = "windows";
    })(exports.eAuth || (exports.eAuth = {}));
    var CORE_CONFIG = new core.InjectionToken('CORE_CONFIG');

    var FoFAuthService = /** @class */ (function () {
        function FoFAuthService(fofConfig, httpClient, router) {
            this.fofConfig = fofConfig;
            this.httpClient = httpClient;
            this.router = router;
            this.environment = this.fofConfig.environment;
            this.currentUserSubject = new rxjs.BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
            this.currentUser$ = this.currentUserSubject.asObservable();
        }
        Object.defineProperty(FoFAuthService.prototype, "currentUser", {
            get: function () {
                if (this.currentUserSubject) {
                    return this.currentUserSubject.value;
                }
                return null;
            },
            enumerable: true,
            configurable: true
        });
        FoFAuthService.prototype.refreshByCookie = function () {
            var _this = this;
            return this.httpClient.get(this.environment.apiPath + "/auth/getUser")
                .pipe(operators.map(function (user) {
                if (user && user.accessToken) {
                    _this.currentUserSubject.next(user);
                    // this.currentUserSubject.complete()         
                }
                return user;
            }));
        };
        FoFAuthService.prototype.loginKerberos = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.httpClient.get(_this.environment.authPath + "/auth/winlogin", { withCredentials: true })
                    .toPromise()
                    .then(function (user) {
                    // give the temporary windows token to do the next get        
                    if (user && user.accessToken) {
                        _this.currentUserSubject.next(user);
                    }
                    _this.httpClient.get(_this.environment.apiPath + "/auth/winUserValidate")
                        .toPromise()
                        .then(function (user) {
                        // get the final user. 
                        // Can work only if the windows user are registered into the App.
                        // if not, it means the user is just authentified against MS AD
                        if (user && user.accessToken) {
                            _this.currentUserSubject.next(user);
                        }
                        _this.currentUserSubject.complete();
                        resolve(user);
                    })
                        .catch(function (error) {
                        reject(error);
                    });
                })
                    .catch(function (error) {
                    reject(error);
                });
            });
        };
        FoFAuthService.prototype.login = function (username, password) {
            var _this = this;
            var data = {
                username: username,
                password: password
            };
            return this.httpClient.post(this.environment.apiPath + "/auth/login", data)
                .pipe(operators.map(function (user) {
                // login successful if there's a jwt token in the response         
                if (user && user.accessToken) {
                    // There is a cookie with the jwt inside for managing web browser refresh            
                    _this.currentUserSubject.next(user);
                }
                return user;
            }));
        };
        FoFAuthService.prototype.logOut = function () {
            var _this = this;
            // remove cookie to log user out    
            return this.httpClient.get(this.environment.apiPath + "/auth/logout")
                .pipe(operators.map(function (cookieDeleted) {
                if (cookieDeleted) {
                    _this.currentUserSubject.next(null);
                    _this.router.navigate(['/login']);
                }
                return cookieDeleted;
            }));
            // .subscribe(
            //   cookieDeleted => {
            //     if (cookieDeleted) {
            //       this.currentUserSubject.next(null)    
            //       this.router.navigate(['/login'])        
            //     }
            //   },
            //   error => {
            //     throw error
            //   })    
        };
        FoFAuthService.ɵfac = function FoFAuthService_Factory(t) { return new (t || FoFAuthService)(core["ɵɵinject"](CORE_CONFIG), core["ɵɵinject"](http.HttpClient), core["ɵɵinject"](router.Router)); };
        FoFAuthService.ɵprov = core["ɵɵdefineInjectable"]({ token: FoFAuthService, factory: FoFAuthService.ɵfac, providedIn: 'root' });
        return FoFAuthService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FoFAuthService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }, { type: http.HttpClient }, { type: router.Router }]; }, null); })();

    var FoFCoreService = /** @class */ (function () {
        function FoFCoreService(fofConfig, httpClient, router, foFAuthService) {
            var _this = this;
            this.fofConfig = fofConfig;
            this.httpClient = httpClient;
            this.router = router;
            this.foFAuthService = foFAuthService;
            this._initializationReadySubject = new rxjs.BehaviorSubject(false);
            this.initializationReady$ = this._initializationReadySubject.asObservable();
            this._environment = this.fofConfig.environment;
            rxjs.forkJoin(this.foFAuthService.currentUser$).subscribe({
                // next: value => console.log('value', value),
                complete: function () {
                    _this._initializationReadySubject.next(true);
                }
            });
        }
        Object.defineProperty(FoFCoreService.prototype, "environment", {
            get: function () {
                return this._environment;
            },
            enumerable: true,
            configurable: true
        });
        FoFCoreService.ɵfac = function FoFCoreService_Factory(t) { return new (t || FoFCoreService)(core["ɵɵinject"](CORE_CONFIG), core["ɵɵinject"](http.HttpClient), core["ɵɵinject"](router.Router), core["ɵɵinject"](FoFAuthService)); };
        FoFCoreService.ɵprov = core["ɵɵdefineInjectable"]({ token: FoFCoreService, factory: FoFCoreService.ɵfac, providedIn: 'root' });
        return FoFCoreService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FoFCoreService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }, { type: http.HttpClient }, { type: router.Router }, { type: FoFAuthService }]; }, null); })();

    // toDO: change all relations! -> must NOT use an ID!
    var FofPermissionService = /** @class */ (function () {
        function FofPermissionService(fofConfig, httpClient, coreService) {
            var _this = this;
            this.fofConfig = fofConfig;
            this.httpClient = httpClient;
            this.coreService = coreService;
            this.user = {
                create: function (role) { return _this.httpClient.post(_this.environment.apiPath + "/users", role); },
                update: function (role) { return _this.httpClient.patch(_this.environment.apiPath + "/users/" + role.id, role); },
                delete: function (role) { return _this.httpClient.delete(_this.environment.apiPath + "/users/" + role.id); },
                getAll: function () { return _this.httpClient.get(_this.environment.apiPath + "/users"); },
                search: function (fullSearch, organizationsId, limit, page, sort, direction) {
                    if (direction === void 0) { direction = 'asc'; }
                    var query = "limit=" + limit + "&page=" + page + "&sort=" + sort + "&order=" + direction;
                    if (organizationsId) {
                        query += "&organizationsId=" + organizationsId;
                    }
                    if (fullSearch) {
                        query += "&fullSearch=" + fullSearch;
                    }
                    return _this.httpClient.get(_this.environment.apiPath + "/users?" + query);
                },
                replaceUserRoleOrganization: function (userRoleOrganizations, userId, organisationId) {
                    return _this.httpClient.put(_this.environment.apiPath + "/users/" + userId + "/organization/" + organisationId + "/roles", userRoleOrganizations);
                },
                deleteUserRoleOrganizations: function (userId, organisationsId) {
                    return _this.httpClient.delete(_this.environment.apiPath + "/users/" + userId + "/organizations?ids=" + JSON.stringify(organisationsId));
                },
                getWithRoleById: function (id) {
                    var qb = crudRequest.RequestQueryBuilder.create();
                    qb.setJoin({ field: 'userRoleOrganizations', select: ['roleId', 'organizationId'] })
                        .setJoin({ field: 'userRoleOrganizations.role', select: ['code', 'description'] })
                        .setJoin({ field: 'userRoleOrganizations.organization', select: ['name', 'code'] })
                        .sortBy({ field: 'organization.name', order: 'DESC' });
                    return _this.httpClient.get(_this.environment.apiPath + "/users/" + id + "?" + qb.query());
                },
                getWithUservicesById: function (id) {
                    var qb = crudRequest.RequestQueryBuilder.create();
                    qb.setJoin({ field: 'userUServices', select: ['uServiceId'] });
                    return _this.httpClient.get(_this.environment.apiPath + "/users/" + id + "?" + qb.query());
                }
            };
            this.role = {
                create: function (role) { return _this.httpClient.post(_this.environment.apiPath + "/roles", role); },
                update: function (role) { return _this.httpClient.patch(_this.environment.apiPath + "/roles/" + role.id, role); },
                delete: function (role) { return _this.httpClient.delete(_this.environment.apiPath + "/roles/" + role.id); },
                getAll: function () { return _this.httpClient.get(_this.environment.apiPath + "/roles"); },
                search: function (query, limit, page, sort, direction) {
                    if (direction === void 0) { direction = 'asc'; }
                    var qb = crudRequest.RequestQueryBuilder.create();
                    qb.setLimit(limit)
                        .setPage(page + 1)
                        .sortBy({ field: sort, order: direction.toUpperCase() });
                    return _this.httpClient.get(_this.environment.apiPath + "/roles?" + qb.query());
                },
                getWithPermissionByRoleCode: function (roleCode) {
                    // toDo: post a CR for edge browser?
                    // https://github.com/nestjsx/crud/wiki/Requests#frontend-usage
                    var qb = crudRequest.RequestQueryBuilder.create();
                    // qb.select(['code', 'description'])
                    qb.setJoin({ field: 'rolePermissions', select: ['permissionId'] })
                        .setJoin({ field: 'rolePermissions.permission', select: ['code'] })
                        // .setLimit(1)
                        // .setPage(1)
                        .sortBy({ field: 'permission.code', order: 'ASC' })
                        .search({ code: roleCode });
                    return _this.httpClient.get(_this.environment.apiPath + "/roles?" + qb.query());
                }
            };
            this.permission = {
                getAll: function () {
                    var qb = crudRequest.RequestQueryBuilder.create();
                    qb.sortBy({ field: 'permission.code', order: 'ASC' });
                    return _this.httpClient.get(_this.environment.apiPath + "/permissions?" + qb.query());
                }
            };
            this.organization = {
                create: function (organization) {
                    return _this.httpClient.post(_this.environment.apiPath + "/organizations", organization);
                },
                update: function (organization) {
                    return _this.httpClient.patch(_this.environment.apiPath + "/organizations/" + organization.id, organization);
                },
                delete: function (organization) {
                    return _this.httpClient.delete(_this.environment.apiPath + "/organizations/" + organization.id);
                },
                getTreeView: function () {
                    var qb = crudRequest.RequestQueryBuilder.create();
                    qb.select(['code', 'name']);
                    return _this.httpClient.get(_this.environment.apiPath + "/organizations/getTreeView?" + qb.query());
                }
            };
            this.rolePermission = {
                create: function (rolePermission) { return _this.httpClient.post(_this.environment.apiPath + "/rolePermissions", rolePermission); },
                delete: function (rolePermission) { return _this.httpClient.delete(_this.environment.apiPath + "/rolePermissions/" + rolePermission.id); }
            };
            this.userRoleOrganization = {
                create: function (userRoleOrganization) { return _this.httpClient.post(_this.environment.apiPath + "/userRoleOrganizations", userRoleOrganization); },
                delete: function (userRoleOrganization) { return _this.httpClient.delete(_this.environment.apiPath + "/userRoleOrganizations/" + userRoleOrganization.id); },
                bulkCreate: function (userRoleOrganizations) { return _this.httpClient.post(_this.environment.apiPath + "/userRoleOrganizations/bulk", { bulk: userRoleOrganizations }); }
            };
            this.userUservice = {
                create: function (userService) { return _this.httpClient.post(_this.environment.apiPath + "/userUservices", userService); },
                delete: function (id) { return _this.httpClient.delete(_this.environment.apiPath + "/userUservices/" + id); }
            };
            this.environment = this.fofConfig.environment;
        }
        FofPermissionService.ɵfac = function FofPermissionService_Factory(t) { return new (t || FofPermissionService)(core["ɵɵinject"](CORE_CONFIG), core["ɵɵinject"](http.HttpClient), core["ɵɵinject"](FoFCoreService)); };
        FofPermissionService.ɵprov = core["ɵɵdefineInjectable"]({ token: FofPermissionService, factory: FofPermissionService.ɵfac, providedIn: 'root' });
        return FofPermissionService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofPermissionService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }, { type: http.HttpClient }, { type: FoFCoreService }]; }, null); })();

    function FofOrganizationsTreeComponent_mat_tree_node_1_Template(rf, ctx) { if (rf & 1) {
        var _r185 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "mat-tree-node", 3);
        core["ɵɵelementStart"](1, "li", 4);
        core["ɵɵelement"](2, "button", 5);
        core["ɵɵelementStart"](3, "mat-checkbox", 6);
        core["ɵɵlistener"]("change", function FofOrganizationsTreeComponent_mat_tree_node_1_Template_mat_checkbox_change_3_listener($event) { core["ɵɵrestoreView"](_r185); var node_r183 = ctx.$implicit; var ctx_r184 = core["ɵɵnextContext"](); return ctx_r184.uiAction.treeViewNodeSelect(node_r183, $event); });
        core["ɵɵtext"](4);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var node_r183 = ctx.$implicit;
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("checked", node_r183.checked)("disabled", node_r183.notSelectable);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", node_r183.name, "");
    } }
    function FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template(rf, ctx) { if (rf & 1) {
        var _r188 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "mat-nested-tree-node");
        core["ɵɵelementStart"](1, "li");
        core["ɵɵelementStart"](2, "div", 4);
        core["ɵɵelementStart"](3, "button", 7);
        core["ɵɵelementStart"](4, "mat-icon", 8);
        core["ɵɵtext"](5);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](6, "mat-checkbox", 6);
        core["ɵɵlistener"]("change", function FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template_mat_checkbox_change_6_listener($event) { core["ɵɵrestoreView"](_r188); var node_r186 = ctx.$implicit; var ctx_r187 = core["ɵɵnextContext"](); return ctx_r187.uiAction.treeViewNodeSelect(node_r186, $event); });
        core["ɵɵtext"](7);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](8, "ul", 9);
        core["ɵɵelementContainer"](9, 10);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var node_r186 = ctx.$implicit;
        var ctx_r182 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](5);
        core["ɵɵtextInterpolate1"](" ", ctx_r182.uiVar.organizations.treeControl.isExpanded(node_r186) ? "expand_more" : "chevron_right", " ");
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("checked", node_r186.checked)("disabled", node_r186.notSelectable);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", node_r186.name, "");
        core["ɵɵadvance"](1);
        core["ɵɵclassProp"]("treeview-invisible", !ctx_r182.uiVar.organizations.treeControl.isExpanded(node_r186));
    } }
    var FofOrganizationsTreeComponent = /** @class */ (function () {
        function FofOrganizationsTreeComponent(fofPermissionService) {
            var _this = this;
            this.fofPermissionService = fofPermissionService;
            /** Can the user could select multiple organizations? */
            this.multiSelect = false;
            /** Submit all the organization selected everytime an orgnization is selected or unselected
             * AN objet or array, depending from the multiSelect param
            */
            this.selectedOrganizationsChange = new core.EventEmitter();
            // All private variables
            this.priVar = {
                currentNode: undefined,
                selectedNodesSynchronizeSave: undefined
            };
            // All private functions
            this.privFunc = {
                selectedNodeSynchronize: function () {
                    var tree = _this.uiVar.organizations.dataSource.data;
                    var selectedNodesSynchronize = _this.selectedNodesSynchronize;
                    var areSomeNodeNotSelectable = false;
                    if (_this.notSelectableOrganizations && _this.notSelectableOrganizations.length > 0) {
                        areSomeNodeNotSelectable = true;
                    }
                    var getSelectedNode = function (node) {
                        if (selectedNodesSynchronize) {
                            var foundedNode = selectedNodesSynchronize.filter(function (n) {
                                if (n.id === node.id) {
                                    // ugly fix: toDo: to review
                                    if (!n.name) {
                                        n.name = node.name;
                                    }
                                    return node;
                                }
                            });
                            if (foundedNode.length > 0) {
                                node.checked = true;
                            }
                            else {
                                node.checked = false;
                            }
                            // if (selectedNodesSynchronize && selectedNodesSynchronize.filter(n => n.id === node.id).length > 0) {
                            //   node.checked = true          
                            // } else {
                            //   node.checked = false
                            // }
                        }
                        if (areSomeNodeNotSelectable) {
                            if (_this.notSelectableOrganizations.filter(function (n) { return n.id === node.id; }).length > 0) {
                                // would prevent the user to check again the same organization 
                                if (_this.priVar.selectedNodesSynchronizeSave.filter(function (n) { return n.id === node.id; }).length === 0) {
                                    node.notSelectable = true;
                                }
                            }
                            else {
                                node.notSelectable = false;
                            }
                        }
                        node.children.forEach(function (child) { return getSelectedNode(child); });
                    };
                    if (tree && tree.length > 0) {
                        getSelectedNode(tree[0]);
                    }
                }
            };
            // All variables shared with UI 
            this.uiVar = {
                loading: true,
                organizations: {
                    treeControl: new tree.NestedTreeControl(function (node) { return node.children; }),
                    dataSource: new tree$1.MatTreeNestedDataSource()
                },
            };
            // All actions shared with UI 
            this.uiAction = {
                OrganizationNodeHasChild: function (_, node) { return !!node.children && node.children.length > 0; },
                organizationTreeLoad: function () {
                    _this.uiVar.loading = true;
                    _this.fofPermissionService.organization.getTreeView()
                        .toPromise()
                        .then(function (organizations) {
                        _this.uiVar.organizations.dataSource.data = organizations;
                        if (organizations.length > 0) {
                            _this.uiVar.organizations.treeControl.expand(organizations[0]);
                            _this.privFunc.selectedNodeSynchronize();
                        }
                    })
                        .finally(function () {
                        _this.uiVar.loading = false;
                    });
                },
                treeViewNodeSelect: function (node, $event) {
                    var tree = _this.uiVar.organizations.dataSource.data;
                    if (_this.multiSelect) {
                        var nodes_1 = [];
                        node.checked = $event.checked;
                        var getSelectedNode_1 = function (node) {
                            if (node.checked) {
                                var selectNode = __assign({}, node);
                                selectNode.children = null;
                                nodes_1.push(selectNode);
                            }
                            node.children.forEach(function (child) { return getSelectedNode_1(child); });
                        };
                        if (tree && tree.length > 0) {
                            getSelectedNode_1(tree[0]);
                        }
                        _this.selectedOrganizationsChange.emit(nodes_1);
                    }
                    else {
                        var nodeUnSelect_1 = function (node) {
                            node.indeterminate = false;
                            node.checked = false;
                            if (!node.children) {
                                node.children = [];
                            }
                            node.children.forEach(function (child) { return nodeUnSelect_1(child); });
                        };
                        if (tree && tree.length > 0) {
                            nodeUnSelect_1(tree[0]);
                        }
                        node.checked = $event.checked;
                        if (node.checked) {
                            _this.priVar.currentNode = node;
                        }
                        else {
                            _this.priVar.currentNode = null;
                        }
                        _this.selectedOrganizationsChange.emit(_this.priVar.currentNode);
                    }
                }
            };
            this.uiAction.organizationTreeLoad();
        }
        // Angular events
        FofOrganizationsTreeComponent.prototype.ngOnInit = function () {
        };
        FofOrganizationsTreeComponent.prototype.ngOnChanges = function () {
            var _this = this;
            if (this.nodeToDelete) {
                var spliceNode_1 = function (node) {
                    if (node.id === _this.nodeToDelete.id)
                        return null;
                    node.children.forEach(function (child, index) {
                        if (child.id === _this.nodeToDelete.id) {
                            node.children.splice(index, 1);
                            return;
                        }
                        spliceNode_1(child);
                    });
                };
                var data = this.uiVar.organizations.dataSource.data.slice();
                spliceNode_1(data[0]);
                // toDO: Performance   
                // bug on tree refresh
                // https://github.com/angular/components/issues/11381   
                this.uiVar.organizations.dataSource.data = null;
                this.uiVar.organizations.dataSource.data = data;
                this.nodeToDelete = null;
                return;
            }
            if (this.nodeChanged) {
                this.priVar.currentNode = this.nodeChanged;
                // toDO: Performance   
                // bug on tree refresh
                // https://github.com/angular/components/issues/11381        
                var data = this.uiVar.organizations.dataSource.data.slice();
                this.uiVar.organizations.dataSource.data = null;
                this.uiVar.organizations.dataSource.data = data;
                this.nodeChanged = null;
            }
            if (this.selectedNodesSynchronize || this.notSelectableOrganizations) {
                if (!this.priVar.selectedNodesSynchronizeSave) {
                    this.priVar.selectedNodesSynchronizeSave = this.selectedNodesSynchronize.slice();
                }
                this.privFunc.selectedNodeSynchronize();
            }
        };
        FofOrganizationsTreeComponent.ɵfac = function FofOrganizationsTreeComponent_Factory(t) { return new (t || FofOrganizationsTreeComponent)(core["ɵɵdirectiveInject"](FofPermissionService)); };
        FofOrganizationsTreeComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofOrganizationsTreeComponent, selectors: [["fof-organizations-tree"]], inputs: { nodeChanged: "nodeChanged", nodeToDelete: "nodeToDelete", multiSelect: "multiSelect", selectedNodesSynchronize: "selectedNodesSynchronize", notSelectableOrganizations: "notSelectableOrganizations" }, outputs: { selectedOrganizationsChange: "selectedOrganizationsChange" }, features: [core["ɵɵNgOnChangesFeature"]], decls: 3, vars: 3, consts: [[1, "treeview", 3, "dataSource", "treeControl"], ["matTreeNodeToggle", "", 4, "matTreeNodeDef"], [4, "matTreeNodeDef", "matTreeNodeDefWhen"], ["matTreeNodeToggle", ""], [1, "mat-tree-node"], ["mat-icon-button", "", "disabled", ""], [3, "checked", "disabled", "change"], ["mat-icon-button", "", "matTreeNodeToggle", "", 1, "fof-test-btn"], [1, "mat-icon-rtl-mirror"], [1, ""], ["matTreeNodeOutlet", ""]], template: function FofOrganizationsTreeComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-tree", 0);
                core["ɵɵtemplate"](1, FofOrganizationsTreeComponent_mat_tree_node_1_Template, 5, 3, "mat-tree-node", 1);
                core["ɵɵtemplate"](2, FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template, 10, 6, "mat-nested-tree-node", 2);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵproperty"]("dataSource", ctx.uiVar.organizations.dataSource)("treeControl", ctx.uiVar.organizations.treeControl);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("matTreeNodeDefWhen", ctx.uiAction.OrganizationNodeHasChild);
            } }, directives: [tree$1.MatTree, tree$1.MatTreeNodeDef, tree$1.MatTreeNode, tree$1.MatTreeNodeToggle, button.MatButton, checkbox.MatCheckbox, tree$1.MatNestedTreeNode, icon.MatIcon, tree$1.MatTreeNodeOutlet], styles: [".mat-tree[_ngcontent-%COMP%]{height:100%;margin-bottom:15px}.mat-tree[_ngcontent-%COMP%]   .mat-tree-node[_ngcontent-%COMP%]{margin-right:15px}.treeview-invisible[_ngcontent-%COMP%]{display:none}.treeview[_ngcontent-%COMP%]   li[_ngcontent-%COMP%], .treeview[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{margin-top:0;margin-bottom:0;list-style-type:none}.treeview[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{padding-left:25px}.mat-tree-node[_ngcontent-%COMP%]{min-height:25px}.mat-form-field[_ngcontent-%COMP%]{width:100%}"] });
        return FofOrganizationsTreeComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofOrganizationsTreeComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-organizations-tree',
                    templateUrl: './fof-organizations-tree.component.html',
                    styleUrls: ['./fof-organizations-tree.component.scss']
                }]
        }], function () { return [{ type: FofPermissionService }]; }, { nodeChanged: [{
                type: core.Input
            }], nodeToDelete: [{
                type: core.Input
            }], multiSelect: [{
                type: core.Input
            }], selectedNodesSynchronize: [{
                type: core.Input
            }], notSelectableOrganizations: [{
                type: core.Input
            }], selectedOrganizationsChange: [{
                type: core.Output
            }] }); })();

    var ComponentsService = /** @class */ (function () {
        function ComponentsService() {
        }
        ComponentsService.ɵfac = function ComponentsService_Factory(t) { return new (t || ComponentsService)(); };
        ComponentsService.ɵprov = core["ɵɵdefineInjectable"]({ token: ComponentsService, factory: ComponentsService.ɵfac, providedIn: 'root' });
        return ComponentsService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](ComponentsService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return []; }, null); })();

    var FofPermissionModule = /** @class */ (function () {
        function FofPermissionModule() {
        }
        FofPermissionModule.ɵmod = core["ɵɵdefineNgModule"]({ type: FofPermissionModule });
        FofPermissionModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function FofPermissionModule_Factory(t) { return new (t || FofPermissionModule)(); }, providers: [
                FofPermissionService
            ], imports: [[
                    common.CommonModule
                ]] });
        return FofPermissionModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](FofPermissionModule, { imports: [common.CommonModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofPermissionModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [],
                    imports: [
                        common.CommonModule
                    ],
                    providers: [
                        FofPermissionService
                    ]
                }]
        }], null, null); })();

    var MaterialModule = /** @class */ (function () {
        function MaterialModule() {
        }
        MaterialModule.ɵmod = core["ɵɵdefineNgModule"]({ type: MaterialModule });
        MaterialModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [[
                    // MatAutocompleteModule,
                    // MatBadgeModule,
                    // MatBottomSheetModule,
                    button.MatButtonModule,
                    // MatButtonToggleModule,
                    card.MatCardModule,
                    checkbox.MatCheckboxModule,
                    chips.MatChipsModule,
                    // MatDatepickerModule,
                    dialog.MatDialogModule,
                    divider.MatDividerModule,
                    expansion.MatExpansionModule,
                    // MatGridListModule,
                    icon.MatIconModule,
                    input.MatInputModule,
                    list.MatListModule,
                    // MatMenuModule,
                    // MatNativeDateModule,
                    paginator.MatPaginatorModule,
                    // MatProgressBarModule,
                    progressSpinner.MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    // MatSelectModule,
                    // MatSidenavModule,
                    // MatSliderModule,
                    // MatSlideToggleModule,
                    // MatSnackBarModule,
                    sort.MatSortModule,
                    // MatStepperModule,
                    table.MatTableModule,
                    // MatTabsModule,
                    // MatToolbarModule,
                    // MatTooltipModule,
                    tree$1.MatTreeModule
                ],
                // MatAutocompleteModule,
                // MatBadgeModule,
                // MatBottomSheetModule,
                button.MatButtonModule,
                // MatButtonToggleModule,
                card.MatCardModule,
                checkbox.MatCheckboxModule,
                chips.MatChipsModule,
                // MatDatepickerModule,
                dialog.MatDialogModule,
                divider.MatDividerModule,
                expansion.MatExpansionModule,
                // MatGridListModule,
                icon.MatIconModule,
                input.MatInputModule,
                list.MatListModule,
                // MatMenuModule,
                // MatNativeDateModule,
                paginator.MatPaginatorModule,
                // MatProgressBarModule,
                progressSpinner.MatProgressSpinnerModule,
                // MatRadioModule,
                // MatRippleModule,
                // MatSelectModule,
                // MatSidenavModule,
                // MatSliderModule,
                // MatSlideToggleModule,
                // MatSnackBarModule,
                sort.MatSortModule,
                // MatStepperModule,
                table.MatTableModule,
                // MatTabsModule,
                // MatToolbarModule,
                // MatTooltipModule,
                tree$1.MatTreeModule] });
        return MaterialModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](MaterialModule, { imports: [
            // MatAutocompleteModule,
            // MatBadgeModule,
            // MatBottomSheetModule,
            button.MatButtonModule,
            // MatButtonToggleModule,
            card.MatCardModule,
            checkbox.MatCheckboxModule,
            chips.MatChipsModule,
            // MatDatepickerModule,
            dialog.MatDialogModule,
            divider.MatDividerModule,
            expansion.MatExpansionModule,
            // MatGridListModule,
            icon.MatIconModule,
            input.MatInputModule,
            list.MatListModule,
            // MatMenuModule,
            // MatNativeDateModule,
            paginator.MatPaginatorModule,
            // MatProgressBarModule,
            progressSpinner.MatProgressSpinnerModule,
            // MatRadioModule,
            // MatRippleModule,
            // MatSelectModule,
            // MatSidenavModule,
            // MatSliderModule,
            // MatSlideToggleModule,
            // MatSnackBarModule,
            sort.MatSortModule,
            // MatStepperModule,
            table.MatTableModule,
            // MatTabsModule,
            // MatToolbarModule,
            // MatTooltipModule,
            tree$1.MatTreeModule], exports: [
            // MatAutocompleteModule,
            // MatBadgeModule,
            // MatBottomSheetModule,
            button.MatButtonModule,
            // MatButtonToggleModule,
            card.MatCardModule,
            checkbox.MatCheckboxModule,
            chips.MatChipsModule,
            // MatDatepickerModule,
            dialog.MatDialogModule,
            divider.MatDividerModule,
            expansion.MatExpansionModule,
            // MatGridListModule,
            icon.MatIconModule,
            input.MatInputModule,
            list.MatListModule,
            // MatMenuModule,
            // MatNativeDateModule,
            paginator.MatPaginatorModule,
            // MatProgressBarModule,
            progressSpinner.MatProgressSpinnerModule,
            // MatRadioModule,
            // MatRippleModule,
            // MatSelectModule,
            // MatSidenavModule,
            // MatSliderModule,
            // MatSlideToggleModule,
            // MatSnackBarModule,
            sort.MatSortModule,
            // MatStepperModule,
            table.MatTableModule,
            // MatTabsModule,
            // MatToolbarModule,
            // MatTooltipModule,
            tree$1.MatTreeModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](MaterialModule, [{
            type: core.NgModule,
            args: [{
                    imports: [
                        // MatAutocompleteModule,
                        // MatBadgeModule,
                        // MatBottomSheetModule,
                        button.MatButtonModule,
                        // MatButtonToggleModule,
                        card.MatCardModule,
                        checkbox.MatCheckboxModule,
                        chips.MatChipsModule,
                        // MatDatepickerModule,
                        dialog.MatDialogModule,
                        divider.MatDividerModule,
                        expansion.MatExpansionModule,
                        // MatGridListModule,
                        icon.MatIconModule,
                        input.MatInputModule,
                        list.MatListModule,
                        // MatMenuModule,
                        // MatNativeDateModule,
                        paginator.MatPaginatorModule,
                        // MatProgressBarModule,
                        progressSpinner.MatProgressSpinnerModule,
                        // MatRadioModule,
                        // MatRippleModule,
                        // MatSelectModule,
                        // MatSidenavModule,
                        // MatSliderModule,
                        // MatSlideToggleModule,
                        // MatSnackBarModule,
                        sort.MatSortModule,
                        // MatStepperModule,
                        table.MatTableModule,
                        // MatTabsModule,
                        // MatToolbarModule,
                        // MatTooltipModule,
                        tree$1.MatTreeModule
                    ],
                    exports: [
                        // MatAutocompleteModule,
                        // MatBadgeModule,
                        // MatBottomSheetModule,
                        button.MatButtonModule,
                        // MatButtonToggleModule,
                        card.MatCardModule,
                        checkbox.MatCheckboxModule,
                        chips.MatChipsModule,
                        // MatDatepickerModule,
                        dialog.MatDialogModule,
                        divider.MatDividerModule,
                        expansion.MatExpansionModule,
                        // MatGridListModule,
                        icon.MatIconModule,
                        input.MatInputModule,
                        list.MatListModule,
                        // MatMenuModule,
                        // MatNativeDateModule,
                        paginator.MatPaginatorModule,
                        // MatProgressBarModule,
                        progressSpinner.MatProgressSpinnerModule,
                        // MatRadioModule,
                        // MatRippleModule,
                        // MatSelectModule,
                        // MatSidenavModule,
                        // MatSliderModule,
                        // MatSlideToggleModule,
                        // MatSnackBarModule,
                        sort.MatSortModule,
                        // MatStepperModule,
                        table.MatTableModule,
                        // MatTabsModule,
                        // MatToolbarModule,
                        // MatTooltipModule,
                        tree$1.MatTreeModule
                    ],
                    declarations: []
                }]
        }], null, null); })();

    function FofOrganizationsMultiSelectComponent_mat_chip_4_Template(rf, ctx) { if (rf & 1) {
        var _r193 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "mat-chip", 7);
        core["ɵɵlistener"]("click", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_click_0_listener() { core["ɵɵrestoreView"](_r193); var ctx_r192 = core["ɵɵnextContext"](); return ctx_r192.uiAction.openTree(); })("removed", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_removed_0_listener() { core["ɵɵrestoreView"](_r193); var organisation_r191 = ctx.$implicit; var ctx_r194 = core["ɵɵnextContext"](); return ctx_r194.uiAction.organisationRemove(organisation_r191); });
        core["ɵɵtext"](1);
        core["ɵɵelementStart"](2, "mat-icon", 8);
        core["ɵɵtext"](3, "cancel");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var organisation_r191 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", organisation_r191.name, " ");
    } }
    // https://stackblitz.com/edit/menu-overlay?file=src%2Fapp%2Fselect-input.component.html
    var FofOrganizationsMultiSelectComponent = /** @class */ (function () {
        function FofOrganizationsMultiSelectComponent(elementRef) {
            var _this = this;
            this.elementRef = elementRef;
            this.multiSelect = true;
            this.selectedOrganisations = [];
            this.placeHolder = 'Organisations';
            this.selectedOrganizationsChange = new core.EventEmitter();
            // All private variables
            this.priVar = {};
            // All private functions
            this.privFunc = {
                refreshOuputs: function () {
                    _this.selectedOrganizationsChange.emit(_this.selectedOrganisations);
                }
            };
            // All variables shared with UI 
            this.uiVar = {
                treeMenuVisible: false,
            };
            // All actions shared with UI 
            this.uiAction = {
                openTree: function () {
                    _this.uiVar.treeMenuVisible = !_this.uiVar.treeMenuVisible;
                },
                selectedOrganizationsChange: function (selectedOrganisations) {
                    if (!selectedOrganisations) {
                        _this.selectedOrganisations = null;
                        return;
                    }
                    if (Array.isArray(selectedOrganisations)) {
                        _this.selectedOrganisations = selectedOrganisations;
                    }
                    else {
                        _this.selectedOrganisations = [selectedOrganisations];
                    }
                    _this.privFunc.refreshOuputs();
                },
                // Remove organisation from chip
                organisationRemove: function (organisationToRemove) {
                    _this.selectedOrganisations = _this.selectedOrganisations.filter(function (organisation) { return organisationToRemove.id !== organisation.id; });
                    _this.privFunc.refreshOuputs();
                }
            };
        }
        FofOrganizationsMultiSelectComponent.prototype.onDocumentClick = function (event) {
            if (!this.elementRef.nativeElement.contains(event.target)) {
                this.uiVar.treeMenuVisible = false;
            }
        };
        // Angular events
        FofOrganizationsMultiSelectComponent.prototype.ngOnInit = function () {
        };
        FofOrganizationsMultiSelectComponent.ɵfac = function FofOrganizationsMultiSelectComponent_Factory(t) { return new (t || FofOrganizationsMultiSelectComponent)(core["ɵɵdirectiveInject"](core.ElementRef)); };
        FofOrganizationsMultiSelectComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofOrganizationsMultiSelectComponent, selectors: [["fof-core-fof-organizations-multi-select"]], hostBindings: function FofOrganizationsMultiSelectComponent_HostBindings(rf, ctx) { if (rf & 1) {
                core["ɵɵlistener"]("click", function FofOrganizationsMultiSelectComponent_click_HostBindingHandler($event) { return ctx.onDocumentClick($event); }, false, core["ɵɵresolveDocument"]);
            } }, inputs: { multiSelect: "multiSelect", selectedOrganisations: "selectedOrganisations", notSelectableOrganizations: "notSelectableOrganizations", placeHolder: "placeHolder" }, outputs: { selectedOrganizationsChange: "selectedOrganizationsChange" }, decls: 9, vars: 8, consts: [[1, "fof-menu-custom"], ["chipList", ""], [3, "click", "removed", 4, "ngFor", "ngForOf"], ["matInput", "", "readonly", "", 3, "placeholder", "matChipInputFor", "click"], [1, "cdk-overlay-pane"], [1, "organization-menu", "mat-menu-panel", "mat-elevation-z4"], [3, "multiSelect", "selectedNodesSynchronize", "notSelectableOrganizations", "selectedOrganizationsChange"], [3, "click", "removed"], ["matChipRemove", ""]], template: function FofOrganizationsMultiSelectComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div", 0);
                core["ɵɵelementStart"](1, "mat-form-field");
                core["ɵɵelementStart"](2, "mat-chip-list", null, 1);
                core["ɵɵtemplate"](4, FofOrganizationsMultiSelectComponent_mat_chip_4_Template, 4, 1, "mat-chip", 2);
                core["ɵɵelementStart"](5, "input", 3);
                core["ɵɵlistener"]("click", function FofOrganizationsMultiSelectComponent_Template_input_click_5_listener() { return ctx.uiAction.openTree(); });
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "div", 4);
                core["ɵɵelementStart"](7, "div", 5);
                core["ɵɵelementStart"](8, "fof-organizations-tree", 6);
                core["ɵɵlistener"]("selectedOrganizationsChange", function FofOrganizationsMultiSelectComponent_Template_fof_organizations_tree_selectedOrganizationsChange_8_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                var _r189 = core["ɵɵreference"](3);
                core["ɵɵadvance"](4);
                core["ɵɵproperty"]("ngForOf", ctx.selectedOrganisations);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("placeholder", ctx.placeHolder)("matChipInputFor", _r189);
                core["ɵɵadvance"](1);
                core["ɵɵclassProp"]("tree-display", ctx.uiVar.treeMenuVisible);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("multiSelect", ctx.multiSelect)("selectedNodesSynchronize", ctx.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
            } }, directives: [formField.MatFormField, chips.MatChipList, common.NgForOf, input.MatInput, chips.MatChipInput, FofOrganizationsTreeComponent, chips.MatChip, icon.MatIcon, chips.MatChipRemove], styles: [".fof-menu-custom[_ngcontent-%COMP%]{position:relative}.fof-menu-custom[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]{width:100%;position:absolute;top:60px;display:none}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane.tree-display[_ngcontent-%COMP%]{display:block}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu[_ngcontent-%COMP%]{min-width:112px;overflow:auto;-webkit-overflow-scrolling:touch;border-radius:4px;height:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu.mat-menu-panel[_ngcontent-%COMP%]{max-width:100%;max-height:unset}"] });
        return FofOrganizationsMultiSelectComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofOrganizationsMultiSelectComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-fof-organizations-multi-select',
                    templateUrl: './fof-organizations-multi-select.component.html',
                    styleUrls: ['./fof-organizations-multi-select.component.scss'],
                    host: {
                        '(document:click)': 'onDocumentClick($event)',
                    }
                }]
        }], function () { return [{ type: core.ElementRef }]; }, { multiSelect: [{
                type: core.Input
            }], selectedOrganisations: [{
                type: core.Input
            }], notSelectableOrganizations: [{
                type: core.Input
            }], placeHolder: [{
                type: core.Input
            }], selectedOrganizationsChange: [{
                type: core.Output
            }] }); })();

    var Notification = /** @class */ (function () {
        function Notification() {
        }
        return Notification;
    }());
    var NotificationType;
    (function (NotificationType) {
        NotificationType[NotificationType["Success"] = 0] = "Success";
        NotificationType[NotificationType["Error"] = 1] = "Error";
        NotificationType[NotificationType["Info"] = 2] = "Info";
        NotificationType[NotificationType["Warning"] = 3] = "Warning";
    })(NotificationType || (NotificationType = {}));
    /** params for notification */
    var NotificationParams = /** @class */ (function () {
        function NotificationParams() {
        }
        return NotificationParams;
    }());

    var FofNotificationService = /** @class */ (function () {
        function FofNotificationService() {
            this.saveIsDoneNotification = new rxjs.BehaviorSubject(undefined);
            this.subject = new rxjs.Subject();
            this.keepAfterRouteChange = false;
            // const router = this.injector.get(Router)
            // clear notification messages on route change unless 'keepAfterRouteChange' flag is true
            // router.events.subscribe(event => {
            //   if (event instanceof NavigationStart) {
            //     if (this.keepAfterRouteChange) {
            //       // only keep for a single route change
            //       this.keepAfterRouteChange = false
            //     } else {
            //       // clear notification messages
            //       this.clear()
            //     }
            //   }
            // })
        }
        Object.defineProperty(FofNotificationService.prototype, "savedNotification", {
            get: function () {
                return this.saveIsDoneNotification.asObservable();
            },
            enumerable: true,
            configurable: true
        });
        FofNotificationService.prototype.getnotification = function () {
            return this.subject.asObservable();
        };
        /** Notifiy a success
         * @param message
         * accept html
         */
        FofNotificationService.prototype.success = function (message, params) {
            this.notification(NotificationType.Success, message, params);
        };
        /** Notifiy an error
         * @param message
         * accept html
         */
        FofNotificationService.prototype.error = function (message, params) {
            this.notification(NotificationType.Error, message, params);
        };
        /** Notifiy an info
         * @param message
         * accept html
         */
        FofNotificationService.prototype.info = function (message, params) {
            this.notification(NotificationType.Info, message, params);
        };
        /** Notifiy a warning
         * @param message
         * accept html
         */
        FofNotificationService.prototype.warn = function (message, params) {
            this.notification(NotificationType.Warning, message, params);
        };
        FofNotificationService.prototype.notification = function (type, message, params) {
            var notification = new Notification();
            if (params) {
                if (params.keepAfterRouteChange) {
                    this.keepAfterRouteChange = params.keepAfterRouteChange;
                }
                if (params.mustDisappearAfter) {
                    if (params.mustDisappearAfter < 0) {
                        params.mustDisappearAfter = undefined;
                    }
                    else {
                        notification.mustDisappearAfter = params.mustDisappearAfter;
                    }
                }
                else {
                    notification.mustDisappearAfter = 3000;
                }
            }
            else {
                notification.mustDisappearAfter = 3000;
            }
            notification.type = type;
            notification.message = message;
            this.subject.next(notification);
        };
        FofNotificationService.prototype.saveIsDone = function () {
            this.saveIsDoneNotification.next(true);
        };
        FofNotificationService.prototype.clear = function () {
            // clear notifications
            this.subject.next();
        };
        FofNotificationService.ɵfac = function FofNotificationService_Factory(t) { return new (t || FofNotificationService)(); };
        FofNotificationService.ɵprov = core["ɵɵdefineInjectable"]({ token: FofNotificationService, factory: FofNotificationService.ɵfac, providedIn: 'root' });
        return FofNotificationService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofNotificationService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return []; }, null); })();

    var FofDatatableComponent = /** @class */ (function () {
        function FofDatatableComponent(fofPermissionService, fofNotificationService, breakpointObserver) {
            this.fofPermissionService = fofPermissionService;
            this.fofNotificationService = fofNotificationService;
            this.breakpointObserver = breakpointObserver;
            // All private variables
            this.priVar = {
                breakpointObserverSub: undefined,
            };
            // All private functions
            this.privFunc = {};
            // All variables shared with UI 
            this.uiVar = {
                displayedColumns: ['code', 'description'],
                data: [],
                resultsLength: 0,
                pageSize: 5,
                isLoadingResults: true
            };
            // All actions shared with UI 
            this.uiAction = {};
        }
        // Angular events
        FofDatatableComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.priVar.breakpointObserverSub = this.breakpointObserver.observe(layout.Breakpoints.XSmall)
                .subscribe(function (state) {
                if (state.matches) {
                    // XSmall
                    _this.uiVar.displayedColumns = ['code'];
                }
                else {
                    // > XSmall
                    _this.uiVar.displayedColumns = ['code', 'description'];
                }
            });
        };
        FofDatatableComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            // If the user changes the sort order, reset back to the first page.
            this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
            rxjs.merge(this.sort.sortChange, this.paginator.page)
                .pipe(operators.startWith({}), operators.switchMap(function () {
                _this.uiVar.isLoadingResults = true;
                _this.uiVar.pageSize = _this.paginator.pageSize;
                return _this.fofPermissionService.role.search(null, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
            }), operators.map(function (search) {
                _this.uiVar.isLoadingResults = false;
                _this.uiVar.resultsLength = search.total;
                return search.data;
            }), operators.catchError(function () {
                _this.uiVar.isLoadingResults = false;
                return rxjs.of([]);
            })).subscribe(function (data) { return _this.uiVar.data = data; });
        };
        FofDatatableComponent.prototype.ngOnDestroy = function () {
            if (this.priVar.breakpointObserverSub) {
                this.priVar.breakpointObserverSub.unsubscribe();
            }
        };
        FofDatatableComponent.ɵfac = function FofDatatableComponent_Factory(t) { return new (t || FofDatatableComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](layout.BreakpointObserver)); };
        FofDatatableComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofDatatableComponent, selectors: [["fof-core-fof-datatable"]], viewQuery: function FofDatatableComponent_Query(rf, ctx) { if (rf & 1) {
                core["ɵɵviewQuery"](paginator.MatPaginator, true);
                core["ɵɵviewQuery"](sort.MatSort, true);
            } if (rf & 2) {
                var _t;
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.paginator = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.sort = _t.first);
            } }, decls: 2, vars: 0, template: function FofDatatableComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "p");
                core["ɵɵtext"](1, "fof-datatable works!");
                core["ɵɵelementEnd"]();
            } }, styles: [""] });
        return FofDatatableComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofDatatableComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-fof-datatable',
                    templateUrl: './fof-datatable.component.html',
                    styleUrls: ['./fof-datatable.component.scss']
                }]
        }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: layout.BreakpointObserver }]; }, { paginator: [{
                type: core.ViewChild,
                args: [paginator.MatPaginator]
            }], sort: [{
                type: core.ViewChild,
                args: [sort.MatSort]
            }] }); })();

    var ComponentsModule = /** @class */ (function () {
        function ComponentsModule() {
        }
        ComponentsModule.ɵmod = core["ɵɵdefineNgModule"]({ type: ComponentsModule });
        ComponentsModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function ComponentsModule_Factory(t) { return new (t || ComponentsModule)(); }, providers: [
                ComponentsService
            ], imports: [[
                    common.CommonModule,
                    FofPermissionModule,
                    MaterialModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule
                ]] });
        return ComponentsModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](ComponentsModule, { declarations: [FofOrganizationsTreeComponent,
            FofOrganizationsMultiSelectComponent,
            FofDatatableComponent], imports: [common.CommonModule,
            FofPermissionModule,
            MaterialModule,
            forms.FormsModule,
            forms.ReactiveFormsModule], exports: [FofOrganizationsTreeComponent,
            FofOrganizationsMultiSelectComponent] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](ComponentsModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [
                        FofOrganizationsTreeComponent,
                        FofOrganizationsMultiSelectComponent,
                        FofDatatableComponent
                    ],
                    imports: [
                        common.CommonModule,
                        FofPermissionModule,
                        MaterialModule,
                        forms.FormsModule,
                        forms.ReactiveFormsModule
                    ],
                    providers: [
                        ComponentsService
                    ],
                    exports: [
                        FofOrganizationsTreeComponent,
                        FofOrganizationsMultiSelectComponent
                    ]
                }]
        }], null, null); })();

    function FofRolesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 13);
        core["ɵɵelement"](1, "mat-spinner", 14);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des roles...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function FofRolesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 15);
        core["ɵɵtext"](1, "Code");
        core["ɵɵelementEnd"]();
    } }
    function FofRolesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 16);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r202 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r202.code);
    } }
    function FofRolesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 17);
        core["ɵɵtext"](1, "Description");
        core["ɵɵelementEnd"]();
    } }
    function FofRolesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 16);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r203 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r203.description);
    } }
    function FofRolesComponent_tr_15_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 18);
    } }
    function FofRolesComponent_tr_16_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 19);
    } if (rf & 2) {
        var row_r204 = ctx.$implicit;
        core["ɵɵproperty"]("routerLink", row_r204.code);
    } }
    var _c0 = function () { return [5, 10, 25, 100]; };
    var FofRolesComponent = /** @class */ (function () {
        function FofRolesComponent(fofPermissionService, fofNotificationService, breakpointObserver) {
            this.fofPermissionService = fofPermissionService;
            this.fofNotificationService = fofNotificationService;
            this.breakpointObserver = breakpointObserver;
            // All private variables
            this.priVar = {
                breakpointObserverSub: undefined,
            };
            // All private functions
            this.privFunc = {};
            // All variables shared with UI 
            this.uiVar = {
                displayedColumns: ['code', 'description'],
                data: [],
                resultsLength: 0,
                pageSize: 5,
                isLoadingResults: true
            };
            // All actions shared with UI 
            this.uiAction = {};
        }
        // Angular events
        FofRolesComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.priVar.breakpointObserverSub = this.breakpointObserver.observe(layout.Breakpoints.XSmall)
                .subscribe(function (state) {
                if (state.matches) {
                    // XSmall
                    _this.uiVar.displayedColumns = ['code'];
                }
                else {
                    // > XSmall
                    _this.uiVar.displayedColumns = ['code', 'description'];
                }
            });
        };
        FofRolesComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            // If the user changes the sort order, reset back to the first page.
            this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
            rxjs.merge(this.sort.sortChange, this.paginator.page)
                .pipe(operators.startWith({}), operators.switchMap(function () {
                _this.uiVar.isLoadingResults = true;
                _this.uiVar.pageSize = _this.paginator.pageSize;
                return _this.fofPermissionService.role.search(null, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
            }), operators.map(function (search) {
                _this.uiVar.isLoadingResults = false;
                _this.uiVar.resultsLength = search.total;
                return search.data;
            }), operators.catchError(function () {
                _this.uiVar.isLoadingResults = false;
                return rxjs.of([]);
            })).subscribe(function (data) { return _this.uiVar.data = data; });
        };
        FofRolesComponent.prototype.ngOnDestroy = function () {
            if (this.priVar.breakpointObserverSub) {
                this.priVar.breakpointObserverSub.unsubscribe();
            }
        };
        FofRolesComponent.ɵfac = function FofRolesComponent_Factory(t) { return new (t || FofRolesComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](layout.BreakpointObserver)); };
        FofRolesComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofRolesComponent, selectors: [["fof-roles"]], viewQuery: function FofRolesComponent_Query(rf, ctx) { if (rf & 1) {
                core["ɵɵviewQuery"](paginator.MatPaginator, true);
                core["ɵɵviewQuery"](sort.MatSort, true);
            } if (rf & 2) {
                var _t;
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.paginator = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.sort = _t.first);
            } }, decls: 18, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "code", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "code"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "description"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function FofRolesComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-card", 0);
                core["ɵɵelementStart"](1, "h3");
                core["ɵɵtext"](2, "R\u00F4les");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "a", 1);
                core["ɵɵelementStart"](4, "span");
                core["ɵɵtext"](5, "Ajouter un r\u00F4le");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "div", 2);
                core["ɵɵtemplate"](7, FofRolesComponent_div_7_Template, 4, 0, "div", 3);
                core["ɵɵelementStart"](8, "table", 4);
                core["ɵɵelementContainerStart"](9, 5);
                core["ɵɵtemplate"](10, FofRolesComponent_th_10_Template, 2, 0, "th", 6);
                core["ɵɵtemplate"](11, FofRolesComponent_td_11_Template, 2, 1, "td", 7);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](12, 8);
                core["ɵɵtemplate"](13, FofRolesComponent_th_13_Template, 2, 0, "th", 9);
                core["ɵɵtemplate"](14, FofRolesComponent_td_14_Template, 2, 1, "td", 7);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵtemplate"](15, FofRolesComponent_tr_15_Template, 1, 0, "tr", 10);
                core["ɵɵtemplate"](16, FofRolesComponent_tr_16_Template, 1, 1, "tr", 11);
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](17, "mat-paginator", 12);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](3);
                core["ɵɵproperty"]("routerLink", "/admin/roles/new");
                core["ɵɵadvance"](4);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.isLoadingResults);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("dataSource", ctx.uiVar.data);
                core["ɵɵadvance"](7);
                core["ɵɵproperty"]("matHeaderRowDef", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("matRowDefColumns", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("length", ctx.uiVar.resultsLength)("pageSizeOptions", core["ɵɵpureFunction0"](8, _c0))("pageSize", ctx.uiVar.pageSize);
            } }, directives: [card.MatCard, button.MatAnchor, router.RouterLinkWithHref, common.NgIf, table.MatTable, sort.MatSort, table.MatColumnDef, table.MatHeaderCellDef, table.MatCellDef, table.MatHeaderRowDef, table.MatRowDef, paginator.MatPaginator, progressSpinner.MatSpinner, table.MatHeaderCell, sort.MatSortHeader, table.MatCell, table.MatHeaderRow, table.MatRow, router.RouterLink], styles: [""] });
        return FofRolesComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofRolesComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-roles',
                    templateUrl: './fof-roles.component.html',
                    styleUrls: ['./fof-roles.component.scss'],
                }]
        }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: layout.BreakpointObserver }]; }, { paginator: [{
                type: core.ViewChild,
                args: [paginator.MatPaginator]
            }], sort: [{
                type: core.ViewChild,
                args: [sort.MatSort]
            }] }); })();

    var _this = this;
    var fofUtilsForm = {
        validateAllFields: function (formGroup) {
            Object.keys(formGroup.controls).forEach(function (field) {
                var control = formGroup.get(field);
                if (control instanceof forms.FormControl) {
                    control.markAsTouched({ onlySelf: true });
                }
                else if (control instanceof forms.FormGroup) {
                    _this.validateAllFormFields(control);
                }
            });
        }
    };
    var fofError = {
        clean: function (errorToManage) {
            var error;
            var message;
            var isConstraintError = false;
            if (errorToManage.error) {
                error = errorToManage.error;
                if (error.fofCoreException) {
                    message = error.exceptionDetail.message;
                    if (message.search('constraint') > -1) {
                        isConstraintError = true;
                    }
                    return __assign({ isConstraintError: isConstraintError }, error);
                }
            }
        },
        cleanAndMange: function (errorToManage) {
        }
    };

    // Angular
    function FofCoreDialogYesNoComponent_button_6_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "button", 5);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r205 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](ctx_r205.uiVar.noLabel);
    } }
    var FofCoreDialogYesNoComponent = /** @class */ (function () {
        function FofCoreDialogYesNoComponent(data) {
            this.data = data;
            // All input var
            this.anInput = undefined;
            // All output notification
            this.anOuputEvent = new core.EventEmitter();
            // All variables private to the class
            this.priVar = {};
            // All private functions
            this.privFunc = {};
            // All variables shared with UI HTML interface
            this.uiVar = {
                title: 'fof à une question',
                question: undefined,
                yesLabel: 'oui',
                noLabel: 'non',
                informationOnly: false
            };
            // All actions accessible by user (could be used internaly too)
            this.uiAction = {};
            // this.matDialogRef.updatePosition({ top: '20px', left: '50px' });
            this.uiVar.question = data.question;
            if (data.title) {
                this.uiVar.title = data.title;
            }
            if (data.yesLabel) {
                this.uiVar.yesLabel = data.yesLabel;
            }
            if (data.noLabel) {
                this.uiVar.noLabel = data.noLabel;
            }
            if (data.informationOnly) {
                this.uiVar.informationOnly = data.informationOnly;
            }
        }
        // Angular events
        FofCoreDialogYesNoComponent.prototype.ngOnInit = function () {
        };
        FofCoreDialogYesNoComponent.prototype.ngAfterViewInit = function () {
        };
        FofCoreDialogYesNoComponent.ɵfac = function FofCoreDialogYesNoComponent_Factory(t) { return new (t || FofCoreDialogYesNoComponent)(core["ɵɵdirectiveInject"](dialog.MAT_DIALOG_DATA, 8)); };
        FofCoreDialogYesNoComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofCoreDialogYesNoComponent, selectors: [["core-dialog-yes-no"]], inputs: { anInput: "anInput" }, outputs: { anOuputEvent: "anOuputEvent" }, decls: 9, vars: 5, consts: [[1, "view-mad--core--yes-no"], ["mat-dialog-title", ""], [3, "innerHTML"], ["mat-button", "", "mat-dialog-close", "", 4, "ngIf"], ["mat-button", "", 3, "mat-dialog-close"], ["mat-button", "", "mat-dialog-close", ""]], template: function FofCoreDialogYesNoComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div", 0);
                core["ɵɵelementStart"](1, "h2", 1);
                core["ɵɵtext"](2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "mat-dialog-content");
                core["ɵɵelement"](4, "div", 2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](5, "mat-dialog-actions");
                core["ɵɵtemplate"](6, FofCoreDialogYesNoComponent_button_6_Template, 2, 1, "button", 3);
                core["ɵɵelementStart"](7, "button", 4);
                core["ɵɵtext"](8);
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.uiVar.title);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("innerHTML", ctx.uiVar.question, core["ɵɵsanitizeHtml"]);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("ngIf", !ctx.uiVar.informationOnly);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("mat-dialog-close", true);
                core["ɵɵadvance"](1);
                core["ɵɵtextInterpolate"](ctx.uiVar.yesLabel);
            } }, directives: [dialog.MatDialogTitle, dialog.MatDialogContent, dialog.MatDialogActions, common.NgIf, button.MatButton, dialog.MatDialogClose], styles: [".view-mad--core--yes-no .mat-dialog-actions{margin-top:1rem;display:flex;justify-content:flex-end}"], encapsulation: 2 });
        return FofCoreDialogYesNoComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofCoreDialogYesNoComponent, [{
            type: core.Component,
            args: [{
                    selector: 'core-dialog-yes-no',
                    templateUrl: './core-dialog-yes-no.component.html',
                    styleUrls: ['./core-dialog-yes-no.component.scss'],
                    encapsulation: core.ViewEncapsulation.None
                }]
        }], function () { return [{ type: undefined, decorators: [{
                    type: core.Optional
                }, {
                    type: core.Inject,
                    args: [dialog.MAT_DIALOG_DATA]
                }] }]; }, { anInput: [{
                type: core.Input
            }], anOuputEvent: [{
                type: core.Output
            }] }); })();

    var FofDialogService = /** @class */ (function () {
        function FofDialogService(matDialog) {
            this.matDialog = matDialog;
        }
        FofDialogService.prototype.openYesNo = function (options) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                var _options = options;
                var width = options.width || '300px';
                var height = options.height || undefined;
                _options.informationOnly = _options.informationOnly || false;
                var dialogRef = _this.matDialog.open(FofCoreDialogYesNoComponent, {
                    data: _options,
                    width: width,
                    height: height,
                    // to have a look on, bug
                    position: {
                        top: '150px'
                    }
                });
                dialogRef.afterClosed()
                    .toPromise()
                    .then(function (result) {
                    resolve(result);
                });
            });
        };
        FofDialogService.prototype.openInformation = function (options) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                var _options = options;
                _options.informationOnly = true;
                _options.title = _options.title || 'fof à une information';
                _options.yesLabel = _options.yesLabel || 'ok';
                _this.openYesNo(_options)
                    .then(function (result) {
                    resolve(result);
                });
            });
        };
        FofDialogService.ɵfac = function FofDialogService_Factory(t) { return new (t || FofDialogService)(core["ɵɵinject"](dialog.MatDialog)); };
        FofDialogService.ɵprov = core["ɵɵdefineInjectable"]({ token: FofDialogService, factory: FofDialogService.ɵfac, providedIn: 'root' });
        return FofDialogService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofDialogService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: dialog.MatDialog }]; }, null); })();

    function FofEntityFooterComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 4);
        core["ɵɵtext"](1);
        core["ɵɵpipe"](2, "date");
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r299 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate2"](" Derni\u00E8re modification le ", core["ɵɵpipeBind2"](2, 2, ctx_r299.entityBase._updatedDate, "short"), " par ", ctx_r299.entityBase._updatedByName, " ");
    } }
    function FofEntityFooterComponent_div_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 3);
        core["ɵɵelementStart"](1, "div", 4);
        core["ɵɵtext"](2);
        core["ɵɵpipe"](3, "date");
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](4, FofEntityFooterComponent_div_1_div_4_Template, 3, 5, "div", 5);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r296 = core["ɵɵnextContext"]();
        var _r297 = core["ɵɵreference"](3);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate2"](" Cr\u00E9ation le ", core["ɵɵpipeBind2"](3, 4, ctx_r296.entityBase._createdDate, "short"), " par ", ctx_r296.entityBase._createdByName, " ");
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", ctx_r296.entityBase._updatedByName)("ngIfElse", _r297);
    } }
    function FofEntityFooterComponent_ng_template_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 4);
        core["ɵɵtext"](1, " Aucune modification depuis la cr\u00E9ation ");
        core["ɵɵelementEnd"]();
    } }
    var FofEntityFooterComponent = /** @class */ (function () {
        function FofEntityFooterComponent() {
        }
        FofEntityFooterComponent.prototype.ngOnInit = function () {
        };
        FofEntityFooterComponent.ɵfac = function FofEntityFooterComponent_Factory(t) { return new (t || FofEntityFooterComponent)(); };
        FofEntityFooterComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofEntityFooterComponent, selectors: [["fof-entity-footer"]], inputs: { entityBase: "entityBase" }, decls: 4, vars: 1, consts: [[1, "custom-card"], ["class", "row", 4, "ngIf"], ["elseBlock", ""], [1, "row"], [1, "col-md-6"], ["class", "col-md-6", 4, "ngIf", "ngIfElse"]], template: function FofEntityFooterComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div", 0);
                core["ɵɵtemplate"](1, FofEntityFooterComponent_div_1_Template, 5, 7, "div", 1);
                core["ɵɵtemplate"](2, FofEntityFooterComponent_ng_template_2_Template, 2, 0, "ng-template", null, 2, core["ɵɵtemplateRefExtractor"]);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.entityBase);
            } }, directives: [common.NgIf], pipes: [common.DatePipe], styles: [".custom-card[_ngcontent-%COMP%]{font-size:smaller;padding-top:15px}"] });
        return FofEntityFooterComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofEntityFooterComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-entity-footer',
                    templateUrl: './fof-entity-footer.component.html',
                    styleUrls: ['./fof-entity-footer.component.scss']
                }]
        }], function () { return []; }, { entityBase: [{
                type: core.Input
            }] }); })();

    function FofRoleComponent_div_11_mat_error_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le code du r\u00F4le est obligatoire et doit \u00EAtre compos\u00E9 de au moins 3 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function FofRoleComponent_div_11_mat_error_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " La description ne peut pas exc\u00E9der 200 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function FofRoleComponent_div_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 10);
        core["ɵɵelementStart"](1, "form", 11);
        core["ɵɵelementStart"](2, "mat-form-field");
        core["ɵɵelement"](3, "input", 12);
        core["ɵɵtemplate"](4, FofRoleComponent_div_11_mat_error_4_Template, 2, 0, "mat-error", 13);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](5, "mat-form-field");
        core["ɵɵelement"](6, "textarea", 14);
        core["ɵɵelementStart"](7, "mat-hint");
        core["ɵɵtext"](8, "D\u00E9crivez succintement le r\u00F4le");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](9, "mat-hint", 15);
        core["ɵɵtext"](10);
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](11, FofRoleComponent_div_11_mat_error_11_Template, 2, 0, "mat-error", 13);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r206 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("formGroup", ctx_r206.uiVar.form);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r206.uiVar.form.get("code").invalid);
        core["ɵɵadvance"](6);
        core["ɵɵtextInterpolate1"]("", ctx_r206.uiVar.form.get("description").value == null ? null : ctx_r206.uiVar.form.get("description").value.length, " / 200");
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r206.uiVar.form.get("description").invalid);
    } }
    function FofRoleComponent_div_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 16);
        core["ɵɵelement"](1, "mat-spinner", 17);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des roles et des authorisations...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template(rf, ctx) { if (rf & 1) {
        var _r218 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div");
        core["ɵɵelementStart"](1, "mat-checkbox", 21);
        core["ɵɵlistener"]("change", function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template_mat_checkbox_change_1_listener() { core["ɵɵrestoreView"](_r218); var permission_r216 = ctx.$implicit; var ctx_r217 = core["ɵɵnextContext"](3); return ctx_r217.uiAction.rolePermissionSave(permission_r216); })("ngModelChange", function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template_mat_checkbox_ngModelChange_1_listener($event) { core["ɵɵrestoreView"](_r218); var permission_r216 = ctx.$implicit; return permission_r216.checked = $event; });
        core["ɵɵtext"](2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var permission_r216 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngModel", permission_r216.checked)("checked", permission_r216.checked);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](permission_r216.code);
    } }
    function FofRoleComponent_mat_card_13_mat_card_content_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-card-content");
        core["ɵɵtemplate"](1, FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template, 3, 3, "div", 20);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var group_r211 = core["ɵɵnextContext"]().$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", group_r211.children);
    } }
    function FofRoleComponent_mat_card_13_ng_template_5_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "mat-spinner", 22);
    } }
    function FofRoleComponent_mat_card_13_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-card", 18);
        core["ɵɵelementStart"](1, "mat-card-header");
        core["ɵɵelementStart"](2, "mat-card-title");
        core["ɵɵtext"](3);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](4, FofRoleComponent_mat_card_13_mat_card_content_4_Template, 2, 1, "mat-card-content", 13);
        core["ɵɵtemplate"](5, FofRoleComponent_mat_card_13_ng_template_5_Template, 1, 0, "ng-template", null, 19, core["ɵɵtemplateRefExtractor"]);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var group_r211 = ctx.$implicit;
        core["ɵɵadvance"](3);
        core["ɵɵtextInterpolate"](group_r211.code);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", group_r211.children);
    } }
    var FofRoleComponent = /** @class */ (function () {
        function FofRoleComponent(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router) {
            var _this = this;
            this.fofPermissionService = fofPermissionService;
            this.activatedRoute = activatedRoute;
            this.formBuilder = formBuilder;
            this.fofNotificationService = fofNotificationService;
            this.fofDialogService = fofDialogService;
            this.router = router;
            // All private variables
            this.priVar = {
                roleCode: undefined,
                permissionRoluUpdating: false
            };
            // All private functions
            this.privFunc = {
                roleLoad: function () {
                    _this.uiVar.loading = true;
                    rxjs.forkJoin(_this.fofPermissionService.permission.getAll(), _this.fofPermissionService.role.getWithPermissionByRoleCode(_this.priVar.roleCode)).subscribe({
                        next: function (result) {
                            var permissions = result[0];
                            var currentRoles = result[1];
                            var currentRole = undefined;
                            var _permissions = [];
                            if (currentRoles.length > 0) {
                                currentRole = result[1][0];
                            }
                            else {
                                _this.fofNotificationService.error("Cet r\u00F4le n'existe pas");
                                _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                                return;
                            }
                            _this.uiVar.role = currentRole;
                            _this.uiVar.form.patchValue(_this.uiVar.role);
                            var _permission;
                            var previousNameRoot = undefined;
                            permissions.forEach(function (permission) {
                                //split to upper case
                                var names = permission.code.split(/(?=[A-Z])/);
                                var nameRoot = names[0];
                                var found = false;
                                var rolePermission;
                                if (currentRole) {
                                    rolePermission = currentRole.rolePermissions.find(function (item) { return item.permissionId == permission.id; });
                                    if (rolePermission) {
                                        found = true;
                                    }
                                }
                                var permissionCode = permission.code.slice(nameRoot.length);
                                if (nameRoot === previousNameRoot) {
                                    _permission.children.push({
                                        id: permission.id,
                                        checked: found,
                                        rolePermission: rolePermission,
                                        code: permissionCode
                                    });
                                }
                                else {
                                    previousNameRoot = nameRoot;
                                    if (_permission) {
                                        _permissions.push(_permission);
                                    }
                                    _permission = {};
                                    _permission.code = nameRoot;
                                    _permission.children = [];
                                    _permission.children.push({
                                        id: permission.id,
                                        checked: found,
                                        rolePermission: rolePermission,
                                        code: permissionCode
                                    });
                                }
                            });
                            if (_permission) {
                                _permissions.push(_permission);
                            }
                            _this.uiVar.permissionGroups = _permissions;
                            _this.uiVar.loading = false;
                            // console.log('currentRole', currentRole)
                        },
                        complete: function () {
                        }
                    });
                }
            };
            // All variables shared with UI 
            this.uiVar = {
                title: 'Nouveau rôle',
                permissionGroups: undefined,
                loading: false,
                roleIsNew: false,
                role: undefined,
                form: this.formBuilder.group({
                    code: ['', [forms.Validators.required, forms.Validators.minLength(3)]],
                    description: ['', [forms.Validators.maxLength(200)]]
                })
            };
            // All actions shared with UI 
            this.uiAction = {
                roleSave: function () {
                    var roleToSave = _this.uiVar.form.value;
                    if (!_this.uiVar.form.valid) {
                        _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                        fofUtilsForm.validateAllFields(_this.uiVar.form);
                        return;
                    }
                    if (roleToSave.code.toLowerCase() == 'new') {
                        _this.fofNotificationService.error('Désolé, ce code est réservé');
                        return;
                    }
                    if (_this.uiVar.roleIsNew) {
                        _this.fofPermissionService.role.create(roleToSave)
                            .toPromise()
                            .then(function (newRole) {
                            _this.fofNotificationService.success('Rôle sauvé', { mustDisappearAfter: 1000 });
                            _this.priVar.roleCode = newRole.code;
                            _this.uiVar.title = 'Modification de rôle';
                            _this.uiVar.roleIsNew = false;
                            _this.privFunc.roleLoad();
                        });
                    }
                    else {
                        roleToSave.id = _this.uiVar.role.id;
                        _this.fofPermissionService.role.update(roleToSave)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Rôle sauvé', { mustDisappearAfter: 1000 });
                        });
                    }
                },
                roleCancel: function () {
                    _this.uiVar.permissionGroups = null;
                    _this.privFunc.roleLoad();
                },
                roleDelete: function () {
                    _this.fofDialogService.openYesNo({
                        question: 'Voulez vous vraiment supprimer le rôle ?'
                    }).then(function (yes) {
                        if (yes) {
                            _this.fofPermissionService.role.delete(_this.uiVar.role)
                                .toPromise()
                                .then(function (result) {
                                _this.fofNotificationService.success('Rôle supprimé');
                                _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                            });
                        }
                    });
                },
                rolePermissionSave: function (permission) {
                    var rolePermission = permission.rolePermission;
                    // The rolePermission must be deleted
                    if (rolePermission) {
                        _this.fofPermissionService.rolePermission.delete(rolePermission)
                            .toPromise()
                            .then(function (result) {
                            permission.rolePermission = null;
                            _this.fofNotificationService.saveIsDone();
                        });
                    }
                    else {
                        // The rolePermission must be created
                        var rolePermissionToSave = {
                            permissionId: permission.id,
                            roleId: _this.uiVar.role.id
                        };
                        _this.fofPermissionService.rolePermission.create(rolePermissionToSave)
                            .toPromise()
                            .then(function (result) {
                            _this.priVar.permissionRoluUpdating = false;
                            permission.rolePermission = result;
                            _this.fofNotificationService.saveIsDone();
                        });
                    }
                }
            };
        }
        // Angular events
        FofRoleComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.activatedRoute.paramMap.subscribe(function (params) {
                var code = params.get('code');
                _this.priVar.roleCode = code;
                if (code) {
                    if (code.toLowerCase() == 'new') {
                        _this.uiVar.roleIsNew = true;
                    }
                    else {
                        _this.uiVar.title = 'Modification de rôle';
                        _this.privFunc.roleLoad();
                    }
                }
            });
        };
        FofRoleComponent.ɵfac = function FofRoleComponent_Factory(t) { return new (t || FofRoleComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](router.ActivatedRoute), core["ɵɵdirectiveInject"](forms.FormBuilder), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](FofDialogService), core["ɵɵdirectiveInject"](router.Router)); };
        FofRoleComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofRoleComponent, selectors: [["fof-role"]], decls: 15, vars: 5, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "fof-fade-in detail", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], ["class", "group-card fof-fade-in", 4, "ngFor", "ngForOf"], [3, "entityBase"], [1, "fof-fade-in", "detail"], [3, "formGroup"], ["matInput", "", "required", "", "formControlName", "code", "placeholder", "Code du r\u00F4le", "value", ""], [4, "ngIf"], ["matInput", "", "formControlName", "description", "rows", "3", "placeholder", "Description"], ["align", "end"], [1, "fof-loading"], ["diameter", "20"], [1, "group-card", "fof-fade-in"], ["loading", ""], [4, "ngFor", "ngForOf"], [3, "ngModel", "checked", "change", "ngModelChange"], ["diameter", "30"]], template: function FofRoleComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-card", 0);
                core["ɵɵelementStart"](1, "h3");
                core["ɵɵtext"](2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "div", 1);
                core["ɵɵelementStart"](4, "button", 2);
                core["ɵɵlistener"]("click", function FofRoleComponent_Template_button_click_4_listener() { return ctx.uiAction.roleCancel(); });
                core["ɵɵtext"](5, "Annuler");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "button", 3);
                core["ɵɵlistener"]("click", function FofRoleComponent_Template_button_click_6_listener() { return ctx.uiAction.roleDelete(); });
                core["ɵɵtext"](7, "Supprimer");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](8, "button", 4);
                core["ɵɵlistener"]("click", function FofRoleComponent_Template_button_click_8_listener() { return ctx.uiAction.roleSave(); });
                core["ɵɵtext"](9, " Enregister");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](10, "div", 5);
                core["ɵɵtemplate"](11, FofRoleComponent_div_11_Template, 12, 4, "div", 6);
                core["ɵɵtemplate"](12, FofRoleComponent_div_12_Template, 4, 0, "div", 7);
                core["ɵɵtemplate"](13, FofRoleComponent_mat_card_13_Template, 7, 2, "mat-card", 8);
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](14, "fof-entity-footer", 9);
            } if (rf & 2) {
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.uiVar.title);
                core["ɵɵadvance"](9);
                core["ɵɵproperty"]("ngIf", !ctx.uiVar.loading);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.loading);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngForOf", ctx.uiVar.permissionGroups);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("entityBase", ctx.uiVar.role);
            } }, directives: [card.MatCard, button.MatButton, common.NgIf, common.NgForOf, FofEntityFooterComponent, forms["ɵangular_packages_forms_forms_y"], forms.NgControlStatusGroup, forms.FormGroupDirective, formField.MatFormField, input.MatInput, forms.DefaultValueAccessor, forms.RequiredValidator, forms.NgControlStatus, forms.FormControlName, formField.MatHint, formField.MatError, progressSpinner.MatSpinner, card.MatCardHeader, card.MatCardTitle, card.MatCardContent, checkbox.MatCheckbox, forms.NgModel], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{margin:15px 15px 15px 0;width:18em}.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]   mat-card-title[_ngcontent-%COMP%]{text-transform:uppercase}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
        return FofRoleComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofRoleComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-role',
                    templateUrl: './fof-role.component.html',
                    styleUrls: ['./fof-role.component.scss'],
                }]
        }], function () { return [{ type: FofPermissionService }, { type: router.ActivatedRoute }, { type: forms.FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: router.Router }]; }, null); })();

    var FofErrorService = /** @class */ (function () {
        function FofErrorService(fofNotificationService) {
            this.fofNotificationService = fofNotificationService;
        }
        /** Only for the core service, the error should be already cleaned */
        FofErrorService.prototype.cleanError = function (errorToManage) {
            var errorToReturn = errorToManage;
            var message;
            var isConstraintError = false;
            // console.log('errorToManage', errorToManage)
            if (errorToManage.error instanceof ErrorEvent) {
                // client-side error
                console.log('toDo: manage error');
                console.log('errorToManage', errorToManage);
                return errorToManage;
            }
            else {
                // HTTP or server-side error
                if (errorToManage.error) {
                    if (errorToManage.error instanceof Object) {
                        if (errorToManage.error.fofCoreException) {
                            if (errorToManage.error.message instanceof Object) {
                                // it's an http rest error
                                var _error = errorToManage.error.message;
                                if (_error.error) {
                                    errorToManage.error.statusText = _error.error;
                                    errorToManage.error.message = _error.message;
                                }
                            }
                            else {
                                message = errorToManage.error.message;
                                if (message.search('constraint') > -1) {
                                    isConstraintError = true;
                                }
                            }
                            return __assign({ isConstraintError: isConstraintError }, errorToManage.error);
                        }
                        else {
                            if (errorToManage.status == 0) {
                                console.error('FOF-UNKNOWN-ERROR', errorToManage.error);
                                errorToManage.error = errorToManage.message;
                                errorToManage.message = 'Impossible to contact the server';
                            }
                        }
                    }
                    else {
                        // the error to manage is comming from the httpClient
                        return errorToManage;
                    }
                }
            }
            return errorToReturn;
        };
        FofErrorService.prototype.errorManage = function (errorToManage) {
            var message = errorToManage.message;
            if (errorToManage.error) {
                message = message + ("<br>" + errorToManage.error);
            }
            if (errorToManage.status) {
                switch (errorToManage.status) {
                    case 401:
                    case 403:
                        message = "Vous n'avez pas le droit d'acc\u00E9der \u00E0 ces ressources";
                        break;
                    default:
                        message = "Oups, nous avons une erreur<br>\n          <small>" + message + "</small><br>";
                        break;
                }
            }
            this.fofNotificationService.error(message, { mustDisappearAfter: -1 });
        };
        FofErrorService.ɵfac = function FofErrorService_Factory(t) { return new (t || FofErrorService)(core["ɵɵinject"](FofNotificationService)); };
        FofErrorService.ɵprov = core["ɵɵdefineInjectable"]({ token: FofErrorService, factory: FofErrorService.ɵfac, providedIn: 'root' });
        return FofErrorService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofErrorService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: FofNotificationService }]; }, null); })();

    function FofUsersComponent_div_21_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 21);
        core["ɵɵelement"](1, "mat-spinner", 22);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des utilisateurs...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function FofUsersComponent_th_24_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Email");
        core["ɵɵelementEnd"]();
    } }
    function FofUsersComponent_td_25_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r232 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r232.email);
    } }
    function FofUsersComponent_th_27_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Login");
        core["ɵɵelementEnd"]();
    } }
    function FofUsersComponent_td_28_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r233 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r233.login);
    } }
    function FofUsersComponent_th_30_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Pr\u00E9nom");
        core["ɵɵelementEnd"]();
    } }
    function FofUsersComponent_td_31_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r234 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r234.firstName);
    } }
    function FofUsersComponent_th_33_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Nom");
        core["ɵɵelementEnd"]();
    } }
    function FofUsersComponent_td_34_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r235 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r235.lastName);
    } }
    function FofUsersComponent_tr_35_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 25);
    } }
    function FofUsersComponent_tr_36_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 26);
    } if (rf & 2) {
        var row_r236 = ctx.$implicit;
        core["ɵɵproperty"]("routerLink", row_r236.id);
    } }
    var _c0$1 = function () { return [5, 10, 25, 100]; };
    var FofUsersComponent = /** @class */ (function () {
        function FofUsersComponent(fofPermissionService, fofNotificationService, breakpointObserver, fofErrorService) {
            var _this = this;
            this.fofPermissionService = fofPermissionService;
            this.fofNotificationService = fofNotificationService;
            this.breakpointObserver = breakpointObserver;
            this.fofErrorService = fofErrorService;
            // All private variables
            this.priVar = {
                breakpointObserverSub: undefined,
                filter: undefined,
                filterOrganizationsChange: new core.EventEmitter(),
                filterOrganizations: undefined
            };
            // All private functions
            this.privFunc = {};
            // All variables shared with UI 
            this.uiVar = {
                displayedColumns: [],
                data: [],
                resultsLength: 0,
                pageSize: 5,
                isLoadingResults: true,
                searchUsersCtrl: new forms.FormControl()
            };
            // All actions shared with UI 
            this.uiAction = {
                organisationMultiSelectedChange: function (organization) {
                    if (organization) {
                        _this.priVar.filterOrganizations = [organization.id];
                    }
                    else {
                        _this.priVar.filterOrganizations = null;
                    }
                    _this.priVar.filterOrganizationsChange.emit(organization);
                }
            };
        }
        // Angular events
        FofUsersComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.priVar.filter = this.uiVar.searchUsersCtrl.valueChanges
                .pipe(operators.debounceTime(500), operators.distinctUntilChanged(), operators.filter(function (query) { return query.length >= 3 || query.length === 0; }));
            this.priVar.breakpointObserverSub = this.breakpointObserver.observe(layout.Breakpoints.XSmall)
                .subscribe(function (state) {
                if (state.matches) {
                    // XSmall
                    _this.uiVar.displayedColumns = ['email', 'login'];
                }
                else {
                    // > XSmall
                    _this.uiVar.displayedColumns = ['email', 'login', 'firstName', 'lastName'];
                }
            });
        };
        FofUsersComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            // If the user changes the sort order, reset back to the first page.
            this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
            rxjs.merge(this.sort.sortChange, this.paginator.page, this.priVar.filter, this.priVar.filterOrganizationsChange)
                .pipe(operators.startWith({}), operators.switchMap(function () {
                _this.uiVar.isLoadingResults = true;
                _this.uiVar.pageSize = _this.paginator.pageSize;
                return _this.fofPermissionService.user.search(_this.uiVar.searchUsersCtrl.value, _this.priVar.filterOrganizations, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
            }), operators.map(function (search) {
                _this.uiVar.isLoadingResults = false;
                _this.uiVar.resultsLength = search.total;
                return search.data;
            }), operators.catchError(function (error) {
                _this.fofErrorService.errorManage(error);
                _this.uiVar.isLoadingResults = false;
                return rxjs.of([]);
            })).subscribe(function (data) {
                _this.uiVar.data = data;
            });
        };
        FofUsersComponent.prototype.ngOnDestroy = function () {
            if (this.priVar.breakpointObserverSub) {
                this.priVar.breakpointObserverSub.unsubscribe();
            }
        };
        FofUsersComponent.ɵfac = function FofUsersComponent_Factory(t) { return new (t || FofUsersComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](layout.BreakpointObserver), core["ɵɵdirectiveInject"](FofErrorService)); };
        FofUsersComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofUsersComponent, selectors: [["fof-core-fof-users"]], viewQuery: function FofUsersComponent_Query(rf, ctx) { if (rf & 1) {
                core["ɵɵviewQuery"](paginator.MatPaginator, true);
                core["ɵɵviewQuery"](sort.MatSort, true);
            } if (rf & 2) {
                var _t;
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.paginator = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.sort = _t.first);
            } }, decls: 38, vars: 17, consts: [[1, "row"], [1, "col-md-3"], [1, "card-tree"], [3, "multiSelect", "selectedOrganizationsChange"], [1, "col-md-9"], [1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "filtres"], ["autofocus", "", "matInput", "", "placeholder", "email ou nom ou pr\u00E9nom ou login", 3, "formControl"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "email", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "email"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "login"], ["matColumnDef", "firstName"], ["matColumnDef", "lastName"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function FofUsersComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div", 0);
                core["ɵɵelementStart"](1, "div", 1);
                core["ɵɵelementStart"](2, "mat-card", 2);
                core["ɵɵelementStart"](3, "fof-organizations-tree", 3);
                core["ɵɵlistener"]("selectedOrganizationsChange", function FofUsersComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](4, "div", 4);
                core["ɵɵelementStart"](5, "mat-card", 5);
                core["ɵɵelementStart"](6, "h3");
                core["ɵɵtext"](7);
                core["ɵɵpipe"](8, "translate");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](9, "a", 6);
                core["ɵɵelementStart"](10, "span");
                core["ɵɵtext"](11);
                core["ɵɵpipe"](12, "translate");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](13, "mat-card", 7);
                core["ɵɵelementStart"](14, "mat-form-field");
                core["ɵɵelementStart"](15, "mat-label");
                core["ɵɵtext"](16, "Filtre");
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](17, "input", 8);
                core["ɵɵelementStart"](18, "mat-hint");
                core["ɵɵtext"](19, "Recherche \u00E0 partir de 3 caract\u00E8res saisies");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](20, "div", 9);
                core["ɵɵtemplate"](21, FofUsersComponent_div_21_Template, 4, 0, "div", 10);
                core["ɵɵelementStart"](22, "table", 11);
                core["ɵɵelementContainerStart"](23, 12);
                core["ɵɵtemplate"](24, FofUsersComponent_th_24_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](25, FofUsersComponent_td_25_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](26, 15);
                core["ɵɵtemplate"](27, FofUsersComponent_th_27_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](28, FofUsersComponent_td_28_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](29, 16);
                core["ɵɵtemplate"](30, FofUsersComponent_th_30_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](31, FofUsersComponent_td_31_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](32, 17);
                core["ɵɵtemplate"](33, FofUsersComponent_th_33_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](34, FofUsersComponent_td_34_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵtemplate"](35, FofUsersComponent_tr_35_Template, 1, 0, "tr", 18);
                core["ɵɵtemplate"](36, FofUsersComponent_tr_36_Template, 1, 1, "tr", 19);
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](37, "mat-paginator", 20);
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](3);
                core["ɵɵproperty"]("multiSelect", false);
                core["ɵɵadvance"](4);
                core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](8, 12, "admin.users.title-main"));
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("routerLink", "/admin/users/new");
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](core["ɵɵpipeBind1"](12, 14, "admin.users.btn-user-add"));
                core["ɵɵadvance"](6);
                core["ɵɵproperty"]("formControl", ctx.uiVar.searchUsersCtrl);
                core["ɵɵadvance"](4);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.isLoadingResults);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("dataSource", ctx.uiVar.data);
                core["ɵɵadvance"](13);
                core["ɵɵproperty"]("matHeaderRowDef", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("matRowDefColumns", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("length", ctx.uiVar.resultsLength)("pageSizeOptions", core["ɵɵpureFunction0"](16, _c0$1))("pageSize", ctx.uiVar.pageSize);
            } }, directives: [card.MatCard, FofOrganizationsTreeComponent, button.MatAnchor, router.RouterLinkWithHref, formField.MatFormField, formField.MatLabel, input.MatInput, forms.DefaultValueAccessor, forms.NgControlStatus, forms.FormControlDirective, formField.MatHint, common.NgIf, table.MatTable, sort.MatSort, table.MatColumnDef, table.MatHeaderCellDef, table.MatCellDef, table.MatHeaderRowDef, table.MatRowDef, paginator.MatPaginator, progressSpinner.MatSpinner, table.MatHeaderCell, sort.MatSortHeader, table.MatCell, table.MatHeaderRow, table.MatRow, router.RouterLink], pipes: [core$1.TranslatePipe], styles: [".filtres[_ngcontent-%COMP%]{margin-bottom:15px}.filtres[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.card-tree[_ngcontent-%COMP%]{height:100%}"] });
        return FofUsersComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofUsersComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-fof-users',
                    templateUrl: './fof-users.component.html',
                    styleUrls: ['./fof-users.component.scss']
                }]
        }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: layout.BreakpointObserver }, { type: FofErrorService }]; }, { paginator: [{
                type: core.ViewChild,
                args: [paginator.MatPaginator]
            }], sort: [{
                type: core.ViewChild,
                args: [sort.MatSort]
            }] }); })();

    function FofUserRolesSelectComponent_div_0_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 6);
        core["ɵɵelement"](1, "mat-spinner", 7);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Rafraichissement des r\u00F4les...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function FofUserRolesSelectComponent_div_7_Template(rf, ctx) { if (rf & 1) {
        var _r241 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div", 8);
        core["ɵɵelementStart"](1, "mat-checkbox", 9);
        core["ɵɵlistener"]("change", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_change_1_listener() { core["ɵɵrestoreView"](_r241); var role_r239 = ctx.$implicit; var ctx_r240 = core["ɵɵnextContext"](); return ctx_r240.uiAction.userRoleOrganizationSave(role_r239); })("ngModelChange", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_ngModelChange_1_listener($event) { core["ɵɵrestoreView"](_r241); var role_r239 = ctx.$implicit; return role_r239.checked = $event; });
        core["ɵɵtext"](2);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](3, "div", 10);
        core["ɵɵtext"](4);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var role_r239 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngModel", role_r239.checked)("checked", role_r239.checked);
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](role_r239.code);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](role_r239.description);
    } }
    var FofUserRolesSelectComponent = /** @class */ (function () {
        function FofUserRolesSelectComponent(matDialogRef, fofPermissionService, fofNotificationService, fofErrorService, data) {
            var _this = this;
            this.matDialogRef = matDialogRef;
            this.fofPermissionService = fofPermissionService;
            this.fofNotificationService = fofNotificationService;
            this.fofErrorService = fofErrorService;
            this.userRoleOrganizations = undefined;
            // All private variables
            this.priVar = {
                userId: undefined,
                organizationId: undefined
            };
            // All private functions
            this.privFunc = {
                componenentDataRefresh: function () {
                    if (_this.userRoleOrganizations) {
                        // it's an update
                        _this.uiVar.isCreation = false;
                        _this.uiVar.title = "Modification des acc\u00E8s";
                        // Only one organization possible in update
                        _this.uiVar.organizationMultipleSelect = false;
                        if (_this.userRoleOrganizations.length > 0) {
                            // we get only the first one since it's impossible to have mutiple organisations
                            _this.priVar.organizationId = _this.userRoleOrganizations[0].organization.id;
                            _this.uiVar.selectedOrganisations = [];
                            _this.uiVar.selectedOrganisations.push(_this.userRoleOrganizations[0].organization);
                        }
                        if (_this.uiVar.allRoles) {
                            _this.uiVar.allRoles.forEach(function (role) {
                                var userRoleOrganization = _this.userRoleOrganizations.find(function (item) { return item.roleId == role.id; });
                                if (userRoleOrganization) {
                                    role.userRoleOrganization = userRoleOrganization;
                                    role.checked = true;
                                }
                            });
                        }
                    }
                    else {
                        // it's a creation
                        _this.uiVar.isCreation = true;
                        _this.uiVar.title = "Creation d'un acc\u00E8s";
                    }
                }
            };
            // All variables shared with UI 
            this.uiVar = {
                allRoles: undefined,
                organizationMultipleSelect: true,
                selectedOrganisations: [],
                title: "Creation d'un acc\u00E8s",
                isCreation: false,
                loading: false
            };
            // All actions shared with UI 
            this.uiAction = {
                organisationMultiSelectedChange: function (organisations) {
                    _this.uiVar.selectedOrganisations = organisations;
                },
                userRoleOrganizationSave: function (role) {
                    var userRoleOrganization = role.userRoleOrganization;
                    // if (userRoleOrganization) {
                    //   this.fofPermissionService.userRoleOrganization.delete(userRoleOrganization)
                    //   .toPromise()
                    //   .then((result: any) => {          
                    //     role.userRoleOrganization = null          
                    //     this.fofNotificationService.saveIsDone()
                    //   })      
                    // } else {
                    //   // The userRoleOrganization must be created
                    //   const userRoleOrganizationToSave: iUserRoleOrganization = {        
                    //     roleId: role.id,
                    //     userId: this.uiVar.user.id
                    //   }
                    //   this.fofPermissionService.userRoleOrganization.create(userRoleOrganizationToSave)
                    //   .toPromise()
                    //   .then((result: any) => {          
                    //     role.userRoleOrganization = result          
                    //     this.fofNotificationService.saveIsDone()
                    //   })      
                    // }
                },
                save: function () {
                    var userRoleOrganizations = [];
                    var userRoleOrganization;
                    if (!_this.priVar.userId) {
                        _this.fofNotificationService.error("\n          Probl\u00E8me avec les donn\u00E9es<br>\n          <small>Manque le user</small>\n        ");
                    }
                    _this.uiVar.selectedOrganisations.forEach(function (organisation) {
                        _this.uiVar.allRoles.forEach(function (role) {
                            if (role.checked) {
                                userRoleOrganization = {
                                    userId: _this.priVar.userId,
                                    organizationId: organisation.id,
                                    roleId: role.id
                                };
                                userRoleOrganizations.push(userRoleOrganization);
                            }
                        });
                    });
                    if (userRoleOrganizations.length === 0) {
                        _this.fofNotificationService.error("\n          Vous devez s\u00E9lectionner au moins<br>\n          <ul>\n            <li>Une organisation</li>\n            <li>Un role</li>\n          </ul>\n        ", { mustDisappearAfter: -1 });
                        return;
                    }
                    _this.uiVar.loading = true;
                    if (_this.uiVar.isCreation) {
                        _this.fofPermissionService.userRoleOrganization.bulkCreate(userRoleOrganizations)
                            .toPromise()
                            .then(function (result) {
                            _this.matDialogRef.close({ saved: true });
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                            // this.fofNotificationService.error(`
                            //   Erreur lors de la sauvegarde<br>
                            //   <small>${reason}</small>
                            // `)
                        })
                            .finally(function () {
                            _this.uiVar.loading = false;
                        });
                    }
                    else {
                        _this.fofPermissionService.user
                            .replaceUserRoleOrganization(userRoleOrganizations, _this.priVar.userId, _this.priVar.organizationId)
                            .toPromise()
                            .then(function (result) {
                            _this.matDialogRef.close({ saved: true });
                        })
                            .catch(function (reason) {
                            _this.fofNotificationService.error("\n            Erreur lors de la sauvegarde<br>\n            <small>" + reason + "</small>\n          ");
                        })
                            .finally(function () {
                            _this.uiVar.loading = false;
                        });
                    }
                }
            };
            this.userRoleOrganizations = data.userRoleOrganizations;
            this.uiVar.allRoles = data.roles;
            this.priVar.userId = data.userId;
            if (data.notSelectableOrganizations) {
                this.notSelectableOrganizations = data.notSelectableOrganizations;
            }
            if (this.uiVar.allRoles && this.uiVar.allRoles.length) {
                this.uiVar.allRoles.forEach(function (role) {
                    role.checked = false;
                });
            }
            this.privFunc.componenentDataRefresh();
        }
        // Angular events
        FofUserRolesSelectComponent.prototype.ngOnInit = function () {
        };
        FofUserRolesSelectComponent.prototype.ngOnChanges = function () {
            this.privFunc.componenentDataRefresh();
        };
        FofUserRolesSelectComponent.ɵfac = function FofUserRolesSelectComponent_Factory(t) { return new (t || FofUserRolesSelectComponent)(core["ɵɵdirectiveInject"](dialog.MatDialogRef), core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](FofErrorService), core["ɵɵdirectiveInject"](dialog.MAT_DIALOG_DATA)); };
        FofUserRolesSelectComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofUserRolesSelectComponent, selectors: [["fof-user-roles-select"]], inputs: { userRoleOrganizations: "userRoleOrganizations", notSelectableOrganizations: "notSelectableOrganizations" }, features: [core["ɵɵNgOnChangesFeature"]], decls: 13, vars: 6, consts: [["class", "fof-loading", 4, "ngIf"], ["mat-dialog-title", ""], [3, "multiSelect", "selectedOrganisations", "notSelectableOrganizations", "selectedOrganizationsChange"], ["class", "fof-fade-in", 4, "ngFor", "ngForOf"], ["mat-flat-button", "", "mat-dialog-close", ""], ["mat-flat-button", "", "color", "primary", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in"], [3, "ngModel", "checked", "change", "ngModelChange"], [1, "role-hint"]], template: function FofUserRolesSelectComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵtemplate"](0, FofUserRolesSelectComponent_div_0_Template, 4, 0, "div", 0);
                core["ɵɵelementStart"](1, "h2", 1);
                core["ɵɵtext"](2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "mat-dialog-content");
                core["ɵɵelementStart"](4, "fof-core-fof-organizations-multi-select", 2);
                core["ɵɵlistener"]("selectedOrganizationsChange", function FofUserRolesSelectComponent_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_4_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](5, "h4");
                core["ɵɵtext"](6, "R\u00F4les");
                core["ɵɵelementEnd"]();
                core["ɵɵtemplate"](7, FofUserRolesSelectComponent_div_7_Template, 5, 4, "div", 3);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](8, "mat-dialog-actions");
                core["ɵɵelementStart"](9, "button", 4);
                core["ɵɵtext"](10, "Annuler");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](11, "button", 5);
                core["ɵɵlistener"]("click", function FofUserRolesSelectComponent_Template_button_click_11_listener() { return ctx.uiAction.save(); });
                core["ɵɵtext"](12, "Enregistrer");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵproperty"]("ngIf", ctx.uiVar.loading);
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.uiVar.title);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("multiSelect", ctx.uiVar.organizationMultipleSelect)("selectedOrganisations", ctx.uiVar.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
                core["ɵɵadvance"](3);
                core["ɵɵproperty"]("ngForOf", ctx.uiVar.allRoles);
            } }, directives: [common.NgIf, dialog.MatDialogTitle, dialog.MatDialogContent, FofOrganizationsMultiSelectComponent, common.NgForOf, dialog.MatDialogActions, button.MatButton, dialog.MatDialogClose, progressSpinner.MatSpinner, checkbox.MatCheckbox, forms.NgControlStatus, forms.NgModel], styles: ["[_nghost-%COMP%]{position:relative}[_nghost-%COMP%]   .fof-loading[_ngcontent-%COMP%]{position:absolute;height:calc(100% + 15px);z-index:2}[_nghost-%COMP%]   .mat-dialog-actions[_ngcontent-%COMP%]{margin-top:1rem;display:flex;justify-content:flex-end}"] });
        return FofUserRolesSelectComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofUserRolesSelectComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-user-roles-select',
                    templateUrl: './fof-user-roles-select.component.html',
                    styleUrls: ['./fof-user-roles-select.component.scss']
                }]
        }], function () { return [{ type: dialog.MatDialogRef }, { type: FofPermissionService }, { type: FofNotificationService }, { type: FofErrorService }, { type: undefined, decorators: [{
                    type: core.Inject,
                    args: [dialog.MAT_DIALOG_DATA]
                }] }]; }, { userRoleOrganizations: [{
                type: core.Input
            }], notSelectableOrganizations: [{
                type: core.Input
            }] }); })();

    function FofUserComponent_div_11_mat_error_6_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le login ne doit pas exc\u00E9der 30 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_mat_error_9_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Un email valide est obligatoire ");
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_mat_error_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le pr\u00E9nom ne doit pas exc\u00E9der 30 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_mat_error_15_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le nom de famille ne doit pas exc\u00E9der 30 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_div_17_div_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 33);
        core["ɵɵelement"](1, "mat-spinner", 34);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Rafraichissement des r\u00F4les...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_div_17_th_14_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "th", 35);
    } }
    function FofUserComponent_div_11_div_17_td_15_Template(rf, ctx) { if (rf & 1) {
        var _r263 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "td", 36);
        core["ɵɵelementStart"](1, "mat-checkbox", 37);
        core["ɵɵlistener"]("ngModelChange", function FofUserComponent_div_11_div_17_td_15_Template_mat_checkbox_ngModelChange_1_listener($event) { core["ɵɵrestoreView"](_r263); var row_r261 = ctx.$implicit; return row_r261.checked = $event; });
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r261 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngModel", row_r261.checked);
    } }
    function FofUserComponent_div_11_div_17_th_17_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 35);
        core["ɵɵtext"](1, "Organisation");
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_div_17_td_18_Template(rf, ctx) { if (rf & 1) {
        var _r266 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "td", 38);
        core["ɵɵlistener"]("click", function FofUserComponent_div_11_div_17_td_18_Template_td_click_0_listener() { core["ɵɵrestoreView"](_r266); var row_r264 = ctx.$implicit; var ctx_r265 = core["ɵɵnextContext"](3); return ctx_r265.uiAction.roleSelectComponentOpen(row_r264); });
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r264 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", row_r264.organization, "");
    } }
    function FofUserComponent_div_11_div_17_th_20_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 35);
        core["ɵɵtext"](1, "Roles");
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_div_17_td_21_div_1_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div");
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var role_r269 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"](" ", role_r269, " ");
    } }
    function FofUserComponent_div_11_div_17_td_21_Template(rf, ctx) { if (rf & 1) {
        var _r271 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "td", 38);
        core["ɵɵlistener"]("click", function FofUserComponent_div_11_div_17_td_21_Template_td_click_0_listener() { core["ɵɵrestoreView"](_r271); var row_r267 = ctx.$implicit; var ctx_r270 = core["ɵɵnextContext"](3); return ctx_r270.uiAction.roleSelectComponentOpen(row_r267); });
        core["ɵɵtemplate"](1, FofUserComponent_div_11_div_17_td_21_div_1_Template, 2, 1, "div", 39);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r267 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngForOf", row_r267.roles);
    } }
    function FofUserComponent_div_11_div_17_th_23_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "th", 35);
    } }
    function FofUserComponent_div_11_div_17_td_24_Template(rf, ctx) { if (rf & 1) {
        var _r274 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "td", 40);
        core["ɵɵlistener"]("click", function FofUserComponent_div_11_div_17_td_24_Template_td_click_0_listener() { core["ɵɵrestoreView"](_r274); var row_r272 = ctx.$implicit; var ctx_r273 = core["ɵɵnextContext"](3); return ctx_r273.uiAction.roleSelectComponentOpen(row_r272); });
        core["ɵɵelementStart"](1, "mat-icon");
        core["ɵɵtext"](2, "chevron_right");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function FofUserComponent_div_11_div_17_tr_25_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 41);
    } }
    function FofUserComponent_div_11_div_17_tr_26_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 42);
    } }
    function FofUserComponent_div_11_div_17_Template(rf, ctx) { if (rf & 1) {
        var _r277 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div", 11);
        core["ɵɵelementStart"](1, "mat-card");
        core["ɵɵelementStart"](2, "div", 0);
        core["ɵɵelementStart"](3, "h4");
        core["ɵɵtext"](4, "R\u00F4les");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](5, "div", 1);
        core["ɵɵelementStart"](6, "button", 3);
        core["ɵɵlistener"]("click", function FofUserComponent_div_11_div_17_Template_button_click_6_listener() { core["ɵɵrestoreView"](_r277); var ctx_r276 = core["ɵɵnextContext"](2); return ctx_r276.uiAction.userRolesDelete(); });
        core["ɵɵtext"](7, "Supprimer r\u00F4les");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](8, "button", 2);
        core["ɵɵlistener"]("click", function FofUserComponent_div_11_div_17_Template_button_click_8_listener() { core["ɵɵrestoreView"](_r277); var ctx_r278 = core["ɵɵnextContext"](2); return ctx_r278.uiAction.roleSelectComponentOpen(); });
        core["ɵɵtext"](9, "Ajouter un r\u00F4le");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](10, "div", 20);
        core["ɵɵtemplate"](11, FofUserComponent_div_11_div_17_div_11_Template, 4, 0, "div", 21);
        core["ɵɵelementStart"](12, "table", 22);
        core["ɵɵelementContainerStart"](13, 23);
        core["ɵɵtemplate"](14, FofUserComponent_div_11_div_17_th_14_Template, 1, 0, "th", 24);
        core["ɵɵtemplate"](15, FofUserComponent_div_11_div_17_td_15_Template, 2, 1, "td", 25);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵelementContainerStart"](16, 26);
        core["ɵɵtemplate"](17, FofUserComponent_div_11_div_17_th_17_Template, 2, 0, "th", 24);
        core["ɵɵtemplate"](18, FofUserComponent_div_11_div_17_td_18_Template, 2, 1, "td", 27);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵelementContainerStart"](19, 28);
        core["ɵɵtemplate"](20, FofUserComponent_div_11_div_17_th_20_Template, 2, 0, "th", 24);
        core["ɵɵtemplate"](21, FofUserComponent_div_11_div_17_td_21_Template, 2, 1, "td", 27);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵelementContainerStart"](22, 29);
        core["ɵɵtemplate"](23, FofUserComponent_div_11_div_17_th_23_Template, 1, 0, "th", 24);
        core["ɵɵtemplate"](24, FofUserComponent_div_11_div_17_td_24_Template, 3, 0, "td", 30);
        core["ɵɵelementContainerEnd"]();
        core["ɵɵtemplate"](25, FofUserComponent_div_11_div_17_tr_25_Template, 1, 0, "tr", 31);
        core["ɵɵtemplate"](26, FofUserComponent_div_11_div_17_tr_26_Template, 1, 0, "tr", 32);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r249 = core["ɵɵnextContext"](2);
        core["ɵɵadvance"](11);
        core["ɵɵproperty"]("ngIf", ctx_r249.uiVar.loadingRoles);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("dataSource", ctx_r249.uiVar.userRolesToDisplay);
        core["ɵɵadvance"](13);
        core["ɵɵproperty"]("matHeaderRowDef", ctx_r249.uiVar.roleDisplayedColumns);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("matRowDefColumns", ctx_r249.uiVar.roleDisplayedColumns);
    } }
    function FofUserComponent_div_11_Template(rf, ctx) { if (rf & 1) {
        var _r280 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div", 9);
        core["ɵɵelementStart"](1, "div", 10);
        core["ɵɵelementStart"](2, "div", 11);
        core["ɵɵelementStart"](3, "form", 12);
        core["ɵɵelementStart"](4, "mat-form-field");
        core["ɵɵelement"](5, "input", 13);
        core["ɵɵtemplate"](6, FofUserComponent_div_11_mat_error_6_Template, 2, 0, "mat-error", 14);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](7, "mat-form-field");
        core["ɵɵelement"](8, "input", 15);
        core["ɵɵtemplate"](9, FofUserComponent_div_11_mat_error_9_Template, 2, 0, "mat-error", 14);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](10, "mat-form-field");
        core["ɵɵelement"](11, "input", 16);
        core["ɵɵtemplate"](12, FofUserComponent_div_11_mat_error_12_Template, 2, 0, "mat-error", 14);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](13, "mat-form-field");
        core["ɵɵelement"](14, "input", 17);
        core["ɵɵtemplate"](15, FofUserComponent_div_11_mat_error_15_Template, 2, 0, "mat-error", 14);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](16, "fof-core-fof-organizations-multi-select", 18);
        core["ɵɵlistener"]("selectedOrganizationsChange", function FofUserComponent_div_11_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_16_listener($event) { core["ɵɵrestoreView"](_r280); var ctx_r279 = core["ɵɵnextContext"](); return ctx_r279.uiAction.organisationMultiSelectedChange($event); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtemplate"](17, FofUserComponent_div_11_div_17_Template, 27, 4, "div", 19);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r243 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("formGroup", ctx_r243.uiVar.form);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r243.uiVar.form.get("login").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r243.uiVar.form.get("email").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r243.uiVar.form.get("firstName").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r243.uiVar.form.get("lastName").invalid);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("placeHolder", "Organisation de rattachement")("multiSelect", false)("selectedOrganisations", ctx_r243.uiVar.userOrganizationsDisplay);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", ctx_r243.uiVar.allRoles);
    } }
    function FofUserComponent_div_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 43);
        core["ɵɵelement"](1, "mat-spinner", 34);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des utilisateurs et des authorisations...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    var FofUserComponent = /** @class */ (function () {
        function FofUserComponent(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, matDialog, fofErrorService) {
            var _this = this;
            this.fofPermissionService = fofPermissionService;
            this.activatedRoute = activatedRoute;
            this.formBuilder = formBuilder;
            this.fofNotificationService = fofNotificationService;
            this.fofDialogService = fofDialogService;
            this.router = router;
            this.matDialog = matDialog;
            this.fofErrorService = fofErrorService;
            // All private variables
            this.priVar = {
                userId: undefined,
                userOrganizationId: undefined,
                organizationAlreadyWithRoles: undefined
            };
            // All private functions
            this.privFunc = {
                userLoad: function () {
                    _this.uiVar.loadingUser = true;
                    Promise.all([
                        _this.fofPermissionService.role.getAll().toPromise(),
                        _this.fofPermissionService.user.getWithRoleById(_this.priVar.userId).toPromise()
                    ])
                        .then(function (result) {
                        var roles = result[0];
                        var currentUsers = result[1];
                        console.log('currentUsers', currentUsers);
                        _this.uiVar.allRoles = roles;
                        _this.privFunc.userRefresh(currentUsers);
                        // Here for preventing to refresh the user form when refreshing only the roles.
                        // toDo: improve
                        _this.uiVar.form.patchValue(_this.uiVar.user);
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                        _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                    })
                        .finally(function () {
                        _this.uiVar.loadingUser = false;
                    });
                },
                userRefresh: function (currentUser) {
                    if (!currentUser) {
                        _this.fofNotificationService.error("Ce utilisateur n'existe pas");
                        _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        return;
                    }
                    _this.uiVar.user = currentUser;
                    if (currentUser.organizationId) {
                        _this.uiVar.userOrganizationsDisplay = [{ id: currentUser.organizationId }];
                        _this.priVar.userOrganizationId = currentUser.organizationId;
                    }
                    var _previousOrganisationId = undefined;
                    var _roleToDisplay = undefined;
                    _this.uiVar.userRolesToDisplay = [];
                    if (currentUser.userRoleOrganizations && currentUser.userRoleOrganizations.length > 0) {
                        // we will display the user roles grouped by organization 
                        // with one on several roles on it
                        currentUser.userRoleOrganizations.forEach(function (uro) {
                            if (_previousOrganisationId !== uro.organization.id) {
                                // it's a new organisation, reinit
                                _previousOrganisationId = uro.organization.id;
                                if (_roleToDisplay) {
                                    _this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                                }
                                _roleToDisplay = {
                                    organizationId: uro.organization.id,
                                    organization: uro.organization.name,
                                    roles: [],
                                    userRoleOrganizations: []
                                };
                            }
                            _roleToDisplay.roles.push(uro.role.code);
                            _roleToDisplay.userRoleOrganizations.push(uro);
                        });
                    }
                    // add the last one
                    if (_roleToDisplay) {
                        _this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                    }
                    // we don't want the following organization could be selectebale in the 
                    // organization tree
                    _this.priVar.organizationAlreadyWithRoles = [];
                    _this.uiVar.userRolesToDisplay.forEach(function (org) {
                        _this.priVar.organizationAlreadyWithRoles.push({
                            id: org.organizationId,
                            name: org.organization
                        });
                    });
                },
                useRolesRefresh: function () {
                    _this.uiVar.loadingRoles = true;
                    _this.fofPermissionService.user.getWithRoleById(_this.priVar.userId)
                        .toPromise()
                        .then(function (usersResult) {
                        _this.privFunc.userRefresh(usersResult);
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                        // this.router.navigate(['../'], {relativeTo: this.activatedRoute})
                    })
                        .finally(function () {
                        _this.uiVar.loadingRoles = false;
                    });
                }
            };
            // All variables shared with UI 
            this.uiVar = {
                title: 'Nouvel utilisateur',
                loadingUser: false,
                loadingRoles: false,
                userIsNew: false,
                user: undefined,
                userOrganizationsDisplay: undefined,
                form: this.formBuilder.group({
                    login: ['', [forms.Validators.maxLength(30)]],
                    email: ['', [forms.Validators.required, forms.Validators.email, forms.Validators.maxLength(60)]],
                    firstName: ['', [forms.Validators.maxLength(30)]],
                    lastName: ['', [forms.Validators.maxLength(30)]]
                }),
                allRoles: undefined,
                selectedUserRoleOrganizations: undefined,
                userRolesToDisplay: [],
                roleDisplayedColumns: ['delete', 'organization', 'roles', 'icon'],
                rolesAll: undefined
            };
            // All actions shared with UI 
            this.uiAction = {
                userSave: function () {
                    var userToSave = _this.uiVar.form.value;
                    userToSave.organizationId = _this.priVar.userOrganizationId;
                    if (!_this.uiVar.form.valid) {
                        _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                        fofUtilsForm.validateAllFields(_this.uiVar.form);
                        return;
                    }
                    if (_this.uiVar.userIsNew) {
                        _this.fofPermissionService.user.create(userToSave)
                            .toPromise()
                            .then(function (newUser) {
                            _this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                            _this.priVar.userId = newUser.id;
                            _this.uiVar.title = "Modification d'un utilisateur";
                            _this.uiVar.userIsNew = false;
                            _this.privFunc.userLoad();
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                    else {
                        userToSave.id = _this.uiVar.user.id;
                        _this.fofPermissionService.user.update(userToSave)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                },
                userCancel: function () {
                    _this.privFunc.userLoad();
                },
                userDelete: function () {
                    _this.fofDialogService.openYesNo({
                        question: "Voulez vous vraiment supprimer l' utilisateur ?"
                    }).then(function (yes) {
                        if (yes) {
                            _this.fofPermissionService.user.delete(_this.uiVar.user)
                                .toPromise()
                                .then(function (result) {
                                _this.fofNotificationService.success('Utilisateur supprimé');
                                _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                            });
                        }
                    });
                },
                userRolesDelete: function () {
                    var userRoles = _this.uiVar.userRolesToDisplay;
                    var organizationsIdToDelete = [];
                    var somethingToDelete = false;
                    var message = "Voulez vous vraiment supprimer les r\u00F4les des organisations suivante ?<br>";
                    message = message + '<ul>';
                    userRoles.forEach(function (ur) {
                        //don't want to create an interface for 1 param)
                        if (ur['checked']) {
                            somethingToDelete = true;
                            message = message + ("<li>" + ur.organization + "</li>");
                            organizationsIdToDelete.push(ur.organizationId);
                        }
                    });
                    message = message + '</ul>';
                    if (somethingToDelete) {
                        _this.fofDialogService.openYesNo({
                            title: 'Supprimer rôles',
                            question: message
                        }).then(function (yes) {
                            if (yes) {
                                _this.fofPermissionService.user.deleteUserRoleOrganizations(_this.uiVar.user.id, organizationsIdToDelete)
                                    .toPromise()
                                    .then(function (result) {
                                    _this.fofNotificationService.success('Rôles supprimés');
                                    _this.privFunc.useRolesRefresh();
                                });
                            }
                        });
                    }
                    else {
                        _this.fofNotificationService.info('Vous devez sélectionner ua moins une organisation');
                    }
                },
                roleSelectComponentOpen: function (userRole) {
                    var userRoleOrganizations;
                    if (userRole) {
                        userRoleOrganizations = userRole.userRoleOrganizations;
                    }
                    var dialogRef = _this.matDialog.open(FofUserRolesSelectComponent, {
                        data: {
                            roles: _this.uiVar.allRoles,
                            userRoleOrganizations: userRoleOrganizations,
                            notSelectableOrganizations: _this.priVar.organizationAlreadyWithRoles,
                            userId: _this.uiVar.user.id
                        },
                        width: '600px',
                        height: 'calc(100vh - 200px)'
                    });
                    dialogRef.afterClosed()
                        .toPromise()
                        .then(function (mustBeRefresd) {
                        if (mustBeRefresd) {
                            _this.privFunc.useRolesRefresh();
                        }
                    });
                },
                organisationMultiSelectedChange: function (organizations) {
                    if (organizations && organizations.length > 0) {
                        _this.priVar.userOrganizationId = organizations[0].id;
                    }
                    else {
                        _this.priVar.userOrganizationId = null;
                    }
                }
            };
        }
        // Angular events
        FofUserComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.activatedRoute.paramMap.subscribe(function (params) {
                var code = params.get('code');
                if (code) {
                    if (code.toLowerCase() == 'new') {
                        _this.uiVar.userIsNew = true;
                    }
                    else {
                        _this.priVar.userId = code;
                        _this.uiVar.title = "Modification d'un utilisateur";
                        _this.privFunc.userLoad();
                    }
                }
            });
        };
        FofUserComponent.ɵfac = function FofUserComponent_Factory(t) { return new (t || FofUserComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](router.ActivatedRoute), core["ɵɵdirectiveInject"](forms.FormBuilder), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](FofDialogService), core["ɵɵdirectiveInject"](router.Router), core["ɵɵdirectiveInject"](dialog.MatDialog), core["ɵɵdirectiveInject"](FofErrorService)); };
        FofUserComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofUserComponent, selectors: [["fof-core-fof-user"]], decls: 14, vars: 4, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "detail fof-fade-in", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [3, "entityBase"], [1, "detail", "fof-fade-in"], [1, "row"], [1, "col-md-6"], [3, "formGroup"], ["matInput", "", "formControlName", "login", "placeholder", "login", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "type", "email", "formControlName", "email", "placeholder", "Email", "value", ""], ["matInput", "", "required", "", "type", "firstName", "formControlName", "firstName", "placeholder", "Pr\u00E9nom", "value", ""], ["matInput", "", "required", "", "type", "lastName", "formControlName", "lastName", "placeholder", "Nom de famille", "value", ""], [3, "placeHolder", "multiSelect", "selectedOrganisations", "selectedOrganizationsChange"], ["class", "col-md-6", 4, "ngIf"], [1, "fof-table-container"], ["class", "fof-loading fof-loading-roles", 4, "ngIf"], ["mat-table", "", 1, "data-table", 3, "dataSource"], ["matColumnDef", "delete"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "organization"], ["mat-cell", "", 3, "click", 4, "matCellDef"], ["matColumnDef", "roles"], ["matColumnDef", "icon"], ["class", "icon-select", "mat-cell", "", 3, "click", 4, "matCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 4, "matRowDef", "matRowDefColumns"], [1, "fof-loading", "fof-loading-roles"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], [3, "ngModel", "ngModelChange"], ["mat-cell", "", 3, "click"], [4, "ngFor", "ngForOf"], ["mat-cell", "", 1, "icon-select", 3, "click"], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over"], [1, "fof-loading"]], template: function FofUserComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-card", 0);
                core["ɵɵelementStart"](1, "h3");
                core["ɵɵtext"](2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "div", 1);
                core["ɵɵelementStart"](4, "button", 2);
                core["ɵɵlistener"]("click", function FofUserComponent_Template_button_click_4_listener() { return ctx.uiAction.userCancel(); });
                core["ɵɵtext"](5, "Annuler");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "button", 3);
                core["ɵɵlistener"]("click", function FofUserComponent_Template_button_click_6_listener() { return ctx.uiAction.userDelete(); });
                core["ɵɵtext"](7, "Supprimer");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](8, "button", 4);
                core["ɵɵlistener"]("click", function FofUserComponent_Template_button_click_8_listener() { return ctx.uiAction.userSave(); });
                core["ɵɵtext"](9, " Enregister");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](10, "div", 5);
                core["ɵɵtemplate"](11, FofUserComponent_div_11_Template, 18, 9, "div", 6);
                core["ɵɵtemplate"](12, FofUserComponent_div_12_Template, 4, 0, "div", 7);
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](13, "fof-entity-footer", 8);
            } if (rf & 2) {
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.uiVar.title);
                core["ɵɵadvance"](9);
                core["ɵɵproperty"]("ngIf", !ctx.uiVar.loadingUser);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.loadingUser);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("entityBase", ctx.uiVar.user);
            } }, directives: [card.MatCard, button.MatButton, common.NgIf, FofEntityFooterComponent, forms["ɵangular_packages_forms_forms_y"], forms.NgControlStatusGroup, forms.FormGroupDirective, formField.MatFormField, input.MatInput, forms.DefaultValueAccessor, forms.NgControlStatus, forms.FormControlName, forms.RequiredValidator, FofOrganizationsMultiSelectComponent, formField.MatError, table.MatTable, table.MatColumnDef, table.MatHeaderCellDef, table.MatCellDef, table.MatHeaderRowDef, table.MatRowDef, progressSpinner.MatSpinner, table.MatHeaderCell, table.MatCell, checkbox.MatCheckbox, forms.NgModel, common.NgForOf, icon.MatIcon, table.MatHeaderRow, table.MatRow], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row;margin-bottom:15px}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .fof-loading-roles[_ngcontent-%COMP%]{position:absolute}.main[_ngcontent-%COMP%]   .role-hint[_ngcontent-%COMP%]{padding-left:25px;font-size:smaller}.main[_ngcontent-%COMP%]   .icon-select[_ngcontent-%COMP%]{text-align:right}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
        return FofUserComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofUserComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-fof-user',
                    templateUrl: './fof-user.component.html',
                    styleUrls: ['./fof-user.component.scss']
                }]
        }], function () { return [{ type: FofPermissionService }, { type: router.ActivatedRoute }, { type: forms.FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: router.Router }, { type: dialog.MatDialog }, { type: FofErrorService }]; }, null); })();

    function FofOrganizationsComponent_button_9_Template(rf, ctx) { if (rf & 1) {
        var _r287 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 11);
        core["ɵɵlistener"]("click", function FofOrganizationsComponent_button_9_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r287); var ctx_r286 = core["ɵɵnextContext"](); return ctx_r286.uiAction.organisationCancel(); });
        core["ɵɵtext"](1, "Annuler");
        core["ɵɵelementEnd"]();
    } }
    function FofOrganizationsComponent_button_10_Template(rf, ctx) { if (rf & 1) {
        var _r289 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 12);
        core["ɵɵlistener"]("click", function FofOrganizationsComponent_button_10_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r289); var ctx_r288 = core["ɵɵnextContext"](); return ctx_r288.uiAction.organisationDelete(); });
        core["ɵɵtext"](1, "Supprimer");
        core["ɵɵelementEnd"]();
    } }
    function FofOrganizationsComponent_button_11_Template(rf, ctx) { if (rf & 1) {
        var _r291 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 13);
        core["ɵɵlistener"]("click", function FofOrganizationsComponent_button_11_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r291); var ctx_r290 = core["ɵɵnextContext"](); return ctx_r290.uiAction.organisationSave(); });
        core["ɵɵtext"](1, " Enregistrer");
        core["ɵɵelementEnd"]();
    } }
    function FofOrganizationsComponent_div_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 14);
        core["ɵɵelement"](1, "mat-spinner", 15);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des organisations...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function FofOrganizationsComponent_div_13_button_5_Template(rf, ctx) { if (rf & 1) {
        var _r295 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "button", 22);
        core["ɵɵlistener"]("click", function FofOrganizationsComponent_div_13_button_5_Template_button_click_0_listener() { core["ɵɵrestoreView"](_r295); var ctx_r294 = core["ɵɵnextContext"](2); return ctx_r294.uiAction.organisationAdd(); });
        core["ɵɵtext"](1, "Ajouter une organisation enfant");
        core["ɵɵelementEnd"]();
    } }
    function FofOrganizationsComponent_div_13_mat_error_9_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le nom ne doit pas \u00EAtre nul et faire plus de 100 cararct\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function FofOrganizationsComponent_div_13_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 16);
        core["ɵɵelementStart"](1, "div", 17);
        core["ɵɵelementStart"](2, "h4");
        core["ɵɵtext"](3);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](4, "div", 5);
        core["ɵɵtemplate"](5, FofOrganizationsComponent_div_13_button_5_Template, 2, 0, "button", 18);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](6, "form", 19);
        core["ɵɵelementStart"](7, "mat-form-field");
        core["ɵɵelement"](8, "input", 20);
        core["ɵɵtemplate"](9, FofOrganizationsComponent_div_13_mat_error_9_Template, 2, 0, "mat-error", 21);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r285 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](3);
        core["ɵɵtextInterpolate"](ctx_r285.uiVar.actionTitle);
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("ngIf", !ctx_r285.uiVar.organizationIsNew);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("formGroup", ctx_r285.uiVar.form);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r285.uiVar.form.get("name").invalid);
    } }
    var FofOrganizationsComponent = /** @class */ (function () {
        function FofOrganizationsComponent(fofPermissionService, formBuilder, fofNotificationService, fofDialogService, fofErrorService) {
            var _this = this;
            this.fofPermissionService = fofPermissionService;
            this.formBuilder = formBuilder;
            this.fofNotificationService = fofNotificationService;
            this.fofDialogService = fofDialogService;
            this.fofErrorService = fofErrorService;
            // All private variables
            this.priVar = {
                currentNode: undefined
            };
            // All private functions
            this.privFunc = {};
            // All variables shared with UI 
            this.uiVar = {
                loading: false,
                organizationIsNew: false,
                nodeForm: false,
                form: this.formBuilder.group({
                    name: ['', [forms.Validators.required, forms.Validators.maxLength(100)]],
                    description: ['', [forms.Validators.maxLength(200)]],
                }),
                actionTitle: "Modification de l'organisation",
                mainTitle: "G\u00E9rer l'organisation",
                nodeChanged: undefined,
                nodeToDelete: undefined
            };
            // All actions shared with UI 
            this.uiAction = {
                organisationAdd: function () {
                    if (!_this.priVar.currentNode) {
                        return;
                    }
                    _this.uiVar.form.reset();
                    _this.uiVar.organizationIsNew = true;
                    _this.uiVar.actionTitle = "Ajouter une organisation enfant";
                },
                organisationDelete: function () {
                    var nodeToDelete = _this.priVar.currentNode;
                    _this.fofDialogService.openYesNo({
                        title: "Supprimer une organisation",
                        question: "Voulez vous vraiment supprimer \n          " + nodeToDelete.name + " ?"
                    }).then(function (yes) {
                        if (yes) {
                            _this.fofPermissionService.organization.delete(nodeToDelete)
                                .toPromise()
                                .then(function (result) {
                                _this.uiVar.nodeToDelete = nodeToDelete;
                            })
                                .catch(function (reason) {
                                if (reason.isConstraintError) {
                                    _this.fofNotificationService.info("Vous ne pouvez pas supprimer l'organisation !<br>\n                <small>Vous devez supprimer tous les objets rattach\u00E9s \u00E0 l'organisation ou contacter un admninistrateur</small>", { mustDisappearAfter: -1 });
                                }
                                else {
                                    _this.fofErrorService.errorManage(reason);
                                }
                            });
                        }
                    });
                    _this.uiVar.organizationIsNew = false;
                },
                organisationSave: function () {
                    var nodeToSave = _this.uiVar.form.value;
                    if (!_this.uiVar.form.valid) {
                        _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                        fofUtilsForm.validateAllFields(_this.uiVar.form);
                        return;
                    }
                    // New organization
                    if (_this.uiVar.organizationIsNew) {
                        nodeToSave.parentId = _this.priVar.currentNode.id;
                        _this.fofPermissionService.organization.create({
                            id: nodeToSave.id,
                            name: nodeToSave.name
                        })
                            .toPromise()
                            .then(function (nodeResult) {
                            _this.fofNotificationService.saveIsDone();
                            _this.priVar.currentNode.children.push(nodeResult);
                            // deep copy for pushing the tree component to refresh the object
                            _this.uiVar.nodeChanged = JSON.parse(JSON.stringify(_this.priVar.currentNode));
                            _this.uiAction.selectedOrganizationsChange(_this.priVar.currentNode);
                            _this.uiVar.organizationIsNew = false;
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                    else {
                        // Update an organization
                        _this.priVar.currentNode.name = nodeToSave.name;
                        _this.fofPermissionService.organization.update({
                            id: _this.priVar.currentNode.id,
                            name: _this.priVar.currentNode.name
                        })
                            .toPromise()
                            .then(function (nodeResult) {
                            _this.uiVar.nodeChanged = nodeResult;
                            _this.fofNotificationService.saveIsDone();
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                },
                organisationCancel: function () {
                    _this.uiVar.actionTitle = "Modification de l'organisation";
                    _this.uiVar.nodeForm = false;
                    _this.uiVar.organizationIsNew = false;
                },
                selectedOrganizationsChange: function (node) {
                    if (node) {
                        _this.uiVar.nodeForm = node.checked;
                        _this.uiVar.form.patchValue(node);
                    }
                    else {
                        _this.uiVar.nodeForm = false;
                        _this.uiVar.form.reset();
                    }
                    if (node && node.checked) {
                        _this.priVar.currentNode = node;
                        _this.uiVar.mainTitle = node.name;
                    }
                    else {
                        _this.priVar.currentNode = null;
                        _this.uiVar.mainTitle = "G\u00E9rer l'organisation";
                    }
                }
            };
        }
        // Angular events
        FofOrganizationsComponent.prototype.ngOnInit = function () {
        };
        FofOrganizationsComponent.ɵfac = function FofOrganizationsComponent_Factory(t) { return new (t || FofOrganizationsComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](forms.FormBuilder), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](FofDialogService), core["ɵɵdirectiveInject"](FofErrorService)); };
        FofOrganizationsComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: FofOrganizationsComponent, selectors: [["fof-core-fof-organizations"]], decls: 14, vars: 8, consts: [[1, "row", "main"], [1, "col-md-6"], [1, "card-tree"], [3, "nodeChanged", "nodeToDelete", "selectedOrganizationsChange"], [1, "fof-header", "card-detail"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "warn", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "accent", 3, "click", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], ["class", "fof-fade-in card-detail", 4, "ngIf"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in", "card-detail"], [1, "fof-header"], ["mat-stroked-button", "", "class", "child-add", "color", "primary", 3, "click", 4, "ngIf"], [3, "formGroup"], ["matInput", "", "required", "", "type", "name", "formControlName", "name", "placeholder", "Nom de l'organisation", "value", ""], [4, "ngIf"], ["mat-stroked-button", "", "color", "primary", 1, "child-add", 3, "click"]], template: function FofOrganizationsComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div", 0);
                core["ɵɵelementStart"](1, "div", 1);
                core["ɵɵelementStart"](2, "mat-card", 2);
                core["ɵɵelementStart"](3, "fof-organizations-tree", 3);
                core["ɵɵlistener"]("selectedOrganizationsChange", function FofOrganizationsComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](4, "div", 1);
                core["ɵɵelementStart"](5, "mat-card", 4);
                core["ɵɵelementStart"](6, "h3");
                core["ɵɵtext"](7);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](8, "div", 5);
                core["ɵɵtemplate"](9, FofOrganizationsComponent_button_9_Template, 2, 0, "button", 6);
                core["ɵɵtemplate"](10, FofOrganizationsComponent_button_10_Template, 2, 0, "button", 7);
                core["ɵɵtemplate"](11, FofOrganizationsComponent_button_11_Template, 2, 0, "button", 8);
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵtemplate"](12, FofOrganizationsComponent_div_12_Template, 4, 0, "div", 9);
                core["ɵɵtemplate"](13, FofOrganizationsComponent_div_13_Template, 10, 4, "div", 10);
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](3);
                core["ɵɵproperty"]("nodeChanged", ctx.uiVar.nodeChanged)("nodeToDelete", ctx.uiVar.nodeToDelete);
                core["ɵɵadvance"](4);
                core["ɵɵtextInterpolate"](ctx.uiVar.mainTitle);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.nodeForm);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.nodeForm && !ctx.uiVar.organizationIsNew);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.nodeForm);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.loading);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", !ctx.uiVar.loading && ctx.uiVar.nodeForm);
            } }, directives: [card.MatCard, FofOrganizationsTreeComponent, common.NgIf, button.MatButton, progressSpinner.MatSpinner, forms["ɵangular_packages_forms_forms_y"], forms.NgControlStatusGroup, forms.FormGroupDirective, formField.MatFormField, input.MatInput, forms.DefaultValueAccessor, forms.RequiredValidator, forms.NgControlStatus, forms.FormControlName, formField.MatError], styles: [".row.main[_ngcontent-%COMP%], .row.main[_ngcontent-%COMP%]   .card-tree[_ngcontent-%COMP%]{height:100%}.row.main[_ngcontent-%COMP%]   .card-detail[_ngcontent-%COMP%]{position:-webkit-sticky;position:sticky;top:0}.mat-form-field[_ngcontent-%COMP%]{width:100%}.child-add[_ngcontent-%COMP%]{margin-bottom:15px}"] });
        return FofOrganizationsComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofOrganizationsComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-fof-organizations',
                    templateUrl: './fof-organizations.component.html',
                    styleUrls: ['./fof-organizations.component.scss']
                }]
        }], function () { return [{ type: FofPermissionService }, { type: forms.FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: FofErrorService }]; }, null); })();

    var FofAuthGuard = /** @class */ (function () {
        function FofAuthGuard(router, foFAuthService, fofNotificationService, fofConfig) {
            this.router = router;
            this.foFAuthService = foFAuthService;
            this.fofNotificationService = fofNotificationService;
            this.fofConfig = fofConfig;
        }
        FofAuthGuard.prototype.canActivate = function (next, state) {
            // for permissions in route 
            // https://jasonwatmore.com/post/2019/08/06/angular-8-role-based-authorization-tutorial-with-example#tsconfig-json
            var currentUser = this.foFAuthService.currentUser;
            var notAuthentifiedRoute = '/login';
            if (this.fofConfig.authentication) {
                if (this.fofConfig.authentication.type == exports.eAuth.windows) {
                    notAuthentifiedRoute = 'loading';
                }
            }
            if (currentUser) {
                // check if route is restricted by permission      
                // get the deepest root data
                // https://stackoverflow.com/questions/43806188/how-can-i-access-an-activated-child-routes-data-from-the-parent-routes-compone
                var route = next;
                while (route.firstChild) {
                    route = route.firstChild;
                }
                var data_1 = route.data;
                if (data_1.permissions) {
                    var found = Object.values(currentUser.permissions).some(function (r) { return data_1.permissions.indexOf(r) >= 0; });
                    if (!found) {
                        this.fofNotificationService.error("Vous n'avez pas les droit suffisant<br>\n            pour naviguer vers " + state.url);
                        this.router.navigate(['/']);
                        return false;
                    }
                }
                // authorised
                return true;
            }
            // not logged in. Redirect to then notAuthentified page with the url to return on
            this.router.navigate([notAuthentifiedRoute], { queryParams: { returnUrl: state.url } });
            return false;
        };
        FofAuthGuard.ɵfac = function FofAuthGuard_Factory(t) { return new (t || FofAuthGuard)(core["ɵɵinject"](router.Router), core["ɵɵinject"](FoFAuthService), core["ɵɵinject"](FofNotificationService), core["ɵɵinject"](CORE_CONFIG)); };
        FofAuthGuard.ɵprov = core["ɵɵdefineInjectable"]({ token: FofAuthGuard, factory: FofAuthGuard.ɵfac, providedIn: 'root' });
        return FofAuthGuard;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofAuthGuard, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: router.Router }, { type: FoFAuthService }, { type: FofNotificationService }, { type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }]; }, null); })();

    var eCp;
    (function (eCp) {
        eCp["userCreate"] = "userCreate";
        eCp["userUpdate"] = "userUpdate";
        eCp["userRead"] = "userRead";
        eCp["userDelete"] = "userDelete";
        eCp["roleCreate"] = "roleCreate";
        eCp["roleUpdate"] = "roleUpdate";
        eCp["roleRead"] = "roleRead";
        eCp["roleDelete"] = "roleDelete";
        eCp["userRoleCreate"] = "userRoleCreate";
        eCp["userRoleUpdate"] = "userRoleUpdate";
        eCp["userRoleRead"] = "userRoleRead";
        eCp["userRoleDelete"] = "userRoleDelete";
        eCp["organizationCreate"] = "organizationCreate";
        eCp["organizationRead"] = "organizationRead";
        eCp["organizationUpdate"] = "organizationUpdate";
        eCp["organizationDelete"] = "organizationDelete";
        eCp["coreAdminAccess"] = "coreAdminAccess";
    })(eCp || (eCp = {}));

    var routes = [
        { path: 'admin', canActivate: [FofAuthGuard], data: { permissions: [eCp.coreAdminAccess] },
            children: [
                { path: '', component: FofRolesComponent },
                /**
                 * if a child doesn't have permissions, it will get that ones
                 * if it has some, it will override this ones
                 * e.g. first child (path: '') get that ones
                 */
                // { path: 'roles', data: {permissions: [eCp.roleRead]},
                { path: 'roles',
                    children: [
                        { path: '', component: FofRolesComponent },
                        // { path: 'new', component: FofRoleComponent, data: {permissions: [eCp.roleCreate]} }
                        // { path: 'new', component: FofRoleComponent },
                        { path: ':code', component: FofRoleComponent }
                    ]
                },
                { path: 'users',
                    children: [
                        { path: '', component: FofUsersComponent },
                        { path: ':code', component: FofUserComponent }
                    ]
                },
                { path: 'organizations', data: { permissions: [eCp.organizationRead] },
                    children: [
                        { path: '', component: FofOrganizationsComponent }
                    ]
                }
            ]
        }
    ];
    var AdminRoutingModule = /** @class */ (function () {
        function AdminRoutingModule() {
        }
        AdminRoutingModule.ɵmod = core["ɵɵdefineNgModule"]({ type: AdminRoutingModule });
        AdminRoutingModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function AdminRoutingModule_Factory(t) { return new (t || AdminRoutingModule)(); }, imports: [[router.RouterModule.forChild(routes)],
                router.RouterModule] });
        return AdminRoutingModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](AdminRoutingModule, { imports: [router.RouterModule], exports: [router.RouterModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](AdminRoutingModule, [{
            type: core.NgModule,
            args: [{
                    imports: [router.RouterModule.forChild(routes)],
                    exports: [router.RouterModule]
                }]
        }], null, null); })();

    var fofTranslate = /** @class */ (function () {
        function fofTranslate() {
        }
        fofTranslate.prototype.transform = function (content) {
            return "<b>" + content + "</b>";
        };
        fofTranslate.ɵfac = function fofTranslate_Factory(t) { return new (t || fofTranslate)(); };
        fofTranslate.ɵpipe = core["ɵɵdefinePipe"]({ name: "fofTranslate", type: fofTranslate, pure: true });
        return fofTranslate;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](fofTranslate, [{
            type: core.Pipe,
            args: [{ name: 'fofTranslate' }]
        }], null, null); })();

    var SharedModule = /** @class */ (function () {
        function SharedModule() {
        }
        SharedModule.ɵmod = core["ɵɵdefineNgModule"]({ type: SharedModule });
        SharedModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
                    common.CommonModule
                ]] });
        return SharedModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](SharedModule, { declarations: [fofTranslate], imports: [common.CommonModule], exports: [fofTranslate] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](SharedModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [
                        fofTranslate
                    ],
                    imports: [
                        common.CommonModule
                    ],
                    exports: [
                        fofTranslate
                    ]
                }]
        }], null, null); })();

    var AdminModule = /** @class */ (function () {
        function AdminModule() {
        }
        AdminModule.ɵmod = core["ɵɵdefineNgModule"]({ type: AdminModule });
        AdminModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function AdminModule_Factory(t) { return new (t || AdminModule)(); }, imports: [[
                    common.CommonModule,
                    SharedModule,
                    AdminRoutingModule,
                    ComponentsModule,
                    FofPermissionModule,
                    MaterialModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule,
                    core$1.TranslateModule //.forChild()
                ]] });
        return AdminModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](AdminModule, { declarations: [FofRolesComponent,
            FofRoleComponent,
            FofUsersComponent,
            FofUserComponent,
            FofEntityFooterComponent,
            FofOrganizationsComponent,
            FofUserRolesSelectComponent], imports: [common.CommonModule,
            SharedModule,
            AdminRoutingModule,
            ComponentsModule,
            FofPermissionModule,
            MaterialModule,
            forms.FormsModule,
            forms.ReactiveFormsModule,
            core$1.TranslateModule //.forChild()
        ] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](AdminModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [
                        FofRolesComponent,
                        FofRoleComponent,
                        FofUsersComponent,
                        FofUserComponent,
                        FofEntityFooterComponent,
                        FofOrganizationsComponent,
                        FofUserRolesSelectComponent
                    ],
                    imports: [
                        common.CommonModule,
                        SharedModule,
                        AdminRoutingModule,
                        ComponentsModule,
                        FofPermissionModule,
                        MaterialModule,
                        forms.FormsModule,
                        forms.ReactiveFormsModule,
                        core$1.TranslateModule //.forChild()
                    ]
                }]
        }], null, null); })();

    var FofLocalstorageService = /** @class */ (function () {
        function FofLocalstorageService(fofConfig) {
            this.fofConfig = fofConfig;
            this.appShortName = this.fofConfig.appName.technical.toLocaleUpperCase() + "-";
        }
        FofLocalstorageService.prototype.setItem = function (key, value) {
            localStorage.setItem("" + this.appShortName + key, JSON.stringify(value));
        };
        FofLocalstorageService.prototype.getItem = function (key) {
            return JSON.parse(localStorage.getItem("" + this.appShortName + key));
        };
        FofLocalstorageService.prototype.removeItem = function (key) {
            localStorage.removeItem("" + this.appShortName + key);
        };
        FofLocalstorageService.ɵfac = function FofLocalstorageService_Factory(t) { return new (t || FofLocalstorageService)(core["ɵɵinject"](CORE_CONFIG)); };
        FofLocalstorageService.ɵprov = core["ɵɵdefineInjectable"]({ token: FofLocalstorageService, factory: FofLocalstorageService.ɵfac, providedIn: 'root' });
        return FofLocalstorageService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofLocalstorageService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }]; }, null); })();

    // Angular
    function notificationComponent_div_1_Template(rf, ctx) { if (rf & 1) {
        var _r304 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div");
        core["ɵɵelementStart"](1, "button", 2);
        core["ɵɵlistener"]("click", function notificationComponent_div_1_Template_button_click_1_listener() { core["ɵɵrestoreView"](_r304); var notification_r302 = ctx.$implicit; var ctx_r303 = core["ɵɵnextContext"](); return ctx_r303.uiAction.removenotification(notification_r302); });
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "\u00D7");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelement"](4, "div", 3);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var notification_r302 = ctx.$implicit;
        var ctx_r300 = core["ɵɵnextContext"]();
        core["ɵɵclassMapInterpolate1"]("", ctx_r300.uiAction.cssClass(notification_r302), " notification-dismissable");
        core["ɵɵproperty"]("@inOut", undefined);
        core["ɵɵadvance"](4);
        core["ɵɵproperty"]("innerHTML", notification_r302.message, core["ɵɵsanitizeHtml"]);
    } }
    function notificationComponent_div_2_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 4);
        core["ɵɵelement"](1, "span");
        core["ɵɵelementEnd"]();
    } }
    // how to
    // https://angular.io/guide/animations
    // https://github.com/PointInside/ng2-toastr/blob/master/src/toast-container.component.ts
    var notificationComponent = /** @class */ (function () {
        function notificationComponent(fofNotificationService, ngZone) {
            var _this = this;
            this.fofNotificationService = fofNotificationService;
            this.ngZone = ngZone;
            // All private variables
            this.priVar = {
                getnotificationSub: undefined,
                savedNotificationSub: undefined
            };
            // All private functions
            this.privFunc = {};
            // All variables shared with UI 
            this.uiVar = {
                notifications: [],
                savedActive: false
            };
            // All actions shared with UI 
            this.uiAction = {
                removenotification: function (notification) {
                    _this.uiVar.notifications = _this.uiVar.notifications.filter(function (x) { return x !== notification; });
                },
                cssClass: function (notification) {
                    if (!notification) {
                        return;
                    }
                    // return css class based on notification type
                    switch (notification.type) {
                        case NotificationType.Success:
                            return 'notification notification-success';
                        case NotificationType.Error:
                            return 'notification notification-danger';
                        case NotificationType.Info:
                            return 'notification notification-info';
                        case NotificationType.Warning:
                            return 'notification notification-warning';
                    }
                }
            };
        }
        // Angular events
        notificationComponent.prototype.ngOnInit = function () {
            var _this = this;
            var template = this;
            this.priVar.getnotificationSub = this.fofNotificationService.getnotification()
                .subscribe(function (notification) {
                if (!notification) {
                    // clear notifications when an empty notification is received
                    _this.uiVar.notifications = [];
                    return;
                }
                if (notification.mustDisappearAfter) {
                    setTimeout(function () {
                        template.uiAction.removenotification(notification);
                    }, notification.mustDisappearAfter);
                }
                // ensure the notif will be displayed even if received from a promise pipe
                _this.ngZone.run(function () {
                    // push or unshift depend if there are on the top or bottom
                    // this.notifications.push(notification)
                    _this.uiVar.notifications.unshift(notification);
                });
            });
            this.fofNotificationService.savedNotification
                .subscribe(function (saved) {
                if (!saved) {
                    return;
                }
                _this.uiVar.savedActive = true;
                setTimeout(function () {
                    _this.uiVar.savedActive = false;
                }, 300);
            });
        };
        notificationComponent.prototype.ngOnDestroy = function () {
            if (this.priVar.getnotificationSub) {
                this.priVar.getnotificationSub.unsubscribe();
            }
            if (this.priVar.savedNotificationSub) {
                this.priVar.savedNotificationSub.unsubscribe();
            }
        };
        notificationComponent.ɵfac = function notificationComponent_Factory(t) { return new (t || notificationComponent)(core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](core.NgZone)); };
        notificationComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: notificationComponent, selectors: [["fof-notification"]], decls: 3, vars: 2, consts: [[3, "class", 4, "ngFor", "ngForOf"], ["class", "saved-notification", 4, "ngIf"], ["type", "button", "data-dismiss", "notification", 1, "close", 3, "click"], [3, "innerHTML"], [1, "saved-notification"]], template: function notificationComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div");
                core["ɵɵtemplate"](1, notificationComponent_div_1_Template, 5, 5, "div", 0);
                core["ɵɵelementEnd"]();
                core["ɵɵtemplate"](2, notificationComponent_div_2_Template, 2, 0, "div", 1);
            } if (rf & 2) {
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngForOf", ctx.uiVar.notifications);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.savedActive);
            } }, directives: [common.NgForOf, common.NgIf], styles: ["[_nghost-%COMP%]{position:fixed;z-index:999999;bottom:12px;right:12px;width:300px}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]{padding:.5rem;margin-top:.5rem;position:relative}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]:hover   .close[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]{position:absolute;border:none;background-color:transparent;right:-5px;top:-13px;font-size:25px;cursor:pointer;opacity:.3}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]{position:fixed;z-index:9999999;bottom:15px;right:15px;width:25px;height:25px;border-radius:50px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{position:absolute;left:-3px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after, [_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{box-sizing:border-box;content:\"\";position:absolute}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{border-style:solid;border-width:7px 2px 1px;border-radius:1px;height:16px;left:8px;top:5px;width:16px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after{border-style:solid;border-width:1px 1px 1px 4px;height:5px;left:13px;top:5px;width:7px}"], data: { animation: [
                    animations.trigger('inOut', [
                        animations.transition(':enter', [
                            animations.style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }),
                            animations.animate('0.2s ease-in')
                        ]),
                        animations.transition(':leave', [
                            animations.animate('0.2s 10ms ease-out', animations.style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }))
                        ])
                    ])
                ] } });
        return notificationComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](notificationComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-notification',
                    templateUrl: './notification.component.html',
                    styleUrls: ['./notification.component.scss'],
                    animations: [
                        animations.trigger('inOut', [
                            animations.transition(':enter', [
                                animations.style({
                                    opacity: 0,
                                    transform: 'translateX(100%)'
                                }),
                                animations.animate('0.2s ease-in')
                            ]),
                            animations.transition(':leave', [
                                animations.animate('0.2s 10ms ease-out', animations.style({
                                    opacity: 0,
                                    transform: 'translateX(100%)'
                                }))
                            ])
                        ])
                    ]
                }]
        }], function () { return [{ type: FofNotificationService }, { type: core.NgZone }]; }, null); })();

    /** Application-wide error handler that adds a UI notification to the error handling
     * provided by the default Angular ErrorHandler.
     */
    var fofErrorHandler = /** @class */ (function (_super) {
        __extends(fofErrorHandler, _super);
        function fofErrorHandler(notificationsService, fofConfig) {
            var _this = _super.call(this) || this;
            _this.notificationsService = notificationsService;
            _this.fofConfig = fofConfig;
            return _this;
        }
        // handleError(error: Error | HttpErrorResponse | UnhandledRejection) {
        fofErrorHandler.prototype.handleError = function (exception) {
            var displayMessage = "Oops, nous avons une erreur...";
            var error;
            // if (exception instanceof HttpErrorResponse) {
            //   console.log('HttpErrorResponse', exception)
            //   error = exception.error
            //   if (error.fofCoreException) { 
            //     console.log('fof core exception', exception)
            //   }
            // }   
            // ToDO: manage promise unhandledrejection -> error in code
            // https://javascript.info/promise-error-handling
            if (exception.promise) {
                displayMessage = "<small>Technical notice</small><br>\n        Forgot to manage a promise rejection...<br>\n        <small>(yeah we know, it's funny ;)</small>";
                console.info("fofErrorHandler - Promises not catched!");
            }
            // console.error('fofErrorHandler - Common error', exception)
            this.notificationsService.error(displayMessage, { mustDisappearAfter: -1 });
            _super.prototype.handleError.call(this, exception);
        };
        fofErrorHandler.ɵfac = function fofErrorHandler_Factory(t) { return new (t || fofErrorHandler)(core["ɵɵinject"](FofNotificationService), core["ɵɵinject"](CORE_CONFIG)); };
        fofErrorHandler.ɵprov = core["ɵɵdefineInjectable"]({ token: fofErrorHandler, factory: fofErrorHandler.ɵfac });
        return fofErrorHandler;
    }(core.ErrorHandler));
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](fofErrorHandler, [{
            type: core.Injectable
        }], function () { return [{ type: FofNotificationService }, { type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }]; }, null); })();

    var FofErrorInterceptor = /** @class */ (function () {
        function FofErrorInterceptor(fofErrorService) {
            this.fofErrorService = fofErrorService;
        }
        FofErrorInterceptor.prototype.intercept = function (request, next) {
            var _this = this;
            return next.handle(request)
                .pipe(operators.catchError(function (httpError) {
                return rxjs.throwError(_this.fofErrorService.cleanError(httpError));
            }));
        };
        FofErrorInterceptor.ɵfac = function FofErrorInterceptor_Factory(t) { return new (t || FofErrorInterceptor)(core["ɵɵinject"](FofErrorService)); };
        FofErrorInterceptor.ɵprov = core["ɵɵdefineInjectable"]({ token: FofErrorInterceptor, factory: FofErrorInterceptor.ɵfac });
        return FofErrorInterceptor;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FofErrorInterceptor, [{
            type: core.Injectable
        }], function () { return [{ type: FofErrorService }]; }, null); })();

    var FoFJwtInterceptor = /** @class */ (function () {
        function FoFJwtInterceptor(foFAuthService, fofConfig) {
            this.foFAuthService = foFAuthService;
            this.fofConfig = fofConfig;
            this.environment = this.fofConfig.environment;
        }
        FoFJwtInterceptor.prototype.intercept = function (request, next) {
            // add auth header with jwt if user is logged in and request is to api url    
            var currentUser = this.foFAuthService.currentUser;
            var isLoggedIn = currentUser && currentUser.accessToken;
            var isApiUrl = request.url.startsWith(this.environment.apiPath);
            if (isLoggedIn && isApiUrl) {
                request = request.clone({
                    setHeaders: {
                        Authorization: "Bearer " + currentUser.accessToken
                    }
                });
            }
            return next.handle(request);
        };
        FoFJwtInterceptor.ɵfac = function FoFJwtInterceptor_Factory(t) { return new (t || FoFJwtInterceptor)(core["ɵɵinject"](FoFAuthService), core["ɵɵinject"](CORE_CONFIG)); };
        FoFJwtInterceptor.ɵprov = core["ɵɵdefineInjectable"]({ token: FoFJwtInterceptor, factory: FoFJwtInterceptor.ɵfac });
        return FoFJwtInterceptor;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FoFJwtInterceptor, [{
            type: core.Injectable
        }], function () { return [{ type: FoFAuthService }, { type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }]; }, null); })();

    // import { ComponentsModule } from '../components/components.module'
    // import { ModuleTranslateLoader, IModuleTranslationOptions } from '@larscom/ngx-translate-module-loader'
    // import { registerLocaleData } from '@angular/common'
    // import localeFr from '@angular/common/locales/fr'
    // import localeFrExtra from '@angular/common/locales/extra/fr'
    // registerLocaleData(localeFr)
    // AoT requires an exported function for factories
    function HttpLoaderFactory(http) {
        return new httpLoader.TranslateHttpLoader(http);
    }
    // https://github.com/larscom/ngx-translate-module-loader
    // export function moduleHttpLoaderFactory(http: HttpClient) {
    //   const baseTranslateUrl = './assets/i18n';
    //   const options: IModuleTranslationOptions = {
    //     modules: [
    //       // final url: ./assets/i18n/en.json
    //       { baseTranslateUrl },
    //       // final url: ./assets/i18n/admin/en.json
    //       { moduleName: 'admin', baseTranslateUrl },
    //       // final url: ./assets/i18n/feature2/en.json
    //       { moduleName: 'feature2', baseTranslateUrl }
    //     ]
    //   };
    //   return new ModuleTranslateLoader(http, options);
    // }
    var FoFCoreModule = /** @class */ (function () {
        function FoFCoreModule() {
        }
        FoFCoreModule.ɵmod = core["ɵɵdefineNgModule"]({ type: FoFCoreModule });
        FoFCoreModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function FoFCoreModule_Factory(t) { return new (t || FoFCoreModule)(); }, providers: [
                FofLocalstorageService,
                // { provide: LOCALE_ID, useValue: 'fr' },
                { provide: core.ErrorHandler, useClass: fofErrorHandler },
                { provide: http.HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
                { provide: http.HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
                FofAuthGuard,
                FoFAuthService,
                FoFCoreService,
                FofNotificationService,
                FofDialogService,
                FofErrorService
            ], imports: [[
                    common.CommonModule,
                    SharedModule,
                    MaterialModule,
                    http.HttpClientModule,
                    // ComponentsModule,
                    core$1.TranslateModule.forRoot({
                        defaultLanguage: 'fr',
                        loader: {
                            provide: core$1.TranslateLoader,
                            useFactory: HttpLoaderFactory,
                            deps: [http.HttpClient]
                        }
                    })
                ]] });
        return FoFCoreModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](FoFCoreModule, { declarations: [notificationComponent,
            FofCoreDialogYesNoComponent], imports: [common.CommonModule,
            SharedModule,
            MaterialModule,
            http.HttpClientModule, core$1.TranslateModule], exports: [notificationComponent,
            FofCoreDialogYesNoComponent] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FoFCoreModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [
                        notificationComponent,
                        FofCoreDialogYesNoComponent
                    ],
                    imports: [
                        common.CommonModule,
                        SharedModule,
                        MaterialModule,
                        http.HttpClientModule,
                        // ComponentsModule,
                        core$1.TranslateModule.forRoot({
                            defaultLanguage: 'fr',
                            loader: {
                                provide: core$1.TranslateLoader,
                                useFactory: HttpLoaderFactory,
                                deps: [http.HttpClient]
                            }
                        })
                    ],
                    entryComponents: [
                        FofCoreDialogYesNoComponent
                    ],
                    providers: [
                        FofLocalstorageService,
                        // { provide: LOCALE_ID, useValue: 'fr' },
                        { provide: core.ErrorHandler, useClass: fofErrorHandler },
                        { provide: http.HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
                        { provide: http.HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
                        FofAuthGuard,
                        FoFAuthService,
                        FoFCoreService,
                        FofNotificationService,
                        FofDialogService,
                        FofErrorService
                    ],
                    exports: [
                        notificationComponent,
                        FofCoreDialogYesNoComponent,
                    ]
                }]
        }], null, null); })();

    function UsersComponent_div_19_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 21);
        core["ɵɵelement"](1, "mat-spinner", 22);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des collaborateurs...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function UsersComponent_th_22_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Email");
        core["ɵɵelementEnd"]();
    } }
    function UsersComponent_td_23_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r316 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r316.email);
    } }
    function UsersComponent_th_25_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Login");
        core["ɵɵelementEnd"]();
    } }
    function UsersComponent_td_26_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r317 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r317.login);
    } }
    function UsersComponent_th_28_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Pr\u00E9nom");
        core["ɵɵelementEnd"]();
    } }
    function UsersComponent_td_29_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r318 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r318.firstName);
    } }
    function UsersComponent_th_31_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 23);
        core["ɵɵtext"](1, "Nom");
        core["ɵɵelementEnd"]();
    } }
    function UsersComponent_td_32_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 24);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r319 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r319.lastName);
    } }
    function UsersComponent_tr_33_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 25);
    } }
    function UsersComponent_tr_34_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 26);
    } if (rf & 2) {
        var row_r320 = ctx.$implicit;
        core["ɵɵproperty"]("routerLink", row_r320.id);
    } }
    var _c0$2 = function () { return [5, 10, 25, 100]; };
    var UsersComponent = /** @class */ (function () {
        function UsersComponent(fofPermissionService, fofNotificationService, breakpointObserver, fofErrorService) {
            var _this = this;
            this.fofPermissionService = fofPermissionService;
            this.fofNotificationService = fofNotificationService;
            this.breakpointObserver = breakpointObserver;
            this.fofErrorService = fofErrorService;
            // All private variables
            this.priVar = {
                breakpointObserverSub: undefined,
                filter: undefined,
                filterOrganizationsChange: new core.EventEmitter(),
                filterOrganizations: undefined
            };
            // All private functions
            this.privFunc = {};
            // All variables shared with UI 
            this.uiVar = {
                displayedColumns: [],
                data: [],
                resultsLength: 0,
                pageSize: 5,
                isLoadingResults: true,
                searchUsersCtrl: new forms.FormControl()
            };
            // All actions shared with UI 
            this.uiAction = {
                organisationMultiSelectedChange: function (organization) {
                    if (organization) {
                        _this.priVar.filterOrganizations = [organization.id];
                    }
                    else {
                        _this.priVar.filterOrganizations = null;
                    }
                    _this.priVar.filterOrganizationsChange.emit(organization);
                }
            };
        }
        // Angular events
        UsersComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.priVar.filter = this.uiVar.searchUsersCtrl.valueChanges
                .pipe(operators.debounceTime(500), operators.distinctUntilChanged(), operators.filter(function (query) { return query.length >= 3 || query.length === 0; }));
            this.priVar.breakpointObserverSub = this.breakpointObserver.observe(layout.Breakpoints.XSmall)
                .subscribe(function (state) {
                if (state.matches) {
                    // XSmall
                    _this.uiVar.displayedColumns = ['email', 'login'];
                }
                else {
                    // > XSmall
                    _this.uiVar.displayedColumns = ['email', 'login', 'firstName', 'lastName'];
                }
            });
        };
        UsersComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            // If the user changes the sort order, reset back to the first page.
            this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
            rxjs.merge(this.sort.sortChange, this.paginator.page, this.priVar.filter, this.priVar.filterOrganizationsChange)
                .pipe(operators.startWith({}), operators.switchMap(function () {
                _this.uiVar.isLoadingResults = true;
                _this.uiVar.pageSize = _this.paginator.pageSize;
                return _this.fofPermissionService.user.search(_this.uiVar.searchUsersCtrl.value, _this.priVar.filterOrganizations, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
            }), operators.map(function (search) {
                _this.uiVar.isLoadingResults = false;
                _this.uiVar.resultsLength = search.total;
                return search.data;
            }), operators.catchError(function (error) {
                _this.fofErrorService.errorManage(error);
                _this.uiVar.isLoadingResults = false;
                return rxjs.of([]);
            })).subscribe(function (data) {
                _this.uiVar.data = data;
            });
        };
        UsersComponent.prototype.ngOnDestroy = function () {
            if (this.priVar.breakpointObserverSub) {
                this.priVar.breakpointObserverSub.unsubscribe();
            }
        };
        UsersComponent.ɵfac = function UsersComponent_Factory(t) { return new (t || UsersComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](layout.BreakpointObserver), core["ɵɵdirectiveInject"](FofErrorService)); };
        UsersComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: UsersComponent, selectors: [["fof-core-users"]], viewQuery: function UsersComponent_Query(rf, ctx) { if (rf & 1) {
                core["ɵɵviewQuery"](paginator.MatPaginator, true);
                core["ɵɵviewQuery"](sort.MatSort, true);
            } if (rf & 2) {
                var _t;
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.paginator = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.sort = _t.first);
            } }, decls: 36, vars: 11, consts: [[1, "row"], [1, "col-md-3"], [1, "card-tree"], [3, "multiSelect", "selectedOrganizationsChange"], [1, "col-md-9"], [1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "filtres"], ["autofocus", "", "matInput", "", "placeholder", "email ou nom ou pr\u00E9nom ou login", 3, "formControl"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "email", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "email"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "login"], ["matColumnDef", "firstName"], ["matColumnDef", "lastName"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UsersComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "div", 0);
                core["ɵɵelementStart"](1, "div", 1);
                core["ɵɵelementStart"](2, "mat-card", 2);
                core["ɵɵelementStart"](3, "fof-organizations-tree", 3);
                core["ɵɵlistener"]("selectedOrganizationsChange", function UsersComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](4, "div", 4);
                core["ɵɵelementStart"](5, "mat-card", 5);
                core["ɵɵelementStart"](6, "h3");
                core["ɵɵtext"](7, "Gestion des collaborateurs");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](8, "a", 6);
                core["ɵɵelementStart"](9, "span");
                core["ɵɵtext"](10, "Ajouter un collaborateur");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](11, "mat-card", 7);
                core["ɵɵelementStart"](12, "mat-form-field");
                core["ɵɵelementStart"](13, "mat-label");
                core["ɵɵtext"](14, "Filtre");
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](15, "input", 8);
                core["ɵɵelementStart"](16, "mat-hint");
                core["ɵɵtext"](17, "Recherche \u00E0 partir de 3 caract\u00E8res saisies");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](18, "div", 9);
                core["ɵɵtemplate"](19, UsersComponent_div_19_Template, 4, 0, "div", 10);
                core["ɵɵelementStart"](20, "table", 11);
                core["ɵɵelementContainerStart"](21, 12);
                core["ɵɵtemplate"](22, UsersComponent_th_22_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](23, UsersComponent_td_23_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](24, 15);
                core["ɵɵtemplate"](25, UsersComponent_th_25_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](26, UsersComponent_td_26_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](27, 16);
                core["ɵɵtemplate"](28, UsersComponent_th_28_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](29, UsersComponent_td_29_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](30, 17);
                core["ɵɵtemplate"](31, UsersComponent_th_31_Template, 2, 0, "th", 13);
                core["ɵɵtemplate"](32, UsersComponent_td_32_Template, 2, 1, "td", 14);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵtemplate"](33, UsersComponent_tr_33_Template, 1, 0, "tr", 18);
                core["ɵɵtemplate"](34, UsersComponent_tr_34_Template, 1, 1, "tr", 19);
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](35, "mat-paginator", 20);
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](3);
                core["ɵɵproperty"]("multiSelect", false);
                core["ɵɵadvance"](5);
                core["ɵɵproperty"]("routerLink", "/admin/users/new");
                core["ɵɵadvance"](7);
                core["ɵɵproperty"]("formControl", ctx.uiVar.searchUsersCtrl);
                core["ɵɵadvance"](4);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.isLoadingResults);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("dataSource", ctx.uiVar.data);
                core["ɵɵadvance"](13);
                core["ɵɵproperty"]("matHeaderRowDef", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("matRowDefColumns", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("length", ctx.uiVar.resultsLength)("pageSizeOptions", core["ɵɵpureFunction0"](10, _c0$2))("pageSize", ctx.uiVar.pageSize);
            } }, directives: [card.MatCard, FofOrganizationsTreeComponent, button.MatAnchor, router.RouterLinkWithHref, formField.MatFormField, formField.MatLabel, input.MatInput, forms.DefaultValueAccessor, forms.NgControlStatus, forms.FormControlDirective, formField.MatHint, common.NgIf, table.MatTable, sort.MatSort, table.MatColumnDef, table.MatHeaderCellDef, table.MatCellDef, table.MatHeaderRowDef, table.MatRowDef, paginator.MatPaginator, progressSpinner.MatSpinner, table.MatHeaderCell, sort.MatSortHeader, table.MatCell, table.MatHeaderRow, table.MatRow, router.RouterLink], styles: [".filtres[_ngcontent-%COMP%]{margin-bottom:15px}.filtres[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.card-tree[_ngcontent-%COMP%]{height:100%}"] });
        return UsersComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UsersComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-users',
                    templateUrl: './users.component.html',
                    styleUrls: ['./users.component.scss']
                }]
        }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: layout.BreakpointObserver }, { type: FofErrorService }]; }, { paginator: [{
                type: core.ViewChild,
                args: [paginator.MatPaginator]
            }], sort: [{
                type: core.ViewChild,
                args: [sort.MatSort]
            }] }); })();

    var UservicesService = /** @class */ (function () {
        function UservicesService(fofConfig, httpClient) {
            var _this = this;
            this.fofConfig = fofConfig;
            this.httpClient = httpClient;
            this.uServices = {
                create: function (uService) { return _this.httpClient.post(_this.environment.apiPath + "/uservices", uService); },
                update: function (uService) { return _this.httpClient.patch(_this.environment.apiPath + "/uservices/" + uService.id, uService); },
                delete: function (uService) { return _this.httpClient.delete(_this.environment.apiPath + "/uservices/" + uService.id); },
                getOne: function (id) { return _this.httpClient.get(_this.environment.apiPath + "/uservices/" + id); },
                getAll: function () { return _this.httpClient.get(_this.environment.apiPath + "/uservices"); }
            };
            this.environment = this.fofConfig.environment;
        }
        UservicesService.ɵfac = function UservicesService_Factory(t) { return new (t || UservicesService)(core["ɵɵinject"](CORE_CONFIG), core["ɵɵinject"](http.HttpClient)); };
        UservicesService.ɵprov = core["ɵɵdefineInjectable"]({ token: UservicesService, factory: UservicesService.ɵfac, providedIn: 'root' });
        return UservicesService;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UservicesService, [{
            type: core.Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return [{ type: undefined, decorators: [{
                    type: core.Inject,
                    args: [CORE_CONFIG]
                }] }, { type: http.HttpClient }]; }, null); })();

    function UserComponent_div_11_mat_error_6_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le login ne doit pas exc\u00E9der 30 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function UserComponent_div_11_mat_error_9_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Un email valide est obligatoire ");
        core["ɵɵelementEnd"]();
    } }
    function UserComponent_div_11_mat_error_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le pr\u00E9nom ne doit pas exc\u00E9der 30 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function UserComponent_div_11_mat_error_15_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le nom de famille ne doit pas exc\u00E9der 30 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template(rf, ctx) { if (rf & 1) {
        var _r332 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "mat-expansion-panel");
        core["ɵɵelementStart"](1, "mat-expansion-panel-header", 20);
        core["ɵɵelementStart"](2, "mat-checkbox", 21);
        core["ɵɵlistener"]("click", function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template_mat_checkbox_click_2_listener($event) { core["ɵɵrestoreView"](_r332); return $event.stopPropagation(); })("change", function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template_mat_checkbox_change_2_listener() { core["ɵɵrestoreView"](_r332); var uService_r328 = core["ɵɵnextContext"]().$implicit; var ctx_r333 = core["ɵɵnextContext"](2); return ctx_r333.uiAction.userUserviceSave(uService_r328); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](3, "div", 22);
        core["ɵɵtext"](4);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵtext"](5, " This the expansion 2 content ");
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var uService_r328 = core["ɵɵnextContext"]().$implicit;
        core["ɵɵadvance"](2);
        core["ɵɵproperty"]("checked", uService_r328.checked);
        core["ɵɵadvance"](2);
        core["ɵɵtextInterpolate"](uService_r328.name);
    } }
    function UserComponent_div_11_ng_container_22_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementContainerStart"](0);
        core["ɵɵtemplate"](1, UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template, 6, 2, "mat-expansion-panel", 13);
        core["ɵɵelementContainerEnd"]();
    } if (rf & 2) {
        var uService_r328 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("ngIf", uService_r328.availableForUsers);
    } }
    function UserComponent_div_11_Template(rf, ctx) { if (rf & 1) {
        var _r337 = core["ɵɵgetCurrentView"]();
        core["ɵɵelementStart"](0, "div", 8);
        core["ɵɵelementStart"](1, "div", 9);
        core["ɵɵelementStart"](2, "div", 10);
        core["ɵɵelementStart"](3, "form", 11);
        core["ɵɵelementStart"](4, "mat-form-field");
        core["ɵɵelement"](5, "input", 12);
        core["ɵɵtemplate"](6, UserComponent_div_11_mat_error_6_Template, 2, 0, "mat-error", 13);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](7, "mat-form-field");
        core["ɵɵelement"](8, "input", 14);
        core["ɵɵtemplate"](9, UserComponent_div_11_mat_error_9_Template, 2, 0, "mat-error", 13);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](10, "mat-form-field");
        core["ɵɵelement"](11, "input", 15);
        core["ɵɵtemplate"](12, UserComponent_div_11_mat_error_12_Template, 2, 0, "mat-error", 13);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](13, "mat-form-field");
        core["ɵɵelement"](14, "input", 16);
        core["ɵɵtemplate"](15, UserComponent_div_11_mat_error_15_Template, 2, 0, "mat-error", 13);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](16, "fof-core-fof-organizations-multi-select", 17);
        core["ɵɵlistener"]("selectedOrganizationsChange", function UserComponent_div_11_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_16_listener($event) { core["ɵɵrestoreView"](_r337); var ctx_r336 = core["ɵɵnextContext"](); return ctx_r336.uiAction.organisationMultiSelectedChange($event); });
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](17, "div", 10);
        core["ɵɵelementStart"](18, "h3");
        core["ɵɵtext"](19, "Acc\u00E8s du collaborateur aux mini-services");
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](20, "mat-list");
        core["ɵɵelementStart"](21, "mat-accordion", 18);
        core["ɵɵtemplate"](22, UserComponent_div_11_ng_container_22_Template, 2, 1, "ng-container", 19);
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r321 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("formGroup", ctx_r321.uiVar.form);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r321.uiVar.form.get("login").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r321.uiVar.form.get("email").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r321.uiVar.form.get("firstName").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r321.uiVar.form.get("lastName").invalid);
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("placeHolder", "Organisation de rattachement")("multiSelect", false)("selectedOrganisations", ctx_r321.uiVar.userOrganizationsDisplay);
        core["ɵɵadvance"](6);
        core["ɵɵproperty"]("ngForOf", ctx_r321.uiVar.uServicesAll);
    } }
    function UserComponent_div_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 23);
        core["ɵɵelement"](1, "mat-spinner", 24);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des utilisateurs et des authorisations...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    var UserComponent = /** @class */ (function () {
        function UserComponent(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, matDialog, fofErrorService, uservicesService) {
            var _this = this;
            this.fofPermissionService = fofPermissionService;
            this.activatedRoute = activatedRoute;
            this.formBuilder = formBuilder;
            this.fofNotificationService = fofNotificationService;
            this.fofDialogService = fofDialogService;
            this.router = router;
            this.matDialog = matDialog;
            this.fofErrorService = fofErrorService;
            this.uservicesService = uservicesService;
            // All private variables
            this.priVar = {
                userId: undefined,
                userOrganizationId: undefined,
                organizationAlreadyWithRoles: undefined
            };
            // All private functions
            this.privFunc = {
                userLoad: function () {
                    _this.uiVar.loadingUser = true;
                    Promise.all([
                        // Just need to ensure the uServices and user are both loaded the first time
                        _this.uiVar.uServicesAll ? null : _this.uservicesService.uServices.getAll().toPromise(),
                        _this.fofPermissionService.user.getWithUservicesById(_this.priVar.userId).toPromise()
                    ])
                        .then(function (result) {
                        var uServices = result[0];
                        var currentUsers = result[1];
                        if (uServices) {
                            _this.uiVar.uServicesAll = uServices;
                        }
                        if (currentUsers.userUServices && currentUsers.userUServices.length > 0) {
                            uServices.forEach(function (service) {
                                var result = currentUsers.userUServices.filter(function (userService) { return userService.uServiceId == service.id; });
                                if (result && result.length > 0) {
                                    service.checked = true;
                                    service.userUserviceId = result[0].id;
                                }
                            });
                        }
                        _this.privFunc.userRefresh(currentUsers);
                        _this.uiVar.form.patchValue(_this.uiVar.user);
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                        _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                    })
                        .finally(function () {
                        _this.uiVar.loadingUser = false;
                    });
                },
                userRefresh: function (currentUser) {
                    if (!currentUser) {
                        _this.fofNotificationService.error("Ce collaborateur n'existe pas");
                        _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        return;
                    }
                    _this.uiVar.user = currentUser;
                    if (currentUser.organizationId) {
                        _this.uiVar.userOrganizationsDisplay = [{ id: currentUser.organizationId }];
                        _this.priVar.userOrganizationId = currentUser.organizationId;
                    }
                }
            };
            // All variables shared with UI 
            this.uiVar = {
                title: 'Nouveau collaborateur',
                loadingUser: false,
                uServicesAll: undefined,
                userIsNew: false,
                user: undefined,
                userOrganizationsDisplay: undefined,
                form: this.formBuilder.group({
                    login: ['', [forms.Validators.maxLength(30)]],
                    email: ['', [forms.Validators.required, forms.Validators.email, forms.Validators.maxLength(60)]],
                    firstName: ['', [forms.Validators.maxLength(30)]],
                    lastName: ['', [forms.Validators.maxLength(30)]]
                })
            };
            // All actions shared with UI 
            this.uiAction = {
                userSave: function () {
                    var userToSave = _this.uiVar.form.value;
                    userToSave.organizationId = _this.priVar.userOrganizationId;
                    if (!_this.uiVar.form.valid) {
                        _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                        fofUtilsForm.validateAllFields(_this.uiVar.form);
                        return;
                    }
                    if (_this.uiVar.userIsNew) {
                        _this.fofPermissionService.user.create(userToSave)
                            .toPromise()
                            .then(function (newUser) {
                            _this.fofNotificationService.success('collaborateur sauvé', { mustDisappearAfter: 1000 });
                            _this.priVar.userId = newUser.id;
                            _this.uiVar.title = "Modification d'un collaborateur";
                            _this.uiVar.userIsNew = false;
                            _this.privFunc.userLoad();
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                    else {
                        userToSave.id = _this.uiVar.user.id;
                        _this.fofPermissionService.user.update(userToSave)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('collaborateur sauvé', { mustDisappearAfter: 1000 });
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                },
                userCancel: function () {
                    _this.privFunc.userLoad();
                },
                userDelete: function () {
                    _this.fofDialogService.openYesNo({
                        question: "Voulez vous vraiment supprimer le collaborateur ?"
                    }).then(function (yes) {
                        if (yes) {
                            _this.fofPermissionService.user.delete(_this.uiVar.user)
                                .toPromise()
                                .then(function (result) {
                                _this.fofNotificationService.success('collaborateur supprimé');
                                _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                            });
                        }
                    });
                },
                userUserviceSave: function (uService) {
                    if (uService.userUserviceId) {
                        _this.fofPermissionService.userUservice.delete(uService.userUserviceId)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.saveIsDone();
                            uService.checked = false;
                            uService.userUserviceId = null;
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                    else {
                        var userService = {
                            userId: _this.priVar.userId,
                            uServiceId: uService.id
                        };
                        _this.fofPermissionService.userUservice.create(userService)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.saveIsDone();
                            uService.checked = true;
                            uService.userUserviceId = result.id;
                        })
                            .catch(function (reason) {
                            _this.fofErrorService.errorManage(reason);
                        });
                    }
                },
                organisationMultiSelectedChange: function (organizations) {
                    if (organizations && organizations.length > 0) {
                        _this.priVar.userOrganizationId = organizations[0].id;
                    }
                    else {
                        _this.priVar.userOrganizationId = null;
                    }
                }
            };
        }
        // Angular events
        UserComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.activatedRoute.paramMap.subscribe(function (params) {
                var code = params.get('code');
                if (code) {
                    if (code.toLowerCase() == 'new') {
                        _this.uiVar.userIsNew = true;
                    }
                    else {
                        _this.priVar.userId = code;
                        _this.uiVar.title = "Modification d'un collaborateur";
                        _this.privFunc.userLoad();
                    }
                }
            });
        };
        UserComponent.ɵfac = function UserComponent_Factory(t) { return new (t || UserComponent)(core["ɵɵdirectiveInject"](FofPermissionService), core["ɵɵdirectiveInject"](router.ActivatedRoute), core["ɵɵdirectiveInject"](forms.FormBuilder), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](FofDialogService), core["ɵɵdirectiveInject"](router.Router), core["ɵɵdirectiveInject"](dialog.MatDialog), core["ɵɵdirectiveInject"](FofErrorService), core["ɵɵdirectiveInject"](UservicesService)); };
        UserComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: UserComponent, selectors: [["fof-core-user"]], decls: 13, vars: 3, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "detail fof-fade-in", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [1, "detail", "fof-fade-in"], [1, "row"], [1, "col-md-6"], [3, "formGroup"], ["matInput", "", "formControlName", "login", "placeholder", "login", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "type", "email", "formControlName", "email", "placeholder", "Email", "value", ""], ["matInput", "", "required", "", "type", "firstName", "formControlName", "firstName", "placeholder", "Pr\u00E9nom", "value", ""], ["matInput", "", "required", "", "type", "lastName", "formControlName", "lastName", "placeholder", "Nom de famille", "value", ""], [3, "placeHolder", "multiSelect", "selectedOrganisations", "selectedOrganizationsChange"], ["multi", "true"], [4, "ngFor", "ngForOf"], [1, "uservice-access-header"], [3, "checked", "click", "change"], [1, "check-label"], [1, "fof-loading"], ["diameter", "20"]], template: function UserComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-card", 0);
                core["ɵɵelementStart"](1, "h3");
                core["ɵɵtext"](2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "div", 1);
                core["ɵɵelementStart"](4, "button", 2);
                core["ɵɵlistener"]("click", function UserComponent_Template_button_click_4_listener() { return ctx.uiAction.userCancel(); });
                core["ɵɵtext"](5, "Annuler");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "button", 3);
                core["ɵɵlistener"]("click", function UserComponent_Template_button_click_6_listener() { return ctx.uiAction.userDelete(); });
                core["ɵɵtext"](7, "Supprimer");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](8, "button", 4);
                core["ɵɵlistener"]("click", function UserComponent_Template_button_click_8_listener() { return ctx.uiAction.userSave(); });
                core["ɵɵtext"](9, " Enregister");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](10, "div", 5);
                core["ɵɵtemplate"](11, UserComponent_div_11_Template, 23, 9, "div", 6);
                core["ɵɵtemplate"](12, UserComponent_div_12_Template, 4, 0, "div", 7);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.uiVar.title);
                core["ɵɵadvance"](9);
                core["ɵɵproperty"]("ngIf", !ctx.uiVar.loadingUser);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.loadingUser);
            } }, directives: [card.MatCard, button.MatButton, common.NgIf, forms["ɵangular_packages_forms_forms_y"], forms.NgControlStatusGroup, forms.FormGroupDirective, formField.MatFormField, input.MatInput, forms.DefaultValueAccessor, forms.NgControlStatus, forms.FormControlName, forms.RequiredValidator, FofOrganizationsMultiSelectComponent, list.MatList, expansion.MatAccordion, common.NgForOf, formField.MatError, expansion.MatExpansionPanel, expansion.MatExpansionPanelHeader, checkbox.MatCheckbox, progressSpinner.MatSpinner], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row;margin-bottom:15px}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .uservice-access-header[_ngcontent-%COMP%]   .check-label[_ngcontent-%COMP%]{padding-left:10px;margin-top:.1em}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
        return UserComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UserComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-user',
                    templateUrl: './user.component.html',
                    styleUrls: ['./user.component.scss']
                }]
        }], function () { return [{ type: FofPermissionService }, { type: router.ActivatedRoute }, { type: forms.FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: router.Router }, { type: dialog.MatDialog }, { type: FofErrorService }, { type: UservicesService }]; }, null); })();

    var routes$1 = [
        { path: '', canActivate: [FofAuthGuard],
            children: [
                { path: '',
                    children: [
                        { path: '', component: UsersComponent },
                        { path: ':code', component: UserComponent }
                    ]
                },
            ]
        }
    ];
    var UsersRoutingModule = /** @class */ (function () {
        function UsersRoutingModule() {
        }
        UsersRoutingModule.ɵmod = core["ɵɵdefineNgModule"]({ type: UsersRoutingModule });
        UsersRoutingModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function UsersRoutingModule_Factory(t) { return new (t || UsersRoutingModule)(); }, imports: [[router.RouterModule.forChild(routes$1)],
                router.RouterModule] });
        return UsersRoutingModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](UsersRoutingModule, { imports: [router.RouterModule], exports: [router.RouterModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UsersRoutingModule, [{
            type: core.NgModule,
            args: [{
                    imports: [router.RouterModule.forChild(routes$1)],
                    exports: [router.RouterModule]
                }]
        }], null, null); })();

    function UservicesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 15);
        core["ɵɵelement"](1, "mat-spinner", 16);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des \u03BCServices...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    function UservicesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 17);
        core["ɵɵtext"](1, "Nom technique");
        core["ɵɵelementEnd"]();
    } }
    function UservicesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 18);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r500 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r500.technicalName);
    } }
    function UservicesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 17);
        core["ɵɵtext"](1, "Nom");
        core["ɵɵelementEnd"]();
    } }
    function UservicesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 18);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r501 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r501.name);
    } }
    function UservicesComponent_th_16_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 17);
        core["ɵɵtext"](1, "Url interface");
        core["ɵɵelementEnd"]();
    } }
    function UservicesComponent_td_17_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 18);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r502 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r502.frontUrl);
    } }
    function UservicesComponent_th_19_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 17);
        core["ɵɵtext"](1, "Url backend");
        core["ɵɵelementEnd"]();
    } }
    function UservicesComponent_td_20_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 18);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r503 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r503.backUrl);
    } }
    function UservicesComponent_th_22_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "th", 17);
        core["ɵɵtext"](1, "Visible pour les utilisateurs");
        core["ɵɵelementEnd"]();
    } }
    function UservicesComponent_td_23_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "td", 18);
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var row_r504 = ctx.$implicit;
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate"](row_r504.availableForUsers);
    } }
    function UservicesComponent_tr_24_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 19);
    } }
    function UservicesComponent_tr_25_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "tr", 20);
    } if (rf & 2) {
        var row_r505 = ctx.$implicit;
        core["ɵɵproperty"]("routerLink", row_r505.id);
    } }
    var _c0$3 = function () { return [100]; };
    var UservicesComponent = /** @class */ (function () {
        function UservicesComponent(uservicesService, fofNotificationService, breakpointObserver) {
            this.uservicesService = uservicesService;
            this.fofNotificationService = fofNotificationService;
            this.breakpointObserver = breakpointObserver;
            // All private variables
            this.priVar = {
                breakpointObserverSub: undefined,
            };
            // All private functions
            this.privFunc = {};
            // All variables shared with UI 
            this.uiVar = {
                displayedColumns: ['technicalName', 'name', 'urlFront', 'urlBack', 'availableForUsers'],
                data: [],
                resultsLength: 0,
                pageSize: 100,
                isLoadingResults: true
            };
            // All actions shared with UI 
            this.uiAction = {};
        }
        // Angular events
        UservicesComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.priVar.breakpointObserverSub = this.breakpointObserver.observe(layout.Breakpoints.XSmall)
                .subscribe(function (state) {
                if (state.matches) {
                    // XSmall
                    _this.uiVar.displayedColumns = ['technicalName', 'name'];
                }
                else {
                    // > XSmall
                    _this.uiVar.displayedColumns = ['technicalName', 'name', 'frontUrl', 'backUrl', 'availableForUsers'];
                }
            });
        };
        UservicesComponent.prototype.ngAfterViewInit = function () {
            var _this = this;
            // If the user changes the sort order, reset back to the first page.
            this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
            rxjs.merge(this.sort.sortChange, this.paginator.page)
                .pipe(operators.startWith({}), operators.switchMap(function () {
                _this.uiVar.isLoadingResults = true;
                _this.uiVar.pageSize = _this.paginator.pageSize;
                return _this.uservicesService.uServices.getAll();
            }), operators.map(function (uServices) {
                _this.uiVar.isLoadingResults = false;
                _this.uiVar.resultsLength = uServices.length;
                return uServices;
            }), operators.catchError(function () {
                _this.uiVar.isLoadingResults = false;
                return rxjs.of([]);
            })).subscribe(function (data) { return _this.uiVar.data = data; });
        };
        UservicesComponent.prototype.ngOnDestroy = function () {
            if (this.priVar.breakpointObserverSub) {
                this.priVar.breakpointObserverSub.unsubscribe();
            }
        };
        UservicesComponent.ɵfac = function UservicesComponent_Factory(t) { return new (t || UservicesComponent)(core["ɵɵdirectiveInject"](UservicesService), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](layout.BreakpointObserver)); };
        UservicesComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: UservicesComponent, selectors: [["fof-core-uservices"]], viewQuery: function UservicesComponent_Query(rf, ctx) { if (rf & 1) {
                core["ɵɵviewQuery"](paginator.MatPaginator, true);
                core["ɵɵviewQuery"](sort.MatSort, true);
            } if (rf & 2) {
                var _t;
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.paginator = _t.first);
                core["ɵɵqueryRefresh"](_t = core["ɵɵloadQuery"]()) && (ctx.sort = _t.first);
            } }, decls: 27, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "technicalName", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "technicalName"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "name"], ["matColumnDef", "frontUrl"], ["matColumnDef", "backUrl"], ["matColumnDef", "availableForUsers"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UservicesComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-card", 0);
                core["ɵɵelementStart"](1, "h3");
                core["ɵɵtext"](2, "Mini-services");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "a", 1);
                core["ɵɵelementStart"](4, "span");
                core["ɵɵtext"](5, "Ajouter un mini-service");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "div", 2);
                core["ɵɵtemplate"](7, UservicesComponent_div_7_Template, 4, 0, "div", 3);
                core["ɵɵelementStart"](8, "table", 4);
                core["ɵɵelementContainerStart"](9, 5);
                core["ɵɵtemplate"](10, UservicesComponent_th_10_Template, 2, 0, "th", 6);
                core["ɵɵtemplate"](11, UservicesComponent_td_11_Template, 2, 1, "td", 7);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](12, 8);
                core["ɵɵtemplate"](13, UservicesComponent_th_13_Template, 2, 0, "th", 6);
                core["ɵɵtemplate"](14, UservicesComponent_td_14_Template, 2, 1, "td", 7);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](15, 9);
                core["ɵɵtemplate"](16, UservicesComponent_th_16_Template, 2, 0, "th", 6);
                core["ɵɵtemplate"](17, UservicesComponent_td_17_Template, 2, 1, "td", 7);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](18, 10);
                core["ɵɵtemplate"](19, UservicesComponent_th_19_Template, 2, 0, "th", 6);
                core["ɵɵtemplate"](20, UservicesComponent_td_20_Template, 2, 1, "td", 7);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵelementContainerStart"](21, 11);
                core["ɵɵtemplate"](22, UservicesComponent_th_22_Template, 2, 0, "th", 6);
                core["ɵɵtemplate"](23, UservicesComponent_td_23_Template, 2, 1, "td", 7);
                core["ɵɵelementContainerEnd"]();
                core["ɵɵtemplate"](24, UservicesComponent_tr_24_Template, 1, 0, "tr", 12);
                core["ɵɵtemplate"](25, UservicesComponent_tr_25_Template, 1, 1, "tr", 13);
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](26, "mat-paginator", 14);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](3);
                core["ɵɵproperty"]("routerLink", "new");
                core["ɵɵadvance"](4);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.isLoadingResults);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("dataSource", ctx.uiVar.data);
                core["ɵɵadvance"](16);
                core["ɵɵproperty"]("matHeaderRowDef", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("matRowDefColumns", ctx.uiVar.displayedColumns);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("length", ctx.uiVar.resultsLength)("pageSizeOptions", core["ɵɵpureFunction0"](8, _c0$3))("pageSize", ctx.uiVar.pageSize);
            } }, directives: [card.MatCard, button.MatAnchor, router.RouterLinkWithHref, common.NgIf, table.MatTable, sort.MatSort, table.MatColumnDef, table.MatHeaderCellDef, table.MatCellDef, table.MatHeaderRowDef, table.MatRowDef, paginator.MatPaginator, progressSpinner.MatSpinner, table.MatHeaderCell, table.MatCell, table.MatHeaderRow, table.MatRow, router.RouterLink], styles: [""] });
        return UservicesComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UservicesComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-uservices',
                    templateUrl: './uservices.component.html',
                    styleUrls: ['./uservices.component.scss']
                }]
        }], function () { return [{ type: UservicesService }, { type: FofNotificationService }, { type: layout.BreakpointObserver }]; }, { paginator: [{
                type: core.ViewChild,
                args: [paginator.MatPaginator]
            }], sort: [{
                type: core.ViewChild,
                args: [sort.MatSort]
            }] }); })();

    function UserviceComponent_div_11_mat_error_4_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le nom technique est obligatoire et doit \u00EAtre compos\u00E9 de 3 \u00E0 40 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function UserviceComponent_div_11_mat_error_7_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " Le nom est obligatoire et doit \u00EAtre compos\u00E9 de moins de 30 caract\u00E8res ");
        core["ɵɵelementEnd"]();
    } }
    function UserviceComponent_div_11_mat_error_10_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " L'url doit \u00EAtre valide ");
        core["ɵɵelementEnd"]();
    } }
    function UserviceComponent_div_11_mat_error_13_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1, " L'url doit \u00EAtre valide ");
        core["ɵɵelementEnd"]();
    } }
    function UserviceComponent_div_11_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 8);
        core["ɵɵelementStart"](1, "form", 9);
        core["ɵɵelementStart"](2, "mat-form-field");
        core["ɵɵelement"](3, "input", 10);
        core["ɵɵtemplate"](4, UserviceComponent_div_11_mat_error_4_Template, 2, 0, "mat-error", 11);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](5, "mat-form-field");
        core["ɵɵelement"](6, "input", 12);
        core["ɵɵtemplate"](7, UserviceComponent_div_11_mat_error_7_Template, 2, 0, "mat-error", 11);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](8, "mat-form-field");
        core["ɵɵelement"](9, "input", 13);
        core["ɵɵtemplate"](10, UserviceComponent_div_11_mat_error_10_Template, 2, 0, "mat-error", 11);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](11, "mat-form-field");
        core["ɵɵelement"](12, "input", 14);
        core["ɵɵtemplate"](13, UserviceComponent_div_11_mat_error_13_Template, 2, 0, "mat-error", 11);
        core["ɵɵelementEnd"]();
        core["ɵɵelementStart"](14, "mat-checkbox", 15);
        core["ɵɵtext"](15, "Accessible aux utilisateurs ?");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r506 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](1);
        core["ɵɵproperty"]("formGroup", ctx_r506.uiVar.form);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r506.uiVar.form.get("technicalName").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r506.uiVar.form.get("name").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r506.uiVar.form.get("backUrl").invalid);
        core["ɵɵadvance"](3);
        core["ɵɵproperty"]("ngIf", ctx_r506.uiVar.form.get("frontUrl").invalid);
    } }
    function UserviceComponent_div_12_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "div", 16);
        core["ɵɵelement"](1, "mat-spinner", 17);
        core["ɵɵelementStart"](2, "span");
        core["ɵɵtext"](3, "Chargements des uServices et des authorisations...");
        core["ɵɵelementEnd"]();
        core["ɵɵelementEnd"]();
    } }
    var UserviceComponent = /** @class */ (function () {
        function UserviceComponent(uUservicesService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, fofErrorService) {
            var _this = this;
            this.uUservicesService = uUservicesService;
            this.activatedRoute = activatedRoute;
            this.formBuilder = formBuilder;
            this.fofNotificationService = fofNotificationService;
            this.fofDialogService = fofDialogService;
            this.router = router;
            this.fofErrorService = fofErrorService;
            // All private variables
            this.priVar = {
                uServiceId: undefined,
                URL_REGEXP: /^[A-Za-z][A-Za-z\d.+-]*:\/*(?:\w+(?::\w+)?@)?[^\s/]+(?::\d+)?(?:\/[\w#!:.?+=&%@\-/]*)?$/
            };
            // All private functions
            this.privFunc = {
                uServiceLoad: function () {
                    _this.uiVar.loading = true;
                    _this.uUservicesService.uServices.getOne(_this.priVar.uServiceId)
                        .toPromise()
                        .then(function (uservice) {
                        _this.uiVar.uService = uservice;
                        _this.uiVar.form.patchValue(_this.uiVar.uService);
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                        _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                    })
                        .finally(function () {
                        _this.uiVar.loading = false;
                    });
                }
            };
            // All variables shared with UI 
            this.uiVar = {
                title: 'Nouveau rôle',
                permissionGroups: undefined,
                loading: false,
                uServiceIsNew: false,
                uService: undefined,
                form: this.formBuilder.group({
                    technicalName: ['', [forms.Validators.required, forms.Validators.minLength(3), forms.Validators.maxLength(30)]],
                    name: ['', [forms.Validators.required, forms.Validators.maxLength(30)]],
                    frontUrl: ['', [forms.Validators.pattern(this.priVar.URL_REGEXP)]],
                    backUrl: ['', [forms.Validators.pattern(this.priVar.URL_REGEXP)]],
                    availableForUsers: ['']
                })
            };
            // All actions shared with UI 
            this.uiAction = {
                uServiceSave: function () {
                    var uServiceToSave = _this.uiVar.form.value;
                    console.log('uServiceToSave', uServiceToSave);
                    if (!_this.uiVar.form.valid) {
                        _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                        fofUtilsForm.validateAllFields(_this.uiVar.form);
                        return;
                    }
                    if (_this.uiVar.uServiceIsNew) {
                        _this.uUservicesService.uServices.create(uServiceToSave)
                            .toPromise()
                            .then(function (newUservice) {
                            _this.fofNotificationService.success('Mini-service sauvé', { mustDisappearAfter: 1000 });
                            _this.priVar.uServiceId = newUservice.id;
                            _this.uiVar.title = 'Modification de mini-service';
                            _this.uiVar.uServiceIsNew = false;
                            _this.privFunc.uServiceLoad();
                        });
                    }
                    else {
                        uServiceToSave.id = _this.uiVar.uService.id;
                        _this.uUservicesService.uServices.update(uServiceToSave)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Mini-service sauvé', { mustDisappearAfter: 1000 });
                        });
                    }
                },
                uServiceCancel: function () {
                    _this.privFunc.uServiceLoad();
                },
                uServiceDelete: function () {
                    _this.fofDialogService.openYesNo({
                        question: 'Voulez vous vraiment supprimer le mini-service ?'
                    }).then(function (yes) {
                        if (yes) {
                            _this.uUservicesService.uServices.delete(_this.uiVar.uService)
                                .toPromise()
                                .then(function (result) {
                                _this.fofNotificationService.success('Mini-service supprimé');
                                _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                            });
                        }
                    });
                }
            };
        }
        // Angular events
        UserviceComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.activatedRoute.paramMap.subscribe(function (params) {
                var id = params.get('id');
                _this.priVar.uServiceId = id;
                if (id) {
                    if (id.toLowerCase() == 'new') {
                        _this.uiVar.uServiceIsNew = true;
                    }
                    else {
                        _this.uiVar.title = 'Modification de mini-service';
                        _this.privFunc.uServiceLoad();
                    }
                }
            });
        };
        UserviceComponent.ɵfac = function UserviceComponent_Factory(t) { return new (t || UserviceComponent)(core["ɵɵdirectiveInject"](UservicesService), core["ɵɵdirectiveInject"](router.ActivatedRoute), core["ɵɵdirectiveInject"](forms.FormBuilder), core["ɵɵdirectiveInject"](FofNotificationService), core["ɵɵdirectiveInject"](FofDialogService), core["ɵɵdirectiveInject"](router.Router), core["ɵɵdirectiveInject"](FofErrorService)); };
        UserviceComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: UserviceComponent, selectors: [["fof-core-uservice"]], decls: 13, vars: 3, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "fof-fade-in detail", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [1, "fof-fade-in", "detail"], [3, "formGroup"], ["matInput", "", "required", "", "formControlName", "technicalName", "placeholder", "Nom technique", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "formControlName", "name", "placeholder", "Nom", "value", ""], ["matInput", "", "type", "url", "formControlName", "backUrl", "placeholder", "Url du backend", "value", ""], ["matInput", "", "type", "url", "formControlName", "frontUrl", "placeholder", "Url du frontend", "value", ""], ["formControlName", "availableForUsers"], [1, "fof-loading"], ["diameter", "20"]], template: function UserviceComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-card", 0);
                core["ɵɵelementStart"](1, "h3");
                core["ɵɵtext"](2);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](3, "div", 1);
                core["ɵɵelementStart"](4, "button", 2);
                core["ɵɵlistener"]("click", function UserviceComponent_Template_button_click_4_listener() { return ctx.uiAction.uServiceCancel(); });
                core["ɵɵtext"](5, "Annuler");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "button", 3);
                core["ɵɵlistener"]("click", function UserviceComponent_Template_button_click_6_listener() { return ctx.uiAction.uServiceDelete(); });
                core["ɵɵtext"](7, "Supprimer");
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](8, "button", 4);
                core["ɵɵlistener"]("click", function UserviceComponent_Template_button_click_8_listener() { return ctx.uiAction.uServiceSave(); });
                core["ɵɵtext"](9, " Enregister");
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](10, "div", 5);
                core["ɵɵtemplate"](11, UserviceComponent_div_11_Template, 16, 5, "div", 6);
                core["ɵɵtemplate"](12, UserviceComponent_div_12_Template, 4, 0, "div", 7);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.uiVar.title);
                core["ɵɵadvance"](9);
                core["ɵɵproperty"]("ngIf", !ctx.uiVar.loading);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", ctx.uiVar.loading);
            } }, directives: [card.MatCard, button.MatButton, common.NgIf, forms["ɵangular_packages_forms_forms_y"], forms.NgControlStatusGroup, forms.FormGroupDirective, formField.MatFormField, input.MatInput, forms.DefaultValueAccessor, forms.RequiredValidator, forms.NgControlStatus, forms.FormControlName, checkbox.MatCheckbox, formField.MatError, progressSpinner.MatSpinner], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
        return UserviceComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UserviceComponent, [{
            type: core.Component,
            args: [{
                    selector: 'fof-core-uservice',
                    templateUrl: './uservice.component.html',
                    styleUrls: ['./uservice.component.scss']
                }]
        }], function () { return [{ type: UservicesService }, { type: router.ActivatedRoute }, { type: forms.FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: router.Router }, { type: FofErrorService }]; }, null); })();

    var routes$2 = [
        { path: '', canActivate: [FofAuthGuard],
            children: [
                { path: '',
                    children: [
                        { path: '', component: UservicesComponent },
                        { path: ':id', component: UserviceComponent }
                    ]
                },
            ]
        }
    ];
    var UservicesRoutingModule = /** @class */ (function () {
        function UservicesRoutingModule() {
        }
        UservicesRoutingModule.ɵmod = core["ɵɵdefineNgModule"]({ type: UservicesRoutingModule });
        UservicesRoutingModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function UservicesRoutingModule_Factory(t) { return new (t || UservicesRoutingModule)(); }, imports: [[router.RouterModule.forChild(routes$2)],
                router.RouterModule] });
        return UservicesRoutingModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](UservicesRoutingModule, { imports: [router.RouterModule], exports: [router.RouterModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UservicesRoutingModule, [{
            type: core.NgModule,
            args: [{
                    imports: [router.RouterModule.forChild(routes$2)],
                    exports: [router.RouterModule]
                }]
        }], null, null); })();

    var UservicesModule = /** @class */ (function () {
        function UservicesModule() {
        }
        UservicesModule.ɵmod = core["ɵɵdefineNgModule"]({ type: UservicesModule });
        UservicesModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function UservicesModule_Factory(t) { return new (t || UservicesModule)(); }, imports: [[
                    common.CommonModule,
                    MaterialModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule,
                    UservicesRoutingModule
                ]] });
        return UservicesModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](UservicesModule, { declarations: [UservicesComponent, UserviceComponent], imports: [common.CommonModule,
            MaterialModule,
            forms.FormsModule,
            forms.ReactiveFormsModule,
            UservicesRoutingModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UservicesModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [UservicesComponent, UserviceComponent],
                    imports: [
                        common.CommonModule,
                        MaterialModule,
                        forms.FormsModule,
                        forms.ReactiveFormsModule,
                        UservicesRoutingModule
                    ]
                }]
        }], null, null); })();

    var UsersModule = /** @class */ (function () {
        function UsersModule() {
        }
        UsersModule.ɵmod = core["ɵɵdefineNgModule"]({ type: UsersModule });
        UsersModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function UsersModule_Factory(t) { return new (t || UsersModule)(); }, imports: [[
                    common.CommonModule,
                    UsersRoutingModule,
                    MaterialModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule,
                    ComponentsModule,
                    UservicesModule
                ]] });
        return UsersModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](UsersModule, { declarations: [UsersComponent,
            UserComponent], imports: [common.CommonModule,
            UsersRoutingModule,
            MaterialModule,
            forms.FormsModule,
            forms.ReactiveFormsModule,
            ComponentsModule,
            UservicesModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](UsersModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [
                        UsersComponent,
                        UserComponent
                    ],
                    imports: [
                        common.CommonModule,
                        UsersRoutingModule,
                        MaterialModule,
                        forms.FormsModule,
                        forms.ReactiveFormsModule,
                        ComponentsModule,
                        UservicesModule
                    ]
                }]
        }], null, null); })();

    exports.AdminModule = AdminModule;
    exports.CORE_CONFIG = CORE_CONFIG;
    exports.ComponentsModule = ComponentsModule;
    exports.FoFAuthService = FoFAuthService;
    exports.FoFCoreModule = FoFCoreModule;
    exports.FofAuthGuard = FofAuthGuard;
    exports.FofCoreDialogYesNoComponent = FofCoreDialogYesNoComponent;
    exports.FofDialogService = FofDialogService;
    exports.FofErrorService = FofErrorService;
    exports.FofLocalstorageService = FofLocalstorageService;
    exports.FofNotificationService = FofNotificationService;
    exports.FofOrganizationsMultiSelectComponent = FofOrganizationsMultiSelectComponent;
    exports.FofOrganizationsTreeComponent = FofOrganizationsTreeComponent;
    exports.FofPermissionService = FofPermissionService;
    exports.HttpLoaderFactory = HttpLoaderFactory;
    exports.UsersModule = UsersModule;
    exports.UservicesModule = UservicesModule;
    exports.notificationComponent = notificationComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=fof-angular-core.umd.js.map

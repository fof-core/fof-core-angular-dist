import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FofRolesComponent } from './fof-roles/fof-roles.component';
import { FofRoleComponent } from './fof-role/fof-role.component';
import { FofUsersComponent } from './fof-users/fof-users.component';
import { FofUserComponent } from './fof-user/fof-user.component';
import { FofOrganizationsComponent } from './fof-organizations/fof-organizations.component';
import { FofAuthGuard } from '../core/auth.guard';
import { eCp } from '../permission/interfaces/permission.enum';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
const routes = [
    { path: 'admin', canActivate: [FofAuthGuard], data: { permissions: [eCp.coreAdminAccess] },
        children: [
            { path: '', component: FofRolesComponent },
            /**
             * if a child doesn't have permissions, it will get that ones
             * if it has some, it will override this ones
             * e.g. first child (path: '') get that ones
             */
            // { path: 'roles', data: {permissions: [eCp.roleRead]},
            { path: 'roles',
                children: [
                    { path: '', component: FofRolesComponent },
                    // { path: 'new', component: FofRoleComponent, data: {permissions: [eCp.roleCreate]} }
                    // { path: 'new', component: FofRoleComponent },
                    { path: ':code', component: FofRoleComponent }
                ]
            },
            { path: 'users',
                children: [
                    { path: '', component: FofUsersComponent },
                    { path: ':code', component: FofUserComponent }
                ]
            },
            { path: 'organizations', data: { permissions: [eCp.organizationRead] },
                children: [
                    { path: '', component: FofOrganizationsComponent }
                ]
            }
        ]
    }
];
export class AdminRoutingModule {
}
AdminRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: AdminRoutingModule });
AdminRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function AdminRoutingModule_Factory(t) { return new (t || AdminRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
        RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(AdminRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AdminRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRtaW4tcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9hZG1pbi9hZG1pbi1yb3V0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQTtBQUNoRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQTtBQUNoRSxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxpREFBaUQsQ0FBQTtBQUMzRixPQUFPLEVBQUUsWUFBWSxFQUFDLE1BQU0sb0JBQW9CLENBQUE7QUFDaEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLDBDQUEwQyxDQUFBOzs7QUFHOUQsTUFBTSxNQUFNLEdBQVc7SUFDckIsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsRUFBQztRQUN0RixRQUFRLEVBQUU7WUFDUixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFDO1lBQ3pDOzs7O2VBSUc7WUFDSCx3REFBd0Q7WUFDeEQsRUFBRSxJQUFJLEVBQUUsT0FBTztnQkFDYixRQUFRLEVBQUU7b0JBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRTtvQkFDMUMsc0ZBQXNGO29CQUN0RixnREFBZ0Q7b0JBQ2hELEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUU7aUJBQy9DO2FBQ0Y7WUFDRCxFQUFFLElBQUksRUFBRSxPQUFPO2dCQUNiLFFBQVEsRUFBRTtvQkFDUixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFFO29CQUMxQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFO2lCQUMvQzthQUNGO1lBQ0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRSxFQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFDO2dCQUNsRSxRQUFRLEVBQUU7b0JBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx5QkFBeUIsRUFBRTtpQkFDbkQ7YUFDRjtTQUNGO0tBQ0Y7Q0FDRixDQUFBO0FBTUQsTUFBTSxPQUFPLGtCQUFrQjs7c0RBQWxCLGtCQUFrQjttSEFBbEIsa0JBQWtCLGtCQUhwQixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsWUFBWTt3RkFFWCxrQkFBa0IsMENBRm5CLFlBQVk7a0RBRVgsa0JBQWtCO2NBSjlCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDeEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IFJvdXRlcywgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xyXG5pbXBvcnQgeyBGb2ZSb2xlc0NvbXBvbmVudCB9IGZyb20gJy4vZm9mLXJvbGVzL2ZvZi1yb2xlcy5jb21wb25lbnQnXHJcbmltcG9ydCB7IEZvZlJvbGVDb21wb25lbnQgfSBmcm9tICcuL2ZvZi1yb2xlL2ZvZi1yb2xlLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9mVXNlcnNDb21wb25lbnQgfSBmcm9tICcuL2ZvZi11c2Vycy9mb2YtdXNlcnMuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb2ZVc2VyQ29tcG9uZW50IH0gZnJvbSAnLi9mb2YtdXNlci9mb2YtdXNlci5jb21wb25lbnQnXHJcbmltcG9ydCB7IEZvZk9yZ2FuaXphdGlvbnNDb21wb25lbnQgfSBmcm9tICcuL2ZvZi1vcmdhbml6YXRpb25zL2ZvZi1vcmdhbml6YXRpb25zLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9mQXV0aEd1YXJkfSBmcm9tICcuLi9jb3JlL2F1dGguZ3VhcmQnXHJcbmltcG9ydCB7IGVDcCB9IGZyb20gJy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9uLmVudW0nXHJcblxyXG5cclxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXHJcbiAgeyBwYXRoOiAnYWRtaW4nLCBjYW5BY3RpdmF0ZTogW0ZvZkF1dGhHdWFyZF0sIGRhdGE6IHtwZXJtaXNzaW9uczogW2VDcC5jb3JlQWRtaW5BY2Nlc3NdfSwgICBcclxuICAgIGNoaWxkcmVuOiBbICAgICBcclxuICAgICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBGb2ZSb2xlc0NvbXBvbmVudH0sICAgICAgIFxyXG4gICAgICAvKiogXHJcbiAgICAgICAqIGlmIGEgY2hpbGQgZG9lc24ndCBoYXZlIHBlcm1pc3Npb25zLCBpdCB3aWxsIGdldCB0aGF0IG9uZXNcclxuICAgICAgICogaWYgaXQgaGFzIHNvbWUsIGl0IHdpbGwgb3ZlcnJpZGUgdGhpcyBvbmVzXHJcbiAgICAgICAqIGUuZy4gZmlyc3QgY2hpbGQgKHBhdGg6ICcnKSBnZXQgdGhhdCBvbmVzXHJcbiAgICAgICAqL1xyXG4gICAgICAvLyB7IHBhdGg6ICdyb2xlcycsIGRhdGE6IHtwZXJtaXNzaW9uczogW2VDcC5yb2xlUmVhZF19LFxyXG4gICAgICB7IHBhdGg6ICdyb2xlcycsXHJcbiAgICAgICAgY2hpbGRyZW46IFtcclxuICAgICAgICAgIHsgcGF0aDogJycsIGNvbXBvbmVudDogRm9mUm9sZXNDb21wb25lbnQgfSwgIFxyXG4gICAgICAgICAgLy8geyBwYXRoOiAnbmV3JywgY29tcG9uZW50OiBGb2ZSb2xlQ29tcG9uZW50LCBkYXRhOiB7cGVybWlzc2lvbnM6IFtlQ3Aucm9sZUNyZWF0ZV19IH1cclxuICAgICAgICAgIC8vIHsgcGF0aDogJ25ldycsIGNvbXBvbmVudDogRm9mUm9sZUNvbXBvbmVudCB9LFxyXG4gICAgICAgICAgeyBwYXRoOiAnOmNvZGUnLCBjb21wb25lbnQ6IEZvZlJvbGVDb21wb25lbnQgfVxyXG4gICAgICAgIF1cclxuICAgICAgfSxcclxuICAgICAgeyBwYXRoOiAndXNlcnMnLFxyXG4gICAgICAgIGNoaWxkcmVuOiBbXHJcbiAgICAgICAgICB7IHBhdGg6ICcnLCBjb21wb25lbnQ6IEZvZlVzZXJzQ29tcG9uZW50IH0sICAgICAgICAgICAgXHJcbiAgICAgICAgICB7IHBhdGg6ICc6Y29kZScsIGNvbXBvbmVudDogRm9mVXNlckNvbXBvbmVudCB9XHJcbiAgICAgICAgXVxyXG4gICAgICB9LFxyXG4gICAgICB7IHBhdGg6ICdvcmdhbml6YXRpb25zJywgZGF0YToge3Blcm1pc3Npb25zOiBbZUNwLm9yZ2FuaXphdGlvblJlYWRdfSxcclxuICAgICAgICBjaGlsZHJlbjogW1xyXG4gICAgICAgICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBGb2ZPcmdhbml6YXRpb25zQ29tcG9uZW50IH0gICAgICAgICAgXHJcbiAgICAgICAgXVxyXG4gICAgICB9ICAgICBcclxuICAgIF1cclxuICB9XHJcbl1cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1JvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXSxcclxuICBleHBvcnRzOiBbUm91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWRtaW5Sb3V0aW5nTW9kdWxlIHsgfVxyXG4iXX0=
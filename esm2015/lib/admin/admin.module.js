import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components/components.module';
import { AdminRoutingModule } from './admin-routing.module';
import { FofPermissionModule } from '../permission/permission.module';
import { FofRolesComponent } from './fof-roles/fof-roles.component';
import { FofRoleComponent } from './fof-role/fof-role.component';
import { MaterialModule } from '../core/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FofUsersComponent } from './fof-users/fof-users.component';
import { FofUserComponent } from './fof-user/fof-user.component';
import { FofEntityFooterComponent } from './fof-entity-footer/fof-entity-footer.component';
import { FofOrganizationsComponent } from './fof-organizations/fof-organizations.component';
import { FofUserRolesSelectComponent } from './fof-user-roles-select/fof-user-roles-select.component';
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from '../shared/shared.module';
import * as i0 from "@angular/core";
export class AdminModule {
}
AdminModule.ɵmod = i0.ɵɵdefineNgModule({ type: AdminModule });
AdminModule.ɵinj = i0.ɵɵdefineInjector({ factory: function AdminModule_Factory(t) { return new (t || AdminModule)(); }, imports: [[
            CommonModule,
            SharedModule,
            AdminRoutingModule,
            ComponentsModule,
            FofPermissionModule,
            MaterialModule,
            FormsModule,
            ReactiveFormsModule,
            TranslateModule //.forChild()
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(AdminModule, { declarations: [FofRolesComponent,
        FofRoleComponent,
        FofUsersComponent,
        FofUserComponent,
        FofEntityFooterComponent,
        FofOrganizationsComponent,
        FofUserRolesSelectComponent], imports: [CommonModule,
        SharedModule,
        AdminRoutingModule,
        ComponentsModule,
        FofPermissionModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule //.forChild()
    ] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AdminModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    FofRolesComponent,
                    FofRoleComponent,
                    FofUsersComponent,
                    FofUserComponent,
                    FofEntityFooterComponent,
                    FofOrganizationsComponent,
                    FofUserRolesSelectComponent
                ],
                imports: [
                    CommonModule,
                    SharedModule,
                    AdminRoutingModule,
                    ComponentsModule,
                    FofPermissionModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    TranslateModule //.forChild()
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRtaW4ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vYWRtaW4ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGlDQUFpQyxDQUFBO0FBQ2xFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBQzNELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFBO0FBQ3JFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFBO0FBQ25FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLCtCQUErQixDQUFBO0FBQ2hFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUE7QUFDakUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUE7QUFDbkUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sK0JBQStCLENBQUE7QUFDaEUsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0saURBQWlELENBQUE7QUFDMUYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDNUYsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0seURBQXlELENBQUE7QUFDckcsT0FBTyxFQUFFLGVBQWUsRUFBQyxNQUFNLHFCQUFxQixDQUFBO0FBQ3BELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTs7QUF5QnRELE1BQU0sT0FBTyxXQUFXOzsrQ0FBWCxXQUFXO3FHQUFYLFdBQVcsa0JBWmI7WUFDUCxZQUFZO1lBQ1osWUFBWTtZQUNaLGtCQUFrQjtZQUNsQixnQkFBZ0I7WUFDaEIsbUJBQW1CO1lBQ25CLGNBQWM7WUFDZCxXQUFXO1lBQ1gsbUJBQW1CO1lBQ25CLGVBQWUsQ0FBQyxhQUFhO1NBQzlCO3dGQUVVLFdBQVcsbUJBcEJwQixpQkFBaUI7UUFDakIsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtRQUNqQixnQkFBZ0I7UUFDaEIsd0JBQXdCO1FBQ3hCLHlCQUF5QjtRQUN6QiwyQkFBMkIsYUFHM0IsWUFBWTtRQUNaLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIsZ0JBQWdCO1FBQ2hCLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsV0FBVztRQUNYLG1CQUFtQjtRQUNuQixlQUFlLENBQUMsYUFBYTs7a0RBR3BCLFdBQVc7Y0F0QnZCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUU7b0JBQ1osaUJBQWlCO29CQUNqQixnQkFBZ0I7b0JBQ2hCLGlCQUFpQjtvQkFDakIsZ0JBQWdCO29CQUNoQix3QkFBd0I7b0JBQ3hCLHlCQUF5QjtvQkFDekIsMkJBQTJCO2lCQUM1QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixZQUFZO29CQUNaLGtCQUFrQjtvQkFDbEIsZ0JBQWdCO29CQUNoQixtQkFBbUI7b0JBQ25CLGNBQWM7b0JBQ2QsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGVBQWUsQ0FBQyxhQUFhO2lCQUM5QjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXHJcbmltcG9ydCB7IENvbXBvbmVudHNNb2R1bGUgfSBmcm9tICcuLi9jb21wb25lbnRzL2NvbXBvbmVudHMubW9kdWxlJ1xyXG5pbXBvcnQgeyBBZG1pblJvdXRpbmdNb2R1bGUgfSBmcm9tICcuL2FkbWluLXJvdXRpbmcubW9kdWxlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uTW9kdWxlIH0gZnJvbSAnLi4vcGVybWlzc2lvbi9wZXJtaXNzaW9uLm1vZHVsZSdcclxuaW1wb3J0IHsgRm9mUm9sZXNDb21wb25lbnQgfSBmcm9tICcuL2ZvZi1yb2xlcy9mb2Ytcm9sZXMuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb2ZSb2xlQ29tcG9uZW50IH0gZnJvbSAnLi9mb2Ytcm9sZS9mb2Ytcm9sZS5jb21wb25lbnQnXHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vY29yZS9tYXRlcmlhbC5tb2R1bGUnXHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnXHJcbmltcG9ydCB7IEZvZlVzZXJzQ29tcG9uZW50IH0gZnJvbSAnLi9mb2YtdXNlcnMvZm9mLXVzZXJzLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9mVXNlckNvbXBvbmVudCB9IGZyb20gJy4vZm9mLXVzZXIvZm9mLXVzZXIuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb2ZFbnRpdHlGb290ZXJDb21wb25lbnQgfSBmcm9tICcuL2ZvZi1lbnRpdHktZm9vdGVyL2ZvZi1lbnRpdHktZm9vdGVyLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9mT3JnYW5pemF0aW9uc0NvbXBvbmVudCB9IGZyb20gJy4vZm9mLW9yZ2FuaXphdGlvbnMvZm9mLW9yZ2FuaXphdGlvbnMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9mVXNlclJvbGVzU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnLi9mb2YtdXNlci1yb2xlcy1zZWxlY3QvZm9mLXVzZXItcm9sZXMtc2VsZWN0LmNvbXBvbmVudCdcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiXHJcbmltcG9ydCB7IFNoYXJlZE1vZHVsZSB9IGZyb20gJy4uL3NoYXJlZC9zaGFyZWQubW9kdWxlJ1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbICAgIFxyXG4gICAgRm9mUm9sZXNDb21wb25lbnQsXHJcbiAgICBGb2ZSb2xlQ29tcG9uZW50LFxyXG4gICAgRm9mVXNlcnNDb21wb25lbnQsXHJcbiAgICBGb2ZVc2VyQ29tcG9uZW50LFxyXG4gICAgRm9mRW50aXR5Rm9vdGVyQ29tcG9uZW50LFxyXG4gICAgRm9mT3JnYW5pemF0aW9uc0NvbXBvbmVudCxcclxuICAgIEZvZlVzZXJSb2xlc1NlbGVjdENvbXBvbmVudFxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgU2hhcmVkTW9kdWxlLFxyXG4gICAgQWRtaW5Sb3V0aW5nTW9kdWxlLFxyXG4gICAgQ29tcG9uZW50c01vZHVsZSxcclxuICAgIEZvZlBlcm1pc3Npb25Nb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgIFRyYW5zbGF0ZU1vZHVsZSAvLy5mb3JDaGlsZCgpXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWRtaW5Nb2R1bGUgeyB9XHJcbiJdfQ==
import { Component, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function FofEntityFooterComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "date");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r118 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2(" Derni\u00E8re modification le ", i0.ɵɵpipeBind2(2, 2, ctx_r118.entityBase._updatedDate, "short"), " par ", ctx_r118.entityBase._updatedByName, " ");
} }
function FofEntityFooterComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 3);
    i0.ɵɵelementStart(1, "div", 4);
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "date");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, FofEntityFooterComponent_div_1_div_4_Template, 3, 5, "div", 5);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r115 = i0.ɵɵnextContext();
    const _r116 = i0.ɵɵreference(3);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate2(" Cr\u00E9ation le ", i0.ɵɵpipeBind2(3, 4, ctx_r115.entityBase._createdDate, "short"), " par ", ctx_r115.entityBase._createdByName, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r115.entityBase._updatedByName)("ngIfElse", _r116);
} }
function FofEntityFooterComponent_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵtext(1, " Aucune modification depuis la cr\u00E9ation ");
    i0.ɵɵelementEnd();
} }
export class FofEntityFooterComponent {
    constructor() {
    }
    ngOnInit() {
    }
}
FofEntityFooterComponent.ɵfac = function FofEntityFooterComponent_Factory(t) { return new (t || FofEntityFooterComponent)(); };
FofEntityFooterComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofEntityFooterComponent, selectors: [["fof-entity-footer"]], inputs: { entityBase: "entityBase" }, decls: 4, vars: 1, consts: [[1, "custom-card"], ["class", "row", 4, "ngIf"], ["elseBlock", ""], [1, "row"], [1, "col-md-6"], ["class", "col-md-6", 4, "ngIf", "ngIfElse"]], template: function FofEntityFooterComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵtemplate(1, FofEntityFooterComponent_div_1_Template, 5, 7, "div", 1);
        i0.ɵɵtemplate(2, FofEntityFooterComponent_ng_template_2_Template, 2, 0, "ng-template", null, 2, i0.ɵɵtemplateRefExtractor);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.entityBase);
    } }, directives: [i1.NgIf], pipes: [i1.DatePipe], styles: [".custom-card[_ngcontent-%COMP%]{font-size:smaller;padding-top:15px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofEntityFooterComponent, [{
        type: Component,
        args: [{
                selector: 'fof-entity-footer',
                templateUrl: './fof-entity-footer.component.html',
                styleUrls: ['./fof-entity-footer.component.scss']
            }]
    }], function () { return []; }, { entityBase: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWVudGl0eS1mb290ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vZm9mLWVudGl0eS1mb290ZXIvZm9mLWVudGl0eS1mb290ZXIuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi1lbnRpdHktZm9vdGVyL2ZvZi1lbnRpdHktZm9vdGVyLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFBOzs7O0lDTy9ELDhCQUNFO0lBQ0EsWUFFRjs7SUFBQSxpQkFBTTs7O0lBRkosZUFFRjtJQUZFLDJLQUVGOzs7SUFWRiw4QkFDRTtJQUFBLDhCQUNFO0lBQ0EsWUFFRjs7SUFBQSxpQkFBTTtJQUNOLCtFQUNFO0lBSUosaUJBQU07Ozs7SUFSRixlQUVGO0lBRkUsOEpBRUY7SUFDc0IsZUFBaUQ7SUFBakQseURBQWlELG1CQUFBOzs7SUFPdkUsOEJBQ0U7SUFBQSw2REFDRjtJQUFBLGlCQUFNOztBRFJWLE1BQU0sT0FBTyx3QkFBd0I7SUFHbkM7SUFBZ0IsQ0FBQztJQUVqQixRQUFRO0lBR1IsQ0FBQzs7Z0dBUlUsd0JBQXdCOzZEQUF4Qix3QkFBd0I7UUNSckMsOEJBQ0U7UUFBQSx5RUFDRTtRQVdGLDBIQUNFO1FBSUosaUJBQU07O1FBakJhLGVBQWtCO1FBQWxCLHFDQUFrQjs7a0RET3hCLHdCQUF3QjtjQUxwQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsV0FBVyxFQUFFLG9DQUFvQztnQkFDakQsU0FBUyxFQUFFLENBQUMsb0NBQW9DLENBQUM7YUFDbEQ7O2tCQUVFLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IGlGb2ZFbnRpdHlDb3JlIH0gZnJvbSAnLi4vLi4vY29yZS9lbnRpdHlDb3JlLmludGVyZmFjZSdcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZm9mLWVudGl0eS1mb290ZXInLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mb2YtZW50aXR5LWZvb3Rlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9mLWVudGl0eS1mb290ZXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9mRW50aXR5Rm9vdGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBlbnRpdHlCYXNlOiBpRm9mRW50aXR5Q29yZVxyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIFxyXG4gICAgXHJcbiAgfVxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJjdXN0b20tY2FyZFwiPlxyXG4gIDxkaXYgY2xhc3M9XCJyb3dcIiAqbmdJZj1cImVudGl0eUJhc2VcIj5cclxuICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgICA8IS0tIENyw6lhdGlvbiBsZSB7eyBlbnRpdHlCYXNlLl9jcmVhdGVkRGF0ZSB8IGRhdGU6J0VFRUUgZCBNTU1NIHkgw6AgSEg6bW06c3MnIH19ICAtLT5cclxuICAgICAgQ3LDqWF0aW9uIGxlIHt7IGVudGl0eUJhc2UuX2NyZWF0ZWREYXRlIHwgZGF0ZTonc2hvcnQnIH19IFxyXG4gICAgICBwYXIge3sgdGhpcy5lbnRpdHlCYXNlLl9jcmVhdGVkQnlOYW1lIH19XHJcbiAgICA8L2Rpdj5cclxuICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiICpuZ0lmPVwiZW50aXR5QmFzZS5fdXBkYXRlZEJ5TmFtZTsgZWxzZSBlbHNlQmxvY2tcIj4gICAgICBcclxuICAgICAgPCEtLSBEZXJuacOocmUgbW9kaWZpY2F0aW9uIGxlIHt7IGVudGl0eUJhc2UuX3VwZGF0ZWREYXRlIHwgZGF0ZTonRUVFRSBkIE1NTU0geSDDoCBISDptbTpzcycgfX0gIC0tPlxyXG4gICAgICBEZXJuacOocmUgbW9kaWZpY2F0aW9uIGxlIHt7IGVudGl0eUJhc2UuX3VwZGF0ZWREYXRlIHwgZGF0ZTonc2hvcnQnIH19IFxyXG4gICAgICBwYXIge3sgdGhpcy5lbnRpdHlCYXNlLl91cGRhdGVkQnlOYW1lIH19XHJcbiAgICA8L2Rpdj4gICAgICAgXHJcbiAgPC9kaXY+XHJcbiAgPG5nLXRlbXBsYXRlICNlbHNlQmxvY2s+XHJcbiAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj4gICAgICBcclxuICAgICAgQXVjdW5lIG1vZGlmaWNhdGlvbiBkZXB1aXMgbGEgY3LDqWF0aW9uXHJcbiAgICA8L2Rpdj5cclxuICA8L25nLXRlbXBsYXRlPiAgICBcclxuPC9kaXY+XHJcbiJdfQ==
import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { fofUtilsForm } from '../../core/fof-utils';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "@angular/forms";
import * as i3 from "../../core/notification/notification.service";
import * as i4 from "../../core/fof-dialog.service";
import * as i5 from "../../core/fof-error.service";
import * as i6 from "@angular/material/card";
import * as i7 from "../../components/fof-organizations-tree/fof-organizations-tree.component";
import * as i8 from "@angular/common";
import * as i9 from "@angular/material/button";
import * as i10 from "@angular/material/progress-spinner";
import * as i11 from "@angular/material/form-field";
import * as i12 from "@angular/material/input";
function FofOrganizationsComponent_button_9_Template(rf, ctx) { if (rf & 1) {
    const _r106 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 11);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r106); const ctx_r105 = i0.ɵɵnextContext(); return ctx_r105.uiAction.organisationCancel(); });
    i0.ɵɵtext(1, "Annuler");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_button_10_Template(rf, ctx) { if (rf & 1) {
    const _r108 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 12);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_button_10_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r108); const ctx_r107 = i0.ɵɵnextContext(); return ctx_r107.uiAction.organisationDelete(); });
    i0.ɵɵtext(1, "Supprimer");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_button_11_Template(rf, ctx) { if (rf & 1) {
    const _r110 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_button_11_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r110); const ctx_r109 = i0.ɵɵnextContext(); return ctx_r109.uiAction.organisationSave(); });
    i0.ɵɵtext(1, " Enregistrer");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 14);
    i0.ɵɵelement(1, "mat-spinner", 15);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des organisations...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_button_5_Template(rf, ctx) { if (rf & 1) {
    const _r114 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 22);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_div_13_button_5_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r114); const ctx_r113 = i0.ɵɵnextContext(2); return ctx_r113.uiAction.organisationAdd(); });
    i0.ɵɵtext(1, "Ajouter une organisation enfant");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le nom ne doit pas \u00EAtre nul et faire plus de 100 cararct\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵelementStart(1, "div", 17);
    i0.ɵɵelementStart(2, "h4");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 5);
    i0.ɵɵtemplate(5, FofOrganizationsComponent_div_13_button_5_Template, 2, 0, "button", 18);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "form", 19);
    i0.ɵɵelementStart(7, "mat-form-field");
    i0.ɵɵelement(8, "input", 20);
    i0.ɵɵtemplate(9, FofOrganizationsComponent_div_13_mat_error_9_Template, 2, 0, "mat-error", 21);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r104 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r104.uiVar.actionTitle);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r104.uiVar.organizationIsNew);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("formGroup", ctx_r104.uiVar.form);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r104.uiVar.form.get("name").invalid);
} }
export class FofOrganizationsComponent {
    constructor(fofPermissionService, formBuilder, fofNotificationService, fofDialogService, fofErrorService) {
        this.fofPermissionService = fofPermissionService;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            currentNode: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            loading: false,
            organizationIsNew: false,
            nodeForm: false,
            form: this.formBuilder.group({
                name: ['', [Validators.required, Validators.maxLength(100)]],
                description: ['', [Validators.maxLength(200)]],
            }),
            actionTitle: `Modification de l'organisation`,
            mainTitle: `Gérer l'organisation`,
            nodeChanged: undefined,
            nodeToDelete: undefined
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationAdd: () => {
                if (!this.priVar.currentNode) {
                    return;
                }
                this.uiVar.form.reset();
                this.uiVar.organizationIsNew = true;
                this.uiVar.actionTitle = `Ajouter une organisation enfant`;
            },
            organisationDelete: () => {
                const nodeToDelete = this.priVar.currentNode;
                this.fofDialogService.openYesNo({
                    title: "Supprimer une organisation",
                    question: `Voulez vous vraiment supprimer 
          ${nodeToDelete.name} ?`
                }).then(yes => {
                    if (yes) {
                        this.fofPermissionService.organization.delete(nodeToDelete)
                            .toPromise()
                            .then(result => {
                            this.uiVar.nodeToDelete = nodeToDelete;
                        })
                            .catch(reason => {
                            if (reason.isConstraintError) {
                                this.fofNotificationService.info(`Vous ne pouvez pas supprimer l'organisation !<br>
                <small>Vous devez supprimer tous les objets rattachés à l'organisation ou contacter un admninistrateur</small>`, { mustDisappearAfter: -1 });
                            }
                            else {
                                this.fofErrorService.errorManage(reason);
                            }
                        });
                    }
                });
                this.uiVar.organizationIsNew = false;
            },
            organisationSave: () => {
                const nodeToSave = this.uiVar.form.value;
                if (!this.uiVar.form.valid) {
                    this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(this.uiVar.form);
                    return;
                }
                // New organization
                if (this.uiVar.organizationIsNew) {
                    nodeToSave.parentId = this.priVar.currentNode.id;
                    this.fofPermissionService.organization.create({
                        id: nodeToSave.id,
                        name: nodeToSave.name
                    })
                        .toPromise()
                        .then((nodeResult) => {
                        this.fofNotificationService.saveIsDone();
                        this.priVar.currentNode.children.push(nodeResult);
                        // deep copy for pushing the tree component to refresh the object
                        this.uiVar.nodeChanged = JSON.parse(JSON.stringify(this.priVar.currentNode));
                        this.uiAction.selectedOrganizationsChange(this.priVar.currentNode);
                        this.uiVar.organizationIsNew = false;
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    // Update an organization
                    this.priVar.currentNode.name = nodeToSave.name;
                    this.fofPermissionService.organization.update({
                        id: this.priVar.currentNode.id,
                        name: this.priVar.currentNode.name
                    })
                        .toPromise()
                        .then((nodeResult) => {
                        this.uiVar.nodeChanged = nodeResult;
                        this.fofNotificationService.saveIsDone();
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
            },
            organisationCancel: () => {
                this.uiVar.actionTitle = `Modification de l'organisation`;
                this.uiVar.nodeForm = false;
                this.uiVar.organizationIsNew = false;
            },
            selectedOrganizationsChange: (node) => {
                if (node) {
                    this.uiVar.nodeForm = node.checked;
                    this.uiVar.form.patchValue(node);
                }
                else {
                    this.uiVar.nodeForm = false;
                    this.uiVar.form.reset();
                }
                if (node && node.checked) {
                    this.priVar.currentNode = node;
                    this.uiVar.mainTitle = node.name;
                }
                else {
                    this.priVar.currentNode = null;
                    this.uiVar.mainTitle = `Gérer l'organisation`;
                }
            }
        };
    }
    // Angular events
    ngOnInit() {
    }
}
FofOrganizationsComponent.ɵfac = function FofOrganizationsComponent_Factory(t) { return new (t || FofOrganizationsComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FormBuilder), i0.ɵɵdirectiveInject(i3.FofNotificationService), i0.ɵɵdirectiveInject(i4.FofDialogService), i0.ɵɵdirectiveInject(i5.FofErrorService)); };
FofOrganizationsComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofOrganizationsComponent, selectors: [["fof-core-fof-organizations"]], decls: 14, vars: 8, consts: [[1, "row", "main"], [1, "col-md-6"], [1, "card-tree"], [3, "nodeChanged", "nodeToDelete", "selectedOrganizationsChange"], [1, "fof-header", "card-detail"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "warn", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "accent", 3, "click", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], ["class", "fof-fade-in card-detail", 4, "ngIf"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in", "card-detail"], [1, "fof-header"], ["mat-stroked-button", "", "class", "child-add", "color", "primary", 3, "click", 4, "ngIf"], [3, "formGroup"], ["matInput", "", "required", "", "type", "name", "formControlName", "name", "placeholder", "Nom de l'organisation", "value", ""], [4, "ngIf"], ["mat-stroked-button", "", "color", "primary", 1, "child-add", 3, "click"]], template: function FofOrganizationsComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "mat-card", 2);
        i0.ɵɵelementStart(3, "fof-organizations-tree", 3);
        i0.ɵɵlistener("selectedOrganizationsChange", function FofOrganizationsComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "div", 1);
        i0.ɵɵelementStart(5, "mat-card", 4);
        i0.ɵɵelementStart(6, "h3");
        i0.ɵɵtext(7);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "div", 5);
        i0.ɵɵtemplate(9, FofOrganizationsComponent_button_9_Template, 2, 0, "button", 6);
        i0.ɵɵtemplate(10, FofOrganizationsComponent_button_10_Template, 2, 0, "button", 7);
        i0.ɵɵtemplate(11, FofOrganizationsComponent_button_11_Template, 2, 0, "button", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(12, FofOrganizationsComponent_div_12_Template, 4, 0, "div", 9);
        i0.ɵɵtemplate(13, FofOrganizationsComponent_div_13_Template, 10, 4, "div", 10);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("nodeChanged", ctx.uiVar.nodeChanged)("nodeToDelete", ctx.uiVar.nodeToDelete);
        i0.ɵɵadvance(4);
        i0.ɵɵtextInterpolate(ctx.uiVar.mainTitle);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.uiVar.nodeForm);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.nodeForm && !ctx.uiVar.organizationIsNew);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.nodeForm);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.uiVar.loading && ctx.uiVar.nodeForm);
    } }, directives: [i6.MatCard, i7.FofOrganizationsTreeComponent, i8.NgIf, i9.MatButton, i10.MatSpinner, i2.ɵangular_packages_forms_forms_y, i2.NgControlStatusGroup, i2.FormGroupDirective, i11.MatFormField, i12.MatInput, i2.DefaultValueAccessor, i2.RequiredValidator, i2.NgControlStatus, i2.FormControlName, i11.MatError], styles: [".row.main[_ngcontent-%COMP%], .row.main[_ngcontent-%COMP%]   .card-tree[_ngcontent-%COMP%]{height:100%}.row.main[_ngcontent-%COMP%]   .card-detail[_ngcontent-%COMP%]{position:-webkit-sticky;position:sticky;top:0}.mat-form-field[_ngcontent-%COMP%]{width:100%}.child-add[_ngcontent-%COMP%]{margin-bottom:15px}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofOrganizationsComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-organizations',
                templateUrl: './fof-organizations.component.html',
                styleUrls: ['./fof-organizations.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FormBuilder }, { type: i3.FofNotificationService }, { type: i4.FofDialogService }, { type: i5.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLW9yZ2FuaXphdGlvbnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vZm9mLW9yZ2FuaXphdGlvbnMvZm9mLW9yZ2FuaXphdGlvbnMuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi1vcmdhbml6YXRpb25zL2ZvZi1vcmdhbml6YXRpb25zLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUE7QUFLakQsT0FBTyxFQUFlLFVBQVUsRUFBRyxNQUFNLGdCQUFnQixDQUFBO0FBR3pELE9BQU8sRUFBRSxZQUFZLEVBQVksTUFBTSxzQkFBc0IsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7OztJQ0tyRCxrQ0FDMEM7SUFBeEMsNktBQVMsc0NBQTZCLElBQUM7SUFBQyx1QkFBTztJQUFBLGlCQUFTOzs7O0lBQzFELGtDQUMwQztJQUF4Qyw4S0FBUyxzQ0FBNkIsSUFBQztJQUFDLHlCQUFTO0lBQUEsaUJBQVM7Ozs7SUFDNUQsa0NBRUU7SUFEQSw4S0FBUyxvQ0FBMkIsSUFBQztJQUNyQyw0QkFBVztJQUFBLGlCQUFTOzs7SUFJMUIsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGdEQUFnQztJQUFBLGlCQUFPO0lBQ3ZGLGlCQUFNOzs7O0lBT0Esa0NBRXVDO0lBQXJDLHFMQUFTLG1DQUEwQixJQUFDO0lBQUMsK0NBQStCO0lBQUEsaUJBQVM7OztJQWUvRSxpQ0FDRTtJQUFBLHdGQUNGO0lBQUEsaUJBQVk7OztJQXhCbEIsK0JBRUU7SUFBQSwrQkFDRTtJQUFBLDBCQUFJO0lBQUEsWUFBdUI7SUFBQSxpQkFBSztJQUNoQyw4QkFDRTtJQUFBLHdGQUV1QztJQUN6QyxpQkFBTTtJQUNSLGlCQUFNO0lBT04sZ0NBQ0U7SUFBQSxzQ0FDRTtJQUFBLDRCQUlBO0lBQUEsOEZBQ0U7SUFFSixpQkFBaUI7SUFDbkIsaUJBQU87SUFDVCxpQkFBTTs7O0lBeEJFLGVBQXVCO0lBQXZCLGdEQUF1QjtJQUVHLGVBQWdDO0lBQWhDLHdEQUFnQztJQVcxRCxlQUF3QjtJQUF4QiwrQ0FBd0I7SUFNZixlQUFzQztJQUF0Qyw4REFBc0M7O0FEM0IzRCxNQUFNLE9BQU8seUJBQXlCO0lBRXBDLFlBQ1Usb0JBQTBDLEVBQzFDLFdBQXdCLEVBQ3hCLHNCQUE4QyxFQUM5QyxnQkFBa0MsRUFDbEMsZUFBZ0M7UUFKaEMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUMxQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBSzFDLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixXQUFXLEVBQWlCLFNBQVM7U0FDdEMsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUcsRUFDbEIsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixPQUFPLEVBQUUsS0FBSztZQUNkLGlCQUFpQixFQUFFLEtBQUs7WUFDeEIsUUFBUSxFQUFFLEtBQUs7WUFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM1RCxXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDL0MsQ0FBQztZQUNGLFdBQVcsRUFBRSxnQ0FBZ0M7WUFDN0MsU0FBUyxFQUFFLHNCQUFzQjtZQUNqQyxXQUFXLEVBQWlCLFNBQVM7WUFDckMsWUFBWSxFQUFpQixTQUFTO1NBQ3ZDLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHO1lBQ2hCLGVBQWUsRUFBQyxHQUFHLEVBQUU7Z0JBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRTtvQkFDNUIsT0FBTTtpQkFDUDtnQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtnQkFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUE7Z0JBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLGlDQUFpQyxDQUFBO1lBQzVELENBQUM7WUFDRCxrQkFBa0IsRUFBQyxHQUFHLEVBQUU7Z0JBRXRCLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFBO2dCQUU1QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO29CQUM5QixLQUFLLEVBQUUsNEJBQTRCO29CQUNuQyxRQUFRLEVBQUU7WUFDTixZQUFZLENBQUMsSUFBSSxJQUFJO2lCQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNaLElBQUksR0FBRyxFQUFFO3dCQUNQLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQzs2QkFDMUQsU0FBUyxFQUFFOzZCQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTs0QkFDYixJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUE7d0JBQ3hDLENBQUMsQ0FBQzs2QkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7NEJBQ2QsSUFBSSxNQUFNLENBQUMsaUJBQWlCLEVBQUU7Z0NBQzVCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7K0hBQ2dGLEVBQzdHLEVBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFBOzZCQUM5QjtpQ0FBTTtnQ0FDTCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTs2QkFDekM7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7cUJBQ0g7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7Z0JBQ0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUE7WUFDdEMsQ0FBQztZQUNELGdCQUFnQixFQUFDLEdBQUcsRUFBRTtnQkFDcEIsTUFBTSxVQUFVLEdBQWtCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQTtnQkFFdkQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFBO29CQUN2RixZQUFZLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtvQkFDL0MsT0FBTTtpQkFDUDtnQkFFRCxtQkFBbUI7Z0JBQ25CLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRTtvQkFDaEMsVUFBVSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUE7b0JBQ2hELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO3dCQUM1QyxFQUFFLEVBQUUsVUFBVSxDQUFDLEVBQUU7d0JBQ2pCLElBQUksRUFBRSxVQUFVLENBQUMsSUFBSTtxQkFDdEIsQ0FBQzt5QkFDRCxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLENBQUMsVUFBeUIsRUFBRSxFQUFFO3dCQUNsQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxFQUFFLENBQUE7d0JBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUE7d0JBRWpELGlFQUFpRTt3QkFDakUsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQTt3QkFFNUUsSUFBSSxDQUFDLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFBO3dCQUNsRSxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQTtvQkFDdEMsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtvQkFDMUMsQ0FBQyxDQUFDLENBQUE7aUJBQ0g7cUJBQU07b0JBQ0wseUJBQXlCO29CQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQTtvQkFDOUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7d0JBQzVDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxFQUFFO3dCQUM5QixJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsSUFBSTtxQkFDbkMsQ0FBQzt5QkFDRCxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLENBQUMsVUFBeUIsRUFBRSxFQUFFO3dCQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUE7d0JBQ25DLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtvQkFDMUMsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtvQkFDMUMsQ0FBQyxDQUFDLENBQUE7aUJBQ0g7WUFDSCxDQUFDO1lBQ0Qsa0JBQWtCLEVBQUMsR0FBRyxFQUFFO2dCQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxnQ0FBZ0MsQ0FBQTtnQkFDekQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFBO2dCQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQTtZQUN0QyxDQUFDO1lBQ0QsMkJBQTJCLEVBQUMsQ0FBQyxJQUFZLEVBQUUsRUFBRTtnQkFDM0MsSUFBSSxJQUFJLEVBQUU7b0JBQ1IsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtvQkFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFBO2lCQUNqQztxQkFBTTtvQkFDTCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7b0JBQzNCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO2lCQUN4QjtnQkFFRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUN4QixJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUE7b0JBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7aUJBQ2pDO3FCQUFNO29CQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtvQkFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsc0JBQXNCLENBQUE7aUJBQzlDO1lBQ0gsQ0FBQztTQUNGLENBQUE7SUFsSUQsQ0FBQztJQW1JRCxpQkFBaUI7SUFDakIsUUFBUTtJQUVSLENBQUM7O2tHQWhKVSx5QkFBeUI7OERBQXpCLHlCQUF5QjtRQ3RCdEMsOEJBQ0U7UUFBQSw4QkFDRTtRQUFBLG1DQUNFO1FBQUEsaURBRytEO1FBRjdELHdLQUErQixnREFBNEMsSUFBQztRQUV4QyxpQkFBeUI7UUFDakUsaUJBQVc7UUFDYixpQkFBTTtRQUNOLDhCQUNFO1FBQUEsbUNBQ0U7UUFBQSwwQkFBSTtRQUFBLFlBQXFCO1FBQUEsaUJBQUs7UUFDOUIsOEJBQ0U7UUFBQSxnRkFDMEM7UUFDMUMsa0ZBQzBDO1FBQzFDLGtGQUVFO1FBQ0osaUJBQU07UUFDUixpQkFBVztRQUVYLDRFQUNFO1FBR0YsOEVBRUU7UUEyQkosaUJBQU07UUFDUixpQkFBTTs7UUFwREUsZUFBaUM7UUFBakMsbURBQWlDLHdDQUFBO1FBTS9CLGVBQXFCO1FBQXJCLHlDQUFxQjtRQUVJLGVBQXNCO1FBQXRCLHlDQUFzQjtRQUVULGVBQWtEO1FBQWxELHlFQUFrRDtRQUVoRCxlQUFzQjtRQUF0Qix5Q0FBc0I7UUFNL0QsZUFBcUI7UUFBckIsd0NBQXFCO1FBSXJCLGVBQXdDO1FBQXhDLCtEQUF3Qzs7a0RETHBDLHlCQUF5QjtjQUxyQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLDRCQUE0QjtnQkFDdEMsV0FBVyxFQUFFLG9DQUFvQztnQkFDakQsU0FBUyxFQUFFLENBQUMsb0NBQW9DLENBQUM7YUFDbEQiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IEZvZlBlcm1pc3Npb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBOZXN0ZWRUcmVlQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay90cmVlJ1xyXG5pbXBvcnQgeyBNYXRUcmVlTmVzdGVkRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RyZWUnXHJcbmltcG9ydCB7IGlPcmdhbml6YXRpb24gfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyAgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIlxyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZkRpYWxvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL2ZvZi1kaWFsb2cuc2VydmljZSdcclxuaW1wb3J0IHsgZm9mVXRpbHNGb3JtLCBmb2ZFcnJvciB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLXV0aWxzJ1xyXG5pbXBvcnQgeyBGb2ZFcnJvclNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL2ZvZi1lcnJvci5zZXJ2aWNlJ1xyXG5cclxuaW50ZXJmYWNlIGlPcmdVSSBleHRlbmRzIGlPcmdhbml6YXRpb24ge1xyXG4gIGNoZWNrZWQ/OiBib29sZWFuLFxyXG4gIGluZGV0ZXJtaW5hdGU/OiBib29sZWFuLFxyXG4gIGNoaWxkcmVuPzogaU9yZ1VJW11cclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2YtY29yZS1mb2Ytb3JnYW5pemF0aW9ucycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZvZi1vcmdhbml6YXRpb25zLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9mb2Ytb3JnYW5pemF0aW9ucy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZPcmdhbml6YXRpb25zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuIFxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmb2ZQZXJtaXNzaW9uU2VydmljZTogRm9mUGVybWlzc2lvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGZvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mRGlhbG9nU2VydmljZTogRm9mRGlhbG9nU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2VcclxuICApIHsgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgY3VycmVudE5vZGU6IDxpT3JnYW5pemF0aW9uPnVuZGVmaW5lZFxyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gIH1cclxuICAvLyBBbGwgdmFyaWFibGVzIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aVZhciA9IHtcclxuICAgIGxvYWRpbmc6IGZhbHNlLFxyXG4gICAgb3JnYW5pemF0aW9uSXNOZXc6IGZhbHNlLCAgXHJcbiAgICBub2RlRm9ybTogZmFsc2UsXHJcbiAgICBmb3JtOiB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHsgXHJcbiAgICAgIG5hbWU6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWF4TGVuZ3RoKDEwMCldXSxcclxuICAgICAgZGVzY3JpcHRpb246IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDIwMCldXSxcclxuICAgIH0pLFxyXG4gICAgYWN0aW9uVGl0bGU6IGBNb2RpZmljYXRpb24gZGUgbCdvcmdhbmlzYXRpb25gLFxyXG4gICAgbWFpblRpdGxlOiBgR8OpcmVyIGwnb3JnYW5pc2F0aW9uYCxcclxuICAgIG5vZGVDaGFuZ2VkOiA8aU9yZ2FuaXphdGlvbj51bmRlZmluZWQsXHJcbiAgICBub2RlVG9EZWxldGU6IDxpT3JnYW5pemF0aW9uPnVuZGVmaW5lZFxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7ICAgIFxyXG4gICAgb3JnYW5pc2F0aW9uQWRkOigpID0+IHtcclxuICAgICAgaWYgKCF0aGlzLnByaVZhci5jdXJyZW50Tm9kZSkge1xyXG4gICAgICAgIHJldHVyblxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMudWlWYXIuZm9ybS5yZXNldCgpXHJcbiAgICAgIHRoaXMudWlWYXIub3JnYW5pemF0aW9uSXNOZXcgPSB0cnVlXHJcbiAgICAgIHRoaXMudWlWYXIuYWN0aW9uVGl0bGUgPSBgQWpvdXRlciB1bmUgb3JnYW5pc2F0aW9uIGVuZmFudGBcclxuICAgIH0sXHJcbiAgICBvcmdhbmlzYXRpb25EZWxldGU6KCkgPT4ge1xyXG5cclxuICAgICAgY29uc3Qgbm9kZVRvRGVsZXRlID0gdGhpcy5wcmlWYXIuY3VycmVudE5vZGVcclxuICAgICBcclxuICAgICAgdGhpcy5mb2ZEaWFsb2dTZXJ2aWNlLm9wZW5ZZXNObyh7XHJcbiAgICAgICAgdGl0bGU6IFwiU3VwcHJpbWVyIHVuZSBvcmdhbmlzYXRpb25cIixcclxuICAgICAgICBxdWVzdGlvbjogYFZvdWxleiB2b3VzIHZyYWltZW50IHN1cHByaW1lciBcclxuICAgICAgICAgICR7bm9kZVRvRGVsZXRlLm5hbWV9ID9gXHJcbiAgICAgIH0pLnRoZW4oeWVzID0+IHsgICAgICAgIFxyXG4gICAgICAgIGlmICh5ZXMpIHsgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLm9yZ2FuaXphdGlvbi5kZWxldGUobm9kZVRvRGVsZXRlKVxyXG4gICAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnVpVmFyLm5vZGVUb0RlbGV0ZSA9IG5vZGVUb0RlbGV0ZVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC5jYXRjaChyZWFzb24gPT4geyAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZiAocmVhc29uLmlzQ29uc3RyYWludEVycm9yKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmluZm8oYFZvdXMgbmUgcG91dmV6IHBhcyBzdXBwcmltZXIgbCdvcmdhbmlzYXRpb24gITxicj5cclxuICAgICAgICAgICAgICAgIDxzbWFsbD5Wb3VzIGRldmV6IHN1cHByaW1lciB0b3VzIGxlcyBvYmpldHMgcmF0dGFjaMOpcyDDoCBsJ29yZ2FuaXNhdGlvbiBvdSBjb250YWN0ZXIgdW4gYWRtbmluaXN0cmF0ZXVyPC9zbWFsbD5gXHJcbiAgICAgICAgICAgICAgICAsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IC0xfSlcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgICAgIH0gICAgICAgICAgICBcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgICB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbklzTmV3ID0gZmFsc2VcclxuICAgIH0sXHJcbiAgICBvcmdhbmlzYXRpb25TYXZlOigpID0+IHtcclxuICAgICAgY29uc3Qgbm9kZVRvU2F2ZTogaU9yZ2FuaXphdGlvbiA9IHRoaXMudWlWYXIuZm9ybS52YWx1ZSAgICAgXHJcblxyXG4gICAgICBpZiAoIXRoaXMudWlWYXIuZm9ybS52YWxpZCkge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignVmV1aWxsZXogY29ycmlnZXIgbGVzIGVycmV1cnMgYXZhbnQgZGUgc2F1dmVnYXJkZXInKVxyXG4gICAgICAgIGZvZlV0aWxzRm9ybS52YWxpZGF0ZUFsbEZpZWxkcyh0aGlzLnVpVmFyLmZvcm0pXHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIE5ldyBvcmdhbml6YXRpb25cclxuICAgICAgaWYgKHRoaXMudWlWYXIub3JnYW5pemF0aW9uSXNOZXcpIHtcclxuICAgICAgICBub2RlVG9TYXZlLnBhcmVudElkID0gdGhpcy5wcmlWYXIuY3VycmVudE5vZGUuaWRcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLm9yZ2FuaXphdGlvbi5jcmVhdGUoe1xyXG4gICAgICAgICAgaWQ6IG5vZGVUb1NhdmUuaWQsXHJcbiAgICAgICAgICBuYW1lOiBub2RlVG9TYXZlLm5hbWVcclxuICAgICAgICB9KVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKChub2RlUmVzdWx0OiBpT3JnYW5pemF0aW9uKSA9PiB7ICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnNhdmVJc0RvbmUoKVxyXG4gICAgICAgICAgdGhpcy5wcmlWYXIuY3VycmVudE5vZGUuY2hpbGRyZW4ucHVzaChub2RlUmVzdWx0KSAgXHJcblxyXG4gICAgICAgICAgLy8gZGVlcCBjb3B5IGZvciBwdXNoaW5nIHRoZSB0cmVlIGNvbXBvbmVudCB0byByZWZyZXNoIHRoZSBvYmplY3RcclxuICAgICAgICAgIHRoaXMudWlWYXIubm9kZUNoYW5nZWQgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHRoaXMucHJpVmFyLmN1cnJlbnROb2RlKSlcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aUFjdGlvbi5zZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2UodGhpcy5wcmlWYXIuY3VycmVudE5vZGUpXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbklzTmV3ID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChyZWFzb24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxyXG4gICAgICAgIH0pICAgICAgXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gVXBkYXRlIGFuIG9yZ2FuaXphdGlvblxyXG4gICAgICAgIHRoaXMucHJpVmFyLmN1cnJlbnROb2RlLm5hbWUgPSBub2RlVG9TYXZlLm5hbWVcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLm9yZ2FuaXphdGlvbi51cGRhdGUoe1xyXG4gICAgICAgICAgaWQ6IHRoaXMucHJpVmFyLmN1cnJlbnROb2RlLmlkLFxyXG4gICAgICAgICAgbmFtZTogdGhpcy5wcmlWYXIuY3VycmVudE5vZGUubmFtZVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4oKG5vZGVSZXN1bHQ6IGlPcmdhbml6YXRpb24pID0+IHsgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5ub2RlQ2hhbmdlZCA9IG5vZGVSZXN1bHQgICAgICAgICBcclxuICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zYXZlSXNEb25lKCkgICAgICAgICAgXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2gocmVhc29uID0+IHtcclxuICAgICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcclxuICAgICAgICB9KVxyXG4gICAgICB9ICAgICAgXHJcbiAgICB9LFxyXG4gICAgb3JnYW5pc2F0aW9uQ2FuY2VsOigpID0+IHsgICAgICAgICAgICBcclxuICAgICAgdGhpcy51aVZhci5hY3Rpb25UaXRsZSA9IGBNb2RpZmljYXRpb24gZGUgbCdvcmdhbmlzYXRpb25gXHJcbiAgICAgIHRoaXMudWlWYXIubm9kZUZvcm0gPSBmYWxzZVxyXG4gICAgICB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbklzTmV3ID0gZmFsc2VcclxuICAgIH0sXHJcbiAgICBzZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2U6KG5vZGU6IGlPcmdVSSkgPT4ge1xyXG4gICAgICBpZiAobm9kZSkge1xyXG4gICAgICAgIHRoaXMudWlWYXIubm9kZUZvcm0gPSBub2RlLmNoZWNrZWQgXHJcbiAgICAgICAgdGhpcy51aVZhci5mb3JtLnBhdGNoVmFsdWUobm9kZSlcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnVpVmFyLm5vZGVGb3JtID0gZmFsc2VcclxuICAgICAgICB0aGlzLnVpVmFyLmZvcm0ucmVzZXQoKVxyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICBpZiAobm9kZSAmJiBub2RlLmNoZWNrZWQpIHtcclxuICAgICAgICB0aGlzLnByaVZhci5jdXJyZW50Tm9kZSA9IG5vZGVcclxuICAgICAgICB0aGlzLnVpVmFyLm1haW5UaXRsZSA9IG5vZGUubmFtZVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMucHJpVmFyLmN1cnJlbnROb2RlID0gbnVsbFxyXG4gICAgICAgIHRoaXMudWlWYXIubWFpblRpdGxlID0gYEfDqXJlciBsJ29yZ2FuaXNhdGlvbmBcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gIFxyXG4gIH0gIFxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJyb3cgbWFpblwiPlxyXG4gIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG4gICAgPG1hdC1jYXJkIGNsYXNzPVwiY2FyZC10cmVlXCI+XHJcbiAgICAgIDxmb2Ytb3JnYW5pemF0aW9ucy10cmVlXHJcbiAgICAgICAgKHNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZSk9XCJ1aUFjdGlvbi5zZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2UoJGV2ZW50KVwiXHJcbiAgICAgICAgW25vZGVDaGFuZ2VkXT1cInVpVmFyLm5vZGVDaGFuZ2VkXCJcclxuICAgICAgICBbbm9kZVRvRGVsZXRlXT1cInVpVmFyLm5vZGVUb0RlbGV0ZVwiPjwvZm9mLW9yZ2FuaXphdGlvbnMtdHJlZT4gICAgICBcclxuICAgIDwvbWF0LWNhcmQ+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICA8bWF0LWNhcmQgY2xhc3M9XCJmb2YtaGVhZGVyIGNhcmQtZGV0YWlsXCI+ICAgICAgICBcclxuICAgICAgPGgzPnt7IHVpVmFyLm1haW5UaXRsZSB9fTwvaDM+IFxyXG4gICAgICA8ZGl2IGNsYXNzPVwiZm9mLXRvb2xiYXJcIj4gICAgXHJcbiAgICAgICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gKm5nSWY9XCJ1aVZhci5ub2RlRm9ybVwiXHJcbiAgICAgICAgICAoY2xpY2spPVwidWlBY3Rpb24ub3JnYW5pc2F0aW9uQ2FuY2VsKClcIj5Bbm51bGVyPC9idXR0b24+ICAgIFxyXG4gICAgICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwid2FyblwiICpuZ0lmPVwidWlWYXIubm9kZUZvcm0gJiYgIXVpVmFyLm9yZ2FuaXphdGlvbklzTmV3XCJcclxuICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5vcmdhbmlzYXRpb25EZWxldGUoKVwiPlN1cHByaW1lcjwvYnV0dG9uPiAgICAgXHJcbiAgICAgICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJhY2NlbnRcIiAqbmdJZj1cInVpVmFyLm5vZGVGb3JtXCJcclxuICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5vcmdhbmlzYXRpb25TYXZlKClcIj5cclxuICAgICAgICAgIEVucmVnaXN0cmVyPC9idXR0b24+ICAgICAgICBcclxuICAgICAgPC9kaXY+IFxyXG4gICAgPC9tYXQtY2FyZD5cclxuICAgIFxyXG4gICAgPGRpdiAqbmdJZj1cInVpVmFyLmxvYWRpbmdcIiBjbGFzcz1cImZvZi1sb2FkaW5nXCI+XHJcbiAgICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5DaGFyZ2VtZW50cyBkZXMgb3JnYW5pc2F0aW9ucy4uLjwvc3Bhbj5cclxuICAgIDwvZGl2PlxyXG4gICAgXHJcbiAgICA8ZGl2ICpuZ0lmPVwiIXVpVmFyLmxvYWRpbmcgJiYgdWlWYXIubm9kZUZvcm1cIiBjbGFzcz1cImZvZi1mYWRlLWluIGNhcmQtZGV0YWlsXCI+XHJcblxyXG4gICAgICA8ZGl2IGNsYXNzPVwiZm9mLWhlYWRlclwiPlxyXG4gICAgICAgIDxoND57eyB1aVZhci5hY3Rpb25UaXRsZSB9fTwvaDQ+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImZvZi10b29sYmFyXCI+XHJcbiAgICAgICAgICA8YnV0dG9uIG1hdC1zdHJva2VkLWJ1dHRvbiAgKm5nSWY9XCIhdWlWYXIub3JnYW5pemF0aW9uSXNOZXdcIlxyXG4gICAgICAgICAgICBjbGFzcz1cImNoaWxkLWFkZFwiIGNvbG9yPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5vcmdhbmlzYXRpb25BZGQoKVwiPkFqb3V0ZXIgdW5lIG9yZ2FuaXNhdGlvbiBlbmZhbnQ8L2J1dHRvbj5cclxuICAgICAgICA8L2Rpdj4gXHJcbiAgICAgIDwvZGl2PlxyXG5cclxuXHJcbiAgICAgIDwhLS0gPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gICpuZ0lmPVwiIXVpVmFyLm9yZ2FuaXphdGlvbklzTmV3XCJcclxuICAgICAgICBjbGFzcz1cImNoaWxkLWFkZFwiIGNvbG9yPVwicHJpbWFyeVwiXHJcbiAgICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLm9yZ2FuaXNhdGlvbkFkZCgpXCI+QWpvdXRlciB1bmUgb3JnYW5pc2F0aW9uIGVuZmFudDwvYnV0dG9uPiAgXHJcbiAgICAgIDxoND57eyB1aVZhci5hY3Rpb25UaXRsZSB9fTwvaDQ+IC0tPlxyXG4gICAgICA8Zm9ybSBbZm9ybUdyb3VwXT1cInVpVmFyLmZvcm1cIj5cclxuICAgICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgICA8aW5wdXQgbWF0SW5wdXQgcmVxdWlyZWQgXHJcbiAgICAgICAgICAgIHR5cGU9XCJuYW1lXCJcclxuICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwibmFtZVwiXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiTm9tIGRlIGwnb3JnYW5pc2F0aW9uXCIgdmFsdWU9XCJcIj5cclxuICAgICAgICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJ1aVZhci5mb3JtLmdldCgnbmFtZScpLmludmFsaWRcIj5cclxuICAgICAgICAgICAgTGUgbm9tIG5lIGRvaXQgcGFzIMOqdHJlIG51bCBldCBmYWlyZSBwbHVzIGRlIDEwMCBjYXJhcmN0w6hyZXNcclxuICAgICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+ICAgICAgICBcclxuICAgICAgPC9mb3JtPiAgICAgICAgICAgIFxyXG4gICAgPC9kaXY+XHJcbiAgICBcclxuICA8L2Rpdj5cclxuPC9kaXY+XHJcblxyXG4iXX0=
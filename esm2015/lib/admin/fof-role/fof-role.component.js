import { Component } from '@angular/core';
import { forkJoin } from 'rxjs';
import { Validators } from "@angular/forms";
import { fofUtilsForm } from '../../core/fof-utils';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "@angular/router";
import * as i3 from "@angular/forms";
import * as i4 from "../../core/notification/notification.service";
import * as i5 from "../../core/fof-dialog.service";
import * as i6 from "@angular/material/card";
import * as i7 from "@angular/material/button";
import * as i8 from "@angular/common";
import * as i9 from "../fof-entity-footer/fof-entity-footer.component";
import * as i10 from "@angular/material/form-field";
import * as i11 from "@angular/material/input";
import * as i12 from "@angular/material/progress-spinner";
import * as i13 from "@angular/material/checkbox";
function FofRoleComponent_div_11_mat_error_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le code du r\u00F4le est obligatoire et doit \u00EAtre compos\u00E9 de au moins 3 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofRoleComponent_div_11_mat_error_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " La description ne peut pas exc\u00E9der 200 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofRoleComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 10);
    i0.ɵɵelementStart(1, "form", 11);
    i0.ɵɵelementStart(2, "mat-form-field");
    i0.ɵɵelement(3, "input", 12);
    i0.ɵɵtemplate(4, FofRoleComponent_div_11_mat_error_4_Template, 2, 0, "mat-error", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "mat-form-field");
    i0.ɵɵelement(6, "textarea", 14);
    i0.ɵɵelementStart(7, "mat-hint");
    i0.ɵɵtext(8, "D\u00E9crivez succintement le r\u00F4le");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(9, "mat-hint", 15);
    i0.ɵɵtext(10);
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(11, FofRoleComponent_div_11_mat_error_11_Template, 2, 0, "mat-error", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r25 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("formGroup", ctx_r25.uiVar.form);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r25.uiVar.form.get("code").invalid);
    i0.ɵɵadvance(6);
    i0.ɵɵtextInterpolate1("", ctx_r25.uiVar.form.get("description").value == null ? null : ctx_r25.uiVar.form.get("description").value.length, " / 200");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r25.uiVar.form.get("description").invalid);
} }
function FofRoleComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵelement(1, "mat-spinner", 17);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des roles et des authorisations...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r37 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵelementStart(1, "mat-checkbox", 21);
    i0.ɵɵlistener("change", function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template_mat_checkbox_change_1_listener() { i0.ɵɵrestoreView(_r37); const permission_r35 = ctx.$implicit; const ctx_r36 = i0.ɵɵnextContext(3); return ctx_r36.uiAction.rolePermissionSave(permission_r35); })("ngModelChange", function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template_mat_checkbox_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r37); const permission_r35 = ctx.$implicit; return permission_r35.checked = $event; });
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const permission_r35 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", permission_r35.checked)("checked", permission_r35.checked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(permission_r35.code);
} }
function FofRoleComponent_mat_card_13_mat_card_content_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-card-content");
    i0.ɵɵtemplate(1, FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template, 3, 3, "div", 20);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const group_r30 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", group_r30.children);
} }
function FofRoleComponent_mat_card_13_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-spinner", 22);
} }
function FofRoleComponent_mat_card_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-card", 18);
    i0.ɵɵelementStart(1, "mat-card-header");
    i0.ɵɵelementStart(2, "mat-card-title");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, FofRoleComponent_mat_card_13_mat_card_content_4_Template, 2, 1, "mat-card-content", 13);
    i0.ɵɵtemplate(5, FofRoleComponent_mat_card_13_ng_template_5_Template, 1, 0, "ng-template", null, 19, i0.ɵɵtemplateRefExtractor);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const group_r30 = ctx.$implicit;
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(group_r30.code);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", group_r30.children);
} }
export class FofRoleComponent {
    constructor(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router) {
        this.fofPermissionService = fofPermissionService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        // All private variables
        this.priVar = {
            roleCode: undefined,
            permissionRoluUpdating: false
        };
        // All private functions
        this.privFunc = {
            roleLoad: () => {
                this.uiVar.loading = true;
                forkJoin(this.fofPermissionService.permission.getAll(), this.fofPermissionService.role.getWithPermissionByRoleCode(this.priVar.roleCode)).subscribe({
                    next: result => {
                        const permissions = result[0];
                        const currentRoles = result[1];
                        let currentRole = undefined;
                        const _permissions = [];
                        if (currentRoles.length > 0) {
                            currentRole = result[1][0];
                        }
                        else {
                            this.fofNotificationService.error(`Cet rôle n'existe pas`);
                            this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                            return;
                        }
                        this.uiVar.role = currentRole;
                        this.uiVar.form.patchValue(this.uiVar.role);
                        let _permission;
                        let previousNameRoot = undefined;
                        permissions.forEach((permission) => {
                            //split to upper case
                            const names = permission.code.split(/(?=[A-Z])/);
                            const nameRoot = names[0];
                            let found = false;
                            let rolePermission;
                            if (currentRole) {
                                rolePermission = currentRole.rolePermissions.find(item => item.permissionId == permission.id);
                                if (rolePermission) {
                                    found = true;
                                }
                            }
                            const permissionCode = permission.code.slice(nameRoot.length);
                            if (nameRoot === previousNameRoot) {
                                _permission.children.push({
                                    id: permission.id,
                                    checked: found,
                                    rolePermission: rolePermission,
                                    code: permissionCode
                                });
                            }
                            else {
                                previousNameRoot = nameRoot;
                                if (_permission) {
                                    _permissions.push(_permission);
                                }
                                _permission = {};
                                _permission.code = nameRoot;
                                _permission.children = [];
                                _permission.children.push({
                                    id: permission.id,
                                    checked: found,
                                    rolePermission: rolePermission,
                                    code: permissionCode
                                });
                            }
                        });
                        if (_permission) {
                            _permissions.push(_permission);
                        }
                        this.uiVar.permissionGroups = _permissions;
                        this.uiVar.loading = false;
                        // console.log('currentRole', currentRole)
                    },
                    complete: () => {
                    }
                });
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouveau rôle',
            permissionGroups: undefined,
            loading: false,
            roleIsNew: false,
            role: undefined,
            form: this.formBuilder.group({
                code: ['', [Validators.required, Validators.minLength(3)]],
                description: ['', [Validators.maxLength(200)]]
            })
        };
        // All actions shared with UI 
        this.uiAction = {
            roleSave: () => {
                const roleToSave = this.uiVar.form.value;
                if (!this.uiVar.form.valid) {
                    this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(this.uiVar.form);
                    return;
                }
                if (roleToSave.code.toLowerCase() == 'new') {
                    this.fofNotificationService.error('Désolé, ce code est réservé');
                    return;
                }
                if (this.uiVar.roleIsNew) {
                    this.fofPermissionService.role.create(roleToSave)
                        .toPromise()
                        .then((newRole) => {
                        this.fofNotificationService.success('Rôle sauvé', { mustDisappearAfter: 1000 });
                        this.priVar.roleCode = newRole.code;
                        this.uiVar.title = 'Modification de rôle';
                        this.uiVar.roleIsNew = false;
                        this.privFunc.roleLoad();
                    });
                }
                else {
                    roleToSave.id = this.uiVar.role.id;
                    this.fofPermissionService.role.update(roleToSave)
                        .toPromise()
                        .then(result => {
                        this.fofNotificationService.success('Rôle sauvé', { mustDisappearAfter: 1000 });
                    });
                }
            },
            roleCancel: () => {
                this.uiVar.permissionGroups = null;
                this.privFunc.roleLoad();
            },
            roleDelete: () => {
                this.fofDialogService.openYesNo({
                    question: 'Voulez vous vraiment supprimer le rôle ?'
                }).then(yes => {
                    if (yes) {
                        this.fofPermissionService.role.delete(this.uiVar.role)
                            .toPromise()
                            .then(result => {
                            this.fofNotificationService.success('Rôle supprimé');
                            this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                        });
                    }
                });
            },
            rolePermissionSave: (permission) => {
                const rolePermission = permission.rolePermission;
                // The rolePermission must be deleted
                if (rolePermission) {
                    this.fofPermissionService.rolePermission.delete(rolePermission)
                        .toPromise()
                        .then((result) => {
                        permission.rolePermission = null;
                        this.fofNotificationService.saveIsDone();
                    });
                }
                else {
                    // The rolePermission must be created
                    const rolePermissionToSave = {
                        permissionId: permission.id,
                        roleId: this.uiVar.role.id
                    };
                    this.fofPermissionService.rolePermission.create(rolePermissionToSave)
                        .toPromise()
                        .then((result) => {
                        this.priVar.permissionRoluUpdating = false;
                        permission.rolePermission = result;
                        this.fofNotificationService.saveIsDone();
                    });
                }
            }
        };
    }
    // Angular events
    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(params => {
            const code = params.get('code');
            this.priVar.roleCode = code;
            if (code) {
                if (code.toLowerCase() == 'new') {
                    this.uiVar.roleIsNew = true;
                }
                else {
                    this.uiVar.title = 'Modification de rôle';
                    this.privFunc.roleLoad();
                }
            }
        });
    }
}
FofRoleComponent.ɵfac = function FofRoleComponent_Factory(t) { return new (t || FofRoleComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i3.FormBuilder), i0.ɵɵdirectiveInject(i4.FofNotificationService), i0.ɵɵdirectiveInject(i5.FofDialogService), i0.ɵɵdirectiveInject(i2.Router)); };
FofRoleComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofRoleComponent, selectors: [["fof-role"]], decls: 15, vars: 5, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "fof-fade-in detail", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], ["class", "group-card fof-fade-in", 4, "ngFor", "ngForOf"], [3, "entityBase"], [1, "fof-fade-in", "detail"], [3, "formGroup"], ["matInput", "", "required", "", "formControlName", "code", "placeholder", "Code du r\u00F4le", "value", ""], [4, "ngIf"], ["matInput", "", "formControlName", "description", "rows", "3", "placeholder", "Description"], ["align", "end"], [1, "fof-loading"], ["diameter", "20"], [1, "group-card", "fof-fade-in"], ["loading", ""], [4, "ngFor", "ngForOf"], [3, "ngModel", "checked", "change", "ngModelChange"], ["diameter", "30"]], template: function FofRoleComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-card", 0);
        i0.ɵɵelementStart(1, "h3");
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "div", 1);
        i0.ɵɵelementStart(4, "button", 2);
        i0.ɵɵlistener("click", function FofRoleComponent_Template_button_click_4_listener() { return ctx.uiAction.roleCancel(); });
        i0.ɵɵtext(5, "Annuler");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "button", 3);
        i0.ɵɵlistener("click", function FofRoleComponent_Template_button_click_6_listener() { return ctx.uiAction.roleDelete(); });
        i0.ɵɵtext(7, "Supprimer");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "button", 4);
        i0.ɵɵlistener("click", function FofRoleComponent_Template_button_click_8_listener() { return ctx.uiAction.roleSave(); });
        i0.ɵɵtext(9, " Enregister");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(10, "div", 5);
        i0.ɵɵtemplate(11, FofRoleComponent_div_11_Template, 12, 4, "div", 6);
        i0.ɵɵtemplate(12, FofRoleComponent_div_12_Template, 4, 0, "div", 7);
        i0.ɵɵtemplate(13, FofRoleComponent_mat_card_13_Template, 7, 2, "mat-card", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelement(14, "fof-entity-footer", 9);
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.uiVar.title);
        i0.ɵɵadvance(9);
        i0.ɵɵproperty("ngIf", !ctx.uiVar.loading);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx.uiVar.permissionGroups);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("entityBase", ctx.uiVar.role);
    } }, directives: [i6.MatCard, i7.MatButton, i8.NgIf, i8.NgForOf, i9.FofEntityFooterComponent, i3.ɵangular_packages_forms_forms_y, i3.NgControlStatusGroup, i3.FormGroupDirective, i10.MatFormField, i11.MatInput, i3.DefaultValueAccessor, i3.RequiredValidator, i3.NgControlStatus, i3.FormControlName, i10.MatHint, i10.MatError, i12.MatSpinner, i6.MatCardHeader, i6.MatCardTitle, i6.MatCardContent, i13.MatCheckbox, i3.NgModel], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{margin:15px 15px 15px 0;width:18em}.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]   mat-card-title[_ngcontent-%COMP%]{text-transform:uppercase}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofRoleComponent, [{
        type: Component,
        args: [{
                selector: 'fof-role',
                templateUrl: './fof-role.component.html',
                styleUrls: ['./fof-role.component.scss'],
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.ActivatedRoute }, { type: i3.FormBuilder }, { type: i4.FofNotificationService }, { type: i5.FofDialogService }, { type: i2.Router }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXJvbGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vZm9mLXJvbGUvZm9mLXJvbGUuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi1yb2xlL2ZvZi1yb2xlLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQW1DLE1BQU0sZUFBZSxDQUFBO0FBSzFFLE9BQU8sRUFBK0IsUUFBUSxFQUFNLE1BQU0sTUFBTSxDQUFBO0FBQ2hFLE9BQU8sRUFBMEIsVUFBVSxFQUFHLE1BQU0sZ0JBQWdCLENBQUE7QUFHcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFBOzs7Ozs7Ozs7Ozs7Ozs7O0lDVTNDLGlDQUNFO0lBQUEsbUhBQ0Y7SUFBQSxpQkFBWTs7O0lBV1osaUNBQ0U7SUFBQSw2RUFDRjtJQUFBLGlCQUFZOzs7SUFyQmxCLCtCQUNFO0lBQUEsZ0NBQ0U7SUFBQSxzQ0FDRTtJQUFBLDRCQUdBO0lBQUEscUZBQ0U7SUFFSixpQkFBaUI7SUFFakIsc0NBQ0U7SUFBQSwrQkFHdUM7SUFDdkMsZ0NBQVU7SUFBQSx1REFBNkI7SUFBQSxpQkFBVztJQUNsRCxvQ0FBc0I7SUFBQSxhQUNqQjtJQUFBLGlCQUFXO0lBQ2hCLHVGQUNFO0lBRUosaUJBQWlCO0lBQ25CLGlCQUFPO0lBQ1QsaUJBQU07OztJQXZCRSxlQUF3QjtJQUF4Qiw4Q0FBd0I7SUFLZixlQUFzQztJQUF0Qyw2REFBc0M7SUFXM0IsZUFDakI7SUFEaUIsb0pBQ2pCO0lBQ00sZUFBNkM7SUFBN0Msb0VBQTZDOzs7SUFNOUQsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLDhEQUE4QztJQUFBLGlCQUFPO0lBQ3JHLGlCQUFNOzs7O0lBTUYsMkJBQ0U7SUFBQSx3Q0FHaUM7SUFGL0IsNk9BQVUsbURBQXVDLElBQUMsa1BBQUE7SUFFbkIsWUFBcUI7SUFBQSxpQkFBZTtJQUN2RSxpQkFBTTs7O0lBRkYsZUFBZ0M7SUFBaEMsZ0RBQWdDLG1DQUFBO0lBQ0QsZUFBcUI7SUFBckIseUNBQXFCOzs7SUFMMUQsd0NBQ0U7SUFBQSxpR0FDRTtJQUtKLGlCQUFtQjs7O0lBTlosZUFBeUM7SUFBekMsNENBQXlDOzs7SUFROUMsa0NBQXVDOzs7SUFiM0Msb0NBQ0U7SUFBQSx1Q0FDRTtJQUFBLHNDQUFnQjtJQUFBLFlBQWdCO0lBQUEsaUJBQWlCO0lBQ25ELGlCQUFrQjtJQUNsQix3R0FDRTtJQU9GLCtIQUNFO0lBRUosaUJBQVc7OztJQWJTLGVBQWdCO0lBQWhCLG9DQUFnQjtJQUVoQixlQUFzQjtJQUF0Qix5Q0FBc0I7O0FENUI1QyxNQUFNLE9BQU8sZ0JBQWdCO0lBRTNCLFlBQ1Usb0JBQTBDLEVBQzFDLGNBQThCLEVBQzlCLFdBQXdCLEVBQ3hCLHNCQUE4QyxFQUM5QyxnQkFBa0MsRUFDbEMsTUFBYztRQUxkLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBSXhCLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixRQUFRLEVBQVUsU0FBUztZQUMzQixzQkFBc0IsRUFBVyxLQUFLO1NBQ3ZDLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHO1lBQ2pCLFFBQVEsRUFBQyxHQUFHLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO2dCQUN6QixRQUFRLENBQ04sSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsRUFDN0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUNqRixDQUFDLFNBQVMsQ0FBQztvQkFDVixJQUFJLEVBQUUsTUFBTSxDQUFDLEVBQUU7d0JBQ2IsTUFBTSxXQUFXLEdBQWtCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQTt3QkFDNUMsTUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO3dCQUM5QixJQUFJLFdBQVcsR0FBVSxTQUFTLENBQUE7d0JBRWxDLE1BQU0sWUFBWSxHQUFVLEVBQUUsQ0FBQTt3QkFFOUIsSUFBSSxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDM0IsV0FBVyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTt5QkFDM0I7NkJBQU07NEJBQ0wsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBOzRCQUMxRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUMsQ0FBQyxDQUFBOzRCQUNoRSxPQUFNO3lCQUNQO3dCQUVELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLFdBQVcsQ0FBQTt3QkFDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7d0JBRTNDLElBQUksV0FBZ0IsQ0FBQTt3QkFDcEIsSUFBSSxnQkFBZ0IsR0FBVyxTQUFTLENBQUE7d0JBQ3hDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxVQUF1QixFQUFFLEVBQUU7NEJBQzlDLHFCQUFxQjs0QkFDckIsTUFBTSxLQUFLLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUE7NEJBQ2hELE1BQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTs0QkFFekIsSUFBSSxLQUFLLEdBQU8sS0FBSyxDQUFBOzRCQUNyQixJQUFJLGNBQStCLENBQUE7NEJBRW5DLElBQUksV0FBVyxFQUFFO2dDQUNmLGNBQWMsR0FBRyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFBO2dDQUM3RixJQUFJLGNBQWMsRUFBRTtvQ0FDbEIsS0FBSyxHQUFHLElBQUksQ0FBQTtpQ0FDYjs2QkFDRjs0QkFDRCxNQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUE7NEJBRTdELElBQUksUUFBUSxLQUFLLGdCQUFnQixFQUFFO2dDQUNqQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztvQ0FDeEIsRUFBRSxFQUFFLFVBQVUsQ0FBQyxFQUFFO29DQUNqQixPQUFPLEVBQUUsS0FBSztvQ0FDZCxjQUFjLEVBQUUsY0FBYztvQ0FDOUIsSUFBSSxFQUFFLGNBQWM7aUNBQ3JCLENBQUMsQ0FBQTs2QkFDSDtpQ0FBTTtnQ0FDTCxnQkFBZ0IsR0FBRyxRQUFRLENBQUE7Z0NBQzNCLElBQUksV0FBVyxFQUFFO29DQUFFLFlBQVksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7aUNBQUM7Z0NBQ2xELFdBQVcsR0FBRyxFQUFFLENBQUE7Z0NBQ2hCLFdBQVcsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFBO2dDQUMzQixXQUFXLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQTtnQ0FDekIsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0NBQ3hCLEVBQUUsRUFBRSxVQUFVLENBQUMsRUFBRTtvQ0FDakIsT0FBTyxFQUFFLEtBQUs7b0NBQ2QsY0FBYyxFQUFFLGNBQWM7b0NBQzlCLElBQUksRUFBRSxjQUFjO2lDQUNyQixDQUFDLENBQUE7NkJBQ0g7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBQ0YsSUFBSSxXQUFXLEVBQUU7NEJBQUUsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTt5QkFBQzt3QkFFbEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxZQUFZLENBQUE7d0JBQzFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQTt3QkFFMUIsMENBQTBDO29CQUM1QyxDQUFDO29CQUNELFFBQVEsRUFBRSxHQUFHLEVBQUU7b0JBRWYsQ0FBQztpQkFDRixDQUFDLENBQUE7WUFDSixDQUFDO1NBQ0YsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixLQUFLLEVBQUUsY0FBYztZQUNyQixnQkFBZ0IsRUFBTyxTQUFTO1lBQ2hDLE9BQU8sRUFBRSxLQUFLO1lBQ2QsU0FBUyxFQUFFLEtBQUs7WUFDaEIsSUFBSSxFQUFTLFNBQVM7WUFDdEIsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUMzQixJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDMUQsV0FBVyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQy9DLENBQUM7U0FDSCxDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRztZQUNoQixRQUFRLEVBQUMsR0FBRyxFQUFFO2dCQUNaLE1BQU0sVUFBVSxHQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQTtnQkFFL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFBO29CQUN2RixZQUFZLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtvQkFDL0MsT0FBTTtpQkFDUDtnQkFFRCxJQUFJLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksS0FBSyxFQUFFO29CQUMxQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUE7b0JBQ2hFLE9BQU07aUJBQ1A7Z0JBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRTtvQkFDeEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO3lCQUNoRCxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLENBQUMsT0FBYyxFQUFFLEVBQUU7d0JBQ3ZCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEVBQUMsa0JBQWtCLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTt3QkFDN0UsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQTt3QkFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsc0JBQXNCLENBQUE7d0JBQ3pDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTt3QkFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtvQkFDMUIsQ0FBQyxDQUFDLENBQUE7aUJBQ0g7cUJBQU07b0JBQ0wsVUFBVSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUE7b0JBRWxDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQzt5QkFDaEQsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDYixJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxFQUFDLGtCQUFrQixFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7b0JBQy9FLENBQUMsQ0FBQyxDQUFBO2lCQUNIO1lBQ0gsQ0FBQztZQUNELFVBQVUsRUFBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUE7Z0JBQ2xDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUE7WUFDMUIsQ0FBQztZQUNELFVBQVUsRUFBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztvQkFDOUIsUUFBUSxFQUFFLDBDQUEwQztpQkFDckQsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDWixJQUFJLEdBQUcsRUFBRTt3QkFDUCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQzs2QkFDckQsU0FBUyxFQUFFOzZCQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTs0QkFDYixJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxDQUFBOzRCQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUMsQ0FBQyxDQUFBO3dCQUNsRSxDQUFDLENBQUMsQ0FBQTtxQkFDSDtnQkFDSCxDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUM7WUFDRCxrQkFBa0IsRUFBRSxDQUFDLFVBQWUsRUFBRSxFQUFFO2dCQUN0QyxNQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFBO2dCQUVoRCxxQ0FBcUM7Z0JBQ3JDLElBQUksY0FBYyxFQUFFO29CQUNsQixJQUFJLENBQUMsb0JBQW9CLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUM7eUJBQzlELFNBQVMsRUFBRTt5QkFDWCxJQUFJLENBQUMsQ0FBQyxNQUFXLEVBQUUsRUFBRTt3QkFDcEIsVUFBVSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUE7d0JBQ2hDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtvQkFDMUMsQ0FBQyxDQUFDLENBQUE7aUJBQ0g7cUJBQU07b0JBQ0wscUNBQXFDO29CQUNyQyxNQUFNLG9CQUFvQixHQUFvQjt3QkFDNUMsWUFBWSxFQUFFLFVBQVUsQ0FBQyxFQUFFO3dCQUMzQixNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtxQkFDM0IsQ0FBQTtvQkFFRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQzt5QkFDcEUsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO3dCQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQTt3QkFDMUMsVUFBVSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUE7d0JBQ2xDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLEVBQUUsQ0FBQTtvQkFDMUMsQ0FBQyxDQUFDLENBQUE7aUJBQ0g7WUFDSCxDQUFDO1NBQ0YsQ0FBQTtJQWxMRCxDQUFDO0lBbUxELGlCQUFpQjtJQUNqQixRQUFRO1FBQ04sSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQzlDLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7WUFDL0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO1lBRTNCLElBQUksSUFBSSxFQUFFO2dCQUNSLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLEtBQUssRUFBRTtvQkFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFBO2lCQUM1QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxzQkFBc0IsQ0FBQTtvQkFDekMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtpQkFDekI7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQzs7Z0ZBNU1VLGdCQUFnQjtxREFBaEIsZ0JBQWdCO1FDakI3QixtQ0FDRTtRQUFBLDBCQUFJO1FBQUEsWUFBaUI7UUFBQSxpQkFBSztRQUMxQiw4QkFDRTtRQUFBLGlDQUNrQztRQUFoQyw2RkFBUyx5QkFBcUIsSUFBQztRQUFDLHVCQUFPO1FBQUEsaUJBQVM7UUFDbEQsaUNBQ2tDO1FBQWhDLDZGQUFTLHlCQUFxQixJQUFDO1FBQUMseUJBQVM7UUFBQSxpQkFBUztRQUNwRCxpQ0FFRTtRQURBLDZGQUFTLHVCQUFtQixJQUFDO1FBQzdCLDJCQUFVO1FBQUEsaUJBQVM7UUFDdkIsaUJBQU07UUFDUixpQkFBVztRQUNYLCtCQUNFO1FBQUEsb0VBQ0U7UUF3QkYsbUVBQ0U7UUFFRiw2RUFDRTtRQWVKLGlCQUFNO1FBRU4sd0NBRW9COztRQTVEZCxlQUFpQjtRQUFqQixxQ0FBaUI7UUFZaEIsZUFBc0I7UUFBdEIseUNBQXNCO1FBeUJ0QixlQUFxQjtRQUFyQix3Q0FBcUI7UUFHZSxlQUE0QztRQUE1QyxvREFBNEM7UUFtQnJGLGVBQXlCO1FBQXpCLDJDQUF5Qjs7a0REM0NkLGdCQUFnQjtjQU41QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLFdBQVcsRUFBRSwyQkFBMkI7Z0JBQ3hDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO2FBRXpDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXHJcbmltcG9ydCB7IGlQZXJtaXNzaW9uLCBpUm9sZSwgaVJvbGVQZXJtaXNzaW9uIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgZm9ya0pvaW4sIG9mIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyAgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIlxyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZkRpYWxvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL2ZvZi1kaWFsb2cuc2VydmljZSdcclxuaW1wb3J0IHsgZm9mVXRpbHNGb3JtIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtdXRpbHMnXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1yb2xlJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9mLXJvbGUuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi1yb2xlLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgLy8gY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZlJvbGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9mUGVybWlzc2lvblNlcnZpY2U6IEZvZlBlcm1pc3Npb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIGZvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mRGlhbG9nU2VydmljZTogRm9mRGlhbG9nU2VydmljZSxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcclxuICApIHsgICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICByb2xlQ29kZTogPHN0cmluZz51bmRlZmluZWQsICAgIFxyXG4gICAgcGVybWlzc2lvblJvbHVVcGRhdGluZzogPGJvb2xlYW4+ZmFsc2VcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICAgIHJvbGVMb2FkOigpID0+IHtcclxuICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gdHJ1ZVxyXG4gICAgICBmb3JrSm9pbiAoICAgICAgICBcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnBlcm1pc3Npb24uZ2V0QWxsKCksXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlLmdldFdpdGhQZXJtaXNzaW9uQnlSb2xlQ29kZSh0aGlzLnByaVZhci5yb2xlQ29kZSkgICAgICAgIFxyXG4gICAgICApLnN1YnNjcmliZSh7XHJcbiAgICAgICAgbmV4dDogcmVzdWx0ID0+IHsgICAgICAgICAgIFxyXG4gICAgICAgICAgY29uc3QgcGVybWlzc2lvbnM6IGlQZXJtaXNzaW9uW10gPSByZXN1bHRbMF0gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgY29uc3QgY3VycmVudFJvbGVzID0gcmVzdWx0WzFdXHJcbiAgICAgICAgICBsZXQgY3VycmVudFJvbGU6IGlSb2xlID0gdW5kZWZpbmVkXHJcbiAgICAgICAgICBcclxuICAgICAgICAgIGNvbnN0IF9wZXJtaXNzaW9uczogYW55W10gPSBbXVxyXG5cclxuICAgICAgICAgIGlmIChjdXJyZW50Um9sZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjdXJyZW50Um9sZSA9IHJlc3VsdFsxXVswXVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBDZXQgcsO0bGUgbidleGlzdGUgcGFzYClcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLi8nXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGV9KVxyXG4gICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5yb2xlID0gY3VycmVudFJvbGVcclxuICAgICAgICAgIHRoaXMudWlWYXIuZm9ybS5wYXRjaFZhbHVlKHRoaXMudWlWYXIucm9sZSlcclxuXHJcbiAgICAgICAgICBsZXQgX3Blcm1pc3Npb246IGFueSAgICAgICAgICBcclxuICAgICAgICAgIGxldCBwcmV2aW91c05hbWVSb290OiBzdHJpbmcgPSB1bmRlZmluZWRcclxuICAgICAgICAgIHBlcm1pc3Npb25zLmZvckVhY2goKHBlcm1pc3Npb246IGlQZXJtaXNzaW9uKSA9PiB7ICAgICAgICAgIFxyXG4gICAgICAgICAgICAvL3NwbGl0IHRvIHVwcGVyIGNhc2VcclxuICAgICAgICAgICAgY29uc3QgbmFtZXMgPSBwZXJtaXNzaW9uLmNvZGUuc3BsaXQoLyg/PVtBLVpdKS8pXHJcbiAgICAgICAgICAgIGNvbnN0IG5hbWVSb290ID0gbmFtZXNbMF1cclxuXHJcbiAgICAgICAgICAgIGxldCBmb3VuZDphbnkgPSBmYWxzZVxyXG4gICAgICAgICAgICBsZXQgcm9sZVBlcm1pc3Npb246IGlSb2xlUGVybWlzc2lvblxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYgKGN1cnJlbnRSb2xlKSB7XHJcbiAgICAgICAgICAgICAgcm9sZVBlcm1pc3Npb24gPSBjdXJyZW50Um9sZS5yb2xlUGVybWlzc2lvbnMuZmluZChpdGVtID0+IGl0ZW0ucGVybWlzc2lvbklkID09IHBlcm1pc3Npb24uaWQpXHJcbiAgICAgICAgICAgICAgaWYgKHJvbGVQZXJtaXNzaW9uKSB7IFxyXG4gICAgICAgICAgICAgICAgZm91bmQgPSB0cnVlIFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBjb25zdCBwZXJtaXNzaW9uQ29kZSA9IHBlcm1pc3Npb24uY29kZS5zbGljZShuYW1lUm9vdC5sZW5ndGgpXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZiAobmFtZVJvb3QgPT09IHByZXZpb3VzTmFtZVJvb3QpIHtcclxuICAgICAgICAgICAgICBfcGVybWlzc2lvbi5jaGlsZHJlbi5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGlkOiBwZXJtaXNzaW9uLmlkLCAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIGNoZWNrZWQ6IGZvdW5kLFxyXG4gICAgICAgICAgICAgICAgcm9sZVBlcm1pc3Npb246IHJvbGVQZXJtaXNzaW9uLFxyXG4gICAgICAgICAgICAgICAgY29kZTogcGVybWlzc2lvbkNvZGVcclxuICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHByZXZpb3VzTmFtZVJvb3QgPSBuYW1lUm9vdFxyXG4gICAgICAgICAgICAgIGlmIChfcGVybWlzc2lvbikgeyBfcGVybWlzc2lvbnMucHVzaChfcGVybWlzc2lvbil9XHJcbiAgICAgICAgICAgICAgX3Blcm1pc3Npb24gPSB7fSAgICAgICAgIFxyXG4gICAgICAgICAgICAgIF9wZXJtaXNzaW9uLmNvZGUgPSBuYW1lUm9vdFxyXG4gICAgICAgICAgICAgIF9wZXJtaXNzaW9uLmNoaWxkcmVuID0gW11cclxuICAgICAgICAgICAgICBfcGVybWlzc2lvbi5jaGlsZHJlbi5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGlkOiBwZXJtaXNzaW9uLmlkLFxyXG4gICAgICAgICAgICAgICAgY2hlY2tlZDogZm91bmQsXHJcbiAgICAgICAgICAgICAgICByb2xlUGVybWlzc2lvbjogcm9sZVBlcm1pc3Npb24sXHJcbiAgICAgICAgICAgICAgICBjb2RlOiBwZXJtaXNzaW9uQ29kZVxyXG4gICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0gICAgICAgICAgXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgaWYgKF9wZXJtaXNzaW9uKSB7IF9wZXJtaXNzaW9ucy5wdXNoKF9wZXJtaXNzaW9uKX1cclxuICBcclxuICAgICAgICAgIHRoaXMudWlWYXIucGVybWlzc2lvbkdyb3VwcyA9IF9wZXJtaXNzaW9ucyAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMudWlWYXIubG9hZGluZyA9IGZhbHNlXHJcblxyXG4gICAgICAgICAgLy8gY29uc29sZS5sb2coJ2N1cnJlbnRSb2xlJywgY3VycmVudFJvbGUpXHJcbiAgICAgICAgfSxcclxuICAgICAgICBjb21wbGV0ZTogKCkgPT4ge1xyXG4gICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgdGl0bGU6ICdOb3V2ZWF1IHLDtGxlJyxcclxuICAgIHBlcm1pc3Npb25Hcm91cHM6IDxhbnk+dW5kZWZpbmVkLFxyXG4gICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICByb2xlSXNOZXc6IGZhbHNlLFxyXG4gICAgcm9sZTogPGlSb2xlPnVuZGVmaW5lZCxcclxuICAgIGZvcm06IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoeyAgICAgIFxyXG4gICAgICBjb2RlOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLm1pbkxlbmd0aCgzKV1dLFxyXG4gICAgICBkZXNjcmlwdGlvbjogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjAwKV1dICAgICAgXHJcbiAgICB9KVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgICByb2xlU2F2ZTooKSA9PiB7ICAgICAgXHJcbiAgICAgIGNvbnN0IHJvbGVUb1NhdmU6IGlSb2xlID0gdGhpcy51aVZhci5mb3JtLnZhbHVlXHJcblxyXG4gICAgICBpZiAoIXRoaXMudWlWYXIuZm9ybS52YWxpZCkge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignVmV1aWxsZXogY29ycmlnZXIgbGVzIGVycmV1cnMgYXZhbnQgZGUgc2F1dmVnYXJkZXInKVxyXG4gICAgICAgIGZvZlV0aWxzRm9ybS52YWxpZGF0ZUFsbEZpZWxkcyh0aGlzLnVpVmFyLmZvcm0pXHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChyb2xlVG9TYXZlLmNvZGUudG9Mb3dlckNhc2UoKSA9PSAnbmV3Jykge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignRMOpc29sw6ksIGNlIGNvZGUgZXN0IHLDqXNlcnbDqScpXHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnVpVmFyLnJvbGVJc05ldykgeyAgICAgICAgXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlLmNyZWF0ZShyb2xlVG9TYXZlKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKChuZXdSb2xlOiBpUm9sZSkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1LDtGxlIHNhdXbDqScsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IDEwMDB9KVxyXG4gICAgICAgICAgdGhpcy5wcmlWYXIucm9sZUNvZGUgPSBuZXdSb2xlLmNvZGVcclxuICAgICAgICAgIHRoaXMudWlWYXIudGl0bGUgPSAnTW9kaWZpY2F0aW9uIGRlIHLDtGxlJ1xyXG4gICAgICAgICAgdGhpcy51aVZhci5yb2xlSXNOZXcgPSBmYWxzZVxyXG4gICAgICAgICAgdGhpcy5wcml2RnVuYy5yb2xlTG9hZCgpICAgICAgICAgIFxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcm9sZVRvU2F2ZS5pZCA9IHRoaXMudWlWYXIucm9sZS5pZCBcclxuICAgICAgXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlLnVwZGF0ZShyb2xlVG9TYXZlKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnUsO0bGUgc2F1dsOpJywge211c3REaXNhcHBlYXJBZnRlcjogMTAwMH0pXHJcbiAgICAgICAgfSkgXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICByb2xlQ2FuY2VsOigpID0+IHtcclxuICAgICAgdGhpcy51aVZhci5wZXJtaXNzaW9uR3JvdXBzID0gbnVsbCAgICAgIFxyXG4gICAgICB0aGlzLnByaXZGdW5jLnJvbGVMb2FkKClcclxuICAgIH0sXHJcbiAgICByb2xlRGVsZXRlOigpID0+IHtcclxuICAgICAgdGhpcy5mb2ZEaWFsb2dTZXJ2aWNlLm9wZW5ZZXNObyh7ICAgICAgICBcclxuICAgICAgICBxdWVzdGlvbjogJ1ZvdWxleiB2b3VzIHZyYWltZW50IHN1cHByaW1lciBsZSByw7RsZSA/J1xyXG4gICAgICB9KS50aGVuKHllcyA9PiB7XHJcbiAgICAgICAgaWYgKHllcykge1xyXG4gICAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlLmRlbGV0ZSh0aGlzLnVpVmFyLnJvbGUpXHJcbiAgICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdSw7RsZSBzdXBwcmltw6knKVxyXG4gICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy4uLyddLCB7cmVsYXRpdmVUbzogdGhpcy5hY3RpdmF0ZWRSb3V0ZX0pXHJcbiAgICAgICAgICB9KSBcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgcm9sZVBlcm1pc3Npb25TYXZlOiAocGVybWlzc2lvbjogYW55KSA9PiB7XHJcbiAgICAgIGNvbnN0IHJvbGVQZXJtaXNzaW9uID0gcGVybWlzc2lvbi5yb2xlUGVybWlzc2lvblxyXG5cclxuICAgICAgLy8gVGhlIHJvbGVQZXJtaXNzaW9uIG11c3QgYmUgZGVsZXRlZFxyXG4gICAgICBpZiAocm9sZVBlcm1pc3Npb24pIHtcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnJvbGVQZXJtaXNzaW9uLmRlbGV0ZShyb2xlUGVybWlzc2lvbilcclxuICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAudGhlbigocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgIHBlcm1pc3Npb24ucm9sZVBlcm1pc3Npb24gPSBudWxsXHJcbiAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc2F2ZUlzRG9uZSgpXHJcbiAgICAgICAgfSkgICAgICBcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyBUaGUgcm9sZVBlcm1pc3Npb24gbXVzdCBiZSBjcmVhdGVkXHJcbiAgICAgICAgY29uc3Qgcm9sZVBlcm1pc3Npb25Ub1NhdmU6IGlSb2xlUGVybWlzc2lvbiA9IHsgICAgICAgIFxyXG4gICAgICAgICAgcGVybWlzc2lvbklkOiBwZXJtaXNzaW9uLmlkLFxyXG4gICAgICAgICAgcm9sZUlkOiB0aGlzLnVpVmFyLnJvbGUuaWRcclxuICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlUGVybWlzc2lvbi5jcmVhdGUocm9sZVBlcm1pc3Npb25Ub1NhdmUpXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4oKHJlc3VsdDogYW55KSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnByaVZhci5wZXJtaXNzaW9uUm9sdVVwZGF0aW5nID0gZmFsc2VcclxuICAgICAgICAgIHBlcm1pc3Npb24ucm9sZVBlcm1pc3Npb24gPSByZXN1bHRcclxuICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zYXZlSXNEb25lKClcclxuICAgICAgICB9KSAgICAgIFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLnBhcmFtTWFwLnN1YnNjcmliZShwYXJhbXMgPT4ge1xyXG4gICAgICBjb25zdCBjb2RlID0gcGFyYW1zLmdldCgnY29kZScpXHJcbiAgICAgIHRoaXMucHJpVmFyLnJvbGVDb2RlID0gY29kZVxyXG4gICAgICBcclxuICAgICAgaWYgKGNvZGUpIHtcclxuICAgICAgICBpZiAoY29kZS50b0xvd2VyQ2FzZSgpID09ICduZXcnKSB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnJvbGVJc05ldyA9IHRydWVcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy51aVZhci50aXRsZSA9ICdNb2RpZmljYXRpb24gZGUgcsO0bGUnXHJcbiAgICAgICAgICB0aGlzLnByaXZGdW5jLnJvbGVMb2FkKClcclxuICAgICAgICB9XHJcbiAgICAgIH0gICAgICBcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcbiIsIjxtYXQtY2FyZCBjbGFzcz1cImZvZi1oZWFkZXJcIj4gICAgICAgIFxyXG4gIDxoMz57eyB1aVZhci50aXRsZSB9fTwvaDM+IFxyXG4gIDxkaXYgY2xhc3M9XCJmb2YtdG9vbGJhclwiPlxyXG4gICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b25cclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnJvbGVDYW5jZWwoKVwiPkFubnVsZXI8L2J1dHRvbj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwid2FyblwiXHJcbiAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5yb2xlRGVsZXRlKClcIj5TdXBwcmltZXI8L2J1dHRvbj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwiYWNjZW50XCJcclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnJvbGVTYXZlKClcIj5cclxuICAgICAgRW5yZWdpc3RlcjwvYnV0dG9uPiAgICBcclxuICA8L2Rpdj4gXHJcbjwvbWF0LWNhcmQ+XHJcbjxkaXYgY2xhc3M9XCJtYWluXCI+ICBcclxuICA8ZGl2ICpuZ0lmPVwiIXVpVmFyLmxvYWRpbmdcIiBjbGFzcz1cImZvZi1mYWRlLWluIGRldGFpbFwiPiAgICBcclxuICAgIDxmb3JtIFtmb3JtR3JvdXBdPVwidWlWYXIuZm9ybVwiPlxyXG4gICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgPGlucHV0IG1hdElucHV0IHJlcXVpcmVkXHJcbiAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJjb2RlXCJcclxuICAgICAgICAgIHBsYWNlaG9sZGVyPVwiQ29kZSBkdSByw7RsZVwiIHZhbHVlPVwiXCI+XHJcbiAgICAgICAgPG1hdC1lcnJvciAqbmdJZj1cInVpVmFyLmZvcm0uZ2V0KCdjb2RlJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgTGUgY29kZSBkdSByw7RsZSBlc3Qgb2JsaWdhdG9pcmUgZXQgZG9pdCDDqnRyZSBjb21wb3PDqSBkZSBhdSBtb2lucyAzIGNhcmFjdMOocmVzXHJcbiAgICAgICAgPC9tYXQtZXJyb3I+XHJcbiAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgIFxyXG4gICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgPHRleHRhcmVhIG1hdElucHV0IFxyXG4gICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiZGVzY3JpcHRpb25cIlxyXG4gICAgICAgICAgcm93cz1cIjNcIlxyXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJEZXNjcmlwdGlvblwiPjwvdGV4dGFyZWE+XHJcbiAgICAgICAgPG1hdC1oaW50PkTDqWNyaXZleiBzdWNjaW50ZW1lbnQgbGUgcsO0bGU8L21hdC1oaW50PlxyXG4gICAgICAgIDxtYXQtaGludCBhbGlnbj1cImVuZFwiPnt7dWlWYXIuZm9ybS5nZXQoJ2Rlc2NyaXB0aW9uJykudmFsdWU/Lmxlbmd0aH19IC9cclxuICAgICAgICAgIDIwMDwvbWF0LWhpbnQ+XHJcbiAgICAgICAgPG1hdC1lcnJvciAqbmdJZj1cInVpVmFyLmZvcm0uZ2V0KCdkZXNjcmlwdGlvbicpLmludmFsaWRcIj5cclxuICAgICAgICAgIExhIGRlc2NyaXB0aW9uIG5lIHBldXQgcGFzIGV4Y8OpZGVyIDIwMCBjYXJhY3TDqHJlc1xyXG4gICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG4gICAgPC9mb3JtPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgKm5nSWY9XCJ1aVZhci5sb2FkaW5nXCIgY2xhc3M9XCJmb2YtbG9hZGluZ1wiPlxyXG4gICAgPG1hdC1zcGlubmVyIGRpYW1ldGVyPTIwPjwvbWF0LXNwaW5uZXI+IDxzcGFuPkNoYXJnZW1lbnRzIGRlcyByb2xlcyBldCBkZXMgYXV0aG9yaXNhdGlvbnMuLi48L3NwYW4+XHJcbiAgPC9kaXY+XHJcbiAgPG1hdC1jYXJkIGNsYXNzPVwiZ3JvdXAtY2FyZCBmb2YtZmFkZS1pblwiICpuZ0Zvcj1cImxldCBncm91cCBvZiB1aVZhci5wZXJtaXNzaW9uR3JvdXBzXCI+XHJcbiAgICA8bWF0LWNhcmQtaGVhZGVyPiAgICAgIFxyXG4gICAgICA8bWF0LWNhcmQtdGl0bGU+e3sgZ3JvdXAuY29kZSB9fTwvbWF0LWNhcmQtdGl0bGU+ICAgICAgXHJcbiAgICA8L21hdC1jYXJkLWhlYWRlcj5cclxuICAgIDxtYXQtY2FyZC1jb250ZW50ICpuZ0lmPVwiZ3JvdXAuY2hpbGRyZW5cIj5cclxuICAgICAgPGRpdiAqbmdGb3I9XCJsZXQgcGVybWlzc2lvbiBvZiBncm91cC5jaGlsZHJlblwiPlxyXG4gICAgICAgIDxtYXQtY2hlY2tib3hcclxuICAgICAgICAgIChjaGFuZ2UpPVwidWlBY3Rpb24ucm9sZVBlcm1pc3Npb25TYXZlKHBlcm1pc3Npb24pXCIgICAgICAgICAgXHJcbiAgICAgICAgICBbKG5nTW9kZWwpXT1cInBlcm1pc3Npb24uY2hlY2tlZFwiXHJcbiAgICAgICAgICBbY2hlY2tlZF09XCJwZXJtaXNzaW9uLmNoZWNrZWRcIj57eyBwZXJtaXNzaW9uLmNvZGUgfX08L21hdC1jaGVja2JveD5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L21hdC1jYXJkLWNvbnRlbnQ+XHJcbiAgICA8bmctdGVtcGxhdGUgI2xvYWRpbmc+XHJcbiAgICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0zMD48L21hdC1zcGlubmVyPlxyXG4gICAgPC9uZy10ZW1wbGF0ZT4gICAgXHJcbiAgPC9tYXQtY2FyZD5cclxuPC9kaXY+XHJcblxyXG48Zm9mLWVudGl0eS1mb290ZXJcclxuICBbZW50aXR5QmFzZV09XCJ1aVZhci5yb2xlXCI+XHJcbjwvZm9mLWVudGl0eS1mb290ZXI+Il19
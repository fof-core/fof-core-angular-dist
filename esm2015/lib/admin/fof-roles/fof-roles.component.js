import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
import * as i4 from "@angular/material/card";
import * as i5 from "@angular/material/button";
import * as i6 from "@angular/router";
import * as i7 from "@angular/common";
import * as i8 from "@angular/material/table";
import * as i9 from "@angular/material/sort";
import * as i10 from "@angular/material/paginator";
import * as i11 from "@angular/material/progress-spinner";
function FofRolesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 13);
    i0.ɵɵelement(1, "mat-spinner", 14);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des roles...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofRolesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 15);
    i0.ɵɵtext(1, "Code");
    i0.ɵɵelementEnd();
} }
function FofRolesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r21 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r21.code);
} }
function FofRolesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Description");
    i0.ɵɵelementEnd();
} }
function FofRolesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r22 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r22.description);
} }
function FofRolesComponent_tr_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 18);
} }
function FofRolesComponent_tr_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 19);
} if (rf & 2) {
    const row_r23 = ctx.$implicit;
    i0.ɵɵproperty("routerLink", row_r23.code);
} }
const _c0 = function () { return [5, 10, 25, 100]; };
export class FofRolesComponent {
    constructor(fofPermissionService, fofNotificationService, breakpointObserver) {
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['code', 'description'],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    ngOnInit() {
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe((state) => {
            if (state.matches) {
                // XSmall
                this.uiVar.displayedColumns = ['code'];
            }
            else {
                // > XSmall
                this.uiVar.displayedColumns = ['code', 'description'];
            }
        });
    }
    ngAfterViewInit() {
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(() => {
            this.uiVar.isLoadingResults = true;
            this.uiVar.pageSize = this.paginator.pageSize;
            return this.fofPermissionService.role.search(null, this.uiVar.pageSize, this.paginator.pageIndex, this.sort.active, this.sort.direction);
        }), map((search) => {
            this.uiVar.isLoadingResults = false;
            this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(() => {
            this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(data => this.uiVar.data = data);
    }
    ngOnDestroy() {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    }
}
FofRolesComponent.ɵfac = function FofRolesComponent_Factory(t) { return new (t || FofRolesComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver)); };
FofRolesComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofRolesComponent, selectors: [["fof-roles"]], viewQuery: function FofRolesComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(MatPaginator, true);
        i0.ɵɵviewQuery(MatSort, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
    } }, decls: 18, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "code", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "code"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "description"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function FofRolesComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-card", 0);
        i0.ɵɵelementStart(1, "h3");
        i0.ɵɵtext(2, "R\u00F4les");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "a", 1);
        i0.ɵɵelementStart(4, "span");
        i0.ɵɵtext(5, "Ajouter un r\u00F4le");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "div", 2);
        i0.ɵɵtemplate(7, FofRolesComponent_div_7_Template, 4, 0, "div", 3);
        i0.ɵɵelementStart(8, "table", 4);
        i0.ɵɵelementContainerStart(9, 5);
        i0.ɵɵtemplate(10, FofRolesComponent_th_10_Template, 2, 0, "th", 6);
        i0.ɵɵtemplate(11, FofRolesComponent_td_11_Template, 2, 1, "td", 7);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(12, 8);
        i0.ɵɵtemplate(13, FofRolesComponent_th_13_Template, 2, 0, "th", 9);
        i0.ɵɵtemplate(14, FofRolesComponent_td_14_Template, 2, 1, "td", 7);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵtemplate(15, FofRolesComponent_tr_15_Template, 1, 0, "tr", 10);
        i0.ɵɵtemplate(16, FofRolesComponent_tr_16_Template, 1, 1, "tr", 11);
        i0.ɵɵelementEnd();
        i0.ɵɵelement(17, "mat-paginator", 12);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("routerLink", "/admin/roles/new");
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("dataSource", ctx.uiVar.data);
        i0.ɵɵadvance(7);
        i0.ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", i0.ɵɵpureFunction0(8, _c0))("pageSize", ctx.uiVar.pageSize);
    } }, directives: [i4.MatCard, i5.MatAnchor, i6.RouterLinkWithHref, i7.NgIf, i8.MatTable, i9.MatSort, i8.MatColumnDef, i8.MatHeaderCellDef, i8.MatCellDef, i8.MatHeaderRowDef, i8.MatRowDef, i10.MatPaginator, i11.MatSpinner, i8.MatHeaderCell, i9.MatSortHeader, i8.MatCell, i8.MatHeaderRow, i8.MatRow, i6.RouterLink], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofRolesComponent, [{
        type: Component,
        args: [{
                selector: 'fof-roles',
                templateUrl: './fof-roles.component.html',
                styleUrls: ['./fof-roles.component.scss'],
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXJvbGVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2FkbWluL2ZvZi1yb2xlcy9mb2Ytcm9sZXMuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi1yb2xlcy9mb2Ytcm9sZXMuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBbUMsU0FBUyxFQUE0QixNQUFNLGVBQWUsQ0FBQTtBQUkvRyxPQUFPLEVBQUUsWUFBWSxFQUFvQixNQUFNLDZCQUE2QixDQUFBO0FBQzVFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQTtBQUNoRCxPQUFPLEVBQUUsS0FBSyxFQUFjLEVBQUUsSUFBSSxZQUFZLEVBQWdCLE1BQU0sTUFBTSxDQUFBO0FBQzFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUV0RSxPQUFPLEVBQXNCLFdBQVcsRUFBbUIsTUFBTSxxQkFBcUIsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7SUNDcEYsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLHdDQUF3QjtJQUFBLGlCQUFPO0lBQy9FLGlCQUFNOzs7SUFNRiw4QkFBc0Q7SUFBQSxvQkFBSTtJQUFBLGlCQUFLOzs7SUFDL0QsOEJBQW1DO0lBQUEsWUFBWTtJQUFBLGlCQUFLOzs7SUFBakIsZUFBWTtJQUFaLGtDQUFZOzs7SUFJL0MsOEJBQXNDO0lBQUEsMkJBQVc7SUFBQSxpQkFBSzs7O0lBQ3RELDhCQUFtQztJQUFBLFlBQW1CO0lBQUEsaUJBQUs7OztJQUF4QixlQUFtQjtJQUFuQix5Q0FBbUI7OztJQUd4RCx5QkFBa0U7OztJQUNsRSx5QkFDOEQ7OztJQUR6Qix5Q0FBdUI7OztBRFZoRSxNQUFNLE9BQU8saUJBQWlCO0lBSTVCLFlBQ1Usb0JBQTBDLEVBQzFDLHNCQUE4QyxFQUM5QyxrQkFBc0M7UUFGdEMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUMxQywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFLaEQsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRztZQUNmLHFCQUFxQixFQUFnQixTQUFTO1NBQy9DLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHLEVBQ2xCLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsZ0JBQWdCLEVBQVksQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDO1lBQ25ELElBQUksRUFBVyxFQUFFO1lBQ2pCLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLFFBQVEsRUFBRSxDQUFDO1lBQ1gsZ0JBQWdCLEVBQUUsSUFBSTtTQUN2QixDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRyxFQUNqQixDQUFBO0lBbkJELENBQUM7SUFvQkQsaUJBQWlCO0lBQ2pCLFFBQVE7UUFDTixJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzthQUN0RixTQUFTLENBQUMsQ0FBQyxLQUFzQixFQUFFLEVBQUU7WUFDcEMsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO2dCQUNqQixTQUFTO2dCQUNULElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTthQUN2QztpQkFBTTtnQkFDTCxXQUFXO2dCQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLENBQUE7YUFDdEQ7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCxlQUFlO1FBQ2Isb0VBQW9FO1FBQ3BFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQTtRQUVsRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7YUFDN0MsSUFBSSxDQUNILFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUE7WUFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUE7WUFDN0MsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQ3JFLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDcEUsQ0FBQyxDQUFDLEVBQ0YsR0FBRyxDQUFDLENBQUMsTUFBeUIsRUFBRSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUE7WUFDdkMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQTtZQUNuQyxPQUFPLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUN6QixDQUFDLENBQUMsQ0FDSCxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxDQUFBO0lBQy9DLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFO1lBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUFFO0lBQzVGLENBQUM7O2tGQXZFVSxpQkFBaUI7c0RBQWpCLGlCQUFpQjt1QkFDakIsWUFBWTt1QkFDWixPQUFPOzs7Ozs7UUNwQnBCLG1DQUNFO1FBQUEsMEJBQUk7UUFBQSwwQkFBSztRQUFBLGlCQUFLO1FBQ2QsNEJBRUU7UUFBQSw0QkFBTTtRQUFBLG9DQUFlO1FBQUEsaUJBQU87UUFDOUIsaUJBQUk7UUFDTixpQkFBVztRQUVYLDhCQUVFO1FBQUEsa0VBQ0U7UUFHRixnQ0FHRTtRQUFBLGdDQUNFO1FBQUEsa0VBQXNEO1FBQ3RELGtFQUFtQztRQUNyQywwQkFBZTtRQUVmLGlDQUNFO1FBQUEsa0VBQXNDO1FBQ3RDLGtFQUFtQztRQUNyQywwQkFBZTtRQUVmLG1FQUE2RDtRQUM3RCxtRUFDeUQ7UUFDM0QsaUJBQVE7UUFFUixxQ0FFOEM7UUFFaEQsaUJBQU07O1FBakNGLGVBQWlDO1FBQWpDLCtDQUFpQztRQU9VLGVBQThCO1FBQTlCLGlEQUE4QjtRQUkxRCxlQUF5QjtRQUF6QiwyQ0FBeUI7UUFhckIsZUFBeUM7UUFBekMsNERBQXlDO1FBRTFELGVBQXNEO1FBQXRELDZEQUFzRDtRQUczQyxlQUE4QjtRQUE5QixnREFBOEIsK0NBQUEsZ0NBQUE7O2tERGRsQyxpQkFBaUI7Y0FON0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxXQUFXO2dCQUNyQixXQUFXLEVBQUUsNEJBQTRCO2dCQUN6QyxTQUFTLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQzthQUUxQzs7a0JBRUUsU0FBUzttQkFBQyxZQUFZOztrQkFDdEIsU0FBUzttQkFBQyxPQUFPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IEZvZlBlcm1pc3Npb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IGlSb2xlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yLCBNYXRQYWdpbmF0b3JJbnRsIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJ1xyXG5pbXBvcnQgeyBNYXRTb3J0IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc29ydCdcclxuaW1wb3J0IHsgbWVyZ2UsIE9ic2VydmFibGUsIG9mIGFzIG9ic2VydmFibGVPZiwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCBzdGFydFdpdGgsIHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJ1xyXG5pbXBvcnQgeyBpZm9mU2VhcmNoIH0gZnJvbSAnLi4vLi4vY29yZS9jb3JlLmludGVyZmFjZSdcclxuaW1wb3J0IHsgQnJlYWtwb2ludE9ic2VydmVyLCBCcmVha3BvaW50cywgQnJlYWtwb2ludFN0YXRlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCdcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1yb2xlcycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZvZi1yb2xlcy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9mLXJvbGVzLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgLy8gY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2hcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZlJvbGVzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcclxuICBAVmlld0NoaWxkKE1hdFBhZ2luYXRvcikgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3JcclxuICBAVmlld0NoaWxkKE1hdFNvcnQpIHNvcnQ6IE1hdFNvcnRcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvZlBlcm1pc3Npb25TZXJ2aWNlOiBGb2ZQZXJtaXNzaW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgYnJlYWtwb2ludE9ic2VydmVyOiBCcmVha3BvaW50T2JzZXJ2ZXJcclxuICApIHsgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgYnJlYWtwb2ludE9ic2VydmVyU3ViOiA8U3Vic2NyaXB0aW9uPnVuZGVmaW5lZCxcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICB9XHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlWYXIgPSB7ICAgIFxyXG4gICAgZGlzcGxheWVkQ29sdW1uczogPHN0cmluZ1tdPlsnY29kZScsICdkZXNjcmlwdGlvbiddLFxyXG4gICAgZGF0YTogPGlSb2xlW10+W10sXHJcbiAgICByZXN1bHRzTGVuZ3RoOiAwLFxyXG4gICAgcGFnZVNpemU6IDUsXHJcbiAgICBpc0xvYWRpbmdSZXN1bHRzOiB0cnVlXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcclxuICB9XHJcbiAgLy8gQW5ndWxhciBldmVudHNcclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1YiA9IHRoaXMuYnJlYWtwb2ludE9ic2VydmVyLm9ic2VydmUoQnJlYWtwb2ludHMuWFNtYWxsKVxyXG4gICAgLnN1YnNjcmliZSgoc3RhdGU6IEJyZWFrcG9pbnRTdGF0ZSkgPT4geyAgICAgIFxyXG4gICAgICBpZiAoc3RhdGUubWF0Y2hlcykge1xyXG4gICAgICAgIC8vIFhTbWFsbFxyXG4gICAgICAgIHRoaXMudWlWYXIuZGlzcGxheWVkQ29sdW1ucyA9IFsnY29kZSddXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gPiBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ2NvZGUnLCAnZGVzY3JpcHRpb24nXVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0gIFxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7ICAgIFxyXG4gICAgLy8gSWYgdGhlIHVzZXIgY2hhbmdlcyB0aGUgc29ydCBvcmRlciwgcmVzZXQgYmFjayB0byB0aGUgZmlyc3QgcGFnZS5cclxuICAgIHRoaXMuc29ydC5zb3J0Q2hhbmdlLnN1YnNjcmliZSgoKSA9PiB0aGlzLnBhZ2luYXRvci5wYWdlSW5kZXggPSAwKVxyXG5cclxuICAgIG1lcmdlKHRoaXMuc29ydC5zb3J0Q2hhbmdlLCB0aGlzLnBhZ2luYXRvci5wYWdlKVxyXG4gICAgICAucGlwZShcclxuICAgICAgICBzdGFydFdpdGgoe30pLFxyXG4gICAgICAgIHN3aXRjaE1hcCgoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSB0cnVlICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5wYWdlU2l6ZSA9IHRoaXMucGFnaW5hdG9yLnBhZ2VTaXplXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlLnNlYXJjaCAobnVsbCwgdGhpcy51aVZhci5wYWdlU2l6ZSwgXHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCwgdGhpcy5zb3J0LmFjdGl2ZSwgdGhpcy5zb3J0LmRpcmVjdGlvbilcclxuICAgICAgICB9KSxcclxuICAgICAgICBtYXAoKHNlYXJjaDogaWZvZlNlYXJjaDxpUm9sZT4pID0+IHsgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMudWlWYXIucmVzdWx0c0xlbmd0aCA9IHNlYXJjaC50b3RhbFxyXG4gICAgICAgICAgcmV0dXJuIHNlYXJjaC5kYXRhXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgY2F0Y2hFcnJvcigoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSBmYWxzZSAgICAgICAgICBcclxuICAgICAgICAgIHJldHVybiBvYnNlcnZhYmxlT2YoW10pXHJcbiAgICAgICAgfSlcclxuICAgICAgKS5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnVpVmFyLmRhdGEgPSBkYXRhKVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViKSB7IHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1Yi51bnN1YnNjcmliZSgpIH1cclxuICB9XHJcblxyXG59XHJcbiIsIjxtYXQtY2FyZCBjbGFzcz1cImZvZi1oZWFkZXJcIj4gICAgICAgIFxyXG4gIDxoMz5Sw7RsZXM8L2gzPiAgIFxyXG4gIDxhIG1hdC1zdHJva2VkLWJ1dHRvbiBjb2xvcj1cImFjY2VudFwiIFxyXG4gICAgW3JvdXRlckxpbmtdPVwiJy9hZG1pbi9yb2xlcy9uZXcnXCI+ICAgIFxyXG4gICAgPHNwYW4+QWpvdXRlciB1biByw7RsZTwvc3Bhbj5cclxuICA8L2E+ICAgIFxyXG48L21hdC1jYXJkPlxyXG5cclxuPGRpdiBjbGFzcz1cImZvZi10YWJsZS1jb250YWluZXIgbWF0LWVsZXZhdGlvbi16MlwiPlxyXG5cclxuICA8ZGl2IGNsYXNzPVwidGFibGUtbG9hZGluZy1zaGFkZSBmb2YtbG9hZGluZ1wiICpuZ0lmPVwidWlWYXIuaXNMb2FkaW5nUmVzdWx0c1wiPlxyXG4gICAgPG1hdC1zcGlubmVyIGRpYW1ldGVyPTIwPjwvbWF0LXNwaW5uZXI+IDxzcGFuPkNoYXJnZW1lbnRzIGRlcyByb2xlcy4uLjwvc3Bhbj5cclxuICA8L2Rpdj5cclxuXHJcbiAgPHRhYmxlIG1hdC10YWJsZSBbZGF0YVNvdXJjZV09XCJ1aVZhci5kYXRhXCIgY2xhc3M9XCJkYXRhLXRhYmxlXCJcclxuICAgICAgICBtYXRTb3J0IG1hdFNvcnRBY3RpdmU9XCJjb2RlXCIgbWF0U29ydERpc2FibGVDbGVhciBtYXRTb3J0RGlyZWN0aW9uPVwiYXNjXCI+XHJcbiAgICBcclxuICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwiY29kZVwiPlxyXG4gICAgICA8dGggbWF0LWhlYWRlci1jZWxsIG1hdC1zb3J0LWhlYWRlciAqbWF0SGVhZGVyQ2VsbERlZj5Db2RlPC90aD5cclxuICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LmNvZGV9fTwvdGQ+XHJcbiAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImRlc2NyaXB0aW9uXCI+XHJcbiAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWY+RGVzY3JpcHRpb248L3RoPlxyXG4gICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cuZGVzY3JpcHRpb259fTwvdGQ+XHJcbiAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICA8dHIgbWF0LWhlYWRlci1yb3cgKm1hdEhlYWRlclJvd0RlZj1cInVpVmFyLmRpc3BsYXllZENvbHVtbnNcIj48L3RyPlxyXG4gICAgPHRyIG1hdC1yb3cgY2xhc3M9XCJmb2YtZWxlbWVudC1vdmVyXCIgW3JvdXRlckxpbmtdPVwicm93LmNvZGVcIlxyXG4gICAgICAqbWF0Um93RGVmPVwibGV0IHJvdzsgY29sdW1uczogdWlWYXIuZGlzcGxheWVkQ29sdW1ucztcIj48L3RyPlxyXG4gIDwvdGFibGU+XHJcblxyXG4gIDxtYXQtcGFnaW5hdG9yIFtsZW5ndGhdPVwidWlWYXIucmVzdWx0c0xlbmd0aFwiIFxyXG4gICAgW3BhZ2VTaXplT3B0aW9uc109XCJbNSwgMTAsIDI1LCAxMDBdXCJcclxuICAgIFtwYWdlU2l6ZV09XCJ1aVZhci5wYWdlU2l6ZVwiPjwvbWF0LXBhZ2luYXRvcj5cclxuXHJcbjwvZGl2PlxyXG5cclxuIl19
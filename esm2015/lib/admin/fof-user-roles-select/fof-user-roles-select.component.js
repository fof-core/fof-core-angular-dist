import { Component, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "../../permission/fof-permission.service";
import * as i3 from "../../core/notification/notification.service";
import * as i4 from "../../core/fof-error.service";
import * as i5 from "@angular/common";
import * as i6 from "../../components/fof-organizations-multi-select/fof-organizations-multi-select.component";
import * as i7 from "@angular/material/button";
import * as i8 from "@angular/material/progress-spinner";
import * as i9 from "@angular/material/checkbox";
import * as i10 from "@angular/forms";
function FofUserRolesSelectComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵelement(1, "mat-spinner", 7);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Rafraichissement des r\u00F4les...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofUserRolesSelectComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r60 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 8);
    i0.ɵɵelementStart(1, "mat-checkbox", 9);
    i0.ɵɵlistener("change", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_change_1_listener() { i0.ɵɵrestoreView(_r60); const role_r58 = ctx.$implicit; const ctx_r59 = i0.ɵɵnextContext(); return ctx_r59.uiAction.userRoleOrganizationSave(role_r58); })("ngModelChange", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r60); const role_r58 = ctx.$implicit; return role_r58.checked = $event; });
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "div", 10);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const role_r58 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", role_r58.checked)("checked", role_r58.checked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(role_r58.code);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(role_r58.description);
} }
export class FofUserRolesSelectComponent {
    constructor(matDialogRef, fofPermissionService, fofNotificationService, fofErrorService, data) {
        this.matDialogRef = matDialogRef;
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.fofErrorService = fofErrorService;
        this.userRoleOrganizations = undefined;
        // All private variables
        this.priVar = {
            userId: undefined,
            organizationId: undefined
        };
        // All private functions
        this.privFunc = {
            componenentDataRefresh: () => {
                if (this.userRoleOrganizations) {
                    // it's an update
                    this.uiVar.isCreation = false;
                    this.uiVar.title = `Modification des accès`;
                    // Only one organization possible in update
                    this.uiVar.organizationMultipleSelect = false;
                    if (this.userRoleOrganizations.length > 0) {
                        // we get only the first one since it's impossible to have mutiple organisations
                        this.priVar.organizationId = this.userRoleOrganizations[0].organization.id;
                        this.uiVar.selectedOrganisations = [];
                        this.uiVar.selectedOrganisations.push(this.userRoleOrganizations[0].organization);
                    }
                    if (this.uiVar.allRoles) {
                        this.uiVar.allRoles.forEach((role) => {
                            const userRoleOrganization = this.userRoleOrganizations.find(item => item.roleId == role.id);
                            if (userRoleOrganization) {
                                role.userRoleOrganization = userRoleOrganization;
                                role.checked = true;
                            }
                        });
                    }
                }
                else {
                    // it's a creation
                    this.uiVar.isCreation = true;
                    this.uiVar.title = `Creation d'un accès`;
                }
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            allRoles: undefined,
            organizationMultipleSelect: true,
            selectedOrganisations: [],
            title: `Creation d'un accès`,
            isCreation: false,
            loading: false
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: (organisations) => {
                this.uiVar.selectedOrganisations = organisations;
            },
            userRoleOrganizationSave: (role) => {
                const userRoleOrganization = role.userRoleOrganization;
                // if (userRoleOrganization) {
                //   this.fofPermissionService.userRoleOrganization.delete(userRoleOrganization)
                //   .toPromise()
                //   .then((result: any) => {          
                //     role.userRoleOrganization = null          
                //     this.fofNotificationService.saveIsDone()
                //   })      
                // } else {
                //   // The userRoleOrganization must be created
                //   const userRoleOrganizationToSave: iUserRoleOrganization = {        
                //     roleId: role.id,
                //     userId: this.uiVar.user.id
                //   }
                //   this.fofPermissionService.userRoleOrganization.create(userRoleOrganizationToSave)
                //   .toPromise()
                //   .then((result: any) => {          
                //     role.userRoleOrganization = result          
                //     this.fofNotificationService.saveIsDone()
                //   })      
                // }
            },
            save: () => {
                const userRoleOrganizations = [];
                let userRoleOrganization;
                if (!this.priVar.userId) {
                    this.fofNotificationService.error(`
          Problème avec les données<br>
          <small>Manque le user</small>
        `);
                }
                this.uiVar.selectedOrganisations.forEach(organisation => {
                    this.uiVar.allRoles.forEach((role) => {
                        if (role.checked) {
                            userRoleOrganization = {
                                userId: this.priVar.userId,
                                organizationId: organisation.id,
                                roleId: role.id
                            };
                            userRoleOrganizations.push(userRoleOrganization);
                        }
                    });
                });
                if (userRoleOrganizations.length === 0) {
                    this.fofNotificationService.error(`
          Vous devez sélectionner au moins<br>
          <ul>
            <li>Une organisation</li>
            <li>Un role</li>
          </ul>
        `, { mustDisappearAfter: -1 });
                    return;
                }
                this.uiVar.loading = true;
                if (this.uiVar.isCreation) {
                    this.fofPermissionService.userRoleOrganization.bulkCreate(userRoleOrganizations)
                        .toPromise()
                        .then(result => {
                        this.matDialogRef.close({ saved: true });
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                        // this.fofNotificationService.error(`
                        //   Erreur lors de la sauvegarde<br>
                        //   <small>${reason}</small>
                        // `)
                    })
                        .finally(() => {
                        this.uiVar.loading = false;
                    });
                }
                else {
                    this.fofPermissionService.user
                        .replaceUserRoleOrganization(userRoleOrganizations, this.priVar.userId, this.priVar.organizationId)
                        .toPromise()
                        .then(result => {
                        this.matDialogRef.close({ saved: true });
                    })
                        .catch(reason => {
                        this.fofNotificationService.error(`
            Erreur lors de la sauvegarde<br>
            <small>${reason}</small>
          `);
                    })
                        .finally(() => {
                        this.uiVar.loading = false;
                    });
                }
            }
        };
        this.userRoleOrganizations = data.userRoleOrganizations;
        this.uiVar.allRoles = data.roles;
        this.priVar.userId = data.userId;
        if (data.notSelectableOrganizations) {
            this.notSelectableOrganizations = data.notSelectableOrganizations;
        }
        if (this.uiVar.allRoles && this.uiVar.allRoles.length) {
            this.uiVar.allRoles.forEach((role) => {
                role.checked = false;
            });
        }
        this.privFunc.componenentDataRefresh();
    }
    // Angular events
    ngOnInit() {
    }
    ngOnChanges() {
        this.privFunc.componenentDataRefresh();
    }
}
FofUserRolesSelectComponent.ɵfac = function FofUserRolesSelectComponent_Factory(t) { return new (t || FofUserRolesSelectComponent)(i0.ɵɵdirectiveInject(i1.MatDialogRef), i0.ɵɵdirectiveInject(i2.FofPermissionService), i0.ɵɵdirectiveInject(i3.FofNotificationService), i0.ɵɵdirectiveInject(i4.FofErrorService), i0.ɵɵdirectiveInject(MAT_DIALOG_DATA)); };
FofUserRolesSelectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofUserRolesSelectComponent, selectors: [["fof-user-roles-select"]], inputs: { userRoleOrganizations: "userRoleOrganizations", notSelectableOrganizations: "notSelectableOrganizations" }, features: [i0.ɵɵNgOnChangesFeature], decls: 13, vars: 6, consts: [["class", "fof-loading", 4, "ngIf"], ["mat-dialog-title", ""], [3, "multiSelect", "selectedOrganisations", "notSelectableOrganizations", "selectedOrganizationsChange"], ["class", "fof-fade-in", 4, "ngFor", "ngForOf"], ["mat-flat-button", "", "mat-dialog-close", ""], ["mat-flat-button", "", "color", "primary", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in"], [3, "ngModel", "checked", "change", "ngModelChange"], [1, "role-hint"]], template: function FofUserRolesSelectComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵtemplate(0, FofUserRolesSelectComponent_div_0_Template, 4, 0, "div", 0);
        i0.ɵɵelementStart(1, "h2", 1);
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "mat-dialog-content");
        i0.ɵɵelementStart(4, "fof-core-fof-organizations-multi-select", 2);
        i0.ɵɵlistener("selectedOrganizationsChange", function FofUserRolesSelectComponent_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_4_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "h4");
        i0.ɵɵtext(6, "R\u00F4les");
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(7, FofUserRolesSelectComponent_div_7_Template, 5, 4, "div", 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "mat-dialog-actions");
        i0.ɵɵelementStart(9, "button", 4);
        i0.ɵɵtext(10, "Annuler");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(11, "button", 5);
        i0.ɵɵlistener("click", function FofUserRolesSelectComponent_Template_button_click_11_listener() { return ctx.uiAction.save(); });
        i0.ɵɵtext(12, "Enregistrer");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.uiVar.title);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("multiSelect", ctx.uiVar.organizationMultipleSelect)("selectedOrganisations", ctx.uiVar.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("ngForOf", ctx.uiVar.allRoles);
    } }, directives: [i5.NgIf, i1.MatDialogTitle, i1.MatDialogContent, i6.FofOrganizationsMultiSelectComponent, i5.NgForOf, i1.MatDialogActions, i7.MatButton, i1.MatDialogClose, i8.MatSpinner, i9.MatCheckbox, i10.NgControlStatus, i10.NgModel], styles: ["[_nghost-%COMP%]{position:relative}[_nghost-%COMP%]   .fof-loading[_ngcontent-%COMP%]{position:absolute;height:calc(100% + 15px);z-index:2}[_nghost-%COMP%]   .mat-dialog-actions[_ngcontent-%COMP%]{margin-top:1rem;display:flex;justify-content:flex-end}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofUserRolesSelectComponent, [{
        type: Component,
        args: [{
                selector: 'fof-user-roles-select',
                templateUrl: './fof-user-roles-select.component.html',
                styleUrls: ['./fof-user-roles-select.component.scss']
            }]
    }], function () { return [{ type: i1.MatDialogRef }, { type: i2.FofPermissionService }, { type: i3.FofNotificationService }, { type: i4.FofErrorService }, { type: undefined, decorators: [{
                type: Inject,
                args: [MAT_DIALOG_DATA]
            }] }]; }, { userRoleOrganizations: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXVzZXItcm9sZXMtc2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2FkbWluL2ZvZi11c2VyLXJvbGVzLXNlbGVjdC9mb2YtdXNlci1yb2xlcy1zZWxlY3QuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi11c2VyLXJvbGVzLXNlbGVjdC9mb2YtdXNlci1yb2xlcy1zZWxlY3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQWEsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBRzNFLE9BQU8sRUFBRSxlQUFlLEVBQWdCLE1BQU0sMEJBQTBCLENBQUE7Ozs7Ozs7Ozs7Ozs7SUNIeEUsOEJBQ0U7SUFBQSxpQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGtEQUE2QjtJQUFBLGlCQUFPO0lBQ3BGLGlCQUFNOzs7O0lBYUosOEJBQ0k7SUFBQSx1Q0FHMkI7SUFGekIsa05BQVUsbURBQXVDLElBQUMsa05BQUE7SUFFekIsWUFBZTtJQUFBLGlCQUFlO0lBQ3ZELCtCQUF1QjtJQUFBLFlBQXNCO0lBQUEsaUJBQU07SUFDekQsaUJBQU07OztJQUhBLGVBQTBCO0lBQTFCLDBDQUEwQiw2QkFBQTtJQUNELGVBQWU7SUFBZixtQ0FBZTtJQUNqQixlQUFzQjtJQUF0QiwwQ0FBc0I7O0FERnJELE1BQU0sT0FBTywyQkFBMkI7SUFJdEMsWUFDVSxZQUFzRCxFQUN0RCxvQkFBMEMsRUFDMUMsc0JBQThDLEVBQzlDLGVBQWdDLEVBQ2YsSUFLeEI7UUFUTyxpQkFBWSxHQUFaLFlBQVksQ0FBMEM7UUFDdEQseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUMxQywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQVBqQywwQkFBcUIsR0FBNEIsU0FBUyxDQUFBO1FBZ0NuRSx3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2YsTUFBTSxFQUFVLFNBQVM7WUFDekIsY0FBYyxFQUFVLFNBQVM7U0FDbEMsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUc7WUFDakIsc0JBQXNCLEVBQUMsR0FBRyxFQUFFO2dCQUUxQixJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtvQkFDOUIsaUJBQWlCO29CQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUE7b0JBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLHdCQUF3QixDQUFBO29CQUMzQywyQ0FBMkM7b0JBQzNDLElBQUksQ0FBQyxLQUFLLENBQUMsMEJBQTBCLEdBQUcsS0FBSyxDQUFBO29CQUM3QyxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUN6QyxnRkFBZ0Y7d0JBQ2hGLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFBO3dCQUMxRSxJQUFJLENBQUMsS0FBSyxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQTt3QkFDckMsSUFBSSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQ25DLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQzNDLENBQUE7cUJBQ0Y7b0JBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTt3QkFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBYSxFQUFFLEVBQUU7NEJBQzVDLE1BQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFBOzRCQUM1RixJQUFJLG9CQUFvQixFQUFFO2dDQUN4QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsb0JBQW9CLENBQUE7Z0NBQ2hELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBOzZCQUNwQjt3QkFDSCxDQUFDLENBQUMsQ0FBQTtxQkFDSDtpQkFDRjtxQkFBTTtvQkFDTCxrQkFBa0I7b0JBQ2xCLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQTtvQkFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcscUJBQXFCLENBQUE7aUJBQ3pDO1lBRUgsQ0FBQztTQUNGLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsUUFBUSxFQUFhLFNBQVM7WUFDOUIsMEJBQTBCLEVBQVcsSUFBSTtZQUN6QyxxQkFBcUIsRUFBbUIsRUFBRTtZQUMxQyxLQUFLLEVBQVUscUJBQXFCO1lBQ3BDLFVBQVUsRUFBVyxLQUFLO1lBQzFCLE9BQU8sRUFBRSxLQUFLO1NBQ2YsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsK0JBQStCLEVBQUMsQ0FBQyxhQUE4QixFQUFFLEVBQUU7Z0JBQ2pFLElBQUksQ0FBQyxLQUFLLENBQUMscUJBQXFCLEdBQUcsYUFBYSxDQUFBO1lBQ2xELENBQUM7WUFDRCx3QkFBd0IsRUFBQyxDQUFDLElBQWEsRUFBRSxFQUFFO2dCQUN6QyxNQUFNLG9CQUFvQixHQUF5QixJQUFJLENBQUMsb0JBQW9CLENBQUE7Z0JBRTVFLDhCQUE4QjtnQkFDOUIsZ0ZBQWdGO2dCQUNoRixpQkFBaUI7Z0JBQ2pCLHVDQUF1QztnQkFDdkMsaURBQWlEO2dCQUNqRCwrQ0FBK0M7Z0JBQy9DLGFBQWE7Z0JBQ2IsV0FBVztnQkFDWCxnREFBZ0Q7Z0JBQ2hELHdFQUF3RTtnQkFDeEUsdUJBQXVCO2dCQUN2QixpQ0FBaUM7Z0JBQ2pDLE1BQU07Z0JBRU4sc0ZBQXNGO2dCQUN0RixpQkFBaUI7Z0JBQ2pCLHVDQUF1QztnQkFDdkMsbURBQW1EO2dCQUNuRCwrQ0FBK0M7Z0JBQy9DLGFBQWE7Z0JBQ2IsSUFBSTtZQUNOLENBQUM7WUFDRCxJQUFJLEVBQUMsR0FBRyxFQUFFO2dCQUNSLE1BQU0scUJBQXFCLEdBQTRCLEVBQUUsQ0FBQTtnQkFDekQsSUFBSSxvQkFBMkMsQ0FBQTtnQkFFL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO29CQUN2QixJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDOzs7U0FHakMsQ0FBQyxDQUFBO2lCQUNIO2dCQUVELElBQUksQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFO29CQUN0RCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFhLEVBQUUsRUFBRTt3QkFDNUMsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFOzRCQUNoQixvQkFBb0IsR0FBRztnQ0FDckIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTTtnQ0FDMUIsY0FBYyxFQUFFLFlBQVksQ0FBQyxFQUFFO2dDQUMvQixNQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUU7NkJBQ2hCLENBQUE7NEJBQ0QscUJBQXFCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUE7eUJBQ2pEO29CQUNILENBQUMsQ0FBQyxDQUFBO2dCQUNKLENBQUMsQ0FBQyxDQUFBO2dCQUVGLElBQUkscUJBQXFCLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtvQkFDdEMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQzs7Ozs7O1NBTWpDLEVBQUUsRUFBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUE7b0JBRTVCLE9BQU07aUJBQ1A7Z0JBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO2dCQUV6QixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFO29CQUN6QixJQUFJLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLHFCQUFxQixDQUFDO3lCQUMvRSxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNiLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUMsS0FBSyxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7b0JBQ3hDLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7d0JBQ3hDLHNDQUFzQzt3QkFDdEMscUNBQXFDO3dCQUNyQyw2QkFBNkI7d0JBQzdCLEtBQUs7b0JBQ1AsQ0FBQyxDQUFDO3lCQUNELE9BQU8sQ0FBQyxHQUFHLEVBQUU7d0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO29CQUM1QixDQUFDLENBQUMsQ0FBQTtpQkFDSDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSTt5QkFDN0IsMkJBQTJCLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUM7eUJBQ2xHLFNBQVMsRUFBRTt5QkFDWCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2IsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtvQkFDeEMsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDZCxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDOztxQkFFdkIsTUFBTTtXQUNoQixDQUFDLENBQUE7b0JBQ0osQ0FBQyxDQUFDO3lCQUNELE9BQU8sQ0FBQyxHQUFHLEVBQUU7d0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO29CQUM1QixDQUFDLENBQUMsQ0FBQTtpQkFDSDtZQUlILENBQUM7U0FDRixDQUFBO1FBNUtDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUE7UUFDdkQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQTtRQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRWhDLElBQUksSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQ25DLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUE7U0FDbEU7UUFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRTtZQUNyRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFhLEVBQUUsRUFBRTtnQkFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7WUFDdEIsQ0FBQyxDQUFDLENBQUE7U0FDSDtRQUVELElBQUksQ0FBQyxRQUFRLENBQUMsc0JBQXNCLEVBQUUsQ0FBQTtJQUN4QyxDQUFDO0lBOEpELGlCQUFpQjtJQUNqQixRQUFRO0lBRVIsQ0FBQztJQUNELFdBQVc7UUFDVCxJQUFJLENBQUMsUUFBUSxDQUFDLHNCQUFzQixFQUFFLENBQUE7SUFDeEMsQ0FBQzs7c0dBbk1VLDJCQUEyQix3TUFTNUIsZUFBZTtnRUFUZCwyQkFBMkI7UUNsQnhDLDRFQUNFO1FBRUYsNkJBQXFCO1FBQUEsWUFBaUI7UUFBQSxpQkFBSztRQUMzQywwQ0FFRTtRQUFBLGtFQUsyQztRQUR6QywyTEFBaUMsb0RBQWdELElBQUM7UUFDbkYsaUJBQTBDO1FBRTNDLDBCQUFJO1FBQUEsMEJBQUs7UUFBQSxpQkFBSztRQUVkLDRFQUNJO1FBT04saUJBQXFCO1FBRXJCLDBDQUNFO1FBQUEsaUNBQXlDO1FBQUEsd0JBQU87UUFBQSxpQkFBUztRQUN6RCxrQ0FFa0I7UUFEaEIseUdBQVMsbUJBQWUsSUFBQztRQUNULDRCQUFXO1FBQUEsaUJBQVM7UUFFeEMsaUJBQXFCOztRQS9CaEIsd0NBQXFCO1FBR0wsZUFBaUI7UUFBakIscUNBQWlCO1FBSWxDLGVBQWdEO1FBQWhELGtFQUFnRCwwREFBQSw4REFBQTtRQVF6QixlQUFtQztRQUFuQyw0Q0FBbUM7O2tEREdqRCwyQkFBMkI7Y0FMdkMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSx1QkFBdUI7Z0JBQ2pDLFdBQVcsRUFBRSx3Q0FBd0M7Z0JBQ3JELFNBQVMsRUFBRSxDQUFDLHdDQUF3QyxDQUFDO2FBQ3REOztzQkFVSSxNQUFNO3VCQUFDLGVBQWU7O2tCQVJ4QixLQUFLOztrQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPbkNoYW5nZXMsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IGlSb2xlLCBpVXNlclJvbGVPcmdhbml6YXRpb24sIGlPcmdhbml6YXRpb24gfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uU2VydmljZX0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZydcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgRm9mRXJyb3JTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtZXJyb3Iuc2VydmljZSdcclxuXHJcbmludGVyZmFjZSBpUm9sZVVJIGV4dGVuZHMgaVJvbGUge1xyXG4gIGNoZWNrZWQ/OiBib29sZWFuXHJcbiAgdXNlclJvbGVPcmdhbml6YXRpb24/OiBpVXNlclJvbGVPcmdhbml6YXRpb25cclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2YtdXNlci1yb2xlcy1zZWxlY3QnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mb2YtdXNlci1yb2xlcy1zZWxlY3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi11c2VyLXJvbGVzLXNlbGVjdC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZVc2VyUm9sZXNTZWxlY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgQElucHV0KCkgdXNlclJvbGVPcmdhbml6YXRpb25zOiBpVXNlclJvbGVPcmdhbml6YXRpb25bXSA9IHVuZGVmaW5lZFxyXG4gIEBJbnB1dCgpIG5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zOiBpT3JnYW5pemF0aW9uW11cclxuXHJcbiAgY29uc3RydWN0b3IgKFxyXG4gICAgcHJpdmF0ZSBtYXREaWFsb2dSZWY6TWF0RGlhbG9nUmVmPEZvZlVzZXJSb2xlc1NlbGVjdENvbXBvbmVudD4sXHJcbiAgICBwcml2YXRlIGZvZlBlcm1pc3Npb25TZXJ2aWNlOiBGb2ZQZXJtaXNzaW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2UsXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgZGF0YToge1xyXG4gICAgICByb2xlczogaVJvbGVbXSxcclxuICAgICAgdXNlclJvbGVPcmdhbml6YXRpb25zOiBpVXNlclJvbGVPcmdhbml6YXRpb25bXSxcclxuICAgICAgdXNlcklkOiBudW1iZXIsXHJcbiAgICAgIG5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zOiBpT3JnYW5pemF0aW9uW11cclxuICAgIH1cclxuICApIHtcclxuICAgIHRoaXMudXNlclJvbGVPcmdhbml6YXRpb25zID0gZGF0YS51c2VyUm9sZU9yZ2FuaXphdGlvbnNcclxuICAgIHRoaXMudWlWYXIuYWxsUm9sZXMgPSBkYXRhLnJvbGVzXHJcbiAgICB0aGlzLnByaVZhci51c2VySWQgPSBkYXRhLnVzZXJJZFxyXG5cclxuICAgIGlmIChkYXRhLm5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zKSB7XHJcbiAgICAgIHRoaXMubm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnMgPSBkYXRhLm5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zIFxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnVpVmFyLmFsbFJvbGVzICYmIHRoaXMudWlWYXIuYWxsUm9sZXMubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMudWlWYXIuYWxsUm9sZXMuZm9yRWFjaCgocm9sZTogaVJvbGVVSSkgPT4ge1xyXG4gICAgICAgIHJvbGUuY2hlY2tlZCA9IGZhbHNlXHJcbiAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5wcml2RnVuYy5jb21wb25lbmVudERhdGFSZWZyZXNoKClcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgdXNlcklkOiA8bnVtYmVyPnVuZGVmaW5lZCxcclxuICAgIG9yZ2FuaXphdGlvbklkOiA8bnVtYmVyPnVuZGVmaW5lZFxyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gICAgY29tcG9uZW5lbnREYXRhUmVmcmVzaDooKSA9PiB7XHJcblxyXG4gICAgICBpZiAodGhpcy51c2VyUm9sZU9yZ2FuaXphdGlvbnMpIHtcclxuICAgICAgICAvLyBpdCdzIGFuIHVwZGF0ZVxyXG4gICAgICAgIHRoaXMudWlWYXIuaXNDcmVhdGlvbiA9IGZhbHNlXHJcbiAgICAgICAgdGhpcy51aVZhci50aXRsZSA9IGBNb2RpZmljYXRpb24gZGVzIGFjY8Ooc2BcclxuICAgICAgICAvLyBPbmx5IG9uZSBvcmdhbml6YXRpb24gcG9zc2libGUgaW4gdXBkYXRlXHJcbiAgICAgICAgdGhpcy51aVZhci5vcmdhbml6YXRpb25NdWx0aXBsZVNlbGVjdCA9IGZhbHNlXHJcbiAgICAgICAgaWYgKHRoaXMudXNlclJvbGVPcmdhbml6YXRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIC8vIHdlIGdldCBvbmx5IHRoZSBmaXJzdCBvbmUgc2luY2UgaXQncyBpbXBvc3NpYmxlIHRvIGhhdmUgbXV0aXBsZSBvcmdhbmlzYXRpb25zXHJcbiAgICAgICAgICB0aGlzLnByaVZhci5vcmdhbml6YXRpb25JZCA9IHRoaXMudXNlclJvbGVPcmdhbml6YXRpb25zWzBdLm9yZ2FuaXphdGlvbi5pZFxyXG4gICAgICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMgPSBbXVxyXG4gICAgICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMucHVzaChcclxuICAgICAgICAgICAgdGhpcy51c2VyUm9sZU9yZ2FuaXphdGlvbnNbMF0ub3JnYW5pemF0aW9uXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIGlmICh0aGlzLnVpVmFyLmFsbFJvbGVzKSB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmFsbFJvbGVzLmZvckVhY2goKHJvbGU6IGlSb2xlVUkpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdXNlclJvbGVPcmdhbml6YXRpb24gPSB0aGlzLnVzZXJSb2xlT3JnYW5pemF0aW9ucy5maW5kKGl0ZW0gPT4gaXRlbS5yb2xlSWQgPT0gcm9sZS5pZClcclxuICAgICAgICAgICAgaWYgKHVzZXJSb2xlT3JnYW5pemF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgcm9sZS51c2VyUm9sZU9yZ2FuaXphdGlvbiA9IHVzZXJSb2xlT3JnYW5pemF0aW9uXHJcbiAgICAgICAgICAgICAgcm9sZS5jaGVja2VkID0gdHJ1ZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0gICAgIFxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vIGl0J3MgYSBjcmVhdGlvblxyXG4gICAgICAgIHRoaXMudWlWYXIuaXNDcmVhdGlvbiA9IHRydWVcclxuICAgICAgICB0aGlzLnVpVmFyLnRpdGxlID0gYENyZWF0aW9uIGQndW4gYWNjw6hzYFxyXG4gICAgICB9XHJcbiAgICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgYWxsUm9sZXM6IDxpUm9sZVVJW10+dW5kZWZpbmVkLFxyXG4gICAgb3JnYW5pemF0aW9uTXVsdGlwbGVTZWxlY3Q6IDxib29sZWFuPnRydWUsXHJcbiAgICBzZWxlY3RlZE9yZ2FuaXNhdGlvbnM6IDxpT3JnYW5pemF0aW9uW10+W10sXHJcbiAgICB0aXRsZTogPHN0cmluZz5gQ3JlYXRpb24gZCd1biBhY2PDqHNgLFxyXG4gICAgaXNDcmVhdGlvbjogPGJvb2xlYW4+ZmFsc2UsXHJcbiAgICBsb2FkaW5nOiBmYWxzZVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgICBvcmdhbmlzYXRpb25NdWx0aVNlbGVjdGVkQ2hhbmdlOihvcmdhbmlzYXRpb25zOiBpT3JnYW5pemF0aW9uW10pID0+IHsgICAgICBcclxuICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMgPSBvcmdhbmlzYXRpb25zICAgICAgXHJcbiAgICB9LFxyXG4gICAgdXNlclJvbGVPcmdhbml6YXRpb25TYXZlOihyb2xlOiBpUm9sZVVJKSA9PiB7ICAgICAgXHJcbiAgICAgIGNvbnN0IHVzZXJSb2xlT3JnYW5pemF0aW9uOmlVc2VyUm9sZU9yZ2FuaXphdGlvbiA9IHJvbGUudXNlclJvbGVPcmdhbml6YXRpb25cclxuXHJcbiAgICAgIC8vIGlmICh1c2VyUm9sZU9yZ2FuaXphdGlvbikge1xyXG4gICAgICAvLyAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlclJvbGVPcmdhbml6YXRpb24uZGVsZXRlKHVzZXJSb2xlT3JnYW5pemF0aW9uKVxyXG4gICAgICAvLyAgIC50b1Byb21pc2UoKVxyXG4gICAgICAvLyAgIC50aGVuKChyZXN1bHQ6IGFueSkgPT4geyAgICAgICAgICBcclxuICAgICAgLy8gICAgIHJvbGUudXNlclJvbGVPcmdhbml6YXRpb24gPSBudWxsICAgICAgICAgIFxyXG4gICAgICAvLyAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnNhdmVJc0RvbmUoKVxyXG4gICAgICAvLyAgIH0pICAgICAgXHJcbiAgICAgIC8vIH0gZWxzZSB7XHJcbiAgICAgIC8vICAgLy8gVGhlIHVzZXJSb2xlT3JnYW5pemF0aW9uIG11c3QgYmUgY3JlYXRlZFxyXG4gICAgICAvLyAgIGNvbnN0IHVzZXJSb2xlT3JnYW5pemF0aW9uVG9TYXZlOiBpVXNlclJvbGVPcmdhbml6YXRpb24gPSB7ICAgICAgICBcclxuICAgICAgLy8gICAgIHJvbGVJZDogcm9sZS5pZCxcclxuICAgICAgLy8gICAgIHVzZXJJZDogdGhpcy51aVZhci51c2VyLmlkXHJcbiAgICAgIC8vICAgfVxyXG4gIFxyXG4gICAgICAvLyAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlclJvbGVPcmdhbml6YXRpb24uY3JlYXRlKHVzZXJSb2xlT3JnYW5pemF0aW9uVG9TYXZlKVxyXG4gICAgICAvLyAgIC50b1Byb21pc2UoKVxyXG4gICAgICAvLyAgIC50aGVuKChyZXN1bHQ6IGFueSkgPT4geyAgICAgICAgICBcclxuICAgICAgLy8gICAgIHJvbGUudXNlclJvbGVPcmdhbml6YXRpb24gPSByZXN1bHQgICAgICAgICAgXHJcbiAgICAgIC8vICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc2F2ZUlzRG9uZSgpXHJcbiAgICAgIC8vICAgfSkgICAgICBcclxuICAgICAgLy8gfVxyXG4gICAgfSxcclxuICAgIHNhdmU6KCkgPT4ge1xyXG4gICAgICBjb25zdCB1c2VyUm9sZU9yZ2FuaXphdGlvbnM6IGlVc2VyUm9sZU9yZ2FuaXphdGlvbltdID0gW11cclxuICAgICAgbGV0IHVzZXJSb2xlT3JnYW5pemF0aW9uOiBpVXNlclJvbGVPcmdhbml6YXRpb25cclxuXHJcbiAgICAgIGlmICghdGhpcy5wcmlWYXIudXNlcklkKSB7XHJcbiAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBcclxuICAgICAgICAgIFByb2Jsw6htZSBhdmVjIGxlcyBkb25uw6llczxicj5cclxuICAgICAgICAgIDxzbWFsbD5NYW5xdWUgbGUgdXNlcjwvc21hbGw+XHJcbiAgICAgICAgYClcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMuZm9yRWFjaChvcmdhbmlzYXRpb24gPT4ge1xyXG4gICAgICAgIHRoaXMudWlWYXIuYWxsUm9sZXMuZm9yRWFjaCgocm9sZTogaVJvbGVVSSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHJvbGUuY2hlY2tlZCkge1xyXG4gICAgICAgICAgICB1c2VyUm9sZU9yZ2FuaXphdGlvbiA9IHtcclxuICAgICAgICAgICAgICB1c2VySWQ6IHRoaXMucHJpVmFyLnVzZXJJZCxcclxuICAgICAgICAgICAgICBvcmdhbml6YXRpb25JZDogb3JnYW5pc2F0aW9uLmlkLFxyXG4gICAgICAgICAgICAgIHJvbGVJZDogcm9sZS5pZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHVzZXJSb2xlT3JnYW5pemF0aW9ucy5wdXNoKHVzZXJSb2xlT3JnYW5pemF0aW9uKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0pXHJcblxyXG4gICAgICBpZiAodXNlclJvbGVPcmdhbml6YXRpb25zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihgXHJcbiAgICAgICAgICBWb3VzIGRldmV6IHPDqWxlY3Rpb25uZXIgYXUgbW9pbnM8YnI+XHJcbiAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgIDxsaT5VbmUgb3JnYW5pc2F0aW9uPC9saT5cclxuICAgICAgICAgICAgPGxpPlVuIHJvbGU8L2xpPlxyXG4gICAgICAgICAgPC91bD5cclxuICAgICAgICBgLCB7bXVzdERpc2FwcGVhckFmdGVyOiAtMX0pXHJcblxyXG4gICAgICAgIHJldHVyblxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnVpVmFyLmxvYWRpbmcgPSB0cnVlXHJcbiAgICAgIFxyXG4gICAgICBpZiAodGhpcy51aVZhci5pc0NyZWF0aW9uKSB7IFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlclJvbGVPcmdhbml6YXRpb24uYnVsa0NyZWF0ZSh1c2VyUm9sZU9yZ2FuaXphdGlvbnMpXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKHtzYXZlZDogdHJ1ZX0pICAgICAgICAgIFxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgICAvLyB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoYFxyXG4gICAgICAgICAgLy8gICBFcnJldXIgbG9ycyBkZSBsYSBzYXV2ZWdhcmRlPGJyPlxyXG4gICAgICAgICAgLy8gICA8c21hbGw+JHtyZWFzb259PC9zbWFsbD5cclxuICAgICAgICAgIC8vIGApXHJcbiAgICAgICAgfSkgXHJcbiAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2UgeyAgICAgICAgICAgICAgICBcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIgICAgICAgIFxyXG4gICAgICAgIC5yZXBsYWNlVXNlclJvbGVPcmdhbml6YXRpb24odXNlclJvbGVPcmdhbml6YXRpb25zLCB0aGlzLnByaVZhci51c2VySWQsIHRoaXMucHJpVmFyLm9yZ2FuaXphdGlvbklkKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSh7c2F2ZWQ6IHRydWV9KSAgICAgICAgICBcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChyZWFzb24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBcclxuICAgICAgICAgICAgRXJyZXVyIGxvcnMgZGUgbGEgc2F1dmVnYXJkZTxicj5cclxuICAgICAgICAgICAgPHNtYWxsPiR7cmVhc29ufTwvc21hbGw+XHJcbiAgICAgICAgICBgKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcblxyXG4gICAgICBcclxuICAgICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICBcclxuICB9IFxyXG4gIG5nT25DaGFuZ2VzKCkge1xyXG4gICAgdGhpcy5wcml2RnVuYy5jb21wb25lbmVudERhdGFSZWZyZXNoKCkgICAgXHJcbiAgfSBcclxufVxyXG4iLCI8ZGl2ICpuZ0lmPVwidWlWYXIubG9hZGluZ1wiIGNsYXNzPVwiZm9mLWxvYWRpbmdcIj5cclxuICA8bWF0LXNwaW5uZXIgZGlhbWV0ZXI9MjA+PC9tYXQtc3Bpbm5lcj4gPHNwYW4+UmFmcmFpY2hpc3NlbWVudCBkZXMgcsO0bGVzLi4uPC9zcGFuPlxyXG48L2Rpdj5cclxuPGgyIG1hdC1kaWFsb2ctdGl0bGU+e3sgdWlWYXIudGl0bGUgfX08L2gyPlxyXG48bWF0LWRpYWxvZy1jb250ZW50PiAgXHJcblxyXG4gIDxmb2YtY29yZS1mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3RcclxuICAgIFttdWx0aVNlbGVjdF09XCJ1aVZhci5vcmdhbml6YXRpb25NdWx0aXBsZVNlbGVjdFwiXHJcbiAgICBbc2VsZWN0ZWRPcmdhbmlzYXRpb25zXT1cInVpVmFyLnNlbGVjdGVkT3JnYW5pc2F0aW9uc1wiXHJcbiAgICBbbm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnNdPVwibm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnNcIiAgXHJcbiAgICAoc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlKSA9IFwidWlBY3Rpb24ub3JnYW5pc2F0aW9uTXVsdGlTZWxlY3RlZENoYW5nZSgkZXZlbnQpXCJcclxuICA+PC9mb2YtY29yZS1mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3Q+XHJcblxyXG4gIDxoND5Sw7RsZXM8L2g0PlxyXG5cclxuICA8ZGl2IGNsYXNzPVwiZm9mLWZhZGUtaW5cIiAqbmdGb3I9XCJsZXQgcm9sZSBvZiB1aVZhci5hbGxSb2xlc1wiPiAgICAgICAgICAgIFxyXG4gICAgICA8bWF0LWNoZWNrYm94XHJcbiAgICAgICAgKGNoYW5nZSk9XCJ1aUFjdGlvbi51c2VyUm9sZU9yZ2FuaXphdGlvblNhdmUocm9sZSlcIiAgICAgICAgICBcclxuICAgICAgICBbKG5nTW9kZWwpXT1cInJvbGUuY2hlY2tlZFwiXHJcbiAgICAgICAgW2NoZWNrZWRdPVwicm9sZS5jaGVja2VkXCI+e3sgcm9sZS5jb2RlIH19PC9tYXQtY2hlY2tib3g+ICAgICAgICAgICAgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJvbGUtaGludFwiPnt7IHJvbGUuZGVzY3JpcHRpb24gfX08L2Rpdj4gICAgICAgICAgICBcclxuICA8L2Rpdj5cclxuICBcclxuPC9tYXQtZGlhbG9nLWNvbnRlbnQ+XHJcblxyXG48bWF0LWRpYWxvZy1hY3Rpb25zPlxyXG4gIDxidXR0b24gbWF0LWZsYXQtYnV0dG9uIG1hdC1kaWFsb2ctY2xvc2U+QW5udWxlcjwvYnV0dG9uPiAgICBcclxuICA8YnV0dG9uIG1hdC1mbGF0LWJ1dHRvbiBcclxuICAgIChjbGljayk9XCJ1aUFjdGlvbi5zYXZlKClcIlxyXG4gICAgY29sb3I9XCJwcmltYXJ5XCI+RW5yZWdpc3RyZXI8L2J1dHRvbj4gICAgXHJcbiAgICA8IS0tIFttYXQtZGlhbG9nLWNsb3NlXT1cInRydWVcIiAtLT5cclxuPC9tYXQtZGlhbG9nLWFjdGlvbnM+XHJcblxyXG4iXX0=
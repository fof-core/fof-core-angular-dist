import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { fofUtilsForm } from '../../core/fof-utils';
import { FofUserRolesSelectComponent } from '../fof-user-roles-select/fof-user-roles-select.component';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "@angular/router";
import * as i3 from "@angular/forms";
import * as i4 from "../../core/notification/notification.service";
import * as i5 from "../../core/fof-dialog.service";
import * as i6 from "@angular/material/dialog";
import * as i7 from "../../core/fof-error.service";
import * as i8 from "@angular/material/card";
import * as i9 from "@angular/material/button";
import * as i10 from "@angular/common";
import * as i11 from "../fof-entity-footer/fof-entity-footer.component";
import * as i12 from "@angular/material/form-field";
import * as i13 from "@angular/material/input";
import * as i14 from "../../components/fof-organizations-multi-select/fof-organizations-multi-select.component";
import * as i15 from "@angular/material/table";
import * as i16 from "@angular/material/progress-spinner";
import * as i17 from "@angular/material/checkbox";
import * as i18 from "@angular/material/icon";
function FofUserComponent_div_11_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le login ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Un email valide est obligatoire ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le pr\u00E9nom ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le nom de famille ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_div_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 33);
    i0.ɵɵelement(1, "mat-spinner", 34);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Rafraichissement des r\u00F4les...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_th_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "th", 35);
} }
function FofUserComponent_div_11_div_17_td_15_Template(rf, ctx) { if (rf & 1) {
    const _r82 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 36);
    i0.ɵɵelementStart(1, "mat-checkbox", 37);
    i0.ɵɵlistener("ngModelChange", function FofUserComponent_div_11_div_17_td_15_Template_mat_checkbox_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r82); const row_r80 = ctx.$implicit; return row_r80.checked = $event; });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r80 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", row_r80.checked);
} }
function FofUserComponent_div_11_div_17_th_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 35);
    i0.ɵɵtext(1, "Organisation");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_td_18_Template(rf, ctx) { if (rf & 1) {
    const _r85 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 38);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_18_Template_td_click_0_listener() { i0.ɵɵrestoreView(_r85); const row_r83 = ctx.$implicit; const ctx_r84 = i0.ɵɵnextContext(3); return ctx_r84.uiAction.roleSelectComponentOpen(row_r83); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r83 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", row_r83.organization, "");
} }
function FofUserComponent_div_11_div_17_th_20_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 35);
    i0.ɵɵtext(1, "Roles");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_td_21_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const role_r88 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", role_r88, " ");
} }
function FofUserComponent_div_11_div_17_td_21_Template(rf, ctx) { if (rf & 1) {
    const _r90 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 38);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_21_Template_td_click_0_listener() { i0.ɵɵrestoreView(_r90); const row_r86 = ctx.$implicit; const ctx_r89 = i0.ɵɵnextContext(3); return ctx_r89.uiAction.roleSelectComponentOpen(row_r86); });
    i0.ɵɵtemplate(1, FofUserComponent_div_11_div_17_td_21_div_1_Template, 2, 1, "div", 39);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r86 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", row_r86.roles);
} }
function FofUserComponent_div_11_div_17_th_23_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "th", 35);
} }
function FofUserComponent_div_11_div_17_td_24_Template(rf, ctx) { if (rf & 1) {
    const _r93 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 40);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_24_Template_td_click_0_listener() { i0.ɵɵrestoreView(_r93); const row_r91 = ctx.$implicit; const ctx_r92 = i0.ɵɵnextContext(3); return ctx_r92.uiAction.roleSelectComponentOpen(row_r91); });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2, "chevron_right");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_tr_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 41);
} }
function FofUserComponent_div_11_div_17_tr_26_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 42);
} }
function FofUserComponent_div_11_div_17_Template(rf, ctx) { if (rf & 1) {
    const _r96 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 11);
    i0.ɵɵelementStart(1, "mat-card");
    i0.ɵɵelementStart(2, "div", 0);
    i0.ɵɵelementStart(3, "h4");
    i0.ɵɵtext(4, "R\u00F4les");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 1);
    i0.ɵɵelementStart(6, "button", 3);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_Template_button_click_6_listener() { i0.ɵɵrestoreView(_r96); const ctx_r95 = i0.ɵɵnextContext(2); return ctx_r95.uiAction.userRolesDelete(); });
    i0.ɵɵtext(7, "Supprimer r\u00F4les");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "button", 2);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_Template_button_click_8_listener() { i0.ɵɵrestoreView(_r96); const ctx_r97 = i0.ɵɵnextContext(2); return ctx_r97.uiAction.roleSelectComponentOpen(); });
    i0.ɵɵtext(9, "Ajouter un r\u00F4le");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 20);
    i0.ɵɵtemplate(11, FofUserComponent_div_11_div_17_div_11_Template, 4, 0, "div", 21);
    i0.ɵɵelementStart(12, "table", 22);
    i0.ɵɵelementContainerStart(13, 23);
    i0.ɵɵtemplate(14, FofUserComponent_div_11_div_17_th_14_Template, 1, 0, "th", 24);
    i0.ɵɵtemplate(15, FofUserComponent_div_11_div_17_td_15_Template, 2, 1, "td", 25);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(16, 26);
    i0.ɵɵtemplate(17, FofUserComponent_div_11_div_17_th_17_Template, 2, 0, "th", 24);
    i0.ɵɵtemplate(18, FofUserComponent_div_11_div_17_td_18_Template, 2, 1, "td", 27);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(19, 28);
    i0.ɵɵtemplate(20, FofUserComponent_div_11_div_17_th_20_Template, 2, 0, "th", 24);
    i0.ɵɵtemplate(21, FofUserComponent_div_11_div_17_td_21_Template, 2, 1, "td", 27);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(22, 29);
    i0.ɵɵtemplate(23, FofUserComponent_div_11_div_17_th_23_Template, 1, 0, "th", 24);
    i0.ɵɵtemplate(24, FofUserComponent_div_11_div_17_td_24_Template, 3, 0, "td", 30);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵtemplate(25, FofUserComponent_div_11_div_17_tr_25_Template, 1, 0, "tr", 31);
    i0.ɵɵtemplate(26, FofUserComponent_div_11_div_17_tr_26_Template, 1, 0, "tr", 32);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r68 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(11);
    i0.ɵɵproperty("ngIf", ctx_r68.uiVar.loadingRoles);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("dataSource", ctx_r68.uiVar.userRolesToDisplay);
    i0.ɵɵadvance(13);
    i0.ɵɵproperty("matHeaderRowDef", ctx_r68.uiVar.roleDisplayedColumns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matRowDefColumns", ctx_r68.uiVar.roleDisplayedColumns);
} }
function FofUserComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    const _r99 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 9);
    i0.ɵɵelementStart(1, "div", 10);
    i0.ɵɵelementStart(2, "div", 11);
    i0.ɵɵelementStart(3, "form", 12);
    i0.ɵɵelementStart(4, "mat-form-field");
    i0.ɵɵelement(5, "input", 13);
    i0.ɵɵtemplate(6, FofUserComponent_div_11_mat_error_6_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "mat-form-field");
    i0.ɵɵelement(8, "input", 15);
    i0.ɵɵtemplate(9, FofUserComponent_div_11_mat_error_9_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "mat-form-field");
    i0.ɵɵelement(11, "input", 16);
    i0.ɵɵtemplate(12, FofUserComponent_div_11_mat_error_12_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "mat-form-field");
    i0.ɵɵelement(14, "input", 17);
    i0.ɵɵtemplate(15, FofUserComponent_div_11_mat_error_15_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(16, "fof-core-fof-organizations-multi-select", 18);
    i0.ɵɵlistener("selectedOrganizationsChange", function FofUserComponent_div_11_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_16_listener($event) { i0.ɵɵrestoreView(_r99); const ctx_r98 = i0.ɵɵnextContext(); return ctx_r98.uiAction.organisationMultiSelectedChange($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(17, FofUserComponent_div_11_div_17_Template, 27, 4, "div", 19);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r62 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("formGroup", ctx_r62.uiVar.form);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r62.uiVar.form.get("login").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r62.uiVar.form.get("email").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r62.uiVar.form.get("firstName").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r62.uiVar.form.get("lastName").invalid);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("placeHolder", "Organisation de rattachement")("multiSelect", false)("selectedOrganisations", ctx_r62.uiVar.userOrganizationsDisplay);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r62.uiVar.allRoles);
} }
function FofUserComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 43);
    i0.ɵɵelement(1, "mat-spinner", 34);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des utilisateurs et des authorisations...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
export class FofUserComponent {
    constructor(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, matDialog, fofErrorService) {
        this.fofPermissionService = fofPermissionService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        this.matDialog = matDialog;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            userId: undefined,
            userOrganizationId: undefined,
            organizationAlreadyWithRoles: undefined
        };
        // All private functions
        this.privFunc = {
            userLoad: () => {
                this.uiVar.loadingUser = true;
                Promise.all([
                    this.fofPermissionService.role.getAll().toPromise(),
                    this.fofPermissionService.user.getWithRoleById(this.priVar.userId).toPromise()
                ])
                    .then(result => {
                    const roles = result[0];
                    const currentUsers = result[1];
                    console.log('currentUsers', currentUsers);
                    this.uiVar.allRoles = roles;
                    this.privFunc.userRefresh(currentUsers);
                    // Here for preventing to refresh the user form when refreshing only the roles.
                    // toDo: improve
                    this.uiVar.form.patchValue(this.uiVar.user);
                })
                    .catch(reason => {
                    this.fofErrorService.errorManage(reason);
                    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                })
                    .finally(() => {
                    this.uiVar.loadingUser = false;
                });
            },
            userRefresh: (currentUser) => {
                if (!currentUser) {
                    this.fofNotificationService.error(`Ce utilisateur n'existe pas`);
                    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                    return;
                }
                this.uiVar.user = currentUser;
                if (currentUser.organizationId) {
                    this.uiVar.userOrganizationsDisplay = [{ id: currentUser.organizationId }];
                    this.priVar.userOrganizationId = currentUser.organizationId;
                }
                let _previousOrganisationId = undefined;
                let _roleToDisplay = undefined;
                this.uiVar.userRolesToDisplay = [];
                if (currentUser.userRoleOrganizations && currentUser.userRoleOrganizations.length > 0) {
                    // we will display the user roles grouped by organization 
                    // with one on several roles on it
                    currentUser.userRoleOrganizations.forEach(uro => {
                        if (_previousOrganisationId !== uro.organization.id) {
                            // it's a new organisation, reinit
                            _previousOrganisationId = uro.organization.id;
                            if (_roleToDisplay) {
                                this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                            }
                            _roleToDisplay = {
                                organizationId: uro.organization.id,
                                organization: uro.organization.name,
                                roles: [],
                                userRoleOrganizations: []
                            };
                        }
                        _roleToDisplay.roles.push(uro.role.code);
                        _roleToDisplay.userRoleOrganizations.push(uro);
                    });
                }
                // add the last one
                if (_roleToDisplay) {
                    this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                }
                // we don't want the following organization could be selectebale in the 
                // organization tree
                this.priVar.organizationAlreadyWithRoles = [];
                this.uiVar.userRolesToDisplay.forEach(org => {
                    this.priVar.organizationAlreadyWithRoles.push({
                        id: org.organizationId,
                        name: org.organization
                    });
                });
            },
            useRolesRefresh: () => {
                this.uiVar.loadingRoles = true;
                this.fofPermissionService.user.getWithRoleById(this.priVar.userId)
                    .toPromise()
                    .then(usersResult => {
                    this.privFunc.userRefresh(usersResult);
                })
                    .catch(reason => {
                    this.fofErrorService.errorManage(reason);
                    // this.router.navigate(['../'], {relativeTo: this.activatedRoute})
                })
                    .finally(() => {
                    this.uiVar.loadingRoles = false;
                });
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouvel utilisateur',
            loadingUser: false,
            loadingRoles: false,
            userIsNew: false,
            user: undefined,
            userOrganizationsDisplay: undefined,
            form: this.formBuilder.group({
                login: ['', [Validators.maxLength(30)]],
                email: ['', [Validators.required, Validators.email, Validators.maxLength(60)]],
                firstName: ['', [Validators.maxLength(30)]],
                lastName: ['', [Validators.maxLength(30)]]
            }),
            allRoles: undefined,
            selectedUserRoleOrganizations: undefined,
            userRolesToDisplay: [],
            roleDisplayedColumns: ['delete', 'organization', 'roles', 'icon'],
            rolesAll: undefined
        };
        // All actions shared with UI 
        this.uiAction = {
            userSave: () => {
                const userToSave = this.uiVar.form.value;
                userToSave.organizationId = this.priVar.userOrganizationId;
                if (!this.uiVar.form.valid) {
                    this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(this.uiVar.form);
                    return;
                }
                if (this.uiVar.userIsNew) {
                    this.fofPermissionService.user.create(userToSave)
                        .toPromise()
                        .then((newUser) => {
                        this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                        this.priVar.userId = newUser.id;
                        this.uiVar.title = `Modification d'un utilisateur`;
                        this.uiVar.userIsNew = false;
                        this.privFunc.userLoad();
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    userToSave.id = this.uiVar.user.id;
                    this.fofPermissionService.user.update(userToSave)
                        .toPromise()
                        .then(result => {
                        this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
            },
            userCancel: () => {
                this.privFunc.userLoad();
            },
            userDelete: () => {
                this.fofDialogService.openYesNo({
                    question: `Voulez vous vraiment supprimer l' utilisateur ?`
                }).then(yes => {
                    if (yes) {
                        this.fofPermissionService.user.delete(this.uiVar.user)
                            .toPromise()
                            .then(result => {
                            this.fofNotificationService.success('Utilisateur supprimé');
                            this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                        });
                    }
                });
            },
            userRolesDelete: () => {
                const userRoles = this.uiVar.userRolesToDisplay;
                const organizationsIdToDelete = [];
                let somethingToDelete = false;
                let message = `Voulez vous vraiment supprimer les rôles des organisations suivante ?<br>`;
                message = message + '<ul>';
                userRoles.forEach(ur => {
                    //don't want to create an interface for 1 param)
                    if (ur['checked']) {
                        somethingToDelete = true;
                        message = message + `<li>${ur.organization}</li>`;
                        organizationsIdToDelete.push(ur.organizationId);
                    }
                });
                message = message + '</ul>';
                if (somethingToDelete) {
                    this.fofDialogService.openYesNo({
                        title: 'Supprimer rôles',
                        question: message
                    }).then(yes => {
                        if (yes) {
                            this.fofPermissionService.user.deleteUserRoleOrganizations(this.uiVar.user.id, organizationsIdToDelete)
                                .toPromise()
                                .then(result => {
                                this.fofNotificationService.success('Rôles supprimés');
                                this.privFunc.useRolesRefresh();
                            });
                        }
                    });
                }
                else {
                    this.fofNotificationService.info('Vous devez sélectionner ua moins une organisation');
                }
            },
            roleSelectComponentOpen: (userRole) => {
                let userRoleOrganizations;
                if (userRole) {
                    userRoleOrganizations = userRole.userRoleOrganizations;
                }
                const dialogRef = this.matDialog.open(FofUserRolesSelectComponent, {
                    data: {
                        roles: this.uiVar.allRoles,
                        userRoleOrganizations: userRoleOrganizations,
                        notSelectableOrganizations: this.priVar.organizationAlreadyWithRoles,
                        userId: this.uiVar.user.id
                    },
                    width: '600px',
                    height: 'calc(100vh - 200px)'
                });
                dialogRef.afterClosed()
                    .toPromise()
                    .then(mustBeRefresd => {
                    if (mustBeRefresd) {
                        this.privFunc.useRolesRefresh();
                    }
                });
            },
            organisationMultiSelectedChange: (organizations) => {
                if (organizations && organizations.length > 0) {
                    this.priVar.userOrganizationId = organizations[0].id;
                }
                else {
                    this.priVar.userOrganizationId = null;
                }
            }
        };
    }
    // Angular events
    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(params => {
            const code = params.get('code');
            if (code) {
                if (code.toLowerCase() == 'new') {
                    this.uiVar.userIsNew = true;
                }
                else {
                    this.priVar.userId = code;
                    this.uiVar.title = `Modification d'un utilisateur`;
                    this.privFunc.userLoad();
                }
            }
        });
    }
}
FofUserComponent.ɵfac = function FofUserComponent_Factory(t) { return new (t || FofUserComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i3.FormBuilder), i0.ɵɵdirectiveInject(i4.FofNotificationService), i0.ɵɵdirectiveInject(i5.FofDialogService), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i6.MatDialog), i0.ɵɵdirectiveInject(i7.FofErrorService)); };
FofUserComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofUserComponent, selectors: [["fof-core-fof-user"]], decls: 14, vars: 4, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "detail fof-fade-in", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [3, "entityBase"], [1, "detail", "fof-fade-in"], [1, "row"], [1, "col-md-6"], [3, "formGroup"], ["matInput", "", "formControlName", "login", "placeholder", "login", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "type", "email", "formControlName", "email", "placeholder", "Email", "value", ""], ["matInput", "", "required", "", "type", "firstName", "formControlName", "firstName", "placeholder", "Pr\u00E9nom", "value", ""], ["matInput", "", "required", "", "type", "lastName", "formControlName", "lastName", "placeholder", "Nom de famille", "value", ""], [3, "placeHolder", "multiSelect", "selectedOrganisations", "selectedOrganizationsChange"], ["class", "col-md-6", 4, "ngIf"], [1, "fof-table-container"], ["class", "fof-loading fof-loading-roles", 4, "ngIf"], ["mat-table", "", 1, "data-table", 3, "dataSource"], ["matColumnDef", "delete"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "organization"], ["mat-cell", "", 3, "click", 4, "matCellDef"], ["matColumnDef", "roles"], ["matColumnDef", "icon"], ["class", "icon-select", "mat-cell", "", 3, "click", 4, "matCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 4, "matRowDef", "matRowDefColumns"], [1, "fof-loading", "fof-loading-roles"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], [3, "ngModel", "ngModelChange"], ["mat-cell", "", 3, "click"], [4, "ngFor", "ngForOf"], ["mat-cell", "", 1, "icon-select", 3, "click"], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over"], [1, "fof-loading"]], template: function FofUserComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-card", 0);
        i0.ɵɵelementStart(1, "h3");
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "div", 1);
        i0.ɵɵelementStart(4, "button", 2);
        i0.ɵɵlistener("click", function FofUserComponent_Template_button_click_4_listener() { return ctx.uiAction.userCancel(); });
        i0.ɵɵtext(5, "Annuler");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "button", 3);
        i0.ɵɵlistener("click", function FofUserComponent_Template_button_click_6_listener() { return ctx.uiAction.userDelete(); });
        i0.ɵɵtext(7, "Supprimer");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "button", 4);
        i0.ɵɵlistener("click", function FofUserComponent_Template_button_click_8_listener() { return ctx.uiAction.userSave(); });
        i0.ɵɵtext(9, " Enregister");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(10, "div", 5);
        i0.ɵɵtemplate(11, FofUserComponent_div_11_Template, 18, 9, "div", 6);
        i0.ɵɵtemplate(12, FofUserComponent_div_12_Template, 4, 0, "div", 7);
        i0.ɵɵelementEnd();
        i0.ɵɵelement(13, "fof-entity-footer", 8);
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.uiVar.title);
        i0.ɵɵadvance(9);
        i0.ɵɵproperty("ngIf", !ctx.uiVar.loadingUser);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.loadingUser);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("entityBase", ctx.uiVar.user);
    } }, directives: [i8.MatCard, i9.MatButton, i10.NgIf, i11.FofEntityFooterComponent, i3.ɵangular_packages_forms_forms_y, i3.NgControlStatusGroup, i3.FormGroupDirective, i12.MatFormField, i13.MatInput, i3.DefaultValueAccessor, i3.NgControlStatus, i3.FormControlName, i3.RequiredValidator, i14.FofOrganizationsMultiSelectComponent, i12.MatError, i15.MatTable, i15.MatColumnDef, i15.MatHeaderCellDef, i15.MatCellDef, i15.MatHeaderRowDef, i15.MatRowDef, i16.MatSpinner, i15.MatHeaderCell, i15.MatCell, i17.MatCheckbox, i3.NgModel, i10.NgForOf, i18.MatIcon, i15.MatHeaderRow, i15.MatRow], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row;margin-bottom:15px}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .fof-loading-roles[_ngcontent-%COMP%]{position:absolute}.main[_ngcontent-%COMP%]   .role-hint[_ngcontent-%COMP%]{padding-left:25px;font-size:smaller}.main[_ngcontent-%COMP%]   .icon-select[_ngcontent-%COMP%]{text-align:right}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofUserComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-user',
                templateUrl: './fof-user.component.html',
                styleUrls: ['./fof-user.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.ActivatedRoute }, { type: i3.FormBuilder }, { type: i4.FofNotificationService }, { type: i5.FofDialogService }, { type: i2.Router }, { type: i6.MatDialog }, { type: i7.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXVzZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vZm9mLXVzZXIvZm9mLXVzZXIuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi11c2VyL2ZvZi11c2VyLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQW1DLE1BQU0sZUFBZSxDQUFBO0FBTTFFLE9BQU8sRUFBMEIsVUFBVSxFQUFHLE1BQU0sZ0JBQWdCLENBQUE7QUFHcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFBO0FBRW5ELE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDBEQUEwRCxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNhMUYsaUNBQ0U7SUFBQSxzRUFDRjtJQUFBLGlCQUFZOzs7SUFRWixpQ0FDRTtJQUFBLGlEQUNGO0lBQUEsaUJBQVk7OztJQVFaLGlDQUNFO0lBQUEsNEVBQ0Y7SUFBQSxpQkFBWTs7O0lBUVosaUNBQ0U7SUFBQSwrRUFDRjtJQUFBLGlCQUFZOzs7SUE0QlosK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGtEQUE2QjtJQUFBLGlCQUFPO0lBQ3BGLGlCQUFNOzs7SUFLRix5QkFBMkM7Ozs7SUFDM0MsOEJBQ0U7SUFBQSx3Q0FFZTtJQURiLGdPQUF5QjtJQUMzQixpQkFBZTtJQUNqQixpQkFBSzs7O0lBRkQsZUFBeUI7SUFBekIseUNBQXlCOzs7SUFNN0IsOEJBQXNDO0lBQUEsNEJBQVk7SUFBQSxpQkFBSzs7OztJQUN2RCw4QkFFQTtJQURBLHlNQUFTLGlEQUFxQyxJQUFDO0lBQy9DLFlBQW9CO0lBQUEsaUJBQUs7OztJQUF6QixlQUFvQjtJQUFwQixvREFBb0I7OztJQUlwQiw4QkFBc0M7SUFBQSxxQkFBSztJQUFBLGlCQUFLOzs7SUFHOUMsMkJBQ0U7SUFBQSxZQUNGO0lBQUEsaUJBQU07OztJQURKLGVBQ0Y7SUFERSx5Q0FDRjs7OztJQUpGLDhCQUVFO0lBREEseU1BQVMsaURBQXFDLElBQUM7SUFDL0Msc0ZBQ0U7SUFFSixpQkFBSzs7O0lBSEUsZUFBOEI7SUFBOUIsdUNBQThCOzs7SUFPckMseUJBQTJDOzs7O0lBQzNDLDhCQUVFO0lBREEseU1BQVMsaURBQXFDLElBQUM7SUFDL0MsZ0NBQVU7SUFBQSw2QkFBYTtJQUFBLGlCQUFXO0lBQ3BDLGlCQUFLOzs7SUFHUCx5QkFBc0U7OztJQUN0RSx5QkFDa0U7Ozs7SUF4RDFFLCtCQUVFO0lBQUEsZ0NBQ0U7SUFBQSw4QkFDRTtJQUFBLDBCQUFJO0lBQUEsMEJBQUs7SUFBQSxpQkFBSztJQUNkLDhCQUNFO0lBQUEsaUNBQ3VDO0lBQXJDLHdLQUFTLGtDQUEwQixJQUFDO0lBQUMsb0NBQWU7SUFBQSxpQkFBUztJQUMvRCxpQ0FDK0M7SUFBN0Msd0tBQVMsMENBQWtDLElBQUM7SUFBQyxvQ0FBZTtJQUFBLGlCQUFTO0lBQ3pFLGlCQUFNO0lBQ1IsaUJBQU07SUFFTixnQ0FDRTtJQUFBLGtGQUNFO0lBR0Ysa0NBRUU7SUFBQSxrQ0FDRTtJQUFBLGdGQUFzQztJQUN0QyxnRkFDRTtJQUlKLDBCQUFlO0lBRWYsa0NBQ0U7SUFBQSxnRkFBc0M7SUFDdEMsZ0ZBRUE7SUFDRiwwQkFBZTtJQUVmLGtDQUNFO0lBQUEsZ0ZBQXNDO0lBQ3RDLGdGQUVFO0lBSUosMEJBQWU7SUFFZixrQ0FDRTtJQUFBLGdGQUFzQztJQUN0QyxnRkFFRTtJQUVKLDBCQUFlO0lBRWYsZ0ZBQWlFO0lBQ2pFLGdGQUM2RDtJQUUvRCxpQkFBUTtJQUVWLGlCQUFNO0lBQ1IsaUJBQVc7SUFFYixpQkFBTTs7O0lBakRLLGdCQUEwQjtJQUExQixpREFBMEI7SUFJZCxlQUF1QztJQUF2Qyw2REFBdUM7SUFvQ25DLGdCQUE2QztJQUE3QyxvRUFBNkM7SUFFOUQsZUFBMEQ7SUFBMUQscUVBQTBEOzs7O0lBakh4RSw4QkFFRTtJQUFBLCtCQUNFO0lBQUEsK0JBRUU7SUFBQSxnQ0FFRTtJQUFBLHNDQUNFO0lBQUEsNEJBR0E7SUFBQSxxRkFDRTtJQUVKLGlCQUFpQjtJQUVqQixzQ0FDRTtJQUFBLDRCQUlBO0lBQUEscUZBQ0U7SUFFSixpQkFBaUI7SUFFakIsdUNBQ0U7SUFBQSw2QkFJQTtJQUFBLHVGQUNFO0lBRUosaUJBQWlCO0lBRWpCLHVDQUNFO0lBQUEsNkJBSUE7SUFBQSx1RkFDRTtJQUVKLGlCQUFpQjtJQUVqQixvRUFLMkM7SUFEekMsb1BBQWlDLHdEQUFnRCxJQUFDO0lBQ25GLGlCQUEwQztJQUU3QyxpQkFBTztJQUVULGlCQUFNO0lBRU4sNEVBRUU7SUE4REosaUJBQU07SUFDUixpQkFBTTs7O0lBckhNLGVBQXdCO0lBQXhCLDhDQUF3QjtJQU1mLGVBQXVDO0lBQXZDLDhEQUF1QztJQVV2QyxlQUF1QztJQUF2Qyw4REFBdUM7SUFVdkMsZUFBMkM7SUFBM0Msa0VBQTJDO0lBVTNDLGVBQTBDO0lBQTFDLGlFQUEwQztJQU1yRCxlQUE4QztJQUE5Qyw0REFBOEMsc0JBQUEsaUVBQUE7SUFVOUIsZUFBc0I7SUFBdEIsNkNBQXNCOzs7SUFrRWhELCtCQUNFO0lBQUEsa0NBQXVDO0lBQUMsNEJBQU07SUFBQSxxRUFBcUQ7SUFBQSxpQkFBTztJQUM1RyxpQkFBTTs7QUQzR1IsTUFBTSxPQUFPLGdCQUFnQjtJQUMzQixZQUNVLG9CQUEwQyxFQUMxQyxjQUE4QixFQUM5QixXQUF3QixFQUN4QixzQkFBOEMsRUFDOUMsZ0JBQWtDLEVBQ2xDLE1BQWMsRUFDZCxTQUFvQixFQUNwQixlQUFnQztRQVBoQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBSTFDLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixNQUFNLEVBQVUsU0FBUztZQUN6QixrQkFBa0IsRUFBVSxTQUFTO1lBQ3JDLDRCQUE0QixFQUFtQixTQUFTO1NBQ3pELENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHO1lBQ2pCLFFBQVEsRUFBQyxHQUFHLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFBO2dCQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDO29CQUNWLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsU0FBUyxFQUFFO29CQUNuRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsRUFBRTtpQkFDL0UsQ0FBQztxQkFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ2IsTUFBTSxLQUFLLEdBQVksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO29CQUNoQyxNQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7b0JBRTlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFBO29CQUV6QyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7b0JBQzNCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFBO29CQUN2QywrRUFBK0U7b0JBQy9FLGdCQUFnQjtvQkFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQzdDLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBQyxDQUFDLENBQUE7Z0JBQ2xFLENBQUMsQ0FBQztxQkFDRCxPQUFPLENBQUMsR0FBRyxFQUFFO29CQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQTtnQkFDaEMsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO1lBQ0QsV0FBVyxFQUFDLENBQUMsV0FBa0IsRUFBRSxFQUFFO2dCQUVqQyxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNoQixJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLDZCQUE2QixDQUFDLENBQUE7b0JBQ2hFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBQyxDQUFDLENBQUE7b0JBQ2hFLE9BQU07aUJBQ1A7Z0JBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFBO2dCQUU3QixJQUFJLFdBQVcsQ0FBQyxjQUFjLEVBQUU7b0JBQzlCLElBQUksQ0FBQyxLQUFLLENBQUMsd0JBQXdCLEdBQUcsQ0FBQyxFQUFDLEVBQUUsRUFBRSxXQUFXLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQTtvQkFDeEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxXQUFXLENBQUMsY0FBYyxDQUFBO2lCQUM1RDtnQkFFRCxJQUFJLHVCQUF1QixHQUFXLFNBQVMsQ0FBQTtnQkFDL0MsSUFBSSxjQUFjLEdBQXVCLFNBQVMsQ0FBQTtnQkFFbEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUE7Z0JBRWxDLElBQUksV0FBVyxDQUFDLHFCQUFxQixJQUFJLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNyRiwwREFBMEQ7b0JBQzFELGtDQUFrQztvQkFDbEMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDOUMsSUFBSSx1QkFBdUIsS0FBSyxHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRTs0QkFDbkQsa0NBQWtDOzRCQUNsQyx1QkFBdUIsR0FBRyxHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQTs0QkFDN0MsSUFBSSxjQUFjLEVBQUU7Z0NBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7NkJBQUU7NEJBQzFFLGNBQWMsR0FBRztnQ0FDZixjQUFjLEVBQUUsR0FBRyxDQUFDLFlBQVksQ0FBQyxFQUFFO2dDQUNuQyxZQUFZLEVBQUUsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJO2dDQUNuQyxLQUFLLEVBQUUsRUFBRTtnQ0FDVCxxQkFBcUIsRUFBRSxFQUFFOzZCQUMxQixDQUFBO3lCQUNGO3dCQUNELGNBQWMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7d0JBQ3hDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7b0JBQ2hELENBQUMsQ0FBQyxDQUFBO2lCQUNIO2dCQUVELG1CQUFtQjtnQkFDbkIsSUFBSSxjQUFjLEVBQUU7b0JBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7aUJBQUU7Z0JBRTFFLHdFQUF3RTtnQkFDeEUsb0JBQW9CO2dCQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLDRCQUE0QixHQUFHLEVBQUUsQ0FBQTtnQkFDN0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQzFDLElBQUksQ0FBQyxNQUFNLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDO3dCQUM1QyxFQUFFLEVBQUUsR0FBRyxDQUFDLGNBQWM7d0JBQ3RCLElBQUksRUFBRSxHQUFHLENBQUMsWUFBWTtxQkFDdkIsQ0FBQyxDQUFBO2dCQUNKLENBQUMsQ0FBQyxDQUFBO1lBRUosQ0FBQztZQUNELGVBQWUsRUFBQyxHQUFHLEVBQUU7Z0JBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQTtnQkFDOUIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7cUJBQ2pFLFNBQVMsRUFBRTtxQkFDWCxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUU7b0JBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFBO2dCQUN4QyxDQUFDLENBQUM7cUJBQ0QsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFO29CQUNkLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUN4QyxtRUFBbUU7Z0JBQ3JFLENBQUMsQ0FBQztxQkFDRCxPQUFPLENBQUMsR0FBRyxFQUFFO29CQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQTtnQkFDakMsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO1NBQ0YsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixLQUFLLEVBQUUsb0JBQW9CO1lBQzNCLFdBQVcsRUFBRSxLQUFLO1lBQ2xCLFlBQVksRUFBRSxLQUFLO1lBQ25CLFNBQVMsRUFBRSxLQUFLO1lBQ2hCLElBQUksRUFBUyxTQUFTO1lBQ3RCLHdCQUF3QixFQUFtQixTQUFTO1lBQ3BELElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztnQkFDM0IsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN2QyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUM5RSxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzNDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUMzQyxDQUFDO1lBQ0YsUUFBUSxFQUFhLFNBQVM7WUFDOUIsNkJBQTZCLEVBQTJCLFNBQVM7WUFDakUsa0JBQWtCLEVBQXdCLEVBQUU7WUFDNUMsb0JBQW9CLEVBQUUsQ0FBQyxRQUFRLEVBQUUsY0FBYyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUM7WUFDakUsUUFBUSxFQUFXLFNBQVM7U0FDN0IsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsUUFBUSxFQUFDLEdBQUcsRUFBRTtnQkFDWixNQUFNLFVBQVUsR0FBVSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUE7Z0JBQy9DLFVBQVUsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQTtnQkFFMUQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFBO29CQUN2RixZQUFZLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtvQkFDL0MsT0FBTTtpQkFDUDtnQkFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFO29CQUN4QixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7eUJBQ2hELFNBQVMsRUFBRTt5QkFDWCxJQUFJLENBQUMsQ0FBQyxPQUFjLEVBQUUsRUFBRTt3QkFDdkIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFDLGtCQUFrQixFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7d0JBQ3BGLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUE7d0JBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLCtCQUErQixDQUFBO3dCQUNsRCxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7d0JBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUE7b0JBQzFCLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQzFDLENBQUMsQ0FBQyxDQUFBO2lCQUNIO3FCQUFNO29CQUNMLFVBQVUsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFBO29CQUVsQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUM7eUJBQ2hELFNBQVMsRUFBRTt5QkFDWCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFDLGtCQUFrQixFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7b0JBQ3RGLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQzFDLENBQUMsQ0FBQyxDQUFBO2lCQUNIO1lBQ0gsQ0FBQztZQUNELFVBQVUsRUFBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtZQUMxQixDQUFDO1lBQ0QsVUFBVSxFQUFDLEdBQUcsRUFBRTtnQkFDZCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO29CQUM5QixRQUFRLEVBQUUsaURBQWlEO2lCQUM1RCxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUNaLElBQUksR0FBRyxFQUFFO3dCQUNQLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDOzZCQUNyRCxTQUFTLEVBQUU7NkJBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUNiLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQTs0QkFDM0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQTt3QkFDbEUsQ0FBQyxDQUFDLENBQUE7cUJBQ0g7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO1lBQ0QsZUFBZSxFQUFDLEdBQUcsRUFBRTtnQkFDbkIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQTtnQkFDL0MsTUFBTSx1QkFBdUIsR0FBa0IsRUFBRSxDQUFBO2dCQUNqRCxJQUFJLGlCQUFpQixHQUFHLEtBQUssQ0FBQTtnQkFDN0IsSUFBSSxPQUFPLEdBQUcsMkVBQTJFLENBQUE7Z0JBQ3pGLE9BQU8sR0FBRyxPQUFPLEdBQUcsTUFBTSxDQUFBO2dCQUUxQixTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFO29CQUNyQixnREFBZ0Q7b0JBQ2hELElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUNqQixpQkFBaUIsR0FBRyxJQUFJLENBQUE7d0JBQ3hCLE9BQU8sR0FBRyxPQUFPLEdBQUcsT0FBTyxFQUFFLENBQUMsWUFBWSxPQUFPLENBQUE7d0JBQ2pELHVCQUF1QixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxDQUFDLENBQUE7cUJBQ2hEO2dCQUNILENBQUMsQ0FBQyxDQUFBO2dCQUVGLE9BQU8sR0FBRyxPQUFPLEdBQUcsT0FBTyxDQUFBO2dCQUczQixJQUFJLGlCQUFpQixFQUFFO29CQUNyQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO3dCQUM5QixLQUFLLEVBQUUsaUJBQWlCO3dCQUN4QixRQUFRLEVBQUUsT0FBTztxQkFDbEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDWixJQUFJLEdBQUcsRUFBRTs0QkFDUCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSx1QkFBdUIsQ0FBQztpQ0FDdEcsU0FBUyxFQUFFO2lDQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtnQ0FDYixJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUE7Z0NBQ3RELElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLENBQUE7NEJBQ2pDLENBQUMsQ0FBQyxDQUFBO3lCQUNIO29CQUNILENBQUMsQ0FBQyxDQUFBO2lCQUNIO3FCQUFNO29CQUNMLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsbURBQW1ELENBQUMsQ0FBQTtpQkFDdEY7WUFDSCxDQUFDO1lBQ0QsdUJBQXVCLEVBQUMsQ0FBQyxRQUE2QixFQUFFLEVBQUU7Z0JBQ3hELElBQUkscUJBQThDLENBQUE7Z0JBRWxELElBQUksUUFBUSxFQUFFO29CQUNaLHFCQUFxQixHQUFHLFFBQVEsQ0FBQyxxQkFBcUIsQ0FBQTtpQkFDdkQ7Z0JBRUQsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsMkJBQTJCLEVBQUU7b0JBQ2pFLElBQUksRUFBRTt3QkFDSixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRO3dCQUMxQixxQkFBcUIsRUFBRSxxQkFBcUI7d0JBQzVDLDBCQUEwQixFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsNEJBQTRCO3dCQUNwRSxNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtxQkFDM0I7b0JBQ0QsS0FBSyxFQUFFLE9BQU87b0JBQ2QsTUFBTSxFQUFFLHFCQUFxQjtpQkFDOUIsQ0FBQyxDQUFBO2dCQUNGLFNBQVMsQ0FBQyxXQUFXLEVBQUU7cUJBQ3RCLFNBQVMsRUFBRTtxQkFDWCxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7b0JBQ3BCLElBQUksYUFBYSxFQUFFO3dCQUNqQixJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFBO3FCQUNoQztnQkFDSCxDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUM7WUFDRCwrQkFBK0IsRUFBQyxDQUFDLGFBQThCLEVBQUUsRUFBRTtnQkFDakUsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQzdDLElBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQTtpQkFDckQ7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUE7aUJBQ3RDO1lBQ0gsQ0FBQztTQUNGLENBQUE7SUExUEQsQ0FBQztJQTJQRCxpQkFBaUI7SUFDakIsUUFBUTtRQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUM5QyxNQUFNLElBQUksR0FBTyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBRW5DLElBQUksSUFBSSxFQUFFO2dCQUNSLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLEtBQUssRUFBRTtvQkFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFBO2lCQUM1QjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUE7b0JBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLCtCQUErQixDQUFBO29CQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFBO2lCQUN6QjthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDOztnRkFyUlUsZ0JBQWdCO3FEQUFoQixnQkFBZ0I7UUMvQjdCLG1DQUNFO1FBQUEsMEJBQUk7UUFBQSxZQUFpQjtRQUFBLGlCQUFLO1FBQzFCLDhCQUNFO1FBQUEsaUNBQ2tDO1FBQWhDLDZGQUFTLHlCQUFxQixJQUFDO1FBQUMsdUJBQU87UUFBQSxpQkFBUztRQUNsRCxpQ0FDa0M7UUFBaEMsNkZBQVMseUJBQXFCLElBQUM7UUFBQyx5QkFBUztRQUFBLGlCQUFTO1FBQ3BELGlDQUVFO1FBREEsNkZBQVMsdUJBQW1CLElBQUM7UUFDN0IsMkJBQVU7UUFBQSxpQkFBUztRQUN2QixpQkFBTTtRQUNSLGlCQUFXO1FBQ1gsK0JBQ0U7UUFBQSxvRUFFRTtRQXlIRixtRUFDRTtRQUdKLGlCQUFNO1FBRU4sd0NBRW9COztRQS9JZCxlQUFpQjtRQUFqQixxQ0FBaUI7UUFZaEIsZUFBMEI7UUFBMUIsNkNBQTBCO1FBMkgxQixlQUF5QjtRQUF6Qiw0Q0FBeUI7UUFPOUIsZUFBeUI7UUFBekIsMkNBQXlCOztrRERoSGQsZ0JBQWdCO2NBTDVCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3QixXQUFXLEVBQUUsMkJBQTJCO2dCQUN4QyxTQUFTLEVBQUUsQ0FBQywyQkFBMkIsQ0FBQzthQUN6QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IEZvZlBlcm1pc3Npb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xyXG5pbXBvcnQgeyBpUm9sZSwgaVVzZXIsIGlVc2VyUm9sZU9yZ2FuaXphdGlvbiwgaU9yZ2FuaXphdGlvbiB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IG1hcCwgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJ1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUsIGZvcmtKb2luLCBvZiB9IGZyb20gJ3J4anMnXHJcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIsIFZhbGlkYXRvcnMgIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCJcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZEaWFsb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtZGlhbG9nLnNlcnZpY2UnXHJcbmltcG9ydCB7IGZvZlV0aWxzRm9ybSB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLXV0aWxzJ1xyXG5pbXBvcnQgeyBNYXREaWFsb2csIE1hdERpYWxvZ1JlZiwgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nJ1xyXG5pbXBvcnQgeyBGb2ZVc2VyUm9sZXNTZWxlY3RDb21wb25lbnQgfSBmcm9tICcuLi9mb2YtdXNlci1yb2xlcy1zZWxlY3QvZm9mLXVzZXItcm9sZXMtc2VsZWN0LmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9mRXJyb3JTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtZXJyb3Iuc2VydmljZSdcclxuXHJcbmludGVyZmFjZSBpUm9sZVVJIGV4dGVuZHMgaVJvbGUge1xyXG4gIGNoZWNrZWQ/OiBib29sZWFuXHJcbiAgdXNlclJvbGVPcmdhbml6YXRpb24/OiBpVXNlclJvbGVPcmdhbml6YXRpb25cclxufVxyXG5cclxuaW50ZXJmYWNlIGlVc2VyUm9sZVRvRGlzcGxheSB7XHJcbiAgb3JnYW5pemF0aW9uSWQ/OiBudW1iZXIsXHJcbiAgb3JnYW5pemF0aW9uPzogc3RyaW5nLFxyXG4gIHJvbGVzPzogc3RyaW5nW11cclxuICB1c2VyUm9sZU9yZ2FuaXphdGlvbnM/OiBpVXNlclJvbGVPcmdhbml6YXRpb25bXVxyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1jb3JlLWZvZi11c2VyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9mLXVzZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi11c2VyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZlVzZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmb2ZQZXJtaXNzaW9uU2VydmljZTogRm9mUGVybWlzc2lvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZEaWFsb2dTZXJ2aWNlOiBGb2ZEaWFsb2dTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgbWF0RGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIGZvZkVycm9yU2VydmljZTogRm9mRXJyb3JTZXJ2aWNlXHJcbiAgKSB7ICAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgdXNlcklkOiA8bnVtYmVyPnVuZGVmaW5lZCxcclxuICAgIHVzZXJPcmdhbml6YXRpb25JZDogPG51bWJlcj51bmRlZmluZWQsXHJcbiAgICBvcmdhbml6YXRpb25BbHJlYWR5V2l0aFJvbGVzOiA8aU9yZ2FuaXphdGlvbltdPnVuZGVmaW5lZFxyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gICAgdXNlckxvYWQ6KCkgPT4ge1xyXG4gICAgICB0aGlzLnVpVmFyLmxvYWRpbmdVc2VyID0gdHJ1ZVxyXG4gICAgICBQcm9taXNlLmFsbChbXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlLmdldEFsbCgpLnRvUHJvbWlzZSgpLFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlci5nZXRXaXRoUm9sZUJ5SWQodGhpcy5wcmlWYXIudXNlcklkKS50b1Byb21pc2UoKSAgICAgICAgXHJcbiAgICAgIF0pXHJcbiAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgY29uc3Qgcm9sZXM6IGlSb2xlW10gPSByZXN1bHRbMF0gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRVc2VycyA9IHJlc3VsdFsxXVxyXG5cclxuICAgICAgICBjb25zb2xlLmxvZygnY3VycmVudFVzZXJzJywgY3VycmVudFVzZXJzKVxyXG4gICAgICBcclxuICAgICAgICB0aGlzLnVpVmFyLmFsbFJvbGVzID0gcm9sZXNcclxuICAgICAgICB0aGlzLnByaXZGdW5jLnVzZXJSZWZyZXNoKGN1cnJlbnRVc2VycykgICAgICAgICAgXHJcbiAgICAgICAgLy8gSGVyZSBmb3IgcHJldmVudGluZyB0byByZWZyZXNoIHRoZSB1c2VyIGZvcm0gd2hlbiByZWZyZXNoaW5nIG9ubHkgdGhlIHJvbGVzLlxyXG4gICAgICAgIC8vIHRvRG86IGltcHJvdmVcclxuICAgICAgICB0aGlzLnVpVmFyLmZvcm0ucGF0Y2hWYWx1ZSh0aGlzLnVpVmFyLnVzZXIpICAgICAgICAgICAgICAgIFxyXG4gICAgICB9KVxyXG4gICAgICAuY2F0Y2gocmVhc29uID0+IHsgICAgICAgIFxyXG4gICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy4uLyddLCB7cmVsYXRpdmVUbzogdGhpcy5hY3RpdmF0ZWRSb3V0ZX0pXHJcbiAgICAgIH0pXHJcbiAgICAgIC5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICB0aGlzLnVpVmFyLmxvYWRpbmdVc2VyID0gZmFsc2VcclxuICAgICAgfSlcclxuICAgIH0sXHJcbiAgICB1c2VyUmVmcmVzaDooY3VycmVudFVzZXI6IGlVc2VyKSA9PiB7XHJcbiAgICAgIFxyXG4gICAgICBpZiAoIWN1cnJlbnRVc2VyKSB7XHJcbiAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBDZSB1dGlsaXNhdGV1ciBuJ2V4aXN0ZSBwYXNgKVxyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLi4vJ10sIHtyZWxhdGl2ZVRvOiB0aGlzLmFjdGl2YXRlZFJvdXRlfSlcclxuICAgICAgICByZXR1cm5cclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy51aVZhci51c2VyID0gY3VycmVudFVzZXIgICAgICBcclxuXHJcbiAgICAgIGlmIChjdXJyZW50VXNlci5vcmdhbml6YXRpb25JZCkge1xyXG4gICAgICAgIHRoaXMudWlWYXIudXNlck9yZ2FuaXphdGlvbnNEaXNwbGF5ID0gW3tpZDogY3VycmVudFVzZXIub3JnYW5pemF0aW9uSWR9XVxyXG4gICAgICAgIHRoaXMucHJpVmFyLnVzZXJPcmdhbml6YXRpb25JZCA9IGN1cnJlbnRVc2VyLm9yZ2FuaXphdGlvbklkXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGxldCBfcHJldmlvdXNPcmdhbmlzYXRpb25JZDogbnVtYmVyID0gdW5kZWZpbmVkXHJcbiAgICAgIGxldCBfcm9sZVRvRGlzcGxheTogaVVzZXJSb2xlVG9EaXNwbGF5ID0gdW5kZWZpbmVkXHJcbiAgICAgIFxyXG4gICAgICB0aGlzLnVpVmFyLnVzZXJSb2xlc1RvRGlzcGxheSA9IFtdXHJcblxyXG4gICAgICBpZiAoY3VycmVudFVzZXIudXNlclJvbGVPcmdhbml6YXRpb25zICYmIGN1cnJlbnRVc2VyLnVzZXJSb2xlT3JnYW5pemF0aW9ucy5sZW5ndGggPiAwKSB7ICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIC8vIHdlIHdpbGwgZGlzcGxheSB0aGUgdXNlciByb2xlcyBncm91cGVkIGJ5IG9yZ2FuaXphdGlvbiBcclxuICAgICAgICAvLyB3aXRoIG9uZSBvbiBzZXZlcmFsIHJvbGVzIG9uIGl0XHJcbiAgICAgICAgY3VycmVudFVzZXIudXNlclJvbGVPcmdhbml6YXRpb25zLmZvckVhY2godXJvID0+IHtcclxuICAgICAgICAgIGlmIChfcHJldmlvdXNPcmdhbmlzYXRpb25JZCAhPT0gdXJvLm9yZ2FuaXphdGlvbi5pZCkge1xyXG4gICAgICAgICAgICAvLyBpdCdzIGEgbmV3IG9yZ2FuaXNhdGlvbiwgcmVpbml0XHJcbiAgICAgICAgICAgIF9wcmV2aW91c09yZ2FuaXNhdGlvbklkID0gdXJvLm9yZ2FuaXphdGlvbi5pZFxyXG4gICAgICAgICAgICBpZiAoX3JvbGVUb0Rpc3BsYXkpIHsgdGhpcy51aVZhci51c2VyUm9sZXNUb0Rpc3BsYXkucHVzaChfcm9sZVRvRGlzcGxheSkgfVxyXG4gICAgICAgICAgICBfcm9sZVRvRGlzcGxheSA9IHsgXHJcbiAgICAgICAgICAgICAgb3JnYW5pemF0aW9uSWQ6IHVyby5vcmdhbml6YXRpb24uaWQsXHJcbiAgICAgICAgICAgICAgb3JnYW5pemF0aW9uOiB1cm8ub3JnYW5pemF0aW9uLm5hbWUsXHJcbiAgICAgICAgICAgICAgcm9sZXM6IFtdLFxyXG4gICAgICAgICAgICAgIHVzZXJSb2xlT3JnYW5pemF0aW9uczogW11cclxuICAgICAgICAgICAgfSAgICAgICAgICAgIFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgX3JvbGVUb0Rpc3BsYXkucm9sZXMucHVzaCh1cm8ucm9sZS5jb2RlKVxyXG4gICAgICAgICAgX3JvbGVUb0Rpc3BsYXkudXNlclJvbGVPcmdhbml6YXRpb25zLnB1c2godXJvKVxyXG4gICAgICAgIH0pICAgICAgICAgICAgXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIGFkZCB0aGUgbGFzdCBvbmVcclxuICAgICAgaWYgKF9yb2xlVG9EaXNwbGF5KSB7IHRoaXMudWlWYXIudXNlclJvbGVzVG9EaXNwbGF5LnB1c2goX3JvbGVUb0Rpc3BsYXkpIH1cclxuXHJcbiAgICAgIC8vIHdlIGRvbid0IHdhbnQgdGhlIGZvbGxvd2luZyBvcmdhbml6YXRpb24gY291bGQgYmUgc2VsZWN0ZWJhbGUgaW4gdGhlIFxyXG4gICAgICAvLyBvcmdhbml6YXRpb24gdHJlZVxyXG4gICAgICB0aGlzLnByaVZhci5vcmdhbml6YXRpb25BbHJlYWR5V2l0aFJvbGVzID0gW11cclxuICAgICAgdGhpcy51aVZhci51c2VyUm9sZXNUb0Rpc3BsYXkuZm9yRWFjaChvcmcgPT4ge1xyXG4gICAgICAgIHRoaXMucHJpVmFyLm9yZ2FuaXphdGlvbkFscmVhZHlXaXRoUm9sZXMucHVzaCh7XHJcbiAgICAgICAgICBpZDogb3JnLm9yZ2FuaXphdGlvbklkLFxyXG4gICAgICAgICAgbmFtZTogb3JnLm9yZ2FuaXphdGlvblxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0pXHJcblxyXG4gICAgfSxcclxuICAgIHVzZVJvbGVzUmVmcmVzaDooKSA9PiB7ICAgICAgXHJcbiAgICAgIHRoaXMudWlWYXIubG9hZGluZ1JvbGVzID0gdHJ1ZVxyXG4gICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIuZ2V0V2l0aFJvbGVCeUlkKHRoaXMucHJpVmFyLnVzZXJJZClcclxuICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgIC50aGVuKHVzZXJzUmVzdWx0ID0+IHtcclxuICAgICAgICB0aGlzLnByaXZGdW5jLnVzZXJSZWZyZXNoKHVzZXJzUmVzdWx0KSAgICAgICAgICAgIFxyXG4gICAgICB9KVxyXG4gICAgICAuY2F0Y2gocmVhc29uID0+IHtcclxuICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgLy8gdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLi8nXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGV9KVxyXG4gICAgICB9KVxyXG4gICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nUm9sZXMgPSBmYWxzZVxyXG4gICAgICB9KSAgICAgICBcclxuICAgIH1cclxuICB9XHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlWYXIgPSB7XHJcbiAgICB0aXRsZTogJ05vdXZlbCB1dGlsaXNhdGV1cicsICAgIFxyXG4gICAgbG9hZGluZ1VzZXI6IGZhbHNlLFxyXG4gICAgbG9hZGluZ1JvbGVzOiBmYWxzZSxcclxuICAgIHVzZXJJc05ldzogZmFsc2UsXHJcbiAgICB1c2VyOiA8aVVzZXI+dW5kZWZpbmVkLFxyXG4gICAgdXNlck9yZ2FuaXphdGlvbnNEaXNwbGF5OiA8aU9yZ2FuaXphdGlvbltdPnVuZGVmaW5lZCxcclxuICAgIGZvcm06IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoeyAgICAgIFxyXG4gICAgICBsb2dpbjogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMzApXV0sXHJcbiAgICAgIGVtYWlsOiBbJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLmVtYWlsLCBWYWxpZGF0b3JzLm1heExlbmd0aCg2MCldXSxcclxuICAgICAgZmlyc3ROYW1lOiBbJycsIFtWYWxpZGF0b3JzLm1heExlbmd0aCgzMCldXSxcclxuICAgICAgbGFzdE5hbWU6IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDMwKV1dXHJcbiAgICB9KSxcclxuICAgIGFsbFJvbGVzOiA8aVJvbGVVSVtdPnVuZGVmaW5lZCxcclxuICAgIHNlbGVjdGVkVXNlclJvbGVPcmdhbml6YXRpb25zOiA8aVVzZXJSb2xlT3JnYW5pemF0aW9uW10+dW5kZWZpbmVkLFxyXG4gICAgdXNlclJvbGVzVG9EaXNwbGF5OiA8aVVzZXJSb2xlVG9EaXNwbGF5W10+W10sXHJcbiAgICByb2xlRGlzcGxheWVkQ29sdW1uczogWydkZWxldGUnLCAnb3JnYW5pemF0aW9uJywgJ3JvbGVzJywgJ2ljb24nXSxcclxuICAgIHJvbGVzQWxsOiA8aVJvbGVbXT51bmRlZmluZWRcclxuICB9XHJcbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpQWN0aW9uID0ge1xyXG4gICAgdXNlclNhdmU6KCkgPT4geyAgICAgIFxyXG4gICAgICBjb25zdCB1c2VyVG9TYXZlOiBpVXNlciA9IHRoaXMudWlWYXIuZm9ybS52YWx1ZVxyXG4gICAgICB1c2VyVG9TYXZlLm9yZ2FuaXphdGlvbklkID0gdGhpcy5wcmlWYXIudXNlck9yZ2FuaXphdGlvbklkXHJcblxyXG4gICAgICBpZiAoIXRoaXMudWlWYXIuZm9ybS52YWxpZCkge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignVmV1aWxsZXogY29ycmlnZXIgbGVzIGVycmV1cnMgYXZhbnQgZGUgc2F1dmVnYXJkZXInKVxyXG4gICAgICAgIGZvZlV0aWxzRm9ybS52YWxpZGF0ZUFsbEZpZWxkcyh0aGlzLnVpVmFyLmZvcm0pXHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnVpVmFyLnVzZXJJc05ldykgeyAgICAgICAgXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS51c2VyLmNyZWF0ZSh1c2VyVG9TYXZlKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKChuZXdVc2VyOiBpVXNlcikgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1V0aWxpc2F0ZXVyIHNhdXbDqScsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IDEwMDB9KVxyXG4gICAgICAgICAgdGhpcy5wcmlWYXIudXNlcklkID0gbmV3VXNlci5pZFxyXG4gICAgICAgICAgdGhpcy51aVZhci50aXRsZSA9IGBNb2RpZmljYXRpb24gZCd1biB1dGlsaXNhdGV1cmBcclxuICAgICAgICAgIHRoaXMudWlWYXIudXNlcklzTmV3ID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMucHJpdkZ1bmMudXNlckxvYWQoKSAgICAgICAgICBcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChyZWFzb24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdXNlclRvU2F2ZS5pZCA9IHRoaXMudWlWYXIudXNlci5pZCBcclxuICAgICAgXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS51c2VyLnVwZGF0ZSh1c2VyVG9TYXZlKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnVXRpbGlzYXRldXIgc2F1dsOpJywge211c3REaXNhcHBlYXJBZnRlcjogMTAwMH0pXHJcbiAgICAgICAgfSkgXHJcbiAgICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHVzZXJDYW5jZWw6KCkgPT4geyAgICAgIFxyXG4gICAgICB0aGlzLnByaXZGdW5jLnVzZXJMb2FkKClcclxuICAgIH0sXHJcbiAgICB1c2VyRGVsZXRlOigpID0+IHtcclxuICAgICAgdGhpcy5mb2ZEaWFsb2dTZXJ2aWNlLm9wZW5ZZXNObyh7ICAgICAgICBcclxuICAgICAgICBxdWVzdGlvbjogYFZvdWxleiB2b3VzIHZyYWltZW50IHN1cHByaW1lciBsJyB1dGlsaXNhdGV1ciA/YFxyXG4gICAgICB9KS50aGVuKHllcyA9PiB7XHJcbiAgICAgICAgaWYgKHllcykge1xyXG4gICAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS51c2VyLmRlbGV0ZSh0aGlzLnVpVmFyLnVzZXIpXHJcbiAgICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdVdGlsaXNhdGV1ciBzdXBwcmltw6knKVxyXG4gICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy4uLyddLCB7cmVsYXRpdmVUbzogdGhpcy5hY3RpdmF0ZWRSb3V0ZX0pXHJcbiAgICAgICAgICB9KSBcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9LCAgICAgICBcclxuICAgIHVzZXJSb2xlc0RlbGV0ZTooKSA9PiB7XHJcbiAgICAgIGNvbnN0IHVzZXJSb2xlcyA9IHRoaXMudWlWYXIudXNlclJvbGVzVG9EaXNwbGF5XHJcbiAgICAgIGNvbnN0IG9yZ2FuaXphdGlvbnNJZFRvRGVsZXRlOiBBcnJheTxudW1iZXI+ID0gW11cclxuICAgICAgbGV0IHNvbWV0aGluZ1RvRGVsZXRlID0gZmFsc2VcclxuICAgICAgbGV0IG1lc3NhZ2UgPSBgVm91bGV6IHZvdXMgdnJhaW1lbnQgc3VwcHJpbWVyIGxlcyByw7RsZXMgZGVzIG9yZ2FuaXNhdGlvbnMgc3VpdmFudGUgPzxicj5gXHJcbiAgICAgIG1lc3NhZ2UgPSBtZXNzYWdlICsgJzx1bD4nXHJcblxyXG4gICAgICB1c2VyUm9sZXMuZm9yRWFjaCh1ciA9PiB7XHJcbiAgICAgICAgLy9kb24ndCB3YW50IHRvIGNyZWF0ZSBhbiBpbnRlcmZhY2UgZm9yIDEgcGFyYW0pXHJcbiAgICAgICAgaWYgKHVyWydjaGVja2VkJ10pIHtcclxuICAgICAgICAgIHNvbWV0aGluZ1RvRGVsZXRlID0gdHJ1ZVxyXG4gICAgICAgICAgbWVzc2FnZSA9IG1lc3NhZ2UgKyBgPGxpPiR7dXIub3JnYW5pemF0aW9ufTwvbGk+YFxyXG4gICAgICAgICAgb3JnYW5pemF0aW9uc0lkVG9EZWxldGUucHVzaCh1ci5vcmdhbml6YXRpb25JZClcclxuICAgICAgICB9ICAgICAgXHJcbiAgICAgIH0pXHJcblxyXG4gICAgICBtZXNzYWdlID0gbWVzc2FnZSArICc8L3VsPicgICAgICBcclxuXHJcblxyXG4gICAgICBpZiAoc29tZXRoaW5nVG9EZWxldGUpIHtcclxuICAgICAgICB0aGlzLmZvZkRpYWxvZ1NlcnZpY2Uub3Blblllc05vKHsgICAgICAgIFxyXG4gICAgICAgICAgdGl0bGU6ICdTdXBwcmltZXIgcsO0bGVzJyxcclxuICAgICAgICAgIHF1ZXN0aW9uOiBtZXNzYWdlXHJcbiAgICAgICAgfSkudGhlbih5ZXMgPT4ge1xyXG4gICAgICAgICAgaWYgKHllcykgeyAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIuZGVsZXRlVXNlclJvbGVPcmdhbml6YXRpb25zKHRoaXMudWlWYXIudXNlci5pZCwgb3JnYW5pemF0aW9uc0lkVG9EZWxldGUpXHJcbiAgICAgICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdSw7RsZXMgc3VwcHJpbcOpcycpXHJcbiAgICAgICAgICAgICAgdGhpcy5wcml2RnVuYy51c2VSb2xlc1JlZnJlc2goKSAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH0pIFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmluZm8oJ1ZvdXMgZGV2ZXogc8OpbGVjdGlvbm5lciB1YSBtb2lucyB1bmUgb3JnYW5pc2F0aW9uJylcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHJvbGVTZWxlY3RDb21wb25lbnRPcGVuOih1c2VyUm9sZT86IGlVc2VyUm9sZVRvRGlzcGxheSkgPT4geyAgICAgIFxyXG4gICAgICBsZXQgdXNlclJvbGVPcmdhbml6YXRpb25zOiBpVXNlclJvbGVPcmdhbml6YXRpb25bXVxyXG5cclxuICAgICAgaWYgKHVzZXJSb2xlKSB7XHJcbiAgICAgICAgdXNlclJvbGVPcmdhbml6YXRpb25zID0gdXNlclJvbGUudXNlclJvbGVPcmdhbml6YXRpb25zXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMubWF0RGlhbG9nLm9wZW4oRm9mVXNlclJvbGVzU2VsZWN0Q29tcG9uZW50LCB7XHJcbiAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgcm9sZXM6IHRoaXMudWlWYXIuYWxsUm9sZXMsXHJcbiAgICAgICAgICB1c2VyUm9sZU9yZ2FuaXphdGlvbnM6IHVzZXJSb2xlT3JnYW5pemF0aW9ucyxcclxuICAgICAgICAgIG5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zOiB0aGlzLnByaVZhci5vcmdhbml6YXRpb25BbHJlYWR5V2l0aFJvbGVzLFxyXG4gICAgICAgICAgdXNlcklkOiB0aGlzLnVpVmFyLnVzZXIuaWRcclxuICAgICAgICB9LFxyXG4gICAgICAgIHdpZHRoOiAnNjAwcHgnICxcclxuICAgICAgICBoZWlnaHQ6ICdjYWxjKDEwMHZoIC0gMjAwcHgpJyAgICAgIFxyXG4gICAgICB9KVxyXG4gICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgICAudG9Qcm9taXNlKClcclxuICAgICAgLnRoZW4obXVzdEJlUmVmcmVzZCA9PiB7ICAgICAgICBcclxuICAgICAgICBpZiAobXVzdEJlUmVmcmVzZCkge1xyXG4gICAgICAgICAgdGhpcy5wcml2RnVuYy51c2VSb2xlc1JlZnJlc2goKVxyXG4gICAgICAgIH0gICAgICAgIFxyXG4gICAgICB9KVxyXG4gICAgfSxcclxuICAgIG9yZ2FuaXNhdGlvbk11bHRpU2VsZWN0ZWRDaGFuZ2U6KG9yZ2FuaXphdGlvbnM6IGlPcmdhbml6YXRpb25bXSkgPT4geyAgICAgIFxyXG4gICAgICBpZiAob3JnYW5pemF0aW9ucyAmJiBvcmdhbml6YXRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICB0aGlzLnByaVZhci51c2VyT3JnYW5pemF0aW9uSWQgPSBvcmdhbml6YXRpb25zWzBdLmlkXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5wcmlWYXIudXNlck9yZ2FuaXphdGlvbklkID0gbnVsbFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmFjdGl2YXRlZFJvdXRlLnBhcmFtTWFwLnN1YnNjcmliZShwYXJhbXMgPT4ge1xyXG4gICAgICBjb25zdCBjb2RlOmFueSA9IHBhcmFtcy5nZXQoJ2NvZGUnKVxyXG4gICAgICBcclxuICAgICAgaWYgKGNvZGUpIHtcclxuICAgICAgICBpZiAoY29kZS50b0xvd2VyQ2FzZSgpID09ICduZXcnKSB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnVzZXJJc05ldyA9IHRydWVcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5wcmlWYXIudXNlcklkID0gY29kZVxyXG4gICAgICAgICAgdGhpcy51aVZhci50aXRsZSA9IGBNb2RpZmljYXRpb24gZCd1biB1dGlsaXNhdGV1cmBcclxuICAgICAgICAgIHRoaXMucHJpdkZ1bmMudXNlckxvYWQoKVxyXG4gICAgICAgIH1cclxuICAgICAgfSAgICAgIFxyXG4gICAgfSlcclxuICB9XHJcbn1cclxuIiwiPG1hdC1jYXJkIGNsYXNzPVwiZm9mLWhlYWRlclwiPiAgICAgICAgXHJcbiAgPGgzPnt7IHVpVmFyLnRpdGxlIH19PC9oMz4gXHJcbiAgPGRpdiBjbGFzcz1cImZvZi10b29sYmFyXCI+XHJcbiAgICA8YnV0dG9uIG1hdC1zdHJva2VkLWJ1dHRvblxyXG4gICAgICAoY2xpY2spPVwidWlBY3Rpb24udXNlckNhbmNlbCgpXCI+QW5udWxlcjwvYnV0dG9uPlxyXG4gICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJ3YXJuXCJcclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVzZXJEZWxldGUoKVwiPlN1cHByaW1lcjwvYnV0dG9uPlxyXG4gICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJhY2NlbnRcIlxyXG4gICAgICAoY2xpY2spPVwidWlBY3Rpb24udXNlclNhdmUoKVwiPlxyXG4gICAgICBFbnJlZ2lzdGVyPC9idXR0b24+ICAgIFxyXG4gIDwvZGl2PiBcclxuPC9tYXQtY2FyZD5cclxuPGRpdiBjbGFzcz1cIm1haW5cIj4gIFxyXG4gIDxkaXYgKm5nSWY9XCIhdWlWYXIubG9hZGluZ1VzZXJcIiBjbGFzcz1cImRldGFpbCBmb2YtZmFkZS1pblwiPiAgICBcclxuXHJcbiAgICA8ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJjb2wtbWQtNlwiPlxyXG5cclxuICAgICAgICA8Zm9ybSBbZm9ybUdyb3VwXT1cInVpVmFyLmZvcm1cIj5cclxuXHJcbiAgICAgICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgICAgIDxpbnB1dCBtYXRJbnB1dCBcclxuICAgICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJsb2dpblwiXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJsb2dpblwiIHZhbHVlPVwiXCI+XHJcbiAgICAgICAgICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJ1aVZhci5mb3JtLmdldCgnbG9naW4nKS5pbnZhbGlkXCI+XHJcbiAgICAgICAgICAgICAgTGUgbG9naW4gbmUgZG9pdCBwYXMgZXhjw6lkZXIgMzAgY2FyYWN0w6hyZXNcclxuICAgICAgICAgICAgPC9tYXQtZXJyb3I+XHJcbiAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG5cclxuICAgICAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICAgICAgPGlucHV0IG1hdElucHV0IHJlcXVpcmVkIFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJlbWFpbFwiXHJcbiAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiZW1haWxcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW1haWxcIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgICAgICA8bWF0LWVycm9yICpuZ0lmPVwidWlWYXIuZm9ybS5nZXQoJ2VtYWlsJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICAgIFVuIGVtYWlsIHZhbGlkZSBlc3Qgb2JsaWdhdG9pcmVcclxuICAgICAgICAgICAgPC9tYXQtZXJyb3I+XHJcbiAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG5cclxuICAgICAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICAgICAgPGlucHV0IG1hdElucHV0IHJlcXVpcmVkIFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJmaXJzdE5hbWVcIlxyXG4gICAgICAgICAgICAgIGZvcm1Db250cm9sTmFtZT1cImZpcnN0TmFtZVwiXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJQcsOpbm9tXCIgdmFsdWU9XCJcIj5cclxuICAgICAgICAgICAgPG1hdC1lcnJvciAqbmdJZj1cInVpVmFyLmZvcm0uZ2V0KCdmaXJzdE5hbWUnKS5pbnZhbGlkXCI+XHJcbiAgICAgICAgICAgICAgTGUgcHLDqW5vbSBuZSBkb2l0IHBhcyBleGPDqWRlciAzMCBjYXJhY3TDqHJlc1xyXG4gICAgICAgICAgICA8L21hdC1lcnJvcj5cclxuICAgICAgICAgIDwvbWF0LWZvcm0tZmllbGQ+XHJcblxyXG4gICAgICAgICAgPG1hdC1mb3JtLWZpZWxkPlxyXG4gICAgICAgICAgICA8aW5wdXQgbWF0SW5wdXQgcmVxdWlyZWQgXHJcbiAgICAgICAgICAgICAgdHlwZT1cImxhc3ROYW1lXCJcclxuICAgICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJsYXN0TmFtZVwiXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJOb20gZGUgZmFtaWxsZVwiIHZhbHVlPVwiXCI+XHJcbiAgICAgICAgICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJ1aVZhci5mb3JtLmdldCgnbGFzdE5hbWUnKS5pbnZhbGlkXCI+XHJcbiAgICAgICAgICAgICAgTGUgbm9tIGRlIGZhbWlsbGUgbmUgZG9pdCBwYXMgZXhjw6lkZXIgMzAgY2FyYWN0w6hyZXNcclxuICAgICAgICAgICAgPC9tYXQtZXJyb3I+XHJcbiAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPiAgICAgXHJcbiAgICAgICAgICBcclxuICAgICAgICAgIDxmb2YtY29yZS1mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3RcclxuICAgICAgICAgICAgW3BsYWNlSG9sZGVyXT1cIidPcmdhbmlzYXRpb24gZGUgcmF0dGFjaGVtZW50J1wiXHJcbiAgICAgICAgICAgIFttdWx0aVNlbGVjdF09XCJmYWxzZVwiICAgIFxyXG4gICAgICAgICAgICBbc2VsZWN0ZWRPcmdhbmlzYXRpb25zXT1cInVpVmFyLnVzZXJPcmdhbml6YXRpb25zRGlzcGxheVwiICAgXHJcbiAgICAgICAgICAgIChzZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2UpID0gXCJ1aUFjdGlvbi5vcmdhbmlzYXRpb25NdWx0aVNlbGVjdGVkQ2hhbmdlKCRldmVudClcIiAgIFxyXG4gICAgICAgICAgPjwvZm9mLWNvcmUtZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0PlxyXG4gICAgICAgXHJcbiAgICAgICAgPC9mb3JtPlxyXG4gICAgICBcclxuICAgICAgPC9kaXY+XHJcblxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIiAqbmdJZj1cInVpVmFyLmFsbFJvbGVzXCI+XHJcblxyXG4gICAgICAgIDxtYXQtY2FyZD5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb2YtaGVhZGVyXCI+XHJcbiAgICAgICAgICAgIDxoND5Sw7RsZXM8L2g0PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9mLXRvb2xiYXJcIj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIG1hdC1zdHJva2VkLWJ1dHRvbiBjb2xvcj1cIndhcm5cIlxyXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVzZXJSb2xlc0RlbGV0ZSgpXCI+U3VwcHJpbWVyIHLDtGxlczwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uXHJcbiAgICAgICAgICAgICAgICAoY2xpY2spPVwidWlBY3Rpb24ucm9sZVNlbGVjdENvbXBvbmVudE9wZW4oKVwiPkFqb3V0ZXIgdW4gcsO0bGU8L2J1dHRvbj4gICAgICAgICAgIFxyXG4gICAgICAgICAgICA8L2Rpdj4gXHJcbiAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9mLXRhYmxlLWNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgICA8ZGl2ICpuZ0lmPVwidWlWYXIubG9hZGluZ1JvbGVzXCIgY2xhc3M9XCJmb2YtbG9hZGluZyBmb2YtbG9hZGluZy1yb2xlc1wiPlxyXG4gICAgICAgICAgICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5SYWZyYWljaGlzc2VtZW50IGRlcyByw7RsZXMuLi48L3NwYW4+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPHRhYmxlIG1hdC10YWJsZSBbZGF0YVNvdXJjZV09XCJ1aVZhci51c2VyUm9sZXNUb0Rpc3BsYXlcIiBjbGFzcz1cImRhdGEtdGFibGVcIj5cclxuXHJcbiAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJkZWxldGVcIj5cclxuICAgICAgICAgICAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWY+PC90aD5cclxuICAgICAgICAgICAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIj4gIFxyXG4gICAgICAgICAgICAgICAgICA8bWF0LWNoZWNrYm94XHJcbiAgICAgICAgICAgICAgICAgICAgWyhuZ01vZGVsKV09XCJyb3cuY2hlY2tlZFwiPlxyXG4gICAgICAgICAgICAgICAgICA8L21hdC1jaGVja2JveD5cclxuICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJvcmdhbml6YXRpb25cIj5cclxuICAgICAgICAgICAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWY+T3JnYW5pc2F0aW9uPC90aD5cclxuICAgICAgICAgICAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIlxyXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnJvbGVTZWxlY3RDb21wb25lbnRPcGVuKHJvdylcIj5cclxuICAgICAgICAgICAgICAgIHt7cm93Lm9yZ2FuaXphdGlvbn19PC90ZD5cclxuICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwicm9sZXNcIj5cclxuICAgICAgICAgICAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWY+Um9sZXM8L3RoPlxyXG4gICAgICAgICAgICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiXHJcbiAgICAgICAgICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5yb2xlU2VsZWN0Q29tcG9uZW50T3Blbihyb3cpXCI+ICBcclxuICAgICAgICAgICAgICAgICAgPGRpdiAqbmdGb3I9XCJsZXQgcm9sZSBvZiByb3cucm9sZXNcIj5cclxuICAgICAgICAgICAgICAgICAgICB7eyByb2xlIH19XHJcbiAgICAgICAgICAgICAgICAgIDwvZGl2PiAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJpY29uXCI+XHJcbiAgICAgICAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPjwvdGg+XHJcbiAgICAgICAgICAgICAgICA8dGQgY2xhc3M9XCJpY29uLXNlbGVjdFwiIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiXHJcbiAgICAgICAgICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5yb2xlU2VsZWN0Q29tcG9uZW50T3Blbihyb3cpXCI+ICBcclxuICAgICAgICAgICAgICAgICAgPG1hdC1pY29uPmNoZXZyb25fcmlnaHQ8L21hdC1pY29uPlxyXG4gICAgICAgICAgICAgICAgPC90ZD5cclxuICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIDx0ciBtYXQtaGVhZGVyLXJvdyAqbWF0SGVhZGVyUm93RGVmPVwidWlWYXIucm9sZURpc3BsYXllZENvbHVtbnNcIj48L3RyPlxyXG4gICAgICAgICAgICAgIDx0ciBtYXQtcm93IGNsYXNzPVwiZm9mLWVsZW1lbnQtb3ZlclwiIFxyXG4gICAgICAgICAgICAgICAgKm1hdFJvd0RlZj1cImxldCByb3c7IGNvbHVtbnM6IHVpVmFyLnJvbGVEaXNwbGF5ZWRDb2x1bW5zO1wiPjwvdHI+XHJcblxyXG4gICAgICAgICAgICA8L3RhYmxlPiAgICAgICAgXHJcbiAgICAgICAgICBcclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbWF0LWNhcmQ+XHJcblxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgKm5nSWY9XCJ1aVZhci5sb2FkaW5nVXNlclwiIGNsYXNzPVwiZm9mLWxvYWRpbmdcIj5cclxuICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5DaGFyZ2VtZW50cyBkZXMgdXRpbGlzYXRldXJzIGV0IGRlcyBhdXRob3Jpc2F0aW9ucy4uLjwvc3Bhbj5cclxuICA8L2Rpdj5cclxuICAgIFxyXG48L2Rpdj5cclxuXHJcbjxmb2YtZW50aXR5LWZvb3RlclxyXG4gIFtlbnRpdHlCYXNlXT1cInVpVmFyLnVzZXJcIj5cclxuPC9mb2YtZW50aXR5LWZvb3Rlcj4iXX0=
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FofOrganizationsTreeComponent } from './fof-organizations-tree/fof-organizations-tree.component';
import { ComponentsService } from './components.service';
import { FofPermissionModule } from '../permission/permission.module';
import { MaterialModule } from '../core/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FofOrganizationsMultiSelectComponent } from './fof-organizations-multi-select/fof-organizations-multi-select.component';
import { FofDatatableComponent } from './fof-datatable/fof-datatable.component';
import * as i0 from "@angular/core";
export class ComponentsModule {
}
ComponentsModule.ɵmod = i0.ɵɵdefineNgModule({ type: ComponentsModule });
ComponentsModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ComponentsModule_Factory(t) { return new (t || ComponentsModule)(); }, providers: [
        ComponentsService
    ], imports: [[
            CommonModule,
            FofPermissionModule,
            MaterialModule,
            FormsModule,
            ReactiveFormsModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ComponentsModule, { declarations: [FofOrganizationsTreeComponent,
        FofOrganizationsMultiSelectComponent,
        FofDatatableComponent], imports: [CommonModule,
        FofPermissionModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule], exports: [FofOrganizationsTreeComponent,
        FofOrganizationsMultiSelectComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ComponentsModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    FofOrganizationsTreeComponent,
                    FofOrganizationsMultiSelectComponent,
                    FofDatatableComponent
                ],
                imports: [
                    CommonModule,
                    FofPermissionModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                providers: [
                    ComponentsService
                ],
                exports: [
                    FofOrganizationsTreeComponent,
                    FofOrganizationsMultiSelectComponent
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50cy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvbXBvbmVudHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDJEQUEyRCxDQUFBO0FBQ3pHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFBO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFBO0FBQ3JFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLG9DQUFvQyxFQUFFLE1BQU0sMkVBQTJFLENBQUM7QUFDakksT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seUNBQXlDLENBQUE7O0FBdUIvRSxNQUFNLE9BQU8sZ0JBQWdCOztvREFBaEIsZ0JBQWdCOytHQUFoQixnQkFBZ0IsbUJBUmhCO1FBQ1QsaUJBQWlCO0tBQ2xCLFlBVFE7WUFDUCxZQUFZO1lBQ1osbUJBQW1CO1lBQ25CLGNBQWM7WUFDZCxXQUFXO1lBQ1gsbUJBQW1CO1NBQ3BCO3dGQVNVLGdCQUFnQixtQkFuQnpCLDZCQUE2QjtRQUM3QixvQ0FBb0M7UUFDcEMscUJBQXFCLGFBR3JCLFlBQVk7UUFDWixtQkFBbUI7UUFDbkIsY0FBYztRQUNkLFdBQVc7UUFDWCxtQkFBbUIsYUFNbkIsNkJBQTZCO1FBQzdCLG9DQUFvQztrREFHM0IsZ0JBQWdCO2NBckI1QixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLDZCQUE2QjtvQkFDN0Isb0NBQW9DO29CQUNwQyxxQkFBcUI7aUJBQ3RCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLG1CQUFtQjtvQkFDbkIsY0FBYztvQkFDZCxXQUFXO29CQUNYLG1CQUFtQjtpQkFDcEI7Z0JBQ0QsU0FBUyxFQUFFO29CQUNULGlCQUFpQjtpQkFDbEI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLDZCQUE2QjtvQkFDN0Isb0NBQW9DO2lCQUNyQzthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXHJcbmltcG9ydCB7IEZvZk9yZ2FuaXphdGlvbnNUcmVlQ29tcG9uZW50IH0gZnJvbSAnLi9mb2Ytb3JnYW5pemF0aW9ucy10cmVlL2ZvZi1vcmdhbml6YXRpb25zLXRyZWUuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBDb21wb25lbnRzU2VydmljZSB9IGZyb20gJy4vY29tcG9uZW50cy5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uTW9kdWxlIH0gZnJvbSAnLi4vcGVybWlzc2lvbi9wZXJtaXNzaW9uLm1vZHVsZSdcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9jb3JlL21hdGVyaWFsLm1vZHVsZSdcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEZvZk9yZ2FuaXphdGlvbnNNdWx0aVNlbGVjdENvbXBvbmVudCB9IGZyb20gJy4vZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0L2ZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb2ZEYXRhdGFibGVDb21wb25lbnQgfSBmcm9tICcuL2ZvZi1kYXRhdGFibGUvZm9mLWRhdGF0YWJsZS5jb21wb25lbnQnXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgRm9mT3JnYW5pemF0aW9uc1RyZWVDb21wb25lbnQsXHJcbiAgICBGb2ZPcmdhbml6YXRpb25zTXVsdGlTZWxlY3RDb21wb25lbnQsXHJcbiAgICBGb2ZEYXRhdGFibGVDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvZlBlcm1pc3Npb25Nb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLCBcclxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGVcclxuICBdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgQ29tcG9uZW50c1NlcnZpY2VcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtcclxuICAgIEZvZk9yZ2FuaXphdGlvbnNUcmVlQ29tcG9uZW50LFxyXG4gICAgRm9mT3JnYW5pemF0aW9uc011bHRpU2VsZWN0Q29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tcG9uZW50c01vZHVsZSB7IH1cclxuIl19
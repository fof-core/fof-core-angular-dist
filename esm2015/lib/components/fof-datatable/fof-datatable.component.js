import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
export class FofDatatableComponent {
    constructor(fofPermissionService, fofNotificationService, breakpointObserver) {
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['code', 'description'],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    ngOnInit() {
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe((state) => {
            if (state.matches) {
                // XSmall
                this.uiVar.displayedColumns = ['code'];
            }
            else {
                // > XSmall
                this.uiVar.displayedColumns = ['code', 'description'];
            }
        });
    }
    ngAfterViewInit() {
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(() => {
            this.uiVar.isLoadingResults = true;
            this.uiVar.pageSize = this.paginator.pageSize;
            return this.fofPermissionService.role.search(null, this.uiVar.pageSize, this.paginator.pageIndex, this.sort.active, this.sort.direction);
        }), map((search) => {
            this.uiVar.isLoadingResults = false;
            this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(() => {
            this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(data => this.uiVar.data = data);
    }
    ngOnDestroy() {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    }
}
FofDatatableComponent.ɵfac = function FofDatatableComponent_Factory(t) { return new (t || FofDatatableComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver)); };
FofDatatableComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofDatatableComponent, selectors: [["fof-core-fof-datatable"]], viewQuery: function FofDatatableComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(MatPaginator, true);
        i0.ɵɵviewQuery(MatSort, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
    } }, decls: 2, vars: 0, template: function FofDatatableComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "p");
        i0.ɵɵtext(1, "fof-datatable works!");
        i0.ɵɵelementEnd();
    } }, styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofDatatableComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-datatable',
                templateUrl: './fof-datatable.component.html',
                styleUrls: ['./fof-datatable.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWRhdGF0YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2ZvZi1kYXRhdGFibGUvZm9mLWRhdGF0YWJsZS5jb21wb25lbnQudHMiLCJsaWIvY29tcG9uZW50cy9mb2YtZGF0YXRhYmxlL2ZvZi1kYXRhdGFibGUuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBbUMsU0FBUyxFQUE0QixNQUFNLGVBQWUsQ0FBQTtBQUkvRyxPQUFPLEVBQUUsWUFBWSxFQUFvQixNQUFNLDZCQUE2QixDQUFBO0FBQzVFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQTtBQUNoRCxPQUFPLEVBQUUsS0FBSyxFQUFjLEVBQUUsSUFBSSxZQUFZLEVBQWdCLE1BQU0sTUFBTSxDQUFBO0FBQzFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUV0RSxPQUFPLEVBQXNCLFdBQVcsRUFBbUIsTUFBTSxxQkFBcUIsQ0FBQTs7Ozs7QUFPdEYsTUFBTSxPQUFPLHFCQUFxQjtJQUloQyxZQUNVLG9CQUEwQyxFQUMxQyxzQkFBOEMsRUFDOUMsa0JBQXNDO1FBRnRDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBS2hELHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixxQkFBcUIsRUFBZ0IsU0FBUztTQUMvQyxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRyxFQUNsQixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLGdCQUFnQixFQUFZLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQztZQUNuRCxJQUFJLEVBQVcsRUFBRTtZQUNqQixhQUFhLEVBQUUsQ0FBQztZQUNoQixRQUFRLEVBQUUsQ0FBQztZQUNYLGdCQUFnQixFQUFFLElBQUk7U0FDdkIsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUcsRUFDakIsQ0FBQTtJQW5CRCxDQUFDO0lBb0JELGlCQUFpQjtJQUNqQixRQUFRO1FBQ04sSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDdEYsU0FBUyxDQUFDLENBQUMsS0FBc0IsRUFBRSxFQUFFO1lBQ3BDLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtnQkFDakIsU0FBUztnQkFDVCxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7YUFDdkM7aUJBQU07Z0JBQ0wsV0FBVztnQkFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLENBQUMsTUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFBO2FBQ3REO1FBQ0gsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsZUFBZTtRQUNiLG9FQUFvRTtRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFbEUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO2FBQzdDLElBQUksQ0FDSCxTQUFTLENBQUMsRUFBRSxDQUFDLEVBQ2IsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFBO1lBQ2xDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFBO1lBQzdDLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNyRSxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3BFLENBQUMsQ0FBQyxFQUNGLEdBQUcsQ0FBQyxDQUFDLE1BQXlCLEVBQUUsRUFBRTtZQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQTtZQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFBO1lBQ3ZDLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNwQixDQUFDLENBQUMsRUFDRixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUE7WUFDbkMsT0FBTyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUE7UUFDekIsQ0FBQyxDQUFDLENBQ0gsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsQ0FBQTtJQUMvQyxDQUFDO0lBRUQsV0FBVztRQUNULElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRTtZQUFFLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUE7U0FBRTtJQUM1RixDQUFDOzswRkF2RVUscUJBQXFCOzBEQUFyQixxQkFBcUI7dUJBQ3JCLFlBQVk7dUJBQ1osT0FBTzs7Ozs7O1FDbEJwQix5QkFBRztRQUFBLG9DQUFvQjtRQUFBLGlCQUFJOztrRERnQmQscUJBQXFCO2NBTGpDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsd0JBQXdCO2dCQUNsQyxXQUFXLEVBQUUsZ0NBQWdDO2dCQUM3QyxTQUFTLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQzthQUM5Qzs7a0JBRUUsU0FBUzttQkFBQyxZQUFZOztrQkFDdEIsU0FBUzttQkFBQyxPQUFPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IEZvZlBlcm1pc3Npb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IGlSb2xlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yLCBNYXRQYWdpbmF0b3JJbnRsIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJ1xyXG5pbXBvcnQgeyBNYXRTb3J0IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc29ydCdcclxuaW1wb3J0IHsgbWVyZ2UsIE9ic2VydmFibGUsIG9mIGFzIG9ic2VydmFibGVPZiwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCBzdGFydFdpdGgsIHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJ1xyXG5pbXBvcnQgeyBpZm9mU2VhcmNoIH0gZnJvbSAnLi4vLi4vY29yZS9jb3JlLmludGVyZmFjZSdcclxuaW1wb3J0IHsgQnJlYWtwb2ludE9ic2VydmVyLCBCcmVha3BvaW50cywgQnJlYWtwb2ludFN0YXRlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCdcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZm9mLWNvcmUtZm9mLWRhdGF0YWJsZScsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZvZi1kYXRhdGFibGUuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi1kYXRhdGFibGUuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9mRGF0YXRhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBAVmlld0NoaWxkKE1hdFBhZ2luYXRvcikgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3JcclxuICBAVmlld0NoaWxkKE1hdFNvcnQpIHNvcnQ6IE1hdFNvcnRcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvZlBlcm1pc3Npb25TZXJ2aWNlOiBGb2ZQZXJtaXNzaW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgYnJlYWtwb2ludE9ic2VydmVyOiBCcmVha3BvaW50T2JzZXJ2ZXJcclxuICApIHsgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgYnJlYWtwb2ludE9ic2VydmVyU3ViOiA8U3Vic2NyaXB0aW9uPnVuZGVmaW5lZCxcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICB9XHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlWYXIgPSB7ICAgIFxyXG4gICAgZGlzcGxheWVkQ29sdW1uczogPHN0cmluZ1tdPlsnY29kZScsICdkZXNjcmlwdGlvbiddLFxyXG4gICAgZGF0YTogPGlSb2xlW10+W10sXHJcbiAgICByZXN1bHRzTGVuZ3RoOiAwLFxyXG4gICAgcGFnZVNpemU6IDUsXHJcbiAgICBpc0xvYWRpbmdSZXN1bHRzOiB0cnVlXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcclxuICB9XHJcbiAgLy8gQW5ndWxhciBldmVudHNcclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1YiA9IHRoaXMuYnJlYWtwb2ludE9ic2VydmVyLm9ic2VydmUoQnJlYWtwb2ludHMuWFNtYWxsKVxyXG4gICAgLnN1YnNjcmliZSgoc3RhdGU6IEJyZWFrcG9pbnRTdGF0ZSkgPT4geyAgICAgIFxyXG4gICAgICBpZiAoc3RhdGUubWF0Y2hlcykge1xyXG4gICAgICAgIC8vIFhTbWFsbFxyXG4gICAgICAgIHRoaXMudWlWYXIuZGlzcGxheWVkQ29sdW1ucyA9IFsnY29kZSddXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gPiBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ2NvZGUnLCAnZGVzY3JpcHRpb24nXVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0gIFxyXG5cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7ICAgIFxyXG4gICAgLy8gSWYgdGhlIHVzZXIgY2hhbmdlcyB0aGUgc29ydCBvcmRlciwgcmVzZXQgYmFjayB0byB0aGUgZmlyc3QgcGFnZS5cclxuICAgIHRoaXMuc29ydC5zb3J0Q2hhbmdlLnN1YnNjcmliZSgoKSA9PiB0aGlzLnBhZ2luYXRvci5wYWdlSW5kZXggPSAwKVxyXG5cclxuICAgIG1lcmdlKHRoaXMuc29ydC5zb3J0Q2hhbmdlLCB0aGlzLnBhZ2luYXRvci5wYWdlKVxyXG4gICAgICAucGlwZShcclxuICAgICAgICBzdGFydFdpdGgoe30pLFxyXG4gICAgICAgIHN3aXRjaE1hcCgoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSB0cnVlICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5wYWdlU2l6ZSA9IHRoaXMucGFnaW5hdG9yLnBhZ2VTaXplXHJcbiAgICAgICAgICByZXR1cm4gdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5yb2xlLnNlYXJjaCAobnVsbCwgdGhpcy51aVZhci5wYWdlU2l6ZSwgXHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCwgdGhpcy5zb3J0LmFjdGl2ZSwgdGhpcy5zb3J0LmRpcmVjdGlvbilcclxuICAgICAgICB9KSxcclxuICAgICAgICBtYXAoKHNlYXJjaDogaWZvZlNlYXJjaDxpUm9sZT4pID0+IHsgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMudWlWYXIucmVzdWx0c0xlbmd0aCA9IHNlYXJjaC50b3RhbFxyXG4gICAgICAgICAgcmV0dXJuIHNlYXJjaC5kYXRhXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgY2F0Y2hFcnJvcigoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSBmYWxzZSAgICAgICAgICBcclxuICAgICAgICAgIHJldHVybiBvYnNlcnZhYmxlT2YoW10pXHJcbiAgICAgICAgfSlcclxuICAgICAgKS5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnVpVmFyLmRhdGEgPSBkYXRhKVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViKSB7IHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1Yi51bnN1YnNjcmliZSgpIH1cclxuICB9XHJcbn1cclxuIiwiPHA+Zm9mLWRhdGF0YWJsZSB3b3JrcyE8L3A+XHJcbiJdfQ==
import { Component, Output, EventEmitter, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/form-field";
import * as i2 from "@angular/material/chips";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "../fof-organizations-tree/fof-organizations-tree.component";
import * as i6 from "@angular/material/icon";
function FofOrganizationsMultiSelectComponent_mat_chip_4_Template(rf, ctx) { if (rf & 1) {
    const _r12 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-chip", 7);
    i0.ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_click_0_listener() { i0.ɵɵrestoreView(_r12); const ctx_r11 = i0.ɵɵnextContext(); return ctx_r11.uiAction.openTree(); })("removed", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_removed_0_listener() { i0.ɵɵrestoreView(_r12); const organisation_r10 = ctx.$implicit; const ctx_r13 = i0.ɵɵnextContext(); return ctx_r13.uiAction.organisationRemove(organisation_r10); });
    i0.ɵɵtext(1);
    i0.ɵɵelementStart(2, "mat-icon", 8);
    i0.ɵɵtext(3, "cancel");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const organisation_r10 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", organisation_r10.name, " ");
} }
// https://stackblitz.com/edit/menu-overlay?file=src%2Fapp%2Fselect-input.component.html
export class FofOrganizationsMultiSelectComponent {
    constructor(elementRef) {
        this.elementRef = elementRef;
        this.multiSelect = true;
        this.selectedOrganisations = [];
        this.placeHolder = 'Organisations';
        this.selectedOrganizationsChange = new EventEmitter();
        // All private variables
        this.priVar = {};
        // All private functions
        this.privFunc = {
            refreshOuputs: () => {
                this.selectedOrganizationsChange.emit(this.selectedOrganisations);
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            treeMenuVisible: false,
        };
        // All actions shared with UI 
        this.uiAction = {
            openTree: () => {
                this.uiVar.treeMenuVisible = !this.uiVar.treeMenuVisible;
            },
            selectedOrganizationsChange: (selectedOrganisations) => {
                if (!selectedOrganisations) {
                    this.selectedOrganisations = null;
                    return;
                }
                if (Array.isArray(selectedOrganisations)) {
                    this.selectedOrganisations = selectedOrganisations;
                }
                else {
                    this.selectedOrganisations = [selectedOrganisations];
                }
                this.privFunc.refreshOuputs();
            },
            // Remove organisation from chip
            organisationRemove: (organisationToRemove) => {
                this.selectedOrganisations = this.selectedOrganisations.filter(organisation => organisationToRemove.id !== organisation.id);
                this.privFunc.refreshOuputs();
            }
        };
    }
    onDocumentClick(event) {
        if (!this.elementRef.nativeElement.contains(event.target)) {
            this.uiVar.treeMenuVisible = false;
        }
    }
    // Angular events
    ngOnInit() {
    }
}
FofOrganizationsMultiSelectComponent.ɵfac = function FofOrganizationsMultiSelectComponent_Factory(t) { return new (t || FofOrganizationsMultiSelectComponent)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
FofOrganizationsMultiSelectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofOrganizationsMultiSelectComponent, selectors: [["fof-core-fof-organizations-multi-select"]], hostBindings: function FofOrganizationsMultiSelectComponent_HostBindings(rf, ctx) { if (rf & 1) {
        i0.ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_click_HostBindingHandler($event) { return ctx.onDocumentClick($event); }, false, i0.ɵɵresolveDocument);
    } }, inputs: { multiSelect: "multiSelect", selectedOrganisations: "selectedOrganisations", notSelectableOrganizations: "notSelectableOrganizations", placeHolder: "placeHolder" }, outputs: { selectedOrganizationsChange: "selectedOrganizationsChange" }, decls: 9, vars: 8, consts: [[1, "fof-menu-custom"], ["chipList", ""], [3, "click", "removed", 4, "ngFor", "ngForOf"], ["matInput", "", "readonly", "", 3, "placeholder", "matChipInputFor", "click"], [1, "cdk-overlay-pane"], [1, "organization-menu", "mat-menu-panel", "mat-elevation-z4"], [3, "multiSelect", "selectedNodesSynchronize", "notSelectableOrganizations", "selectedOrganizationsChange"], [3, "click", "removed"], ["matChipRemove", ""]], template: function FofOrganizationsMultiSelectComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "mat-form-field");
        i0.ɵɵelementStart(2, "mat-chip-list", null, 1);
        i0.ɵɵtemplate(4, FofOrganizationsMultiSelectComponent_mat_chip_4_Template, 4, 1, "mat-chip", 2);
        i0.ɵɵelementStart(5, "input", 3);
        i0.ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_Template_input_click_5_listener() { return ctx.uiAction.openTree(); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "div", 4);
        i0.ɵɵelementStart(7, "div", 5);
        i0.ɵɵelementStart(8, "fof-organizations-tree", 6);
        i0.ɵɵlistener("selectedOrganizationsChange", function FofOrganizationsMultiSelectComponent_Template_fof_organizations_tree_selectedOrganizationsChange_8_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        const _r8 = i0.ɵɵreference(3);
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngForOf", ctx.selectedOrganisations);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("placeholder", ctx.placeHolder)("matChipInputFor", _r8);
        i0.ɵɵadvance(1);
        i0.ɵɵclassProp("tree-display", ctx.uiVar.treeMenuVisible);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("multiSelect", ctx.multiSelect)("selectedNodesSynchronize", ctx.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
    } }, directives: [i1.MatFormField, i2.MatChipList, i3.NgForOf, i4.MatInput, i2.MatChipInput, i5.FofOrganizationsTreeComponent, i2.MatChip, i6.MatIcon, i2.MatChipRemove], styles: [".fof-menu-custom[_ngcontent-%COMP%]{position:relative}.fof-menu-custom[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]{width:100%;position:absolute;top:60px;display:none}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane.tree-display[_ngcontent-%COMP%]{display:block}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu[_ngcontent-%COMP%]{min-width:112px;overflow:auto;-webkit-overflow-scrolling:touch;border-radius:4px;height:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu.mat-menu-panel[_ngcontent-%COMP%]{max-width:100%;max-height:unset}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofOrganizationsMultiSelectComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-organizations-multi-select',
                templateUrl: './fof-organizations-multi-select.component.html',
                styleUrls: ['./fof-organizations-multi-select.component.scss'],
                host: {
                    '(document:click)': 'onDocumentClick($event)',
                }
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { multiSelect: [{
            type: Input
        }], selectedOrganisations: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }], placeHolder: [{
            type: Input
        }], selectedOrganizationsChange: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0L2ZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdC5jb21wb25lbnQudHMiLCJsaWIvY29tcG9uZW50cy9mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3QvZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0LmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXNCLE1BQU0sRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFBOzs7Ozs7Ozs7O0lDR3ZGLG1DQUdDO0lBRkEsMExBQVMsMkJBQW1CLElBQUMseU5BQ2xCLHFEQUF5QyxJQUR2QjtJQUU3QixZQUNBO0lBQUEsbUNBQXdCO0lBQUEsc0JBQU07SUFBQSxpQkFBVztJQUMxQyxpQkFBVzs7O0lBRlYsZUFDQTtJQURBLHNEQUNBOztBREpKLHdGQUF3RjtBQVV4RixNQUFNLE9BQU8sb0NBQW9DO0lBYy9DLFlBQ1UsVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQVB2QixnQkFBVyxHQUFZLElBQUksQ0FBQTtRQUMzQiwwQkFBcUIsR0FBb0IsRUFBRSxDQUFBO1FBRTNDLGdCQUFXLEdBQVcsZUFBZSxDQUFBO1FBQ3BDLGdDQUEyQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUE7UUFRMUQsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRyxFQUVoQixDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRztZQUNqQixhQUFhLEVBQUMsR0FBRyxFQUFFO2dCQUNqQixJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFBO1lBQ25FLENBQUM7U0FDRixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLGVBQWUsRUFBRSxLQUFLO1NBRXZCLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHO1lBQ2hCLFFBQVEsRUFBQyxHQUFHLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQTtZQUMxRCxDQUFDO1lBQ0QsMkJBQTJCLEVBQUUsQ0FBQyxxQkFBc0QsRUFBRSxFQUFFO2dCQUN0RixJQUFJLENBQUMscUJBQXFCLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUE7b0JBQ2pDLE9BQU07aUJBQ1A7Z0JBRUQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFFLEVBQUU7b0JBQ3pDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxxQkFBcUIsQ0FBQTtpQkFDbkQ7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLHFCQUFxQixHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQTtpQkFDckQ7Z0JBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsQ0FBQTtZQUMvQixDQUFDO1lBQ0QsZ0NBQWdDO1lBQ2hDLGtCQUFrQixFQUFDLENBQUMsb0JBQW1DLEVBQUUsRUFBRTtnQkFDekQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQzVELFlBQVksQ0FBQyxFQUFFLENBQUMsb0JBQW9CLENBQUMsRUFBRSxLQUFLLFlBQVksQ0FBQyxFQUFFLENBQUUsQ0FBQTtnQkFDL0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsQ0FBQTtZQUMvQixDQUFDO1NBQ0YsQ0FBQTtJQTFDRCxDQUFDO0lBaEJELGVBQWUsQ0FBQyxLQUFLO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3pELElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQTtTQUNuQztJQUNILENBQUM7SUF1REQsaUJBQWlCO0lBQ2pCLFFBQVE7SUFFUixDQUFDOzt3SEFoRVUsb0NBQW9DO3lFQUFwQyxvQ0FBb0M7OztRQ2JqRCw4QkFDQztRQUFBLHNDQUNDO1FBQUEsOENBQ0M7UUFBQSwrRkFHQztRQUdELGdDQUlEO1FBREUsZ0hBQVMsdUJBQW1CLElBQUM7UUFIOUIsaUJBSUQ7UUFBQSxpQkFBZ0I7UUFDakIsaUJBQWlCO1FBQ2pCLDhCQUVDO1FBQUEsOEJBQ0M7UUFBQSxpREFLMkI7UUFEMUIsbUxBQStCLGdEQUE0QyxJQUFDO1FBQzNFLGlCQUF5QjtRQUM1QixpQkFBTTtRQUNQLGlCQUFNO1FBQ1AsaUJBQU07OztRQXZCTyxlQUFrRDtRQUFsRCxtREFBa0Q7UUFNNUMsZUFBMkI7UUFBM0IsNkNBQTJCLHdCQUFBO1FBTzVDLGVBQTRDO1FBQTVDLHlEQUE0QztRQUcxQyxlQUEyQjtRQUEzQiw2Q0FBMkIsdURBQUEsOERBQUE7O2tERE5sQixvQ0FBb0M7Y0FSaEQsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSx5Q0FBeUM7Z0JBQ25ELFdBQVcsRUFBRSxpREFBaUQ7Z0JBQzlELFNBQVMsRUFBRSxDQUFDLGlEQUFpRCxDQUFDO2dCQUM5RCxJQUFJLEVBQUU7b0JBQ0osa0JBQWtCLEVBQUUseUJBQXlCO2lCQUM5QzthQUNGOztrQkFTRSxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxLQUFLOztrQkFDTCxNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEVsZW1lbnRSZWYsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IGlPcmdhbml6YXRpb24gfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vZm9mLXBlcm1pc3Npb24uc2VydmljZSdcclxuLy8gaHR0cHM6Ly9zdGFja2JsaXR6LmNvbS9lZGl0L21lbnUtb3ZlcmxheT9maWxlPXNyYyUyRmFwcCUyRnNlbGVjdC1pbnB1dC5jb21wb25lbnQuaHRtbFxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2YtY29yZS1mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3QnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdC5jb21wb25lbnQuc2NzcyddLFxyXG4gIGhvc3Q6IHtcclxuICAgICcoZG9jdW1lbnQ6Y2xpY2spJzogJ29uRG9jdW1lbnRDbGljaygkZXZlbnQpJyxcclxuICB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZPcmdhbml6YXRpb25zTXVsdGlTZWxlY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBvbkRvY3VtZW50Q2xpY2soZXZlbnQpIHtcclxuICAgIGlmICghdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuY29udGFpbnMoZXZlbnQudGFyZ2V0KSkge1xyXG4gICAgICB0aGlzLnVpVmFyLnRyZWVNZW51VmlzaWJsZSA9IGZhbHNlXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBASW5wdXQoKSBtdWx0aVNlbGVjdDogYm9vbGVhbiA9IHRydWVcclxuICBASW5wdXQoKSBzZWxlY3RlZE9yZ2FuaXNhdGlvbnM6IGlPcmdhbml6YXRpb25bXSA9IFtdXHJcbiAgQElucHV0KCkgbm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnM6IGlPcmdhbml6YXRpb25bXVxyXG4gIEBJbnB1dCgpIHBsYWNlSG9sZGVyOiBzdHJpbmcgPSAnT3JnYW5pc2F0aW9ucydcclxuICBAT3V0cHV0KCkgc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpXHJcbiAgXHJcbiAgY29uc3RydWN0b3IgKFxyXG4gICAgcHJpdmF0ZSBlbGVtZW50UmVmOiBFbGVtZW50UmVmXHJcbiAgKSB7IFxyXG4gICAgXHJcbiAgfVxyXG5cclxuICAvLyBBbGwgcHJpdmF0ZSB2YXJpYWJsZXNcclxuICBwcml2YXRlIHByaVZhciA9IHsgXHJcbiAgICBcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICAgIHJlZnJlc2hPdXB1dHM6KCkgPT4ge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZS5lbWl0KHRoaXMuc2VsZWN0ZWRPcmdhbmlzYXRpb25zKVxyXG4gICAgfVxyXG4gIH1cclxuICAvLyBBbGwgdmFyaWFibGVzIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aVZhciA9IHtcclxuICAgIHRyZWVNZW51VmlzaWJsZTogZmFsc2UsICAgIFxyXG4gICAgLy8gc2VsZWN0ZWRPcmdhbmlzYXRpb25zOiA8aU9yZ2FuaXphdGlvbltdPltdXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcclxuICAgIG9wZW5UcmVlOigpID0+IHtcclxuICAgICAgdGhpcy51aVZhci50cmVlTWVudVZpc2libGUgPSAhdGhpcy51aVZhci50cmVlTWVudVZpc2libGVcclxuICAgIH0sXHJcbiAgICBzZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2U6IChzZWxlY3RlZE9yZ2FuaXNhdGlvbnM6IGlPcmdhbml6YXRpb25bXSB8IGlPcmdhbml6YXRpb24pID0+IHsgICAgICBcclxuICAgICAgaWYgKCFzZWxlY3RlZE9yZ2FuaXNhdGlvbnMpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3JnYW5pc2F0aW9ucyA9IG51bGxcclxuICAgICAgICByZXR1cm5cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoc2VsZWN0ZWRPcmdhbmlzYXRpb25zICkpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3JnYW5pc2F0aW9ucyA9IHNlbGVjdGVkT3JnYW5pc2F0aW9ucyAgXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMgPSBbc2VsZWN0ZWRPcmdhbmlzYXRpb25zXVxyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICB0aGlzLnByaXZGdW5jLnJlZnJlc2hPdXB1dHMoKVxyXG4gICAgfSwgICBcclxuICAgIC8vIFJlbW92ZSBvcmdhbmlzYXRpb24gZnJvbSBjaGlwXHJcbiAgICBvcmdhbmlzYXRpb25SZW1vdmU6KG9yZ2FuaXNhdGlvblRvUmVtb3ZlOiBpT3JnYW5pemF0aW9uKSA9PiB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRPcmdhbmlzYXRpb25zID0gdGhpcy5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMuZmlsdGVyIChcclxuICAgICAgICBvcmdhbmlzYXRpb24gPT4gb3JnYW5pc2F0aW9uVG9SZW1vdmUuaWQgIT09IG9yZ2FuaXNhdGlvbi5pZCApXHJcbiAgICAgIHRoaXMucHJpdkZ1bmMucmVmcmVzaE91cHV0cygpXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgXHJcbiAgfVxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJmb2YtbWVudS1jdXN0b21cIj5cdFxyXG5cdDxtYXQtZm9ybS1maWVsZD5cclxuXHRcdDxtYXQtY2hpcC1saXN0ICNjaGlwTGlzdD5cdFxyXG5cdFx0XHQ8bWF0LWNoaXAgKm5nRm9yPVwibGV0IG9yZ2FuaXNhdGlvbiBvZiBzZWxlY3RlZE9yZ2FuaXNhdGlvbnNcIlx0XHRcdFx0XHJcblx0XHRcdFx0KGNsaWNrKT1cInVpQWN0aW9uLm9wZW5UcmVlKClcIlx0XHRcdFx0XHJcblx0XHRcdFx0KHJlbW92ZWQpPVwidWlBY3Rpb24ub3JnYW5pc2F0aW9uUmVtb3ZlKG9yZ2FuaXNhdGlvbilcIj5cclxuXHRcdFx0XHR7eyBvcmdhbmlzYXRpb24ubmFtZSB9fVxyXG5cdFx0XHRcdDxtYXQtaWNvbiBtYXRDaGlwUmVtb3ZlPmNhbmNlbDwvbWF0LWljb24+XHJcblx0XHRcdDwvbWF0LWNoaXA+XHRcdFx0XHJcblx0XHRcdDxpbnB1dCBtYXRJbnB1dCBbcGxhY2Vob2xkZXJdPVwicGxhY2VIb2xkZXJcIlxyXG5cdFx0XHRcdHJlYWRvbmx5XHJcblx0XHRcdFx0W21hdENoaXBJbnB1dEZvcl09XCJjaGlwTGlzdFwiXHJcblx0XHRcdFx0KGNsaWNrKT1cInVpQWN0aW9uLm9wZW5UcmVlKClcIj5cclxuXHRcdDwvbWF0LWNoaXAtbGlzdD5cclxuXHQ8L21hdC1mb3JtLWZpZWxkPlxyXG5cdDxkaXYgY2xhc3M9XCJjZGstb3ZlcmxheS1wYW5lXCJcclxuXHRcdFtjbGFzcy50cmVlLWRpc3BsYXldPVwidWlWYXIudHJlZU1lbnVWaXNpYmxlXCI+XHJcblx0XHQ8ZGl2IGNsYXNzPVwib3JnYW5pemF0aW9uLW1lbnUgbWF0LW1lbnUtcGFuZWwgbWF0LWVsZXZhdGlvbi16NFwiPlxyXG5cdFx0XHQ8Zm9mLW9yZ2FuaXphdGlvbnMtdHJlZVxyXG5cdFx0XHRcdFttdWx0aVNlbGVjdF09XCJtdWx0aVNlbGVjdFwiXHJcblx0XHRcdFx0W3NlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZV09XCJzZWxlY3RlZE9yZ2FuaXNhdGlvbnNcIlxyXG5cdFx0XHRcdFtub3RTZWxlY3RhYmxlT3JnYW5pemF0aW9uc109XCJub3RTZWxlY3RhYmxlT3JnYW5pemF0aW9uc1wiXHJcblx0XHRcdFx0KHNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZSk9XCJ1aUFjdGlvbi5zZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2UoJGV2ZW50KVwiXHJcblx0XHRcdFx0PjwvZm9mLW9yZ2FuaXphdGlvbnMtdHJlZT5cclxuXHRcdDwvZGl2PlxyXG5cdDwvZGl2PlxyXG48L2Rpdj4iXX0=
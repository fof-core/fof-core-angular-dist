import { Component, EventEmitter, Output, Input } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "@angular/material/tree";
import * as i3 from "@angular/material/button";
import * as i4 from "@angular/material/checkbox";
import * as i5 from "@angular/material/icon";
function FofOrganizationsTreeComponent_mat_tree_node_1_Template(rf, ctx) { if (rf & 1) {
    const _r4 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-tree-node", 3);
    i0.ɵɵelementStart(1, "li", 4);
    i0.ɵɵelement(2, "button", 5);
    i0.ɵɵelementStart(3, "mat-checkbox", 6);
    i0.ɵɵlistener("change", function FofOrganizationsTreeComponent_mat_tree_node_1_Template_mat_checkbox_change_3_listener($event) { i0.ɵɵrestoreView(_r4); const node_r2 = ctx.$implicit; const ctx_r3 = i0.ɵɵnextContext(); return ctx_r3.uiAction.treeViewNodeSelect(node_r2, $event); });
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const node_r2 = ctx.$implicit;
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("checked", node_r2.checked)("disabled", node_r2.notSelectable);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", node_r2.name, "");
} }
function FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template(rf, ctx) { if (rf & 1) {
    const _r7 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-nested-tree-node");
    i0.ɵɵelementStart(1, "li");
    i0.ɵɵelementStart(2, "div", 4);
    i0.ɵɵelementStart(3, "button", 7);
    i0.ɵɵelementStart(4, "mat-icon", 8);
    i0.ɵɵtext(5);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "mat-checkbox", 6);
    i0.ɵɵlistener("change", function FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template_mat_checkbox_change_6_listener($event) { i0.ɵɵrestoreView(_r7); const node_r5 = ctx.$implicit; const ctx_r6 = i0.ɵɵnextContext(); return ctx_r6.uiAction.treeViewNodeSelect(node_r5, $event); });
    i0.ɵɵtext(7);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "ul", 9);
    i0.ɵɵelementContainer(9, 10);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const node_r5 = ctx.$implicit;
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵadvance(5);
    i0.ɵɵtextInterpolate1(" ", ctx_r1.uiVar.organizations.treeControl.isExpanded(node_r5) ? "expand_more" : "chevron_right", " ");
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("checked", node_r5.checked)("disabled", node_r5.notSelectable);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", node_r5.name, "");
    i0.ɵɵadvance(1);
    i0.ɵɵclassProp("treeview-invisible", !ctx_r1.uiVar.organizations.treeControl.isExpanded(node_r5));
} }
export class FofOrganizationsTreeComponent {
    constructor(fofPermissionService) {
        this.fofPermissionService = fofPermissionService;
        /** Can the user could select multiple organizations? */
        this.multiSelect = false;
        /** Submit all the organization selected everytime an orgnization is selected or unselected
         * AN objet or array, depending from the multiSelect param
        */
        this.selectedOrganizationsChange = new EventEmitter();
        // All private variables
        this.priVar = {
            currentNode: undefined,
            selectedNodesSynchronizeSave: undefined
        };
        // All private functions
        this.privFunc = {
            selectedNodeSynchronize: () => {
                const tree = this.uiVar.organizations.dataSource.data;
                const selectedNodesSynchronize = this.selectedNodesSynchronize;
                let areSomeNodeNotSelectable = false;
                if (this.notSelectableOrganizations && this.notSelectableOrganizations.length > 0) {
                    areSomeNodeNotSelectable = true;
                }
                const getSelectedNode = (node) => {
                    if (selectedNodesSynchronize) {
                        const foundedNode = selectedNodesSynchronize.filter(n => {
                            if (n.id === node.id) {
                                // ugly fix: toDo: to review
                                if (!n.name) {
                                    n.name = node.name;
                                }
                                return node;
                            }
                        });
                        if (foundedNode.length > 0) {
                            node.checked = true;
                        }
                        else {
                            node.checked = false;
                        }
                        // if (selectedNodesSynchronize && selectedNodesSynchronize.filter(n => n.id === node.id).length > 0) {
                        //   node.checked = true          
                        // } else {
                        //   node.checked = false
                        // }
                    }
                    if (areSomeNodeNotSelectable) {
                        if (this.notSelectableOrganizations.filter(n => n.id === node.id).length > 0) {
                            // would prevent the user to check again the same organization 
                            if (this.priVar.selectedNodesSynchronizeSave.filter(n => n.id === node.id).length === 0) {
                                node.notSelectable = true;
                            }
                        }
                        else {
                            node.notSelectable = false;
                        }
                    }
                    node.children.forEach(child => getSelectedNode(child));
                };
                if (tree && tree.length > 0) {
                    getSelectedNode(tree[0]);
                }
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            loading: true,
            organizations: {
                treeControl: new NestedTreeControl(node => node.children),
                dataSource: new MatTreeNestedDataSource()
            },
        };
        // All actions shared with UI 
        this.uiAction = {
            OrganizationNodeHasChild: (_, node) => !!node.children && node.children.length > 0,
            organizationTreeLoad: () => {
                this.uiVar.loading = true;
                this.fofPermissionService.organization.getTreeView()
                    .toPromise()
                    .then((organizations) => {
                    this.uiVar.organizations.dataSource.data = organizations;
                    if (organizations.length > 0) {
                        this.uiVar.organizations.treeControl.expand(organizations[0]);
                        this.privFunc.selectedNodeSynchronize();
                    }
                })
                    .finally(() => {
                    this.uiVar.loading = false;
                });
            },
            treeViewNodeSelect: (node, $event) => {
                const tree = this.uiVar.organizations.dataSource.data;
                if (this.multiSelect) {
                    const nodes = [];
                    node.checked = $event.checked;
                    const getSelectedNode = (node) => {
                        if (node.checked) {
                            const selectNode = Object.assign({}, node);
                            selectNode.children = null;
                            nodes.push(selectNode);
                        }
                        node.children.forEach(child => getSelectedNode(child));
                    };
                    if (tree && tree.length > 0) {
                        getSelectedNode(tree[0]);
                    }
                    this.selectedOrganizationsChange.emit(nodes);
                }
                else {
                    const nodeUnSelect = (node) => {
                        node.indeterminate = false;
                        node.checked = false;
                        if (!node.children) {
                            node.children = [];
                        }
                        node.children.forEach(child => nodeUnSelect(child));
                    };
                    if (tree && tree.length > 0) {
                        nodeUnSelect(tree[0]);
                    }
                    node.checked = $event.checked;
                    if (node.checked) {
                        this.priVar.currentNode = node;
                    }
                    else {
                        this.priVar.currentNode = null;
                    }
                    this.selectedOrganizationsChange.emit(this.priVar.currentNode);
                }
            }
        };
        this.uiAction.organizationTreeLoad();
    }
    // Angular events
    ngOnInit() {
    }
    ngOnChanges() {
        if (this.nodeToDelete) {
            const spliceNode = (node) => {
                if (node.id === this.nodeToDelete.id)
                    return null;
                node.children.forEach((child, index) => {
                    if (child.id === this.nodeToDelete.id) {
                        node.children.splice(index, 1);
                        return;
                    }
                    spliceNode(child);
                });
            };
            const data = this.uiVar.organizations.dataSource.data.slice();
            spliceNode(data[0]);
            // toDO: Performance   
            // bug on tree refresh
            // https://github.com/angular/components/issues/11381   
            this.uiVar.organizations.dataSource.data = null;
            this.uiVar.organizations.dataSource.data = data;
            this.nodeToDelete = null;
            return;
        }
        if (this.nodeChanged) {
            this.priVar.currentNode = this.nodeChanged;
            // toDO: Performance   
            // bug on tree refresh
            // https://github.com/angular/components/issues/11381        
            const data = this.uiVar.organizations.dataSource.data.slice();
            this.uiVar.organizations.dataSource.data = null;
            this.uiVar.organizations.dataSource.data = data;
            this.nodeChanged = null;
        }
        if (this.selectedNodesSynchronize || this.notSelectableOrganizations) {
            if (!this.priVar.selectedNodesSynchronizeSave) {
                this.priVar.selectedNodesSynchronizeSave = this.selectedNodesSynchronize.slice();
            }
            this.privFunc.selectedNodeSynchronize();
        }
    }
}
FofOrganizationsTreeComponent.ɵfac = function FofOrganizationsTreeComponent_Factory(t) { return new (t || FofOrganizationsTreeComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService)); };
FofOrganizationsTreeComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofOrganizationsTreeComponent, selectors: [["fof-organizations-tree"]], inputs: { nodeChanged: "nodeChanged", nodeToDelete: "nodeToDelete", multiSelect: "multiSelect", selectedNodesSynchronize: "selectedNodesSynchronize", notSelectableOrganizations: "notSelectableOrganizations" }, outputs: { selectedOrganizationsChange: "selectedOrganizationsChange" }, features: [i0.ɵɵNgOnChangesFeature], decls: 3, vars: 3, consts: [[1, "treeview", 3, "dataSource", "treeControl"], ["matTreeNodeToggle", "", 4, "matTreeNodeDef"], [4, "matTreeNodeDef", "matTreeNodeDefWhen"], ["matTreeNodeToggle", ""], [1, "mat-tree-node"], ["mat-icon-button", "", "disabled", ""], [3, "checked", "disabled", "change"], ["mat-icon-button", "", "matTreeNodeToggle", "", 1, "fof-test-btn"], [1, "mat-icon-rtl-mirror"], [1, ""], ["matTreeNodeOutlet", ""]], template: function FofOrganizationsTreeComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-tree", 0);
        i0.ɵɵtemplate(1, FofOrganizationsTreeComponent_mat_tree_node_1_Template, 5, 3, "mat-tree-node", 1);
        i0.ɵɵtemplate(2, FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template, 10, 6, "mat-nested-tree-node", 2);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵproperty("dataSource", ctx.uiVar.organizations.dataSource)("treeControl", ctx.uiVar.organizations.treeControl);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("matTreeNodeDefWhen", ctx.uiAction.OrganizationNodeHasChild);
    } }, directives: [i2.MatTree, i2.MatTreeNodeDef, i2.MatTreeNode, i2.MatTreeNodeToggle, i3.MatButton, i4.MatCheckbox, i2.MatNestedTreeNode, i5.MatIcon, i2.MatTreeNodeOutlet], styles: [".mat-tree[_ngcontent-%COMP%]{height:100%;margin-bottom:15px}.mat-tree[_ngcontent-%COMP%]   .mat-tree-node[_ngcontent-%COMP%]{margin-right:15px}.treeview-invisible[_ngcontent-%COMP%]{display:none}.treeview[_ngcontent-%COMP%]   li[_ngcontent-%COMP%], .treeview[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{margin-top:0;margin-bottom:0;list-style-type:none}.treeview[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{padding-left:25px}.mat-tree-node[_ngcontent-%COMP%]{min-height:25px}.mat-form-field[_ngcontent-%COMP%]{width:100%}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofOrganizationsTreeComponent, [{
        type: Component,
        args: [{
                selector: 'fof-organizations-tree',
                templateUrl: './fof-organizations-tree.component.html',
                styleUrls: ['./fof-organizations-tree.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }]; }, { nodeChanged: [{
            type: Input
        }], nodeToDelete: [{
            type: Input
        }], multiSelect: [{
            type: Input
        }], selectedNodesSynchronize: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }], selectedOrganizationsChange: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLW9yZ2FuaXphdGlvbnMtdHJlZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2ZvZi1vcmdhbml6YXRpb25zLXRyZWUvZm9mLW9yZ2FuaXphdGlvbnMtdHJlZS5jb21wb25lbnQudHMiLCJsaWIvY29tcG9uZW50cy9mb2Ytb3JnYW5pemF0aW9ucy10cmVlL2ZvZi1vcmdhbml6YXRpb25zLXRyZWUuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBcUIsTUFBTSxlQUFlLENBQUE7QUFDekYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUJBQW1CLENBQUE7QUFDckQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sd0JBQXdCLENBQUE7Ozs7Ozs7OztJQ0E5RCx3Q0FDRTtJQUFBLDZCQUNFO0lBQ0EsNEJBQTBDO0lBQzFDLHVDQUlFO0lBREEsaU9BQVUsbURBQXlDLElBQUM7SUFDcEQsWUFBYTtJQUFBLGlCQUFlO0lBQ2hDLGlCQUFLO0lBQ1AsaUJBQWdCOzs7SUFMVixlQUF3QjtJQUF4Qix5Q0FBd0IsbUNBQUE7SUFHeEIsZUFBYTtJQUFiLDRDQUFhOzs7O0lBSW5CLDRDQUNFO0lBQUEsMEJBQ0U7SUFBQSw4QkFDRTtJQUFBLGlDQUVFO0lBQUEsbUNBQ0U7SUFBQSxZQUNGO0lBQUEsaUJBQVc7SUFDYixpQkFBUztJQUNULHVDQUlFO0lBREEsd09BQVUsbURBQXlDLElBQUM7SUFDcEQsWUFBYTtJQUFBLGlCQUFlO0lBQ2hDLGlCQUFNO0lBQ04sNkJBQ0U7SUFBQSw0QkFBK0M7SUFDakQsaUJBQUs7SUFDUCxpQkFBSztJQUNQLGlCQUF1Qjs7OztJQWJiLGVBQ0Y7SUFERSw2SEFDRjtJQUdBLGVBQXdCO0lBQXhCLHlDQUF3QixtQ0FBQTtJQUd4QixlQUFhO0lBQWIsNENBQWE7SUFFSixlQUFtRjtJQUFuRixpR0FBbUY7O0FEWHRHLE1BQU0sT0FBTyw2QkFBNkI7SUFnQnhDLFlBQ1Usb0JBQTBDO1FBQTFDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFacEQsd0RBQXdEO1FBQy9DLGdCQUFXLEdBQVksS0FBSyxDQUFBO1FBS3JDOztVQUVFO1FBQ1EsZ0NBQTJCLEdBQW9DLElBQUksWUFBWSxFQUFFLENBQUE7UUFRM0Ysd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRztZQUNmLFdBQVcsRUFBaUIsU0FBUztZQUNyQyw0QkFBNEIsRUFBWSxTQUFTO1NBQ2xELENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHO1lBQ2pCLHVCQUF1QixFQUFFLEdBQUcsRUFBRTtnQkFDNUIsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQTtnQkFDckQsTUFBTSx3QkFBd0IsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUE7Z0JBQzlELElBQUksd0JBQXdCLEdBQUcsS0FBSyxDQUFBO2dCQUVwQyxJQUFJLElBQUksQ0FBQywwQkFBMEIsSUFBSSxJQUFJLENBQUMsMEJBQTBCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDakYsd0JBQXdCLEdBQUcsSUFBSSxDQUFBO2lCQUNoQztnQkFFRCxNQUFNLGVBQWUsR0FBRyxDQUFDLElBQVksRUFBRSxFQUFFO29CQUV2QyxJQUFJLHdCQUF3QixFQUFFO3dCQUU1QixNQUFNLFdBQVcsR0FBSSx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7NEJBQ3ZELElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFO2dDQUNwQiw0QkFBNEI7Z0NBQzVCLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFO29DQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQTtpQ0FBQztnQ0FDakMsT0FBTyxJQUFJLENBQUE7NkJBQ1o7d0JBQ0gsQ0FBQyxDQUFDLENBQUE7d0JBRUYsSUFBSSxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUE7eUJBQ3BCOzZCQUFNOzRCQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO3lCQUNyQjt3QkFFRCx1R0FBdUc7d0JBQ3ZHLGtDQUFrQzt3QkFDbEMsV0FBVzt3QkFDWCx5QkFBeUI7d0JBQ3pCLElBQUk7cUJBQ0w7b0JBRUQsSUFBSSx3QkFBd0IsRUFBRTt3QkFDNUIsSUFBSSxJQUFJLENBQUMsMEJBQTBCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs0QkFDNUUsK0RBQStEOzRCQUMvRCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsNEJBQTRCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQ0FDdkYsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUE7NkJBQzFCO3lCQUNGOzZCQUFNOzRCQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFBO3lCQUMzQjtxQkFDRjtvQkFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO2dCQUN4RCxDQUFDLENBQUE7Z0JBRUQsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQzNCLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtpQkFDekI7WUFDSCxDQUFDO1NBQ0YsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixPQUFPLEVBQUUsSUFBSTtZQUNiLGFBQWEsRUFBRTtnQkFDYixXQUFXLEVBQUUsSUFBSSxpQkFBaUIsQ0FBUyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ2pFLFVBQVUsRUFBRSxJQUFJLHVCQUF1QixFQUFVO2FBQ2xEO1NBQ0YsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsd0JBQXdCLEVBQUUsQ0FBQyxDQUFTLEVBQUUsSUFBWSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDO1lBQ2xHLG9CQUFvQixFQUFDLEdBQUcsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO2dCQUN6QixJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTtxQkFDbkQsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxDQUFDLGFBQXVCLEVBQUUsRUFBRTtvQkFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxhQUFhLENBQUE7b0JBQ3hELElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7d0JBQzdELElBQUksQ0FBQyxRQUFRLENBQUMsdUJBQXVCLEVBQUUsQ0FBQTtxQkFDeEM7Z0JBQ0gsQ0FBQyxDQUFDO3FCQUNELE9BQU8sQ0FBQyxHQUFHLEVBQUU7b0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO2dCQUM1QixDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUM7WUFDRCxrQkFBa0IsRUFBQyxDQUFDLElBQVksRUFBRSxNQUFXLEVBQUUsRUFBRTtnQkFDL0MsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQTtnQkFFckQsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNwQixNQUFNLEtBQUssR0FBb0IsRUFBRSxDQUFBO29CQUVqQyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUE7b0JBRTdCLE1BQU0sZUFBZSxHQUFHLENBQUMsSUFBWSxFQUFFLEVBQUU7d0JBQ3ZDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs0QkFDaEIsTUFBTSxVQUFVLHFCQUFPLElBQUksQ0FBQyxDQUFBOzRCQUM1QixVQUFVLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTs0QkFDMUIsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQTt5QkFDdkI7d0JBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtvQkFDeEQsQ0FBQyxDQUFBO29CQUVELElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMzQixlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7cUJBQ3pCO29CQUVELElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7aUJBRTdDO3FCQUFNO29CQUNMLE1BQU0sWUFBWSxHQUFHLENBQUMsSUFBWSxFQUFFLEVBQUU7d0JBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFBO3dCQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQTt3QkFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7NEJBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUE7eUJBQUU7d0JBQzFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7b0JBQ3JELENBQUMsQ0FBQTtvQkFFRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDM0IsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBO3FCQUN0QjtvQkFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUE7b0JBRTdCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTt3QkFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFBO3FCQUMvQjt5QkFBTTt3QkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUE7cUJBQy9CO29CQUVELElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTtpQkFDL0Q7WUFDSCxDQUFDO1NBQ0YsQ0FBQTtRQXZJQyxJQUFJLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUFFLENBQUE7SUFDdEMsQ0FBQztJQXVJRCxpQkFBaUI7SUFDakIsUUFBUTtJQUVSLENBQUM7SUFDRCxXQUFXO1FBRVQsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3JCLE1BQU0sVUFBVSxHQUFHLENBQUMsSUFBWSxFQUFFLEVBQUU7Z0JBQ2xDLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQUUsT0FBTyxJQUFJLENBQUE7Z0JBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNyQyxJQUFJLEtBQUssQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUU7d0JBQ3JDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQTt3QkFDOUIsT0FBTTtxQkFDUDtvQkFDRCxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUE7Z0JBQ25CLENBQUMsQ0FBQyxDQUFBO1lBQ0osQ0FBQyxDQUFBO1lBRUQsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtZQUM3RCxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDbkIsdUJBQXVCO1lBQ3ZCLHNCQUFzQjtZQUN0Qix3REFBd0Q7WUFDeEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7WUFDL0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7WUFDL0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUE7WUFFeEIsT0FBTTtTQUNQO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUE7WUFDMUMsdUJBQXVCO1lBQ3ZCLHNCQUFzQjtZQUN0Qiw2REFBNkQ7WUFDN0QsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtZQUM3RCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQTtZQUMvQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQTtZQUMvQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtTQUN4QjtRQUVELElBQUksSUFBSSxDQUFDLHdCQUF3QixJQUFJLElBQUksQ0FBQywwQkFBMEIsRUFBRTtZQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyw0QkFBNEIsRUFBRTtnQkFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxFQUFFLENBQUE7YUFDakY7WUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLHVCQUF1QixFQUFFLENBQUE7U0FDeEM7SUFDSCxDQUFDOzswR0ExTVUsNkJBQTZCO2tFQUE3Qiw2QkFBNkI7UUNsQjFDLG1DQUVFO1FBQUEsa0dBQ0U7UUFXRixpSEFDRTtRQW1CSixpQkFBVzs7UUFsQ0QsK0RBQWtELG9EQUFBO1FBY3BDLGVBQW1FO1FBQW5FLDBFQUFtRTs7a0RESTlFLDZCQUE2QjtjQUx6QyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsV0FBVyxFQUFFLHlDQUF5QztnQkFDdEQsU0FBUyxFQUFFLENBQUMseUNBQXlDLENBQUM7YUFDdkQ7O2tCQUdFLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUlMLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgT3V0cHV0LCBJbnB1dCwgT25Jbml0LCBPbkNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBOZXN0ZWRUcmVlQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay90cmVlJ1xyXG5pbXBvcnQgeyBNYXRUcmVlTmVzdGVkRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RyZWUnXHJcbmltcG9ydCB7IGlPcmdhbml6YXRpb24gfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vZm9mLXBlcm1pc3Npb24uc2VydmljZSdcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgaU9yZ1VJIGV4dGVuZHMgaU9yZ2FuaXphdGlvbiB7XHJcbiAgY2hlY2tlZD86IGJvb2xlYW4sXHJcbiAgaW5kZXRlcm1pbmF0ZT86IGJvb2xlYW4sXHJcbiAgbm90U2VsZWN0YWJsZT86IGJvb2xlYW4sXHJcbiAgY2hpbGRyZW4/OiBpT3JnVUlbXVxyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1vcmdhbml6YXRpb25zLXRyZWUnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mb2Ytb3JnYW5pemF0aW9ucy10cmVlLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9mb2Ytb3JnYW5pemF0aW9ucy10cmVlLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZk9yZ2FuaXphdGlvbnNUcmVlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAvKiogQ2hhbmdlIHRoZSBjdXJyZW50IG9yZ2FuaXphdGlvbnMgKi9cclxuICBASW5wdXQoKSBub2RlQ2hhbmdlZDogaU9yZ1VJXHJcbiAgLyoqIE5vdGlmaXkgdGhlIHRyZWUgdG8gZGVsZXRlIGFuIG9yZ2FuaXphdGlvbnMgKi9cclxuICBASW5wdXQoKSBub2RlVG9EZWxldGU6IGlPcmdVSSBcclxuICAvKiogQ2FuIHRoZSB1c2VyIGNvdWxkIHNlbGVjdCBtdWx0aXBsZSBvcmdhbml6YXRpb25zPyAqLyBcclxuICBASW5wdXQoKSBtdWx0aVNlbGVjdDogYm9vbGVhbiA9IGZhbHNlICBcclxuICAvKiogT3JnYW5pemF0aW9uIHdlIHdhbnQgdG8gYmUgYWxyZWFkeSBjaGVja2VkICovXHJcbiAgQElucHV0KCkgc2VsZWN0ZWROb2Rlc1N5bmNocm9uaXplOiBpT3JnVUlbXVxyXG4gIC8qKiBQcmV2ZW50IHRvIGNoZWNrIHNvbWUgb3JnYW5pemF0aW9ucyAqL1xyXG4gIEBJbnB1dCgpIG5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zOiBpT3JnVUlbXVxyXG4gIC8qKiBTdWJtaXQgYWxsIHRoZSBvcmdhbml6YXRpb24gc2VsZWN0ZWQgZXZlcnl0aW1lIGFuIG9yZ25pemF0aW9uIGlzIHNlbGVjdGVkIG9yIHVuc2VsZWN0ZWQgXHJcbiAgICogQU4gb2JqZXQgb3IgYXJyYXksIGRlcGVuZGluZyBmcm9tIHRoZSBtdWx0aVNlbGVjdCBwYXJhbVxyXG4gICovXHJcbiAgQE91dHB1dCgpIHNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZTogRXZlbnRFbWl0dGVyPGlPcmdVSSB8IGlPcmdVSVtdPiA9IG5ldyBFdmVudEVtaXR0ZXIoKVxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9mUGVybWlzc2lvblNlcnZpY2U6IEZvZlBlcm1pc3Npb25TZXJ2aWNlLFxyXG4gICkgeyAgICAgXHJcbiAgICB0aGlzLnVpQWN0aW9uLm9yZ2FuaXphdGlvblRyZWVMb2FkKClcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgY3VycmVudE5vZGU6IDxpT3JnYW5pemF0aW9uPnVuZGVmaW5lZCxcclxuICAgIHNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZVNhdmU6IDxpT3JnVUlbXT51bmRlZmluZWRcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHsgICAgXHJcbiAgICBzZWxlY3RlZE5vZGVTeW5jaHJvbml6ZTogKCkgPT4ge1xyXG4gICAgICBjb25zdCB0cmVlID0gdGhpcy51aVZhci5vcmdhbml6YXRpb25zLmRhdGFTb3VyY2UuZGF0YVxyXG4gICAgICBjb25zdCBzZWxlY3RlZE5vZGVzU3luY2hyb25pemUgPSB0aGlzLnNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZVxyXG4gICAgICBsZXQgYXJlU29tZU5vZGVOb3RTZWxlY3RhYmxlID0gZmFsc2VcclxuXHJcbiAgICAgIGlmICh0aGlzLm5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zICYmIHRoaXMubm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIGFyZVNvbWVOb2RlTm90U2VsZWN0YWJsZSA9IHRydWVcclxuICAgICAgfVxyXG5cclxuICAgICAgY29uc3QgZ2V0U2VsZWN0ZWROb2RlID0gKG5vZGU6IGlPcmdVSSkgPT4ge1xyXG5cclxuICAgICAgICBpZiAoc2VsZWN0ZWROb2Rlc1N5bmNocm9uaXplKSB7XHJcbiAgICAgICAgICBcclxuICAgICAgICAgIGNvbnN0IGZvdW5kZWROb2RlID0gIHNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZS5maWx0ZXIobiA9PiB7XHJcbiAgICAgICAgICAgIGlmIChuLmlkID09PSBub2RlLmlkKSB7XHJcbiAgICAgICAgICAgICAgLy8gdWdseSBmaXg6IHRvRG86IHRvIHJldmlld1xyXG4gICAgICAgICAgICAgIGlmICghbi5uYW1lKSB7bi5uYW1lID0gbm9kZS5uYW1lfVxyXG4gICAgICAgICAgICAgIHJldHVybiBub2RlXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgaWYgKGZvdW5kZWROb2RlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgbm9kZS5jaGVja2VkID0gdHJ1ZSAgICAgICAgICBcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG5vZGUuY2hlY2tlZCA9IGZhbHNlXHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLy8gaWYgKHNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZSAmJiBzZWxlY3RlZE5vZGVzU3luY2hyb25pemUuZmlsdGVyKG4gPT4gbi5pZCA9PT0gbm9kZS5pZCkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgLy8gICBub2RlLmNoZWNrZWQgPSB0cnVlICAgICAgICAgIFxyXG4gICAgICAgICAgLy8gfSBlbHNlIHtcclxuICAgICAgICAgIC8vICAgbm9kZS5jaGVja2VkID0gZmFsc2VcclxuICAgICAgICAgIC8vIH1cclxuICAgICAgICB9ICAgICAgICBcclxuXHJcbiAgICAgICAgaWYgKGFyZVNvbWVOb2RlTm90U2VsZWN0YWJsZSkge1xyXG4gICAgICAgICAgaWYgKHRoaXMubm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnMuZmlsdGVyKG4gPT4gbi5pZCA9PT0gbm9kZS5pZCkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAvLyB3b3VsZCBwcmV2ZW50IHRoZSB1c2VyIHRvIGNoZWNrIGFnYWluIHRoZSBzYW1lIG9yZ2FuaXphdGlvbiBcclxuICAgICAgICAgICAgaWYgKHRoaXMucHJpVmFyLnNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZVNhdmUuZmlsdGVyKG4gPT4gbi5pZCA9PT0gbm9kZS5pZCkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgbm9kZS5ub3RTZWxlY3RhYmxlID0gdHJ1ZVxyXG4gICAgICAgICAgICB9IFxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbm9kZS5ub3RTZWxlY3RhYmxlID0gZmFsc2VcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIG5vZGUuY2hpbGRyZW4uZm9yRWFjaChjaGlsZCA9PiBnZXRTZWxlY3RlZE5vZGUoY2hpbGQpKSAgICAgICAgICAgICAgICBcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRyZWUgJiYgdHJlZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgZ2V0U2VsZWN0ZWROb2RlKHRyZWVbMF0pXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlWYXIgPSB7XHJcbiAgICBsb2FkaW5nOiB0cnVlLCAgIFxyXG4gICAgb3JnYW5pemF0aW9uczogeyAgICAgXHJcbiAgICAgIHRyZWVDb250cm9sOiBuZXcgTmVzdGVkVHJlZUNvbnRyb2w8aU9yZ1VJPihub2RlID0+IG5vZGUuY2hpbGRyZW4pLFxyXG4gICAgICBkYXRhU291cmNlOiBuZXcgTWF0VHJlZU5lc3RlZERhdGFTb3VyY2U8aU9yZ1VJPigpXHJcbiAgICB9LCAgIFxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgICBPcmdhbml6YXRpb25Ob2RlSGFzQ2hpbGQ6IChfOiBudW1iZXIsIG5vZGU6IGlPcmdVSSkgPT4gISFub2RlLmNoaWxkcmVuICYmIG5vZGUuY2hpbGRyZW4ubGVuZ3RoID4gMCwgICAgXHJcbiAgICBvcmdhbml6YXRpb25UcmVlTG9hZDooKSA9PiB7XHJcbiAgICAgIHRoaXMudWlWYXIubG9hZGluZyA9IHRydWVcclxuICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS5vcmdhbml6YXRpb24uZ2V0VHJlZVZpZXcoKVxyXG4gICAgICAudG9Qcm9taXNlKClcclxuICAgICAgLnRoZW4oKG9yZ2FuaXphdGlvbnM6IGlPcmdVSVtdKSA9PiB7ICAgICAgICBcclxuICAgICAgICB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMuZGF0YVNvdXJjZS5kYXRhID0gb3JnYW5pemF0aW9ucyAgICAgICAgXHJcbiAgICAgICAgaWYgKG9yZ2FuaXphdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5vcmdhbml6YXRpb25zLnRyZWVDb250cm9sLmV4cGFuZChvcmdhbml6YXRpb25zWzBdKSAgICAgXHJcbiAgICAgICAgICB0aGlzLnByaXZGdW5jLnNlbGVjdGVkTm9kZVN5bmNocm9uaXplKClcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgIC5maW5hbGx5KCgpID0+IHtcclxuICAgICAgICB0aGlzLnVpVmFyLmxvYWRpbmcgPSBmYWxzZVxyXG4gICAgICB9KSAgXHJcbiAgICB9LFxyXG4gICAgdHJlZVZpZXdOb2RlU2VsZWN0Oihub2RlOiBpT3JnVUksICRldmVudDogYW55KSA9PiB7XHJcbiAgICAgIGNvbnN0IHRyZWUgPSB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMuZGF0YVNvdXJjZS5kYXRhXHJcbiAgICAgIFxyXG4gICAgICBpZiAodGhpcy5tdWx0aVNlbGVjdCkge1xyXG4gICAgICAgIGNvbnN0IG5vZGVzOiBpT3JnYW5pemF0aW9uW10gPSBbXVxyXG5cclxuICAgICAgICBub2RlLmNoZWNrZWQgPSAkZXZlbnQuY2hlY2tlZFxyXG5cclxuICAgICAgICBjb25zdCBnZXRTZWxlY3RlZE5vZGUgPSAobm9kZTogaU9yZ1VJKSA9PiB7XHJcbiAgICAgICAgICBpZiAobm9kZS5jaGVja2VkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNlbGVjdE5vZGUgPSB7Li4ubm9kZX1cclxuICAgICAgICAgICAgc2VsZWN0Tm9kZS5jaGlsZHJlbiA9IG51bGxcclxuICAgICAgICAgICAgbm9kZXMucHVzaChzZWxlY3ROb2RlKVxyXG4gICAgICAgICAgfSAgICAgICAgICBcclxuICAgICAgICAgIG5vZGUuY2hpbGRyZW4uZm9yRWFjaChjaGlsZCA9PiBnZXRTZWxlY3RlZE5vZGUoY2hpbGQpKSAgICAgICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgXHJcbiAgICAgICAgaWYgKHRyZWUgJiYgdHJlZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICBnZXRTZWxlY3RlZE5vZGUodHJlZVswXSlcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlLmVtaXQobm9kZXMpXHJcblxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IG5vZGVVblNlbGVjdCA9IChub2RlOiBpT3JnVUkpID0+IHtcclxuICAgICAgICAgIG5vZGUuaW5kZXRlcm1pbmF0ZSA9IGZhbHNlXHJcbiAgICAgICAgICBub2RlLmNoZWNrZWQgPSBmYWxzZVxyXG4gICAgICAgICAgaWYgKCFub2RlLmNoaWxkcmVuKSB7IG5vZGUuY2hpbGRyZW4gPSBbXSB9XHJcbiAgICAgICAgICBub2RlLmNoaWxkcmVuLmZvckVhY2goY2hpbGQgPT4gbm9kZVVuU2VsZWN0KGNoaWxkKSkgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIGlmICh0cmVlICYmIHRyZWUubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgbm9kZVVuU2VsZWN0KHRyZWVbMF0pXHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIG5vZGUuY2hlY2tlZCA9ICRldmVudC5jaGVja2VkXHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYgKG5vZGUuY2hlY2tlZCkge1xyXG4gICAgICAgICAgdGhpcy5wcmlWYXIuY3VycmVudE5vZGUgPSBub2RlXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMucHJpVmFyLmN1cnJlbnROb2RlID0gbnVsbFxyXG4gICAgICAgIH1cclxuICBcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZS5lbWl0KHRoaXMucHJpVmFyLmN1cnJlbnROb2RlKVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICBcclxuICB9ICBcclxuICBuZ09uQ2hhbmdlcygpIHtcclxuXHJcbiAgICBpZiAodGhpcy5ub2RlVG9EZWxldGUpIHsgICAgICAgIFxyXG4gICAgICBjb25zdCBzcGxpY2VOb2RlID0gKG5vZGU6IGlPcmdVSSkgPT4geyAgICAgICAgXHJcbiAgICAgICAgaWYgKG5vZGUuaWQgPT09IHRoaXMubm9kZVRvRGVsZXRlLmlkKSByZXR1cm4gbnVsbCAgICAgICBcclxuICAgICAgICBub2RlLmNoaWxkcmVuLmZvckVhY2goKGNoaWxkLCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgaWYgKGNoaWxkLmlkID09PSB0aGlzLm5vZGVUb0RlbGV0ZS5pZCkgeyBcclxuICAgICAgICAgICAgbm9kZS5jaGlsZHJlbi5zcGxpY2UoaW5kZXgsIDEpIFxyXG4gICAgICAgICAgICByZXR1cm5cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHNwbGljZU5vZGUoY2hpbGQpXHJcbiAgICAgICAgfSkgICAgICAgICAgICAgICAgXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGNvbnN0IGRhdGEgPSB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMuZGF0YVNvdXJjZS5kYXRhLnNsaWNlKCkgICAgICBcclxuICAgICAgc3BsaWNlTm9kZShkYXRhWzBdKVxyXG4gICAgICAvLyB0b0RPOiBQZXJmb3JtYW5jZSAgIFxyXG4gICAgICAvLyBidWcgb24gdHJlZSByZWZyZXNoXHJcbiAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2NvbXBvbmVudHMvaXNzdWVzLzExMzgxICAgXHJcbiAgICAgIHRoaXMudWlWYXIub3JnYW5pemF0aW9ucy5kYXRhU291cmNlLmRhdGEgPSBudWxsXHJcbiAgICAgIHRoaXMudWlWYXIub3JnYW5pemF0aW9ucy5kYXRhU291cmNlLmRhdGEgPSBkYXRhXHJcbiAgICAgIHRoaXMubm9kZVRvRGVsZXRlID0gbnVsbFxyXG5cclxuICAgICAgcmV0dXJuXHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMubm9kZUNoYW5nZWQpIHsgXHJcbiAgICAgIHRoaXMucHJpVmFyLmN1cnJlbnROb2RlID0gdGhpcy5ub2RlQ2hhbmdlZFxyXG4gICAgICAvLyB0b0RPOiBQZXJmb3JtYW5jZSAgIFxyXG4gICAgICAvLyBidWcgb24gdHJlZSByZWZyZXNoXHJcbiAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyL2NvbXBvbmVudHMvaXNzdWVzLzExMzgxICAgICAgICBcclxuICAgICAgY29uc3QgZGF0YSA9IHRoaXMudWlWYXIub3JnYW5pemF0aW9ucy5kYXRhU291cmNlLmRhdGEuc2xpY2UoKVxyXG4gICAgICB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMuZGF0YVNvdXJjZS5kYXRhID0gbnVsbFxyXG4gICAgICB0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMuZGF0YVNvdXJjZS5kYXRhID0gZGF0YVxyXG4gICAgICB0aGlzLm5vZGVDaGFuZ2VkID0gbnVsbFxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZSB8fCB0aGlzLm5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zKSB7XHJcbiAgICAgIGlmICghdGhpcy5wcmlWYXIuc2VsZWN0ZWROb2Rlc1N5bmNocm9uaXplU2F2ZSkge1xyXG4gICAgICAgIHRoaXMucHJpVmFyLnNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZVNhdmUgPSB0aGlzLnNlbGVjdGVkTm9kZXNTeW5jaHJvbml6ZS5zbGljZSgpXHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5wcml2RnVuYy5zZWxlY3RlZE5vZGVTeW5jaHJvbml6ZSgpXHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiIsIjxtYXQtdHJlZSBbZGF0YVNvdXJjZV09XCJ0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMuZGF0YVNvdXJjZVwiIFxyXG4gIFt0cmVlQ29udHJvbF09XCJ0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMudHJlZUNvbnRyb2xcIiBjbGFzcz1cInRyZWV2aWV3XCI+XHJcbiAgPG1hdC10cmVlLW5vZGUgKm1hdFRyZWVOb2RlRGVmPVwibGV0IG5vZGVcIiBtYXRUcmVlTm9kZVRvZ2dsZT5cclxuICAgIDxsaSBjbGFzcz1cIm1hdC10cmVlLW5vZGVcIj5cclxuICAgICAgPCEtLSB1c2UgYSBkaXNhYmxlZCBidXR0b24gdG8gcHJvdmlkZSBwYWRkaW5nIGZvciB0cmVlIGxlYWYgLS0+XHJcbiAgICAgIDxidXR0b24gbWF0LWljb24tYnV0dG9uIGRpc2FibGVkPjwvYnV0dG9uPlxyXG4gICAgICA8bWF0LWNoZWNrYm94IFxyXG4gICAgICAgIFtjaGVja2VkXT1cIm5vZGUuY2hlY2tlZFwiICAgICAgICBcclxuICAgICAgICBbZGlzYWJsZWRdPVwibm9kZS5ub3RTZWxlY3RhYmxlXCJcclxuICAgICAgICAoY2hhbmdlKT1cInVpQWN0aW9uLnRyZWVWaWV3Tm9kZVNlbGVjdChub2RlLCAkZXZlbnQpXCI+XHJcbiAgICAgICAge3tub2RlLm5hbWV9fTwvbWF0LWNoZWNrYm94PlxyXG4gICAgPC9saT5cclxuICA8L21hdC10cmVlLW5vZGU+XHJcbiAgPCEtLSBUaGlzIGlzIHRoZSB0cmVlIG5vZGUgdGVtcGxhdGUgZm9yIGV4cGFuZGFibGUgbm9kZXMgLS0+XHJcbiAgPG1hdC1uZXN0ZWQtdHJlZS1ub2RlICptYXRUcmVlTm9kZURlZj1cImxldCBub2RlOyB3aGVuOiB1aUFjdGlvbi5Pcmdhbml6YXRpb25Ob2RlSGFzQ2hpbGRcIj5cclxuICAgIDxsaT5cclxuICAgICAgPGRpdiBjbGFzcz1cIm1hdC10cmVlLW5vZGVcIj5cclxuICAgICAgICA8YnV0dG9uIG1hdC1pY29uLWJ1dHRvbiBtYXRUcmVlTm9kZVRvZ2dsZSBcclxuICAgICAgICAgICAgY2xhc3M9XCJmb2YtdGVzdC1idG5cIj5cclxuICAgICAgICAgIDxtYXQtaWNvbiBjbGFzcz1cIm1hdC1pY29uLXJ0bC1taXJyb3JcIj5cclxuICAgICAgICAgICAge3t0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMudHJlZUNvbnRyb2wuaXNFeHBhbmRlZChub2RlKSA/ICdleHBhbmRfbW9yZScgOiAnY2hldnJvbl9yaWdodCd9fVxyXG4gICAgICAgICAgPC9tYXQtaWNvbj5cclxuICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICA8bWF0LWNoZWNrYm94IFxyXG4gICAgICAgICAgW2NoZWNrZWRdPVwibm9kZS5jaGVja2VkXCIgICAgICAgICAgXHJcbiAgICAgICAgICBbZGlzYWJsZWRdPVwibm9kZS5ub3RTZWxlY3RhYmxlXCJcclxuICAgICAgICAgIChjaGFuZ2UpPVwidWlBY3Rpb24udHJlZVZpZXdOb2RlU2VsZWN0KG5vZGUsICRldmVudClcIj4gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgIHt7bm9kZS5uYW1lfX08L21hdC1jaGVja2JveD4gICAgICAgICAgICAgICAgXHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8dWwgY2xhc3M9XCJcIiBbY2xhc3MudHJlZXZpZXctaW52aXNpYmxlXT1cIiF0aGlzLnVpVmFyLm9yZ2FuaXphdGlvbnMudHJlZUNvbnRyb2wuaXNFeHBhbmRlZChub2RlKVwiPlxyXG4gICAgICAgIDxuZy1jb250YWluZXIgbWF0VHJlZU5vZGVPdXRsZXQ+PC9uZy1jb250YWluZXI+XHJcbiAgICAgIDwvdWw+XHJcbiAgICA8L2xpPlxyXG4gIDwvbWF0LW5lc3RlZC10cmVlLW5vZGU+XHJcbjwvbWF0LXRyZWU+Il19
import { Injectable, ErrorHandler, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "./notification/notification.service";
/** Application-wide error handler that adds a UI notification to the error handling
 * provided by the default Angular ErrorHandler.
 */
export class fofErrorHandler extends ErrorHandler {
    constructor(notificationsService, fofConfig) {
        super();
        this.notificationsService = notificationsService;
        this.fofConfig = fofConfig;
    }
    // handleError(error: Error | HttpErrorResponse | UnhandledRejection) {
    handleError(exception) {
        let displayMessage = `Oops, nous avons une erreur...`;
        let error;
        // if (exception instanceof HttpErrorResponse) {
        //   console.log('HttpErrorResponse', exception)
        //   error = exception.error
        //   if (error.fofCoreException) { 
        //     console.log('fof core exception', exception)
        //   }
        // }   
        // ToDO: manage promise unhandledrejection -> error in code
        // https://javascript.info/promise-error-handling
        if (exception.promise) {
            displayMessage = `<small>Technical notice</small><br>
        Forgot to manage a promise rejection...<br>
        <small>(yeah we know, it's funny ;)</small>`;
            console.info(`fofErrorHandler - Promises not catched!`);
        }
        // console.error('fofErrorHandler - Common error', exception)
        this.notificationsService.error(displayMessage, { mustDisappearAfter: -1 });
        super.handleError(exception);
    }
}
fofErrorHandler.ɵfac = function fofErrorHandler_Factory(t) { return new (t || fofErrorHandler)(i0.ɵɵinject(i1.FofNotificationService), i0.ɵɵinject(CORE_CONFIG)); };
fofErrorHandler.ɵprov = i0.ɵɵdefineInjectable({ token: fofErrorHandler, factory: fofErrorHandler.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(fofErrorHandler, [{
        type: Injectable
    }], function () { return [{ type: i1.FofNotificationService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWVycm9yLWhhbmRsZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYXBwLWVycm9yLWhhbmRsZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFFaEUsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7O0FBSXZEOztHQUVHO0FBRUgsTUFBTSxPQUFPLGVBQWdCLFNBQVEsWUFBWTtJQUUvQyxZQUNVLG9CQUE0QyxFQUN2QixTQUFxQjtRQUVsRCxLQUFLLEVBQUUsQ0FBQTtRQUhDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBd0I7UUFDdkIsY0FBUyxHQUFULFNBQVMsQ0FBWTtJQUdwRCxDQUFDO0lBRUQsdUVBQXVFO0lBQ3ZFLFdBQVcsQ0FBQyxTQUFjO1FBQ3hCLElBQUksY0FBYyxHQUFHLGdDQUFnQyxDQUFBO1FBQ3JELElBQUksS0FBSyxDQUFBO1FBRVQsZ0RBQWdEO1FBQ2hELGdEQUFnRDtRQUNoRCw0QkFBNEI7UUFDNUIsbUNBQW1DO1FBQ25DLG1EQUFtRDtRQUNuRCxNQUFNO1FBQ04sT0FBTztRQUdQLDJEQUEyRDtRQUMzRCxpREFBaUQ7UUFDakQsSUFBSSxTQUFTLENBQUMsT0FBTyxFQUFFO1lBQ3JCLGNBQWMsR0FBRzs7b0RBRTZCLENBQUE7WUFDOUMsT0FBTyxDQUFDLElBQUksQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFBO1NBQ3hEO1FBRUQsNkRBQTZEO1FBRTdELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLEVBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFBO1FBRXpFLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUE7SUFDOUIsQ0FBQzs7OEVBckNVLGVBQWUsc0RBSWhCLFdBQVc7dURBSlYsZUFBZSxXQUFmLGVBQWU7a0RBQWYsZUFBZTtjQUQzQixVQUFVOztzQkFLTixNQUFNO3VCQUFDLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBFcnJvckhhbmRsZXIsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IEh0dHBFcnJvclJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnXHJcbmltcG9ydCB7IElmb2ZDb25maWcsIENPUkVfQ09ORklHIH0gZnJvbSAnLi4vZm9mLWNvbmZpZydcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5cclxuLyoqIEFwcGxpY2F0aW9uLXdpZGUgZXJyb3IgaGFuZGxlciB0aGF0IGFkZHMgYSBVSSBub3RpZmljYXRpb24gdG8gdGhlIGVycm9yIGhhbmRsaW5nXHJcbiAqIHByb3ZpZGVkIGJ5IHRoZSBkZWZhdWx0IEFuZ3VsYXIgRXJyb3JIYW5kbGVyLlxyXG4gKi9cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgZm9mRXJyb3JIYW5kbGVyIGV4dGVuZHMgRXJyb3JIYW5kbGVyIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIG5vdGlmaWNhdGlvbnNTZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgQEluamVjdChDT1JFX0NPTkZJRykgcHJpdmF0ZSBmb2ZDb25maWc6IElmb2ZDb25maWdcclxuICApIHsgICAgXHJcbiAgICBzdXBlcigpICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gaGFuZGxlRXJyb3IoZXJyb3I6IEVycm9yIHwgSHR0cEVycm9yUmVzcG9uc2UgfCBVbmhhbmRsZWRSZWplY3Rpb24pIHtcclxuICBoYW5kbGVFcnJvcihleGNlcHRpb246IGFueSkgeyAgICBcclxuICAgIGxldCBkaXNwbGF5TWVzc2FnZSA9IGBPb3BzLCBub3VzIGF2b25zIHVuZSBlcnJldXIuLi5gXHJcbiAgICBsZXQgZXJyb3JcclxuXHJcbiAgICAvLyBpZiAoZXhjZXB0aW9uIGluc3RhbmNlb2YgSHR0cEVycm9yUmVzcG9uc2UpIHtcclxuICAgIC8vICAgY29uc29sZS5sb2coJ0h0dHBFcnJvclJlc3BvbnNlJywgZXhjZXB0aW9uKVxyXG4gICAgLy8gICBlcnJvciA9IGV4Y2VwdGlvbi5lcnJvclxyXG4gICAgLy8gICBpZiAoZXJyb3IuZm9mQ29yZUV4Y2VwdGlvbikgeyBcclxuICAgIC8vICAgICBjb25zb2xlLmxvZygnZm9mIGNvcmUgZXhjZXB0aW9uJywgZXhjZXB0aW9uKVxyXG4gICAgLy8gICB9XHJcbiAgICAvLyB9ICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgLy8gVG9ETzogbWFuYWdlIHByb21pc2UgdW5oYW5kbGVkcmVqZWN0aW9uIC0+IGVycm9yIGluIGNvZGVcclxuICAgIC8vIGh0dHBzOi8vamF2YXNjcmlwdC5pbmZvL3Byb21pc2UtZXJyb3ItaGFuZGxpbmdcclxuICAgIGlmIChleGNlcHRpb24ucHJvbWlzZSkgeyBcclxuICAgICAgZGlzcGxheU1lc3NhZ2UgPSBgPHNtYWxsPlRlY2huaWNhbCBub3RpY2U8L3NtYWxsPjxicj5cclxuICAgICAgICBGb3Jnb3QgdG8gbWFuYWdlIGEgcHJvbWlzZSByZWplY3Rpb24uLi48YnI+XHJcbiAgICAgICAgPHNtYWxsPih5ZWFoIHdlIGtub3csIGl0J3MgZnVubnkgOyk8L3NtYWxsPmBcclxuICAgICAgY29uc29sZS5pbmZvKGBmb2ZFcnJvckhhbmRsZXIgLSBQcm9taXNlcyBub3QgY2F0Y2hlZCFgKVxyXG4gICAgfVxyXG5cclxuICAgIC8vIGNvbnNvbGUuZXJyb3IoJ2ZvZkVycm9ySGFuZGxlciAtIENvbW1vbiBlcnJvcicsIGV4Y2VwdGlvbilcclxuXHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvbnNTZXJ2aWNlLmVycm9yKGRpc3BsYXlNZXNzYWdlLCB7bXVzdERpc2FwcGVhckFmdGVyOiAtMX0pXHJcblxyXG4gICAgc3VwZXIuaGFuZGxlRXJyb3IoZXhjZXB0aW9uKVxyXG4gIH1cclxufVxyXG4iXX0=
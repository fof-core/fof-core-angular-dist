import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG, eAuth } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./auth.service";
import * as i3 from "./notification/notification.service";
export class FofAuthGuard {
    constructor(router, foFAuthService, fofNotificationService, fofConfig) {
        this.router = router;
        this.foFAuthService = foFAuthService;
        this.fofNotificationService = fofNotificationService;
        this.fofConfig = fofConfig;
    }
    canActivate(next, state) {
        // for permissions in route 
        // https://jasonwatmore.com/post/2019/08/06/angular-8-role-based-authorization-tutorial-with-example#tsconfig-json
        const currentUser = this.foFAuthService.currentUser;
        let notAuthentifiedRoute = '/login';
        if (this.fofConfig.authentication) {
            if (this.fofConfig.authentication.type == eAuth.windows) {
                notAuthentifiedRoute = 'loading';
            }
        }
        if (currentUser) {
            // check if route is restricted by permission      
            // get the deepest root data
            // https://stackoverflow.com/questions/43806188/how-can-i-access-an-activated-child-routes-data-from-the-parent-routes-compone
            let route = next;
            while (route.firstChild) {
                route = route.firstChild;
            }
            const data = route.data;
            if (data.permissions) {
                const found = Object.values(currentUser.permissions).some((r) => data.permissions.indexOf(r) >= 0);
                if (!found) {
                    this.fofNotificationService.error(`Vous n'avez pas les droit suffisant<br>
            pour naviguer vers ${state.url}`);
                    this.router.navigate(['/']);
                    return false;
                }
            }
            // authorised
            return true;
        }
        // not logged in. Redirect to then notAuthentified page with the url to return on
        this.router.navigate([notAuthentifiedRoute], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
FofAuthGuard.ɵfac = function FofAuthGuard_Factory(t) { return new (t || FofAuthGuard)(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.FoFAuthService), i0.ɵɵinject(i3.FofNotificationService), i0.ɵɵinject(CORE_CONFIG)); };
FofAuthGuard.ɵprov = i0.ɵɵdefineInjectable({ token: FofAuthGuard, factory: FofAuthGuard.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofAuthGuard, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.Router }, { type: i2.FoFAuthService }, { type: i3.FofNotificationService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYXV0aC5ndWFyZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUtuRCxPQUFPLEVBQWMsV0FBVyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQTs7Ozs7QUFLOUQsTUFBTSxPQUFPLFlBQVk7SUFFdkIsWUFDVSxNQUFjLEVBQ2QsY0FBOEIsRUFDOUIsc0JBQThDLEVBQ3pCLFNBQXFCO1FBSDFDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUN6QixjQUFTLEdBQVQsU0FBUyxDQUFZO0lBQ2hELENBQUM7SUFFTCxXQUFXLENBQ1QsSUFBNEIsRUFDNUIsS0FBMEI7UUFDMUIsNEJBQTRCO1FBQzVCLGtIQUFrSDtRQUNsSCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQTtRQUNuRCxJQUFJLG9CQUFvQixHQUFHLFFBQVEsQ0FBQTtRQUduQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFO1lBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ3ZELG9CQUFvQixHQUFHLFNBQVMsQ0FBQTthQUNqQztTQUNGO1FBRUQsSUFBSSxXQUFXLEVBQUU7WUFDZixtREFBbUQ7WUFDbkQsNEJBQTRCO1lBQzVCLDhIQUE4SDtZQUM5SCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7WUFBQyxPQUFPLEtBQUssQ0FBQyxVQUFVLEVBQUU7Z0JBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUE7YUFBRTtZQUN2RSxNQUFNLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFBO1lBRXZCLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDcEIsTUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBUyxFQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtnQkFDekcsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDVixJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDO2lDQUNYLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFBO29CQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7b0JBQzNCLE9BQU8sS0FBSyxDQUFBO2lCQUNiO2FBQ0Y7WUFDRCxhQUFhO1lBQ2IsT0FBTyxJQUFJLENBQUE7U0FDWjtRQUVELGlGQUFpRjtRQUNqRixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLG9CQUFvQixDQUFDLEVBQUUsRUFBRSxXQUFXLEVBQUUsRUFBRSxTQUFTLEVBQUUsS0FBSyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQTtRQUN2RixPQUFPLEtBQUssQ0FBQTtJQUNkLENBQUM7O3dFQS9DVSxZQUFZLDhHQU1iLFdBQVc7b0RBTlYsWUFBWSxXQUFaLFlBQVksbUJBRlgsTUFBTTtrREFFUCxZQUFZO2NBSHhCLFVBQVU7ZUFBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7c0JBT0ksTUFBTTt1QkFBQyxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhbkFjdGl2YXRlLCBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXJTdGF0ZVNuYXBzaG90LCBVcmxUcmVlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJ1xyXG5pbXBvcnQgeyBGb0ZBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IElmb2ZDb25maWcsIENPUkVfQ09ORklHLCBlQXV0aCB9IGZyb20gJy4uL2ZvZi1jb25maWcnXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZBdXRoR3VhcmQgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgZm9GQXV0aFNlcnZpY2U6IEZvRkF1dGhTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgQEluamVjdChDT1JFX0NPTkZJRykgcHJpdmF0ZSBmb2ZDb25maWc6IElmb2ZDb25maWdcclxuICApIHsgfVxyXG5cclxuICBjYW5BY3RpdmF0ZShcclxuICAgIG5leHQ6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsICAgIFxyXG4gICAgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBPYnNlcnZhYmxlPGJvb2xlYW4gfCBVcmxUcmVlPiB8IFByb21pc2U8Ym9vbGVhbiB8IFVybFRyZWU+IHwgYm9vbGVhbiB8IFVybFRyZWUge1xyXG4gICAgLy8gZm9yIHBlcm1pc3Npb25zIGluIHJvdXRlIFxyXG4gICAgLy8gaHR0cHM6Ly9qYXNvbndhdG1vcmUuY29tL3Bvc3QvMjAxOS8wOC8wNi9hbmd1bGFyLTgtcm9sZS1iYXNlZC1hdXRob3JpemF0aW9uLXR1dG9yaWFsLXdpdGgtZXhhbXBsZSN0c2NvbmZpZy1qc29uXHJcbiAgICBjb25zdCBjdXJyZW50VXNlciA9IHRoaXMuZm9GQXV0aFNlcnZpY2UuY3VycmVudFVzZXJcclxuICAgIGxldCBub3RBdXRoZW50aWZpZWRSb3V0ZSA9ICcvbG9naW4nXHJcbiAgICBcclxuICAgIFxyXG4gICAgaWYgKHRoaXMuZm9mQ29uZmlnLmF1dGhlbnRpY2F0aW9uKSB7XHJcbiAgICAgIGlmICh0aGlzLmZvZkNvbmZpZy5hdXRoZW50aWNhdGlvbi50eXBlID09IGVBdXRoLndpbmRvd3MpIHtcclxuICAgICAgICBub3RBdXRoZW50aWZpZWRSb3V0ZSA9ICdsb2FkaW5nJ1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGN1cnJlbnRVc2VyKSB7XHJcbiAgICAgIC8vIGNoZWNrIGlmIHJvdXRlIGlzIHJlc3RyaWN0ZWQgYnkgcGVybWlzc2lvbiAgICAgIFxyXG4gICAgICAvLyBnZXQgdGhlIGRlZXBlc3Qgcm9vdCBkYXRhXHJcbiAgICAgIC8vIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzQzODA2MTg4L2hvdy1jYW4taS1hY2Nlc3MtYW4tYWN0aXZhdGVkLWNoaWxkLXJvdXRlcy1kYXRhLWZyb20tdGhlLXBhcmVudC1yb3V0ZXMtY29tcG9uZVxyXG4gICAgICBsZXQgcm91dGUgPSBuZXh0OyB3aGlsZSAocm91dGUuZmlyc3RDaGlsZCkgeyByb3V0ZSA9IHJvdXRlLmZpcnN0Q2hpbGQgfVxyXG4gICAgICBjb25zdCBkYXRhID0gcm91dGUuZGF0YVxyXG5cclxuICAgICAgaWYgKGRhdGEucGVybWlzc2lvbnMpIHtcclxuICAgICAgICBjb25zdCBmb3VuZCA9IE9iamVjdC52YWx1ZXMoY3VycmVudFVzZXIucGVybWlzc2lvbnMpLnNvbWUoKHI6IHN0cmluZyk9PiBkYXRhLnBlcm1pc3Npb25zLmluZGV4T2YocikgPj0gMClcclxuICAgICAgICBpZiAoIWZvdW5kKSB7XHJcbiAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoYFZvdXMgbidhdmV6IHBhcyBsZXMgZHJvaXQgc3VmZmlzYW50PGJyPlxyXG4gICAgICAgICAgICBwb3VyIG5hdmlndWVyIHZlcnMgJHtzdGF0ZS51cmx9YClcclxuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLyddKVxyXG4gICAgICAgICAgcmV0dXJuIGZhbHNlXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC8vIGF1dGhvcmlzZWRcclxuICAgICAgcmV0dXJuIHRydWVcclxuICAgIH1cclxuXHJcbiAgICAvLyBub3QgbG9nZ2VkIGluLiBSZWRpcmVjdCB0byB0aGVuIG5vdEF1dGhlbnRpZmllZCBwYWdlIHdpdGggdGhlIHVybCB0byByZXR1cm4gb25cclxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtub3RBdXRoZW50aWZpZWRSb3V0ZV0sIHsgcXVlcnlQYXJhbXM6IHsgcmV0dXJuVXJsOiBzdGF0ZS51cmwgfSB9KVxyXG4gICAgcmV0dXJuIGZhbHNlXHJcbiAgfVxyXG4gIFxyXG59XHJcbiJdfQ==
import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "@angular/router";
export class FoFAuthService {
    constructor(fofConfig, httpClient, router) {
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.router = router;
        this.environment = this.fofConfig.environment;
        this.currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser$ = this.currentUserSubject.asObservable();
    }
    get currentUser() {
        if (this.currentUserSubject) {
            return this.currentUserSubject.value;
        }
        return null;
    }
    refreshByCookie() {
        return this.httpClient.get(`${this.environment.apiPath}/auth/getUser`)
            .pipe(map((user) => {
            if (user && user.accessToken) {
                this.currentUserSubject.next(user);
                // this.currentUserSubject.complete()         
            }
            return user;
        }));
    }
    loginKerberos() {
        return new Promise((resolve, reject) => {
            this.httpClient.get(`${this.environment.authPath}/auth/winlogin`, { withCredentials: true })
                .toPromise()
                .then((user) => {
                // give the temporary windows token to do the next get        
                if (user && user.accessToken) {
                    this.currentUserSubject.next(user);
                }
                this.httpClient.get(`${this.environment.apiPath}/auth/winUserValidate`)
                    .toPromise()
                    .then((user) => {
                    // get the final user. 
                    // Can work only if the windows user are registered into the App.
                    // if not, it means the user is just authentified against MS AD
                    if (user && user.accessToken) {
                        this.currentUserSubject.next(user);
                    }
                    this.currentUserSubject.complete();
                    resolve(user);
                })
                    .catch(error => {
                    reject(error);
                });
            })
                .catch(error => {
                reject(error);
            });
        });
    }
    login(username, password) {
        const data = {
            username: username,
            password: password
        };
        return this.httpClient.post(`${this.environment.apiPath}/auth/login`, data)
            .pipe(map((user) => {
            // login successful if there's a jwt token in the response         
            if (user && user.accessToken) {
                // There is a cookie with the jwt inside for managing web browser refresh            
                this.currentUserSubject.next(user);
            }
            return user;
        }));
    }
    logOut() {
        // remove cookie to log user out    
        return this.httpClient.get(`${this.environment.apiPath}/auth/logout`)
            .pipe(map((cookieDeleted) => {
            if (cookieDeleted) {
                this.currentUserSubject.next(null);
                this.router.navigate(['/login']);
            }
            return cookieDeleted;
        }));
        // .subscribe(
        //   cookieDeleted => {
        //     if (cookieDeleted) {
        //       this.currentUserSubject.next(null)    
        //       this.router.navigate(['/login'])        
        //     }
        //   },
        //   error => {
        //     throw error
        //   })    
    }
}
FoFAuthService.ɵfac = function FoFAuthService_Factory(t) { return new (t || FoFAuthService)(i0.ɵɵinject(CORE_CONFIG), i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.Router)); };
FoFAuthService.ɵprov = i0.ɵɵdefineInjectable({ token: FoFAuthService, factory: FoFAuthService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFAuthService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: i1.HttpClient }, { type: i2.Router }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9hdXRoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFHbEQsT0FBTyxFQUFFLGVBQWUsRUFBYyxNQUFNLE1BQU0sQ0FBQTtBQUNsRCxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUE7QUFDcEMsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7OztBQU12RCxNQUFNLE9BQU8sY0FBYztJQUV6QixZQUMrQixTQUFxQixFQUMxQyxVQUFzQixFQUN0QixNQUFjO1FBRk8sY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUMxQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFFdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQTtRQUM3QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxlQUFlLENBQWEsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUMxRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtJQUM1RCxDQUFDO0lBTUQsSUFBVyxXQUFXO1FBQ3BCLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzNCLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQTtTQUNyQztRQUNELE9BQU8sSUFBSSxDQUFBO0lBQ2IsQ0FBQztJQUVELGVBQWU7UUFDYixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLGVBQWUsQ0FBQzthQUMvRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBZ0IsRUFBRSxFQUFFO1lBQzdCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQzVCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQ2xDLDhDQUE4QzthQUMvQztZQUNELE9BQU8sSUFBSSxDQUFBO1FBQ2IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNQLENBQUM7SUFFRCxhQUFhO1FBQ1gsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxnQkFBZ0IsRUFBRSxFQUFFLGVBQWUsRUFBRSxJQUFJLEVBQUUsQ0FBQztpQkFDM0YsU0FBUyxFQUFFO2lCQUNYLElBQUksQ0FBQyxDQUFDLElBQWdCLEVBQUUsRUFBRTtnQkFDekIsOERBQThEO2dCQUM5RCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO2lCQUNuQztnQkFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBYSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyx1QkFBdUIsQ0FBQztxQkFDbEYsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxDQUFDLElBQWdCLEVBQUUsRUFBRTtvQkFDekIsdUJBQXVCO29CQUN2QixpRUFBaUU7b0JBQ2pFLCtEQUErRDtvQkFDL0QsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTt3QkFDNUIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtxQkFDbkM7b0JBQ0QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxDQUFBO29CQUNsQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQ2YsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDYixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7Z0JBQ2YsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNiLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUNmLENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBR0QsS0FBSyxDQUFDLFFBQWdCLEVBQUUsUUFBZ0I7UUFFdEMsTUFBTSxJQUFJLEdBQUc7WUFDWCxRQUFRLEVBQUUsUUFBUTtZQUNsQixRQUFRLEVBQUUsUUFBUTtTQUNuQixDQUFBO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBYSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxhQUFhLEVBQUUsSUFBSSxDQUFDO2FBQ3BGLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFnQixFQUFFLEVBQUU7WUFDN0IsbUVBQW1FO1lBQ25FLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQzVCLHFGQUFxRjtnQkFDckYsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUNuQztZQUVELE9BQU8sSUFBSSxDQUFBO1FBQ2IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNQLENBQUM7SUFFRCxNQUFNO1FBQ0osb0NBQW9DO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sY0FBYyxDQUFDO2FBQ3JFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxhQUFzQixFQUFFLEVBQUU7WUFDbkMsSUFBSSxhQUFhLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQTthQUNqQztZQUNELE9BQU8sYUFBYSxDQUFBO1FBQ3RCLENBQUMsQ0FBQyxDQUFDLENBQUE7UUFHWCxjQUFjO1FBQ2QsdUJBQXVCO1FBQ3ZCLDJCQUEyQjtRQUMzQiwrQ0FBK0M7UUFDL0MsaURBQWlEO1FBQ2pELFFBQVE7UUFDUixPQUFPO1FBQ1AsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixXQUFXO0lBQ2IsQ0FBQzs7NEVBM0dVLGNBQWMsY0FHZixXQUFXO3NEQUhWLGNBQWMsV0FBZCxjQUFjLG1CQUZiLE1BQU07a0RBRVAsY0FBYztjQUgxQixVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7O3NCQUlJLE1BQU07dUJBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJ1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJ1xyXG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycydcclxuaW1wb3J0IHsgSWZvZkNvbmZpZywgQ09SRV9DT05GSUcgfSBmcm9tICcuLi9mb2YtY29uZmlnJ1xyXG5pbXBvcnQgeyBpVXNlckxvZ2luIH0gZnJvbSAnLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvRkF1dGhTZXJ2aWNlIHtcclxuICBcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIEBJbmplY3QoQ09SRV9DT05GSUcpIHByaXZhdGUgZm9mQ29uZmlnOiBJZm9mQ29uZmlnLFxyXG4gICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlclxyXG4gICkgeyBcclxuICAgIHRoaXMuZW52aXJvbm1lbnQgPSB0aGlzLmZvZkNvbmZpZy5lbnZpcm9ubWVudFxyXG4gICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGlVc2VyTG9naW4+KEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRVc2VyJykpKVxyXG4gICAgdGhpcy5jdXJyZW50VXNlciQgPSB0aGlzLmN1cnJlbnRVc2VyU3ViamVjdC5hc09ic2VydmFibGUoKSAgICBcclxuICB9XHJcblxyXG4gIHByaXZhdGUgY3VycmVudFVzZXJTdWJqZWN0OiBCZWhhdmlvclN1YmplY3Q8aVVzZXJMb2dpbj5cclxuICBwcml2YXRlIGVudmlyb25tZW50OmFueVxyXG4gIHB1YmxpYyBjdXJyZW50VXNlciQ6IE9ic2VydmFibGU8aVVzZXJMb2dpbj5cclxuXHJcbiAgcHVibGljIGdldCBjdXJyZW50VXNlcigpOiBpVXNlckxvZ2luIHtcclxuICAgIGlmICh0aGlzLmN1cnJlbnRVc2VyU3ViamVjdCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5jdXJyZW50VXNlclN1YmplY3QudmFsdWVcclxuICAgIH1cclxuICAgIHJldHVybiBudWxsXHJcbiAgfVxyXG5cclxuICByZWZyZXNoQnlDb29raWUoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxpVXNlckxvZ2luPihgJHt0aGlzLmVudmlyb25tZW50LmFwaVBhdGh9L2F1dGgvZ2V0VXNlcmApXHJcbiAgICAgIC5waXBlKG1hcCgodXNlcjogaVVzZXJMb2dpbikgPT4geyAgICAgICAgICAgICAgXHJcbiAgICAgICAgaWYgKHVzZXIgJiYgdXNlci5hY2Nlc3NUb2tlbikgeyAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyU3ViamVjdC5uZXh0KHVzZXIpICAgXHJcbiAgICAgICAgICAvLyB0aGlzLmN1cnJlbnRVc2VyU3ViamVjdC5jb21wbGV0ZSgpICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB1c2VyXHJcbiAgICAgIH0pKVxyXG4gIH1cclxuXHJcbiAgbG9naW5LZXJiZXJvcygpIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgIHRoaXMuaHR0cENsaWVudC5nZXQoYCR7dGhpcy5lbnZpcm9ubWVudC5hdXRoUGF0aH0vYXV0aC93aW5sb2dpbmAsIHsgd2l0aENyZWRlbnRpYWxzOiB0cnVlIH0pXHJcbiAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAudGhlbigodXNlcjogaVVzZXJMb2dpbikgPT4ge1xyXG4gICAgICAgIC8vIGdpdmUgdGhlIHRlbXBvcmFyeSB3aW5kb3dzIHRva2VuIHRvIGRvIHRoZSBuZXh0IGdldCAgICAgICAgXHJcbiAgICAgICAgaWYgKHVzZXIgJiYgdXNlci5hY2Nlc3NUb2tlbikgeyAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyU3ViamVjdC5uZXh0KHVzZXIpICAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaHR0cENsaWVudC5nZXQ8aVVzZXJMb2dpbj4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS9hdXRoL3dpblVzZXJWYWxpZGF0ZWApICAgICAgXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4oKHVzZXI6IGlVc2VyTG9naW4pID0+IHtcclxuICAgICAgICAgIC8vIGdldCB0aGUgZmluYWwgdXNlci4gXHJcbiAgICAgICAgICAvLyBDYW4gd29yayBvbmx5IGlmIHRoZSB3aW5kb3dzIHVzZXIgYXJlIHJlZ2lzdGVyZWQgaW50byB0aGUgQXBwLlxyXG4gICAgICAgICAgLy8gaWYgbm90LCBpdCBtZWFucyB0aGUgdXNlciBpcyBqdXN0IGF1dGhlbnRpZmllZCBhZ2FpbnN0IE1TIEFEXHJcbiAgICAgICAgICBpZiAodXNlciAmJiB1c2VyLmFjY2Vzc1Rva2VuKSB7ICAgICAgICBcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QubmV4dCh1c2VyKSAgICAgICAgICAgIFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QuY29tcGxldGUoKVxyXG4gICAgICAgICAgcmVzb2x2ZSh1c2VyKSAgICAgICAgICBcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgICByZWplY3QoZXJyb3IpXHJcbiAgICAgICAgfSlcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKGVycm9yID0+IHtcclxuICAgICAgICByZWplY3QoZXJyb3IpXHJcbiAgICAgIH0pXHJcbiAgICB9KVxyXG4gIH1cclxuICBcclxuXHJcbiAgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZykge1xyXG4gICAgXHJcbiAgICBjb25zdCBkYXRhID0ge1xyXG4gICAgICB1c2VybmFtZTogdXNlcm5hbWUsIFxyXG4gICAgICBwYXNzd29yZDogcGFzc3dvcmRcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8aVVzZXJMb2dpbj4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS9hdXRoL2xvZ2luYCwgZGF0YSlcclxuICAgICAgLnBpcGUobWFwKCh1c2VyOiBpVXNlckxvZ2luKSA9PiB7ICAgICAgIFxyXG4gICAgICAgIC8vIGxvZ2luIHN1Y2Nlc3NmdWwgaWYgdGhlcmUncyBhIGp3dCB0b2tlbiBpbiB0aGUgcmVzcG9uc2UgICAgICAgICBcclxuICAgICAgICBpZiAodXNlciAmJiB1c2VyLmFjY2Vzc1Rva2VuKSB7XHJcbiAgICAgICAgICAvLyBUaGVyZSBpcyBhIGNvb2tpZSB3aXRoIHRoZSBqd3QgaW5zaWRlIGZvciBtYW5hZ2luZyB3ZWIgYnJvd3NlciByZWZyZXNoICAgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRVc2VyU3ViamVjdC5uZXh0KHVzZXIpICAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdXNlclxyXG4gICAgICB9KSlcclxuICB9XHJcblxyXG4gIGxvZ091dCgpIHtcclxuICAgIC8vIHJlbW92ZSBjb29raWUgdG8gbG9nIHVzZXIgb3V0ICAgIFxyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8Ym9vbGVhbj4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS9hdXRoL2xvZ291dGApXHJcbiAgICAgICAgICAgIC5waXBlKG1hcCgoY29va2llRGVsZXRlZDogYm9vbGVhbikgPT4geyBcclxuICAgICAgICAgICAgICBpZiAoY29va2llRGVsZXRlZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QubmV4dChudWxsKSAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pICAgICAgICBcclxuICAgICAgICAgICAgICB9ICAgICAgXHJcbiAgICAgICAgICAgICAgcmV0dXJuIGNvb2tpZURlbGV0ZWRcclxuICAgICAgICAgICAgfSkpXHJcblxyXG5cclxuICAgIC8vIC5zdWJzY3JpYmUoXHJcbiAgICAvLyAgIGNvb2tpZURlbGV0ZWQgPT4ge1xyXG4gICAgLy8gICAgIGlmIChjb29raWVEZWxldGVkKSB7XHJcbiAgICAvLyAgICAgICB0aGlzLmN1cnJlbnRVc2VyU3ViamVjdC5uZXh0KG51bGwpICAgIFxyXG4gICAgLy8gICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSkgICAgICAgIFxyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgfSxcclxuICAgIC8vICAgZXJyb3IgPT4ge1xyXG4gICAgLy8gICAgIHRocm93IGVycm9yXHJcbiAgICAvLyAgIH0pICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gYXN5bmMgbG9nT3V0KCkge1xyXG4gIC8vICAgLy8gcmVtb3ZlIGNvb2tpZSB0byBsb2cgdXNlciBvdXQgICAgXHJcbiAgLy8gICB0aGlzLmh0dHBDbGllbnQuZ2V0PGJvb2xlYW4+KGAke3RoaXMuZW52aXJvbm1lbnQuYXBpUGF0aH0vYXV0aC9sb2dvdXRgKVxyXG4gIC8vICAgLnN1YnNjcmliZShcclxuICAvLyAgICAgY29va2llRGVsZXRlZCA9PiB7XHJcbiAgLy8gICAgICAgaWYgKGNvb2tpZURlbGV0ZWQpIHtcclxuICAvLyAgICAgICAgIHRoaXMuY3VycmVudFVzZXJTdWJqZWN0Lm5leHQobnVsbCkgICAgXHJcbiAgLy8gICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9sb2dpbiddKSAgICAgICAgXHJcbiAgLy8gICAgICAgfVxyXG4gIC8vICAgICB9LFxyXG4gIC8vICAgICBlcnJvciA9PiB7XHJcbiAgLy8gICAgICAgdGhyb3cgZXJyb3JcclxuICAvLyAgICAgfSkgICAgXHJcbiAgLy8gfVxyXG59XHJcbiJdfQ==
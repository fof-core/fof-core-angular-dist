// Angular
import { Component, EventEmitter, Input, Output, ViewEncapsulation, Inject, Optional } from '@angular/core';
// SPS Services
// SPS Models
// SPS Components
// Vendors & utils
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
function FofCoreDialogYesNoComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "button", 5);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r24 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r24.uiVar.noLabel);
} }
export class FofCoreDialogYesNoComponent {
    constructor(data) {
        this.data = data;
        // All input var
        this.anInput = undefined;
        // All output notification
        this.anOuputEvent = new EventEmitter();
        // All variables private to the class
        this.priVar = {};
        // All private functions
        this.privFunc = {};
        // All variables shared with UI HTML interface
        this.uiVar = {
            title: 'fof à une question',
            question: undefined,
            yesLabel: 'oui',
            noLabel: 'non',
            informationOnly: false
        };
        // All actions accessible by user (could be used internaly too)
        this.uiAction = {};
        // this.matDialogRef.updatePosition({ top: '20px', left: '50px' });
        this.uiVar.question = data.question;
        if (data.title) {
            this.uiVar.title = data.title;
        }
        if (data.yesLabel) {
            this.uiVar.yesLabel = data.yesLabel;
        }
        if (data.noLabel) {
            this.uiVar.noLabel = data.noLabel;
        }
        if (data.informationOnly) {
            this.uiVar.informationOnly = data.informationOnly;
        }
    }
    // Angular events
    ngOnInit() {
    }
    ngAfterViewInit() {
    }
}
FofCoreDialogYesNoComponent.ɵfac = function FofCoreDialogYesNoComponent_Factory(t) { return new (t || FofCoreDialogYesNoComponent)(i0.ɵɵdirectiveInject(MAT_DIALOG_DATA, 8)); };
FofCoreDialogYesNoComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofCoreDialogYesNoComponent, selectors: [["core-dialog-yes-no"]], inputs: { anInput: "anInput" }, outputs: { anOuputEvent: "anOuputEvent" }, decls: 9, vars: 5, consts: [[1, "view-mad--core--yes-no"], ["mat-dialog-title", ""], [3, "innerHTML"], ["mat-button", "", "mat-dialog-close", "", 4, "ngIf"], ["mat-button", "", 3, "mat-dialog-close"], ["mat-button", "", "mat-dialog-close", ""]], template: function FofCoreDialogYesNoComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "h2", 1);
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "mat-dialog-content");
        i0.ɵɵelement(4, "div", 2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(5, "mat-dialog-actions");
        i0.ɵɵtemplate(6, FofCoreDialogYesNoComponent_button_6_Template, 2, 1, "button", 3);
        i0.ɵɵelementStart(7, "button", 4);
        i0.ɵɵtext(8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.uiVar.title);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("innerHTML", ctx.uiVar.question, i0.ɵɵsanitizeHtml);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", !ctx.uiVar.informationOnly);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("mat-dialog-close", true);
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(ctx.uiVar.yesLabel);
    } }, directives: [i1.MatDialogTitle, i1.MatDialogContent, i1.MatDialogActions, i2.NgIf, i3.MatButton, i1.MatDialogClose], styles: [".view-mad--core--yes-no .mat-dialog-actions{margin-top:1rem;display:flex;justify-content:flex-end}"], encapsulation: 2 });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofCoreDialogYesNoComponent, [{
        type: Component,
        args: [{
                selector: 'core-dialog-yes-no',
                templateUrl: './core-dialog-yes-no.component.html',
                styleUrls: ['./core-dialog-yes-no.component.scss'],
                encapsulation: ViewEncapsulation.None
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Optional
            }, {
                type: Inject,
                args: [MAT_DIALOG_DATA]
            }] }]; }, { anInput: [{
            type: Input
        }], anOuputEvent: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvY29yZS1kaWFsb2cteWVzLW5vL2NvcmUtZGlhbG9nLXllcy1uby5jb21wb25lbnQudHMiLCJsaWIvY29yZS9jb3JlLWRpYWxvZy15ZXMtbm8vY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFVBQVU7QUFDVixPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLGlCQUFpQixFQUN4RCxNQUFNLEVBQUUsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFBO0FBQ3ZELGVBQWU7QUFDZixhQUFhO0FBQ2IsaUJBQWlCO0FBQ2pCLGtCQUFrQjtBQUNsQixPQUFPLEVBQUMsZUFBZSxFQUFnQixNQUFNLDBCQUEwQixDQUFBOzs7Ozs7SUNBbkUsaUNBQ2lDO0lBQUEsWUFBaUI7SUFBQSxpQkFBUzs7O0lBQTFCLGVBQWlCO0lBQWpCLDJDQUFpQjs7QUR1QnRELE1BQU0sT0FBTywyQkFBMkI7SUFNdEMsWUFDOEMsSUFBZTtRQUFmLFNBQUksR0FBSixJQUFJLENBQVc7UUFON0QsZ0JBQWdCO1FBQ1AsWUFBTyxHQUFRLFNBQVMsQ0FBQTtRQUNqQywwQkFBMEI7UUFDaEIsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFBO1FBeUJoRCxxQ0FBcUM7UUFDN0IsV0FBTSxHQUFHLEVBQ2hCLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHLEVBQ2xCLENBQUE7UUFDRCw4Q0FBOEM7UUFDdkMsVUFBSyxHQUFHO1lBQ2IsS0FBSyxFQUFFLG9CQUFvQjtZQUMzQixRQUFRLEVBQUUsU0FBUztZQUNuQixRQUFRLEVBQUUsS0FBSztZQUNmLE9BQU8sRUFBRSxLQUFLO1lBQ2QsZUFBZSxFQUFFLEtBQUs7U0FDdkIsQ0FBQTtRQUNELCtEQUErRDtRQUN4RCxhQUFRLEdBQUcsRUFDakIsQ0FBQTtRQWpDQyxtRUFBbUU7UUFDbkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQTtRQUVuQyxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDZCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFBO1NBQzlCO1FBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUE7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtTQUNsQztRQUNELElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFBO1NBQ2xEO0lBQ0gsQ0FBQztJQW1CRCxpQkFBaUI7SUFDakIsUUFBUTtJQUVSLENBQUM7SUFDRCxlQUFlO0lBRWYsQ0FBQzs7c0dBcERVLDJCQUEyQix1QkFPaEIsZUFBZTtnRUFQMUIsMkJBQTJCO1FDL0J4Qyw4QkFDRTtRQUFBLDZCQUFxQjtRQUFBLFlBQWU7UUFBQSxpQkFBSztRQUN6QywwQ0FDRTtRQUFBLHlCQUF3QztRQUUxQyxpQkFBcUI7UUFDckIsMENBQ0U7UUFBQSxrRkFDaUM7UUFFakMsaUNBQTZDO1FBQUEsWUFBa0I7UUFBQSxpQkFBUztRQUMxRSxpQkFBcUI7UUFDdkIsaUJBQU07O1FBWGlCLGVBQWU7UUFBZixxQ0FBZTtRQUU3QixlQUE0QjtRQUE1QixpRUFBNEI7UUFLL0IsZUFBOEI7UUFBOUIsaURBQThCO1FBRWIsZUFBeUI7UUFBekIsdUNBQXlCO1FBQUMsZUFBa0I7UUFBbEIsd0NBQWtCOztrRERxQnRELDJCQUEyQjtjQU52QyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsV0FBVyxFQUFFLHFDQUFxQztnQkFDbEQsU0FBUyxFQUFFLENBQUMscUNBQXFDLENBQUM7Z0JBQ2xELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2FBQ3RDOztzQkFRSSxRQUFROztzQkFBSSxNQUFNO3VCQUFDLGVBQWU7O2tCQUxwQyxLQUFLOztrQkFFTCxNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQW5ndWxhclxyXG5pbXBvcnQge0NvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBcclxuICBBZnRlclZpZXdJbml0LCBJbmplY3QsIE9wdGlvbmFsfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG4vLyBTUFMgU2VydmljZXNcclxuLy8gU1BTIE1vZGVsc1xyXG4vLyBTUFMgQ29tcG9uZW50c1xyXG4vLyBWZW5kb3JzICYgdXRpbHNcclxuaW1wb3J0IHtNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZydcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgaUluZm9ybWF0aW9uIHtcclxuICB0aXRsZT8gOiBzdHJpbmcsXHJcbiAgcXVlc3Rpb246IHN0cmluZyxcclxuICB5ZXNMYWJlbD8gOiBzdHJpbmcsICBcclxuICB3aWR0aD86IHN0cmluZyxcclxuICBoZWlnaHQ/OiBzdHJpbmcgIFxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIGlZZXNObyBleHRlbmRzIGlJbmZvcm1hdGlvbiAge1xyXG4gIG5vTGFiZWw/OiBzdHJpbmcsXHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgaUludGVybmFsIGV4dGVuZHMgaVllc05vICB7XHJcbiAgaW5mb3JtYXRpb25Pbmx5OiBib29sZWFuXHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnY29yZS1kaWFsb2cteWVzLW5vJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb3JlLWRpYWxvZy15ZXMtbm8uY29tcG9uZW50LnNjc3MnXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZDb3JlRGlhbG9nWWVzTm9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIC8vIEFsbCBpbnB1dCB2YXJcclxuICBASW5wdXQoKSBhbklucHV0OiBhbnkgPSB1bmRlZmluZWRcclxuICAvLyBBbGwgb3V0cHV0IG5vdGlmaWNhdGlvblxyXG4gIEBPdXRwdXQoKSBhbk91cHV0RXZlbnQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKVxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIEBPcHRpb25hbCgpIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogaUludGVybmFsLFxyXG4gICAgLy8gcHVibGljIG1hdERpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPGFueT5cclxuICAgIC8vIHByaXZhdGUgcmVhZG9ubHkgY29yZUFsZXJ0U2VydmljZTogQ29yZUFsZXJ0U2VydmljZSxcclxuICAgIC8vIHByaXZhdGUgcmVhZG9ubHkgY29yZUVycm9yU2VydmljZTogQ29yZUVycm9yU2VydmljZVxyXG4gICkge1xyXG4gICAgLy8gdGhpcy5tYXREaWFsb2dSZWYudXBkYXRlUG9zaXRpb24oeyB0b3A6ICcyMHB4JywgbGVmdDogJzUwcHgnIH0pO1xyXG4gICAgdGhpcy51aVZhci5xdWVzdGlvbiA9IGRhdGEucXVlc3Rpb25cclxuXHJcbiAgICBpZiAoZGF0YS50aXRsZSkge1xyXG4gICAgICB0aGlzLnVpVmFyLnRpdGxlID0gZGF0YS50aXRsZVxyXG4gICAgfSAgICBcclxuICAgIGlmIChkYXRhLnllc0xhYmVsKSB7XHJcbiAgICAgIHRoaXMudWlWYXIueWVzTGFiZWwgPSBkYXRhLnllc0xhYmVsXHJcbiAgICB9ICAgIFxyXG4gICAgaWYgKGRhdGEubm9MYWJlbCkge1xyXG4gICAgICB0aGlzLnVpVmFyLm5vTGFiZWwgPSBkYXRhLm5vTGFiZWxcclxuICAgIH0gICAgXHJcbiAgICBpZiAoZGF0YS5pbmZvcm1hdGlvbk9ubHkpIHtcclxuICAgICAgdGhpcy51aVZhci5pbmZvcm1hdGlvbk9ubHkgPSBkYXRhLmluZm9ybWF0aW9uT25seVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBwcml2YXRlIHRvIHRoZSBjbGFzc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gIH1cclxuICAvLyBBbGwgdmFyaWFibGVzIHNoYXJlZCB3aXRoIFVJIEhUTUwgaW50ZXJmYWNlXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgdGl0bGU6ICdmb2Ygw6AgdW5lIHF1ZXN0aW9uJyxcclxuICAgIHF1ZXN0aW9uOiB1bmRlZmluZWQsXHJcbiAgICB5ZXNMYWJlbDogJ291aScsXHJcbiAgICBub0xhYmVsOiAnbm9uJyxcclxuICAgIGluZm9ybWF0aW9uT25seTogZmFsc2VcclxuICB9XHJcbiAgLy8gQWxsIGFjdGlvbnMgYWNjZXNzaWJsZSBieSB1c2VyIChjb3VsZCBiZSB1c2VkIGludGVybmFseSB0b28pXHJcbiAgcHVibGljIHVpQWN0aW9uID0ge1xyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICBcclxuICB9XHJcbiAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgXHJcbiAgfVxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJ2aWV3LW1hZC0tY29yZS0teWVzLW5vXCI+XHJcbiAgPGgyIG1hdC1kaWFsb2ctdGl0bGU+e3t1aVZhci50aXRsZX19PC9oMj5cclxuICA8bWF0LWRpYWxvZy1jb250ZW50PlxyXG4gICAgPGRpdiBbaW5uZXJIVE1MXT1cInVpVmFyLnF1ZXN0aW9uXCI+PC9kaXY+XHJcbiAgICA8IS0tIHt7dWlWYXIucXVlc3Rpb259fSAtLT5cclxuICA8L21hdC1kaWFsb2ctY29udGVudD5cclxuICA8bWF0LWRpYWxvZy1hY3Rpb25zPlxyXG4gICAgPGJ1dHRvbiBtYXQtYnV0dG9uIG1hdC1kaWFsb2ctY2xvc2VcclxuICAgICAgKm5nSWY9XCIhdWlWYXIuaW5mb3JtYXRpb25Pbmx5XCI+e3t1aVZhci5ub0xhYmVsfX08L2J1dHRvbj5cclxuICAgIDwhLS0gVGhlIG1hdC1kaWFsb2ctY2xvc2UgZGlyZWN0aXZlIG9wdGlvbmFsbHkgYWNjZXB0cyBhIHZhbHVlIGFzIGEgcmVzdWx0IGZvciB0aGUgZGlhbG9nLiAtLT5cclxuICAgIDxidXR0b24gbWF0LWJ1dHRvbiBbbWF0LWRpYWxvZy1jbG9zZV09XCJ0cnVlXCI+e3t1aVZhci55ZXNMYWJlbH19PC9idXR0b24+ICAgIFxyXG4gIDwvbWF0LWRpYWxvZy1hY3Rpb25zPlxyXG48L2Rpdj4iXX0=
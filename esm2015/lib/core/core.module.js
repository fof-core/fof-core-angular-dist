import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FofLocalstorageService } from './fof-localstorage.service';
import { fofErrorHandler } from './app-error-handler.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FofAuthGuard } from './auth.guard';
import { FoFAuthService } from './auth.service';
import { FofErrorInterceptor } from './http-error.interceptor';
import { FoFJwtInterceptor } from './jwt.interceptor';
import { FoFCoreService } from './core.service';
import { FofNotificationService } from './notification/notification.service';
import { notificationComponent } from './notification/notification/notification.component';
import { MaterialModule } from '../core/material.module';
import { FofCoreDialogYesNoComponent } from './core-dialog-yes-no/core-dialog-yes-no.component';
import { FofDialogService } from './fof-dialog.service';
import { FofErrorService } from './fof-error.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from '../shared/shared.module';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
// import { ComponentsModule } from '../components/components.module'
// import { ModuleTranslateLoader, IModuleTranslationOptions } from '@larscom/ngx-translate-module-loader'
// import { registerLocaleData } from '@angular/common'
// import localeFr from '@angular/common/locales/fr'
// import localeFrExtra from '@angular/common/locales/extra/fr'
// registerLocaleData(localeFr)
// AoT requires an exported function for factories
export function HttpLoaderFactory(http) {
    return new TranslateHttpLoader(http);
}
// https://github.com/larscom/ngx-translate-module-loader
// export function moduleHttpLoaderFactory(http: HttpClient) {
//   const baseTranslateUrl = './assets/i18n';
//   const options: IModuleTranslationOptions = {
//     modules: [
//       // final url: ./assets/i18n/en.json
//       { baseTranslateUrl },
//       // final url: ./assets/i18n/admin/en.json
//       { moduleName: 'admin', baseTranslateUrl },
//       // final url: ./assets/i18n/feature2/en.json
//       { moduleName: 'feature2', baseTranslateUrl }
//     ]
//   };
//   return new ModuleTranslateLoader(http, options);
// }
export class FoFCoreModule {
}
FoFCoreModule.ɵmod = i0.ɵɵdefineNgModule({ type: FoFCoreModule });
FoFCoreModule.ɵinj = i0.ɵɵdefineInjector({ factory: function FoFCoreModule_Factory(t) { return new (t || FoFCoreModule)(); }, providers: [
        FofLocalstorageService,
        // { provide: LOCALE_ID, useValue: 'fr' },
        { provide: ErrorHandler, useClass: fofErrorHandler },
        { provide: HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
        FofAuthGuard,
        FoFAuthService,
        FoFCoreService,
        FofNotificationService,
        FofDialogService,
        FofErrorService
    ], imports: [[
            CommonModule,
            SharedModule,
            MaterialModule,
            HttpClientModule,
            // ComponentsModule,
            TranslateModule.forRoot({
                defaultLanguage: 'fr',
                loader: {
                    provide: TranslateLoader,
                    useFactory: HttpLoaderFactory,
                    deps: [HttpClient]
                }
            })
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(FoFCoreModule, { declarations: [notificationComponent,
        FofCoreDialogYesNoComponent], imports: [CommonModule,
        SharedModule,
        MaterialModule,
        HttpClientModule, i1.TranslateModule], exports: [notificationComponent,
        FofCoreDialogYesNoComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFCoreModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    notificationComponent,
                    FofCoreDialogYesNoComponent
                ],
                imports: [
                    CommonModule,
                    SharedModule,
                    MaterialModule,
                    HttpClientModule,
                    // ComponentsModule,
                    TranslateModule.forRoot({
                        defaultLanguage: 'fr',
                        loader: {
                            provide: TranslateLoader,
                            useFactory: HttpLoaderFactory,
                            deps: [HttpClient]
                        }
                    })
                ],
                entryComponents: [
                    FofCoreDialogYesNoComponent
                ],
                providers: [
                    FofLocalstorageService,
                    // { provide: LOCALE_ID, useValue: 'fr' },
                    { provide: ErrorHandler, useClass: fofErrorHandler },
                    { provide: HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
                    { provide: HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
                    FofAuthGuard,
                    FoFAuthService,
                    FoFCoreService,
                    FofNotificationService,
                    FofDialogService,
                    FofErrorService
                ],
                exports: [
                    notificationComponent,
                    FofCoreDialogYesNoComponent,
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFhLE1BQU0sZUFBZSxDQUFBO0FBQ2pFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFDOUMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUE7QUFDbkUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFBO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFBO0FBQ3hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUE7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDBCQUEwQixDQUFBO0FBQzlELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1CQUFtQixDQUFBO0FBQ3JELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUMvQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQTtBQUM1RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQTtBQUMxRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUE7QUFDeEQsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sbURBQW1ELENBQUE7QUFDL0YsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUE7QUFDdkQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFBO0FBQ3JELE9BQU8sRUFBRSxlQUFlLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUE7QUFDdEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNEJBQTRCLENBQUE7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHlCQUF5QixDQUFBOzs7QUFDdEQscUVBQXFFO0FBQ3JFLDBHQUEwRztBQUUxRyx1REFBdUQ7QUFDdkQsb0RBQW9EO0FBQ3BELCtEQUErRDtBQUMvRCwrQkFBK0I7QUFFL0Isa0RBQWtEO0FBQ2xELE1BQU0sVUFBVSxpQkFBaUIsQ0FBQyxJQUFnQjtJQUNoRCxPQUFPLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDdEMsQ0FBQztBQUVELHlEQUF5RDtBQUN6RCw4REFBOEQ7QUFDOUQsOENBQThDO0FBRTlDLGlEQUFpRDtBQUNqRCxpQkFBaUI7QUFDakIsNENBQTRDO0FBQzVDLDhCQUE4QjtBQUM5QixrREFBa0Q7QUFDbEQsbURBQW1EO0FBQ25ELHFEQUFxRDtBQUNyRCxxREFBcUQ7QUFDckQsUUFBUTtBQUNSLE9BQU87QUFDUCxxREFBcUQ7QUFDckQsSUFBSTtBQTZDSixNQUFNLE9BQU8sYUFBYTs7aURBQWIsYUFBYTt5R0FBYixhQUFhLG1CQW5CYjtRQUNULHNCQUFzQjtRQUN0QiwwQ0FBMEM7UUFDMUMsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUU7UUFDcEQsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7UUFDeEUsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLG1CQUFtQixFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7UUFDMUUsWUFBWTtRQUNaLGNBQWM7UUFDZCxjQUFjO1FBQ2Qsc0JBQXNCO1FBQ3RCLGdCQUFnQjtRQUNoQixlQUFlO0tBQ2hCLFlBOUJRO1lBQ1AsWUFBWTtZQUNaLFlBQVk7WUFDWixjQUFjO1lBQ2QsZ0JBQWdCO1lBQ2hCLG9CQUFvQjtZQUNwQixlQUFlLENBQUMsT0FBTyxDQUFDO2dCQUN0QixlQUFlLEVBQUUsSUFBSTtnQkFDckIsTUFBTSxFQUFFO29CQUNOLE9BQU8sRUFBRSxlQUFlO29CQUN4QixVQUFVLEVBQUUsaUJBQWlCO29CQUM3QixJQUFJLEVBQUUsQ0FBQyxVQUFVLENBQUM7aUJBQ25CO2FBQ0YsQ0FBQztTQUNIO3dGQXVCVSxhQUFhLG1CQXhDdEIscUJBQXFCO1FBQ3JCLDJCQUEyQixhQUczQixZQUFZO1FBQ1osWUFBWTtRQUNaLGNBQWM7UUFDZCxnQkFBZ0IsaUNBNEJoQixxQkFBcUI7UUFDckIsMkJBQTJCO2tEQUlsQixhQUFhO2NBMUN6QixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLHFCQUFxQjtvQkFDckIsMkJBQTJCO2lCQUM1QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixZQUFZO29CQUNaLGNBQWM7b0JBQ2QsZ0JBQWdCO29CQUNoQixvQkFBb0I7b0JBQ3BCLGVBQWUsQ0FBQyxPQUFPLENBQUM7d0JBQ3RCLGVBQWUsRUFBRSxJQUFJO3dCQUNyQixNQUFNLEVBQUU7NEJBQ04sT0FBTyxFQUFFLGVBQWU7NEJBQ3hCLFVBQVUsRUFBRSxpQkFBaUI7NEJBQzdCLElBQUksRUFBRSxDQUFDLFVBQVUsQ0FBQzt5QkFDbkI7cUJBQ0YsQ0FBQztpQkFDSDtnQkFDRCxlQUFlLEVBQUU7b0JBQ2YsMkJBQTJCO2lCQUM1QjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1Qsc0JBQXNCO29CQUN0QiwwQ0FBMEM7b0JBQzFDLEVBQUUsT0FBTyxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFO29CQUNwRCxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRTtvQkFDeEUsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLG1CQUFtQixFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7b0JBQzFFLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxjQUFjO29CQUNkLHNCQUFzQjtvQkFDdEIsZ0JBQWdCO29CQUNoQixlQUFlO2lCQUNoQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AscUJBQXFCO29CQUNyQiwyQkFBMkI7aUJBRTVCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgRXJyb3JIYW5kbGVyLCBMT0NBTEVfSUQgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlLCBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbidcclxuaW1wb3J0IHsgRm9mTG9jYWxzdG9yYWdlU2VydmljZSB9IGZyb20gJy4vZm9mLWxvY2Fsc3RvcmFnZS5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBmb2ZFcnJvckhhbmRsZXIgfSBmcm9tICcuL2FwcC1lcnJvci1oYW5kbGVyLnNlcnZpY2UnXHJcbmltcG9ydCB7IEhUVFBfSU5URVJDRVBUT1JTIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnXHJcbmltcG9ydCB7IEZvZkF1dGhHdWFyZCB9IGZyb20gJy4vYXV0aC5ndWFyZCdcclxuaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UgfSBmcm9tICcuL2F1dGguc2VydmljZSdcclxuaW1wb3J0IHsgRm9mRXJyb3JJbnRlcmNlcHRvciB9IGZyb20gJy4vaHR0cC1lcnJvci5pbnRlcmNlcHRvcidcclxuaW1wb3J0IHsgRm9GSnd0SW50ZXJjZXB0b3IgfSBmcm9tICcuL2p3dC5pbnRlcmNlcHRvcidcclxuaW1wb3J0IHsgRm9GQ29yZVNlcnZpY2UgfSBmcm9tICcuL2NvcmUuc2VydmljZSdcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBub3RpZmljYXRpb25Db21wb25lbnQgfSBmcm9tICcuL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9jb3JlL21hdGVyaWFsLm1vZHVsZSdcclxuaW1wb3J0IHsgRm9mQ29yZURpYWxvZ1llc05vQ29tcG9uZW50IH0gZnJvbSAnLi9jb3JlLWRpYWxvZy15ZXMtbm8vY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9mRGlhbG9nU2VydmljZSB9IGZyb20gJy4vZm9mLWRpYWxvZy5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZFcnJvclNlcnZpY2UgfSBmcm9tICcuL2ZvZi1lcnJvci5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUsIFRyYW5zbGF0ZUxvYWRlciB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnXHJcbmltcG9ydCB7IFRyYW5zbGF0ZUh0dHBMb2FkZXIgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9odHRwLWxvYWRlcidcclxuaW1wb3J0IHsgU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vc2hhcmVkL3NoYXJlZC5tb2R1bGUnXHJcbi8vIGltcG9ydCB7IENvbXBvbmVudHNNb2R1bGUgfSBmcm9tICcuLi9jb21wb25lbnRzL2NvbXBvbmVudHMubW9kdWxlJ1xyXG4vLyBpbXBvcnQgeyBNb2R1bGVUcmFuc2xhdGVMb2FkZXIsIElNb2R1bGVUcmFuc2xhdGlvbk9wdGlvbnMgfSBmcm9tICdAbGFyc2NvbS9uZ3gtdHJhbnNsYXRlLW1vZHVsZS1sb2FkZXInXHJcblxyXG4vLyBpbXBvcnQgeyByZWdpc3RlckxvY2FsZURhdGEgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXHJcbi8vIGltcG9ydCBsb2NhbGVGciBmcm9tICdAYW5ndWxhci9jb21tb24vbG9jYWxlcy9mcidcclxuLy8gaW1wb3J0IGxvY2FsZUZyRXh0cmEgZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2xvY2FsZXMvZXh0cmEvZnInXHJcbi8vIHJlZ2lzdGVyTG9jYWxlRGF0YShsb2NhbGVGcilcclxuXHJcbi8vIEFvVCByZXF1aXJlcyBhbiBleHBvcnRlZCBmdW5jdGlvbiBmb3IgZmFjdG9yaWVzXHJcbmV4cG9ydCBmdW5jdGlvbiBIdHRwTG9hZGVyRmFjdG9yeShodHRwOiBIdHRwQ2xpZW50KSB7XHJcbiAgcmV0dXJuIG5ldyBUcmFuc2xhdGVIdHRwTG9hZGVyKGh0dHApXHJcbn1cclxuXHJcbi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9sYXJzY29tL25neC10cmFuc2xhdGUtbW9kdWxlLWxvYWRlclxyXG4vLyBleHBvcnQgZnVuY3Rpb24gbW9kdWxlSHR0cExvYWRlckZhY3RvcnkoaHR0cDogSHR0cENsaWVudCkge1xyXG4vLyAgIGNvbnN0IGJhc2VUcmFuc2xhdGVVcmwgPSAnLi9hc3NldHMvaTE4bic7XHJcblxyXG4vLyAgIGNvbnN0IG9wdGlvbnM6IElNb2R1bGVUcmFuc2xhdGlvbk9wdGlvbnMgPSB7XHJcbi8vICAgICBtb2R1bGVzOiBbXHJcbi8vICAgICAgIC8vIGZpbmFsIHVybDogLi9hc3NldHMvaTE4bi9lbi5qc29uXHJcbi8vICAgICAgIHsgYmFzZVRyYW5zbGF0ZVVybCB9LFxyXG4vLyAgICAgICAvLyBmaW5hbCB1cmw6IC4vYXNzZXRzL2kxOG4vYWRtaW4vZW4uanNvblxyXG4vLyAgICAgICB7IG1vZHVsZU5hbWU6ICdhZG1pbicsIGJhc2VUcmFuc2xhdGVVcmwgfSxcclxuLy8gICAgICAgLy8gZmluYWwgdXJsOiAuL2Fzc2V0cy9pMThuL2ZlYXR1cmUyL2VuLmpzb25cclxuLy8gICAgICAgeyBtb2R1bGVOYW1lOiAnZmVhdHVyZTInLCBiYXNlVHJhbnNsYXRlVXJsIH1cclxuLy8gICAgIF1cclxuLy8gICB9O1xyXG4vLyAgIHJldHVybiBuZXcgTW9kdWxlVHJhbnNsYXRlTG9hZGVyKGh0dHAsIG9wdGlvbnMpO1xyXG4vLyB9XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIG5vdGlmaWNhdGlvbkNvbXBvbmVudCxcclxuICAgIEZvZkNvcmVEaWFsb2dZZXNOb0NvbXBvbmVudCAgICBcclxuICBdLFxyXG4gIGltcG9ydHM6IFsgICAgXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBTaGFyZWRNb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIEh0dHBDbGllbnRNb2R1bGUsXHJcbiAgICAvLyBDb21wb25lbnRzTW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLmZvclJvb3Qoe1xyXG4gICAgICBkZWZhdWx0TGFuZ3VhZ2U6ICdmcicsXHJcbiAgICAgIGxvYWRlcjoge1xyXG4gICAgICAgIHByb3ZpZGU6IFRyYW5zbGF0ZUxvYWRlcixcclxuICAgICAgICB1c2VGYWN0b3J5OiBIdHRwTG9hZGVyRmFjdG9yeSxcclxuICAgICAgICBkZXBzOiBbSHR0cENsaWVudF1cclxuICAgICAgfVxyXG4gICAgfSlcclxuICBdLFxyXG4gIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgRm9mQ29yZURpYWxvZ1llc05vQ29tcG9uZW50XHJcbiAgXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIEZvZkxvY2Fsc3RvcmFnZVNlcnZpY2UsXHJcbiAgICAvLyB7IHByb3ZpZGU6IExPQ0FMRV9JRCwgdXNlVmFsdWU6ICdmcicgfSxcclxuICAgIHsgcHJvdmlkZTogRXJyb3JIYW5kbGVyLCB1c2VDbGFzczogZm9mRXJyb3JIYW5kbGVyIH0sXHJcbiAgICB7IHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLCB1c2VDbGFzczogRm9GSnd0SW50ZXJjZXB0b3IsIG11bHRpOiB0cnVlIH0sXHJcbiAgICB7IHByb3ZpZGU6IEhUVFBfSU5URVJDRVBUT1JTLCB1c2VDbGFzczogRm9mRXJyb3JJbnRlcmNlcHRvciwgbXVsdGk6IHRydWUgfSxcclxuICAgIEZvZkF1dGhHdWFyZCxcclxuICAgIEZvRkF1dGhTZXJ2aWNlLFxyXG4gICAgRm9GQ29yZVNlcnZpY2UsXHJcbiAgICBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgRm9mRGlhbG9nU2VydmljZSxcclxuICAgIEZvZkVycm9yU2VydmljZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogWyAgICBcclxuICAgIG5vdGlmaWNhdGlvbkNvbXBvbmVudCxcclxuICAgIEZvZkNvcmVEaWFsb2dZZXNOb0NvbXBvbmVudCxcclxuICAgIC8vIENvbXBvbmVudHNNb2R1bGVcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb0ZDb3JlTW9kdWxlIHsgfVxyXG4iXX0=
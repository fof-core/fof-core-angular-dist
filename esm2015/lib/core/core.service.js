import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, forkJoin } from 'rxjs';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "@angular/router";
import * as i3 from "./auth.service";
export class FoFCoreService {
    constructor(fofConfig, httpClient, router, foFAuthService) {
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.router = router;
        this.foFAuthService = foFAuthService;
        this._initializationReadySubject = new BehaviorSubject(false);
        this.initializationReady$ = this._initializationReadySubject.asObservable();
        this._environment = this.fofConfig.environment;
        forkJoin(this.foFAuthService.currentUser$).subscribe({
            // next: value => console.log('value', value),
            complete: () => {
                this._initializationReadySubject.next(true);
            }
        });
    }
    get environment() {
        return this._environment;
    }
}
FoFCoreService.ɵfac = function FoFCoreService_Factory(t) { return new (t || FoFCoreService)(i0.ɵɵinject(CORE_CONFIG), i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.Router), i0.ɵɵinject(i3.FoFAuthService)); };
FoFCoreService.ɵprov = i0.ɵɵdefineInjectable({ token: FoFCoreService, factory: FoFCoreService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFCoreService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: i1.HttpClient }, { type: i2.Router }, { type: i3.FoFAuthService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9jb3JlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFHbEQsT0FBTyxFQUFFLGVBQWUsRUFBYyxRQUFRLEVBQU0sTUFBTSxNQUFNLENBQUE7QUFFaEUsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7Ozs7QUFPdkQsTUFBTSxPQUFPLGNBQWM7SUFFekIsWUFDK0IsU0FBcUIsRUFDMUMsVUFBc0IsRUFDdEIsTUFBYyxFQUNkLGNBQThCO1FBSFQsY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUMxQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFjaEMsZ0NBQTJCLEdBQTZCLElBQUksZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ25GLHlCQUFvQixHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtRQWIzRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFBO1FBRTlDLFFBQVEsQ0FDTixJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FDakMsQ0FBQyxTQUFTLENBQUM7WUFDViw4Q0FBOEM7WUFDOUMsUUFBUSxFQUFFLEdBQUcsRUFBRTtnQkFDYixJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQzdDLENBQUM7U0FDRixDQUFDLENBQUE7SUFDSixDQUFDO0lBTUQsSUFBVyxXQUFXO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQTtJQUMxQixDQUFDOzs0RUExQlUsY0FBYyxjQUdmLFdBQVc7c0RBSFYsY0FBYyxXQUFkLGNBQWMsbUJBRmIsTUFBTTtrREFFUCxjQUFjO2NBSDFCLFVBQVU7ZUFBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7c0JBSUksTUFBTTt1QkFBQyxXQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnXHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgZm9ya0pvaW4sIG9mIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IElmb2ZDb25maWcsIENPUkVfQ09ORklHIH0gZnJvbSAnLi4vZm9mLWNvbmZpZydcclxuLy8gaW1wb3J0IHsgaVVzZXIgfSBmcm9tICcuLi9zaGFyZWQvdXNlci5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IEZvRkF1dGhTZXJ2aWNlIH0gZnJvbSAnLi9hdXRoLnNlcnZpY2UnXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb0ZDb3JlU2VydmljZSB7XHJcbiAgXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KENPUkVfQ09ORklHKSBwcml2YXRlIGZvZkNvbmZpZzogSWZvZkNvbmZpZyxcclxuICAgIHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIGZvRkF1dGhTZXJ2aWNlOiBGb0ZBdXRoU2VydmljZVxyXG4gICkgeyBcclxuICAgIHRoaXMuX2Vudmlyb25tZW50ID0gdGhpcy5mb2ZDb25maWcuZW52aXJvbm1lbnRcclxuICAgXHJcbiAgICBmb3JrSm9pbihcclxuICAgICAgdGhpcy5mb0ZBdXRoU2VydmljZS5jdXJyZW50VXNlciQgICAgICBcclxuICAgICkuc3Vic2NyaWJlKHtcclxuICAgICAgLy8gbmV4dDogdmFsdWUgPT4gY29uc29sZS5sb2coJ3ZhbHVlJywgdmFsdWUpLFxyXG4gICAgICBjb21wbGV0ZTogKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuX2luaXRpYWxpemF0aW9uUmVhZHlTdWJqZWN0Lm5leHQodHJ1ZSlcclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIHByaXZhdGUgX2luaXRpYWxpemF0aW9uUmVhZHlTdWJqZWN0OiBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0KGZhbHNlKVxyXG4gIHB1YmxpYyBpbml0aWFsaXphdGlvblJlYWR5JCA9IHRoaXMuX2luaXRpYWxpemF0aW9uUmVhZHlTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpXHJcblxyXG4gIHByaXZhdGUgX2Vudmlyb25tZW50OmFueVxyXG4gIHB1YmxpYyBnZXQgZW52aXJvbm1lbnQoKTogYW55IHtcclxuICAgIHJldHVybiB0aGlzLl9lbnZpcm9ubWVudFxyXG4gIH1cclxuICBcclxufVxyXG4iXX0=
import { Injectable } from '@angular/core';
import { FofCoreDialogYesNoComponent } from './core-dialog-yes-no/core-dialog-yes-no.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
export class FofDialogService {
    constructor(matDialog) {
        this.matDialog = matDialog;
    }
    openYesNo(options) {
        return new Promise((resolve, reject) => {
            let _options = options;
            let width = options.width || '300px';
            let height = options.height || undefined;
            _options.informationOnly = _options.informationOnly || false;
            const dialogRef = this.matDialog.open(FofCoreDialogYesNoComponent, {
                data: _options,
                width: width,
                height: height,
                // to have a look on, bug
                position: {
                    top: '150px'
                }
            });
            dialogRef.afterClosed()
                .toPromise()
                .then(result => {
                resolve(result);
            });
        });
    }
    openInformation(options) {
        return new Promise((resolve, reject) => {
            let _options = options;
            _options.informationOnly = true;
            _options.title = _options.title || 'fof à une information';
            _options.yesLabel = _options.yesLabel || 'ok';
            this.openYesNo(_options)
                .then(result => {
                resolve(result);
            });
        });
    }
}
FofDialogService.ɵfac = function FofDialogService_Factory(t) { return new (t || FofDialogService)(i0.ɵɵinject(i1.MatDialog)); };
FofDialogService.ɵprov = i0.ɵɵdefineInjectable({ token: FofDialogService, factory: FofDialogService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofDialogService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.MatDialog }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWRpYWxvZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9mb2YtZGlhbG9nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUMxQyxPQUFPLEVBQUUsMkJBQTJCLEVBQW1DLE1BQU0sbURBQW1ELENBQUE7OztBQU9oSSxNQUFNLE9BQU8sZ0JBQWdCO0lBRTNCLFlBQ21CLFNBQW9CO1FBQXBCLGNBQVMsR0FBVCxTQUFTLENBQVc7SUFDbkMsQ0FBQztJQUVFLFNBQVMsQ0FBQyxPQUFlO1FBQzlCLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFckMsSUFBSSxRQUFRLEdBQXlCLE9BQU8sQ0FBQTtZQUU1QyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxJQUFJLE9BQU8sQ0FBQTtZQUNwQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxJQUFJLFNBQVMsQ0FBQTtZQUV4QyxRQUFRLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxlQUFlLElBQUksS0FBSyxDQUFBO1lBRTVELE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLDJCQUEyQixFQUFFO2dCQUNqRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsTUFBTTtnQkFDZCx5QkFBeUI7Z0JBQ3pCLFFBQVEsRUFBRTtvQkFDUixHQUFHLEVBQUUsT0FBTztpQkFDYjthQUNGLENBQUMsQ0FBQTtZQUNGLFNBQVMsQ0FBQyxXQUFXLEVBQUU7aUJBQ3RCLFNBQVMsRUFBRTtpQkFDWCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ2IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ2pCLENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRU0sZUFBZSxDQUFDLE9BQXFCO1FBQzFDLE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFFckMsSUFBSSxRQUFRLEdBQXlCLE9BQU8sQ0FBQTtZQUM1QyxRQUFRLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQTtZQUMvQixRQUFRLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLElBQUksdUJBQXVCLENBQUE7WUFDMUQsUUFBUSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQTtZQUU3QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztpQkFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUNiLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUNqQixDQUFDLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQzs7Z0ZBOUNVLGdCQUFnQjt3REFBaEIsZ0JBQWdCLFdBQWhCLGdCQUFnQixtQkFGZixNQUFNO2tEQUVQLGdCQUFnQjtjQUg1QixVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgRm9mQ29yZURpYWxvZ1llc05vQ29tcG9uZW50LCBpSW50ZXJuYWwsIGlZZXNObywgaUluZm9ybWF0aW9uIH0gZnJvbSAnLi9jb3JlLWRpYWxvZy15ZXMtbm8vY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgTWF0RGlhbG9nLCBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZydcclxuXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZEaWFsb2dTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHJlYWRvbmx5IG1hdERpYWxvZzogTWF0RGlhbG9nXHJcbiAgKSB7IH1cclxuXHJcbiAgcHVibGljIG9wZW5ZZXNObyhvcHRpb25zOiBpWWVzTm8pIHtcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcblxyXG4gICAgICBsZXQgX29wdGlvbnM6IGlJbnRlcm5hbCA9IDxpSW50ZXJuYWw+b3B0aW9uc1xyXG5cclxuICAgICAgbGV0IHdpZHRoID0gb3B0aW9ucy53aWR0aCB8fCAnMzAwcHgnIFxyXG4gICAgICBsZXQgaGVpZ2h0ID0gb3B0aW9ucy5oZWlnaHQgfHwgdW5kZWZpbmVkXHJcblxyXG4gICAgICBfb3B0aW9ucy5pbmZvcm1hdGlvbk9ubHkgPSBfb3B0aW9ucy5pbmZvcm1hdGlvbk9ubHkgfHwgZmFsc2VcclxuXHJcbiAgICAgIGNvbnN0IGRpYWxvZ1JlZiA9IHRoaXMubWF0RGlhbG9nLm9wZW4oRm9mQ29yZURpYWxvZ1llc05vQ29tcG9uZW50LCB7XHJcbiAgICAgICAgZGF0YTogX29wdGlvbnMsICAgICAgICBcclxuICAgICAgICB3aWR0aDogd2lkdGgsXHJcbiAgICAgICAgaGVpZ2h0OiBoZWlnaHQsXHJcbiAgICAgICAgLy8gdG8gaGF2ZSBhIGxvb2sgb24sIGJ1Z1xyXG4gICAgICAgIHBvc2l0aW9uOiB7XHJcbiAgICAgICAgICB0b3A6ICcxNTBweCdcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpXHJcbiAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgIHJlc29sdmUocmVzdWx0KVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvcGVuSW5mb3JtYXRpb24ob3B0aW9uczogaUluZm9ybWF0aW9uKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgbGV0IF9vcHRpb25zOiBpSW50ZXJuYWwgPSA8aUludGVybmFsPm9wdGlvbnNcclxuICAgICAgX29wdGlvbnMuaW5mb3JtYXRpb25Pbmx5ID0gdHJ1ZVxyXG4gICAgICBfb3B0aW9ucy50aXRsZSA9IF9vcHRpb25zLnRpdGxlIHx8ICdmb2Ygw6AgdW5lIGluZm9ybWF0aW9uJ1xyXG4gICAgICBfb3B0aW9ucy55ZXNMYWJlbCA9IF9vcHRpb25zLnllc0xhYmVsIHx8ICdvaydcclxuXHJcbiAgICAgIHRoaXMub3Blblllc05vKF9vcHRpb25zKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgIHJlc29sdmUocmVzdWx0KVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9XHJcbn1cclxuIl19
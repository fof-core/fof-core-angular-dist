import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./notification/notification.service";
export class FofErrorService {
    constructor(fofNotificationService) {
        this.fofNotificationService = fofNotificationService;
    }
    /** Only for the core service, the error should be already cleaned */
    cleanError(errorToManage) {
        let errorToReturn = errorToManage;
        let message;
        let isConstraintError = false;
        // console.log('errorToManage', errorToManage)
        if (errorToManage.error instanceof ErrorEvent) {
            // client-side error
            console.log('toDo: manage error');
            console.log('errorToManage', errorToManage);
            return errorToManage;
        }
        else {
            // HTTP or server-side error
            if (errorToManage.error) {
                if (errorToManage.error instanceof Object) {
                    if (errorToManage.error.fofCoreException) {
                        if (errorToManage.error.message instanceof Object) {
                            // it's an http rest error
                            const _error = errorToManage.error.message;
                            if (_error.error) {
                                errorToManage.error.statusText = _error.error;
                                errorToManage.error.message = _error.message;
                            }
                        }
                        else {
                            message = errorToManage.error.message;
                            if (message.search('constraint') > -1) {
                                isConstraintError = true;
                            }
                        }
                        return Object.assign({ isConstraintError }, errorToManage.error);
                    }
                    else {
                        if (errorToManage.status == 0) {
                            console.error('FOF-UNKNOWN-ERROR', errorToManage.error);
                            errorToManage.error = errorToManage.message;
                            errorToManage.message = 'Impossible to contact the server';
                        }
                    }
                }
                else {
                    // the error to manage is comming from the httpClient
                    return errorToManage;
                }
            }
        }
        return errorToReturn;
    }
    errorManage(errorToManage) {
        let message = errorToManage.message;
        if (errorToManage.error) {
            message = message + `<br>${errorToManage.error}`;
        }
        if (errorToManage.status) {
            switch (errorToManage.status) {
                case 401:
                case 403:
                    message = `Vous n'avez pas le droit d'accéder à ces ressources`;
                    break;
                default:
                    message = `Oups, nous avons une erreur<br>
          <small>${message}</small><br>`;
                    break;
            }
        }
        this.fofNotificationService.error(message, { mustDisappearAfter: -1 });
    }
}
FofErrorService.ɵfac = function FofErrorService_Factory(t) { return new (t || FofErrorService)(i0.ɵɵinject(i1.FofNotificationService)); };
FofErrorService.ɵprov = i0.ɵɵdefineInjectable({ token: FofErrorService, factory: FofErrorService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofErrorService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.FofNotificationService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWVycm9yLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2ZvZi1lcnJvci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUE7OztBQVkxQyxNQUFNLE9BQU8sZUFBZTtJQUUxQixZQUNVLHNCQUE4QztRQUE5QywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO0lBQ3BELENBQUM7SUFFTCxxRUFBcUU7SUFDckUsVUFBVSxDQUFFLGFBQWtCO1FBQzVCLElBQUksYUFBYSxHQUF1QixhQUFhLENBQUE7UUFDckQsSUFBSSxPQUFlLENBQUE7UUFDbkIsSUFBSSxpQkFBaUIsR0FBWSxLQUFLLENBQUE7UUFFdEMsOENBQThDO1FBRTlDLElBQUksYUFBYSxDQUFDLEtBQUssWUFBWSxVQUFVLEVBQUU7WUFDN0Msb0JBQW9CO1lBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtZQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxhQUFhLENBQUMsQ0FBQTtZQUMzQyxPQUFPLGFBQWEsQ0FBQTtTQUNyQjthQUFNO1lBQ0wsNEJBQTRCO1lBQzVCLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtnQkFDdkIsSUFBSSxhQUFhLENBQUMsS0FBSyxZQUFZLE1BQU0sRUFBRTtvQkFDekMsSUFBSSxhQUFhLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFO3dCQUN4QyxJQUFJLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxZQUFZLE1BQU0sRUFBRTs0QkFDbEQsMEJBQTBCOzRCQUMxQixNQUFNLE1BQU0sR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQTs0QkFDMUMsSUFBSSxNQUFNLENBQUMsS0FBSyxFQUFFO2dDQUNqQixhQUFhLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFBO2dDQUM3QyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFBOzZCQUM1Qzt5QkFDRDs2QkFBTTs0QkFDTCxPQUFPLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUE7NEJBQ3JDLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRztnQ0FDdEMsaUJBQWlCLEdBQUcsSUFBSSxDQUFBOzZCQUN6Qjt5QkFDRjt3QkFDRCx1QkFDRSxpQkFBaUIsSUFDZCxhQUFhLENBQUMsS0FBSyxFQUN2QjtxQkFDRjt5QkFBTTt3QkFDTCxJQUFJLGFBQWEsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFOzRCQUM3QixPQUFPLENBQUMsS0FBSyxDQUFDLG1CQUFtQixFQUFFLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQTs0QkFDdkQsYUFBYSxDQUFDLEtBQUssR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFBOzRCQUMzQyxhQUFhLENBQUMsT0FBTyxHQUFHLGtDQUFrQyxDQUFBO3lCQUMzRDtxQkFDRjtpQkFDRjtxQkFBTTtvQkFDTCxxREFBcUQ7b0JBQ3JELE9BQU8sYUFBYSxDQUFBO2lCQUNyQjthQUNGO1NBQ0Y7UUFFRCxPQUFPLGFBQWEsQ0FBQTtJQUN0QixDQUFDO0lBRUQsV0FBVyxDQUFDLGFBQWlDO1FBQzNDLElBQUksT0FBTyxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUE7UUFDbkMsSUFBSSxhQUFhLENBQUMsS0FBSyxFQUFFO1lBQ3ZCLE9BQU8sR0FBRyxPQUFPLEdBQUcsT0FBTyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUE7U0FDakQ7UUFDRCxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDeEIsUUFBUSxhQUFhLENBQUMsTUFBTSxFQUFFO2dCQUM1QixLQUFLLEdBQUcsQ0FBQztnQkFDVCxLQUFLLEdBQUc7b0JBQ04sT0FBTyxHQUFHLHFEQUFxRCxDQUFBO29CQUMvRCxNQUFLO2dCQUNQO29CQUNFLE9BQU8sR0FBRzttQkFDRCxPQUFPLGNBQWMsQ0FBQTtvQkFDOUIsTUFBSzthQUNSO1NBQ0Y7UUFDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxFQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQTtJQUN0RSxDQUFDOzs4RUE1RVUsZUFBZTt1REFBZixlQUFlLFdBQWYsZUFBZSxtQkFGZCxNQUFNO2tEQUVQLGVBQWU7Y0FIM0IsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IGlGb2ZIdHRwRXhjZXB0aW9uIH0gZnJvbSAnLi9jb3JlLmludGVyZmFjZSdcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBlcnJvciB9IGZyb20gJ3Byb3RyYWN0b3InXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIGlGb2ZDbGVhbkV4Y2VwdGlvbiBleHRlbmRzIGlGb2ZIdHRwRXhjZXB0aW9uIHtcclxuICBpc0NvbnN0cmFpbnRFcnJvcjogYm9vbGVhblxyXG59XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZFcnJvclNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZVxyXG4gICkgeyB9XHJcblxyXG4gIC8qKiBPbmx5IGZvciB0aGUgY29yZSBzZXJ2aWNlLCB0aGUgZXJyb3Igc2hvdWxkIGJlIGFscmVhZHkgY2xlYW5lZCAqL1xyXG4gIGNsZWFuRXJyb3IgKGVycm9yVG9NYW5hZ2U6IGFueSk6IGlGb2ZDbGVhbkV4Y2VwdGlvbiB7XHJcbiAgICBsZXQgZXJyb3JUb1JldHVybjogaUZvZkNsZWFuRXhjZXB0aW9uID0gZXJyb3JUb01hbmFnZVxyXG4gICAgbGV0IG1lc3NhZ2U6IHN0cmluZ1xyXG4gICAgbGV0IGlzQ29uc3RyYWludEVycm9yOiBib29sZWFuID0gZmFsc2VcclxuICAgIFxyXG4gICAgLy8gY29uc29sZS5sb2coJ2Vycm9yVG9NYW5hZ2UnLCBlcnJvclRvTWFuYWdlKVxyXG5cclxuICAgIGlmIChlcnJvclRvTWFuYWdlLmVycm9yIGluc3RhbmNlb2YgRXJyb3JFdmVudCkge1xyXG4gICAgICAvLyBjbGllbnQtc2lkZSBlcnJvclxyXG4gICAgICBjb25zb2xlLmxvZygndG9EbzogbWFuYWdlIGVycm9yJylcclxuICAgICAgY29uc29sZS5sb2coJ2Vycm9yVG9NYW5hZ2UnLCBlcnJvclRvTWFuYWdlKVxyXG4gICAgICByZXR1cm4gZXJyb3JUb01hbmFnZVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gSFRUUCBvciBzZXJ2ZXItc2lkZSBlcnJvclxyXG4gICAgICBpZiAoZXJyb3JUb01hbmFnZS5lcnJvcikge1xyXG4gICAgICAgIGlmIChlcnJvclRvTWFuYWdlLmVycm9yIGluc3RhbmNlb2YgT2JqZWN0KSB7XHJcbiAgICAgICAgICBpZiAoZXJyb3JUb01hbmFnZS5lcnJvci5mb2ZDb3JlRXhjZXB0aW9uKSB7XHJcbiAgICAgICAgICAgIGlmIChlcnJvclRvTWFuYWdlLmVycm9yLm1lc3NhZ2UgaW5zdGFuY2VvZiBPYmplY3QpIHtcclxuICAgICAgICAgICAgIC8vIGl0J3MgYW4gaHR0cCByZXN0IGVycm9yXHJcbiAgICAgICAgICAgICBjb25zdCBfZXJyb3IgPSBlcnJvclRvTWFuYWdlLmVycm9yLm1lc3NhZ2VcclxuICAgICAgICAgICAgIGlmIChfZXJyb3IuZXJyb3IpIHsgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICBlcnJvclRvTWFuYWdlLmVycm9yLnN0YXR1c1RleHQgPSBfZXJyb3IuZXJyb3JcclxuICAgICAgICAgICAgICBlcnJvclRvTWFuYWdlLmVycm9yLm1lc3NhZ2UgPSBfZXJyb3IubWVzc2FnZVxyXG4gICAgICAgICAgICAgfSAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgbWVzc2FnZSA9IGVycm9yVG9NYW5hZ2UuZXJyb3IubWVzc2FnZVxyXG4gICAgICAgICAgICAgIGlmIChtZXNzYWdlLnNlYXJjaCgnY29uc3RyYWludCcpID4gLTEgKSB7XHJcbiAgICAgICAgICAgICAgICBpc0NvbnN0cmFpbnRFcnJvciA9IHRydWVcclxuICAgICAgICAgICAgICB9ICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9ICAgICAgICAgIFxyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgIGlzQ29uc3RyYWludEVycm9yLFxyXG4gICAgICAgICAgICAgIC4uLmVycm9yVG9NYW5hZ2UuZXJyb3JcclxuICAgICAgICAgICAgfSAgICAgICAgICAgIFxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKGVycm9yVG9NYW5hZ2Uuc3RhdHVzID09IDApIHsgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0ZPRi1VTktOT1dOLUVSUk9SJywgZXJyb3JUb01hbmFnZS5lcnJvcilcclxuICAgICAgICAgICAgICBlcnJvclRvTWFuYWdlLmVycm9yID0gZXJyb3JUb01hbmFnZS5tZXNzYWdlXHJcbiAgICAgICAgICAgICAgZXJyb3JUb01hbmFnZS5tZXNzYWdlID0gJ0ltcG9zc2libGUgdG8gY29udGFjdCB0aGUgc2VydmVyJ1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIC8vIHRoZSBlcnJvciB0byBtYW5hZ2UgaXMgY29tbWluZyBmcm9tIHRoZSBodHRwQ2xpZW50XHJcbiAgICAgICAgICByZXR1cm4gZXJyb3JUb01hbmFnZVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSAgXHJcblxyXG4gICAgcmV0dXJuIGVycm9yVG9SZXR1cm5cclxuICB9XHJcblxyXG4gIGVycm9yTWFuYWdlKGVycm9yVG9NYW5hZ2U6IGlGb2ZDbGVhbkV4Y2VwdGlvbikge1xyXG4gICAgbGV0IG1lc3NhZ2UgPSBlcnJvclRvTWFuYWdlLm1lc3NhZ2VcclxuICAgIGlmIChlcnJvclRvTWFuYWdlLmVycm9yKSB7XHJcbiAgICAgIG1lc3NhZ2UgPSBtZXNzYWdlICsgYDxicj4ke2Vycm9yVG9NYW5hZ2UuZXJyb3J9YFxyXG4gICAgfVxyXG4gICAgaWYgKGVycm9yVG9NYW5hZ2Uuc3RhdHVzKSB7XHJcbiAgICAgIHN3aXRjaCAoZXJyb3JUb01hbmFnZS5zdGF0dXMpIHtcclxuICAgICAgICBjYXNlIDQwMTpcclxuICAgICAgICBjYXNlIDQwMzpcclxuICAgICAgICAgIG1lc3NhZ2UgPSBgVm91cyBuJ2F2ZXogcGFzIGxlIGRyb2l0IGQnYWNjw6lkZXIgw6AgY2VzIHJlc3NvdXJjZXNgXHJcbiAgICAgICAgICBicmVha1xyXG4gICAgICAgIGRlZmF1bHQ6ICAgICAgICAgIFxyXG4gICAgICAgICAgbWVzc2FnZSA9IGBPdXBzLCBub3VzIGF2b25zIHVuZSBlcnJldXI8YnI+XHJcbiAgICAgICAgICA8c21hbGw+JHttZXNzYWdlfTwvc21hbGw+PGJyPmAgICAgICAgICAgXHJcbiAgICAgICAgICBicmVha1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IobWVzc2FnZSwge211c3REaXNhcHBlYXJBZnRlcjogLTF9KVxyXG4gIH1cclxufVxyXG4iXX0=
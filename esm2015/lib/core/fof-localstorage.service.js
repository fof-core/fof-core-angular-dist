import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
export class FofLocalstorageService {
    constructor(fofConfig) {
        this.fofConfig = fofConfig;
        this.appShortName = `${this.fofConfig.appName.technical.toLocaleUpperCase()}-`;
    }
    setItem(key, value) {
        localStorage.setItem(`${this.appShortName}${key}`, JSON.stringify(value));
    }
    getItem(key) {
        return JSON.parse(localStorage.getItem(`${this.appShortName}${key}`));
    }
    removeItem(key) {
        localStorage.removeItem(`${this.appShortName}${key}`);
    }
}
FofLocalstorageService.ɵfac = function FofLocalstorageService_Factory(t) { return new (t || FofLocalstorageService)(i0.ɵɵinject(CORE_CONFIG)); };
FofLocalstorageService.ɵprov = i0.ɵɵdefineInjectable({ token: FofLocalstorageService, factory: FofLocalstorageService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofLocalstorageService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWxvY2Fsc3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9mb2YtbG9jYWxzdG9yYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDbEQsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7QUFLdkQsTUFBTSxPQUFPLHNCQUFzQjtJQUVqQyxZQUMrQixTQUFxQjtRQUFyQixjQUFTLEdBQVQsU0FBUyxDQUFZO1FBRWxELElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsR0FBRyxDQUFBO0lBQ2hGLENBQUM7SUFJRCxPQUFPLENBQUMsR0FBVyxFQUFFLEtBQVU7UUFDN0IsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO0lBQzNFLENBQUM7SUFFRCxPQUFPLENBQUMsR0FBVztRQUNqQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFBO0lBQ3ZFLENBQUM7SUFFRCxVQUFVLENBQUMsR0FBVztRQUNwQixZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFBO0lBQ3ZELENBQUM7OzRGQXBCVSxzQkFBc0IsY0FHdkIsV0FBVzs4REFIVixzQkFBc0IsV0FBdEIsc0JBQXNCLG1CQUZyQixNQUFNO2tEQUVQLHNCQUFzQjtjQUhsQyxVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7O3NCQUlJLE1BQU07dUJBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IElmb2ZDb25maWcsIENPUkVfQ09ORklHIH0gZnJvbSAnLi4vZm9mLWNvbmZpZydcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZkxvY2Fsc3RvcmFnZVNlcnZpY2Uge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIEBJbmplY3QoQ09SRV9DT05GSUcpIHByaXZhdGUgZm9mQ29uZmlnOiBJZm9mQ29uZmlnXHJcbiAgKSB7IFxyXG4gICAgdGhpcy5hcHBTaG9ydE5hbWUgPSBgJHt0aGlzLmZvZkNvbmZpZy5hcHBOYW1lLnRlY2huaWNhbC50b0xvY2FsZVVwcGVyQ2FzZSgpfS1gXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGFwcFNob3J0TmFtZTogc3RyaW5nXHJcblxyXG4gIHNldEl0ZW0oa2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkpIHtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKGAke3RoaXMuYXBwU2hvcnROYW1lfSR7a2V5fWAsIEpTT04uc3RyaW5naWZ5KHZhbHVlKSlcclxuICB9XHJcblxyXG4gIGdldEl0ZW0oa2V5OiBzdHJpbmcpIHtcclxuICAgIHJldHVybiBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKGAke3RoaXMuYXBwU2hvcnROYW1lfSR7a2V5fWApKVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlSXRlbShrZXk6IHN0cmluZykge1xyXG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oYCR7dGhpcy5hcHBTaG9ydE5hbWV9JHtrZXl9YClcclxuICB9XHJcbn1cclxuIl19
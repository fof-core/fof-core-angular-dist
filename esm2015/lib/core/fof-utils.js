import { FormGroup, FormControl } from '@angular/forms';
export const fofUtilsForm = {
    validateAllFields: (formGroup) => {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
};
export const fofError = {
    clean: (errorToManage) => {
        let error;
        let message;
        let isConstraintError = false;
        if (errorToManage.error) {
            error = errorToManage.error;
            if (error.fofCoreException) {
                message = error.exceptionDetail.message;
                if (message.search('constraint') > -1) {
                    isConstraintError = true;
                }
                return Object.assign({ isConstraintError }, error);
            }
        }
    },
    cleanAndMange: (errorToManage) => {
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXV0aWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9mb2YtdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQU92RCxNQUFNLENBQUMsTUFBTSxZQUFZLEdBQUc7SUFDMUIsaUJBQWlCLEVBQUMsQ0FBQyxTQUFvQixFQUFFLEVBQUU7UUFDekMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQzlDLE1BQU0sT0FBTyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsSUFBSSxPQUFPLFlBQVksV0FBVyxFQUFFO2dCQUNsQyxPQUFPLENBQUMsYUFBYSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7YUFDM0M7aUJBQU0sSUFBSSxPQUFPLFlBQVksU0FBUyxFQUFFO2dCQUN2QyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDckM7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7Q0FDRixDQUFBO0FBRUQsTUFBTSxDQUFDLE1BQU0sUUFBUSxHQUFHO0lBQ3RCLEtBQUssRUFBQyxDQUFDLGFBQWtCLEVBQXNCLEVBQUU7UUFDL0MsSUFBSSxLQUFLLENBQUE7UUFDVCxJQUFJLE9BQWUsQ0FBQTtRQUNuQixJQUFJLGlCQUFpQixHQUFZLEtBQUssQ0FBQTtRQUN0QyxJQUFJLGFBQWEsQ0FBQyxLQUFLLEVBQUU7WUFDdkIsS0FBSyxHQUFzQixhQUFhLENBQUMsS0FBSyxDQUFBO1lBQzlDLElBQUksS0FBSyxDQUFDLGdCQUFnQixFQUFFO2dCQUMxQixPQUFPLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUE7Z0JBQ3ZDLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRztvQkFDdEMsaUJBQWlCLEdBQUcsSUFBSSxDQUFBO2lCQUN6QjtnQkFDRCx1QkFDRSxpQkFBaUIsSUFDZCxLQUFLLEVBQ1Q7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUNELGFBQWEsRUFBQyxDQUFDLGFBQWtCLEVBQUUsRUFBRTtJQUVyQyxDQUFDO0NBQ0YsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3JtcydcclxuaW1wb3J0IHsgaUZvZkh0dHBFeGNlcHRpb24gfSBmcm9tICcuL2NvcmUuaW50ZXJmYWNlJ1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBpRm9mQ2xlYW5FeGNlcHRpb24gZXh0ZW5kcyBpRm9mSHR0cEV4Y2VwdGlvbiB7XHJcbiAgaXNDb25zdHJhaW50RXJyb3I6IGJvb2xlYW5cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGZvZlV0aWxzRm9ybSA9IHtcclxuICB2YWxpZGF0ZUFsbEZpZWxkczooZm9ybUdyb3VwOiBGb3JtR3JvdXApID0+IHsgICAgICAgICBcclxuICAgIE9iamVjdC5rZXlzKGZvcm1Hcm91cC5jb250cm9scykuZm9yRWFjaChmaWVsZCA9PiB7ICBcclxuICAgICAgY29uc3QgY29udHJvbCA9IGZvcm1Hcm91cC5nZXQoZmllbGQpOyAgICAgICAgICAgICBcclxuICAgICAgaWYgKGNvbnRyb2wgaW5zdGFuY2VvZiBGb3JtQ29udHJvbCkgeyAgICAgICAgICAgICBcclxuICAgICAgICBjb250cm9sLm1hcmtBc1RvdWNoZWQoeyBvbmx5U2VsZjogdHJ1ZSB9KTtcclxuICAgICAgfSBlbHNlIGlmIChjb250cm9sIGluc3RhbmNlb2YgRm9ybUdyb3VwKSB7ICAgICAgICBcclxuICAgICAgICB0aGlzLnZhbGlkYXRlQWxsRm9ybUZpZWxkcyhjb250cm9sKTsgICAgICAgICAgICBcclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjb25zdCBmb2ZFcnJvciA9IHtcclxuICBjbGVhbjooZXJyb3JUb01hbmFnZTogYW55KTogaUZvZkNsZWFuRXhjZXB0aW9uID0+IHtcclxuICAgIGxldCBlcnJvclxyXG4gICAgbGV0IG1lc3NhZ2U6IHN0cmluZ1xyXG4gICAgbGV0IGlzQ29uc3RyYWludEVycm9yOiBib29sZWFuID0gZmFsc2VcclxuICAgIGlmIChlcnJvclRvTWFuYWdlLmVycm9yKSB7XHJcbiAgICAgIGVycm9yID0gPGlGb2ZIdHRwRXhjZXB0aW9uPmVycm9yVG9NYW5hZ2UuZXJyb3JcclxuICAgICAgaWYgKGVycm9yLmZvZkNvcmVFeGNlcHRpb24pIHtcclxuICAgICAgICBtZXNzYWdlID0gZXJyb3IuZXhjZXB0aW9uRGV0YWlsLm1lc3NhZ2VcclxuICAgICAgICBpZiAobWVzc2FnZS5zZWFyY2goJ2NvbnN0cmFpbnQnKSA+IC0xICkge1xyXG4gICAgICAgICAgaXNDb25zdHJhaW50RXJyb3IgPSB0cnVlXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICBpc0NvbnN0cmFpbnRFcnJvcixcclxuICAgICAgICAgIC4uLmVycm9yXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfSxcclxuICBjbGVhbkFuZE1hbmdlOihlcnJvclRvTWFuYWdlOiBhbnkpID0+IHtcclxuICAgIFxyXG4gIH1cclxufVxyXG5cclxuIl19
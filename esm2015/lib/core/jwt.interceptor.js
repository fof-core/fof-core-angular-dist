import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "./auth.service";
export class FoFJwtInterceptor {
    constructor(foFAuthService, fofConfig) {
        this.foFAuthService = foFAuthService;
        this.fofConfig = fofConfig;
        this.environment = this.fofConfig.environment;
    }
    intercept(request, next) {
        // add auth header with jwt if user is logged in and request is to api url    
        const currentUser = this.foFAuthService.currentUser;
        const isLoggedIn = currentUser && currentUser.accessToken;
        const isApiUrl = request.url.startsWith(this.environment.apiPath);
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${currentUser.accessToken}`
                }
            });
        }
        return next.handle(request);
    }
}
FoFJwtInterceptor.ɵfac = function FoFJwtInterceptor_Factory(t) { return new (t || FoFJwtInterceptor)(i0.ɵɵinject(i1.FoFAuthService), i0.ɵɵinject(CORE_CONFIG)); };
FoFJwtInterceptor.ɵprov = i0.ɵɵdefineInjectable({ token: FoFJwtInterceptor, factory: FoFJwtInterceptor.ɵfac });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFJwtInterceptor, [{
        type: Injectable
    }], function () { return [{ type: i1.FoFAuthService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9qd3QuaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFLbEQsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7O0FBR3ZELE1BQU0sT0FBTyxpQkFBaUI7SUFHNUIsWUFDVSxjQUE4QixFQUNULFNBQXFCO1FBRDFDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUNULGNBQVMsR0FBVCxTQUFTLENBQVk7UUFFbEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQTtJQUMvQyxDQUFDO0lBRUQsU0FBUyxDQUFDLE9BQXlCLEVBQUUsSUFBaUI7UUFDcEQsOEVBQThFO1FBQzlFLE1BQU0sV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFBO1FBQ25ELE1BQU0sVUFBVSxHQUFHLFdBQVcsSUFBSSxXQUFXLENBQUMsV0FBVyxDQUFBO1FBQ3pELE1BQU0sUUFBUSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUE7UUFDakUsSUFBSSxVQUFVLElBQUksUUFBUSxFQUFFO1lBQ3hCLE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUNwQixVQUFVLEVBQUU7b0JBQ1YsYUFBYSxFQUFFLFVBQVUsV0FBVyxDQUFDLFdBQVcsRUFBRTtpQkFDbkQ7YUFDSixDQUFDLENBQUE7U0FDTDtRQUVELE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQTtJQUM3QixDQUFDOztrRkF4QlUsaUJBQWlCLDhDQUtsQixXQUFXO3lEQUxWLGlCQUFpQixXQUFqQixpQkFBaUI7a0RBQWpCLGlCQUFpQjtjQUQ3QixVQUFVOztzQkFNTixNQUFNO3VCQUFDLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCwgSHR0cEludGVyY2VwdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnXHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJ1xyXG4vLyBpbXBvcnQgeyBpVXNlciB9IGZyb20gJy4uL3NoYXJlZC91c2VyLmludGVyZmFjZSdcclxuaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UgfSBmcm9tICcuL2F1dGguc2VydmljZSdcclxuaW1wb3J0IHsgSWZvZkNvbmZpZywgQ09SRV9DT05GSUcgfSBmcm9tICcuLi9mb2YtY29uZmlnJ1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRm9GSnd0SW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIHByaXZhdGUgZW52aXJvbm1lbnQ6YW55XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmb0ZBdXRoU2VydmljZTogRm9GQXV0aFNlcnZpY2UsXHJcbiAgICBASW5qZWN0KENPUkVfQ09ORklHKSBwcml2YXRlIGZvZkNvbmZpZzogSWZvZkNvbmZpZ1xyXG4gICkgeyBcclxuICAgIHRoaXMuZW52aXJvbm1lbnQgPSB0aGlzLmZvZkNvbmZpZy5lbnZpcm9ubWVudFxyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG4gICAgLy8gYWRkIGF1dGggaGVhZGVyIHdpdGggand0IGlmIHVzZXIgaXMgbG9nZ2VkIGluIGFuZCByZXF1ZXN0IGlzIHRvIGFwaSB1cmwgICAgXHJcbiAgICBjb25zdCBjdXJyZW50VXNlciA9IHRoaXMuZm9GQXV0aFNlcnZpY2UuY3VycmVudFVzZXJcclxuICAgIGNvbnN0IGlzTG9nZ2VkSW4gPSBjdXJyZW50VXNlciAmJiBjdXJyZW50VXNlci5hY2Nlc3NUb2tlblxyXG4gICAgY29uc3QgaXNBcGlVcmwgPSByZXF1ZXN0LnVybC5zdGFydHNXaXRoKHRoaXMuZW52aXJvbm1lbnQuYXBpUGF0aClcclxuICAgIGlmIChpc0xvZ2dlZEluICYmIGlzQXBpVXJsKSB7XHJcbiAgICAgICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xyXG4gICAgICAgICAgICBzZXRIZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2N1cnJlbnRVc2VyLmFjY2Vzc1Rva2VufWBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpXHJcbiAgfVxyXG59Il19
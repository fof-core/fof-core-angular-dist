import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common'
// import { ScrollingModule } from '@angular/cdk/scrolling'
// import { MatAutocompleteModule } from '@angular/material/autocomplete'
// import { MatBadgeModule } from '@angular/material/badge'
// import { MatBottomSheetModule } from '@angular/material/bottom-sheet'
import { MatButtonModule } from '@angular/material/button';
// import { MatButtonToggleModule } from '@angular/material/button-toggle'
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
// import { MatNativeDateModule, MatRippleModule } from '@angular/material/core'
// import { MatDatepickerModule } from '@angular/material/datepicker'
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
// import { MatGridListModule } from '@angular/material/grid-list'
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
// import { MatMenuModule } from '@angular/material/menu'
import { MatPaginatorModule } from '@angular/material/paginator';
// import { MatProgressBarModule } from '@angular/material/progress-bar'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
// import { MatRadioModule } from '@angular/material/radio'
// import { MatSelectModule } from '@angular/material/select'
// import { MatSidenavModule } from '@angular/material/sidenav'
// import { MatSlideToggleModule } from '@angular/material/slide-toggle'
// import { MatSliderModule } from '@angular/material/slider'
// import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatSortModule } from '@angular/material/sort';
// import { MatStepperModule } from '@angular/material/stepper'
import { MatTableModule } from '@angular/material/table';
// import { MatTabsModule } from '@angular/material/tabs'
// import { MatToolbarModule } from '@angular/material/toolbar'
// import { MatTooltipModule } from '@angular/material/tooltip'
import { MatTreeModule } from '@angular/material/tree';
import * as i0 from "@angular/core";
export class MaterialModule {
}
MaterialModule.ɵmod = i0.ɵɵdefineNgModule({ type: MaterialModule });
MaterialModule.ɵinj = i0.ɵɵdefineInjector({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [[
            // MatAutocompleteModule,
            // MatBadgeModule,
            // MatBottomSheetModule,
            MatButtonModule,
            // MatButtonToggleModule,
            MatCardModule,
            MatCheckboxModule,
            MatChipsModule,
            // MatDatepickerModule,
            MatDialogModule,
            MatDividerModule,
            MatExpansionModule,
            // MatGridListModule,
            MatIconModule,
            MatInputModule,
            MatListModule,
            // MatMenuModule,
            // MatNativeDateModule,
            MatPaginatorModule,
            // MatProgressBarModule,
            MatProgressSpinnerModule,
            // MatRadioModule,
            // MatRippleModule,
            // MatSelectModule,
            // MatSidenavModule,
            // MatSliderModule,
            // MatSlideToggleModule,
            // MatSnackBarModule,
            MatSortModule,
            // MatStepperModule,
            MatTableModule,
            // MatTabsModule,
            // MatToolbarModule,
            // MatTooltipModule,
            MatTreeModule
        ],
        // MatAutocompleteModule,
        // MatBadgeModule,
        // MatBottomSheetModule,
        MatButtonModule,
        // MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        // MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        // MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        // MatMenuModule,
        // MatNativeDateModule,
        MatPaginatorModule,
        // MatProgressBarModule,
        MatProgressSpinnerModule,
        // MatRadioModule,
        // MatRippleModule,
        // MatSelectModule,
        // MatSidenavModule,
        // MatSliderModule,
        // MatSlideToggleModule,
        // MatSnackBarModule,
        MatSortModule,
        // MatStepperModule,
        MatTableModule,
        // MatTabsModule,
        // MatToolbarModule,
        // MatTooltipModule,
        MatTreeModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(MaterialModule, { imports: [
        // MatAutocompleteModule,
        // MatBadgeModule,
        // MatBottomSheetModule,
        MatButtonModule,
        // MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        // MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        // MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        // MatMenuModule,
        // MatNativeDateModule,
        MatPaginatorModule,
        // MatProgressBarModule,
        MatProgressSpinnerModule,
        // MatRadioModule,
        // MatRippleModule,
        // MatSelectModule,
        // MatSidenavModule,
        // MatSliderModule,
        // MatSlideToggleModule,
        // MatSnackBarModule,
        MatSortModule,
        // MatStepperModule,
        MatTableModule,
        // MatTabsModule,
        // MatToolbarModule,
        // MatTooltipModule,
        MatTreeModule], exports: [
        // MatAutocompleteModule,
        // MatBadgeModule,
        // MatBottomSheetModule,
        MatButtonModule,
        // MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        // MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        // MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        // MatMenuModule,
        // MatNativeDateModule,
        MatPaginatorModule,
        // MatProgressBarModule,
        MatProgressSpinnerModule,
        // MatRadioModule,
        // MatRippleModule,
        // MatSelectModule,
        // MatSidenavModule,
        // MatSliderModule,
        // MatSlideToggleModule,
        // MatSnackBarModule,
        MatSortModule,
        // MatStepperModule,
        MatTableModule,
        // MatTabsModule,
        // MatToolbarModule,
        // MatTooltipModule,
        MatTreeModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(MaterialModule, [{
        type: NgModule,
        args: [{
                imports: [
                    // MatAutocompleteModule,
                    // MatBadgeModule,
                    // MatBottomSheetModule,
                    MatButtonModule,
                    // MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    // MatDatepickerModule,
                    MatDialogModule,
                    MatDividerModule,
                    MatExpansionModule,
                    // MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    // MatMenuModule,
                    // MatNativeDateModule,
                    MatPaginatorModule,
                    // MatProgressBarModule,
                    MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    // MatSelectModule,
                    // MatSidenavModule,
                    // MatSliderModule,
                    // MatSlideToggleModule,
                    // MatSnackBarModule,
                    MatSortModule,
                    // MatStepperModule,
                    MatTableModule,
                    // MatTabsModule,
                    // MatToolbarModule,
                    // MatTooltipModule,
                    MatTreeModule
                ],
                exports: [
                    // MatAutocompleteModule,
                    // MatBadgeModule,
                    // MatBottomSheetModule,
                    MatButtonModule,
                    // MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    // MatDatepickerModule,
                    MatDialogModule,
                    MatDividerModule,
                    MatExpansionModule,
                    // MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    // MatMenuModule,
                    // MatNativeDateModule,
                    MatPaginatorModule,
                    // MatProgressBarModule,
                    MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    // MatSelectModule,
                    // MatSidenavModule,
                    // MatSliderModule,
                    // MatSlideToggleModule,
                    // MatSnackBarModule,
                    MatSortModule,
                    // MatStepperModule,
                    MatTableModule,
                    // MatTabsModule,
                    // MatToolbarModule,
                    // MatTooltipModule,
                    MatTreeModule
                ],
                declarations: []
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9tYXRlcmlhbC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUN4QyxpREFBaUQ7QUFDakQsMkRBQTJEO0FBQzNELHlFQUF5RTtBQUN6RSwyREFBMkQ7QUFDM0Qsd0VBQXdFO0FBQ3hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQTtBQUMxRCwwRUFBMEU7QUFDMUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBQ3RELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRCQUE0QixDQUFBO0FBQzlELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxnRkFBZ0Y7QUFDaEYscUVBQXFFO0FBQ3JFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQTtBQUMxRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQTtBQUM1RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQTtBQUNoRSxrRUFBa0U7QUFDbEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBRXRELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUE7QUFDdEQseURBQXlEO0FBQ3pELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDZCQUE2QixDQUFBO0FBQ2hFLHdFQUF3RTtBQUN4RSxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQTtBQUM3RSwyREFBMkQ7QUFDM0QsNkRBQTZEO0FBQzdELCtEQUErRDtBQUMvRCx3RUFBd0U7QUFDeEUsNkRBQTZEO0FBQzdELGtFQUFrRTtBQUNsRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUE7QUFDdEQsK0RBQStEO0FBQy9ELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCx5REFBeUQ7QUFDekQsK0RBQStEO0FBQy9ELCtEQUErRDtBQUMvRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUE7O0FBK0V0RCxNQUFNLE9BQU8sY0FBYzs7a0RBQWQsY0FBYzsyR0FBZCxjQUFjLGtCQTVFaEI7WUFDUCx5QkFBeUI7WUFDekIsa0JBQWtCO1lBQ2xCLHdCQUF3QjtZQUN4QixlQUFlO1lBQ2YseUJBQXlCO1lBQ3pCLGFBQWE7WUFDYixpQkFBaUI7WUFDakIsY0FBYztZQUNkLHVCQUF1QjtZQUN2QixlQUFlO1lBQ2YsZ0JBQWdCO1lBQ2hCLGtCQUFrQjtZQUNsQixxQkFBcUI7WUFDckIsYUFBYTtZQUNiLGNBQWM7WUFDZCxhQUFhO1lBQ2IsaUJBQWlCO1lBQ2pCLHVCQUF1QjtZQUN2QixrQkFBa0I7WUFDbEIsd0JBQXdCO1lBQ3hCLHdCQUF3QjtZQUN4QixrQkFBa0I7WUFDbEIsbUJBQW1CO1lBQ25CLG1CQUFtQjtZQUNuQixvQkFBb0I7WUFDcEIsbUJBQW1CO1lBQ25CLHdCQUF3QjtZQUN4QixxQkFBcUI7WUFDckIsYUFBYTtZQUNiLG9CQUFvQjtZQUNwQixjQUFjO1lBQ2QsaUJBQWlCO1lBQ2pCLG9CQUFvQjtZQUNwQixvQkFBb0I7WUFDcEIsYUFBYTtTQUNkO1FBRUMseUJBQXlCO1FBQ3pCLGtCQUFrQjtRQUNsQix3QkFBd0I7UUFDeEIsZUFBZTtRQUNmLHlCQUF5QjtRQUN6QixhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLGNBQWM7UUFDZCx1QkFBdUI7UUFDdkIsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIscUJBQXFCO1FBQ3JCLGFBQWE7UUFDYixjQUFjO1FBQ2QsYUFBYTtRQUNiLGlCQUFpQjtRQUNqQix1QkFBdUI7UUFDdkIsa0JBQWtCO1FBQ2xCLHdCQUF3QjtRQUN4Qix3QkFBd0I7UUFDeEIsa0JBQWtCO1FBQ2xCLG1CQUFtQjtRQUNuQixtQkFBbUI7UUFDbkIsb0JBQW9CO1FBQ3BCLG1CQUFtQjtRQUNuQix3QkFBd0I7UUFDeEIscUJBQXFCO1FBQ3JCLGFBQWE7UUFDYixvQkFBb0I7UUFDcEIsY0FBYztRQUNkLGlCQUFpQjtRQUNqQixvQkFBb0I7UUFDcEIsb0JBQW9CO1FBQ3BCLGFBQWE7d0ZBSUosY0FBYztRQTNFdkIseUJBQXlCO1FBQ3pCLGtCQUFrQjtRQUNsQix3QkFBd0I7UUFDeEIsZUFBZTtRQUNmLHlCQUF5QjtRQUN6QixhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLGNBQWM7UUFDZCx1QkFBdUI7UUFDdkIsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixrQkFBa0I7UUFDbEIscUJBQXFCO1FBQ3JCLGFBQWE7UUFDYixjQUFjO1FBQ2QsYUFBYTtRQUNiLGlCQUFpQjtRQUNqQix1QkFBdUI7UUFDdkIsa0JBQWtCO1FBQ2xCLHdCQUF3QjtRQUN4Qix3QkFBd0I7UUFDeEIsa0JBQWtCO1FBQ2xCLG1CQUFtQjtRQUNuQixtQkFBbUI7UUFDbkIsb0JBQW9CO1FBQ3BCLG1CQUFtQjtRQUNuQix3QkFBd0I7UUFDeEIscUJBQXFCO1FBQ3JCLGFBQWE7UUFDYixvQkFBb0I7UUFDcEIsY0FBYztRQUNkLGlCQUFpQjtRQUNqQixvQkFBb0I7UUFDcEIsb0JBQW9CO1FBQ3BCLGFBQWE7UUFHYix5QkFBeUI7UUFDekIsa0JBQWtCO1FBQ2xCLHdCQUF3QjtRQUN4QixlQUFlO1FBQ2YseUJBQXlCO1FBQ3pCLGFBQWE7UUFDYixpQkFBaUI7UUFDakIsY0FBYztRQUNkLHVCQUF1QjtRQUN2QixlQUFlO1FBQ2YsZ0JBQWdCO1FBQ2hCLGtCQUFrQjtRQUNsQixxQkFBcUI7UUFDckIsYUFBYTtRQUNiLGNBQWM7UUFDZCxhQUFhO1FBQ2IsaUJBQWlCO1FBQ2pCLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsd0JBQXdCO1FBQ3hCLHdCQUF3QjtRQUN4QixrQkFBa0I7UUFDbEIsbUJBQW1CO1FBQ25CLG1CQUFtQjtRQUNuQixvQkFBb0I7UUFDcEIsbUJBQW1CO1FBQ25CLHdCQUF3QjtRQUN4QixxQkFBcUI7UUFDckIsYUFBYTtRQUNiLG9CQUFvQjtRQUNwQixjQUFjO1FBQ2QsaUJBQWlCO1FBQ2pCLG9CQUFvQjtRQUNwQixvQkFBb0I7UUFDcEIsYUFBYTtrREFJSixjQUFjO2NBN0UxQixRQUFRO2VBQUM7Z0JBQ1IsT0FBTyxFQUFFO29CQUNQLHlCQUF5QjtvQkFDekIsa0JBQWtCO29CQUNsQix3QkFBd0I7b0JBQ3hCLGVBQWU7b0JBQ2YseUJBQXlCO29CQUN6QixhQUFhO29CQUNiLGlCQUFpQjtvQkFDakIsY0FBYztvQkFDZCx1QkFBdUI7b0JBQ3ZCLGVBQWU7b0JBQ2YsZ0JBQWdCO29CQUNoQixrQkFBa0I7b0JBQ2xCLHFCQUFxQjtvQkFDckIsYUFBYTtvQkFDYixjQUFjO29CQUNkLGFBQWE7b0JBQ2IsaUJBQWlCO29CQUNqQix1QkFBdUI7b0JBQ3ZCLGtCQUFrQjtvQkFDbEIsd0JBQXdCO29CQUN4Qix3QkFBd0I7b0JBQ3hCLGtCQUFrQjtvQkFDbEIsbUJBQW1CO29CQUNuQixtQkFBbUI7b0JBQ25CLG9CQUFvQjtvQkFDcEIsbUJBQW1CO29CQUNuQix3QkFBd0I7b0JBQ3hCLHFCQUFxQjtvQkFDckIsYUFBYTtvQkFDYixvQkFBb0I7b0JBQ3BCLGNBQWM7b0JBQ2QsaUJBQWlCO29CQUNqQixvQkFBb0I7b0JBQ3BCLG9CQUFvQjtvQkFDcEIsYUFBYTtpQkFDZDtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AseUJBQXlCO29CQUN6QixrQkFBa0I7b0JBQ2xCLHdCQUF3QjtvQkFDeEIsZUFBZTtvQkFDZix5QkFBeUI7b0JBQ3pCLGFBQWE7b0JBQ2IsaUJBQWlCO29CQUNqQixjQUFjO29CQUNkLHVCQUF1QjtvQkFDdkIsZUFBZTtvQkFDZixnQkFBZ0I7b0JBQ2hCLGtCQUFrQjtvQkFDbEIscUJBQXFCO29CQUNyQixhQUFhO29CQUNiLGNBQWM7b0JBQ2QsYUFBYTtvQkFDYixpQkFBaUI7b0JBQ2pCLHVCQUF1QjtvQkFDdkIsa0JBQWtCO29CQUNsQix3QkFBd0I7b0JBQ3hCLHdCQUF3QjtvQkFDeEIsa0JBQWtCO29CQUNsQixtQkFBbUI7b0JBQ25CLG1CQUFtQjtvQkFDbkIsb0JBQW9CO29CQUNwQixtQkFBbUI7b0JBQ25CLHdCQUF3QjtvQkFDeEIscUJBQXFCO29CQUNyQixhQUFhO29CQUNiLG9CQUFvQjtvQkFDcEIsY0FBYztvQkFDZCxpQkFBaUI7b0JBQ2pCLG9CQUFvQjtvQkFDcEIsb0JBQW9CO29CQUNwQixhQUFhO2lCQUNkO2dCQUNELFlBQVksRUFBRSxFQUFFO2FBQ2pCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG4vLyBpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXHJcbi8vIGltcG9ydCB7IFNjcm9sbGluZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9zY3JvbGxpbmcnXHJcbi8vIGltcG9ydCB7IE1hdEF1dG9jb21wbGV0ZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZSdcclxuLy8gaW1wb3J0IHsgTWF0QmFkZ2VNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9iYWRnZSdcclxuLy8gaW1wb3J0IHsgTWF0Qm90dG9tU2hlZXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9ib3R0b20tc2hlZXQnXHJcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2J1dHRvbidcclxuLy8gaW1wb3J0IHsgTWF0QnV0dG9uVG9nZ2xlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYnV0dG9uLXRvZ2dsZSdcclxuaW1wb3J0IHsgTWF0Q2FyZE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NhcmQnXHJcbmltcG9ydCB7IE1hdENoZWNrYm94TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hlY2tib3gnXHJcbmltcG9ydCB7IE1hdENoaXBzTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2hpcHMnXHJcbi8vIGltcG9ydCB7IE1hdE5hdGl2ZURhdGVNb2R1bGUsIE1hdFJpcHBsZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NvcmUnXHJcbi8vIGltcG9ydCB7IE1hdERhdGVwaWNrZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kYXRlcGlja2VyJ1xyXG5pbXBvcnQgeyBNYXREaWFsb2dNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnXHJcbmltcG9ydCB7IE1hdERpdmlkZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaXZpZGVyJ1xyXG5pbXBvcnQgeyBNYXRFeHBhbnNpb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9leHBhbnNpb24nXHJcbi8vIGltcG9ydCB7IE1hdEdyaWRMaXN0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZ3JpZC1saXN0J1xyXG5pbXBvcnQgeyBNYXRJY29uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaWNvbidcclxuXHJcbmltcG9ydCB7IE1hdElucHV0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaW5wdXQnXHJcbmltcG9ydCB7IE1hdExpc3RNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9saXN0J1xyXG4vLyBpbXBvcnQgeyBNYXRNZW51TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvbWVudSdcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJ1xyXG4vLyBpbXBvcnQgeyBNYXRQcm9ncmVzc0Jhck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3Byb2dyZXNzLWJhcidcclxuaW1wb3J0IHsgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcHJvZ3Jlc3Mtc3Bpbm5lcidcclxuLy8gaW1wb3J0IHsgTWF0UmFkaW9Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9yYWRpbydcclxuLy8gaW1wb3J0IHsgTWF0U2VsZWN0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2VsZWN0J1xyXG4vLyBpbXBvcnQgeyBNYXRTaWRlbmF2TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc2lkZW5hdidcclxuLy8gaW1wb3J0IHsgTWF0U2xpZGVUb2dnbGVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbGlkZS10b2dnbGUnXHJcbi8vIGltcG9ydCB7IE1hdFNsaWRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NsaWRlcidcclxuLy8gaW1wb3J0IHsgTWF0U25hY2tCYXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXInXHJcbmltcG9ydCB7IE1hdFNvcnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0J1xyXG4vLyBpbXBvcnQgeyBNYXRTdGVwcGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc3RlcHBlcidcclxuaW1wb3J0IHsgTWF0VGFibGVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90YWJsZSdcclxuLy8gaW1wb3J0IHsgTWF0VGFic01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RhYnMnXHJcbi8vIGltcG9ydCB7IE1hdFRvb2xiYXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC90b29sYmFyJ1xyXG4vLyBpbXBvcnQgeyBNYXRUb29sdGlwTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdG9vbHRpcCdcclxuaW1wb3J0IHsgTWF0VHJlZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3RyZWUnXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtcclxuICAgIC8vIE1hdEF1dG9jb21wbGV0ZU1vZHVsZSxcclxuICAgIC8vIE1hdEJhZGdlTW9kdWxlLFxyXG4gICAgLy8gTWF0Qm90dG9tU2hlZXRNb2R1bGUsXHJcbiAgICBNYXRCdXR0b25Nb2R1bGUsXHJcbiAgICAvLyBNYXRCdXR0b25Ub2dnbGVNb2R1bGUsXHJcbiAgICBNYXRDYXJkTW9kdWxlLFxyXG4gICAgTWF0Q2hlY2tib3hNb2R1bGUsXHJcbiAgICBNYXRDaGlwc01vZHVsZSxcclxuICAgIC8vIE1hdERhdGVwaWNrZXJNb2R1bGUsXHJcbiAgICBNYXREaWFsb2dNb2R1bGUsXHJcbiAgICBNYXREaXZpZGVyTW9kdWxlLFxyXG4gICAgTWF0RXhwYW5zaW9uTW9kdWxlLFxyXG4gICAgLy8gTWF0R3JpZExpc3RNb2R1bGUsXHJcbiAgICBNYXRJY29uTW9kdWxlLFxyXG4gICAgTWF0SW5wdXRNb2R1bGUsXHJcbiAgICBNYXRMaXN0TW9kdWxlLFxyXG4gICAgLy8gTWF0TWVudU1vZHVsZSxcclxuICAgIC8vIE1hdE5hdGl2ZURhdGVNb2R1bGUsXHJcbiAgICBNYXRQYWdpbmF0b3JNb2R1bGUsXHJcbiAgICAvLyBNYXRQcm9ncmVzc0Jhck1vZHVsZSxcclxuICAgIE1hdFByb2dyZXNzU3Bpbm5lck1vZHVsZSxcclxuICAgIC8vIE1hdFJhZGlvTW9kdWxlLFxyXG4gICAgLy8gTWF0UmlwcGxlTW9kdWxlLFxyXG4gICAgLy8gTWF0U2VsZWN0TW9kdWxlLFxyXG4gICAgLy8gTWF0U2lkZW5hdk1vZHVsZSxcclxuICAgIC8vIE1hdFNsaWRlck1vZHVsZSxcclxuICAgIC8vIE1hdFNsaWRlVG9nZ2xlTW9kdWxlLFxyXG4gICAgLy8gTWF0U25hY2tCYXJNb2R1bGUsXHJcbiAgICBNYXRTb3J0TW9kdWxlLFxyXG4gICAgLy8gTWF0U3RlcHBlck1vZHVsZSxcclxuICAgIE1hdFRhYmxlTW9kdWxlLFxyXG4gICAgLy8gTWF0VGFic01vZHVsZSxcclxuICAgIC8vIE1hdFRvb2xiYXJNb2R1bGUsXHJcbiAgICAvLyBNYXRUb29sdGlwTW9kdWxlLFxyXG4gICAgTWF0VHJlZU1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgLy8gTWF0QXV0b2NvbXBsZXRlTW9kdWxlLFxyXG4gICAgLy8gTWF0QmFkZ2VNb2R1bGUsXHJcbiAgICAvLyBNYXRCb3R0b21TaGVldE1vZHVsZSxcclxuICAgIE1hdEJ1dHRvbk1vZHVsZSxcclxuICAgIC8vIE1hdEJ1dHRvblRvZ2dsZU1vZHVsZSxcclxuICAgIE1hdENhcmRNb2R1bGUsXHJcbiAgICBNYXRDaGVja2JveE1vZHVsZSxcclxuICAgIE1hdENoaXBzTW9kdWxlLFxyXG4gICAgLy8gTWF0RGF0ZXBpY2tlck1vZHVsZSxcclxuICAgIE1hdERpYWxvZ01vZHVsZSxcclxuICAgIE1hdERpdmlkZXJNb2R1bGUsXHJcbiAgICBNYXRFeHBhbnNpb25Nb2R1bGUsXHJcbiAgICAvLyBNYXRHcmlkTGlzdE1vZHVsZSxcclxuICAgIE1hdEljb25Nb2R1bGUsXHJcbiAgICBNYXRJbnB1dE1vZHVsZSxcclxuICAgIE1hdExpc3RNb2R1bGUsXHJcbiAgICAvLyBNYXRNZW51TW9kdWxlLFxyXG4gICAgLy8gTWF0TmF0aXZlRGF0ZU1vZHVsZSxcclxuICAgIE1hdFBhZ2luYXRvck1vZHVsZSxcclxuICAgIC8vIE1hdFByb2dyZXNzQmFyTW9kdWxlLFxyXG4gICAgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlLFxyXG4gICAgLy8gTWF0UmFkaW9Nb2R1bGUsXHJcbiAgICAvLyBNYXRSaXBwbGVNb2R1bGUsXHJcbiAgICAvLyBNYXRTZWxlY3RNb2R1bGUsXHJcbiAgICAvLyBNYXRTaWRlbmF2TW9kdWxlLFxyXG4gICAgLy8gTWF0U2xpZGVyTW9kdWxlLFxyXG4gICAgLy8gTWF0U2xpZGVUb2dnbGVNb2R1bGUsXHJcbiAgICAvLyBNYXRTbmFja0Jhck1vZHVsZSxcclxuICAgIE1hdFNvcnRNb2R1bGUsXHJcbiAgICAvLyBNYXRTdGVwcGVyTW9kdWxlLFxyXG4gICAgTWF0VGFibGVNb2R1bGUsXHJcbiAgICAvLyBNYXRUYWJzTW9kdWxlLFxyXG4gICAgLy8gTWF0VG9vbGJhck1vZHVsZSxcclxuICAgIC8vIE1hdFRvb2x0aXBNb2R1bGUsXHJcbiAgICBNYXRUcmVlTW9kdWxlXHJcbiAgXSxcclxuICBkZWNsYXJhdGlvbnM6IFtdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYXRlcmlhbE1vZHVsZSB7IH1cclxuXHJcbiJdfQ==
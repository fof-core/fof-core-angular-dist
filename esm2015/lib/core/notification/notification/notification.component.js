// Angular
import { Component } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
// Models
import { NotificationType } from '../notification-type';
import * as i0 from "@angular/core";
import * as i1 from "../notification.service";
import * as i2 from "@angular/common";
function notificationComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r123 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵelementStart(1, "button", 2);
    i0.ɵɵlistener("click", function notificationComponent_div_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r123); const notification_r121 = ctx.$implicit; const ctx_r122 = i0.ɵɵnextContext(); return ctx_r122.uiAction.removenotification(notification_r121); });
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "\u00D7");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelement(4, "div", 3);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const notification_r121 = ctx.$implicit;
    const ctx_r119 = i0.ɵɵnextContext();
    i0.ɵɵclassMapInterpolate1("", ctx_r119.uiAction.cssClass(notification_r121), " notification-dismissable");
    i0.ɵɵproperty("@inOut", undefined);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("innerHTML", notification_r121.message, i0.ɵɵsanitizeHtml);
} }
function notificationComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵelement(1, "span");
    i0.ɵɵelementEnd();
} }
// how to
// https://angular.io/guide/animations
// https://github.com/PointInside/ng2-toastr/blob/master/src/toast-container.component.ts
export class notificationComponent {
    constructor(fofNotificationService, ngZone) {
        this.fofNotificationService = fofNotificationService;
        this.ngZone = ngZone;
        // All private variables
        this.priVar = {
            getnotificationSub: undefined,
            savedNotificationSub: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            notifications: [],
            savedActive: false
        };
        // All actions shared with UI 
        this.uiAction = {
            removenotification: (notification) => {
                this.uiVar.notifications = this.uiVar.notifications.filter(x => x !== notification);
            },
            cssClass: (notification) => {
                if (!notification) {
                    return;
                }
                // return css class based on notification type
                switch (notification.type) {
                    case NotificationType.Success:
                        return 'notification notification-success';
                    case NotificationType.Error:
                        return 'notification notification-danger';
                    case NotificationType.Info:
                        return 'notification notification-info';
                    case NotificationType.Warning:
                        return 'notification notification-warning';
                }
            }
        };
    }
    // Angular events
    ngOnInit() {
        const template = this;
        this.priVar.getnotificationSub = this.fofNotificationService.getnotification()
            .subscribe((notification) => {
            if (!notification) {
                // clear notifications when an empty notification is received
                this.uiVar.notifications = [];
                return;
            }
            if (notification.mustDisappearAfter) {
                setTimeout(() => {
                    template.uiAction.removenotification(notification);
                }, notification.mustDisappearAfter);
            }
            // ensure the notif will be displayed even if received from a promise pipe
            this.ngZone.run(() => {
                // push or unshift depend if there are on the top or bottom
                // this.notifications.push(notification)
                this.uiVar.notifications.unshift(notification);
            });
        });
        this.fofNotificationService.savedNotification
            .subscribe((saved) => {
            if (!saved) {
                return;
            }
            this.uiVar.savedActive = true;
            setTimeout(() => {
                this.uiVar.savedActive = false;
            }, 300);
        });
    }
    ngOnDestroy() {
        if (this.priVar.getnotificationSub) {
            this.priVar.getnotificationSub.unsubscribe();
        }
        if (this.priVar.savedNotificationSub) {
            this.priVar.savedNotificationSub.unsubscribe();
        }
    }
}
notificationComponent.ɵfac = function notificationComponent_Factory(t) { return new (t || notificationComponent)(i0.ɵɵdirectiveInject(i1.FofNotificationService), i0.ɵɵdirectiveInject(i0.NgZone)); };
notificationComponent.ɵcmp = i0.ɵɵdefineComponent({ type: notificationComponent, selectors: [["fof-notification"]], decls: 3, vars: 2, consts: [[3, "class", 4, "ngFor", "ngForOf"], ["class", "saved-notification", 4, "ngIf"], ["type", "button", "data-dismiss", "notification", 1, "close", 3, "click"], [3, "innerHTML"], [1, "saved-notification"]], template: function notificationComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div");
        i0.ɵɵtemplate(1, notificationComponent_div_1_Template, 5, 5, "div", 0);
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(2, notificationComponent_div_2_Template, 2, 0, "div", 1);
    } if (rf & 2) {
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx.uiVar.notifications);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.savedActive);
    } }, directives: [i2.NgForOf, i2.NgIf], styles: ["[_nghost-%COMP%]{position:fixed;z-index:999999;bottom:12px;right:12px;width:300px}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]{padding:.5rem;margin-top:.5rem;position:relative}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]:hover   .close[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]{position:absolute;border:none;background-color:transparent;right:-5px;top:-13px;font-size:25px;cursor:pointer;opacity:.3}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]{position:fixed;z-index:9999999;bottom:15px;right:15px;width:25px;height:25px;border-radius:50px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{position:absolute;left:-3px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after, [_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{box-sizing:border-box;content:\"\";position:absolute}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{border-style:solid;border-width:7px 2px 1px;border-radius:1px;height:16px;left:8px;top:5px;width:16px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after{border-style:solid;border-width:1px 1px 1px 4px;height:5px;left:13px;top:5px;width:7px}"], data: { animation: [
            trigger('inOut', [
                transition(':enter', [
                    style({
                        opacity: 0,
                        transform: 'translateX(100%)'
                    }),
                    animate('0.2s ease-in')
                ]),
                transition(':leave', [
                    animate('0.2s 10ms ease-out', style({
                        opacity: 0,
                        transform: 'translateX(100%)'
                    }))
                ])
            ])
        ] } });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(notificationComponent, [{
        type: Component,
        args: [{
                selector: 'fof-notification',
                templateUrl: './notification.component.html',
                styleUrls: ['./notification.component.scss'],
                animations: [
                    trigger('inOut', [
                        transition(':enter', [
                            style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }),
                            animate('0.2s ease-in')
                        ]),
                        transition(':leave', [
                            animate('0.2s 10ms ease-out', style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }))
                        ])
                    ])
                ]
            }]
    }], function () { return [{ type: i1.FofNotificationService }, { type: i0.NgZone }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50LnRzIiwibGliL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsVUFBVTtBQUNWLE9BQU8sRUFBQyxTQUFTLEVBQTRCLE1BQU0sZUFBZSxDQUFBO0FBQ2xFLE9BQU8sRUFBQyxPQUFPLEVBQVMsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQTtBQUc5RSxTQUFTO0FBQ1QsT0FBTyxFQUFnQixnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFBOzs7Ozs7SUNMbkUsMkJBR0U7SUFBQSxpQ0FDRTtJQURNLCtNQUFTLHVEQUF5QyxJQUFDO0lBQ3pELDRCQUFPO0lBQUEsc0JBQU87SUFBQSxpQkFBTztJQUN2QixpQkFBUztJQUNULHlCQUE4QztJQUNoRCxpQkFBTTs7OztJQUxBLHlHQUFzRTtJQUR4RSxrQ0FBUTtJQUtMLGVBQWtDO0lBQWxDLHdFQUFrQzs7O0lBSzNDLDhCQUNFO0lBQUEsdUJBQWE7SUFDZixpQkFBTTs7QURITixTQUFTO0FBQ1Qsc0NBQXNDO0FBQ3RDLHlGQUF5RjtBQXdCekYsTUFBTSxPQUFPLHFCQUFxQjtJQUNoQyxZQUNVLHNCQUE4QyxFQUM5QyxNQUFjO1FBRGQsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5QyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBR3hCLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixrQkFBa0IsRUFBZ0IsU0FBUztZQUMzQyxvQkFBb0IsRUFBZ0IsU0FBUztTQUM5QyxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRyxFQUNsQixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLGFBQWEsRUFBc0IsRUFBRTtZQUNyQyxXQUFXLEVBQUUsS0FBSztTQUNuQixDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRztZQUNoQixrQkFBa0IsRUFBQyxDQUFDLFlBQTBCLEVBQUUsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLFlBQVksQ0FBQyxDQUFBO1lBQ3JGLENBQUM7WUFDRCxRQUFRLEVBQUMsQ0FBQyxZQUEwQixFQUFFLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxZQUFZLEVBQUU7b0JBQ2YsT0FBTTtpQkFDVDtnQkFFRCw4Q0FBOEM7Z0JBQzlDLFFBQVEsWUFBWSxDQUFDLElBQUksRUFBRTtvQkFDekIsS0FBSyxnQkFBZ0IsQ0FBQyxPQUFPO3dCQUN6QixPQUFPLG1DQUFtQyxDQUFBO29CQUM5QyxLQUFLLGdCQUFnQixDQUFDLEtBQUs7d0JBQ3ZCLE9BQU8sa0NBQWtDLENBQUE7b0JBQzdDLEtBQUssZ0JBQWdCLENBQUMsSUFBSTt3QkFDdEIsT0FBTyxnQ0FBZ0MsQ0FBQTtvQkFDM0MsS0FBSyxnQkFBZ0IsQ0FBQyxPQUFPO3dCQUN6QixPQUFPLG1DQUFtQyxDQUFBO2lCQUMvQztZQUNILENBQUM7U0FDRixDQUFBO0lBckNHLENBQUM7SUFzQ0wsaUJBQWlCO0lBQ2pCLFFBQVE7UUFDTixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUE7UUFFckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxFQUFFO2FBQzdFLFNBQVMsQ0FBQyxDQUFDLFlBQTBCLEVBQUUsRUFBRTtZQUN4QyxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNmLDZEQUE2RDtnQkFDN0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFBO2dCQUM3QixPQUFNO2FBQ1Q7WUFFRCxJQUFJLFlBQVksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDbkMsVUFBVSxDQUFDLEdBQUcsRUFBRTtvQkFDZCxRQUFRLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFBO2dCQUNwRCxDQUFDLEVBQUUsWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUE7YUFDcEM7WUFFRCwwRUFBMEU7WUFDMUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFO2dCQUNuQiwyREFBMkQ7Z0JBQzNELHdDQUF3QztnQkFDeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFBO1lBQ2hELENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsc0JBQXNCLENBQUMsaUJBQWlCO2FBQzVDLFNBQVMsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ25CLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQUUsT0FBTTthQUFFO1lBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtZQUM3QixVQUFVLENBQUMsR0FBRyxFQUFFO2dCQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQTtZQUNoQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUE7UUFDVCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFDRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFO1lBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUFFO1FBQ3BGLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRTtZQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsV0FBVyxFQUFFLENBQUE7U0FBRTtJQUMxRixDQUFDOzswRkFoRlUscUJBQXFCOzBEQUFyQixxQkFBcUI7UUNyQ2xDLDJCQUNFO1FBQUEsc0VBR0U7UUFLSixpQkFBTTtRQUdOLHNFQUNFOztRQVpLLGVBQWdEO1FBQWhELGlEQUFnRDtRQVdsRCxlQUF5QjtRQUF6Qiw0Q0FBeUI7bzVDRE9oQjtZQUNWLE9BQU8sQ0FBQyxPQUFPLEVBQUU7Z0JBQ2YsVUFBVSxDQUFDLFFBQVEsRUFBRTtvQkFDbkIsS0FBSyxDQUFDO3dCQUNKLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFNBQVMsRUFBRSxrQkFBa0I7cUJBQzlCLENBQUM7b0JBQ0YsT0FBTyxDQUFDLGNBQWMsQ0FBQztpQkFDeEIsQ0FBQztnQkFDRixVQUFVLENBQUMsUUFBUSxFQUFFO29CQUNuQixPQUFPLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDO3dCQUNsQyxPQUFPLEVBQUUsQ0FBQzt3QkFDVixTQUFTLEVBQUUsa0JBQWtCO3FCQUM5QixDQUFDLENBQUM7aUJBQ0osQ0FBQzthQUNILENBQUM7U0FDSDtrREFFVSxxQkFBcUI7Y0F0QmpDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixXQUFXLEVBQUUsK0JBQStCO2dCQUM1QyxTQUFTLEVBQUUsQ0FBQywrQkFBK0IsQ0FBQztnQkFDNUMsVUFBVSxFQUFFO29CQUNWLE9BQU8sQ0FBQyxPQUFPLEVBQUU7d0JBQ2YsVUFBVSxDQUFDLFFBQVEsRUFBRTs0QkFDbkIsS0FBSyxDQUFDO2dDQUNKLE9BQU8sRUFBRSxDQUFDO2dDQUNWLFNBQVMsRUFBRSxrQkFBa0I7NkJBQzlCLENBQUM7NEJBQ0YsT0FBTyxDQUFDLGNBQWMsQ0FBQzt5QkFDeEIsQ0FBQzt3QkFDRixVQUFVLENBQUMsUUFBUSxFQUFFOzRCQUNuQixPQUFPLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDO2dDQUNsQyxPQUFPLEVBQUUsQ0FBQztnQ0FDVixTQUFTLEVBQUUsa0JBQWtCOzZCQUM5QixDQUFDLENBQUM7eUJBQ0osQ0FBQztxQkFDSCxDQUFDO2lCQUNIO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBBbmd1bGFyXHJcbmltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgTmdab25lfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQge2FuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlcn0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucydcclxuLy8gU2VydmljZXNcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG4vLyBNb2RlbHNcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uLCBOb3RpZmljYXRpb25UeXBlIH0gZnJvbSAnLi4vbm90aWZpY2F0aW9uLXR5cGUnXHJcbi8vIENvbXBvbmVudHNcclxuLy8gVmVuZG9ycyAmIHV0aWxzXHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnXHJcblxyXG4vLyBob3cgdG9cclxuLy8gaHR0cHM6Ly9hbmd1bGFyLmlvL2d1aWRlL2FuaW1hdGlvbnNcclxuLy8gaHR0cHM6Ly9naXRodWIuY29tL1BvaW50SW5zaWRlL25nMi10b2FzdHIvYmxvYi9tYXN0ZXIvc3JjL3RvYXN0LWNvbnRhaW5lci5jb21wb25lbnQudHNcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZm9mLW5vdGlmaWNhdGlvbicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgYW5pbWF0aW9uczogW1xyXG4gICAgdHJpZ2dlcignaW5PdXQnLCBbICAgICBcclxuICAgICAgdHJhbnNpdGlvbignOmVudGVyJywgW1xyXG4gICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgIG9wYWNpdHk6IDAsXHJcbiAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDEwMCUpJ1xyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGFuaW1hdGUoJzAuMnMgZWFzZS1pbicpXHJcbiAgICAgIF0pLFxyXG4gICAgICB0cmFuc2l0aW9uKCc6bGVhdmUnLCBbXHJcbiAgICAgICAgYW5pbWF0ZSgnMC4ycyAxMG1zIGVhc2Utb3V0Jywgc3R5bGUoe1xyXG4gICAgICAgICAgb3BhY2l0eTogMCxcclxuICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMTAwJSknXHJcbiAgICAgICAgfSkpXHJcbiAgICAgIF0pXHJcbiAgICBdKSAgICBcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBub3RpZmljYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG5nWm9uZTogTmdab25lXHJcbiAgKSB7IH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICBnZXRub3RpZmljYXRpb25TdWI6IDxTdWJzY3JpcHRpb24+dW5kZWZpbmVkLFxyXG4gICAgc2F2ZWROb3RpZmljYXRpb25TdWI6IDxTdWJzY3JpcHRpb24+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgbm90aWZpY2F0aW9uczo8QXJyYXk8Tm90aWZpY2F0aW9uPj5bXSxcclxuICAgIHNhdmVkQWN0aXZlOiBmYWxzZVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgICByZW1vdmVub3RpZmljYXRpb246KG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uKSA9PiB7XHJcbiAgICAgIHRoaXMudWlWYXIubm90aWZpY2F0aW9ucyA9IHRoaXMudWlWYXIubm90aWZpY2F0aW9ucy5maWx0ZXIoeCA9PiB4ICE9PSBub3RpZmljYXRpb24pXHJcbiAgICB9LFxyXG4gICAgY3NzQ2xhc3M6KG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uKSA9PiB7XHJcbiAgICAgIGlmICghbm90aWZpY2F0aW9uKSB7XHJcbiAgICAgICAgICByZXR1cm5cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gcmV0dXJuIGNzcyBjbGFzcyBiYXNlZCBvbiBub3RpZmljYXRpb24gdHlwZVxyXG4gICAgICBzd2l0Y2ggKG5vdGlmaWNhdGlvbi50eXBlKSB7XHJcbiAgICAgICAgY2FzZSBOb3RpZmljYXRpb25UeXBlLlN1Y2Nlc3M6XHJcbiAgICAgICAgICAgIHJldHVybiAnbm90aWZpY2F0aW9uIG5vdGlmaWNhdGlvbi1zdWNjZXNzJ1xyXG4gICAgICAgIGNhc2UgTm90aWZpY2F0aW9uVHlwZS5FcnJvcjpcclxuICAgICAgICAgICAgcmV0dXJuICdub3RpZmljYXRpb24gbm90aWZpY2F0aW9uLWRhbmdlcidcclxuICAgICAgICBjYXNlIE5vdGlmaWNhdGlvblR5cGUuSW5mbzpcclxuICAgICAgICAgICAgcmV0dXJuICdub3RpZmljYXRpb24gbm90aWZpY2F0aW9uLWluZm8nXHJcbiAgICAgICAgY2FzZSBOb3RpZmljYXRpb25UeXBlLldhcm5pbmc6XHJcbiAgICAgICAgICAgIHJldHVybiAnbm90aWZpY2F0aW9uIG5vdGlmaWNhdGlvbi13YXJuaW5nJ1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICBjb25zdCB0ZW1wbGF0ZSA9IHRoaXNcclxuXHJcbiAgICB0aGlzLnByaVZhci5nZXRub3RpZmljYXRpb25TdWIgPSB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuZ2V0bm90aWZpY2F0aW9uKClcclxuICAgIC5zdWJzY3JpYmUoKG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmICghbm90aWZpY2F0aW9uKSB7XHJcbiAgICAgICAgICAvLyBjbGVhciBub3RpZmljYXRpb25zIHdoZW4gYW4gZW1wdHkgbm90aWZpY2F0aW9uIGlzIHJlY2VpdmVkXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLm5vdGlmaWNhdGlvbnMgPSBbXVxyXG4gICAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChub3RpZmljYXRpb24ubXVzdERpc2FwcGVhckFmdGVyKSB7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICB0ZW1wbGF0ZS51aUFjdGlvbi5yZW1vdmVub3RpZmljYXRpb24obm90aWZpY2F0aW9uKVxyXG4gICAgICAgIH0sIG5vdGlmaWNhdGlvbi5tdXN0RGlzYXBwZWFyQWZ0ZXIpXHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIC8vIGVuc3VyZSB0aGUgbm90aWYgd2lsbCBiZSBkaXNwbGF5ZWQgZXZlbiBpZiByZWNlaXZlZCBmcm9tIGEgcHJvbWlzZSBwaXBlXHJcbiAgICAgIHRoaXMubmdab25lLnJ1bigoKSA9PiB7XHJcbiAgICAgICAgLy8gcHVzaCBvciB1bnNoaWZ0IGRlcGVuZCBpZiB0aGVyZSBhcmUgb24gdGhlIHRvcCBvciBib3R0b21cclxuICAgICAgICAvLyB0aGlzLm5vdGlmaWNhdGlvbnMucHVzaChub3RpZmljYXRpb24pXHJcbiAgICAgICAgdGhpcy51aVZhci5ub3RpZmljYXRpb25zLnVuc2hpZnQobm90aWZpY2F0aW9uKVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuXHJcbiAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc2F2ZWROb3RpZmljYXRpb25cclxuICAgIC5zdWJzY3JpYmUoKHNhdmVkKSA9PiB7ICAgICAgICAgIFxyXG4gICAgICBpZiAoIXNhdmVkKSB7IHJldHVybiB9XHJcbiAgICAgIHRoaXMudWlWYXIuc2F2ZWRBY3RpdmUgPSB0cnVlXHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMudWlWYXIuc2F2ZWRBY3RpdmUgPSBmYWxzZVxyXG4gICAgICB9LCAzMDApXHJcbiAgICB9KVxyXG4gIH1cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnByaVZhci5nZXRub3RpZmljYXRpb25TdWIpIHsgdGhpcy5wcmlWYXIuZ2V0bm90aWZpY2F0aW9uU3ViLnVuc3Vic2NyaWJlKCkgfVxyXG4gICAgaWYgKHRoaXMucHJpVmFyLnNhdmVkTm90aWZpY2F0aW9uU3ViKSB7IHRoaXMucHJpVmFyLnNhdmVkTm90aWZpY2F0aW9uU3ViLnVuc3Vic2NyaWJlKCkgfVxyXG4gIH1cclxufVxyXG4iLCI8ZGl2ID4gICAgXHJcbiAgPGRpdiAqbmdGb3I9XCJsZXQgbm90aWZpY2F0aW9uIG9mIHVpVmFyLm5vdGlmaWNhdGlvbnNcIlxyXG4gICAgICBbQGluT3V0XVxyXG4gICAgICAgIGNsYXNzPVwie3sgdWlBY3Rpb24uY3NzQ2xhc3Mobm90aWZpY2F0aW9uKSB9fSBub3RpZmljYXRpb24tZGlzbWlzc2FibGVcIj5cclxuICAgIDxidXR0b24gKGNsaWNrKT1cInVpQWN0aW9uLnJlbW92ZW5vdGlmaWNhdGlvbihub3RpZmljYXRpb24pXCIgdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJub3RpZmljYXRpb25cIj5cclxuICAgICAgPHNwYW4gPiZ0aW1lczs8L3NwYW4+XHJcbiAgICA8L2J1dHRvbj5cclxuICAgIDxkaXYgW2lubmVySFRNTF09XCJub3RpZmljYXRpb24ubWVzc2FnZVwiPjwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuXHJcblxyXG48ZGl2ICpuZ0lmPVwidWlWYXIuc2F2ZWRBY3RpdmVcIiAgY2xhc3M9XCJzYXZlZC1ub3RpZmljYXRpb25cIiA+XHJcbiAgPHNwYW4+PC9zcGFuPlxyXG48L2Rpdj4iXX0=
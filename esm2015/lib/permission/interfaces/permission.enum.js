export var eCp;
(function (eCp) {
    eCp["userCreate"] = "userCreate";
    eCp["userUpdate"] = "userUpdate";
    eCp["userRead"] = "userRead";
    eCp["userDelete"] = "userDelete";
    eCp["roleCreate"] = "roleCreate";
    eCp["roleUpdate"] = "roleUpdate";
    eCp["roleRead"] = "roleRead";
    eCp["roleDelete"] = "roleDelete";
    eCp["userRoleCreate"] = "userRoleCreate";
    eCp["userRoleUpdate"] = "userRoleUpdate";
    eCp["userRoleRead"] = "userRoleRead";
    eCp["userRoleDelete"] = "userRoleDelete";
    eCp["organizationCreate"] = "organizationCreate";
    eCp["organizationRead"] = "organizationRead";
    eCp["organizationUpdate"] = "organizationUpdate";
    eCp["organizationDelete"] = "organizationDelete";
    eCp["coreAdminAccess"] = "coreAdminAccess";
})(eCp || (eCp = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVybWlzc2lvbi5lbnVtLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb24uZW51bS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxNQUFNLENBQU4sSUFBWSxHQWtCWDtBQWxCRCxXQUFZLEdBQUc7SUFDYixnQ0FBeUIsQ0FBQTtJQUN6QixnQ0FBeUIsQ0FBQTtJQUN6Qiw0QkFBcUIsQ0FBQTtJQUNyQixnQ0FBeUIsQ0FBQTtJQUN6QixnQ0FBeUIsQ0FBQTtJQUN6QixnQ0FBeUIsQ0FBQTtJQUN6Qiw0QkFBcUIsQ0FBQTtJQUNyQixnQ0FBeUIsQ0FBQTtJQUN6Qix3Q0FBaUMsQ0FBQTtJQUNqQyx3Q0FBaUMsQ0FBQTtJQUNqQyxvQ0FBNkIsQ0FBQTtJQUM3Qix3Q0FBaUMsQ0FBQTtJQUNqQyxnREFBeUMsQ0FBQTtJQUN6Qyw0Q0FBcUMsQ0FBQTtJQUNyQyxnREFBeUMsQ0FBQTtJQUN6QyxnREFBeUMsQ0FBQTtJQUN6QywwQ0FBbUMsQ0FBQTtBQUNyQyxDQUFDLEVBbEJXLEdBQUcsS0FBSCxHQUFHLFFBa0JkIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gZUNwIHsgIFxyXG4gIHVzZXJDcmVhdGUgPSAndXNlckNyZWF0ZScsXHJcbiAgdXNlclVwZGF0ZSA9ICd1c2VyVXBkYXRlJywgXHJcbiAgdXNlclJlYWQgPSAndXNlclJlYWQnLFxyXG4gIHVzZXJEZWxldGUgPSAndXNlckRlbGV0ZScsICBcclxuICByb2xlQ3JlYXRlID0gJ3JvbGVDcmVhdGUnLFxyXG4gIHJvbGVVcGRhdGUgPSAncm9sZVVwZGF0ZScsXHJcbiAgcm9sZVJlYWQgPSAncm9sZVJlYWQnLFxyXG4gIHJvbGVEZWxldGUgPSAncm9sZURlbGV0ZScsXHJcbiAgdXNlclJvbGVDcmVhdGUgPSAndXNlclJvbGVDcmVhdGUnLFxyXG4gIHVzZXJSb2xlVXBkYXRlID0gJ3VzZXJSb2xlVXBkYXRlJyxcclxuICB1c2VyUm9sZVJlYWQgPSAndXNlclJvbGVSZWFkJyxcclxuICB1c2VyUm9sZURlbGV0ZSA9ICd1c2VyUm9sZURlbGV0ZScsXHJcbiAgb3JnYW5pemF0aW9uQ3JlYXRlID0gJ29yZ2FuaXphdGlvbkNyZWF0ZScsXHJcbiAgb3JnYW5pemF0aW9uUmVhZCA9ICdvcmdhbml6YXRpb25SZWFkJyxcclxuICBvcmdhbml6YXRpb25VcGRhdGUgPSAnb3JnYW5pemF0aW9uVXBkYXRlJyxcclxuICBvcmdhbml6YXRpb25EZWxldGUgPSAnb3JnYW5pemF0aW9uRGVsZXRlJyxcclxuICBjb3JlQWRtaW5BY2Nlc3MgPSAnY29yZUFkbWluQWNjZXNzJ1xyXG59Il19
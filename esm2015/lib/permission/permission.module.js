import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FofPermissionService } from './fof-permission.service';
import * as i0 from "@angular/core";
export class FofPermissionModule {
}
FofPermissionModule.ɵmod = i0.ɵɵdefineNgModule({ type: FofPermissionModule });
FofPermissionModule.ɵinj = i0.ɵɵdefineInjector({ factory: function FofPermissionModule_Factory(t) { return new (t || FofPermissionModule)(); }, providers: [
        FofPermissionService
    ], imports: [[
            CommonModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(FofPermissionModule, { imports: [CommonModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofPermissionModule, [{
        type: NgModule,
        args: [{
                declarations: [],
                imports: [
                    CommonModule
                ],
                providers: [
                    FofPermissionService
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVybWlzc2lvbi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9wZXJtaXNzaW9uL3Blcm1pc3Npb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFBOztBQWEvRCxNQUFNLE9BQU8sbUJBQW1COzt1REFBbkIsbUJBQW1CO3FIQUFuQixtQkFBbUIsbUJBSm5CO1FBQ1Qsb0JBQW9CO0tBQ3JCLFlBTFE7WUFDUCxZQUFZO1NBQ2I7d0ZBS1UsbUJBQW1CLGNBTjVCLFlBQVk7a0RBTUgsbUJBQW1CO2NBWC9CLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsRUFFYjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtpQkFDYjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1Qsb0JBQW9CO2lCQUNyQjthQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nXHJcbmltcG9ydCB7IEZvZlBlcm1pc3Npb25TZXJ2aWNlIH0gZnJvbSAnLi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICBcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZVxyXG4gIF0sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICBGb2ZQZXJtaXNzaW9uU2VydmljZVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZlBlcm1pc3Npb25Nb2R1bGUgeyB9XHJcbiJdfQ==
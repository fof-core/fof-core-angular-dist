import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
export class fofTranslate {
    transform(content) {
        return `<b>${content}</b>`;
    }
}
fofTranslate.ɵfac = function fofTranslate_Factory(t) { return new (t || fofTranslate)(); };
fofTranslate.ɵpipe = i0.ɵɵdefinePipe({ name: "fofTranslate", type: fofTranslate, pure: true });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(fofTranslate, [{
        type: Pipe,
        args: [{ name: 'fofTranslate' }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm8tdHJhbnNsYXRlLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvcGlwZXMvZm8tdHJhbnNsYXRlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7O0FBR3BELE1BQU0sT0FBTyxZQUFZO0lBQ3ZCLFNBQVMsQ0FBQyxPQUFPO1FBQ2YsT0FBTyxNQUFNLE9BQU8sTUFBTSxDQUFDO0lBQzdCLENBQUM7O3dFQUhVLFlBQVk7bUVBQVosWUFBWTtrREFBWixZQUFZO2NBRHhCLElBQUk7ZUFBQyxFQUFDLElBQUksRUFBRSxjQUFjLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7bmFtZTogJ2ZvZlRyYW5zbGF0ZSd9KVxyXG5leHBvcnQgY2xhc3MgZm9mVHJhbnNsYXRlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcbiAgdHJhbnNmb3JtKGNvbnRlbnQpIHtcclxuICAgIHJldHVybiBgPGI+JHtjb250ZW50fTwvYj5gO1xyXG4gIH1cclxufSJdfQ==
import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { fofUtilsForm } from '../../core/fof-utils';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "@angular/router";
import * as i3 from "@angular/forms";
import * as i4 from "../../core/notification/notification.service";
import * as i5 from "../../core/fof-dialog.service";
import * as i6 from "@angular/material/dialog";
import * as i7 from "../../core/fof-error.service";
import * as i8 from "../../uservices/uservices.service";
import * as i9 from "@angular/material/card";
import * as i10 from "@angular/material/button";
import * as i11 from "@angular/common";
import * as i12 from "@angular/material/form-field";
import * as i13 from "@angular/material/input";
import * as i14 from "../../components/fof-organizations-multi-select/fof-organizations-multi-select.component";
import * as i15 from "@angular/material/list";
import * as i16 from "@angular/material/expansion";
import * as i17 from "@angular/material/checkbox";
import * as i18 from "@angular/material/progress-spinner";
function UserComponent_div_11_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le login ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function UserComponent_div_11_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Un email valide est obligatoire ");
    i0.ɵɵelementEnd();
} }
function UserComponent_div_11_mat_error_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le pr\u00E9nom ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function UserComponent_div_11_mat_error_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le nom de famille ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template(rf, ctx) { if (rf & 1) {
    const _r151 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-expansion-panel");
    i0.ɵɵelementStart(1, "mat-expansion-panel-header", 20);
    i0.ɵɵelementStart(2, "mat-checkbox", 21);
    i0.ɵɵlistener("click", function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template_mat_checkbox_click_2_listener($event) { i0.ɵɵrestoreView(_r151); return $event.stopPropagation(); })("change", function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template_mat_checkbox_change_2_listener() { i0.ɵɵrestoreView(_r151); const uService_r147 = i0.ɵɵnextContext().$implicit; const ctx_r152 = i0.ɵɵnextContext(2); return ctx_r152.uiAction.userUserviceSave(uService_r147); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "div", 22);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtext(5, " This the expansion 2 content ");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const uService_r147 = i0.ɵɵnextContext().$implicit;
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("checked", uService_r147.checked);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(uService_r147.name);
} }
function UserComponent_div_11_ng_container_22_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementContainerStart(0);
    i0.ɵɵtemplate(1, UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template, 6, 2, "mat-expansion-panel", 13);
    i0.ɵɵelementContainerEnd();
} if (rf & 2) {
    const uService_r147 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", uService_r147.availableForUsers);
} }
function UserComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    const _r156 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 8);
    i0.ɵɵelementStart(1, "div", 9);
    i0.ɵɵelementStart(2, "div", 10);
    i0.ɵɵelementStart(3, "form", 11);
    i0.ɵɵelementStart(4, "mat-form-field");
    i0.ɵɵelement(5, "input", 12);
    i0.ɵɵtemplate(6, UserComponent_div_11_mat_error_6_Template, 2, 0, "mat-error", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "mat-form-field");
    i0.ɵɵelement(8, "input", 14);
    i0.ɵɵtemplate(9, UserComponent_div_11_mat_error_9_Template, 2, 0, "mat-error", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "mat-form-field");
    i0.ɵɵelement(11, "input", 15);
    i0.ɵɵtemplate(12, UserComponent_div_11_mat_error_12_Template, 2, 0, "mat-error", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "mat-form-field");
    i0.ɵɵelement(14, "input", 16);
    i0.ɵɵtemplate(15, UserComponent_div_11_mat_error_15_Template, 2, 0, "mat-error", 13);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(16, "fof-core-fof-organizations-multi-select", 17);
    i0.ɵɵlistener("selectedOrganizationsChange", function UserComponent_div_11_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_16_listener($event) { i0.ɵɵrestoreView(_r156); const ctx_r155 = i0.ɵɵnextContext(); return ctx_r155.uiAction.organisationMultiSelectedChange($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(17, "div", 10);
    i0.ɵɵelementStart(18, "h3");
    i0.ɵɵtext(19, "Acc\u00E8s du collaborateur aux mini-services");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(20, "mat-list");
    i0.ɵɵelementStart(21, "mat-accordion", 18);
    i0.ɵɵtemplate(22, UserComponent_div_11_ng_container_22_Template, 2, 1, "ng-container", 19);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r140 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("formGroup", ctx_r140.uiVar.form);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r140.uiVar.form.get("login").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r140.uiVar.form.get("email").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r140.uiVar.form.get("firstName").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r140.uiVar.form.get("lastName").invalid);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("placeHolder", "Organisation de rattachement")("multiSelect", false)("selectedOrganisations", ctx_r140.uiVar.userOrganizationsDisplay);
    i0.ɵɵadvance(6);
    i0.ɵɵproperty("ngForOf", ctx_r140.uiVar.uServicesAll);
} }
function UserComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 23);
    i0.ɵɵelement(1, "mat-spinner", 24);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des utilisateurs et des authorisations...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
export class UserComponent {
    constructor(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, matDialog, fofErrorService, uservicesService) {
        this.fofPermissionService = fofPermissionService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        this.matDialog = matDialog;
        this.fofErrorService = fofErrorService;
        this.uservicesService = uservicesService;
        // All private variables
        this.priVar = {
            userId: undefined,
            userOrganizationId: undefined,
            organizationAlreadyWithRoles: undefined
        };
        // All private functions
        this.privFunc = {
            userLoad: () => {
                this.uiVar.loadingUser = true;
                Promise.all([
                    // Just need to ensure the uServices and user are both loaded the first time
                    this.uiVar.uServicesAll ? null : this.uservicesService.uServices.getAll().toPromise(),
                    this.fofPermissionService.user.getWithUservicesById(this.priVar.userId).toPromise()
                ])
                    .then(result => {
                    const uServices = result[0];
                    const currentUsers = result[1];
                    if (uServices) {
                        this.uiVar.uServicesAll = uServices;
                    }
                    if (currentUsers.userUServices && currentUsers.userUServices.length > 0) {
                        uServices.forEach(service => {
                            const result = currentUsers.userUServices.filter(userService => userService.uServiceId == service.id);
                            if (result && result.length > 0) {
                                service.checked = true;
                                service.userUserviceId = result[0].id;
                            }
                        });
                    }
                    this.privFunc.userRefresh(currentUsers);
                    this.uiVar.form.patchValue(this.uiVar.user);
                })
                    .catch(reason => {
                    this.fofErrorService.errorManage(reason);
                    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                })
                    .finally(() => {
                    this.uiVar.loadingUser = false;
                });
            },
            userRefresh: (currentUser) => {
                if (!currentUser) {
                    this.fofNotificationService.error(`Ce collaborateur n'existe pas`);
                    this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                    return;
                }
                this.uiVar.user = currentUser;
                if (currentUser.organizationId) {
                    this.uiVar.userOrganizationsDisplay = [{ id: currentUser.organizationId }];
                    this.priVar.userOrganizationId = currentUser.organizationId;
                }
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouveau collaborateur',
            loadingUser: false,
            uServicesAll: undefined,
            userIsNew: false,
            user: undefined,
            userOrganizationsDisplay: undefined,
            form: this.formBuilder.group({
                login: ['', [Validators.maxLength(30)]],
                email: ['', [Validators.required, Validators.email, Validators.maxLength(60)]],
                firstName: ['', [Validators.maxLength(30)]],
                lastName: ['', [Validators.maxLength(30)]]
            })
        };
        // All actions shared with UI 
        this.uiAction = {
            userSave: () => {
                const userToSave = this.uiVar.form.value;
                userToSave.organizationId = this.priVar.userOrganizationId;
                if (!this.uiVar.form.valid) {
                    this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(this.uiVar.form);
                    return;
                }
                if (this.uiVar.userIsNew) {
                    this.fofPermissionService.user.create(userToSave)
                        .toPromise()
                        .then((newUser) => {
                        this.fofNotificationService.success('collaborateur sauvé', { mustDisappearAfter: 1000 });
                        this.priVar.userId = newUser.id;
                        this.uiVar.title = `Modification d'un collaborateur`;
                        this.uiVar.userIsNew = false;
                        this.privFunc.userLoad();
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    userToSave.id = this.uiVar.user.id;
                    this.fofPermissionService.user.update(userToSave)
                        .toPromise()
                        .then(result => {
                        this.fofNotificationService.success('collaborateur sauvé', { mustDisappearAfter: 1000 });
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
            },
            userCancel: () => {
                this.privFunc.userLoad();
            },
            userDelete: () => {
                this.fofDialogService.openYesNo({
                    question: `Voulez vous vraiment supprimer le collaborateur ?`
                }).then(yes => {
                    if (yes) {
                        this.fofPermissionService.user.delete(this.uiVar.user)
                            .toPromise()
                            .then(result => {
                            this.fofNotificationService.success('collaborateur supprimé');
                            this.router.navigate(['../'], { relativeTo: this.activatedRoute });
                        });
                    }
                });
            },
            userUserviceSave: (uService) => {
                if (uService.userUserviceId) {
                    this.fofPermissionService.userUservice.delete(uService.userUserviceId)
                        .toPromise()
                        .then(result => {
                        this.fofNotificationService.saveIsDone();
                        uService.checked = false;
                        uService.userUserviceId = null;
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    const userService = {
                        userId: this.priVar.userId,
                        uServiceId: uService.id
                    };
                    this.fofPermissionService.userUservice.create(userService)
                        .toPromise()
                        .then(result => {
                        this.fofNotificationService.saveIsDone();
                        uService.checked = true;
                        uService.userUserviceId = result.id;
                    })
                        .catch(reason => {
                        this.fofErrorService.errorManage(reason);
                    });
                }
            },
            organisationMultiSelectedChange: (organizations) => {
                if (organizations && organizations.length > 0) {
                    this.priVar.userOrganizationId = organizations[0].id;
                }
                else {
                    this.priVar.userOrganizationId = null;
                }
            }
        };
    }
    // Angular events
    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(params => {
            const code = params.get('code');
            if (code) {
                if (code.toLowerCase() == 'new') {
                    this.uiVar.userIsNew = true;
                }
                else {
                    this.priVar.userId = code;
                    this.uiVar.title = `Modification d'un collaborateur`;
                    this.privFunc.userLoad();
                }
            }
        });
    }
}
UserComponent.ɵfac = function UserComponent_Factory(t) { return new (t || UserComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i3.FormBuilder), i0.ɵɵdirectiveInject(i4.FofNotificationService), i0.ɵɵdirectiveInject(i5.FofDialogService), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i6.MatDialog), i0.ɵɵdirectiveInject(i7.FofErrorService), i0.ɵɵdirectiveInject(i8.UservicesService)); };
UserComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UserComponent, selectors: [["fof-core-user"]], decls: 13, vars: 3, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "detail fof-fade-in", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [1, "detail", "fof-fade-in"], [1, "row"], [1, "col-md-6"], [3, "formGroup"], ["matInput", "", "formControlName", "login", "placeholder", "login", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "type", "email", "formControlName", "email", "placeholder", "Email", "value", ""], ["matInput", "", "required", "", "type", "firstName", "formControlName", "firstName", "placeholder", "Pr\u00E9nom", "value", ""], ["matInput", "", "required", "", "type", "lastName", "formControlName", "lastName", "placeholder", "Nom de famille", "value", ""], [3, "placeHolder", "multiSelect", "selectedOrganisations", "selectedOrganizationsChange"], ["multi", "true"], [4, "ngFor", "ngForOf"], [1, "uservice-access-header"], [3, "checked", "click", "change"], [1, "check-label"], [1, "fof-loading"], ["diameter", "20"]], template: function UserComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-card", 0);
        i0.ɵɵelementStart(1, "h3");
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "div", 1);
        i0.ɵɵelementStart(4, "button", 2);
        i0.ɵɵlistener("click", function UserComponent_Template_button_click_4_listener() { return ctx.uiAction.userCancel(); });
        i0.ɵɵtext(5, "Annuler");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "button", 3);
        i0.ɵɵlistener("click", function UserComponent_Template_button_click_6_listener() { return ctx.uiAction.userDelete(); });
        i0.ɵɵtext(7, "Supprimer");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "button", 4);
        i0.ɵɵlistener("click", function UserComponent_Template_button_click_8_listener() { return ctx.uiAction.userSave(); });
        i0.ɵɵtext(9, " Enregister");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(10, "div", 5);
        i0.ɵɵtemplate(11, UserComponent_div_11_Template, 23, 9, "div", 6);
        i0.ɵɵtemplate(12, UserComponent_div_12_Template, 4, 0, "div", 7);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.uiVar.title);
        i0.ɵɵadvance(9);
        i0.ɵɵproperty("ngIf", !ctx.uiVar.loadingUser);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.uiVar.loadingUser);
    } }, directives: [i9.MatCard, i10.MatButton, i11.NgIf, i3.ɵangular_packages_forms_forms_y, i3.NgControlStatusGroup, i3.FormGroupDirective, i12.MatFormField, i13.MatInput, i3.DefaultValueAccessor, i3.NgControlStatus, i3.FormControlName, i3.RequiredValidator, i14.FofOrganizationsMultiSelectComponent, i15.MatList, i16.MatAccordion, i11.NgForOf, i12.MatError, i16.MatExpansionPanel, i16.MatExpansionPanelHeader, i17.MatCheckbox, i18.MatSpinner], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row;margin-bottom:15px}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .uservice-access-header[_ngcontent-%COMP%]   .check-label[_ngcontent-%COMP%]{padding-left:10px;margin-top:.1em}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UserComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-user',
                templateUrl: './user.component.html',
                styleUrls: ['./user.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.ActivatedRoute }, { type: i3.FormBuilder }, { type: i4.FofNotificationService }, { type: i5.FofDialogService }, { type: i2.Router }, { type: i6.MatDialog }, { type: i7.FofErrorService }, { type: i8.UservicesService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi91c2Vycy91c2VyL3VzZXIuY29tcG9uZW50LnRzIiwibGliL3VzZXJzL3VzZXIvdXNlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFtQyxNQUFNLGVBQWUsQ0FBQTtBQU0xRSxPQUFPLEVBQTBCLFVBQVUsRUFBRyxNQUFNLGdCQUFnQixDQUFBO0FBR3BFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDZXZDLGlDQUNFO0lBQUEsc0VBQ0Y7SUFBQSxpQkFBWTs7O0lBUVosaUNBQ0U7SUFBQSxpREFDRjtJQUFBLGlCQUFZOzs7SUFRWixpQ0FDRTtJQUFBLDRFQUNGO0lBQUEsaUJBQVk7OztJQVFaLGlDQUNFO0lBQUEsK0VBQ0Y7SUFBQSxpQkFBWTs7OztJQXFCUiwyQ0FDRTtJQUFBLHNEQUNFO0lBQUEsd0NBR2lCO0lBRmYsNEtBQVMsd0JBQXdCLElBQUUscVBBQ3pCLGlEQUFtQyxJQURWO0lBRW5DLGlCQUFlO0lBQUEsK0JBQXlCO0lBQUEsWUFBbUI7SUFBQSxpQkFBTTtJQUNyRSxpQkFBNkI7SUFDN0IsOENBQ0Y7SUFBQSxpQkFBc0I7OztJQU5KLGVBQTRCO0lBQTVCLCtDQUE0QjtJQUdBLGVBQW1CO0lBQW5CLHdDQUFtQjs7O0lBUHJFLDZCQUVJO0lBQUEsc0hBQ0U7SUFTTiwwQkFBZTs7O0lBVlUsZUFBa0M7SUFBbEMsc0RBQWtDOzs7O0lBaEVyRSw4QkFFRTtJQUFBLDhCQUNFO0lBQUEsK0JBRUU7SUFBQSxnQ0FFRTtJQUFBLHNDQUNFO0lBQUEsNEJBR0E7SUFBQSxrRkFDRTtJQUVKLGlCQUFpQjtJQUVqQixzQ0FDRTtJQUFBLDRCQUlBO0lBQUEsa0ZBQ0U7SUFFSixpQkFBaUI7SUFFakIsdUNBQ0U7SUFBQSw2QkFJQTtJQUFBLG9GQUNFO0lBRUosaUJBQWlCO0lBRWpCLHVDQUNFO0lBQUEsNkJBSUE7SUFBQSxvRkFDRTtJQUVKLGlCQUFpQjtJQUVqQixvRUFLMkM7SUFEekMsbVBBQWlDLHlEQUFnRCxJQUFDO0lBQ25GLGlCQUEwQztJQUU3QyxpQkFBTztJQUVULGlCQUFNO0lBRU4sZ0NBQ0U7SUFBQSwyQkFBSTtJQUFBLDhEQUF3QztJQUFBLGlCQUFLO0lBQ2pELGlDQUVFO0lBQUEsMENBQ0U7SUFBQSwwRkFFSTtJQVdSLGlCQUFnQjtJQUVoQixpQkFBVztJQUNiLGlCQUFNO0lBQ1IsaUJBQU07SUFDUixpQkFBTTs7O0lBM0VNLGVBQXdCO0lBQXhCLCtDQUF3QjtJQU1mLGVBQXVDO0lBQXZDLCtEQUF1QztJQVV2QyxlQUF1QztJQUF2QywrREFBdUM7SUFVdkMsZUFBMkM7SUFBM0MsbUVBQTJDO0lBVTNDLGVBQTBDO0lBQTFDLGtFQUEwQztJQU1yRCxlQUE4QztJQUE5Qyw0REFBOEMsc0JBQUEsa0VBQUE7SUFlaEMsZUFBMEQ7SUFBMUQscURBQTBEOzs7SUFtQmxGLCtCQUNFO0lBQUEsa0NBQXVDO0lBQUMsNEJBQU07SUFBQSxxRUFBcUQ7SUFBQSxpQkFBTztJQUM1RyxpQkFBTTs7QUR2RVIsTUFBTSxPQUFPLGFBQWE7SUFDeEIsWUFDVSxvQkFBMEMsRUFDMUMsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsc0JBQThDLEVBQzlDLGdCQUFrQyxFQUNsQyxNQUFjLEVBQ2QsU0FBb0IsRUFDcEIsZUFBZ0MsRUFDaEMsZ0JBQWtDO1FBUmxDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUNwQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUk1Qyx3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2YsTUFBTSxFQUFVLFNBQVM7WUFDekIsa0JBQWtCLEVBQVUsU0FBUztZQUNyQyw0QkFBNEIsRUFBbUIsU0FBUztTQUN6RCxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRztZQUNqQixRQUFRLEVBQUMsR0FBRyxFQUFFO2dCQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtnQkFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQztvQkFDViw0RUFBNEU7b0JBQzVFLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsU0FBUyxFQUFFO29CQUNwRixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxFQUFFO2lCQUNwRixDQUFDO3FCQUNELElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDYixNQUFNLFNBQVMsR0FBa0IsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO29CQUMxQyxNQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7b0JBRTlCLElBQUksU0FBUyxFQUFFO3dCQUNiLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQTtxQkFDcEM7b0JBRUQsSUFBSSxZQUFZLENBQUMsYUFBYSxJQUFJLFlBQVksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDdkUsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTs0QkFDMUIsTUFBTSxNQUFNLEdBQUcsWUFBWSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsVUFBVSxJQUFJLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQTs0QkFDckcsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0NBQy9CLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO2dDQUN0QixPQUFPLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUE7NkJBQ3RDO3dCQUNILENBQUMsQ0FBQyxDQUFBO3FCQUNIO29CQUVELElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFBO29CQUV2QyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDN0MsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtvQkFDeEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQTtnQkFDbEUsQ0FBQyxDQUFDO3FCQUNELE9BQU8sQ0FBQyxHQUFHLEVBQUU7b0JBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFBO2dCQUNoQyxDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUM7WUFDRCxXQUFXLEVBQUMsQ0FBQyxXQUFrQixFQUFFLEVBQUU7Z0JBRWpDLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQTtvQkFDbEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQTtvQkFDaEUsT0FBTTtpQkFDUDtnQkFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxXQUFXLENBQUE7Z0JBRTdCLElBQUksV0FBVyxDQUFDLGNBQWMsRUFBRTtvQkFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsR0FBRyxDQUFDLEVBQUMsRUFBRSxFQUFFLFdBQVcsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxDQUFBO29CQUN4RSxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUE7aUJBQzVEO1lBRUgsQ0FBQztTQUNGLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsS0FBSyxFQUFFLHVCQUF1QjtZQUM5QixXQUFXLEVBQUUsS0FBSztZQUNsQixZQUFZLEVBQWUsU0FBUztZQUNwQyxTQUFTLEVBQUUsS0FBSztZQUNoQixJQUFJLEVBQVMsU0FBUztZQUN0Qix3QkFBd0IsRUFBbUIsU0FBUztZQUNwRCxJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDdkMsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDOUUsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUMzQyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDM0MsQ0FBQztTQUNILENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHO1lBQ2hCLFFBQVEsRUFBQyxHQUFHLEVBQUU7Z0JBQ1osTUFBTSxVQUFVLEdBQVUsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBO2dCQUMvQyxVQUFVLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUE7Z0JBRTFELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQTtvQkFDdkYsWUFBWSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7b0JBQy9DLE9BQU07aUJBQ1A7Z0JBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRTtvQkFDeEIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO3lCQUNoRCxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLENBQUMsT0FBYyxFQUFFLEVBQUU7d0JBQ3ZCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsRUFBQyxrQkFBa0IsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO3dCQUN0RixJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsRUFBRSxDQUFBO3dCQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxpQ0FBaUMsQ0FBQTt3QkFDcEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFBO3dCQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFBO29CQUMxQixDQUFDLENBQUM7eUJBQ0QsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNkLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUMxQyxDQUFDLENBQUMsQ0FBQTtpQkFDSDtxQkFBTTtvQkFDTCxVQUFVLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQTtvQkFFbEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO3lCQUNoRCxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNiLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMscUJBQXFCLEVBQUUsRUFBQyxrQkFBa0IsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO29CQUN4RixDQUFDLENBQUM7eUJBQ0QsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUNkLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUMxQyxDQUFDLENBQUMsQ0FBQTtpQkFDSDtZQUNILENBQUM7WUFDRCxVQUFVLEVBQUMsR0FBRyxFQUFFO2dCQUNkLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUE7WUFDMUIsQ0FBQztZQUNELFVBQVUsRUFBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztvQkFDOUIsUUFBUSxFQUFFLG1EQUFtRDtpQkFDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDWixJQUFJLEdBQUcsRUFBRTt3QkFDUCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQzs2QkFDckQsU0FBUyxFQUFFOzZCQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTs0QkFDYixJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLHdCQUF3QixDQUFDLENBQUE7NEJBQzdELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBQyxDQUFDLENBQUE7d0JBQ2xFLENBQUMsQ0FBQyxDQUFBO3FCQUNIO2dCQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0osQ0FBQztZQUNELGdCQUFnQixFQUFDLENBQUMsUUFBcUIsRUFBRSxFQUFFO2dCQUV6QyxJQUFJLFFBQVEsQ0FBQyxjQUFjLEVBQUU7b0JBQzNCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUM7eUJBQ3JFLFNBQVMsRUFBRTt5QkFDWCxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRSxDQUFBO3dCQUN4QyxRQUFRLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQTt3QkFDeEIsUUFBUSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUE7b0JBQ2hDLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQzFDLENBQUMsQ0FBQyxDQUFBO2lCQUNIO3FCQUFNO29CQUNMLE1BQU0sV0FBVyxHQUFrQjt3QkFDakMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTTt3QkFDMUIsVUFBVSxFQUFFLFFBQVEsQ0FBQyxFQUFFO3FCQUN4QixDQUFBO29CQUVELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQzt5QkFDekQsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTt3QkFDYixJQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxFQUFFLENBQUE7d0JBQ3hDLFFBQVEsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO3dCQUN2QixRQUFRLENBQUMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUE7b0JBQ3JDLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQzFDLENBQUMsQ0FBQyxDQUFBO2lCQUNIO1lBQ0gsQ0FBQztZQUNELCtCQUErQixFQUFDLENBQUMsYUFBOEIsRUFBRSxFQUFFO2dCQUNqRSxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDN0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFBO2lCQUNyRDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQTtpQkFDdEM7WUFDSCxDQUFDO1NBQ0YsQ0FBQTtJQTVLRCxDQUFDO0lBNktELGlCQUFpQjtJQUNqQixRQUFRO1FBQ04sSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQzlDLE1BQU0sSUFBSSxHQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7WUFFbkMsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksS0FBSyxFQUFFO29CQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUE7aUJBQzVCO3FCQUFNO29CQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQTtvQkFDekIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsaUNBQWlDLENBQUE7b0JBQ3BELElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUE7aUJBQ3pCO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7OzBFQXhNVSxhQUFhO2tEQUFiLGFBQWE7UUN6QjFCLG1DQUNFO1FBQUEsMEJBQUk7UUFBQSxZQUFpQjtRQUFBLGlCQUFLO1FBQzFCLDhCQUNFO1FBQUEsaUNBQ2tDO1FBQWhDLDBGQUFTLHlCQUFxQixJQUFDO1FBQUMsdUJBQU87UUFBQSxpQkFBUztRQUNsRCxpQ0FDa0M7UUFBaEMsMEZBQVMseUJBQXFCLElBQUM7UUFBQyx5QkFBUztRQUFBLGlCQUFTO1FBQ3BELGlDQUVFO1FBREEsMEZBQVMsdUJBQW1CLElBQUM7UUFDN0IsMkJBQVU7UUFBQSxpQkFBUztRQUN2QixpQkFBTTtRQUNSLGlCQUFXO1FBQ1gsK0JBQ0U7UUFBQSxpRUFFRTtRQStFRixnRUFDRTtRQUdKLGlCQUFNOztRQWpHQSxlQUFpQjtRQUFqQixxQ0FBaUI7UUFZaEIsZUFBMEI7UUFBMUIsNkNBQTBCO1FBaUYxQixlQUF5QjtRQUF6Qiw0Q0FBeUI7O2tERHJFbkIsYUFBYTtjQUx6QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLGVBQWU7Z0JBQ3pCLFdBQVcsRUFBRSx1QkFBdUI7Z0JBQ3BDLFNBQVMsRUFBRSxDQUFDLHVCQUF1QixDQUFDO2FBQ3JDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXHJcbmltcG9ydCB7IGlSb2xlLCBpVXNlciwgaVVzZXJSb2xlT3JnYW5pemF0aW9uLCBpT3JnYW5pemF0aW9uIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgZm9ya0pvaW4sIG9mIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyAgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIlxyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZkRpYWxvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL2ZvZi1kaWFsb2cuc2VydmljZSdcclxuaW1wb3J0IHsgZm9mVXRpbHNGb3JtIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtdXRpbHMnXHJcbmltcG9ydCB7IE1hdERpYWxvZywgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnXHJcbmltcG9ydCB7IEZvZkVycm9yU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLWVycm9yLnNlcnZpY2UnXHJcbmltcG9ydCB7IFVzZXJ2aWNlc1NlcnZpY2UgfSBmcm9tICcuLi8uLi91c2VydmljZXMvdXNlcnZpY2VzLnNlcnZpY2UnXHJcbmltcG9ydCB7IGlVc2VydmljZSwgaVVzZXJVc2VydmljZSB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcblxyXG5pbnRlcmZhY2UgaVVzZXJ2aWNlVUkgZXh0ZW5kcyBpVXNlcnZpY2Uge1xyXG4gIGNoZWNrZWQ/OiBib29sZWFuLFxyXG4gIHVzZXJVc2VydmljZUlkPzogbnVtYmVyXHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZm9mLWNvcmUtdXNlcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3VzZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3VzZXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvZlBlcm1pc3Npb25TZXJ2aWNlOiBGb2ZQZXJtaXNzaW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgcHJpdmF0ZSBmb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIsXHJcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGZvZkRpYWxvZ1NlcnZpY2U6IEZvZkRpYWxvZ1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSBtYXREaWFsb2c6IE1hdERpYWxvZyxcclxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHVzZXJ2aWNlc1NlcnZpY2U6IFVzZXJ2aWNlc1NlcnZpY2VcclxuICApIHsgICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICB1c2VySWQ6IDxudW1iZXI+dW5kZWZpbmVkLFxyXG4gICAgdXNlck9yZ2FuaXphdGlvbklkOiA8bnVtYmVyPnVuZGVmaW5lZCxcclxuICAgIG9yZ2FuaXphdGlvbkFscmVhZHlXaXRoUm9sZXM6IDxpT3JnYW5pemF0aW9uW10+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgICB1c2VyTG9hZDooKSA9PiB7XHJcbiAgICAgIHRoaXMudWlWYXIubG9hZGluZ1VzZXIgPSB0cnVlXHJcbiAgICAgIFByb21pc2UuYWxsKFtcclxuICAgICAgICAvLyBKdXN0IG5lZWQgdG8gZW5zdXJlIHRoZSB1U2VydmljZXMgYW5kIHVzZXIgYXJlIGJvdGggbG9hZGVkIHRoZSBmaXJzdCB0aW1lXHJcbiAgICAgICAgdGhpcy51aVZhci51U2VydmljZXNBbGw/IG51bGwgOiB0aGlzLnVzZXJ2aWNlc1NlcnZpY2UudVNlcnZpY2VzLmdldEFsbCgpLnRvUHJvbWlzZSgpLFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlci5nZXRXaXRoVXNlcnZpY2VzQnlJZCh0aGlzLnByaVZhci51c2VySWQpLnRvUHJvbWlzZSgpICAgICAgICBcclxuICAgICAgXSlcclxuICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICBjb25zdCB1U2VydmljZXM6IGlVc2VydmljZVVJW10gPSByZXN1bHRbMF0gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRVc2VycyA9IHJlc3VsdFsxXVxyXG5cclxuICAgICAgICBpZiAodVNlcnZpY2VzKSB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnVTZXJ2aWNlc0FsbCA9IHVTZXJ2aWNlc1xyXG4gICAgICAgIH0gICAgICAgIFxyXG5cclxuICAgICAgICBpZiAoY3VycmVudFVzZXJzLnVzZXJVU2VydmljZXMgJiYgY3VycmVudFVzZXJzLnVzZXJVU2VydmljZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgdVNlcnZpY2VzLmZvckVhY2goc2VydmljZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlc3VsdCA9IGN1cnJlbnRVc2Vycy51c2VyVVNlcnZpY2VzLmZpbHRlcih1c2VyU2VydmljZSA9PiB1c2VyU2VydmljZS51U2VydmljZUlkID09IHNlcnZpY2UuaWQpXHJcbiAgICAgICAgICAgIGlmIChyZXN1bHQgJiYgcmVzdWx0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICBzZXJ2aWNlLmNoZWNrZWQgPSB0cnVlXHJcbiAgICAgICAgICAgICAgc2VydmljZS51c2VyVXNlcnZpY2VJZCA9IHJlc3VsdFswXS5pZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0gICAgICAgIFxyXG5cclxuICAgICAgICB0aGlzLnByaXZGdW5jLnVzZXJSZWZyZXNoKGN1cnJlbnRVc2VycykgICAgICAgICAgXHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy51aVZhci5mb3JtLnBhdGNoVmFsdWUodGhpcy51aVZhci51c2VyKSAgICAgICAgICAgICAgICBcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7ICAgICAgICBcclxuICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLi8nXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGV9KVxyXG4gICAgICB9KVxyXG4gICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nVXNlciA9IGZhbHNlXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgdXNlclJlZnJlc2g6KGN1cnJlbnRVc2VyOiBpVXNlcikgPT4ge1xyXG4gICAgICBcclxuICAgICAgaWYgKCFjdXJyZW50VXNlcikge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihgQ2UgY29sbGFib3JhdGV1ciBuJ2V4aXN0ZSBwYXNgKVxyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLi4vJ10sIHtyZWxhdGl2ZVRvOiB0aGlzLmFjdGl2YXRlZFJvdXRlfSlcclxuICAgICAgICByZXR1cm5cclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy51aVZhci51c2VyID0gY3VycmVudFVzZXIgICAgICBcclxuXHJcbiAgICAgIGlmIChjdXJyZW50VXNlci5vcmdhbml6YXRpb25JZCkge1xyXG4gICAgICAgIHRoaXMudWlWYXIudXNlck9yZ2FuaXphdGlvbnNEaXNwbGF5ID0gW3tpZDogY3VycmVudFVzZXIub3JnYW5pemF0aW9uSWR9XVxyXG4gICAgICAgIHRoaXMucHJpVmFyLnVzZXJPcmdhbml6YXRpb25JZCA9IGN1cnJlbnRVc2VyLm9yZ2FuaXphdGlvbklkXHJcbiAgICAgIH1cclxuXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgdGl0bGU6ICdOb3V2ZWF1IGNvbGxhYm9yYXRldXInLCAgICBcclxuICAgIGxvYWRpbmdVc2VyOiBmYWxzZSwgXHJcbiAgICB1U2VydmljZXNBbGw6IDxpVXNlcnZpY2VbXT51bmRlZmluZWQsICAgXHJcbiAgICB1c2VySXNOZXc6IGZhbHNlLFxyXG4gICAgdXNlcjogPGlVc2VyPnVuZGVmaW5lZCxcclxuICAgIHVzZXJPcmdhbml6YXRpb25zRGlzcGxheTogPGlPcmdhbml6YXRpb25bXT51bmRlZmluZWQsXHJcbiAgICBmb3JtOiB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHsgICAgICBcclxuICAgICAgbG9naW46IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDMwKV1dLFxyXG4gICAgICBlbWFpbDogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5lbWFpbCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNjApXV0sXHJcbiAgICAgIGZpcnN0TmFtZTogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMzApXV0sXHJcbiAgICAgIGxhc3ROYW1lOiBbJycsIFtWYWxpZGF0b3JzLm1heExlbmd0aCgzMCldXVxyXG4gICAgfSlcclxuICB9XHJcbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpQWN0aW9uID0ge1xyXG4gICAgdXNlclNhdmU6KCkgPT4geyAgICAgIFxyXG4gICAgICBjb25zdCB1c2VyVG9TYXZlOiBpVXNlciA9IHRoaXMudWlWYXIuZm9ybS52YWx1ZVxyXG4gICAgICB1c2VyVG9TYXZlLm9yZ2FuaXphdGlvbklkID0gdGhpcy5wcmlWYXIudXNlck9yZ2FuaXphdGlvbklkXHJcblxyXG4gICAgICBpZiAoIXRoaXMudWlWYXIuZm9ybS52YWxpZCkge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcignVmV1aWxsZXogY29ycmlnZXIgbGVzIGVycmV1cnMgYXZhbnQgZGUgc2F1dmVnYXJkZXInKVxyXG4gICAgICAgIGZvZlV0aWxzRm9ybS52YWxpZGF0ZUFsbEZpZWxkcyh0aGlzLnVpVmFyLmZvcm0pXHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICh0aGlzLnVpVmFyLnVzZXJJc05ldykgeyAgICAgICAgXHJcbiAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS51c2VyLmNyZWF0ZSh1c2VyVG9TYXZlKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKChuZXdVc2VyOiBpVXNlcikgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ2NvbGxhYm9yYXRldXIgc2F1dsOpJywge211c3REaXNhcHBlYXJBZnRlcjogMTAwMH0pXHJcbiAgICAgICAgICB0aGlzLnByaVZhci51c2VySWQgPSBuZXdVc2VyLmlkXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnRpdGxlID0gYE1vZGlmaWNhdGlvbiBkJ3VuIGNvbGxhYm9yYXRldXJgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnVzZXJJc05ldyA9IGZhbHNlXHJcbiAgICAgICAgICB0aGlzLnByaXZGdW5jLnVzZXJMb2FkKCkgICAgICAgICAgXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2gocmVhc29uID0+IHtcclxuICAgICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHVzZXJUb1NhdmUuaWQgPSB0aGlzLnVpVmFyLnVzZXIuaWQgXHJcbiAgICAgIFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlci51cGRhdGUodXNlclRvU2F2ZSlcclxuICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ2NvbGxhYm9yYXRldXIgc2F1dsOpJywge211c3REaXNhcHBlYXJBZnRlcjogMTAwMH0pXHJcbiAgICAgICAgfSkgXHJcbiAgICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgfSxcclxuICAgIHVzZXJDYW5jZWw6KCkgPT4geyAgICAgIFxyXG4gICAgICB0aGlzLnByaXZGdW5jLnVzZXJMb2FkKClcclxuICAgIH0sXHJcbiAgICB1c2VyRGVsZXRlOigpID0+IHtcclxuICAgICAgdGhpcy5mb2ZEaWFsb2dTZXJ2aWNlLm9wZW5ZZXNObyh7ICAgICAgICBcclxuICAgICAgICBxdWVzdGlvbjogYFZvdWxleiB2b3VzIHZyYWltZW50IHN1cHByaW1lciBsZSBjb2xsYWJvcmF0ZXVyID9gXHJcbiAgICAgIH0pLnRoZW4oeWVzID0+IHtcclxuICAgICAgICBpZiAoeWVzKSB7XHJcbiAgICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIuZGVsZXRlKHRoaXMudWlWYXIudXNlcilcclxuICAgICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ2NvbGxhYm9yYXRldXIgc3VwcHJpbcOpJylcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLi8nXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGV9KVxyXG4gICAgICAgICAgfSkgXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfSwgIFxyXG4gICAgdXNlclVzZXJ2aWNlU2F2ZToodVNlcnZpY2U6IGlVc2VydmljZVVJKSA9PiB7XHJcblxyXG4gICAgICBpZiAodVNlcnZpY2UudXNlclVzZXJ2aWNlSWQpIHtcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXJVc2VydmljZS5kZWxldGUodVNlcnZpY2UudXNlclVzZXJ2aWNlSWQpXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zYXZlSXNEb25lKClcclxuICAgICAgICAgIHVTZXJ2aWNlLmNoZWNrZWQgPSBmYWxzZVxyXG4gICAgICAgICAgdVNlcnZpY2UudXNlclVzZXJ2aWNlSWQgPSBudWxsXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2gocmVhc29uID0+IHtcclxuICAgICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IHVzZXJTZXJ2aWNlOiBpVXNlclVzZXJ2aWNlID0ge1xyXG4gICAgICAgICAgdXNlcklkOiB0aGlzLnByaVZhci51c2VySWQsXHJcbiAgICAgICAgICB1U2VydmljZUlkOiB1U2VydmljZS5pZFxyXG4gICAgICAgIH1cclxuICBcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXJVc2VydmljZS5jcmVhdGUodXNlclNlcnZpY2UpXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zYXZlSXNEb25lKClcclxuICAgICAgICAgIHVTZXJ2aWNlLmNoZWNrZWQgPSB0cnVlXHJcbiAgICAgICAgICB1U2VydmljZS51c2VyVXNlcnZpY2VJZCA9IHJlc3VsdC5pZFxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgfSwgIFxyXG4gICAgb3JnYW5pc2F0aW9uTXVsdGlTZWxlY3RlZENoYW5nZToob3JnYW5pemF0aW9uczogaU9yZ2FuaXphdGlvbltdKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmIChvcmdhbml6YXRpb25zICYmIG9yZ2FuaXphdGlvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMucHJpVmFyLnVzZXJPcmdhbml6YXRpb25JZCA9IG9yZ2FuaXphdGlvbnNbMF0uaWRcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnByaVZhci51c2VyT3JnYW5pemF0aW9uSWQgPSBudWxsXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLy8gQW5ndWxhciBldmVudHNcclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuYWN0aXZhdGVkUm91dGUucGFyYW1NYXAuc3Vic2NyaWJlKHBhcmFtcyA9PiB7XHJcbiAgICAgIGNvbnN0IGNvZGU6YW55ID0gcGFyYW1zLmdldCgnY29kZScpXHJcblxyXG4gICAgICBpZiAoY29kZSkge1xyXG4gICAgICAgIGlmIChjb2RlLnRvTG93ZXJDYXNlKCkgPT0gJ25ldycpIHtcclxuICAgICAgICAgIHRoaXMudWlWYXIudXNlcklzTmV3ID0gdHJ1ZVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnByaVZhci51c2VySWQgPSBjb2RlXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnRpdGxlID0gYE1vZGlmaWNhdGlvbiBkJ3VuIGNvbGxhYm9yYXRldXJgXHJcbiAgICAgICAgICB0aGlzLnByaXZGdW5jLnVzZXJMb2FkKClcclxuICAgICAgICB9XHJcbiAgICAgIH0gICAgICBcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcbiIsIjxtYXQtY2FyZCBjbGFzcz1cImZvZi1oZWFkZXJcIj4gICAgICAgIFxyXG4gIDxoMz57eyB1aVZhci50aXRsZSB9fTwvaDM+IFxyXG4gIDxkaXYgY2xhc3M9XCJmb2YtdG9vbGJhclwiPlxyXG4gICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b25cclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVzZXJDYW5jZWwoKVwiPkFubnVsZXI8L2J1dHRvbj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwid2FyblwiXHJcbiAgICAgIChjbGljayk9XCJ1aUFjdGlvbi51c2VyRGVsZXRlKClcIj5TdXBwcmltZXI8L2J1dHRvbj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwiYWNjZW50XCJcclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVzZXJTYXZlKClcIj5cclxuICAgICAgRW5yZWdpc3RlcjwvYnV0dG9uPiAgICBcclxuICA8L2Rpdj4gXHJcbjwvbWF0LWNhcmQ+XHJcbjxkaXYgY2xhc3M9XCJtYWluXCI+ICBcclxuICA8ZGl2ICpuZ0lmPVwiIXVpVmFyLmxvYWRpbmdVc2VyXCIgY2xhc3M9XCJkZXRhaWwgZm9mLWZhZGUtaW5cIj4gICAgXHJcblxyXG4gICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuXHJcbiAgICAgICAgPGZvcm0gW2Zvcm1Hcm91cF09XCJ1aVZhci5mb3JtXCI+XHJcblxyXG4gICAgICAgICAgPG1hdC1mb3JtLWZpZWxkPlxyXG4gICAgICAgICAgICA8aW5wdXQgbWF0SW5wdXQgXHJcbiAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwibG9naW5cIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwibG9naW5cIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgICAgICA8bWF0LWVycm9yICpuZ0lmPVwidWlWYXIuZm9ybS5nZXQoJ2xvZ2luJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICAgIExlIGxvZ2luIG5lIGRvaXQgcGFzIGV4Y8OpZGVyIDMwIGNhcmFjdMOocmVzXHJcbiAgICAgICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuXHJcbiAgICAgICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgICAgIDxpbnB1dCBtYXRJbnB1dCByZXF1aXJlZCBcclxuICAgICAgICAgICAgICB0eXBlPVwiZW1haWxcIlxyXG4gICAgICAgICAgICAgIGZvcm1Db250cm9sTmFtZT1cImVtYWlsXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVtYWlsXCIgdmFsdWU9XCJcIj5cclxuICAgICAgICAgICAgPG1hdC1lcnJvciAqbmdJZj1cInVpVmFyLmZvcm0uZ2V0KCdlbWFpbCcpLmludmFsaWRcIj5cclxuICAgICAgICAgICAgICBVbiBlbWFpbCB2YWxpZGUgZXN0IG9ibGlnYXRvaXJlXHJcbiAgICAgICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuXHJcbiAgICAgICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgICAgIDxpbnB1dCBtYXRJbnB1dCByZXF1aXJlZCBcclxuICAgICAgICAgICAgICB0eXBlPVwiZmlyc3ROYW1lXCJcclxuICAgICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJmaXJzdE5hbWVcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiUHLDqW5vbVwiIHZhbHVlPVwiXCI+XHJcbiAgICAgICAgICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJ1aVZhci5mb3JtLmdldCgnZmlyc3ROYW1lJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICAgIExlIHByw6lub20gbmUgZG9pdCBwYXMgZXhjw6lkZXIgMzAgY2FyYWN0w6hyZXNcclxuICAgICAgICAgICAgPC9tYXQtZXJyb3I+XHJcbiAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG5cclxuICAgICAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICAgICAgPGlucHV0IG1hdElucHV0IHJlcXVpcmVkIFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJsYXN0TmFtZVwiXHJcbiAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwibGFzdE5hbWVcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiTm9tIGRlIGZhbWlsbGVcIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgICAgICA8bWF0LWVycm9yICpuZ0lmPVwidWlWYXIuZm9ybS5nZXQoJ2xhc3ROYW1lJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICAgIExlIG5vbSBkZSBmYW1pbGxlIG5lIGRvaXQgcGFzIGV4Y8OpZGVyIDMwIGNhcmFjdMOocmVzXHJcbiAgICAgICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD4gICAgIFxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICA8Zm9mLWNvcmUtZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0XHJcbiAgICAgICAgICAgIFtwbGFjZUhvbGRlcl09XCInT3JnYW5pc2F0aW9uIGRlIHJhdHRhY2hlbWVudCdcIlxyXG4gICAgICAgICAgICBbbXVsdGlTZWxlY3RdPVwiZmFsc2VcIiAgICBcclxuICAgICAgICAgICAgW3NlbGVjdGVkT3JnYW5pc2F0aW9uc109XCJ1aVZhci51c2VyT3JnYW5pemF0aW9uc0Rpc3BsYXlcIiAgIFxyXG4gICAgICAgICAgICAoc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlKSA9IFwidWlBY3Rpb24ub3JnYW5pc2F0aW9uTXVsdGlTZWxlY3RlZENoYW5nZSgkZXZlbnQpXCIgICBcclxuICAgICAgICAgID48L2ZvZi1jb3JlLWZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdD5cclxuICAgICAgIFxyXG4gICAgICAgIDwvZm9ybT5cclxuICAgICAgXHJcbiAgICAgIDwvZGl2PlxyXG4gICAgIFxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgICA8aDM+QWNjw6hzIGR1IGNvbGxhYm9yYXRldXIgYXV4IG1pbmktc2VydmljZXM8L2gzPlxyXG4gICAgICAgIDxtYXQtbGlzdD5cclxuICAgICAgICAgXHJcbiAgICAgICAgICA8bWF0LWFjY29yZGlvbiBtdWx0aT1cInRydWVcIj5cclxuICAgICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdGb3I9XCJsZXQgdVNlcnZpY2Ugb2YgdWlWYXIudVNlcnZpY2VzQWxsOyBsZXQgaSA9IGluZGV4XCI+XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgPG1hdC1leHBhbnNpb24tcGFuZWwgKm5nSWY9XCJ1U2VydmljZS5hdmFpbGFibGVGb3JVc2Vyc1wiPlxyXG4gICAgICAgICAgICAgICAgICA8bWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXIgY2xhc3M9XCJ1c2VydmljZS1hY2Nlc3MtaGVhZGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG1hdC1jaGVja2JveCBbY2hlY2tlZF09XCJ1U2VydmljZS5jaGVja2VkXCJcclxuICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCIkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XCJcclxuICAgICAgICAgICAgICAgICAgICAgIChjaGFuZ2UpPVwidWlBY3Rpb24udXNlclVzZXJ2aWNlU2F2ZSh1U2VydmljZSlcIj5cclxuICAgICAgICAgICAgICAgICAgICAgIDwvbWF0LWNoZWNrYm94PjxkaXYgY2xhc3M9XCJjaGVjay1sYWJlbFwiPnt7IHVTZXJ2aWNlLm5hbWUgfX08L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgPC9tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlcj4gICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgVGhpcyB0aGUgZXhwYW5zaW9uIDIgY29udGVudCAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICA8L21hdC1leHBhbnNpb24tcGFuZWw+ICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgPC9tYXQtYWNjb3JkaW9uPlxyXG5cclxuICAgICAgICA8L21hdC1saXN0PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgKm5nSWY9XCJ1aVZhci5sb2FkaW5nVXNlclwiIGNsYXNzPVwiZm9mLWxvYWRpbmdcIj5cclxuICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5DaGFyZ2VtZW50cyBkZXMgdXRpbGlzYXRldXJzIGV0IGRlcyBhdXRob3Jpc2F0aW9ucy4uLjwvc3Bhbj5cclxuICA8L2Rpdj5cclxuICAgIFxyXG48L2Rpdj5cclxuXHJcbjwhLS0gPGZvZi1lbnRpdHktZm9vdGVyXHJcbiAgW2VudGl0eUJhc2VdPVwidWlWYXIudXNlclwiPlxyXG48L2ZvZi1lbnRpdHktZm9vdGVyPiAtLT5cclxuIl19
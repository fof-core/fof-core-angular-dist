import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FofAuthGuard } from '../core/auth.guard';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
const routes = [
    { path: '', canActivate: [FofAuthGuard],
        children: [
            { path: '',
                children: [
                    { path: '', component: UsersComponent },
                    { path: ':code', component: UserComponent }
                ]
            },
        ]
    }
];
export class UsersRoutingModule {
}
UsersRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: UsersRoutingModule });
UsersRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UsersRoutingModule_Factory(t) { return new (t || UsersRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
        RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UsersRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UsersRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnMtcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi91c2Vycy91c2Vycy1yb3V0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsWUFBWSxFQUFDLE1BQU0sb0JBQW9CLENBQUE7QUFDaEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFBO0FBQ3hELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQTs7O0FBRXJELE1BQU0sTUFBTSxHQUFXO0lBQ3JCLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsQ0FBQyxZQUFZLENBQUM7UUFDckMsUUFBUSxFQUFFO1lBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRTtnQkFDUixRQUFRLEVBQUU7b0JBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUU7b0JBQ3ZDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFO2lCQUM1QzthQUNGO1NBQ0Y7S0FDRjtDQUNGLENBQUE7QUFNRCxNQUFNLE9BQU8sa0JBQWtCOztzREFBbEIsa0JBQWtCO21IQUFsQixrQkFBa0Isa0JBSHBCLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixZQUFZO3dGQUVYLGtCQUFrQiwwQ0FGbkIsWUFBWTtrREFFWCxrQkFBa0I7Y0FKOUIsUUFBUTtlQUFDO2dCQUNSLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQzthQUN4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgUm91dGVzLCBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXHJcbmltcG9ydCB7IEZvZkF1dGhHdWFyZH0gZnJvbSAnLi4vY29yZS9hdXRoLmd1YXJkJ1xyXG5pbXBvcnQgeyBVc2Vyc0NvbXBvbmVudCB9IGZyb20gJy4vdXNlcnMvdXNlcnMuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBVc2VyQ29tcG9uZW50IH0gZnJvbSAnLi91c2VyL3VzZXIuY29tcG9uZW50J1xyXG5cclxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXHJcbiAgeyBwYXRoOiAnJywgY2FuQWN0aXZhdGU6IFtGb2ZBdXRoR3VhcmRdLFxyXG4gICAgY2hpbGRyZW46IFtcclxuICAgICAgeyBwYXRoOiAnJyxcclxuICAgICAgICBjaGlsZHJlbjogW1xyXG4gICAgICAgICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBVc2Vyc0NvbXBvbmVudCB9LCAgICAgICAgICAgIFxyXG4gICAgICAgICAgeyBwYXRoOiAnOmNvZGUnLCBjb21wb25lbnQ6IFVzZXJDb21wb25lbnQgfVxyXG4gICAgICAgIF1cclxuICAgICAgfSxcclxuICAgIF1cclxuICB9ICAgXHJcbl1cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1JvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXSxcclxuICBleHBvcnRzOiBbUm91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlcnNSb3V0aW5nTW9kdWxlIHsgfVxyXG4iXX0=
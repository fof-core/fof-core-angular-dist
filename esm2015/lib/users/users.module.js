import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../core/material.module';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '../components/components.module';
import { UservicesModule } from '../uservices/uservices.module';
import * as i0 from "@angular/core";
export class UsersModule {
}
UsersModule.ɵmod = i0.ɵɵdefineNgModule({ type: UsersModule });
UsersModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UsersModule_Factory(t) { return new (t || UsersModule)(); }, imports: [[
            CommonModule,
            UsersRoutingModule,
            MaterialModule,
            FormsModule,
            ReactiveFormsModule,
            ComponentsModule,
            UservicesModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UsersModule, { declarations: [UsersComponent,
        UserComponent], imports: [CommonModule,
        UsersRoutingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        ComponentsModule,
        UservicesModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UsersModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    UsersComponent,
                    UserComponent
                ],
                imports: [
                    CommonModule,
                    UsersRoutingModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    ComponentsModule,
                    UservicesModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvdXNlcnMvdXNlcnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQTtBQUMzRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUE7QUFDeEQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVCQUF1QixDQUFBO0FBQ3JELE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUNqRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sK0JBQStCLENBQUE7O0FBaUIvRCxNQUFNLE9BQU8sV0FBVzs7K0NBQVgsV0FBVztxR0FBWCxXQUFXLGtCQVZiO1lBQ1AsWUFBWTtZQUNaLGtCQUFrQjtZQUNsQixjQUFjO1lBQ2QsV0FBVztZQUNYLG1CQUFtQjtZQUNuQixnQkFBZ0I7WUFDaEIsZUFBZTtTQUNoQjt3RkFFVSxXQUFXLG1CQWJwQixjQUFjO1FBQ2QsYUFBYSxhQUdiLFlBQVk7UUFDWixrQkFBa0I7UUFDbEIsY0FBYztRQUNkLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsZ0JBQWdCO1FBQ2hCLGVBQWU7a0RBR04sV0FBVztjQWZ2QixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLGNBQWM7b0JBQ2QsYUFBYTtpQkFDZDtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixrQkFBa0I7b0JBQ2xCLGNBQWM7b0JBQ2QsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGdCQUFnQjtvQkFDaEIsZUFBZTtpQkFDaEI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJ1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL2NvcmUvbWF0ZXJpYWwubW9kdWxlJ1xyXG5pbXBvcnQgeyBVc2Vyc1JvdXRpbmdNb2R1bGUgfSBmcm9tICcuL3VzZXJzLXJvdXRpbmcubW9kdWxlJ1xyXG5pbXBvcnQgeyBVc2Vyc0NvbXBvbmVudCB9IGZyb20gJy4vdXNlcnMvdXNlcnMuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBVc2VyQ29tcG9uZW50IH0gZnJvbSAnLi91c2VyL3VzZXIuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJ1xyXG5pbXBvcnQgeyBDb21wb25lbnRzTW9kdWxlIH0gZnJvbSAnLi4vY29tcG9uZW50cy9jb21wb25lbnRzLm1vZHVsZSdcclxuaW1wb3J0IHsgVXNlcnZpY2VzTW9kdWxlIH0gZnJvbSAnLi4vdXNlcnZpY2VzL3VzZXJ2aWNlcy5tb2R1bGUnXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgVXNlcnNDb21wb25lbnQsIFxyXG4gICAgVXNlckNvbXBvbmVudFxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgVXNlcnNSb3V0aW5nTW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSxcclxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICBDb21wb25lbnRzTW9kdWxlLFxyXG4gICAgVXNlcnZpY2VzTW9kdWxlXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlcnNNb2R1bGUgeyB9XHJcbiJdfQ==
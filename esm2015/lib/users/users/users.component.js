import { Component, ViewChild, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import { FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
import * as i4 from "../../core/fof-error.service";
import * as i5 from "@angular/material/card";
import * as i6 from "../../components/fof-organizations-tree/fof-organizations-tree.component";
import * as i7 from "@angular/material/button";
import * as i8 from "@angular/router";
import * as i9 from "@angular/material/form-field";
import * as i10 from "@angular/material/input";
import * as i11 from "@angular/forms";
import * as i12 from "@angular/common";
import * as i13 from "@angular/material/table";
import * as i14 from "@angular/material/sort";
import * as i15 from "@angular/material/paginator";
import * as i16 from "@angular/material/progress-spinner";
function UsersComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 21);
    i0.ɵɵelement(1, "mat-spinner", 22);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des collaborateurs...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function UsersComponent_th_22_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Email");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_23_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r135 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r135.email);
} }
function UsersComponent_th_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Login");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_26_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r136 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r136.login);
} }
function UsersComponent_th_28_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Pr\u00E9nom");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_29_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r137 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r137.firstName);
} }
function UsersComponent_th_31_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Nom");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_32_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r138 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r138.lastName);
} }
function UsersComponent_tr_33_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 25);
} }
function UsersComponent_tr_34_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 26);
} if (rf & 2) {
    const row_r139 = ctx.$implicit;
    i0.ɵɵproperty("routerLink", row_r139.id);
} }
const _c0 = function () { return [5, 10, 25, 100]; };
export class UsersComponent {
    constructor(fofPermissionService, fofNotificationService, breakpointObserver, fofErrorService) {
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
            filter: undefined,
            filterOrganizationsChange: new EventEmitter(),
            filterOrganizations: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: [],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true,
            searchUsersCtrl: new FormControl()
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: (organization) => {
                if (organization) {
                    this.priVar.filterOrganizations = [organization.id];
                }
                else {
                    this.priVar.filterOrganizations = null;
                }
                this.priVar.filterOrganizationsChange.emit(organization);
            }
        };
    }
    // Angular events
    ngOnInit() {
        this.priVar.filter = this.uiVar.searchUsersCtrl.valueChanges
            .pipe(debounceTime(500), distinctUntilChanged(), filter(query => query.length >= 3 || query.length === 0));
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe((state) => {
            if (state.matches) {
                // XSmall
                this.uiVar.displayedColumns = ['email', 'login'];
            }
            else {
                // > XSmall
                this.uiVar.displayedColumns = ['email', 'login', 'firstName', 'lastName'];
            }
        });
    }
    ngAfterViewInit() {
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page, this.priVar.filter, this.priVar.filterOrganizationsChange)
            .pipe(startWith({}), switchMap(() => {
            this.uiVar.isLoadingResults = true;
            this.uiVar.pageSize = this.paginator.pageSize;
            return this.fofPermissionService.user.search(this.uiVar.searchUsersCtrl.value, this.priVar.filterOrganizations, this.uiVar.pageSize, this.paginator.pageIndex, this.sort.active, this.sort.direction);
        }), map((search) => {
            this.uiVar.isLoadingResults = false;
            this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(error => {
            this.fofErrorService.errorManage(error);
            this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(data => {
            this.uiVar.data = data;
        });
    }
    ngOnDestroy() {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    }
}
UsersComponent.ɵfac = function UsersComponent_Factory(t) { return new (t || UsersComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver), i0.ɵɵdirectiveInject(i4.FofErrorService)); };
UsersComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UsersComponent, selectors: [["fof-core-users"]], viewQuery: function UsersComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(MatPaginator, true);
        i0.ɵɵviewQuery(MatSort, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
    } }, decls: 36, vars: 11, consts: [[1, "row"], [1, "col-md-3"], [1, "card-tree"], [3, "multiSelect", "selectedOrganizationsChange"], [1, "col-md-9"], [1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "filtres"], ["autofocus", "", "matInput", "", "placeholder", "email ou nom ou pr\u00E9nom ou login", 3, "formControl"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "email", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "email"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "login"], ["matColumnDef", "firstName"], ["matColumnDef", "lastName"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UsersComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1);
        i0.ɵɵelementStart(2, "mat-card", 2);
        i0.ɵɵelementStart(3, "fof-organizations-tree", 3);
        i0.ɵɵlistener("selectedOrganizationsChange", function UsersComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(4, "div", 4);
        i0.ɵɵelementStart(5, "mat-card", 5);
        i0.ɵɵelementStart(6, "h3");
        i0.ɵɵtext(7, "Gestion des collaborateurs");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(8, "a", 6);
        i0.ɵɵelementStart(9, "span");
        i0.ɵɵtext(10, "Ajouter un collaborateur");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(11, "mat-card", 7);
        i0.ɵɵelementStart(12, "mat-form-field");
        i0.ɵɵelementStart(13, "mat-label");
        i0.ɵɵtext(14, "Filtre");
        i0.ɵɵelementEnd();
        i0.ɵɵelement(15, "input", 8);
        i0.ɵɵelementStart(16, "mat-hint");
        i0.ɵɵtext(17, "Recherche \u00E0 partir de 3 caract\u00E8res saisies");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(18, "div", 9);
        i0.ɵɵtemplate(19, UsersComponent_div_19_Template, 4, 0, "div", 10);
        i0.ɵɵelementStart(20, "table", 11);
        i0.ɵɵelementContainerStart(21, 12);
        i0.ɵɵtemplate(22, UsersComponent_th_22_Template, 2, 0, "th", 13);
        i0.ɵɵtemplate(23, UsersComponent_td_23_Template, 2, 1, "td", 14);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(24, 15);
        i0.ɵɵtemplate(25, UsersComponent_th_25_Template, 2, 0, "th", 13);
        i0.ɵɵtemplate(26, UsersComponent_td_26_Template, 2, 1, "td", 14);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(27, 16);
        i0.ɵɵtemplate(28, UsersComponent_th_28_Template, 2, 0, "th", 13);
        i0.ɵɵtemplate(29, UsersComponent_td_29_Template, 2, 1, "td", 14);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(30, 17);
        i0.ɵɵtemplate(31, UsersComponent_th_31_Template, 2, 0, "th", 13);
        i0.ɵɵtemplate(32, UsersComponent_td_32_Template, 2, 1, "td", 14);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵtemplate(33, UsersComponent_tr_33_Template, 1, 0, "tr", 18);
        i0.ɵɵtemplate(34, UsersComponent_tr_34_Template, 1, 1, "tr", 19);
        i0.ɵɵelementEnd();
        i0.ɵɵelement(35, "mat-paginator", 20);
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("multiSelect", false);
        i0.ɵɵadvance(5);
        i0.ɵɵproperty("routerLink", "/admin/users/new");
        i0.ɵɵadvance(7);
        i0.ɵɵproperty("formControl", ctx.uiVar.searchUsersCtrl);
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("dataSource", ctx.uiVar.data);
        i0.ɵɵadvance(13);
        i0.ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", i0.ɵɵpureFunction0(10, _c0))("pageSize", ctx.uiVar.pageSize);
    } }, directives: [i5.MatCard, i6.FofOrganizationsTreeComponent, i7.MatAnchor, i8.RouterLinkWithHref, i9.MatFormField, i9.MatLabel, i10.MatInput, i11.DefaultValueAccessor, i11.NgControlStatus, i11.FormControlDirective, i9.MatHint, i12.NgIf, i13.MatTable, i14.MatSort, i13.MatColumnDef, i13.MatHeaderCellDef, i13.MatCellDef, i13.MatHeaderRowDef, i13.MatRowDef, i15.MatPaginator, i16.MatSpinner, i13.MatHeaderCell, i14.MatSortHeader, i13.MatCell, i13.MatHeaderRow, i13.MatRow, i8.RouterLink], styles: [".filtres[_ngcontent-%COMP%]{margin-bottom:15px}.filtres[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.card-tree[_ngcontent-%COMP%]{height:100%}"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UsersComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-users',
                templateUrl: './users.component.html',
                styleUrls: ['./users.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }, { type: i4.FofErrorService }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvdXNlcnMvdXNlcnMvdXNlcnMuY29tcG9uZW50LnRzIiwibGliL3VzZXJzL3VzZXJzL3VzZXJzLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQW1DLFNBQVMsRUFDakQsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBSWhELE9BQU8sRUFBRSxZQUFZLEVBQW9CLE1BQU0sNkJBQTZCLENBQUE7QUFDNUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBQ2hELE9BQU8sRUFBRSxLQUFLLEVBQWMsRUFBRSxJQUFJLFlBQVksRUFBaUMsTUFBTSxNQUFNLENBQUE7QUFDM0YsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQzFELG9CQUFvQixFQUFZLE1BQU0sRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBRWhFLE9BQU8sRUFBc0IsV0FBVyxFQUFtQixNQUFNLHFCQUFxQixDQUFBO0FBQ3RGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ2tCdEMsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGlEQUFpQztJQUFBLGlCQUFPO0lBQ3hGLGlCQUFNOzs7SUFNRiw4QkFBc0Q7SUFBQSxxQkFBSztJQUFBLGlCQUFLOzs7SUFDaEUsOEJBQW1DO0lBQUEsWUFBYTtJQUFBLGlCQUFLOzs7SUFBbEIsZUFBYTtJQUFiLG9DQUFhOzs7SUFJaEQsOEJBQXNEO0lBQUEscUJBQUs7SUFBQSxpQkFBSzs7O0lBQ2hFLDhCQUFtQztJQUFBLFlBQWE7SUFBQSxpQkFBSzs7O0lBQWxCLGVBQWE7SUFBYixvQ0FBYTs7O0lBSWhELDhCQUFzRDtJQUFBLDJCQUFNO0lBQUEsaUJBQUs7OztJQUNqRSw4QkFBbUM7SUFBQSxZQUFpQjtJQUFBLGlCQUFLOzs7SUFBdEIsZUFBaUI7SUFBakIsd0NBQWlCOzs7SUFJcEQsOEJBQXNEO0lBQUEsbUJBQUc7SUFBQSxpQkFBSzs7O0lBQzlELDhCQUFtQztJQUFBLFlBQWdCO0lBQUEsaUJBQUs7OztJQUFyQixlQUFnQjtJQUFoQix1Q0FBZ0I7OztJQUdyRCx5QkFBa0U7OztJQUNsRSx5QkFDOEQ7OztJQUR6Qix3Q0FBcUI7OztBRHBDbEUsTUFBTSxPQUFPLGNBQWM7SUFJekIsWUFDVSxvQkFBMEMsRUFDMUMsc0JBQThDLEVBQzlDLGtCQUFzQyxFQUN0QyxlQUFnQztRQUhoQyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFLMUMsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRztZQUNmLHFCQUFxQixFQUFnQixTQUFTO1lBQzlDLE1BQU0sRUFBc0IsU0FBUztZQUNyQyx5QkFBeUIsRUFBK0IsSUFBSSxZQUFZLEVBQUU7WUFDMUUsbUJBQW1CLEVBQVksU0FBUztTQUN6QyxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRyxFQUNsQixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLGdCQUFnQixFQUFZLEVBQUU7WUFDOUIsSUFBSSxFQUFXLEVBQUU7WUFDakIsYUFBYSxFQUFFLENBQUM7WUFDaEIsUUFBUSxFQUFFLENBQUM7WUFDWCxnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLGVBQWUsRUFBRSxJQUFJLFdBQVcsRUFBRTtTQUNuQyxDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRztZQUNoQiwrQkFBK0IsRUFBQyxDQUFDLFlBQTJCLEVBQUUsRUFBRTtnQkFDOUQsSUFBSSxZQUFZLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUE7aUJBQ3BEO3FCQUFNO29CQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFBO2lCQUN2QztnQkFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLHlCQUF5QixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQTtZQUMxRCxDQUFDO1NBQ0YsQ0FBQTtJQS9CRCxDQUFDO0lBZ0NELGlCQUFpQjtJQUNqQixRQUFRO1FBQ04sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsWUFBWTthQUN6RCxJQUFJLENBQ0gsWUFBWSxDQUFDLEdBQUcsQ0FBQyxFQUNqQixvQkFBb0IsRUFBRSxFQUN0QixNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUN6RCxDQUFBO1FBRUgsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDdEYsU0FBUyxDQUFDLENBQUMsS0FBc0IsRUFBRSxFQUFFO1lBQ3BDLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtnQkFDakIsU0FBUztnQkFDVCxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFBO2FBQ2pEO2lCQUFNO2dCQUNMLFdBQVc7Z0JBQ1gsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLE9BQU8sRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQyxDQUFBO2FBQzFFO1FBQ0gsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsZUFBZTtRQUNiLG9FQUFvRTtRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUE7UUFFbEUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUM7YUFDeEcsSUFBSSxDQUNILFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUE7WUFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUE7WUFFN0MsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FDMUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsS0FBSyxFQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUNwRSxDQUFDLENBQUMsRUFDRixHQUFHLENBQUMsQ0FBQyxNQUF5QixFQUFFLEVBQUU7WUFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUE7WUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQTtZQUN2QyxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUE7UUFDcEIsQ0FBQyxDQUFDLEVBQ0YsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO1lBQ3ZDLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLE9BQU8sWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1FBQ3pCLENBQUMsQ0FBQyxDQUNILENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQTtRQUN4QixDQUFDLENBQUMsQ0FBQTtJQUNOLENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFO1lBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUFFO0lBQzVGLENBQUM7OzRFQWxHVSxjQUFjO21EQUFkLGNBQWM7dUJBQ2QsWUFBWTt1QkFDWixPQUFPOzs7Ozs7UUN4QnBCLDhCQUNFO1FBQUEsOEJBQ0U7UUFBQSxtQ0FDRTtRQUFBLGlEQUd5QjtRQUZ2Qiw2SkFBK0Isb0RBQWdELElBQUM7UUFFbEYsaUJBQXlCO1FBQzNCLGlCQUFXO1FBQ2IsaUJBQU07UUFDTiw4QkFFRTtRQUFBLG1DQUNFO1FBQUEsMEJBQUk7UUFBQSwwQ0FBMEI7UUFBQSxpQkFBSztRQUNuQyw0QkFFRTtRQUFBLDRCQUFNO1FBQUEseUNBQXdCO1FBQUEsaUJBQU87UUFDdkMsaUJBQUk7UUFDTixpQkFBVztRQUVYLG9DQUNFO1FBQUEsdUNBQ0U7UUFBQSxrQ0FBVztRQUFBLHVCQUFNO1FBQUEsaUJBQVk7UUFDN0IsNEJBRUE7UUFBQSxpQ0FBVTtRQUFBLHFFQUEwQztRQUFBLGlCQUFXO1FBQ2pFLGlCQUFpQjtRQUNuQixpQkFBVztRQUVYLCtCQUVFO1FBQUEsa0VBQ0U7UUFHRixrQ0FHRTtRQUFBLGtDQUNFO1FBQUEsZ0VBQXNEO1FBQ3RELGdFQUFtQztRQUNyQywwQkFBZTtRQUVmLGtDQUNFO1FBQUEsZ0VBQXNEO1FBQ3RELGdFQUFtQztRQUNyQywwQkFBZTtRQUVmLGtDQUNFO1FBQUEsZ0VBQXNEO1FBQ3RELGdFQUFtQztRQUNyQywwQkFBZTtRQUVmLGtDQUNFO1FBQUEsZ0VBQXNEO1FBQ3RELGdFQUFtQztRQUNyQywwQkFBZTtRQUVmLGdFQUE2RDtRQUM3RCxnRUFDeUQ7UUFDM0QsaUJBQVE7UUFFUixxQ0FFOEM7UUFFaEQsaUJBQU07UUFFUixpQkFBTTtRQUNSLGlCQUFNOztRQWhFRSxlQUFxQjtRQUFyQixtQ0FBcUI7UUFTckIsZUFBaUM7UUFBakMsK0NBQWlDO1FBUy9CLGVBQXFDO1FBQXJDLHVEQUFxQztRQU9JLGVBQThCO1FBQTlCLGlEQUE4QjtRQUkxRCxlQUF5QjtRQUF6QiwyQ0FBeUI7UUF1QnJCLGdCQUF5QztRQUF6Qyw0REFBeUM7UUFFMUQsZUFBc0Q7UUFBdEQsNkRBQXNEO1FBRzNDLGVBQThCO1FBQTlCLGdEQUE4QixnREFBQSxnQ0FBQTs7a0REeEN0QyxjQUFjO2NBTDFCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixXQUFXLEVBQUUsd0JBQXdCO2dCQUNyQyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQzthQUN0Qzs7a0JBRUUsU0FBUzttQkFBQyxZQUFZOztrQkFDdEIsU0FBUzttQkFBQyxPQUFPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIFxyXG4gIE9uRGVzdHJveSwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgaVVzZXIgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFBhZ2luYXRvckludGwgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InXHJcbmltcG9ydCB7IE1hdFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0J1xyXG5pbXBvcnQgeyBtZXJnZSwgT2JzZXJ2YWJsZSwgb2YgYXMgb2JzZXJ2YWJsZU9mLCBTdWJzY3JpcHRpb24sIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnXHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCwgc3RhcnRXaXRoLCBzd2l0Y2hNYXAsIGRlYm91bmNlVGltZSwgdGFwLCBcclxuICBkaXN0aW5jdFVudGlsQ2hhbmdlZCwgZmluYWxpemUsIGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJ1xyXG5pbXBvcnQgeyBpZm9mU2VhcmNoIH0gZnJvbSAnLi4vLi4vY29yZS9jb3JlLmludGVyZmFjZSdcclxuaW1wb3J0IHsgQnJlYWtwb2ludE9ic2VydmVyLCBCcmVha3BvaW50cywgQnJlYWtwb2ludFN0YXRlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCdcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3JtcydcclxuaW1wb3J0IHsgRm9mRXJyb3JTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtZXJyb3Iuc2VydmljZSdcclxuaW1wb3J0IHsgaU9yZ2FuaXphdGlvbiB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IE9ic2VydmVyc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9vYnNlcnZlcnMnXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1jb3JlLXVzZXJzJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vdXNlcnMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3VzZXJzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFVzZXJzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xyXG4gIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yKSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvclxyXG4gIEBWaWV3Q2hpbGQoTWF0U29ydCkgc29ydDogTWF0U29ydFxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9mUGVybWlzc2lvblNlcnZpY2U6IEZvZlBlcm1pc3Npb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBicmVha3BvaW50T2JzZXJ2ZXI6IEJyZWFrcG9pbnRPYnNlcnZlcixcclxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2VcclxuICApIHsgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgYnJlYWtwb2ludE9ic2VydmVyU3ViOiA8U3Vic2NyaXB0aW9uPnVuZGVmaW5lZCwgICAgXHJcbiAgICBmaWx0ZXI6IDxPYnNlcnZhYmxlPHN0cmluZz4+dW5kZWZpbmVkLFxyXG4gICAgZmlsdGVyT3JnYW5pemF0aW9uc0NoYW5nZTogPEV2ZW50RW1pdHRlcjxpT3JnYW5pemF0aW9uPj5uZXcgRXZlbnRFbWl0dGVyKCksXHJcbiAgICBmaWx0ZXJPcmdhbml6YXRpb25zOiA8bnVtYmVyW10+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0geyAgICBcclxuICAgIGRpc3BsYXllZENvbHVtbnM6IDxzdHJpbmdbXT5bXSxcclxuICAgIGRhdGE6IDxpVXNlcltdPltdLFxyXG4gICAgcmVzdWx0c0xlbmd0aDogMCxcclxuICAgIHBhZ2VTaXplOiA1LFxyXG4gICAgaXNMb2FkaW5nUmVzdWx0czogdHJ1ZSxcclxuICAgIHNlYXJjaFVzZXJzQ3RybDogbmV3IEZvcm1Db250cm9sKCkgICAgXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcclxuICAgIG9yZ2FuaXNhdGlvbk11bHRpU2VsZWN0ZWRDaGFuZ2U6KG9yZ2FuaXphdGlvbjogaU9yZ2FuaXphdGlvbikgPT4geyAgICAgIFxyXG4gICAgICBpZiAob3JnYW5pemF0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9ucyA9IFtvcmdhbml6YXRpb24uaWRdXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9ucyA9IG51bGxcclxuICAgICAgfSAgICAgIFxyXG4gICAgICB0aGlzLnByaVZhci5maWx0ZXJPcmdhbml6YXRpb25zQ2hhbmdlLmVtaXQob3JnYW5pemF0aW9uKSAgICAgIFxyXG4gICAgfVxyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5wcmlWYXIuZmlsdGVyID0gdGhpcy51aVZhci5zZWFyY2hVc2Vyc0N0cmwudmFsdWVDaGFuZ2VzXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGRlYm91bmNlVGltZSg1MDApLFxyXG4gICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKCksICAgICAgICBcclxuICAgICAgICBmaWx0ZXIocXVlcnkgPT4gcXVlcnkubGVuZ3RoID49IDMgfHwgcXVlcnkubGVuZ3RoID09PSAwKSAgICAgICAgICAgICBcclxuICAgICAgKSBcclxuICAgIFxyXG4gICAgdGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViID0gdGhpcy5icmVha3BvaW50T2JzZXJ2ZXIub2JzZXJ2ZShCcmVha3BvaW50cy5YU21hbGwpXHJcbiAgICAuc3Vic2NyaWJlKChzdGF0ZTogQnJlYWtwb2ludFN0YXRlKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmIChzdGF0ZS5tYXRjaGVzKSB7XHJcbiAgICAgICAgLy8gWFNtYWxsXHJcbiAgICAgICAgdGhpcy51aVZhci5kaXNwbGF5ZWRDb2x1bW5zID0gWydlbWFpbCcsICdsb2dpbiddXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gPiBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ2VtYWlsJywgJ2xvZ2luJywgJ2ZpcnN0TmFtZScsICdsYXN0TmFtZSddXHJcbiAgICAgIH1cclxuICAgIH0pICAgXHJcbiAgfSAgXHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHsgICAgXHJcbiAgICAvLyBJZiB0aGUgdXNlciBjaGFuZ2VzIHRoZSBzb3J0IG9yZGVyLCByZXNldCBiYWNrIHRvIHRoZSBmaXJzdCBwYWdlLlxyXG4gICAgdGhpcy5zb3J0LnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCA9IDApXHJcblxyXG4gICAgbWVyZ2UodGhpcy5zb3J0LnNvcnRDaGFuZ2UsIHRoaXMucGFnaW5hdG9yLnBhZ2UsIHRoaXMucHJpVmFyLmZpbHRlciwgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9uc0NoYW5nZSlcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgc3RhcnRXaXRoKHt9KSxcclxuICAgICAgICBzd2l0Y2hNYXAoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gdHJ1ZSAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMudWlWYXIucGFnZVNpemUgPSB0aGlzLnBhZ2luYXRvci5wYWdlU2l6ZVxyXG5cclxuICAgICAgICAgIHJldHVybiB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIuc2VhcmNoKCAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnVpVmFyLnNlYXJjaFVzZXJzQ3RybC52YWx1ZSxcclxuICAgICAgICAgICAgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9ucyxcclxuICAgICAgICAgICAgdGhpcy51aVZhci5wYWdlU2l6ZSwgXHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCwgdGhpcy5zb3J0LmFjdGl2ZSwgdGhpcy5zb3J0LmRpcmVjdGlvbilcclxuICAgICAgICB9KSxcclxuICAgICAgICBtYXAoKHNlYXJjaDogaWZvZlNlYXJjaDxpVXNlcj4pID0+IHsgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMudWlWYXIucmVzdWx0c0xlbmd0aCA9IHNlYXJjaC50b3RhbCAgICAgICAgICBcclxuICAgICAgICAgIHJldHVybiBzZWFyY2guZGF0YVxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyb3IgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UoZXJyb3IpICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2UgICAgICAgICAgXHJcbiAgICAgICAgICByZXR1cm4gb2JzZXJ2YWJsZU9mKFtdKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICkuc3Vic2NyaWJlKGRhdGEgPT4geyAgICAgICAgXHJcbiAgICAgICAgdGhpcy51aVZhci5kYXRhID0gZGF0YVxyXG4gICAgICB9KVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViKSB7IHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1Yi51bnN1YnNjcmliZSgpIH1cclxuICB9XHJcblxyXG59XHJcbiIsIjxkaXYgY2xhc3M9XCJyb3dcIj5cclxuICA8ZGl2IGNsYXNzPVwiY29sLW1kLTNcIj5cclxuICAgIDxtYXQtY2FyZCBjbGFzcz1cImNhcmQtdHJlZVwiPlxyXG4gICAgICA8Zm9mLW9yZ2FuaXphdGlvbnMtdHJlZVxyXG4gICAgICAgIChzZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2UpPVwidWlBY3Rpb24ub3JnYW5pc2F0aW9uTXVsdGlTZWxlY3RlZENoYW5nZSgkZXZlbnQpXCJcclxuICAgICAgICBbbXVsdGlTZWxlY3RdPVwiZmFsc2VcIj5cclxuICAgICAgPC9mb2Ytb3JnYW5pemF0aW9ucy10cmVlPlxyXG4gICAgPC9tYXQtY2FyZD5cclxuICA8L2Rpdj5cclxuICA8ZGl2IGNsYXNzPVwiY29sLW1kLTlcIj5cclxuXHJcbiAgICA8bWF0LWNhcmQgY2xhc3M9XCJmb2YtaGVhZGVyXCI+XHJcbiAgICAgIDxoMz5HZXN0aW9uIGRlcyBjb2xsYWJvcmF0ZXVyczwvaDM+ICAgXHJcbiAgICAgIDxhIG1hdC1zdHJva2VkLWJ1dHRvbiBjb2xvcj1cImFjY2VudFwiIFxyXG4gICAgICAgIFtyb3V0ZXJMaW5rXT1cIicvYWRtaW4vdXNlcnMvbmV3J1wiPiAgICBcclxuICAgICAgICA8c3Bhbj5Bam91dGVyIHVuIGNvbGxhYm9yYXRldXI8L3NwYW4+XHJcbiAgICAgIDwvYT4gICAgXHJcbiAgICA8L21hdC1jYXJkPlxyXG4gICAgXHJcbiAgICA8bWF0LWNhcmQgY2xhc3M9XCJmaWx0cmVzXCI+XHJcbiAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICA8bWF0LWxhYmVsPkZpbHRyZTwvbWF0LWxhYmVsPlxyXG4gICAgICAgIDxpbnB1dCBhdXRvZm9jdXMgbWF0SW5wdXQgcGxhY2Vob2xkZXI9XCJlbWFpbCBvdSBub20gb3UgcHLDqW5vbSBvdSBsb2dpblwiIFxyXG4gICAgICAgICAgW2Zvcm1Db250cm9sXT1cInVpVmFyLnNlYXJjaFVzZXJzQ3RybFwiPlxyXG4gICAgICAgIDxtYXQtaGludD5SZWNoZXJjaGUgw6AgcGFydGlyIGRlIDMgY2FyYWN0w6hyZXMgc2Fpc2llczwvbWF0LWhpbnQ+ICAgIFxyXG4gICAgICA8L21hdC1mb3JtLWZpZWxkPiAgXHJcbiAgICA8L21hdC1jYXJkPiAgXHJcbiAgICBcclxuICAgIDxkaXYgY2xhc3M9XCJmb2YtdGFibGUtY29udGFpbmVyIG1hdC1lbGV2YXRpb24tejJcIj5cclxuICAgIFxyXG4gICAgICA8ZGl2IGNsYXNzPVwidGFibGUtbG9hZGluZy1zaGFkZSBmb2YtbG9hZGluZ1wiICpuZ0lmPVwidWlWYXIuaXNMb2FkaW5nUmVzdWx0c1wiPlxyXG4gICAgICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5DaGFyZ2VtZW50cyBkZXMgY29sbGFib3JhdGV1cnMuLi48L3NwYW4+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgXHJcbiAgICAgIDx0YWJsZSBtYXQtdGFibGUgW2RhdGFTb3VyY2VdPVwidWlWYXIuZGF0YVwiIGNsYXNzPVwiZGF0YS10YWJsZVwiXHJcbiAgICAgICAgICAgIG1hdFNvcnQgbWF0U29ydEFjdGl2ZT1cImVtYWlsXCIgbWF0U29ydERpc2FibGVDbGVhciBtYXRTb3J0RGlyZWN0aW9uPVwiYXNjXCI+XHJcbiAgICAgICAgXHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJlbWFpbFwiPlxyXG4gICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCBtYXQtc29ydC1oZWFkZXIgKm1hdEhlYWRlckNlbGxEZWY+RW1haWw8L3RoPlxyXG4gICAgICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LmVtYWlsfX08L3RkPlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgXHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJsb2dpblwiPlxyXG4gICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCBtYXQtc29ydC1oZWFkZXIgKm1hdEhlYWRlckNlbGxEZWY+TG9naW48L3RoPlxyXG4gICAgICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LmxvZ2lufX08L3RkPlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgXHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJmaXJzdE5hbWVcIj5cclxuICAgICAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgbWF0LXNvcnQtaGVhZGVyICptYXRIZWFkZXJDZWxsRGVmPlByw6lub208L3RoPlxyXG4gICAgICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LmZpcnN0TmFtZX19PC90ZD5cclxuICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgIFxyXG4gICAgICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwibGFzdE5hbWVcIj5cclxuICAgICAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgbWF0LXNvcnQtaGVhZGVyICptYXRIZWFkZXJDZWxsRGVmPk5vbTwvdGg+XHJcbiAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cubGFzdE5hbWV9fTwvdGQ+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICBcclxuICAgICAgICA8dHIgbWF0LWhlYWRlci1yb3cgKm1hdEhlYWRlclJvd0RlZj1cInVpVmFyLmRpc3BsYXllZENvbHVtbnNcIj48L3RyPlxyXG4gICAgICAgIDx0ciBtYXQtcm93IGNsYXNzPVwiZm9mLWVsZW1lbnQtb3ZlclwiIFtyb3V0ZXJMaW5rXT1cInJvdy5pZFwiXHJcbiAgICAgICAgICAqbWF0Um93RGVmPVwibGV0IHJvdzsgY29sdW1uczogdWlWYXIuZGlzcGxheWVkQ29sdW1ucztcIj48L3RyPlxyXG4gICAgICA8L3RhYmxlPlxyXG4gICAgXHJcbiAgICAgIDxtYXQtcGFnaW5hdG9yIFtsZW5ndGhdPVwidWlWYXIucmVzdWx0c0xlbmd0aFwiIFxyXG4gICAgICAgIFtwYWdlU2l6ZU9wdGlvbnNdPVwiWzUsIDEwLCAyNSwgMTAwXVwiXHJcbiAgICAgICAgW3BhZ2VTaXplXT1cInVpVmFyLnBhZ2VTaXplXCI+PC9tYXQtcGFnaW5hdG9yPlxyXG4gICAgXHJcbiAgICA8L2Rpdj5cclxuICAgIFxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuIl19
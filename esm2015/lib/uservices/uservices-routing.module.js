import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FofAuthGuard } from '../core/auth.guard';
import { UservicesComponent } from './uservices/uservices.component';
import { UserviceComponent } from './uservice/uservice.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
const routes = [
    { path: '', canActivate: [FofAuthGuard],
        children: [
            { path: '',
                children: [
                    { path: '', component: UservicesComponent },
                    { path: ':id', component: UserviceComponent }
                ]
            },
        ]
    }
];
export class UservicesRoutingModule {
}
UservicesRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: UservicesRoutingModule });
UservicesRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UservicesRoutingModule_Factory(t) { return new (t || UservicesRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
        RouterModule] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UservicesRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvdXNlcnZpY2VzL3VzZXJ2aWNlcy1yb3V0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsWUFBWSxFQUFDLE1BQU0sb0JBQW9CLENBQUE7QUFDaEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0saUNBQWlDLENBQUE7QUFDcEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0JBQStCLENBQUE7OztBQUVqRSxNQUFNLE1BQU0sR0FBVztJQUNyQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsV0FBVyxFQUFFLENBQUMsWUFBWSxDQUFDO1FBQ3JDLFFBQVEsRUFBRTtZQUNSLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsUUFBUSxFQUFFO29CQUNSLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUU7b0JBQzNDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUU7aUJBQzlDO2FBQ0Y7U0FDRjtLQUNGO0NBQ0YsQ0FBQTtBQU1ELE1BQU0sT0FBTyxzQkFBc0I7OzBEQUF0QixzQkFBc0I7MkhBQXRCLHNCQUFzQixrQkFIeEIsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLFlBQVk7d0ZBRVgsc0JBQXNCLDBDQUZ2QixZQUFZO2tEQUVYLHNCQUFzQjtjQUpsQyxRQUFRO2VBQUM7Z0JBQ1IsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDeEMsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2FBQ3hCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBSb3V0ZXMsIFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcclxuaW1wb3J0IHsgRm9mQXV0aEd1YXJkfSBmcm9tICcuLi9jb3JlL2F1dGguZ3VhcmQnXHJcbmltcG9ydCB7IFVzZXJ2aWNlc0NvbXBvbmVudCB9IGZyb20gJy4vdXNlcnZpY2VzL3VzZXJ2aWNlcy5jb21wb25lbnQnXHJcbmltcG9ydCB7IFVzZXJ2aWNlQ29tcG9uZW50IH0gZnJvbSAnLi91c2VydmljZS91c2VydmljZS5jb21wb25lbnQnXHJcblxyXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuICB7IHBhdGg6ICcnLCBjYW5BY3RpdmF0ZTogW0ZvZkF1dGhHdWFyZF0sXHJcbiAgICBjaGlsZHJlbjogW1xyXG4gICAgICB7IHBhdGg6ICcnLFxyXG4gICAgICAgIGNoaWxkcmVuOiBbXHJcbiAgICAgICAgICB7IHBhdGg6ICcnLCBjb21wb25lbnQ6IFVzZXJ2aWNlc0NvbXBvbmVudCB9LCAgICAgICAgICAgIFxyXG4gICAgICAgICAgeyBwYXRoOiAnOmlkJywgY29tcG9uZW50OiBVc2VydmljZUNvbXBvbmVudCB9XHJcbiAgICAgICAgXVxyXG4gICAgICB9LFxyXG4gICAgXVxyXG4gIH0gICBcclxuXVxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBpbXBvcnRzOiBbUm91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyldLFxyXG4gIGV4cG9ydHM6IFtSb3V0ZXJNb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBVc2VydmljZXNSb3V0aW5nTW9kdWxlIHsgfVxyXG4iXX0=
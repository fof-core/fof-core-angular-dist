import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../core/material.module';
import { UservicesRoutingModule } from './uservices-routing.module';
import { UservicesComponent } from './uservices/uservices.component';
import { UserviceComponent } from './uservice/uservice.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as i0 from "@angular/core";
export class UservicesModule {
}
UservicesModule.ɵmod = i0.ɵɵdefineNgModule({ type: UservicesModule });
UservicesModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UservicesModule_Factory(t) { return new (t || UservicesModule)(); }, imports: [[
            CommonModule,
            MaterialModule,
            FormsModule,
            ReactiveFormsModule,
            UservicesRoutingModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UservicesModule, { declarations: [UservicesComponent, UserviceComponent], imports: [CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        UservicesRoutingModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesModule, [{
        type: NgModule,
        args: [{
                declarations: [UservicesComponent, UserviceComponent],
                imports: [
                    CommonModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    UservicesRoutingModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL3VzZXJ2aWNlcy91c2VydmljZXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQTtBQUNqRSxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUE7O0FBYWpFLE1BQU0sT0FBTyxlQUFlOzttREFBZixlQUFlOzZHQUFmLGVBQWUsa0JBUmpCO1lBQ1AsWUFBWTtZQUNaLGNBQWM7WUFDZCxXQUFXO1lBQ1gsbUJBQW1CO1lBQ25CLHNCQUFzQjtTQUN2Qjt3RkFFVSxlQUFlLG1CQVRYLGtCQUFrQixFQUFFLGlCQUFpQixhQUVsRCxZQUFZO1FBQ1osY0FBYztRQUNkLFdBQVc7UUFDWCxtQkFBbUI7UUFDbkIsc0JBQXNCO2tEQUdiLGVBQWU7Y0FWM0IsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGtCQUFrQixFQUFFLGlCQUFpQixDQUFDO2dCQUNyRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixjQUFjO29CQUNkLFdBQVc7b0JBQ1gsbUJBQW1CO29CQUNuQixzQkFBc0I7aUJBQ3ZCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbidcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9jb3JlL21hdGVyaWFsLm1vZHVsZSdcclxuaW1wb3J0IHsgVXNlcnZpY2VzUm91dGluZ01vZHVsZSB9IGZyb20gJy4vdXNlcnZpY2VzLXJvdXRpbmcubW9kdWxlJ1xyXG5pbXBvcnQgeyBVc2VydmljZXNDb21wb25lbnQgfSBmcm9tICcuL3VzZXJ2aWNlcy91c2VydmljZXMuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBVc2VydmljZUNvbXBvbmVudCB9IGZyb20gJy4vdXNlcnZpY2UvdXNlcnZpY2UuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJ1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbVXNlcnZpY2VzQ29tcG9uZW50LCBVc2VydmljZUNvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSwgXHJcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgVXNlcnZpY2VzUm91dGluZ01vZHVsZVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFVzZXJ2aWNlc01vZHVsZSB7IH1cclxuIl19
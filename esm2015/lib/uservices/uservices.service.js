import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class UservicesService {
    constructor(fofConfig, httpClient) {
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.uServices = {
            create: (uService) => this.httpClient.post(`${this.environment.apiPath}/uservices`, uService),
            update: (uService) => this.httpClient.patch(`${this.environment.apiPath}/uservices/${uService.id}`, uService),
            delete: (uService) => this.httpClient.delete(`${this.environment.apiPath}/uservices/${uService.id}`),
            getOne: (id) => this.httpClient.get(`${this.environment.apiPath}/uservices/${id}`),
            getAll: () => this.httpClient.get(`${this.environment.apiPath}/uservices`)
        };
        this.environment = this.fofConfig.environment;
    }
}
UservicesService.ɵfac = function UservicesService_Factory(t) { return new (t || UservicesService)(i0.ɵɵinject(CORE_CONFIG), i0.ɵɵinject(i1.HttpClient)); };
UservicesService.ɵprov = i0.ɵɵdefineInjectable({ token: UservicesService, factory: UservicesService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: i1.HttpClient }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi91c2VydmljZXMvdXNlcnZpY2VzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDbEQsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7O0FBZXZELE1BQU0sT0FBTyxnQkFBZ0I7SUFFM0IsWUFDK0IsU0FBcUIsRUFDMUMsVUFBc0I7UUFERCxjQUFTLEdBQVQsU0FBUyxDQUFZO1FBQzFDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFPekIsY0FBUyxHQUFHO1lBQ2pCLE1BQU0sRUFBQyxDQUFDLFFBQW1CLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLFlBQVksRUFBRSxRQUFRLENBQUM7WUFDbEgsTUFBTSxFQUFDLENBQUMsUUFBbUIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sY0FBYyxRQUFRLENBQUMsRUFBRSxFQUFFLEVBQUUsUUFBUSxDQUFDO1lBQ2xJLE1BQU0sRUFBQyxDQUFDLFFBQW1CLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLGNBQWMsUUFBUSxDQUFDLEVBQUUsRUFBRSxDQUFDO1lBQ3pILE1BQU0sRUFBRSxDQUFDLEVBQVUsRUFBeUIsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLGNBQWMsRUFBRSxFQUFFLENBQUM7WUFDNUgsTUFBTSxFQUFFLEdBQTRCLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBbUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sWUFBWSxDQUFDO1NBQ3RILENBQUE7UUFYQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFBO0lBQy9DLENBQUM7O2dGQVBVLGdCQUFnQixjQUdqQixXQUFXO3dEQUhWLGdCQUFnQixXQUFoQixnQkFBZ0IsbUJBRmYsTUFBTTtrREFFUCxnQkFBZ0I7Y0FINUIsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COztzQkFJSSxNQUFNO3VCQUFDLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBJZm9mQ29uZmlnLCBDT1JFX0NPTkZJRyB9IGZyb20gJy4uL2ZvZi1jb25maWcnXHJcbi8vIGltcG9ydCB7IGVDcCB9IGZyb20gJy4uL3NoYXJlZC9wZXJtaXNzaW9uLmVudW0nXHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yLCBzaGFyZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJ1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnXHJcbi8vIGltcG9ydCB7IEZvRkF1dGhTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9hdXRoLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvRkNvcmVTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9jb3JlLnNlcnZpY2UnXHJcbi8vIGltcG9ydCB7IGlVc2VyIH0gZnJvbSAnLi4vc2hhcmVkL3VzZXIuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBSZXF1ZXN0UXVlcnlCdWlsZGVyLCBDb25kT3BlcmF0b3IgfSBmcm9tICdAbmVzdGpzeC9jcnVkLXJlcXVlc3QnXHJcbmltcG9ydCB7IGlmb2ZTZWFyY2ggfSBmcm9tICcuLi9jb3JlL2NvcmUuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBpVXNlcnZpY2UgfSBmcm9tICcuLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlcnZpY2VzU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgQEluamVjdChDT1JFX0NPTkZJRykgcHJpdmF0ZSBmb2ZDb25maWc6IElmb2ZDb25maWcsXHJcbiAgICBwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnRcclxuICApIHsgXHJcbiAgICB0aGlzLmVudmlyb25tZW50ID0gdGhpcy5mb2ZDb25maWcuZW52aXJvbm1lbnRcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZW52aXJvbm1lbnQ6YW55XHJcblxyXG4gIHB1YmxpYyB1U2VydmljZXMgPSB7XHJcbiAgICBjcmVhdGU6KHVTZXJ2aWNlOiBpVXNlcnZpY2UpID0+IHRoaXMuaHR0cENsaWVudC5wb3N0PGlVc2VydmljZT4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS91c2VydmljZXNgLCB1U2VydmljZSksXHJcbiAgICB1cGRhdGU6KHVTZXJ2aWNlOiBpVXNlcnZpY2UpID0+IHRoaXMuaHR0cENsaWVudC5wYXRjaDxpVXNlcnZpY2U+KGAke3RoaXMuZW52aXJvbm1lbnQuYXBpUGF0aH0vdXNlcnZpY2VzLyR7dVNlcnZpY2UuaWR9YCwgdVNlcnZpY2UpLFxyXG4gICAgZGVsZXRlOih1U2VydmljZTogaVVzZXJ2aWNlKSA9PiB0aGlzLmh0dHBDbGllbnQuZGVsZXRlPGlVc2VydmljZT4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS91c2VydmljZXMvJHt1U2VydmljZS5pZH1gKSxcclxuICAgIGdldE9uZTogKGlkOiBudW1iZXIpOiBPYnNlcnZhYmxlPGlVc2VydmljZT4gPT4gdGhpcy5odHRwQ2xpZW50LmdldDxpVXNlcnZpY2U+KGAke3RoaXMuZW52aXJvbm1lbnQuYXBpUGF0aH0vdXNlcnZpY2VzLyR7aWR9YCksICAgXHJcbiAgICBnZXRBbGw6ICgpOiBPYnNlcnZhYmxlPGlVc2VydmljZVtdPiA9PiB0aGlzLmh0dHBDbGllbnQuZ2V0PEFycmF5PGlVc2VydmljZT4+KGAke3RoaXMuZW52aXJvbm1lbnQuYXBpUGF0aH0vdXNlcnZpY2VzYCkgICBcclxuICB9XHJcbn1cclxuIl19
import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
import * as i1 from "../uservices.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
import * as i4 from "@angular/material/card";
import * as i5 from "@angular/material/button";
import * as i6 from "@angular/router";
import * as i7 from "@angular/common";
import * as i8 from "@angular/material/table";
import * as i9 from "@angular/material/sort";
import * as i10 from "@angular/material/paginator";
import * as i11 from "@angular/material/progress-spinner";
function UservicesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 15);
    i0.ɵɵelement(1, "mat-spinner", 16);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des \u03BCServices...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function UservicesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Nom technique");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r475 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r475.technicalName);
} }
function UservicesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Nom");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r476 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r476.name);
} }
function UservicesComponent_th_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Url interface");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r477 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r477.frontUrl);
} }
function UservicesComponent_th_19_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Url backend");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_20_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r478 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r478.backUrl);
} }
function UservicesComponent_th_22_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Visible pour les utilisateurs");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_23_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const row_r479 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r479.availableForUsers);
} }
function UservicesComponent_tr_24_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 19);
} }
function UservicesComponent_tr_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 20);
} if (rf & 2) {
    const row_r480 = ctx.$implicit;
    i0.ɵɵproperty("routerLink", row_r480.id);
} }
const _c0 = function () { return [100]; };
export class UservicesComponent {
    constructor(uservicesService, fofNotificationService, breakpointObserver) {
        this.uservicesService = uservicesService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['technicalName', 'name', 'urlFront', 'urlBack', 'availableForUsers'],
            data: [],
            resultsLength: 0,
            pageSize: 100,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    ngOnInit() {
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe((state) => {
            if (state.matches) {
                // XSmall
                this.uiVar.displayedColumns = ['technicalName', 'name'];
            }
            else {
                // > XSmall
                this.uiVar.displayedColumns = ['technicalName', 'name', 'frontUrl', 'backUrl', 'availableForUsers'];
            }
        });
    }
    ngAfterViewInit() {
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(() => {
            this.uiVar.isLoadingResults = true;
            this.uiVar.pageSize = this.paginator.pageSize;
            return this.uservicesService.uServices.getAll();
        }), map((uServices) => {
            this.uiVar.isLoadingResults = false;
            this.uiVar.resultsLength = uServices.length;
            return uServices;
        }), catchError(() => {
            this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(data => this.uiVar.data = data);
    }
    ngOnDestroy() {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    }
}
UservicesComponent.ɵfac = function UservicesComponent_Factory(t) { return new (t || UservicesComponent)(i0.ɵɵdirectiveInject(i1.UservicesService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver)); };
UservicesComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UservicesComponent, selectors: [["fof-core-uservices"]], viewQuery: function UservicesComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(MatPaginator, true);
        i0.ɵɵviewQuery(MatSort, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
    } }, decls: 27, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "technicalName", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "technicalName"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "name"], ["matColumnDef", "frontUrl"], ["matColumnDef", "backUrl"], ["matColumnDef", "availableForUsers"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UservicesComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-card", 0);
        i0.ɵɵelementStart(1, "h3");
        i0.ɵɵtext(2, "Mini-services");
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(3, "a", 1);
        i0.ɵɵelementStart(4, "span");
        i0.ɵɵtext(5, "Ajouter un mini-service");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(6, "div", 2);
        i0.ɵɵtemplate(7, UservicesComponent_div_7_Template, 4, 0, "div", 3);
        i0.ɵɵelementStart(8, "table", 4);
        i0.ɵɵelementContainerStart(9, 5);
        i0.ɵɵtemplate(10, UservicesComponent_th_10_Template, 2, 0, "th", 6);
        i0.ɵɵtemplate(11, UservicesComponent_td_11_Template, 2, 1, "td", 7);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(12, 8);
        i0.ɵɵtemplate(13, UservicesComponent_th_13_Template, 2, 0, "th", 6);
        i0.ɵɵtemplate(14, UservicesComponent_td_14_Template, 2, 1, "td", 7);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(15, 9);
        i0.ɵɵtemplate(16, UservicesComponent_th_16_Template, 2, 0, "th", 6);
        i0.ɵɵtemplate(17, UservicesComponent_td_17_Template, 2, 1, "td", 7);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(18, 10);
        i0.ɵɵtemplate(19, UservicesComponent_th_19_Template, 2, 0, "th", 6);
        i0.ɵɵtemplate(20, UservicesComponent_td_20_Template, 2, 1, "td", 7);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵelementContainerStart(21, 11);
        i0.ɵɵtemplate(22, UservicesComponent_th_22_Template, 2, 0, "th", 6);
        i0.ɵɵtemplate(23, UservicesComponent_td_23_Template, 2, 1, "td", 7);
        i0.ɵɵelementContainerEnd();
        i0.ɵɵtemplate(24, UservicesComponent_tr_24_Template, 1, 0, "tr", 12);
        i0.ɵɵtemplate(25, UservicesComponent_tr_25_Template, 1, 1, "tr", 13);
        i0.ɵɵelementEnd();
        i0.ɵɵelement(26, "mat-paginator", 14);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(3);
        i0.ɵɵproperty("routerLink", "new");
        i0.ɵɵadvance(4);
        i0.ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("dataSource", ctx.uiVar.data);
        i0.ɵɵadvance(16);
        i0.ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", i0.ɵɵpureFunction0(8, _c0))("pageSize", ctx.uiVar.pageSize);
    } }, directives: [i4.MatCard, i5.MatAnchor, i6.RouterLinkWithHref, i7.NgIf, i8.MatTable, i9.MatSort, i8.MatColumnDef, i8.MatHeaderCellDef, i8.MatCellDef, i8.MatHeaderRowDef, i8.MatRowDef, i10.MatPaginator, i11.MatSpinner, i8.MatHeaderCell, i8.MatCell, i8.MatHeaderRow, i8.MatRow, i6.RouterLink], styles: [""] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-uservices',
                templateUrl: './uservices.component.html',
                styleUrls: ['./uservices.component.scss']
            }]
    }], function () { return [{ type: i1.UservicesService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL3VzZXJ2aWNlcy91c2VydmljZXMvdXNlcnZpY2VzLmNvbXBvbmVudC50cyIsImxpYi91c2VydmljZXMvdXNlcnZpY2VzL3VzZXJ2aWNlcy5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFtQyxTQUFTLEVBQTRCLE1BQU0sZUFBZSxDQUFBO0FBSS9HLE9BQU8sRUFBRSxZQUFZLEVBQW9CLE1BQU0sNkJBQTZCLENBQUE7QUFDNUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBQ2hELE9BQU8sRUFBRSxLQUFLLEVBQWMsRUFBRSxJQUFJLFlBQVksRUFBZ0IsTUFBTSxNQUFNLENBQUE7QUFDMUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBRXRFLE9BQU8sRUFBc0IsV0FBVyxFQUFtQixNQUFNLHFCQUFxQixDQUFBOzs7Ozs7Ozs7Ozs7OztJQ0NwRiwrQkFDRTtJQUFBLGtDQUF1QztJQUFDLDRCQUFNO0lBQUEsaURBQTRCO0lBQUEsaUJBQU87SUFDbkYsaUJBQU07OztJQU1GLDhCQUFzQztJQUFBLDZCQUFhO0lBQUEsaUJBQUs7OztJQUN4RCw4QkFBbUM7SUFBQSxZQUFxQjtJQUFBLGlCQUFLOzs7SUFBMUIsZUFBcUI7SUFBckIsNENBQXFCOzs7SUFJeEQsOEJBQXNDO0lBQUEsbUJBQUc7SUFBQSxpQkFBSzs7O0lBQzlDLDhCQUFtQztJQUFBLFlBQVk7SUFBQSxpQkFBSzs7O0lBQWpCLGVBQVk7SUFBWixtQ0FBWTs7O0lBSS9DLDhCQUFzQztJQUFBLDZCQUFhO0lBQUEsaUJBQUs7OztJQUN4RCw4QkFBbUM7SUFBQSxZQUFnQjtJQUFBLGlCQUFLOzs7SUFBckIsZUFBZ0I7SUFBaEIsdUNBQWdCOzs7SUFJbkQsOEJBQXNDO0lBQUEsMkJBQVc7SUFBQSxpQkFBSzs7O0lBQ3RELDhCQUFtQztJQUFBLFlBQWU7SUFBQSxpQkFBSzs7O0lBQXBCLGVBQWU7SUFBZixzQ0FBZTs7O0lBSWxELDhCQUFzQztJQUFBLDZDQUE2QjtJQUFBLGlCQUFLOzs7SUFDeEUsOEJBQW1DO0lBQUEsWUFBeUI7SUFBQSxpQkFBSzs7O0lBQTlCLGVBQXlCO0lBQXpCLGdEQUF5Qjs7O0lBRzlELHlCQUFrRTs7O0lBQ2xFLHlCQUM4RDs7O0lBRHpCLHdDQUFxQjs7O0FEM0I5RCxNQUFNLE9BQU8sa0JBQWtCO0lBSTdCLFlBQ1UsZ0JBQWtDLEVBQ2xDLHNCQUE4QyxFQUM5QyxrQkFBc0M7UUFGdEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFLaEQsd0JBQXdCO1FBQ2hCLFdBQU0sR0FBRztZQUNmLHFCQUFxQixFQUFnQixTQUFTO1NBQy9DLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHLEVBQ2xCLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsZ0JBQWdCLEVBQVksQ0FBQyxlQUFlLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsbUJBQW1CLENBQUM7WUFDakcsSUFBSSxFQUFlLEVBQUU7WUFDckIsYUFBYSxFQUFFLENBQUM7WUFDaEIsUUFBUSxFQUFFLEdBQUc7WUFDYixnQkFBZ0IsRUFBRSxJQUFJO1NBQ3ZCLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHLEVBQ2pCLENBQUE7SUFuQkQsQ0FBQztJQW9CRCxpQkFBaUI7SUFDakIsUUFBUTtRQUNOLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO2FBQ3RGLFNBQVMsQ0FBQyxDQUFDLEtBQXNCLEVBQUUsRUFBRTtZQUNwQyxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ2pCLFNBQVM7Z0JBQ1QsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLGVBQWUsRUFBRSxNQUFNLENBQUMsQ0FBQTthQUN4RDtpQkFBTTtnQkFDTCxXQUFXO2dCQUNYLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxlQUFlLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsbUJBQW1CLENBQUMsQ0FBQTthQUNwRztRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELGVBQWU7UUFDYixvRUFBb0U7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFBO1FBRWxFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzthQUM3QyxJQUFJLENBQ0gsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDYixJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQTtZQUNsQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQTtZQUM3QyxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUE7UUFDakQsQ0FBQyxDQUFDLEVBQ0YsR0FBRyxDQUFDLENBQUMsU0FBc0IsRUFBRSxFQUFFO1lBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUE7WUFDM0MsT0FBTyxTQUFTLENBQUE7UUFDbEIsQ0FBQyxDQUFDLEVBQ0YsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLE9BQU8sWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1FBQ3pCLENBQUMsQ0FBQyxDQUNILENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLENBQUE7SUFDL0MsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUU7WUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQUU7SUFDNUYsQ0FBQzs7b0ZBdEVVLGtCQUFrQjt1REFBbEIsa0JBQWtCO3VCQUNsQixZQUFZO3VCQUNaLE9BQU87Ozs7OztRQ2xCcEIsbUNBQ0U7UUFBQSwwQkFBSTtRQUFBLDZCQUFhO1FBQUEsaUJBQUs7UUFDdEIsNEJBRUU7UUFBQSw0QkFBTTtRQUFBLHVDQUF1QjtRQUFBLGlCQUFPO1FBQ3RDLGlCQUFJO1FBQ04saUJBQVc7UUFFWCw4QkFFRTtRQUFBLG1FQUNFO1FBR0YsZ0NBR0U7UUFBQSxnQ0FDRTtRQUFBLG1FQUFzQztRQUN0QyxtRUFBbUM7UUFDckMsMEJBQWU7UUFFZixpQ0FDRTtRQUFBLG1FQUFzQztRQUN0QyxtRUFBbUM7UUFDckMsMEJBQWU7UUFFZixpQ0FDRTtRQUFBLG1FQUFzQztRQUN0QyxtRUFBbUM7UUFDckMsMEJBQWU7UUFFZixrQ0FDRTtRQUFBLG1FQUFzQztRQUN0QyxtRUFBbUM7UUFDckMsMEJBQWU7UUFFZixrQ0FDRTtRQUFBLG1FQUFzQztRQUN0QyxtRUFBbUM7UUFDckMsMEJBQWU7UUFFZixvRUFBNkQ7UUFDN0Qsb0VBQ3lEO1FBQzNELGlCQUFRO1FBRVIscUNBRThDO1FBRWhELGlCQUFNOztRQWhERixlQUFvQjtRQUFwQixrQ0FBb0I7UUFPdUIsZUFBOEI7UUFBOUIsaURBQThCO1FBSTFELGVBQXlCO1FBQXpCLDJDQUF5QjtRQTRCckIsZ0JBQXlDO1FBQXpDLDREQUF5QztRQUUxRCxlQUFzRDtRQUF0RCw2REFBc0Q7UUFHM0MsZUFBOEI7UUFBOUIsZ0RBQThCLCtDQUFBLGdDQUFBOztrREQvQmxDLGtCQUFrQjtjQUw5QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsV0FBVyxFQUFFLDRCQUE0QjtnQkFDekMsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7YUFDMUM7O2tCQUVFLFNBQVM7bUJBQUMsWUFBWTs7a0JBQ3RCLFNBQVM7bUJBQUMsT0FBTyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBVc2VydmljZXNTZXJ2aWNlIH0gZnJvbSAnLi4vdXNlcnZpY2VzLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgaVVzZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yLCBNYXRQYWdpbmF0b3JJbnRsIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvcGFnaW5hdG9yJ1xyXG5pbXBvcnQgeyBNYXRTb3J0IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvc29ydCdcclxuaW1wb3J0IHsgbWVyZ2UsIE9ic2VydmFibGUsIG9mIGFzIG9ic2VydmFibGVPZiwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCBzdGFydFdpdGgsIHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJ1xyXG5pbXBvcnQgeyBpZm9mU2VhcmNoIH0gZnJvbSAnLi4vLi4vY29yZS9jb3JlLmludGVyZmFjZSdcclxuaW1wb3J0IHsgQnJlYWtwb2ludE9ic2VydmVyLCBCcmVha3BvaW50cywgQnJlYWtwb2ludFN0YXRlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCdcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZm9mLWNvcmUtdXNlcnZpY2VzJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vdXNlcnZpY2VzLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi91c2VydmljZXMuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlcnZpY2VzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBAVmlld0NoaWxkKE1hdFBhZ2luYXRvcikgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3JcclxuICBAVmlld0NoaWxkKE1hdFNvcnQpIHNvcnQ6IE1hdFNvcnRcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIHVzZXJ2aWNlc1NlcnZpY2U6IFVzZXJ2aWNlc1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGJyZWFrcG9pbnRPYnNlcnZlcjogQnJlYWtwb2ludE9ic2VydmVyXHJcbiAgKSB7IFxyXG4gICAgXHJcbiAgfVxyXG5cclxuICAvLyBBbGwgcHJpdmF0ZSB2YXJpYWJsZXNcclxuICBwcml2YXRlIHByaVZhciA9IHtcclxuICAgIGJyZWFrcG9pbnRPYnNlcnZlclN1YjogPFN1YnNjcmlwdGlvbj51bmRlZmluZWQsXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0geyAgICBcclxuICAgIGRpc3BsYXllZENvbHVtbnM6IDxzdHJpbmdbXT5bJ3RlY2huaWNhbE5hbWUnLCAnbmFtZScsICd1cmxGcm9udCcsICd1cmxCYWNrJywgJ2F2YWlsYWJsZUZvclVzZXJzJ10sXHJcbiAgICBkYXRhOiA8aVVzZXJ2aWNlW10+W10sXHJcbiAgICByZXN1bHRzTGVuZ3RoOiAwLFxyXG4gICAgcGFnZVNpemU6IDEwMCxcclxuICAgIGlzTG9hZGluZ1Jlc3VsdHM6IHRydWVcclxuICB9XHJcbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpQWN0aW9uID0ge1xyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViID0gdGhpcy5icmVha3BvaW50T2JzZXJ2ZXIub2JzZXJ2ZShCcmVha3BvaW50cy5YU21hbGwpXHJcbiAgICAuc3Vic2NyaWJlKChzdGF0ZTogQnJlYWtwb2ludFN0YXRlKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmIChzdGF0ZS5tYXRjaGVzKSB7XHJcbiAgICAgICAgLy8gWFNtYWxsXHJcbiAgICAgICAgdGhpcy51aVZhci5kaXNwbGF5ZWRDb2x1bW5zID0gWyd0ZWNobmljYWxOYW1lJywgJ25hbWUnXVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vID4gWFNtYWxsXHJcbiAgICAgICAgdGhpcy51aVZhci5kaXNwbGF5ZWRDb2x1bW5zID0gWyd0ZWNobmljYWxOYW1lJywgJ25hbWUnLCAnZnJvbnRVcmwnLCAnYmFja1VybCcsICdhdmFpbGFibGVGb3JVc2VycyddXHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgfSAgXHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHsgICAgXHJcbiAgICAvLyBJZiB0aGUgdXNlciBjaGFuZ2VzIHRoZSBzb3J0IG9yZGVyLCByZXNldCBiYWNrIHRvIHRoZSBmaXJzdCBwYWdlLlxyXG4gICAgdGhpcy5zb3J0LnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCA9IDApXHJcblxyXG4gICAgbWVyZ2UodGhpcy5zb3J0LnNvcnRDaGFuZ2UsIHRoaXMucGFnaW5hdG9yLnBhZ2UpXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIHN0YXJ0V2l0aCh7fSksXHJcbiAgICAgICAgc3dpdGNoTWFwKCgpID0+IHtcclxuICAgICAgICAgIHRoaXMudWlWYXIuaXNMb2FkaW5nUmVzdWx0cyA9IHRydWUgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnBhZ2VTaXplID0gdGhpcy5wYWdpbmF0b3IucGFnZVNpemVcclxuICAgICAgICAgIHJldHVybiB0aGlzLnVzZXJ2aWNlc1NlcnZpY2UudVNlcnZpY2VzLmdldEFsbCgpXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgbWFwKCh1U2VydmljZXM6IGlVc2VydmljZVtdKSA9PiB7ICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMudWlWYXIuaXNMb2FkaW5nUmVzdWx0cyA9IGZhbHNlXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnJlc3VsdHNMZW5ndGggPSB1U2VydmljZXMubGVuZ3RoICAgICAgICAgIFxyXG4gICAgICAgICAgcmV0dXJuIHVTZXJ2aWNlc1xyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGNhdGNoRXJyb3IoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2UgICAgICAgICAgXHJcbiAgICAgICAgICByZXR1cm4gb2JzZXJ2YWJsZU9mKFtdKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICkuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy51aVZhci5kYXRhID0gZGF0YSlcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1YikgeyB0aGlzLnByaVZhci5icmVha3BvaW50T2JzZXJ2ZXJTdWIudW5zdWJzY3JpYmUoKSB9XHJcbiAgfVxyXG5cclxufVxyXG4iLCI8bWF0LWNhcmQgY2xhc3M9XCJmb2YtaGVhZGVyXCI+ICAgICAgICBcclxuICA8aDM+TWluaS1zZXJ2aWNlczwvaDM+ICAgXHJcbiAgPGEgbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwiYWNjZW50XCIgXHJcbiAgICBbcm91dGVyTGlua109XCInbmV3J1wiPiAgICBcclxuICAgIDxzcGFuPkFqb3V0ZXIgdW4gbWluaS1zZXJ2aWNlPC9zcGFuPlxyXG4gIDwvYT4gICAgXHJcbjwvbWF0LWNhcmQ+XHJcblxyXG48ZGl2IGNsYXNzPVwiZm9mLXRhYmxlLWNvbnRhaW5lciBtYXQtZWxldmF0aW9uLXoyXCI+XHJcblxyXG4gIDxkaXYgY2xhc3M9XCJ0YWJsZS1sb2FkaW5nLXNoYWRlIGZvZi1sb2FkaW5nXCIgKm5nSWY9XCJ1aVZhci5pc0xvYWRpbmdSZXN1bHRzXCI+XHJcbiAgICA8bWF0LXNwaW5uZXIgZGlhbWV0ZXI9MjA+PC9tYXQtc3Bpbm5lcj4gPHNwYW4+Q2hhcmdlbWVudHMgZGVzIM68U2VydmljZXMuLi48L3NwYW4+XHJcbiAgPC9kaXY+XHJcblxyXG4gIDx0YWJsZSBtYXQtdGFibGUgW2RhdGFTb3VyY2VdPVwidWlWYXIuZGF0YVwiIGNsYXNzPVwiZGF0YS10YWJsZVwiXHJcbiAgICAgICAgbWF0U29ydCBtYXRTb3J0QWN0aXZlPVwidGVjaG5pY2FsTmFtZVwiIG1hdFNvcnREaXNhYmxlQ2xlYXIgbWF0U29ydERpcmVjdGlvbj1cImFzY1wiPlxyXG4gICAgXHJcbiAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cInRlY2huaWNhbE5hbWVcIj5cclxuICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZj5Ob20gdGVjaG5pcXVlPC90aD5cclxuICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LnRlY2huaWNhbE5hbWV9fTwvdGQ+XHJcbiAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cIm5hbWVcIj5cclxuICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZj5Ob208L3RoPlxyXG4gICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cubmFtZX19PC90ZD5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwiZnJvbnRVcmxcIj5cclxuICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZj5VcmwgaW50ZXJmYWNlPC90aD5cclxuICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LmZyb250VXJsfX08L3RkPlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJiYWNrVXJsXCI+XHJcbiAgICAgIDx0aCBtYXQtaGVhZGVyLWNlbGwgKm1hdEhlYWRlckNlbGxEZWY+VXJsIGJhY2tlbmQ8L3RoPlxyXG4gICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cuYmFja1VybH19PC90ZD5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwiYXZhaWxhYmxlRm9yVXNlcnNcIj5cclxuICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZj5WaXNpYmxlIHBvdXIgbGVzIHV0aWxpc2F0ZXVyczwvdGg+XHJcbiAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIj57e3Jvdy5hdmFpbGFibGVGb3JVc2Vyc319PC90ZD5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgIDx0ciBtYXQtaGVhZGVyLXJvdyAqbWF0SGVhZGVyUm93RGVmPVwidWlWYXIuZGlzcGxheWVkQ29sdW1uc1wiPjwvdHI+XHJcbiAgICA8dHIgbWF0LXJvdyBjbGFzcz1cImZvZi1lbGVtZW50LW92ZXJcIiBbcm91dGVyTGlua109XCJyb3cuaWRcIlxyXG4gICAgICAqbWF0Um93RGVmPVwibGV0IHJvdzsgY29sdW1uczogdWlWYXIuZGlzcGxheWVkQ29sdW1ucztcIj48L3RyPlxyXG4gIDwvdGFibGU+XHJcblxyXG4gIDxtYXQtcGFnaW5hdG9yIFtsZW5ndGhdPVwidWlWYXIucmVzdWx0c0xlbmd0aFwiIFxyXG4gICAgW3BhZ2VTaXplT3B0aW9uc109XCJbMTAwXVwiXHJcbiAgICBbcGFnZVNpemVdPVwidWlWYXIucGFnZVNpemVcIj48L21hdC1wYWdpbmF0b3I+XHJcblxyXG48L2Rpdj5cclxuIl19
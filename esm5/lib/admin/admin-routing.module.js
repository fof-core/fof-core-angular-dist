import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FofRolesComponent } from './fof-roles/fof-roles.component';
import { FofRoleComponent } from './fof-role/fof-role.component';
import { FofUsersComponent } from './fof-users/fof-users.component';
import { FofUserComponent } from './fof-user/fof-user.component';
import { FofOrganizationsComponent } from './fof-organizations/fof-organizations.component';
import { FofAuthGuard } from '../core/auth.guard';
import { eCp } from '../permission/interfaces/permission.enum';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
var routes = [
    { path: 'admin', canActivate: [FofAuthGuard], data: { permissions: [eCp.coreAdminAccess] },
        children: [
            { path: '', component: FofRolesComponent },
            /**
             * if a child doesn't have permissions, it will get that ones
             * if it has some, it will override this ones
             * e.g. first child (path: '') get that ones
             */
            // { path: 'roles', data: {permissions: [eCp.roleRead]},
            { path: 'roles',
                children: [
                    { path: '', component: FofRolesComponent },
                    // { path: 'new', component: FofRoleComponent, data: {permissions: [eCp.roleCreate]} }
                    // { path: 'new', component: FofRoleComponent },
                    { path: ':code', component: FofRoleComponent }
                ]
            },
            { path: 'users',
                children: [
                    { path: '', component: FofUsersComponent },
                    { path: ':code', component: FofUserComponent }
                ]
            },
            { path: 'organizations', data: { permissions: [eCp.organizationRead] },
                children: [
                    { path: '', component: FofOrganizationsComponent }
                ]
            }
        ]
    }
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: AdminRoutingModule });
    AdminRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function AdminRoutingModule_Factory(t) { return new (t || AdminRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
            RouterModule] });
    return AdminRoutingModule;
}());
export { AdminRoutingModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(AdminRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(AdminRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRtaW4tcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9hZG1pbi9hZG1pbi1yb3V0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQTtBQUNoRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQTtBQUNoRSxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxpREFBaUQsQ0FBQTtBQUMzRixPQUFPLEVBQUUsWUFBWSxFQUFDLE1BQU0sb0JBQW9CLENBQUE7QUFDaEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLDBDQUEwQyxDQUFBOzs7QUFHOUQsSUFBTSxNQUFNLEdBQVc7SUFDckIsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsRUFBQztRQUN0RixRQUFRLEVBQUU7WUFDUixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFDO1lBQ3pDOzs7O2VBSUc7WUFDSCx3REFBd0Q7WUFDeEQsRUFBRSxJQUFJLEVBQUUsT0FBTztnQkFDYixRQUFRLEVBQUU7b0JBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRTtvQkFDMUMsc0ZBQXNGO29CQUN0RixnREFBZ0Q7b0JBQ2hELEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUU7aUJBQy9DO2FBQ0Y7WUFDRCxFQUFFLElBQUksRUFBRSxPQUFPO2dCQUNiLFFBQVEsRUFBRTtvQkFDUixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFFO29CQUMxQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLGdCQUFnQixFQUFFO2lCQUMvQzthQUNGO1lBQ0QsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRSxFQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFDO2dCQUNsRSxRQUFRLEVBQUU7b0JBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSx5QkFBeUIsRUFBRTtpQkFDbkQ7YUFDRjtTQUNGO0tBQ0Y7Q0FDRixDQUFBO0FBRUQ7SUFBQTtLQUltQzswREFBdEIsa0JBQWtCO3VIQUFsQixrQkFBa0Isa0JBSHBCLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUM5QixZQUFZOzZCQTlDeEI7Q0FnRG1DLEFBSm5DLElBSW1DO1NBQXRCLGtCQUFrQjt3RkFBbEIsa0JBQWtCLDBDQUZuQixZQUFZO2tEQUVYLGtCQUFrQjtjQUo5QixRQUFRO2VBQUM7Z0JBQ1IsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDeEMsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2FBQ3hCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBSb3V0ZXMsIFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcclxuaW1wb3J0IHsgRm9mUm9sZXNDb21wb25lbnQgfSBmcm9tICcuL2ZvZi1yb2xlcy9mb2Ytcm9sZXMuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb2ZSb2xlQ29tcG9uZW50IH0gZnJvbSAnLi9mb2Ytcm9sZS9mb2Ytcm9sZS5jb21wb25lbnQnXHJcbmltcG9ydCB7IEZvZlVzZXJzQ29tcG9uZW50IH0gZnJvbSAnLi9mb2YtdXNlcnMvZm9mLXVzZXJzLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9mVXNlckNvbXBvbmVudCB9IGZyb20gJy4vZm9mLXVzZXIvZm9mLXVzZXIuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb2ZPcmdhbml6YXRpb25zQ29tcG9uZW50IH0gZnJvbSAnLi9mb2Ytb3JnYW5pemF0aW9ucy9mb2Ytb3JnYW5pemF0aW9ucy5jb21wb25lbnQnXHJcbmltcG9ydCB7IEZvZkF1dGhHdWFyZH0gZnJvbSAnLi4vY29yZS9hdXRoLmd1YXJkJ1xyXG5pbXBvcnQgeyBlQ3AgfSBmcm9tICcuLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbi5lbnVtJ1xyXG5cclxuXHJcbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xyXG4gIHsgcGF0aDogJ2FkbWluJywgY2FuQWN0aXZhdGU6IFtGb2ZBdXRoR3VhcmRdLCBkYXRhOiB7cGVybWlzc2lvbnM6IFtlQ3AuY29yZUFkbWluQWNjZXNzXX0sICAgXHJcbiAgICBjaGlsZHJlbjogWyAgICAgXHJcbiAgICAgIHsgcGF0aDogJycsIGNvbXBvbmVudDogRm9mUm9sZXNDb21wb25lbnR9LCAgICAgICBcclxuICAgICAgLyoqIFxyXG4gICAgICAgKiBpZiBhIGNoaWxkIGRvZXNuJ3QgaGF2ZSBwZXJtaXNzaW9ucywgaXQgd2lsbCBnZXQgdGhhdCBvbmVzXHJcbiAgICAgICAqIGlmIGl0IGhhcyBzb21lLCBpdCB3aWxsIG92ZXJyaWRlIHRoaXMgb25lc1xyXG4gICAgICAgKiBlLmcuIGZpcnN0IGNoaWxkIChwYXRoOiAnJykgZ2V0IHRoYXQgb25lc1xyXG4gICAgICAgKi9cclxuICAgICAgLy8geyBwYXRoOiAncm9sZXMnLCBkYXRhOiB7cGVybWlzc2lvbnM6IFtlQ3Aucm9sZVJlYWRdfSxcclxuICAgICAgeyBwYXRoOiAncm9sZXMnLFxyXG4gICAgICAgIGNoaWxkcmVuOiBbXHJcbiAgICAgICAgICB7IHBhdGg6ICcnLCBjb21wb25lbnQ6IEZvZlJvbGVzQ29tcG9uZW50IH0sICBcclxuICAgICAgICAgIC8vIHsgcGF0aDogJ25ldycsIGNvbXBvbmVudDogRm9mUm9sZUNvbXBvbmVudCwgZGF0YToge3Blcm1pc3Npb25zOiBbZUNwLnJvbGVDcmVhdGVdfSB9XHJcbiAgICAgICAgICAvLyB7IHBhdGg6ICduZXcnLCBjb21wb25lbnQ6IEZvZlJvbGVDb21wb25lbnQgfSxcclxuICAgICAgICAgIHsgcGF0aDogJzpjb2RlJywgY29tcG9uZW50OiBGb2ZSb2xlQ29tcG9uZW50IH1cclxuICAgICAgICBdXHJcbiAgICAgIH0sXHJcbiAgICAgIHsgcGF0aDogJ3VzZXJzJyxcclxuICAgICAgICBjaGlsZHJlbjogW1xyXG4gICAgICAgICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBGb2ZVc2Vyc0NvbXBvbmVudCB9LCAgICAgICAgICAgIFxyXG4gICAgICAgICAgeyBwYXRoOiAnOmNvZGUnLCBjb21wb25lbnQ6IEZvZlVzZXJDb21wb25lbnQgfVxyXG4gICAgICAgIF1cclxuICAgICAgfSxcclxuICAgICAgeyBwYXRoOiAnb3JnYW5pemF0aW9ucycsIGRhdGE6IHtwZXJtaXNzaW9uczogW2VDcC5vcmdhbml6YXRpb25SZWFkXX0sXHJcbiAgICAgICAgY2hpbGRyZW46IFtcclxuICAgICAgICAgIHsgcGF0aDogJycsIGNvbXBvbmVudDogRm9mT3JnYW5pemF0aW9uc0NvbXBvbmVudCB9ICAgICAgICAgIFxyXG4gICAgICAgIF1cclxuICAgICAgfSAgICAgXHJcbiAgICBdXHJcbiAgfVxyXG5dXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXHJcbiAgZXhwb3J0czogW1JvdXRlck1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIEFkbWluUm91dGluZ01vZHVsZSB7IH1cclxuIl19
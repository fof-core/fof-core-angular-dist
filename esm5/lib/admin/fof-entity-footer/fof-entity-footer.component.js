import { Component, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
function FofEntityFooterComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵtext(1);
    i0.ɵɵpipe(2, "date");
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r299 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate2(" Derni\u00E8re modification le ", i0.ɵɵpipeBind2(2, 2, ctx_r299.entityBase._updatedDate, "short"), " par ", ctx_r299.entityBase._updatedByName, " ");
} }
function FofEntityFooterComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 3);
    i0.ɵɵelementStart(1, "div", 4);
    i0.ɵɵtext(2);
    i0.ɵɵpipe(3, "date");
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(4, FofEntityFooterComponent_div_1_div_4_Template, 3, 5, "div", 5);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r296 = i0.ɵɵnextContext();
    var _r297 = i0.ɵɵreference(3);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate2(" Cr\u00E9ation le ", i0.ɵɵpipeBind2(3, 4, ctx_r296.entityBase._createdDate, "short"), " par ", ctx_r296.entityBase._createdByName, " ");
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", ctx_r296.entityBase._updatedByName)("ngIfElse", _r297);
} }
function FofEntityFooterComponent_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵtext(1, " Aucune modification depuis la cr\u00E9ation ");
    i0.ɵɵelementEnd();
} }
var FofEntityFooterComponent = /** @class */ (function () {
    function FofEntityFooterComponent() {
    }
    FofEntityFooterComponent.prototype.ngOnInit = function () {
    };
    FofEntityFooterComponent.ɵfac = function FofEntityFooterComponent_Factory(t) { return new (t || FofEntityFooterComponent)(); };
    FofEntityFooterComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofEntityFooterComponent, selectors: [["fof-entity-footer"]], inputs: { entityBase: "entityBase" }, decls: 4, vars: 1, consts: [[1, "custom-card"], ["class", "row", 4, "ngIf"], ["elseBlock", ""], [1, "row"], [1, "col-md-6"], ["class", "col-md-6", 4, "ngIf", "ngIfElse"]], template: function FofEntityFooterComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵtemplate(1, FofEntityFooterComponent_div_1_Template, 5, 7, "div", 1);
            i0.ɵɵtemplate(2, FofEntityFooterComponent_ng_template_2_Template, 2, 0, "ng-template", null, 2, i0.ɵɵtemplateRefExtractor);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.entityBase);
        } }, directives: [i1.NgIf], pipes: [i1.DatePipe], styles: [".custom-card[_ngcontent-%COMP%]{font-size:smaller;padding-top:15px}"] });
    return FofEntityFooterComponent;
}());
export { FofEntityFooterComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofEntityFooterComponent, [{
        type: Component,
        args: [{
                selector: 'fof-entity-footer',
                templateUrl: './fof-entity-footer.component.html',
                styleUrls: ['./fof-entity-footer.component.scss']
            }]
    }], function () { return []; }, { entityBase: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWVudGl0eS1mb290ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vZm9mLWVudGl0eS1mb290ZXIvZm9mLWVudGl0eS1mb290ZXIuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi1lbnRpdHktZm9vdGVyL2ZvZi1lbnRpdHktZm9vdGVyLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFBOzs7O0lDTy9ELDhCQUNFO0lBQ0EsWUFFRjs7SUFBQSxpQkFBTTs7O0lBRkosZUFFRjtJQUZFLDJLQUVGOzs7SUFWRiw4QkFDRTtJQUFBLDhCQUNFO0lBQ0EsWUFFRjs7SUFBQSxpQkFBTTtJQUNOLCtFQUNFO0lBSUosaUJBQU07Ozs7SUFSRixlQUVGO0lBRkUsOEpBRUY7SUFDc0IsZUFBaUQ7SUFBakQseURBQWlELG1CQUFBOzs7SUFPdkUsOEJBQ0U7SUFBQSw2REFDRjtJQUFBLGlCQUFNOztBRGJWO0lBUUU7SUFBZ0IsQ0FBQztJQUVqQiwyQ0FBUSxHQUFSO0lBR0EsQ0FBQztvR0FSVSx3QkFBd0I7aUVBQXhCLHdCQUF3QjtZQ1JyQyw4QkFDRTtZQUFBLHlFQUNFO1lBV0YsMEhBQ0U7WUFJSixpQkFBTTs7WUFqQmEsZUFBa0I7WUFBbEIscUNBQWtCOzttQ0REckM7Q0FpQkMsQUFkRCxJQWNDO1NBVFksd0JBQXdCO2tEQUF4Qix3QkFBd0I7Y0FMcEMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLFdBQVcsRUFBRSxvQ0FBb0M7Z0JBQ2pELFNBQVMsRUFBRSxDQUFDLG9DQUFvQyxDQUFDO2FBQ2xEOztrQkFFRSxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPbkNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBpRm9mRW50aXR5Q29yZSB9IGZyb20gJy4uLy4uL2NvcmUvZW50aXR5Q29yZS5pbnRlcmZhY2UnXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1lbnRpdHktZm9vdGVyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9mLWVudGl0eS1mb290ZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi1lbnRpdHktZm9vdGVyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZkVudGl0eUZvb3RlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgZW50aXR5QmFzZTogaUZvZkVudGl0eUNvcmVcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICBcclxuICAgIFxyXG4gIH1cclxufVxyXG4iLCI8ZGl2IGNsYXNzPVwiY3VzdG9tLWNhcmRcIj5cclxuICA8ZGl2IGNsYXNzPVwicm93XCIgKm5nSWY9XCJlbnRpdHlCYXNlXCI+XHJcbiAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgICAgPCEtLSBDcsOpYXRpb24gbGUge3sgZW50aXR5QmFzZS5fY3JlYXRlZERhdGUgfCBkYXRlOidFRUVFIGQgTU1NTSB5IMOgIEhIOm1tOnNzJyB9fSAgLS0+XHJcbiAgICAgIENyw6lhdGlvbiBsZSB7eyBlbnRpdHlCYXNlLl9jcmVhdGVkRGF0ZSB8IGRhdGU6J3Nob3J0JyB9fSBcclxuICAgICAgcGFyIHt7IHRoaXMuZW50aXR5QmFzZS5fY3JlYXRlZEJ5TmFtZSB9fVxyXG4gICAgPC9kaXY+XHJcbiAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIiAqbmdJZj1cImVudGl0eUJhc2UuX3VwZGF0ZWRCeU5hbWU7IGVsc2UgZWxzZUJsb2NrXCI+ICAgICAgXHJcbiAgICAgIDwhLS0gRGVybmnDqHJlIG1vZGlmaWNhdGlvbiBsZSB7eyBlbnRpdHlCYXNlLl91cGRhdGVkRGF0ZSB8IGRhdGU6J0VFRUUgZCBNTU1NIHkgw6AgSEg6bW06c3MnIH19ICAtLT5cclxuICAgICAgRGVybmnDqHJlIG1vZGlmaWNhdGlvbiBsZSB7eyBlbnRpdHlCYXNlLl91cGRhdGVkRGF0ZSB8IGRhdGU6J3Nob3J0JyB9fSBcclxuICAgICAgcGFyIHt7IHRoaXMuZW50aXR5QmFzZS5fdXBkYXRlZEJ5TmFtZSB9fVxyXG4gICAgPC9kaXY+ICAgICAgIFxyXG4gIDwvZGl2PlxyXG4gIDxuZy10ZW1wbGF0ZSAjZWxzZUJsb2NrPlxyXG4gICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+ICAgICAgXHJcbiAgICAgIEF1Y3VuZSBtb2RpZmljYXRpb24gZGVwdWlzIGxhIGNyw6lhdGlvblxyXG4gICAgPC9kaXY+XHJcbiAgPC9uZy10ZW1wbGF0ZT4gICAgXHJcbjwvZGl2PlxyXG4iXX0=
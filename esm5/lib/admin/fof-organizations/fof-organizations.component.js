import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { fofUtilsForm } from '../../core/fof-utils';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "@angular/forms";
import * as i3 from "../../core/notification/notification.service";
import * as i4 from "../../core/fof-dialog.service";
import * as i5 from "../../core/fof-error.service";
import * as i6 from "@angular/material/card";
import * as i7 from "../../components/fof-organizations-tree/fof-organizations-tree.component";
import * as i8 from "@angular/common";
import * as i9 from "@angular/material/button";
import * as i10 from "@angular/material/progress-spinner";
import * as i11 from "@angular/material/form-field";
import * as i12 from "@angular/material/input";
function FofOrganizationsComponent_button_9_Template(rf, ctx) { if (rf & 1) {
    var _r287 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 11);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_button_9_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r287); var ctx_r286 = i0.ɵɵnextContext(); return ctx_r286.uiAction.organisationCancel(); });
    i0.ɵɵtext(1, "Annuler");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_button_10_Template(rf, ctx) { if (rf & 1) {
    var _r289 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 12);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_button_10_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r289); var ctx_r288 = i0.ɵɵnextContext(); return ctx_r288.uiAction.organisationDelete(); });
    i0.ɵɵtext(1, "Supprimer");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_button_11_Template(rf, ctx) { if (rf & 1) {
    var _r291 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 13);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_button_11_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r291); var ctx_r290 = i0.ɵɵnextContext(); return ctx_r290.uiAction.organisationSave(); });
    i0.ɵɵtext(1, " Enregistrer");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 14);
    i0.ɵɵelement(1, "mat-spinner", 15);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des organisations...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_button_5_Template(rf, ctx) { if (rf & 1) {
    var _r295 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "button", 22);
    i0.ɵɵlistener("click", function FofOrganizationsComponent_div_13_button_5_Template_button_click_0_listener() { i0.ɵɵrestoreView(_r295); var ctx_r294 = i0.ɵɵnextContext(2); return ctx_r294.uiAction.organisationAdd(); });
    i0.ɵɵtext(1, "Ajouter une organisation enfant");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le nom ne doit pas \u00EAtre nul et faire plus de 100 cararct\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵelementStart(1, "div", 17);
    i0.ɵɵelementStart(2, "h4");
    i0.ɵɵtext(3);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(4, "div", 5);
    i0.ɵɵtemplate(5, FofOrganizationsComponent_div_13_button_5_Template, 2, 0, "button", 18);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(6, "form", 19);
    i0.ɵɵelementStart(7, "mat-form-field");
    i0.ɵɵelement(8, "input", 20);
    i0.ɵɵtemplate(9, FofOrganizationsComponent_div_13_mat_error_9_Template, 2, 0, "mat-error", 21);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r285 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵtextInterpolate(ctx_r285.uiVar.actionTitle);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r285.uiVar.organizationIsNew);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("formGroup", ctx_r285.uiVar.form);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r285.uiVar.form.get("name").invalid);
} }
var FofOrganizationsComponent = /** @class */ (function () {
    function FofOrganizationsComponent(fofPermissionService, formBuilder, fofNotificationService, fofDialogService, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            currentNode: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            loading: false,
            organizationIsNew: false,
            nodeForm: false,
            form: this.formBuilder.group({
                name: ['', [Validators.required, Validators.maxLength(100)]],
                description: ['', [Validators.maxLength(200)]],
            }),
            actionTitle: "Modification de l'organisation",
            mainTitle: "G\u00E9rer l'organisation",
            nodeChanged: undefined,
            nodeToDelete: undefined
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationAdd: function () {
                if (!_this.priVar.currentNode) {
                    return;
                }
                _this.uiVar.form.reset();
                _this.uiVar.organizationIsNew = true;
                _this.uiVar.actionTitle = "Ajouter une organisation enfant";
            },
            organisationDelete: function () {
                var nodeToDelete = _this.priVar.currentNode;
                _this.fofDialogService.openYesNo({
                    title: "Supprimer une organisation",
                    question: "Voulez vous vraiment supprimer \n          " + nodeToDelete.name + " ?"
                }).then(function (yes) {
                    if (yes) {
                        _this.fofPermissionService.organization.delete(nodeToDelete)
                            .toPromise()
                            .then(function (result) {
                            _this.uiVar.nodeToDelete = nodeToDelete;
                        })
                            .catch(function (reason) {
                            if (reason.isConstraintError) {
                                _this.fofNotificationService.info("Vous ne pouvez pas supprimer l'organisation !<br>\n                <small>Vous devez supprimer tous les objets rattach\u00E9s \u00E0 l'organisation ou contacter un admninistrateur</small>", { mustDisappearAfter: -1 });
                            }
                            else {
                                _this.fofErrorService.errorManage(reason);
                            }
                        });
                    }
                });
                _this.uiVar.organizationIsNew = false;
            },
            organisationSave: function () {
                var nodeToSave = _this.uiVar.form.value;
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                // New organization
                if (_this.uiVar.organizationIsNew) {
                    nodeToSave.parentId = _this.priVar.currentNode.id;
                    _this.fofPermissionService.organization.create({
                        id: nodeToSave.id,
                        name: nodeToSave.name
                    })
                        .toPromise()
                        .then(function (nodeResult) {
                        _this.fofNotificationService.saveIsDone();
                        _this.priVar.currentNode.children.push(nodeResult);
                        // deep copy for pushing the tree component to refresh the object
                        _this.uiVar.nodeChanged = JSON.parse(JSON.stringify(_this.priVar.currentNode));
                        _this.uiAction.selectedOrganizationsChange(_this.priVar.currentNode);
                        _this.uiVar.organizationIsNew = false;
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    // Update an organization
                    _this.priVar.currentNode.name = nodeToSave.name;
                    _this.fofPermissionService.organization.update({
                        id: _this.priVar.currentNode.id,
                        name: _this.priVar.currentNode.name
                    })
                        .toPromise()
                        .then(function (nodeResult) {
                        _this.uiVar.nodeChanged = nodeResult;
                        _this.fofNotificationService.saveIsDone();
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
            },
            organisationCancel: function () {
                _this.uiVar.actionTitle = "Modification de l'organisation";
                _this.uiVar.nodeForm = false;
                _this.uiVar.organizationIsNew = false;
            },
            selectedOrganizationsChange: function (node) {
                if (node) {
                    _this.uiVar.nodeForm = node.checked;
                    _this.uiVar.form.patchValue(node);
                }
                else {
                    _this.uiVar.nodeForm = false;
                    _this.uiVar.form.reset();
                }
                if (node && node.checked) {
                    _this.priVar.currentNode = node;
                    _this.uiVar.mainTitle = node.name;
                }
                else {
                    _this.priVar.currentNode = null;
                    _this.uiVar.mainTitle = "G\u00E9rer l'organisation";
                }
            }
        };
    }
    // Angular events
    FofOrganizationsComponent.prototype.ngOnInit = function () {
    };
    FofOrganizationsComponent.ɵfac = function FofOrganizationsComponent_Factory(t) { return new (t || FofOrganizationsComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FormBuilder), i0.ɵɵdirectiveInject(i3.FofNotificationService), i0.ɵɵdirectiveInject(i4.FofDialogService), i0.ɵɵdirectiveInject(i5.FofErrorService)); };
    FofOrganizationsComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofOrganizationsComponent, selectors: [["fof-core-fof-organizations"]], decls: 14, vars: 8, consts: [[1, "row", "main"], [1, "col-md-6"], [1, "card-tree"], [3, "nodeChanged", "nodeToDelete", "selectedOrganizationsChange"], [1, "fof-header", "card-detail"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "warn", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "accent", 3, "click", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], ["class", "fof-fade-in card-detail", 4, "ngIf"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in", "card-detail"], [1, "fof-header"], ["mat-stroked-button", "", "class", "child-add", "color", "primary", 3, "click", 4, "ngIf"], [3, "formGroup"], ["matInput", "", "required", "", "type", "name", "formControlName", "name", "placeholder", "Nom de l'organisation", "value", ""], [4, "ngIf"], ["mat-stroked-button", "", "color", "primary", 1, "child-add", 3, "click"]], template: function FofOrganizationsComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "div", 1);
            i0.ɵɵelementStart(2, "mat-card", 2);
            i0.ɵɵelementStart(3, "fof-organizations-tree", 3);
            i0.ɵɵlistener("selectedOrganizationsChange", function FofOrganizationsComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(4, "div", 1);
            i0.ɵɵelementStart(5, "mat-card", 4);
            i0.ɵɵelementStart(6, "h3");
            i0.ɵɵtext(7);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "div", 5);
            i0.ɵɵtemplate(9, FofOrganizationsComponent_button_9_Template, 2, 0, "button", 6);
            i0.ɵɵtemplate(10, FofOrganizationsComponent_button_10_Template, 2, 0, "button", 7);
            i0.ɵɵtemplate(11, FofOrganizationsComponent_button_11_Template, 2, 0, "button", 8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(12, FofOrganizationsComponent_div_12_Template, 4, 0, "div", 9);
            i0.ɵɵtemplate(13, FofOrganizationsComponent_div_13_Template, 10, 4, "div", 10);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("nodeChanged", ctx.uiVar.nodeChanged)("nodeToDelete", ctx.uiVar.nodeToDelete);
            i0.ɵɵadvance(4);
            i0.ɵɵtextInterpolate(ctx.uiVar.mainTitle);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.uiVar.nodeForm);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.uiVar.nodeForm && !ctx.uiVar.organizationIsNew);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.uiVar.nodeForm);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx.uiVar.loading && ctx.uiVar.nodeForm);
        } }, directives: [i6.MatCard, i7.FofOrganizationsTreeComponent, i8.NgIf, i9.MatButton, i10.MatSpinner, i2.ɵangular_packages_forms_forms_y, i2.NgControlStatusGroup, i2.FormGroupDirective, i11.MatFormField, i12.MatInput, i2.DefaultValueAccessor, i2.RequiredValidator, i2.NgControlStatus, i2.FormControlName, i11.MatError], styles: [".row.main[_ngcontent-%COMP%], .row.main[_ngcontent-%COMP%]   .card-tree[_ngcontent-%COMP%]{height:100%}.row.main[_ngcontent-%COMP%]   .card-detail[_ngcontent-%COMP%]{position:-webkit-sticky;position:sticky;top:0}.mat-form-field[_ngcontent-%COMP%]{width:100%}.child-add[_ngcontent-%COMP%]{margin-bottom:15px}"] });
    return FofOrganizationsComponent;
}());
export { FofOrganizationsComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofOrganizationsComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-organizations',
                templateUrl: './fof-organizations.component.html',
                styleUrls: ['./fof-organizations.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FormBuilder }, { type: i3.FofNotificationService }, { type: i4.FofDialogService }, { type: i5.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLW9yZ2FuaXphdGlvbnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vZm9mLW9yZ2FuaXphdGlvbnMvZm9mLW9yZ2FuaXphdGlvbnMuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi1vcmdhbml6YXRpb25zL2ZvZi1vcmdhbml6YXRpb25zLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUE7QUFLakQsT0FBTyxFQUFlLFVBQVUsRUFBRyxNQUFNLGdCQUFnQixDQUFBO0FBR3pELE9BQU8sRUFBRSxZQUFZLEVBQVksTUFBTSxzQkFBc0IsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7OztJQ0tyRCxrQ0FDMEM7SUFBeEMsMktBQVMsc0NBQTZCLElBQUM7SUFBQyx1QkFBTztJQUFBLGlCQUFTOzs7O0lBQzFELGtDQUMwQztJQUF4Qyw0S0FBUyxzQ0FBNkIsSUFBQztJQUFDLHlCQUFTO0lBQUEsaUJBQVM7Ozs7SUFDNUQsa0NBRUU7SUFEQSw0S0FBUyxvQ0FBMkIsSUFBQztJQUNyQyw0QkFBVztJQUFBLGlCQUFTOzs7SUFJMUIsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGdEQUFnQztJQUFBLGlCQUFPO0lBQ3ZGLGlCQUFNOzs7O0lBT0Esa0NBRXVDO0lBQXJDLG1MQUFTLG1DQUEwQixJQUFDO0lBQUMsK0NBQStCO0lBQUEsaUJBQVM7OztJQWUvRSxpQ0FDRTtJQUFBLHdGQUNGO0lBQUEsaUJBQVk7OztJQXhCbEIsK0JBRUU7SUFBQSwrQkFDRTtJQUFBLDBCQUFJO0lBQUEsWUFBdUI7SUFBQSxpQkFBSztJQUNoQyw4QkFDRTtJQUFBLHdGQUV1QztJQUN6QyxpQkFBTTtJQUNSLGlCQUFNO0lBT04sZ0NBQ0U7SUFBQSxzQ0FDRTtJQUFBLDRCQUlBO0lBQUEsOEZBQ0U7SUFFSixpQkFBaUI7SUFDbkIsaUJBQU87SUFDVCxpQkFBTTs7O0lBeEJFLGVBQXVCO0lBQXZCLGdEQUF1QjtJQUVHLGVBQWdDO0lBQWhDLHdEQUFnQztJQVcxRCxlQUF3QjtJQUF4QiwrQ0FBd0I7SUFNZixlQUFzQztJQUF0Qyw4REFBc0M7O0FEaEMzRDtJQU9FLG1DQUNVLG9CQUEwQyxFQUMxQyxXQUF3QixFQUN4QixzQkFBOEMsRUFDOUMsZ0JBQWtDLEVBQ2xDLGVBQWdDO1FBTDFDLGlCQVFDO1FBUFMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQUMxQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBSzFDLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixXQUFXLEVBQWlCLFNBQVM7U0FDdEMsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUcsRUFDbEIsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixPQUFPLEVBQUUsS0FBSztZQUNkLGlCQUFpQixFQUFFLEtBQUs7WUFDeEIsUUFBUSxFQUFFLEtBQUs7WUFDZixJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2dCQUM1RCxXQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7YUFDL0MsQ0FBQztZQUNGLFdBQVcsRUFBRSxnQ0FBZ0M7WUFDN0MsU0FBUyxFQUFFLDJCQUFzQjtZQUNqQyxXQUFXLEVBQWlCLFNBQVM7WUFDckMsWUFBWSxFQUFpQixTQUFTO1NBQ3ZDLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHO1lBQ2hCLGVBQWUsRUFBQztnQkFDZCxJQUFJLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUU7b0JBQzVCLE9BQU07aUJBQ1A7Z0JBQ0QsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7Z0JBQ3ZCLEtBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFBO2dCQUNuQyxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxpQ0FBaUMsQ0FBQTtZQUM1RCxDQUFDO1lBQ0Qsa0JBQWtCLEVBQUM7Z0JBRWpCLElBQU0sWUFBWSxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFBO2dCQUU1QyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO29CQUM5QixLQUFLLEVBQUUsNEJBQTRCO29CQUNuQyxRQUFRLEVBQUUsZ0RBQ04sWUFBWSxDQUFDLElBQUksT0FBSTtpQkFDMUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7b0JBQ1QsSUFBSSxHQUFHLEVBQUU7d0JBQ1AsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDOzZCQUMxRCxTQUFTLEVBQUU7NkJBQ1gsSUFBSSxDQUFDLFVBQUEsTUFBTTs0QkFDVixLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUE7d0JBQ3hDLENBQUMsQ0FBQzs2QkFDRCxLQUFLLENBQUMsVUFBQSxNQUFNOzRCQUNYLElBQUksTUFBTSxDQUFDLGlCQUFpQixFQUFFO2dDQUM1QixLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLDZMQUNnRixFQUM3RyxFQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQTs2QkFDOUI7aUNBQU07Z0NBQ0wsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7NkJBQ3pDO3dCQUNILENBQUMsQ0FBQyxDQUFBO3FCQUNIO2dCQUNILENBQUMsQ0FBQyxDQUFBO2dCQUNGLEtBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFBO1lBQ3RDLENBQUM7WUFDRCxnQkFBZ0IsRUFBQztnQkFDZixJQUFNLFVBQVUsR0FBa0IsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBO2dCQUV2RCxJQUFJLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUMxQixLQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxDQUFDLENBQUE7b0JBQ3ZGLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO29CQUMvQyxPQUFNO2lCQUNQO2dCQUVELG1CQUFtQjtnQkFDbkIsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixFQUFFO29CQUNoQyxVQUFVLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQTtvQkFDaEQsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7d0JBQzVDLEVBQUUsRUFBRSxVQUFVLENBQUMsRUFBRTt3QkFDakIsSUFBSSxFQUFFLFVBQVUsQ0FBQyxJQUFJO3FCQUN0QixDQUFDO3lCQUNELFNBQVMsRUFBRTt5QkFDWCxJQUFJLENBQUMsVUFBQyxVQUF5Qjt3QkFDOUIsS0FBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRSxDQUFBO3dCQUN4QyxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFBO3dCQUVqRCxpRUFBaUU7d0JBQ2pFLEtBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUE7d0JBRTVFLEtBQUksQ0FBQyxRQUFRLENBQUMsMkJBQTJCLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTt3QkFDbEUsS0FBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUE7b0JBQ3RDLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsVUFBQSxNQUFNO3dCQUNYLEtBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUMxQyxDQUFDLENBQUMsQ0FBQTtpQkFDSDtxQkFBTTtvQkFDTCx5QkFBeUI7b0JBQ3pCLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFBO29CQUM5QyxLQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQzt3QkFDNUMsRUFBRSxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEVBQUU7d0JBQzlCLElBQUksRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJO3FCQUNuQyxDQUFDO3lCQUNELFNBQVMsRUFBRTt5QkFDWCxJQUFJLENBQUMsVUFBQyxVQUF5Qjt3QkFDOUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFBO3dCQUNuQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsVUFBVSxFQUFFLENBQUE7b0JBQzFDLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsVUFBQSxNQUFNO3dCQUNYLEtBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUMxQyxDQUFDLENBQUMsQ0FBQTtpQkFDSDtZQUNILENBQUM7WUFDRCxrQkFBa0IsRUFBQztnQkFDakIsS0FBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsZ0NBQWdDLENBQUE7Z0JBQ3pELEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQTtnQkFDM0IsS0FBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUE7WUFDdEMsQ0FBQztZQUNELDJCQUEyQixFQUFDLFVBQUMsSUFBWTtnQkFDdkMsSUFBSSxJQUFJLEVBQUU7b0JBQ1IsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtvQkFDbEMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFBO2lCQUNqQztxQkFBTTtvQkFDTCxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7b0JBQzNCLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFBO2lCQUN4QjtnQkFFRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUN4QixLQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUE7b0JBQzlCLEtBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUE7aUJBQ2pDO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtvQkFDOUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsMkJBQXNCLENBQUE7aUJBQzlDO1lBQ0gsQ0FBQztTQUNGLENBQUE7SUFsSUQsQ0FBQztJQW1JRCxpQkFBaUI7SUFDakIsNENBQVEsR0FBUjtJQUVBLENBQUM7c0dBaEpVLHlCQUF5QjtrRUFBekIseUJBQXlCO1lDdEJ0Qyw4QkFDRTtZQUFBLDhCQUNFO1lBQUEsbUNBQ0U7WUFBQSxpREFHK0Q7WUFGN0Qsd0tBQStCLGdEQUE0QyxJQUFDO1lBRXhDLGlCQUF5QjtZQUNqRSxpQkFBVztZQUNiLGlCQUFNO1lBQ04sOEJBQ0U7WUFBQSxtQ0FDRTtZQUFBLDBCQUFJO1lBQUEsWUFBcUI7WUFBQSxpQkFBSztZQUM5Qiw4QkFDRTtZQUFBLGdGQUMwQztZQUMxQyxrRkFDMEM7WUFDMUMsa0ZBRUU7WUFDSixpQkFBTTtZQUNSLGlCQUFXO1lBRVgsNEVBQ0U7WUFHRiw4RUFFRTtZQTJCSixpQkFBTTtZQUNSLGlCQUFNOztZQXBERSxlQUFpQztZQUFqQyxtREFBaUMsd0NBQUE7WUFNL0IsZUFBcUI7WUFBckIseUNBQXFCO1lBRUksZUFBc0I7WUFBdEIseUNBQXNCO1lBRVQsZUFBa0Q7WUFBbEQseUVBQWtEO1lBRWhELGVBQXNCO1lBQXRCLHlDQUFzQjtZQU0vRCxlQUFxQjtZQUFyQix3Q0FBcUI7WUFJckIsZUFBd0M7WUFBeEMsK0RBQXdDOztvQ0QzQmpEO0NBdUtDLEFBdEpELElBc0pDO1NBakpZLHlCQUF5QjtrREFBekIseUJBQXlCO2NBTHJDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsNEJBQTRCO2dCQUN0QyxXQUFXLEVBQUUsb0NBQW9DO2dCQUNqRCxTQUFTLEVBQUUsQ0FBQyxvQ0FBb0MsQ0FBQzthQUNsRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IE5lc3RlZFRyZWVDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3RyZWUnXHJcbmltcG9ydCB7IE1hdFRyZWVOZXN0ZWREYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvdHJlZSdcclxuaW1wb3J0IHsgaU9yZ2FuaXphdGlvbiB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzICB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiXHJcbmltcG9ydCB7IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgRm9mRGlhbG9nU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLWRpYWxvZy5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBmb2ZVdGlsc0Zvcm0sIGZvZkVycm9yIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtdXRpbHMnXHJcbmltcG9ydCB7IEZvZkVycm9yU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLWVycm9yLnNlcnZpY2UnXHJcblxyXG5pbnRlcmZhY2UgaU9yZ1VJIGV4dGVuZHMgaU9yZ2FuaXphdGlvbiB7XHJcbiAgY2hlY2tlZD86IGJvb2xlYW4sXHJcbiAgaW5kZXRlcm1pbmF0ZT86IGJvb2xlYW4sXHJcbiAgY2hpbGRyZW4/OiBpT3JnVUlbXVxyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1jb3JlLWZvZi1vcmdhbml6YXRpb25zJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9mLW9yZ2FuaXphdGlvbnMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi1vcmdhbml6YXRpb25zLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZk9yZ2FuaXphdGlvbnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvZlBlcm1pc3Npb25TZXJ2aWNlOiBGb2ZQZXJtaXNzaW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZEaWFsb2dTZXJ2aWNlOiBGb2ZEaWFsb2dTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZFcnJvclNlcnZpY2U6IEZvZkVycm9yU2VydmljZVxyXG4gICkgeyBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICBjdXJyZW50Tm9kZTogPGlPcmdhbml6YXRpb24+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICBvcmdhbml6YXRpb25Jc05ldzogZmFsc2UsICBcclxuICAgIG5vZGVGb3JtOiBmYWxzZSxcclxuICAgIGZvcm06IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoeyBcclxuICAgICAgbmFtZTogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMTAwKV1dLFxyXG4gICAgICBkZXNjcmlwdGlvbjogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMjAwKV1dLFxyXG4gICAgfSksXHJcbiAgICBhY3Rpb25UaXRsZTogYE1vZGlmaWNhdGlvbiBkZSBsJ29yZ2FuaXNhdGlvbmAsXHJcbiAgICBtYWluVGl0bGU6IGBHw6lyZXIgbCdvcmdhbmlzYXRpb25gLFxyXG4gICAgbm9kZUNoYW5nZWQ6IDxpT3JnYW5pemF0aW9uPnVuZGVmaW5lZCxcclxuICAgIG5vZGVUb0RlbGV0ZTogPGlPcmdhbml6YXRpb24+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHsgICAgXHJcbiAgICBvcmdhbmlzYXRpb25BZGQ6KCkgPT4ge1xyXG4gICAgICBpZiAoIXRoaXMucHJpVmFyLmN1cnJlbnROb2RlKSB7XHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuICAgICAgdGhpcy51aVZhci5mb3JtLnJlc2V0KClcclxuICAgICAgdGhpcy51aVZhci5vcmdhbml6YXRpb25Jc05ldyA9IHRydWVcclxuICAgICAgdGhpcy51aVZhci5hY3Rpb25UaXRsZSA9IGBBam91dGVyIHVuZSBvcmdhbmlzYXRpb24gZW5mYW50YFxyXG4gICAgfSxcclxuICAgIG9yZ2FuaXNhdGlvbkRlbGV0ZTooKSA9PiB7XHJcblxyXG4gICAgICBjb25zdCBub2RlVG9EZWxldGUgPSB0aGlzLnByaVZhci5jdXJyZW50Tm9kZVxyXG4gICAgIFxyXG4gICAgICB0aGlzLmZvZkRpYWxvZ1NlcnZpY2Uub3Blblllc05vKHtcclxuICAgICAgICB0aXRsZTogXCJTdXBwcmltZXIgdW5lIG9yZ2FuaXNhdGlvblwiLFxyXG4gICAgICAgIHF1ZXN0aW9uOiBgVm91bGV6IHZvdXMgdnJhaW1lbnQgc3VwcHJpbWVyIFxyXG4gICAgICAgICAgJHtub2RlVG9EZWxldGUubmFtZX0gP2BcclxuICAgICAgfSkudGhlbih5ZXMgPT4geyAgICAgICAgXHJcbiAgICAgICAgaWYgKHllcykgeyAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2Uub3JnYW5pemF0aW9uLmRlbGV0ZShub2RlVG9EZWxldGUpXHJcbiAgICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudWlWYXIubm9kZVRvRGVsZXRlID0gbm9kZVRvRGVsZXRlXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGlmIChyZWFzb24uaXNDb25zdHJhaW50RXJyb3IpIHtcclxuICAgICAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuaW5mbyhgVm91cyBuZSBwb3V2ZXogcGFzIHN1cHByaW1lciBsJ29yZ2FuaXNhdGlvbiAhPGJyPlxyXG4gICAgICAgICAgICAgICAgPHNtYWxsPlZvdXMgZGV2ZXogc3VwcHJpbWVyIHRvdXMgbGVzIG9iamV0cyByYXR0YWNow6lzIMOgIGwnb3JnYW5pc2F0aW9uIG91IGNvbnRhY3RlciB1biBhZG1uaW5pc3RyYXRldXI8L3NtYWxsPmBcclxuICAgICAgICAgICAgICAgICwge211c3REaXNhcHBlYXJBZnRlcjogLTF9KVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcclxuICAgICAgICAgICAgfSAgICAgICAgICAgIFxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICAgIHRoaXMudWlWYXIub3JnYW5pemF0aW9uSXNOZXcgPSBmYWxzZVxyXG4gICAgfSxcclxuICAgIG9yZ2FuaXNhdGlvblNhdmU6KCkgPT4ge1xyXG4gICAgICBjb25zdCBub2RlVG9TYXZlOiBpT3JnYW5pemF0aW9uID0gdGhpcy51aVZhci5mb3JtLnZhbHVlICAgICBcclxuXHJcbiAgICAgIGlmICghdGhpcy51aVZhci5mb3JtLnZhbGlkKSB7XHJcbiAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdWZXVpbGxleiBjb3JyaWdlciBsZXMgZXJyZXVycyBhdmFudCBkZSBzYXV2ZWdhcmRlcicpXHJcbiAgICAgICAgZm9mVXRpbHNGb3JtLnZhbGlkYXRlQWxsRmllbGRzKHRoaXMudWlWYXIuZm9ybSlcclxuICAgICAgICByZXR1cm5cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gTmV3IG9yZ2FuaXphdGlvblxyXG4gICAgICBpZiAodGhpcy51aVZhci5vcmdhbml6YXRpb25Jc05ldykge1xyXG4gICAgICAgIG5vZGVUb1NhdmUucGFyZW50SWQgPSB0aGlzLnByaVZhci5jdXJyZW50Tm9kZS5pZFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2Uub3JnYW5pemF0aW9uLmNyZWF0ZSh7XHJcbiAgICAgICAgICBpZDogbm9kZVRvU2F2ZS5pZCxcclxuICAgICAgICAgIG5hbWU6IG5vZGVUb1NhdmUubmFtZVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4oKG5vZGVSZXN1bHQ6IGlPcmdhbml6YXRpb24pID0+IHsgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc2F2ZUlzRG9uZSgpXHJcbiAgICAgICAgICB0aGlzLnByaVZhci5jdXJyZW50Tm9kZS5jaGlsZHJlbi5wdXNoKG5vZGVSZXN1bHQpICBcclxuXHJcbiAgICAgICAgICAvLyBkZWVwIGNvcHkgZm9yIHB1c2hpbmcgdGhlIHRyZWUgY29tcG9uZW50IHRvIHJlZnJlc2ggdGhlIG9iamVjdFxyXG4gICAgICAgICAgdGhpcy51aVZhci5ub2RlQ2hhbmdlZCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5wcmlWYXIuY3VycmVudE5vZGUpKVxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnVpQWN0aW9uLnNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZSh0aGlzLnByaVZhci5jdXJyZW50Tm9kZSlcclxuICAgICAgICAgIHRoaXMudWlWYXIub3JnYW5pemF0aW9uSXNOZXcgPSBmYWxzZVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgfSkgICAgICBcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyBVcGRhdGUgYW4gb3JnYW5pemF0aW9uXHJcbiAgICAgICAgdGhpcy5wcmlWYXIuY3VycmVudE5vZGUubmFtZSA9IG5vZGVUb1NhdmUubmFtZVxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2Uub3JnYW5pemF0aW9uLnVwZGF0ZSh7XHJcbiAgICAgICAgICBpZDogdGhpcy5wcmlWYXIuY3VycmVudE5vZGUuaWQsXHJcbiAgICAgICAgICBuYW1lOiB0aGlzLnByaVZhci5jdXJyZW50Tm9kZS5uYW1lXHJcbiAgICAgICAgfSlcclxuICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAudGhlbigobm9kZVJlc3VsdDogaU9yZ2FuaXphdGlvbikgPT4geyAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLm5vZGVDaGFuZ2VkID0gbm9kZVJlc3VsdCAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnNhdmVJc0RvbmUoKSAgICAgICAgICBcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChyZWFzb24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0gICAgICBcclxuICAgIH0sXHJcbiAgICBvcmdhbmlzYXRpb25DYW5jZWw6KCkgPT4geyAgICAgICAgICAgIFxyXG4gICAgICB0aGlzLnVpVmFyLmFjdGlvblRpdGxlID0gYE1vZGlmaWNhdGlvbiBkZSBsJ29yZ2FuaXNhdGlvbmBcclxuICAgICAgdGhpcy51aVZhci5ub2RlRm9ybSA9IGZhbHNlXHJcbiAgICAgIHRoaXMudWlWYXIub3JnYW5pemF0aW9uSXNOZXcgPSBmYWxzZVxyXG4gICAgfSxcclxuICAgIHNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZToobm9kZTogaU9yZ1VJKSA9PiB7XHJcbiAgICAgIGlmIChub2RlKSB7XHJcbiAgICAgICAgdGhpcy51aVZhci5ub2RlRm9ybSA9IG5vZGUuY2hlY2tlZCBcclxuICAgICAgICB0aGlzLnVpVmFyLmZvcm0ucGF0Y2hWYWx1ZShub2RlKVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMudWlWYXIubm9kZUZvcm0gPSBmYWxzZVxyXG4gICAgICAgIHRoaXMudWlWYXIuZm9ybS5yZXNldCgpXHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIGlmIChub2RlICYmIG5vZGUuY2hlY2tlZCkge1xyXG4gICAgICAgIHRoaXMucHJpVmFyLmN1cnJlbnROb2RlID0gbm9kZVxyXG4gICAgICAgIHRoaXMudWlWYXIubWFpblRpdGxlID0gbm9kZS5uYW1lXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5wcmlWYXIuY3VycmVudE5vZGUgPSBudWxsXHJcbiAgICAgICAgdGhpcy51aVZhci5tYWluVGl0bGUgPSBgR8OpcmVyIGwnb3JnYW5pc2F0aW9uYFxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgXHJcbiAgfSAgXHJcbn1cclxuIiwiPGRpdiBjbGFzcz1cInJvdyBtYWluXCI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCI+XHJcbiAgICA8bWF0LWNhcmQgY2xhc3M9XCJjYXJkLXRyZWVcIj5cclxuICAgICAgPGZvZi1vcmdhbml6YXRpb25zLXRyZWVcclxuICAgICAgICAoc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlKT1cInVpQWN0aW9uLnNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZSgkZXZlbnQpXCJcclxuICAgICAgICBbbm9kZUNoYW5nZWRdPVwidWlWYXIubm9kZUNoYW5nZWRcIlxyXG4gICAgICAgIFtub2RlVG9EZWxldGVdPVwidWlWYXIubm9kZVRvRGVsZXRlXCI+PC9mb2Ytb3JnYW5pemF0aW9ucy10cmVlPiAgICAgIFxyXG4gICAgPC9tYXQtY2FyZD5cclxuICA8L2Rpdj5cclxuICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuICAgIDxtYXQtY2FyZCBjbGFzcz1cImZvZi1oZWFkZXIgY2FyZC1kZXRhaWxcIj4gICAgICAgIFxyXG4gICAgICA8aDM+e3sgdWlWYXIubWFpblRpdGxlIH19PC9oMz4gXHJcbiAgICAgIDxkaXYgY2xhc3M9XCJmb2YtdG9vbGJhclwiPiAgICBcclxuICAgICAgICA8YnV0dG9uIG1hdC1zdHJva2VkLWJ1dHRvbiAqbmdJZj1cInVpVmFyLm5vZGVGb3JtXCJcclxuICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5vcmdhbmlzYXRpb25DYW5jZWwoKVwiPkFubnVsZXI8L2J1dHRvbj4gICAgXHJcbiAgICAgICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJ3YXJuXCIgKm5nSWY9XCJ1aVZhci5ub2RlRm9ybSAmJiAhdWlWYXIub3JnYW5pemF0aW9uSXNOZXdcIlxyXG4gICAgICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLm9yZ2FuaXNhdGlvbkRlbGV0ZSgpXCI+U3VwcHJpbWVyPC9idXR0b24+ICAgICBcclxuICAgICAgICA8YnV0dG9uIG1hdC1zdHJva2VkLWJ1dHRvbiBjb2xvcj1cImFjY2VudFwiICpuZ0lmPVwidWlWYXIubm9kZUZvcm1cIlxyXG4gICAgICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLm9yZ2FuaXNhdGlvblNhdmUoKVwiPlxyXG4gICAgICAgICAgRW5yZWdpc3RyZXI8L2J1dHRvbj4gICAgICAgIFxyXG4gICAgICA8L2Rpdj4gXHJcbiAgICA8L21hdC1jYXJkPlxyXG4gICAgXHJcbiAgICA8ZGl2ICpuZ0lmPVwidWlWYXIubG9hZGluZ1wiIGNsYXNzPVwiZm9mLWxvYWRpbmdcIj5cclxuICAgICAgPG1hdC1zcGlubmVyIGRpYW1ldGVyPTIwPjwvbWF0LXNwaW5uZXI+IDxzcGFuPkNoYXJnZW1lbnRzIGRlcyBvcmdhbmlzYXRpb25zLi4uPC9zcGFuPlxyXG4gICAgPC9kaXY+XHJcbiAgICBcclxuICAgIDxkaXYgKm5nSWY9XCIhdWlWYXIubG9hZGluZyAmJiB1aVZhci5ub2RlRm9ybVwiIGNsYXNzPVwiZm9mLWZhZGUtaW4gY2FyZC1kZXRhaWxcIj5cclxuXHJcbiAgICAgIDxkaXYgY2xhc3M9XCJmb2YtaGVhZGVyXCI+XHJcbiAgICAgICAgPGg0Pnt7IHVpVmFyLmFjdGlvblRpdGxlIH19PC9oND5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiZm9mLXRvb2xiYXJcIj5cclxuICAgICAgICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uICAqbmdJZj1cIiF1aVZhci5vcmdhbml6YXRpb25Jc05ld1wiXHJcbiAgICAgICAgICAgIGNsYXNzPVwiY2hpbGQtYWRkXCIgY29sb3I9XCJwcmltYXJ5XCJcclxuICAgICAgICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLm9yZ2FuaXNhdGlvbkFkZCgpXCI+QWpvdXRlciB1bmUgb3JnYW5pc2F0aW9uIGVuZmFudDwvYnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PiBcclxuICAgICAgPC9kaXY+XHJcblxyXG5cclxuICAgICAgPCEtLSA8YnV0dG9uIG1hdC1zdHJva2VkLWJ1dHRvbiAgKm5nSWY9XCIhdWlWYXIub3JnYW5pemF0aW9uSXNOZXdcIlxyXG4gICAgICAgIGNsYXNzPVwiY2hpbGQtYWRkXCIgY29sb3I9XCJwcmltYXJ5XCJcclxuICAgICAgICAoY2xpY2spPVwidWlBY3Rpb24ub3JnYW5pc2F0aW9uQWRkKClcIj5Bam91dGVyIHVuZSBvcmdhbmlzYXRpb24gZW5mYW50PC9idXR0b24+ICBcclxuICAgICAgPGg0Pnt7IHVpVmFyLmFjdGlvblRpdGxlIH19PC9oND4gLS0+XHJcbiAgICAgIDxmb3JtIFtmb3JtR3JvdXBdPVwidWlWYXIuZm9ybVwiPlxyXG4gICAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICAgIDxpbnB1dCBtYXRJbnB1dCByZXF1aXJlZCBcclxuICAgICAgICAgICAgdHlwZT1cIm5hbWVcIlxyXG4gICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJuYW1lXCJcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJOb20gZGUgbCdvcmdhbmlzYXRpb25cIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgICAgPG1hdC1lcnJvciAqbmdJZj1cInVpVmFyLmZvcm0uZ2V0KCduYW1lJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICBMZSBub20gbmUgZG9pdCBwYXMgw6p0cmUgbnVsIGV0IGZhaXJlIHBsdXMgZGUgMTAwIGNhcmFyY3TDqHJlc1xyXG4gICAgICAgICAgPC9tYXQtZXJyb3I+XHJcbiAgICAgICAgPC9tYXQtZm9ybS1maWVsZD4gICAgICAgIFxyXG4gICAgICA8L2Zvcm0+ICAgICAgICAgICAgXHJcbiAgICA8L2Rpdj5cclxuICAgIFxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuXHJcbiJdfQ==
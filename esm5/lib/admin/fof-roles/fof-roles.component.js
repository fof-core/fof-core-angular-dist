import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
import * as i4 from "@angular/material/card";
import * as i5 from "@angular/material/button";
import * as i6 from "@angular/router";
import * as i7 from "@angular/common";
import * as i8 from "@angular/material/table";
import * as i9 from "@angular/material/sort";
import * as i10 from "@angular/material/paginator";
import * as i11 from "@angular/material/progress-spinner";
function FofRolesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 13);
    i0.ɵɵelement(1, "mat-spinner", 14);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des roles...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofRolesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 15);
    i0.ɵɵtext(1, "Code");
    i0.ɵɵelementEnd();
} }
function FofRolesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r202 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r202.code);
} }
function FofRolesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Description");
    i0.ɵɵelementEnd();
} }
function FofRolesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 16);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r203 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r203.description);
} }
function FofRolesComponent_tr_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 18);
} }
function FofRolesComponent_tr_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 19);
} if (rf & 2) {
    var row_r204 = ctx.$implicit;
    i0.ɵɵproperty("routerLink", row_r204.code);
} }
var _c0 = function () { return [5, 10, 25, 100]; };
var FofRolesComponent = /** @class */ (function () {
    function FofRolesComponent(fofPermissionService, fofNotificationService, breakpointObserver) {
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['code', 'description'],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    FofRolesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['code'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['code', 'description'];
            }
        });
    };
    FofRolesComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.role.search(null, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function () {
            _this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(function (data) { return _this.uiVar.data = data; });
    };
    FofRolesComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    FofRolesComponent.ɵfac = function FofRolesComponent_Factory(t) { return new (t || FofRolesComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver)); };
    FofRolesComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofRolesComponent, selectors: [["fof-roles"]], viewQuery: function FofRolesComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(MatPaginator, true);
            i0.ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 18, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "code", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "code"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "description"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function FofRolesComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "mat-card", 0);
            i0.ɵɵelementStart(1, "h3");
            i0.ɵɵtext(2, "R\u00F4les");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "a", 1);
            i0.ɵɵelementStart(4, "span");
            i0.ɵɵtext(5, "Ajouter un r\u00F4le");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 2);
            i0.ɵɵtemplate(7, FofRolesComponent_div_7_Template, 4, 0, "div", 3);
            i0.ɵɵelementStart(8, "table", 4);
            i0.ɵɵelementContainerStart(9, 5);
            i0.ɵɵtemplate(10, FofRolesComponent_th_10_Template, 2, 0, "th", 6);
            i0.ɵɵtemplate(11, FofRolesComponent_td_11_Template, 2, 1, "td", 7);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(12, 8);
            i0.ɵɵtemplate(13, FofRolesComponent_th_13_Template, 2, 0, "th", 9);
            i0.ɵɵtemplate(14, FofRolesComponent_td_14_Template, 2, 1, "td", 7);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵtemplate(15, FofRolesComponent_tr_15_Template, 1, 0, "tr", 10);
            i0.ɵɵtemplate(16, FofRolesComponent_tr_16_Template, 1, 1, "tr", 11);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(17, "mat-paginator", 12);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("routerLink", "/admin/roles/new");
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("dataSource", ctx.uiVar.data);
            i0.ɵɵadvance(7);
            i0.ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", i0.ɵɵpureFunction0(8, _c0))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [i4.MatCard, i5.MatAnchor, i6.RouterLinkWithHref, i7.NgIf, i8.MatTable, i9.MatSort, i8.MatColumnDef, i8.MatHeaderCellDef, i8.MatCellDef, i8.MatHeaderRowDef, i8.MatRowDef, i10.MatPaginator, i11.MatSpinner, i8.MatHeaderCell, i9.MatSortHeader, i8.MatCell, i8.MatHeaderRow, i8.MatRow, i6.RouterLink], styles: [""] });
    return FofRolesComponent;
}());
export { FofRolesComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofRolesComponent, [{
        type: Component,
        args: [{
                selector: 'fof-roles',
                templateUrl: './fof-roles.component.html',
                styleUrls: ['./fof-roles.component.scss'],
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXJvbGVzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2FkbWluL2ZvZi1yb2xlcy9mb2Ytcm9sZXMuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi1yb2xlcy9mb2Ytcm9sZXMuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBbUMsU0FBUyxFQUE0QixNQUFNLGVBQWUsQ0FBQTtBQUkvRyxPQUFPLEVBQUUsWUFBWSxFQUFvQixNQUFNLDZCQUE2QixDQUFBO0FBQzVFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQTtBQUNoRCxPQUFPLEVBQUUsS0FBSyxFQUFjLEVBQUUsSUFBSSxZQUFZLEVBQWdCLE1BQU0sTUFBTSxDQUFBO0FBQzFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUV0RSxPQUFPLEVBQXNCLFdBQVcsRUFBbUIsTUFBTSxxQkFBcUIsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7SUNDcEYsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLHdDQUF3QjtJQUFBLGlCQUFPO0lBQy9FLGlCQUFNOzs7SUFNRiw4QkFBc0Q7SUFBQSxvQkFBSTtJQUFBLGlCQUFLOzs7SUFDL0QsOEJBQW1DO0lBQUEsWUFBWTtJQUFBLGlCQUFLOzs7SUFBakIsZUFBWTtJQUFaLG1DQUFZOzs7SUFJL0MsOEJBQXNDO0lBQUEsMkJBQVc7SUFBQSxpQkFBSzs7O0lBQ3RELDhCQUFtQztJQUFBLFlBQW1CO0lBQUEsaUJBQUs7OztJQUF4QixlQUFtQjtJQUFuQiwwQ0FBbUI7OztJQUd4RCx5QkFBa0U7OztJQUNsRSx5QkFDOEQ7OztJQUR6QiwwQ0FBdUI7OztBRGhCaEU7SUFVRSwyQkFDVSxvQkFBMEMsRUFDMUMsc0JBQThDLEVBQzlDLGtCQUFzQztRQUZ0Qyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUtoRCx3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2YscUJBQXFCLEVBQWdCLFNBQVM7U0FDL0MsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUcsRUFDbEIsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixnQkFBZ0IsRUFBWSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUM7WUFDbkQsSUFBSSxFQUFXLEVBQUU7WUFDakIsYUFBYSxFQUFFLENBQUM7WUFDaEIsUUFBUSxFQUFFLENBQUM7WUFDWCxnQkFBZ0IsRUFBRSxJQUFJO1NBQ3ZCLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHLEVBQ2pCLENBQUE7SUFuQkQsQ0FBQztJQW9CRCxpQkFBaUI7SUFDakIsb0NBQVEsR0FBUjtRQUFBLGlCQVdDO1FBVkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDdEYsU0FBUyxDQUFDLFVBQUMsS0FBc0I7WUFDaEMsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO2dCQUNqQixTQUFTO2dCQUNULEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTthQUN2QztpQkFBTTtnQkFDTCxXQUFXO2dCQUNYLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLENBQUE7YUFDdEQ7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCwyQ0FBZSxHQUFmO1FBQUEsaUJBdUJDO1FBdEJDLG9FQUFvRTtRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFBO1FBRWxFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzthQUM3QyxJQUFJLENBQ0gsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLFNBQVMsQ0FBQztZQUNSLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFBO1lBQ2xDLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFBO1lBQzdDLE9BQU8sS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNyRSxLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3BFLENBQUMsQ0FBQyxFQUNGLEdBQUcsQ0FBQyxVQUFDLE1BQXlCO1lBQzVCLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLEtBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUE7WUFDdkMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQztZQUNULEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLE9BQU8sWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1FBQ3pCLENBQUMsQ0FBQyxDQUNILENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxFQUF0QixDQUFzQixDQUFDLENBQUE7SUFDL0MsQ0FBQztJQUVELHVDQUFXLEdBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUU7WUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQUU7SUFDNUYsQ0FBQztzRkF2RVUsaUJBQWlCOzBEQUFqQixpQkFBaUI7MkJBQ2pCLFlBQVk7MkJBQ1osT0FBTzs7Ozs7O1lDcEJwQixtQ0FDRTtZQUFBLDBCQUFJO1lBQUEsMEJBQUs7WUFBQSxpQkFBSztZQUNkLDRCQUVFO1lBQUEsNEJBQU07WUFBQSxvQ0FBZTtZQUFBLGlCQUFPO1lBQzlCLGlCQUFJO1lBQ04saUJBQVc7WUFFWCw4QkFFRTtZQUFBLGtFQUNFO1lBR0YsZ0NBR0U7WUFBQSxnQ0FDRTtZQUFBLGtFQUFzRDtZQUN0RCxrRUFBbUM7WUFDckMsMEJBQWU7WUFFZixpQ0FDRTtZQUFBLGtFQUFzQztZQUN0QyxrRUFBbUM7WUFDckMsMEJBQWU7WUFFZixtRUFBNkQ7WUFDN0QsbUVBQ3lEO1lBQzNELGlCQUFRO1lBRVIscUNBRThDO1lBRWhELGlCQUFNOztZQWpDRixlQUFpQztZQUFqQywrQ0FBaUM7WUFPVSxlQUE4QjtZQUE5QixpREFBOEI7WUFJMUQsZUFBeUI7WUFBekIsMkNBQXlCO1lBYXJCLGVBQXlDO1lBQXpDLDREQUF5QztZQUUxRCxlQUFzRDtZQUF0RCw2REFBc0Q7WUFHM0MsZUFBOEI7WUFBOUIsZ0RBQThCLCtDQUFBLGdDQUFBOzs0QkRoQy9DO0NBMkZDLEFBL0VELElBK0VDO1NBekVZLGlCQUFpQjtrREFBakIsaUJBQWlCO2NBTjdCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsV0FBVztnQkFDckIsV0FBVyxFQUFFLDRCQUE0QjtnQkFDekMsU0FBUyxFQUFFLENBQUMsNEJBQTRCLENBQUM7YUFFMUM7O2tCQUVFLFNBQVM7bUJBQUMsWUFBWTs7a0JBQ3RCLFNBQVM7bUJBQUMsT0FBTyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vZm9mLXBlcm1pc3Npb24uc2VydmljZSdcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBpUm9sZSB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IE1hdFBhZ2luYXRvciwgTWF0UGFnaW5hdG9ySW50bCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3BhZ2luYXRvcidcclxuaW1wb3J0IHsgTWF0U29ydCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NvcnQnXHJcbmltcG9ydCB7IG1lcmdlLCBPYnNlcnZhYmxlLCBvZiBhcyBvYnNlcnZhYmxlT2YsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnXHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCwgc3RhcnRXaXRoLCBzd2l0Y2hNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycydcclxuaW1wb3J0IHsgaWZvZlNlYXJjaCB9IGZyb20gJy4uLy4uL2NvcmUvY29yZS5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IEJyZWFrcG9pbnRPYnNlcnZlciwgQnJlYWtwb2ludHMsIEJyZWFrcG9pbnRTdGF0ZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9sYXlvdXQnXHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2Ytcm9sZXMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mb2Ytcm9sZXMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi1yb2xlcy5jb21wb25lbnQuc2NzcyddLFxyXG4gIC8vIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZSb2xlc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yXHJcbiAgQFZpZXdDaGlsZChNYXRTb3J0KSBzb3J0OiBNYXRTb3J0XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmb2ZQZXJtaXNzaW9uU2VydmljZTogRm9mUGVybWlzc2lvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGJyZWFrcG9pbnRPYnNlcnZlcjogQnJlYWtwb2ludE9ic2VydmVyXHJcbiAgKSB7IFxyXG4gICAgXHJcbiAgfVxyXG5cclxuICAvLyBBbGwgcHJpdmF0ZSB2YXJpYWJsZXNcclxuICBwcml2YXRlIHByaVZhciA9IHtcclxuICAgIGJyZWFrcG9pbnRPYnNlcnZlclN1YjogPFN1YnNjcmlwdGlvbj51bmRlZmluZWQsXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0geyAgICBcclxuICAgIGRpc3BsYXllZENvbHVtbnM6IDxzdHJpbmdbXT5bJ2NvZGUnLCAnZGVzY3JpcHRpb24nXSxcclxuICAgIGRhdGE6IDxpUm9sZVtdPltdLFxyXG4gICAgcmVzdWx0c0xlbmd0aDogMCxcclxuICAgIHBhZ2VTaXplOiA1LFxyXG4gICAgaXNMb2FkaW5nUmVzdWx0czogdHJ1ZVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnByaVZhci5icmVha3BvaW50T2JzZXJ2ZXJTdWIgPSB0aGlzLmJyZWFrcG9pbnRPYnNlcnZlci5vYnNlcnZlKEJyZWFrcG9pbnRzLlhTbWFsbClcclxuICAgIC5zdWJzY3JpYmUoKHN0YXRlOiBCcmVha3BvaW50U3RhdGUpID0+IHsgICAgICBcclxuICAgICAgaWYgKHN0YXRlLm1hdGNoZXMpIHtcclxuICAgICAgICAvLyBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ2NvZGUnXVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vID4gWFNtYWxsXHJcbiAgICAgICAgdGhpcy51aVZhci5kaXNwbGF5ZWRDb2x1bW5zID0gWydjb2RlJywgJ2Rlc2NyaXB0aW9uJ11cclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9ICBcclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCkgeyAgICBcclxuICAgIC8vIElmIHRoZSB1c2VyIGNoYW5nZXMgdGhlIHNvcnQgb3JkZXIsIHJlc2V0IGJhY2sgdG8gdGhlIGZpcnN0IHBhZ2UuXHJcbiAgICB0aGlzLnNvcnQuc29ydENoYW5nZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4ID0gMClcclxuXHJcbiAgICBtZXJnZSh0aGlzLnNvcnQuc29ydENoYW5nZSwgdGhpcy5wYWdpbmF0b3IucGFnZSlcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgc3RhcnRXaXRoKHt9KSxcclxuICAgICAgICBzd2l0Y2hNYXAoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gdHJ1ZSAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMudWlWYXIucGFnZVNpemUgPSB0aGlzLnBhZ2luYXRvci5wYWdlU2l6ZVxyXG4gICAgICAgICAgcmV0dXJuIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2Uucm9sZS5zZWFyY2ggKG51bGwsIHRoaXMudWlWYXIucGFnZVNpemUsIFxyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRvci5wYWdlSW5kZXgsIHRoaXMuc29ydC5hY3RpdmUsIHRoaXMuc29ydC5kaXJlY3Rpb24pXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgbWFwKChzZWFyY2g6IGlmb2ZTZWFyY2g8aVJvbGU+KSA9PiB7ICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMudWlWYXIuaXNMb2FkaW5nUmVzdWx0cyA9IGZhbHNlXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnJlc3VsdHNMZW5ndGggPSBzZWFyY2gudG90YWxcclxuICAgICAgICAgIHJldHVybiBzZWFyY2guZGF0YVxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGNhdGNoRXJyb3IoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2UgICAgICAgICAgXHJcbiAgICAgICAgICByZXR1cm4gb2JzZXJ2YWJsZU9mKFtdKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICkuc3Vic2NyaWJlKGRhdGEgPT4gdGhpcy51aVZhci5kYXRhID0gZGF0YSlcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgaWYgKHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1YikgeyB0aGlzLnByaVZhci5icmVha3BvaW50T2JzZXJ2ZXJTdWIudW5zdWJzY3JpYmUoKSB9XHJcbiAgfVxyXG5cclxufVxyXG4iLCI8bWF0LWNhcmQgY2xhc3M9XCJmb2YtaGVhZGVyXCI+ICAgICAgICBcclxuICA8aDM+UsO0bGVzPC9oMz4gICBcclxuICA8YSBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJhY2NlbnRcIiBcclxuICAgIFtyb3V0ZXJMaW5rXT1cIicvYWRtaW4vcm9sZXMvbmV3J1wiPiAgICBcclxuICAgIDxzcGFuPkFqb3V0ZXIgdW4gcsO0bGU8L3NwYW4+XHJcbiAgPC9hPiAgICBcclxuPC9tYXQtY2FyZD5cclxuXHJcbjxkaXYgY2xhc3M9XCJmb2YtdGFibGUtY29udGFpbmVyIG1hdC1lbGV2YXRpb24tejJcIj5cclxuXHJcbiAgPGRpdiBjbGFzcz1cInRhYmxlLWxvYWRpbmctc2hhZGUgZm9mLWxvYWRpbmdcIiAqbmdJZj1cInVpVmFyLmlzTG9hZGluZ1Jlc3VsdHNcIj5cclxuICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5DaGFyZ2VtZW50cyBkZXMgcm9sZXMuLi48L3NwYW4+XHJcbiAgPC9kaXY+XHJcblxyXG4gIDx0YWJsZSBtYXQtdGFibGUgW2RhdGFTb3VyY2VdPVwidWlWYXIuZGF0YVwiIGNsYXNzPVwiZGF0YS10YWJsZVwiXHJcbiAgICAgICAgbWF0U29ydCBtYXRTb3J0QWN0aXZlPVwiY29kZVwiIG1hdFNvcnREaXNhYmxlQ2xlYXIgbWF0U29ydERpcmVjdGlvbj1cImFzY1wiPlxyXG4gICAgXHJcbiAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImNvZGVcIj5cclxuICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCBtYXQtc29ydC1oZWFkZXIgKm1hdEhlYWRlckNlbGxEZWY+Q29kZTwvdGg+XHJcbiAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIj57e3Jvdy5jb2RlfX08L3RkPlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJkZXNjcmlwdGlvblwiPlxyXG4gICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPkRlc2NyaXB0aW9uPC90aD5cclxuICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LmRlc2NyaXB0aW9ufX08L3RkPlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgPHRyIG1hdC1oZWFkZXItcm93ICptYXRIZWFkZXJSb3dEZWY9XCJ1aVZhci5kaXNwbGF5ZWRDb2x1bW5zXCI+PC90cj5cclxuICAgIDx0ciBtYXQtcm93IGNsYXNzPVwiZm9mLWVsZW1lbnQtb3ZlclwiIFtyb3V0ZXJMaW5rXT1cInJvdy5jb2RlXCJcclxuICAgICAgKm1hdFJvd0RlZj1cImxldCByb3c7IGNvbHVtbnM6IHVpVmFyLmRpc3BsYXllZENvbHVtbnM7XCI+PC90cj5cclxuICA8L3RhYmxlPlxyXG5cclxuICA8bWF0LXBhZ2luYXRvciBbbGVuZ3RoXT1cInVpVmFyLnJlc3VsdHNMZW5ndGhcIiBcclxuICAgIFtwYWdlU2l6ZU9wdGlvbnNdPVwiWzUsIDEwLCAyNSwgMTAwXVwiXHJcbiAgICBbcGFnZVNpemVdPVwidWlWYXIucGFnZVNpemVcIj48L21hdC1wYWdpbmF0b3I+XHJcblxyXG48L2Rpdj5cclxuXHJcbiJdfQ==
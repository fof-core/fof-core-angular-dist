import { Component, Input, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "../../permission/fof-permission.service";
import * as i3 from "../../core/notification/notification.service";
import * as i4 from "../../core/fof-error.service";
import * as i5 from "@angular/common";
import * as i6 from "../../components/fof-organizations-multi-select/fof-organizations-multi-select.component";
import * as i7 from "@angular/material/button";
import * as i8 from "@angular/material/progress-spinner";
import * as i9 from "@angular/material/checkbox";
import * as i10 from "@angular/forms";
function FofUserRolesSelectComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 6);
    i0.ɵɵelement(1, "mat-spinner", 7);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Rafraichissement des r\u00F4les...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofUserRolesSelectComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    var _r241 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 8);
    i0.ɵɵelementStart(1, "mat-checkbox", 9);
    i0.ɵɵlistener("change", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_change_1_listener() { i0.ɵɵrestoreView(_r241); var role_r239 = ctx.$implicit; var ctx_r240 = i0.ɵɵnextContext(); return ctx_r240.uiAction.userRoleOrganizationSave(role_r239); })("ngModelChange", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r241); var role_r239 = ctx.$implicit; return role_r239.checked = $event; });
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(3, "div", 10);
    i0.ɵɵtext(4);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var role_r239 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", role_r239.checked)("checked", role_r239.checked);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(role_r239.code);
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(role_r239.description);
} }
var FofUserRolesSelectComponent = /** @class */ (function () {
    function FofUserRolesSelectComponent(matDialogRef, fofPermissionService, fofNotificationService, fofErrorService, data) {
        var _this = this;
        this.matDialogRef = matDialogRef;
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.fofErrorService = fofErrorService;
        this.userRoleOrganizations = undefined;
        // All private variables
        this.priVar = {
            userId: undefined,
            organizationId: undefined
        };
        // All private functions
        this.privFunc = {
            componenentDataRefresh: function () {
                if (_this.userRoleOrganizations) {
                    // it's an update
                    _this.uiVar.isCreation = false;
                    _this.uiVar.title = "Modification des acc\u00E8s";
                    // Only one organization possible in update
                    _this.uiVar.organizationMultipleSelect = false;
                    if (_this.userRoleOrganizations.length > 0) {
                        // we get only the first one since it's impossible to have mutiple organisations
                        _this.priVar.organizationId = _this.userRoleOrganizations[0].organization.id;
                        _this.uiVar.selectedOrganisations = [];
                        _this.uiVar.selectedOrganisations.push(_this.userRoleOrganizations[0].organization);
                    }
                    if (_this.uiVar.allRoles) {
                        _this.uiVar.allRoles.forEach(function (role) {
                            var userRoleOrganization = _this.userRoleOrganizations.find(function (item) { return item.roleId == role.id; });
                            if (userRoleOrganization) {
                                role.userRoleOrganization = userRoleOrganization;
                                role.checked = true;
                            }
                        });
                    }
                }
                else {
                    // it's a creation
                    _this.uiVar.isCreation = true;
                    _this.uiVar.title = "Creation d'un acc\u00E8s";
                }
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            allRoles: undefined,
            organizationMultipleSelect: true,
            selectedOrganisations: [],
            title: "Creation d'un acc\u00E8s",
            isCreation: false,
            loading: false
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: function (organisations) {
                _this.uiVar.selectedOrganisations = organisations;
            },
            userRoleOrganizationSave: function (role) {
                var userRoleOrganization = role.userRoleOrganization;
                // if (userRoleOrganization) {
                //   this.fofPermissionService.userRoleOrganization.delete(userRoleOrganization)
                //   .toPromise()
                //   .then((result: any) => {          
                //     role.userRoleOrganization = null          
                //     this.fofNotificationService.saveIsDone()
                //   })      
                // } else {
                //   // The userRoleOrganization must be created
                //   const userRoleOrganizationToSave: iUserRoleOrganization = {        
                //     roleId: role.id,
                //     userId: this.uiVar.user.id
                //   }
                //   this.fofPermissionService.userRoleOrganization.create(userRoleOrganizationToSave)
                //   .toPromise()
                //   .then((result: any) => {          
                //     role.userRoleOrganization = result          
                //     this.fofNotificationService.saveIsDone()
                //   })      
                // }
            },
            save: function () {
                var userRoleOrganizations = [];
                var userRoleOrganization;
                if (!_this.priVar.userId) {
                    _this.fofNotificationService.error("\n          Probl\u00E8me avec les donn\u00E9es<br>\n          <small>Manque le user</small>\n        ");
                }
                _this.uiVar.selectedOrganisations.forEach(function (organisation) {
                    _this.uiVar.allRoles.forEach(function (role) {
                        if (role.checked) {
                            userRoleOrganization = {
                                userId: _this.priVar.userId,
                                organizationId: organisation.id,
                                roleId: role.id
                            };
                            userRoleOrganizations.push(userRoleOrganization);
                        }
                    });
                });
                if (userRoleOrganizations.length === 0) {
                    _this.fofNotificationService.error("\n          Vous devez s\u00E9lectionner au moins<br>\n          <ul>\n            <li>Une organisation</li>\n            <li>Un role</li>\n          </ul>\n        ", { mustDisappearAfter: -1 });
                    return;
                }
                _this.uiVar.loading = true;
                if (_this.uiVar.isCreation) {
                    _this.fofPermissionService.userRoleOrganization.bulkCreate(userRoleOrganizations)
                        .toPromise()
                        .then(function (result) {
                        _this.matDialogRef.close({ saved: true });
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                        // this.fofNotificationService.error(`
                        //   Erreur lors de la sauvegarde<br>
                        //   <small>${reason}</small>
                        // `)
                    })
                        .finally(function () {
                        _this.uiVar.loading = false;
                    });
                }
                else {
                    _this.fofPermissionService.user
                        .replaceUserRoleOrganization(userRoleOrganizations, _this.priVar.userId, _this.priVar.organizationId)
                        .toPromise()
                        .then(function (result) {
                        _this.matDialogRef.close({ saved: true });
                    })
                        .catch(function (reason) {
                        _this.fofNotificationService.error("\n            Erreur lors de la sauvegarde<br>\n            <small>" + reason + "</small>\n          ");
                    })
                        .finally(function () {
                        _this.uiVar.loading = false;
                    });
                }
            }
        };
        this.userRoleOrganizations = data.userRoleOrganizations;
        this.uiVar.allRoles = data.roles;
        this.priVar.userId = data.userId;
        if (data.notSelectableOrganizations) {
            this.notSelectableOrganizations = data.notSelectableOrganizations;
        }
        if (this.uiVar.allRoles && this.uiVar.allRoles.length) {
            this.uiVar.allRoles.forEach(function (role) {
                role.checked = false;
            });
        }
        this.privFunc.componenentDataRefresh();
    }
    // Angular events
    FofUserRolesSelectComponent.prototype.ngOnInit = function () {
    };
    FofUserRolesSelectComponent.prototype.ngOnChanges = function () {
        this.privFunc.componenentDataRefresh();
    };
    FofUserRolesSelectComponent.ɵfac = function FofUserRolesSelectComponent_Factory(t) { return new (t || FofUserRolesSelectComponent)(i0.ɵɵdirectiveInject(i1.MatDialogRef), i0.ɵɵdirectiveInject(i2.FofPermissionService), i0.ɵɵdirectiveInject(i3.FofNotificationService), i0.ɵɵdirectiveInject(i4.FofErrorService), i0.ɵɵdirectiveInject(MAT_DIALOG_DATA)); };
    FofUserRolesSelectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofUserRolesSelectComponent, selectors: [["fof-user-roles-select"]], inputs: { userRoleOrganizations: "userRoleOrganizations", notSelectableOrganizations: "notSelectableOrganizations" }, features: [i0.ɵɵNgOnChangesFeature], decls: 13, vars: 6, consts: [["class", "fof-loading", 4, "ngIf"], ["mat-dialog-title", ""], [3, "multiSelect", "selectedOrganisations", "notSelectableOrganizations", "selectedOrganizationsChange"], ["class", "fof-fade-in", 4, "ngFor", "ngForOf"], ["mat-flat-button", "", "mat-dialog-close", ""], ["mat-flat-button", "", "color", "primary", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in"], [3, "ngModel", "checked", "change", "ngModelChange"], [1, "role-hint"]], template: function FofUserRolesSelectComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵtemplate(0, FofUserRolesSelectComponent_div_0_Template, 4, 0, "div", 0);
            i0.ɵɵelementStart(1, "h2", 1);
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "mat-dialog-content");
            i0.ɵɵelementStart(4, "fof-core-fof-organizations-multi-select", 2);
            i0.ɵɵlistener("selectedOrganizationsChange", function FofUserRolesSelectComponent_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_4_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "h4");
            i0.ɵɵtext(6, "R\u00F4les");
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(7, FofUserRolesSelectComponent_div_7_Template, 5, 4, "div", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "mat-dialog-actions");
            i0.ɵɵelementStart(9, "button", 4);
            i0.ɵɵtext(10, "Annuler");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(11, "button", 5);
            i0.ɵɵlistener("click", function FofUserRolesSelectComponent_Template_button_click_11_listener() { return ctx.uiAction.save(); });
            i0.ɵɵtext(12, "Enregistrer");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.uiVar.title);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("multiSelect", ctx.uiVar.organizationMultipleSelect)("selectedOrganisations", ctx.uiVar.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("ngForOf", ctx.uiVar.allRoles);
        } }, directives: [i5.NgIf, i1.MatDialogTitle, i1.MatDialogContent, i6.FofOrganizationsMultiSelectComponent, i5.NgForOf, i1.MatDialogActions, i7.MatButton, i1.MatDialogClose, i8.MatSpinner, i9.MatCheckbox, i10.NgControlStatus, i10.NgModel], styles: ["[_nghost-%COMP%]{position:relative}[_nghost-%COMP%]   .fof-loading[_ngcontent-%COMP%]{position:absolute;height:calc(100% + 15px);z-index:2}[_nghost-%COMP%]   .mat-dialog-actions[_ngcontent-%COMP%]{margin-top:1rem;display:flex;justify-content:flex-end}"] });
    return FofUserRolesSelectComponent;
}());
export { FofUserRolesSelectComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofUserRolesSelectComponent, [{
        type: Component,
        args: [{
                selector: 'fof-user-roles-select',
                templateUrl: './fof-user-roles-select.component.html',
                styleUrls: ['./fof-user-roles-select.component.scss']
            }]
    }], function () { return [{ type: i1.MatDialogRef }, { type: i2.FofPermissionService }, { type: i3.FofNotificationService }, { type: i4.FofErrorService }, { type: undefined, decorators: [{
                type: Inject,
                args: [MAT_DIALOG_DATA]
            }] }]; }, { userRoleOrganizations: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXVzZXItcm9sZXMtc2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2FkbWluL2ZvZi11c2VyLXJvbGVzLXNlbGVjdC9mb2YtdXNlci1yb2xlcy1zZWxlY3QuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi11c2VyLXJvbGVzLXNlbGVjdC9mb2YtdXNlci1yb2xlcy1zZWxlY3QuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQWEsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBRzNFLE9BQU8sRUFBRSxlQUFlLEVBQWdCLE1BQU0sMEJBQTBCLENBQUE7Ozs7Ozs7Ozs7Ozs7SUNIeEUsOEJBQ0U7SUFBQSxpQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGtEQUE2QjtJQUFBLGlCQUFPO0lBQ3BGLGlCQUFNOzs7O0lBYUosOEJBQ0k7SUFBQSx1Q0FHMkI7SUFGekIsaU5BQVUscURBQXVDLElBQUMsbU5BQUE7SUFFekIsWUFBZTtJQUFBLGlCQUFlO0lBQ3ZELCtCQUF1QjtJQUFBLFlBQXNCO0lBQUEsaUJBQU07SUFDekQsaUJBQU07OztJQUhBLGVBQTBCO0lBQTFCLDJDQUEwQiw4QkFBQTtJQUNELGVBQWU7SUFBZixvQ0FBZTtJQUNqQixlQUFzQjtJQUF0QiwyQ0FBc0I7O0FEUHJEO0lBU0UscUNBQ1UsWUFBc0QsRUFDdEQsb0JBQTBDLEVBQzFDLHNCQUE4QyxFQUM5QyxlQUFnQyxFQUNmLElBS3hCO1FBVkgsaUJBMkJDO1FBMUJTLGlCQUFZLEdBQVosWUFBWSxDQUEwQztRQUN0RCx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBUGpDLDBCQUFxQixHQUE0QixTQUFTLENBQUE7UUFnQ25FLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixNQUFNLEVBQVUsU0FBUztZQUN6QixjQUFjLEVBQVUsU0FBUztTQUNsQyxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRztZQUNqQixzQkFBc0IsRUFBQztnQkFFckIsSUFBSSxLQUFJLENBQUMscUJBQXFCLEVBQUU7b0JBQzlCLGlCQUFpQjtvQkFDakIsS0FBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFBO29CQUM3QixLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyw2QkFBd0IsQ0FBQTtvQkFDM0MsMkNBQTJDO29CQUMzQyxLQUFJLENBQUMsS0FBSyxDQUFDLDBCQUEwQixHQUFHLEtBQUssQ0FBQTtvQkFDN0MsSUFBSSxLQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDekMsZ0ZBQWdGO3dCQUNoRixLQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQTt3QkFDMUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxFQUFFLENBQUE7d0JBQ3JDLEtBQUksQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUNuQyxLQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUMzQyxDQUFBO3FCQUNGO29CQUVELElBQUksS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7d0JBQ3ZCLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQWE7NEJBQ3hDLElBQU0sb0JBQW9CLEdBQUcsS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFBOzRCQUM1RixJQUFJLG9CQUFvQixFQUFFO2dDQUN4QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsb0JBQW9CLENBQUE7Z0NBQ2hELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBOzZCQUNwQjt3QkFDSCxDQUFDLENBQUMsQ0FBQTtxQkFDSDtpQkFDRjtxQkFBTTtvQkFDTCxrQkFBa0I7b0JBQ2xCLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQTtvQkFDNUIsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsMEJBQXFCLENBQUE7aUJBQ3pDO1lBRUgsQ0FBQztTQUNGLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsUUFBUSxFQUFhLFNBQVM7WUFDOUIsMEJBQTBCLEVBQVcsSUFBSTtZQUN6QyxxQkFBcUIsRUFBbUIsRUFBRTtZQUMxQyxLQUFLLEVBQVUsMEJBQXFCO1lBQ3BDLFVBQVUsRUFBVyxLQUFLO1lBQzFCLE9BQU8sRUFBRSxLQUFLO1NBQ2YsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsK0JBQStCLEVBQUMsVUFBQyxhQUE4QjtnQkFDN0QsS0FBSSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxhQUFhLENBQUE7WUFDbEQsQ0FBQztZQUNELHdCQUF3QixFQUFDLFVBQUMsSUFBYTtnQkFDckMsSUFBTSxvQkFBb0IsR0FBeUIsSUFBSSxDQUFDLG9CQUFvQixDQUFBO2dCQUU1RSw4QkFBOEI7Z0JBQzlCLGdGQUFnRjtnQkFDaEYsaUJBQWlCO2dCQUNqQix1Q0FBdUM7Z0JBQ3ZDLGlEQUFpRDtnQkFDakQsK0NBQStDO2dCQUMvQyxhQUFhO2dCQUNiLFdBQVc7Z0JBQ1gsZ0RBQWdEO2dCQUNoRCx3RUFBd0U7Z0JBQ3hFLHVCQUF1QjtnQkFDdkIsaUNBQWlDO2dCQUNqQyxNQUFNO2dCQUVOLHNGQUFzRjtnQkFDdEYsaUJBQWlCO2dCQUNqQix1Q0FBdUM7Z0JBQ3ZDLG1EQUFtRDtnQkFDbkQsK0NBQStDO2dCQUMvQyxhQUFhO2dCQUNiLElBQUk7WUFDTixDQUFDO1lBQ0QsSUFBSSxFQUFDO2dCQUNILElBQU0scUJBQXFCLEdBQTRCLEVBQUUsQ0FBQTtnQkFDekQsSUFBSSxvQkFBMkMsQ0FBQTtnQkFFL0MsSUFBSSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO29CQUN2QixLQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLHdHQUdqQyxDQUFDLENBQUE7aUJBQ0g7Z0JBRUQsS0FBSSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsVUFBQSxZQUFZO29CQUNuRCxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFhO3dCQUN4QyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7NEJBQ2hCLG9CQUFvQixHQUFHO2dDQUNyQixNQUFNLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNO2dDQUMxQixjQUFjLEVBQUUsWUFBWSxDQUFDLEVBQUU7Z0NBQy9CLE1BQU0sRUFBRSxJQUFJLENBQUMsRUFBRTs2QkFDaEIsQ0FBQTs0QkFDRCxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQTt5QkFDakQ7b0JBQ0gsQ0FBQyxDQUFDLENBQUE7Z0JBQ0osQ0FBQyxDQUFDLENBQUE7Z0JBRUYsSUFBSSxxQkFBcUIsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUN0QyxLQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLHVLQU1qQyxFQUFFLEVBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFBO29CQUU1QixPQUFNO2lCQUNQO2dCQUVELEtBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQTtnQkFFekIsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRTtvQkFDekIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQzt5QkFDL0UsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxVQUFBLE1BQU07d0JBQ1YsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtvQkFDeEMsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxVQUFBLE1BQU07d0JBQ1gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7d0JBQ3hDLHNDQUFzQzt3QkFDdEMscUNBQXFDO3dCQUNyQyw2QkFBNkI7d0JBQzdCLEtBQUs7b0JBQ1AsQ0FBQyxDQUFDO3lCQUNELE9BQU8sQ0FBQzt3QkFDUCxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUE7b0JBQzVCLENBQUMsQ0FBQyxDQUFBO2lCQUNIO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJO3lCQUM3QiwyQkFBMkIsQ0FBQyxxQkFBcUIsRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQzt5QkFDbEcsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxVQUFBLE1BQU07d0JBQ1YsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsRUFBQyxLQUFLLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtvQkFDeEMsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxVQUFBLE1BQU07d0JBQ1gsS0FBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyx3RUFFdkIsTUFBTSx5QkFDaEIsQ0FBQyxDQUFBO29CQUNKLENBQUMsQ0FBQzt5QkFDRCxPQUFPLENBQUM7d0JBQ1AsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO29CQUM1QixDQUFDLENBQUMsQ0FBQTtpQkFDSDtZQUlILENBQUM7U0FDRixDQUFBO1FBNUtDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUE7UUFDdkQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQTtRQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO1FBRWhDLElBQUksSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQ25DLElBQUksQ0FBQywwQkFBMEIsR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUE7U0FDbEU7UUFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRTtZQUNyRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFhO2dCQUN4QyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQTtZQUN0QixDQUFDLENBQUMsQ0FBQTtTQUNIO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxzQkFBc0IsRUFBRSxDQUFBO0lBQ3hDLENBQUM7SUE4SkQsaUJBQWlCO0lBQ2pCLDhDQUFRLEdBQVI7SUFFQSxDQUFDO0lBQ0QsaURBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsc0JBQXNCLEVBQUUsQ0FBQTtJQUN4QyxDQUFDOzBHQW5NVSwyQkFBMkIsd01BUzVCLGVBQWU7b0VBVGQsMkJBQTJCO1lDbEJ4Qyw0RUFDRTtZQUVGLDZCQUFxQjtZQUFBLFlBQWlCO1lBQUEsaUJBQUs7WUFDM0MsMENBRUU7WUFBQSxrRUFLMkM7WUFEekMsMkxBQWlDLG9EQUFnRCxJQUFDO1lBQ25GLGlCQUEwQztZQUUzQywwQkFBSTtZQUFBLDBCQUFLO1lBQUEsaUJBQUs7WUFFZCw0RUFDSTtZQU9OLGlCQUFxQjtZQUVyQiwwQ0FDRTtZQUFBLGlDQUF5QztZQUFBLHdCQUFPO1lBQUEsaUJBQVM7WUFDekQsa0NBRWtCO1lBRGhCLHlHQUFTLG1CQUFlLElBQUM7WUFDVCw0QkFBVztZQUFBLGlCQUFTO1lBRXhDLGlCQUFxQjs7WUEvQmhCLHdDQUFxQjtZQUdMLGVBQWlCO1lBQWpCLHFDQUFpQjtZQUlsQyxlQUFnRDtZQUFoRCxrRUFBZ0QsMERBQUEsOERBQUE7WUFRekIsZUFBbUM7WUFBbkMsNENBQW1DOztzQ0RmOUQ7Q0FzTkMsQUF6TUQsSUF5TUM7U0FwTVksMkJBQTJCO2tEQUEzQiwyQkFBMkI7Y0FMdkMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSx1QkFBdUI7Z0JBQ2pDLFdBQVcsRUFBRSx3Q0FBd0M7Z0JBQ3JELFNBQVMsRUFBRSxDQUFDLHdDQUF3QyxDQUFDO2FBQ3REOztzQkFVSSxNQUFNO3VCQUFDLGVBQWU7O2tCQVJ4QixLQUFLOztrQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPbkNoYW5nZXMsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IGlSb2xlLCBpVXNlclJvbGVPcmdhbml6YXRpb24sIGlPcmdhbml6YXRpb24gfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uU2VydmljZX0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZydcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgRm9mRXJyb3JTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtZXJyb3Iuc2VydmljZSdcclxuXHJcbmludGVyZmFjZSBpUm9sZVVJIGV4dGVuZHMgaVJvbGUge1xyXG4gIGNoZWNrZWQ/OiBib29sZWFuXHJcbiAgdXNlclJvbGVPcmdhbml6YXRpb24/OiBpVXNlclJvbGVPcmdhbml6YXRpb25cclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2YtdXNlci1yb2xlcy1zZWxlY3QnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9mb2YtdXNlci1yb2xlcy1zZWxlY3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZvZi11c2VyLXJvbGVzLXNlbGVjdC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZVc2VyUm9sZXNTZWxlY3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgQElucHV0KCkgdXNlclJvbGVPcmdhbml6YXRpb25zOiBpVXNlclJvbGVPcmdhbml6YXRpb25bXSA9IHVuZGVmaW5lZFxyXG4gIEBJbnB1dCgpIG5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zOiBpT3JnYW5pemF0aW9uW11cclxuXHJcbiAgY29uc3RydWN0b3IgKFxyXG4gICAgcHJpdmF0ZSBtYXREaWFsb2dSZWY6TWF0RGlhbG9nUmVmPEZvZlVzZXJSb2xlc1NlbGVjdENvbXBvbmVudD4sXHJcbiAgICBwcml2YXRlIGZvZlBlcm1pc3Npb25TZXJ2aWNlOiBGb2ZQZXJtaXNzaW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2UsXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgZGF0YToge1xyXG4gICAgICByb2xlczogaVJvbGVbXSxcclxuICAgICAgdXNlclJvbGVPcmdhbml6YXRpb25zOiBpVXNlclJvbGVPcmdhbml6YXRpb25bXSxcclxuICAgICAgdXNlcklkOiBudW1iZXIsXHJcbiAgICAgIG5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zOiBpT3JnYW5pemF0aW9uW11cclxuICAgIH1cclxuICApIHtcclxuICAgIHRoaXMudXNlclJvbGVPcmdhbml6YXRpb25zID0gZGF0YS51c2VyUm9sZU9yZ2FuaXphdGlvbnNcclxuICAgIHRoaXMudWlWYXIuYWxsUm9sZXMgPSBkYXRhLnJvbGVzXHJcbiAgICB0aGlzLnByaVZhci51c2VySWQgPSBkYXRhLnVzZXJJZFxyXG5cclxuICAgIGlmIChkYXRhLm5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zKSB7XHJcbiAgICAgIHRoaXMubm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnMgPSBkYXRhLm5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zIFxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLnVpVmFyLmFsbFJvbGVzICYmIHRoaXMudWlWYXIuYWxsUm9sZXMubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMudWlWYXIuYWxsUm9sZXMuZm9yRWFjaCgocm9sZTogaVJvbGVVSSkgPT4ge1xyXG4gICAgICAgIHJvbGUuY2hlY2tlZCA9IGZhbHNlXHJcbiAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5wcml2RnVuYy5jb21wb25lbmVudERhdGFSZWZyZXNoKClcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgdXNlcklkOiA8bnVtYmVyPnVuZGVmaW5lZCxcclxuICAgIG9yZ2FuaXphdGlvbklkOiA8bnVtYmVyPnVuZGVmaW5lZFxyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gICAgY29tcG9uZW5lbnREYXRhUmVmcmVzaDooKSA9PiB7XHJcblxyXG4gICAgICBpZiAodGhpcy51c2VyUm9sZU9yZ2FuaXphdGlvbnMpIHtcclxuICAgICAgICAvLyBpdCdzIGFuIHVwZGF0ZVxyXG4gICAgICAgIHRoaXMudWlWYXIuaXNDcmVhdGlvbiA9IGZhbHNlXHJcbiAgICAgICAgdGhpcy51aVZhci50aXRsZSA9IGBNb2RpZmljYXRpb24gZGVzIGFjY8Ooc2BcclxuICAgICAgICAvLyBPbmx5IG9uZSBvcmdhbml6YXRpb24gcG9zc2libGUgaW4gdXBkYXRlXHJcbiAgICAgICAgdGhpcy51aVZhci5vcmdhbml6YXRpb25NdWx0aXBsZVNlbGVjdCA9IGZhbHNlXHJcbiAgICAgICAgaWYgKHRoaXMudXNlclJvbGVPcmdhbml6YXRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIC8vIHdlIGdldCBvbmx5IHRoZSBmaXJzdCBvbmUgc2luY2UgaXQncyBpbXBvc3NpYmxlIHRvIGhhdmUgbXV0aXBsZSBvcmdhbmlzYXRpb25zXHJcbiAgICAgICAgICB0aGlzLnByaVZhci5vcmdhbml6YXRpb25JZCA9IHRoaXMudXNlclJvbGVPcmdhbml6YXRpb25zWzBdLm9yZ2FuaXphdGlvbi5pZFxyXG4gICAgICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMgPSBbXVxyXG4gICAgICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMucHVzaChcclxuICAgICAgICAgICAgdGhpcy51c2VyUm9sZU9yZ2FuaXphdGlvbnNbMF0ub3JnYW5pemF0aW9uXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgfVxyXG4gIFxyXG4gICAgICAgIGlmICh0aGlzLnVpVmFyLmFsbFJvbGVzKSB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmFsbFJvbGVzLmZvckVhY2goKHJvbGU6IGlSb2xlVUkpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgdXNlclJvbGVPcmdhbml6YXRpb24gPSB0aGlzLnVzZXJSb2xlT3JnYW5pemF0aW9ucy5maW5kKGl0ZW0gPT4gaXRlbS5yb2xlSWQgPT0gcm9sZS5pZClcclxuICAgICAgICAgICAgaWYgKHVzZXJSb2xlT3JnYW5pemF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgcm9sZS51c2VyUm9sZU9yZ2FuaXphdGlvbiA9IHVzZXJSb2xlT3JnYW5pemF0aW9uXHJcbiAgICAgICAgICAgICAgcm9sZS5jaGVja2VkID0gdHJ1ZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH0gICAgIFxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vIGl0J3MgYSBjcmVhdGlvblxyXG4gICAgICAgIHRoaXMudWlWYXIuaXNDcmVhdGlvbiA9IHRydWVcclxuICAgICAgICB0aGlzLnVpVmFyLnRpdGxlID0gYENyZWF0aW9uIGQndW4gYWNjw6hzYFxyXG4gICAgICB9XHJcbiAgICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgYWxsUm9sZXM6IDxpUm9sZVVJW10+dW5kZWZpbmVkLFxyXG4gICAgb3JnYW5pemF0aW9uTXVsdGlwbGVTZWxlY3Q6IDxib29sZWFuPnRydWUsXHJcbiAgICBzZWxlY3RlZE9yZ2FuaXNhdGlvbnM6IDxpT3JnYW5pemF0aW9uW10+W10sXHJcbiAgICB0aXRsZTogPHN0cmluZz5gQ3JlYXRpb24gZCd1biBhY2PDqHNgLFxyXG4gICAgaXNDcmVhdGlvbjogPGJvb2xlYW4+ZmFsc2UsXHJcbiAgICBsb2FkaW5nOiBmYWxzZVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgICBvcmdhbmlzYXRpb25NdWx0aVNlbGVjdGVkQ2hhbmdlOihvcmdhbmlzYXRpb25zOiBpT3JnYW5pemF0aW9uW10pID0+IHsgICAgICBcclxuICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMgPSBvcmdhbmlzYXRpb25zICAgICAgXHJcbiAgICB9LFxyXG4gICAgdXNlclJvbGVPcmdhbml6YXRpb25TYXZlOihyb2xlOiBpUm9sZVVJKSA9PiB7ICAgICAgXHJcbiAgICAgIGNvbnN0IHVzZXJSb2xlT3JnYW5pemF0aW9uOmlVc2VyUm9sZU9yZ2FuaXphdGlvbiA9IHJvbGUudXNlclJvbGVPcmdhbml6YXRpb25cclxuXHJcbiAgICAgIC8vIGlmICh1c2VyUm9sZU9yZ2FuaXphdGlvbikge1xyXG4gICAgICAvLyAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlclJvbGVPcmdhbml6YXRpb24uZGVsZXRlKHVzZXJSb2xlT3JnYW5pemF0aW9uKVxyXG4gICAgICAvLyAgIC50b1Byb21pc2UoKVxyXG4gICAgICAvLyAgIC50aGVuKChyZXN1bHQ6IGFueSkgPT4geyAgICAgICAgICBcclxuICAgICAgLy8gICAgIHJvbGUudXNlclJvbGVPcmdhbml6YXRpb24gPSBudWxsICAgICAgICAgIFxyXG4gICAgICAvLyAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnNhdmVJc0RvbmUoKVxyXG4gICAgICAvLyAgIH0pICAgICAgXHJcbiAgICAgIC8vIH0gZWxzZSB7XHJcbiAgICAgIC8vICAgLy8gVGhlIHVzZXJSb2xlT3JnYW5pemF0aW9uIG11c3QgYmUgY3JlYXRlZFxyXG4gICAgICAvLyAgIGNvbnN0IHVzZXJSb2xlT3JnYW5pemF0aW9uVG9TYXZlOiBpVXNlclJvbGVPcmdhbml6YXRpb24gPSB7ICAgICAgICBcclxuICAgICAgLy8gICAgIHJvbGVJZDogcm9sZS5pZCxcclxuICAgICAgLy8gICAgIHVzZXJJZDogdGhpcy51aVZhci51c2VyLmlkXHJcbiAgICAgIC8vICAgfVxyXG4gIFxyXG4gICAgICAvLyAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlclJvbGVPcmdhbml6YXRpb24uY3JlYXRlKHVzZXJSb2xlT3JnYW5pemF0aW9uVG9TYXZlKVxyXG4gICAgICAvLyAgIC50b1Byb21pc2UoKVxyXG4gICAgICAvLyAgIC50aGVuKChyZXN1bHQ6IGFueSkgPT4geyAgICAgICAgICBcclxuICAgICAgLy8gICAgIHJvbGUudXNlclJvbGVPcmdhbml6YXRpb24gPSByZXN1bHQgICAgICAgICAgXHJcbiAgICAgIC8vICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc2F2ZUlzRG9uZSgpXHJcbiAgICAgIC8vICAgfSkgICAgICBcclxuICAgICAgLy8gfVxyXG4gICAgfSxcclxuICAgIHNhdmU6KCkgPT4ge1xyXG4gICAgICBjb25zdCB1c2VyUm9sZU9yZ2FuaXphdGlvbnM6IGlVc2VyUm9sZU9yZ2FuaXphdGlvbltdID0gW11cclxuICAgICAgbGV0IHVzZXJSb2xlT3JnYW5pemF0aW9uOiBpVXNlclJvbGVPcmdhbml6YXRpb25cclxuXHJcbiAgICAgIGlmICghdGhpcy5wcmlWYXIudXNlcklkKSB7XHJcbiAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBcclxuICAgICAgICAgIFByb2Jsw6htZSBhdmVjIGxlcyBkb25uw6llczxicj5cclxuICAgICAgICAgIDxzbWFsbD5NYW5xdWUgbGUgdXNlcjwvc21hbGw+XHJcbiAgICAgICAgYClcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy51aVZhci5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMuZm9yRWFjaChvcmdhbmlzYXRpb24gPT4ge1xyXG4gICAgICAgIHRoaXMudWlWYXIuYWxsUm9sZXMuZm9yRWFjaCgocm9sZTogaVJvbGVVSSkgPT4ge1xyXG4gICAgICAgICAgaWYgKHJvbGUuY2hlY2tlZCkge1xyXG4gICAgICAgICAgICB1c2VyUm9sZU9yZ2FuaXphdGlvbiA9IHtcclxuICAgICAgICAgICAgICB1c2VySWQ6IHRoaXMucHJpVmFyLnVzZXJJZCxcclxuICAgICAgICAgICAgICBvcmdhbml6YXRpb25JZDogb3JnYW5pc2F0aW9uLmlkLFxyXG4gICAgICAgICAgICAgIHJvbGVJZDogcm9sZS5pZFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHVzZXJSb2xlT3JnYW5pemF0aW9ucy5wdXNoKHVzZXJSb2xlT3JnYW5pemF0aW9uKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0pXHJcblxyXG4gICAgICBpZiAodXNlclJvbGVPcmdhbml6YXRpb25zLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihgXHJcbiAgICAgICAgICBWb3VzIGRldmV6IHPDqWxlY3Rpb25uZXIgYXUgbW9pbnM8YnI+XHJcbiAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgIDxsaT5VbmUgb3JnYW5pc2F0aW9uPC9saT5cclxuICAgICAgICAgICAgPGxpPlVuIHJvbGU8L2xpPlxyXG4gICAgICAgICAgPC91bD5cclxuICAgICAgICBgLCB7bXVzdERpc2FwcGVhckFmdGVyOiAtMX0pXHJcblxyXG4gICAgICAgIHJldHVyblxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnVpVmFyLmxvYWRpbmcgPSB0cnVlXHJcbiAgICAgIFxyXG4gICAgICBpZiAodGhpcy51aVZhci5pc0NyZWF0aW9uKSB7IFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlclJvbGVPcmdhbml6YXRpb24uYnVsa0NyZWF0ZSh1c2VyUm9sZU9yZ2FuaXphdGlvbnMpXHJcbiAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKHtzYXZlZDogdHJ1ZX0pICAgICAgICAgIFxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgICAvLyB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoYFxyXG4gICAgICAgICAgLy8gICBFcnJldXIgbG9ycyBkZSBsYSBzYXV2ZWdhcmRlPGJyPlxyXG4gICAgICAgICAgLy8gICA8c21hbGw+JHtyZWFzb259PC9zbWFsbD5cclxuICAgICAgICAgIC8vIGApXHJcbiAgICAgICAgfSkgXHJcbiAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2UgeyAgICAgICAgICAgICAgICBcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIgICAgICAgIFxyXG4gICAgICAgIC5yZXBsYWNlVXNlclJvbGVPcmdhbml6YXRpb24odXNlclJvbGVPcmdhbml6YXRpb25zLCB0aGlzLnByaVZhci51c2VySWQsIHRoaXMucHJpVmFyLm9yZ2FuaXphdGlvbklkKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSh7c2F2ZWQ6IHRydWV9KSAgICAgICAgICBcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5jYXRjaChyZWFzb24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBcclxuICAgICAgICAgICAgRXJyZXVyIGxvcnMgZGUgbGEgc2F1dmVnYXJkZTxicj5cclxuICAgICAgICAgICAgPHNtYWxsPiR7cmVhc29ufTwvc21hbGw+XHJcbiAgICAgICAgICBgKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcblxyXG4gICAgICBcclxuICAgICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICBcclxuICB9IFxyXG4gIG5nT25DaGFuZ2VzKCkge1xyXG4gICAgdGhpcy5wcml2RnVuYy5jb21wb25lbmVudERhdGFSZWZyZXNoKCkgICAgXHJcbiAgfSBcclxufVxyXG4iLCI8ZGl2ICpuZ0lmPVwidWlWYXIubG9hZGluZ1wiIGNsYXNzPVwiZm9mLWxvYWRpbmdcIj5cclxuICA8bWF0LXNwaW5uZXIgZGlhbWV0ZXI9MjA+PC9tYXQtc3Bpbm5lcj4gPHNwYW4+UmFmcmFpY2hpc3NlbWVudCBkZXMgcsO0bGVzLi4uPC9zcGFuPlxyXG48L2Rpdj5cclxuPGgyIG1hdC1kaWFsb2ctdGl0bGU+e3sgdWlWYXIudGl0bGUgfX08L2gyPlxyXG48bWF0LWRpYWxvZy1jb250ZW50PiAgXHJcblxyXG4gIDxmb2YtY29yZS1mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3RcclxuICAgIFttdWx0aVNlbGVjdF09XCJ1aVZhci5vcmdhbml6YXRpb25NdWx0aXBsZVNlbGVjdFwiXHJcbiAgICBbc2VsZWN0ZWRPcmdhbmlzYXRpb25zXT1cInVpVmFyLnNlbGVjdGVkT3JnYW5pc2F0aW9uc1wiXHJcbiAgICBbbm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnNdPVwibm90U2VsZWN0YWJsZU9yZ2FuaXphdGlvbnNcIiAgXHJcbiAgICAoc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlKSA9IFwidWlBY3Rpb24ub3JnYW5pc2F0aW9uTXVsdGlTZWxlY3RlZENoYW5nZSgkZXZlbnQpXCJcclxuICA+PC9mb2YtY29yZS1mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3Q+XHJcblxyXG4gIDxoND5Sw7RsZXM8L2g0PlxyXG5cclxuICA8ZGl2IGNsYXNzPVwiZm9mLWZhZGUtaW5cIiAqbmdGb3I9XCJsZXQgcm9sZSBvZiB1aVZhci5hbGxSb2xlc1wiPiAgICAgICAgICAgIFxyXG4gICAgICA8bWF0LWNoZWNrYm94XHJcbiAgICAgICAgKGNoYW5nZSk9XCJ1aUFjdGlvbi51c2VyUm9sZU9yZ2FuaXphdGlvblNhdmUocm9sZSlcIiAgICAgICAgICBcclxuICAgICAgICBbKG5nTW9kZWwpXT1cInJvbGUuY2hlY2tlZFwiXHJcbiAgICAgICAgW2NoZWNrZWRdPVwicm9sZS5jaGVja2VkXCI+e3sgcm9sZS5jb2RlIH19PC9tYXQtY2hlY2tib3g+ICAgICAgICAgICAgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInJvbGUtaGludFwiPnt7IHJvbGUuZGVzY3JpcHRpb24gfX08L2Rpdj4gICAgICAgICAgICBcclxuICA8L2Rpdj5cclxuICBcclxuPC9tYXQtZGlhbG9nLWNvbnRlbnQ+XHJcblxyXG48bWF0LWRpYWxvZy1hY3Rpb25zPlxyXG4gIDxidXR0b24gbWF0LWZsYXQtYnV0dG9uIG1hdC1kaWFsb2ctY2xvc2U+QW5udWxlcjwvYnV0dG9uPiAgICBcclxuICA8YnV0dG9uIG1hdC1mbGF0LWJ1dHRvbiBcclxuICAgIChjbGljayk9XCJ1aUFjdGlvbi5zYXZlKClcIlxyXG4gICAgY29sb3I9XCJwcmltYXJ5XCI+RW5yZWdpc3RyZXI8L2J1dHRvbj4gICAgXHJcbiAgICA8IS0tIFttYXQtZGlhbG9nLWNsb3NlXT1cInRydWVcIiAtLT5cclxuPC9tYXQtZGlhbG9nLWFjdGlvbnM+XHJcblxyXG4iXX0=
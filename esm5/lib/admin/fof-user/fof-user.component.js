import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { fofUtilsForm } from '../../core/fof-utils';
import { FofUserRolesSelectComponent } from '../fof-user-roles-select/fof-user-roles-select.component';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "@angular/router";
import * as i3 from "@angular/forms";
import * as i4 from "../../core/notification/notification.service";
import * as i5 from "../../core/fof-dialog.service";
import * as i6 from "@angular/material/dialog";
import * as i7 from "../../core/fof-error.service";
import * as i8 from "@angular/material/card";
import * as i9 from "@angular/material/button";
import * as i10 from "@angular/common";
import * as i11 from "../fof-entity-footer/fof-entity-footer.component";
import * as i12 from "@angular/material/form-field";
import * as i13 from "@angular/material/input";
import * as i14 from "../../components/fof-organizations-multi-select/fof-organizations-multi-select.component";
import * as i15 from "@angular/material/table";
import * as i16 from "@angular/material/progress-spinner";
import * as i17 from "@angular/material/checkbox";
import * as i18 from "@angular/material/icon";
function FofUserComponent_div_11_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le login ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Un email valide est obligatoire ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le pr\u00E9nom ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_15_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le nom de famille ne doit pas exc\u00E9der 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_div_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 33);
    i0.ɵɵelement(1, "mat-spinner", 34);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Rafraichissement des r\u00F4les...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_th_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "th", 35);
} }
function FofUserComponent_div_11_div_17_td_15_Template(rf, ctx) { if (rf & 1) {
    var _r263 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 36);
    i0.ɵɵelementStart(1, "mat-checkbox", 37);
    i0.ɵɵlistener("ngModelChange", function FofUserComponent_div_11_div_17_td_15_Template_mat_checkbox_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r263); var row_r261 = ctx.$implicit; return row_r261.checked = $event; });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r261 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", row_r261.checked);
} }
function FofUserComponent_div_11_div_17_th_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 35);
    i0.ɵɵtext(1, "Organisation");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_td_18_Template(rf, ctx) { if (rf & 1) {
    var _r266 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 38);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_18_Template_td_click_0_listener() { i0.ɵɵrestoreView(_r266); var row_r264 = ctx.$implicit; var ctx_r265 = i0.ɵɵnextContext(3); return ctx_r265.uiAction.roleSelectComponentOpen(row_r264); });
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r264 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", row_r264.organization, "");
} }
function FofUserComponent_div_11_div_17_th_20_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 35);
    i0.ɵɵtext(1, "Roles");
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_td_21_div_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var role_r269 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", role_r269, " ");
} }
function FofUserComponent_div_11_div_17_td_21_Template(rf, ctx) { if (rf & 1) {
    var _r271 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 38);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_21_Template_td_click_0_listener() { i0.ɵɵrestoreView(_r271); var row_r267 = ctx.$implicit; var ctx_r270 = i0.ɵɵnextContext(3); return ctx_r270.uiAction.roleSelectComponentOpen(row_r267); });
    i0.ɵɵtemplate(1, FofUserComponent_div_11_div_17_td_21_div_1_Template, 2, 1, "div", 39);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r267 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngForOf", row_r267.roles);
} }
function FofUserComponent_div_11_div_17_th_23_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "th", 35);
} }
function FofUserComponent_div_11_div_17_td_24_Template(rf, ctx) { if (rf & 1) {
    var _r274 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "td", 40);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_24_Template_td_click_0_listener() { i0.ɵɵrestoreView(_r274); var row_r272 = ctx.$implicit; var ctx_r273 = i0.ɵɵnextContext(3); return ctx_r273.uiAction.roleSelectComponentOpen(row_r272); });
    i0.ɵɵelementStart(1, "mat-icon");
    i0.ɵɵtext(2, "chevron_right");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_tr_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 41);
} }
function FofUserComponent_div_11_div_17_tr_26_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 42);
} }
function FofUserComponent_div_11_div_17_Template(rf, ctx) { if (rf & 1) {
    var _r277 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 11);
    i0.ɵɵelementStart(1, "mat-card");
    i0.ɵɵelementStart(2, "div", 0);
    i0.ɵɵelementStart(3, "h4");
    i0.ɵɵtext(4, "R\u00F4les");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "div", 1);
    i0.ɵɵelementStart(6, "button", 3);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_Template_button_click_6_listener() { i0.ɵɵrestoreView(_r277); var ctx_r276 = i0.ɵɵnextContext(2); return ctx_r276.uiAction.userRolesDelete(); });
    i0.ɵɵtext(7, "Supprimer r\u00F4les");
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "button", 2);
    i0.ɵɵlistener("click", function FofUserComponent_div_11_div_17_Template_button_click_8_listener() { i0.ɵɵrestoreView(_r277); var ctx_r278 = i0.ɵɵnextContext(2); return ctx_r278.uiAction.roleSelectComponentOpen(); });
    i0.ɵɵtext(9, "Ajouter un r\u00F4le");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "div", 20);
    i0.ɵɵtemplate(11, FofUserComponent_div_11_div_17_div_11_Template, 4, 0, "div", 21);
    i0.ɵɵelementStart(12, "table", 22);
    i0.ɵɵelementContainerStart(13, 23);
    i0.ɵɵtemplate(14, FofUserComponent_div_11_div_17_th_14_Template, 1, 0, "th", 24);
    i0.ɵɵtemplate(15, FofUserComponent_div_11_div_17_td_15_Template, 2, 1, "td", 25);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(16, 26);
    i0.ɵɵtemplate(17, FofUserComponent_div_11_div_17_th_17_Template, 2, 0, "th", 24);
    i0.ɵɵtemplate(18, FofUserComponent_div_11_div_17_td_18_Template, 2, 1, "td", 27);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(19, 28);
    i0.ɵɵtemplate(20, FofUserComponent_div_11_div_17_th_20_Template, 2, 0, "th", 24);
    i0.ɵɵtemplate(21, FofUserComponent_div_11_div_17_td_21_Template, 2, 1, "td", 27);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵelementContainerStart(22, 29);
    i0.ɵɵtemplate(23, FofUserComponent_div_11_div_17_th_23_Template, 1, 0, "th", 24);
    i0.ɵɵtemplate(24, FofUserComponent_div_11_div_17_td_24_Template, 3, 0, "td", 30);
    i0.ɵɵelementContainerEnd();
    i0.ɵɵtemplate(25, FofUserComponent_div_11_div_17_tr_25_Template, 1, 0, "tr", 31);
    i0.ɵɵtemplate(26, FofUserComponent_div_11_div_17_tr_26_Template, 1, 0, "tr", 32);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r249 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(11);
    i0.ɵɵproperty("ngIf", ctx_r249.uiVar.loadingRoles);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("dataSource", ctx_r249.uiVar.userRolesToDisplay);
    i0.ɵɵadvance(13);
    i0.ɵɵproperty("matHeaderRowDef", ctx_r249.uiVar.roleDisplayedColumns);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("matRowDefColumns", ctx_r249.uiVar.roleDisplayedColumns);
} }
function FofUserComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    var _r280 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 9);
    i0.ɵɵelementStart(1, "div", 10);
    i0.ɵɵelementStart(2, "div", 11);
    i0.ɵɵelementStart(3, "form", 12);
    i0.ɵɵelementStart(4, "mat-form-field");
    i0.ɵɵelement(5, "input", 13);
    i0.ɵɵtemplate(6, FofUserComponent_div_11_mat_error_6_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(7, "mat-form-field");
    i0.ɵɵelement(8, "input", 15);
    i0.ɵɵtemplate(9, FofUserComponent_div_11_mat_error_9_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(10, "mat-form-field");
    i0.ɵɵelement(11, "input", 16);
    i0.ɵɵtemplate(12, FofUserComponent_div_11_mat_error_12_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(13, "mat-form-field");
    i0.ɵɵelement(14, "input", 17);
    i0.ɵɵtemplate(15, FofUserComponent_div_11_mat_error_15_Template, 2, 0, "mat-error", 14);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(16, "fof-core-fof-organizations-multi-select", 18);
    i0.ɵɵlistener("selectedOrganizationsChange", function FofUserComponent_div_11_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_16_listener($event) { i0.ɵɵrestoreView(_r280); var ctx_r279 = i0.ɵɵnextContext(); return ctx_r279.uiAction.organisationMultiSelectedChange($event); });
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵtemplate(17, FofUserComponent_div_11_div_17_Template, 27, 4, "div", 19);
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r243 = i0.ɵɵnextContext();
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("formGroup", ctx_r243.uiVar.form);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("login").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("email").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("firstName").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("lastName").invalid);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("placeHolder", "Organisation de rattachement")("multiSelect", false)("selectedOrganisations", ctx_r243.uiVar.userOrganizationsDisplay);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r243.uiVar.allRoles);
} }
function FofUserComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 43);
    i0.ɵɵelement(1, "mat-spinner", 34);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des utilisateurs et des authorisations...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
var FofUserComponent = /** @class */ (function () {
    function FofUserComponent(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, matDialog, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        this.matDialog = matDialog;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            userId: undefined,
            userOrganizationId: undefined,
            organizationAlreadyWithRoles: undefined
        };
        // All private functions
        this.privFunc = {
            userLoad: function () {
                _this.uiVar.loadingUser = true;
                Promise.all([
                    _this.fofPermissionService.role.getAll().toPromise(),
                    _this.fofPermissionService.user.getWithRoleById(_this.priVar.userId).toPromise()
                ])
                    .then(function (result) {
                    var roles = result[0];
                    var currentUsers = result[1];
                    console.log('currentUsers', currentUsers);
                    _this.uiVar.allRoles = roles;
                    _this.privFunc.userRefresh(currentUsers);
                    // Here for preventing to refresh the user form when refreshing only the roles.
                    // toDo: improve
                    _this.uiVar.form.patchValue(_this.uiVar.user);
                })
                    .catch(function (reason) {
                    _this.fofErrorService.errorManage(reason);
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                })
                    .finally(function () {
                    _this.uiVar.loadingUser = false;
                });
            },
            userRefresh: function (currentUser) {
                if (!currentUser) {
                    _this.fofNotificationService.error("Ce utilisateur n'existe pas");
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                    return;
                }
                _this.uiVar.user = currentUser;
                if (currentUser.organizationId) {
                    _this.uiVar.userOrganizationsDisplay = [{ id: currentUser.organizationId }];
                    _this.priVar.userOrganizationId = currentUser.organizationId;
                }
                var _previousOrganisationId = undefined;
                var _roleToDisplay = undefined;
                _this.uiVar.userRolesToDisplay = [];
                if (currentUser.userRoleOrganizations && currentUser.userRoleOrganizations.length > 0) {
                    // we will display the user roles grouped by organization 
                    // with one on several roles on it
                    currentUser.userRoleOrganizations.forEach(function (uro) {
                        if (_previousOrganisationId !== uro.organization.id) {
                            // it's a new organisation, reinit
                            _previousOrganisationId = uro.organization.id;
                            if (_roleToDisplay) {
                                _this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                            }
                            _roleToDisplay = {
                                organizationId: uro.organization.id,
                                organization: uro.organization.name,
                                roles: [],
                                userRoleOrganizations: []
                            };
                        }
                        _roleToDisplay.roles.push(uro.role.code);
                        _roleToDisplay.userRoleOrganizations.push(uro);
                    });
                }
                // add the last one
                if (_roleToDisplay) {
                    _this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                }
                // we don't want the following organization could be selectebale in the 
                // organization tree
                _this.priVar.organizationAlreadyWithRoles = [];
                _this.uiVar.userRolesToDisplay.forEach(function (org) {
                    _this.priVar.organizationAlreadyWithRoles.push({
                        id: org.organizationId,
                        name: org.organization
                    });
                });
            },
            useRolesRefresh: function () {
                _this.uiVar.loadingRoles = true;
                _this.fofPermissionService.user.getWithRoleById(_this.priVar.userId)
                    .toPromise()
                    .then(function (usersResult) {
                    _this.privFunc.userRefresh(usersResult);
                })
                    .catch(function (reason) {
                    _this.fofErrorService.errorManage(reason);
                    // this.router.navigate(['../'], {relativeTo: this.activatedRoute})
                })
                    .finally(function () {
                    _this.uiVar.loadingRoles = false;
                });
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouvel utilisateur',
            loadingUser: false,
            loadingRoles: false,
            userIsNew: false,
            user: undefined,
            userOrganizationsDisplay: undefined,
            form: this.formBuilder.group({
                login: ['', [Validators.maxLength(30)]],
                email: ['', [Validators.required, Validators.email, Validators.maxLength(60)]],
                firstName: ['', [Validators.maxLength(30)]],
                lastName: ['', [Validators.maxLength(30)]]
            }),
            allRoles: undefined,
            selectedUserRoleOrganizations: undefined,
            userRolesToDisplay: [],
            roleDisplayedColumns: ['delete', 'organization', 'roles', 'icon'],
            rolesAll: undefined
        };
        // All actions shared with UI 
        this.uiAction = {
            userSave: function () {
                var userToSave = _this.uiVar.form.value;
                userToSave.organizationId = _this.priVar.userOrganizationId;
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                if (_this.uiVar.userIsNew) {
                    _this.fofPermissionService.user.create(userToSave)
                        .toPromise()
                        .then(function (newUser) {
                        _this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                        _this.priVar.userId = newUser.id;
                        _this.uiVar.title = "Modification d'un utilisateur";
                        _this.uiVar.userIsNew = false;
                        _this.privFunc.userLoad();
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    userToSave.id = _this.uiVar.user.id;
                    _this.fofPermissionService.user.update(userToSave)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
            },
            userCancel: function () {
                _this.privFunc.userLoad();
            },
            userDelete: function () {
                _this.fofDialogService.openYesNo({
                    question: "Voulez vous vraiment supprimer l' utilisateur ?"
                }).then(function (yes) {
                    if (yes) {
                        _this.fofPermissionService.user.delete(_this.uiVar.user)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Utilisateur supprimé');
                            _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        });
                    }
                });
            },
            userRolesDelete: function () {
                var userRoles = _this.uiVar.userRolesToDisplay;
                var organizationsIdToDelete = [];
                var somethingToDelete = false;
                var message = "Voulez vous vraiment supprimer les r\u00F4les des organisations suivante ?<br>";
                message = message + '<ul>';
                userRoles.forEach(function (ur) {
                    //don't want to create an interface for 1 param)
                    if (ur['checked']) {
                        somethingToDelete = true;
                        message = message + ("<li>" + ur.organization + "</li>");
                        organizationsIdToDelete.push(ur.organizationId);
                    }
                });
                message = message + '</ul>';
                if (somethingToDelete) {
                    _this.fofDialogService.openYesNo({
                        title: 'Supprimer rôles',
                        question: message
                    }).then(function (yes) {
                        if (yes) {
                            _this.fofPermissionService.user.deleteUserRoleOrganizations(_this.uiVar.user.id, organizationsIdToDelete)
                                .toPromise()
                                .then(function (result) {
                                _this.fofNotificationService.success('Rôles supprimés');
                                _this.privFunc.useRolesRefresh();
                            });
                        }
                    });
                }
                else {
                    _this.fofNotificationService.info('Vous devez sélectionner ua moins une organisation');
                }
            },
            roleSelectComponentOpen: function (userRole) {
                var userRoleOrganizations;
                if (userRole) {
                    userRoleOrganizations = userRole.userRoleOrganizations;
                }
                var dialogRef = _this.matDialog.open(FofUserRolesSelectComponent, {
                    data: {
                        roles: _this.uiVar.allRoles,
                        userRoleOrganizations: userRoleOrganizations,
                        notSelectableOrganizations: _this.priVar.organizationAlreadyWithRoles,
                        userId: _this.uiVar.user.id
                    },
                    width: '600px',
                    height: 'calc(100vh - 200px)'
                });
                dialogRef.afterClosed()
                    .toPromise()
                    .then(function (mustBeRefresd) {
                    if (mustBeRefresd) {
                        _this.privFunc.useRolesRefresh();
                    }
                });
            },
            organisationMultiSelectedChange: function (organizations) {
                if (organizations && organizations.length > 0) {
                    _this.priVar.userOrganizationId = organizations[0].id;
                }
                else {
                    _this.priVar.userOrganizationId = null;
                }
            }
        };
    }
    // Angular events
    FofUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.paramMap.subscribe(function (params) {
            var code = params.get('code');
            if (code) {
                if (code.toLowerCase() == 'new') {
                    _this.uiVar.userIsNew = true;
                }
                else {
                    _this.priVar.userId = code;
                    _this.uiVar.title = "Modification d'un utilisateur";
                    _this.privFunc.userLoad();
                }
            }
        });
    };
    FofUserComponent.ɵfac = function FofUserComponent_Factory(t) { return new (t || FofUserComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i3.FormBuilder), i0.ɵɵdirectiveInject(i4.FofNotificationService), i0.ɵɵdirectiveInject(i5.FofDialogService), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i6.MatDialog), i0.ɵɵdirectiveInject(i7.FofErrorService)); };
    FofUserComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofUserComponent, selectors: [["fof-core-fof-user"]], decls: 14, vars: 4, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "detail fof-fade-in", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [3, "entityBase"], [1, "detail", "fof-fade-in"], [1, "row"], [1, "col-md-6"], [3, "formGroup"], ["matInput", "", "formControlName", "login", "placeholder", "login", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "type", "email", "formControlName", "email", "placeholder", "Email", "value", ""], ["matInput", "", "required", "", "type", "firstName", "formControlName", "firstName", "placeholder", "Pr\u00E9nom", "value", ""], ["matInput", "", "required", "", "type", "lastName", "formControlName", "lastName", "placeholder", "Nom de famille", "value", ""], [3, "placeHolder", "multiSelect", "selectedOrganisations", "selectedOrganizationsChange"], ["class", "col-md-6", 4, "ngIf"], [1, "fof-table-container"], ["class", "fof-loading fof-loading-roles", 4, "ngIf"], ["mat-table", "", 1, "data-table", 3, "dataSource"], ["matColumnDef", "delete"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "organization"], ["mat-cell", "", 3, "click", 4, "matCellDef"], ["matColumnDef", "roles"], ["matColumnDef", "icon"], ["class", "icon-select", "mat-cell", "", 3, "click", 4, "matCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 4, "matRowDef", "matRowDefColumns"], [1, "fof-loading", "fof-loading-roles"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], [3, "ngModel", "ngModelChange"], ["mat-cell", "", 3, "click"], [4, "ngFor", "ngForOf"], ["mat-cell", "", 1, "icon-select", 3, "click"], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over"], [1, "fof-loading"]], template: function FofUserComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "mat-card", 0);
            i0.ɵɵelementStart(1, "h3");
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "div", 1);
            i0.ɵɵelementStart(4, "button", 2);
            i0.ɵɵlistener("click", function FofUserComponent_Template_button_click_4_listener() { return ctx.uiAction.userCancel(); });
            i0.ɵɵtext(5, "Annuler");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "button", 3);
            i0.ɵɵlistener("click", function FofUserComponent_Template_button_click_6_listener() { return ctx.uiAction.userDelete(); });
            i0.ɵɵtext(7, "Supprimer");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "button", 4);
            i0.ɵɵlistener("click", function FofUserComponent_Template_button_click_8_listener() { return ctx.uiAction.userSave(); });
            i0.ɵɵtext(9, " Enregister");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(10, "div", 5);
            i0.ɵɵtemplate(11, FofUserComponent_div_11_Template, 18, 9, "div", 6);
            i0.ɵɵtemplate(12, FofUserComponent_div_12_Template, 4, 0, "div", 7);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(13, "fof-entity-footer", 8);
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.uiVar.title);
            i0.ɵɵadvance(9);
            i0.ɵɵproperty("ngIf", !ctx.uiVar.loadingUser);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.uiVar.loadingUser);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("entityBase", ctx.uiVar.user);
        } }, directives: [i8.MatCard, i9.MatButton, i10.NgIf, i11.FofEntityFooterComponent, i3.ɵangular_packages_forms_forms_y, i3.NgControlStatusGroup, i3.FormGroupDirective, i12.MatFormField, i13.MatInput, i3.DefaultValueAccessor, i3.NgControlStatus, i3.FormControlName, i3.RequiredValidator, i14.FofOrganizationsMultiSelectComponent, i12.MatError, i15.MatTable, i15.MatColumnDef, i15.MatHeaderCellDef, i15.MatCellDef, i15.MatHeaderRowDef, i15.MatRowDef, i16.MatSpinner, i15.MatHeaderCell, i15.MatCell, i17.MatCheckbox, i3.NgModel, i10.NgForOf, i18.MatIcon, i15.MatHeaderRow, i15.MatRow], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row;margin-bottom:15px}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .fof-loading-roles[_ngcontent-%COMP%]{position:absolute}.main[_ngcontent-%COMP%]   .role-hint[_ngcontent-%COMP%]{padding-left:25px;font-size:smaller}.main[_ngcontent-%COMP%]   .icon-select[_ngcontent-%COMP%]{text-align:right}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
    return FofUserComponent;
}());
export { FofUserComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofUserComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-user',
                templateUrl: './fof-user.component.html',
                styleUrls: ['./fof-user.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.ActivatedRoute }, { type: i3.FormBuilder }, { type: i4.FofNotificationService }, { type: i5.FofDialogService }, { type: i2.Router }, { type: i6.MatDialog }, { type: i7.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXVzZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvYWRtaW4vZm9mLXVzZXIvZm9mLXVzZXIuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi11c2VyL2ZvZi11c2VyLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQW1DLE1BQU0sZUFBZSxDQUFBO0FBTTFFLE9BQU8sRUFBMEIsVUFBVSxFQUFHLE1BQU0sZ0JBQWdCLENBQUE7QUFHcEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHNCQUFzQixDQUFBO0FBRW5ELE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDBEQUEwRCxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNhMUYsaUNBQ0U7SUFBQSxzRUFDRjtJQUFBLGlCQUFZOzs7SUFRWixpQ0FDRTtJQUFBLGlEQUNGO0lBQUEsaUJBQVk7OztJQVFaLGlDQUNFO0lBQUEsNEVBQ0Y7SUFBQSxpQkFBWTs7O0lBUVosaUNBQ0U7SUFBQSwrRUFDRjtJQUFBLGlCQUFZOzs7SUE0QlosK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGtEQUE2QjtJQUFBLGlCQUFPO0lBQ3BGLGlCQUFNOzs7SUFLRix5QkFBMkM7Ozs7SUFDM0MsOEJBQ0U7SUFBQSx3Q0FFZTtJQURiLGlPQUF5QjtJQUMzQixpQkFBZTtJQUNqQixpQkFBSzs7O0lBRkQsZUFBeUI7SUFBekIsMENBQXlCOzs7SUFNN0IsOEJBQXNDO0lBQUEsNEJBQVk7SUFBQSxpQkFBSzs7OztJQUN2RCw4QkFFQTtJQURBLHdNQUFTLG1EQUFxQyxJQUFDO0lBQy9DLFlBQW9CO0lBQUEsaUJBQUs7OztJQUF6QixlQUFvQjtJQUFwQixxREFBb0I7OztJQUlwQiw4QkFBc0M7SUFBQSxxQkFBSztJQUFBLGlCQUFLOzs7SUFHOUMsMkJBQ0U7SUFBQSxZQUNGO0lBQUEsaUJBQU07OztJQURKLGVBQ0Y7SUFERSwwQ0FDRjs7OztJQUpGLDhCQUVFO0lBREEsd01BQVMsbURBQXFDLElBQUM7SUFDL0Msc0ZBQ0U7SUFFSixpQkFBSzs7O0lBSEUsZUFBOEI7SUFBOUIsd0NBQThCOzs7SUFPckMseUJBQTJDOzs7O0lBQzNDLDhCQUVFO0lBREEsd01BQVMsbURBQXFDLElBQUM7SUFDL0MsZ0NBQVU7SUFBQSw2QkFBYTtJQUFBLGlCQUFXO0lBQ3BDLGlCQUFLOzs7SUFHUCx5QkFBc0U7OztJQUN0RSx5QkFDa0U7Ozs7SUF4RDFFLCtCQUVFO0lBQUEsZ0NBQ0U7SUFBQSw4QkFDRTtJQUFBLDBCQUFJO0lBQUEsMEJBQUs7SUFBQSxpQkFBSztJQUNkLDhCQUNFO0lBQUEsaUNBQ3VDO0lBQXJDLHdLQUFTLG1DQUEwQixJQUFDO0lBQUMsb0NBQWU7SUFBQSxpQkFBUztJQUMvRCxpQ0FDK0M7SUFBN0Msd0tBQVMsMkNBQWtDLElBQUM7SUFBQyxvQ0FBZTtJQUFBLGlCQUFTO0lBQ3pFLGlCQUFNO0lBQ1IsaUJBQU07SUFFTixnQ0FDRTtJQUFBLGtGQUNFO0lBR0Ysa0NBRUU7SUFBQSxrQ0FDRTtJQUFBLGdGQUFzQztJQUN0QyxnRkFDRTtJQUlKLDBCQUFlO0lBRWYsa0NBQ0U7SUFBQSxnRkFBc0M7SUFDdEMsZ0ZBRUE7SUFDRiwwQkFBZTtJQUVmLGtDQUNFO0lBQUEsZ0ZBQXNDO0lBQ3RDLGdGQUVFO0lBSUosMEJBQWU7SUFFZixrQ0FDRTtJQUFBLGdGQUFzQztJQUN0QyxnRkFFRTtJQUVKLDBCQUFlO0lBRWYsZ0ZBQWlFO0lBQ2pFLGdGQUM2RDtJQUUvRCxpQkFBUTtJQUVWLGlCQUFNO0lBQ1IsaUJBQVc7SUFFYixpQkFBTTs7O0lBakRLLGdCQUEwQjtJQUExQixrREFBMEI7SUFJZCxlQUF1QztJQUF2Qyw4REFBdUM7SUFvQ25DLGdCQUE2QztJQUE3QyxxRUFBNkM7SUFFOUQsZUFBMEQ7SUFBMUQsc0VBQTBEOzs7O0lBakh4RSw4QkFFRTtJQUFBLCtCQUNFO0lBQUEsK0JBRUU7SUFBQSxnQ0FFRTtJQUFBLHNDQUNFO0lBQUEsNEJBR0E7SUFBQSxxRkFDRTtJQUVKLGlCQUFpQjtJQUVqQixzQ0FDRTtJQUFBLDRCQUlBO0lBQUEscUZBQ0U7SUFFSixpQkFBaUI7SUFFakIsdUNBQ0U7SUFBQSw2QkFJQTtJQUFBLHVGQUNFO0lBRUosaUJBQWlCO0lBRWpCLHVDQUNFO0lBQUEsNkJBSUE7SUFBQSx1RkFDRTtJQUVKLGlCQUFpQjtJQUVqQixvRUFLMkM7SUFEekMsb1BBQWlDLHlEQUFnRCxJQUFDO0lBQ25GLGlCQUEwQztJQUU3QyxpQkFBTztJQUVULGlCQUFNO0lBRU4sNEVBRUU7SUE4REosaUJBQU07SUFDUixpQkFBTTs7O0lBckhNLGVBQXdCO0lBQXhCLCtDQUF3QjtJQU1mLGVBQXVDO0lBQXZDLCtEQUF1QztJQVV2QyxlQUF1QztJQUF2QywrREFBdUM7SUFVdkMsZUFBMkM7SUFBM0MsbUVBQTJDO0lBVTNDLGVBQTBDO0lBQTFDLGtFQUEwQztJQU1yRCxlQUE4QztJQUE5Qyw0REFBOEMsc0JBQUEsa0VBQUE7SUFVOUIsZUFBc0I7SUFBdEIsOENBQXNCOzs7SUFrRWhELCtCQUNFO0lBQUEsa0NBQXVDO0lBQUMsNEJBQU07SUFBQSxxRUFBcUQ7SUFBQSxpQkFBTztJQUM1RyxpQkFBTTs7QURoSFI7SUFNRSwwQkFDVSxvQkFBMEMsRUFDMUMsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsc0JBQThDLEVBQzlDLGdCQUFrQyxFQUNsQyxNQUFjLEVBQ2QsU0FBb0IsRUFDcEIsZUFBZ0M7UUFSMUMsaUJBVUM7UUFUUyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDcEIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBSTFDLHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixNQUFNLEVBQVUsU0FBUztZQUN6QixrQkFBa0IsRUFBVSxTQUFTO1lBQ3JDLDRCQUE0QixFQUFtQixTQUFTO1NBQ3pELENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHO1lBQ2pCLFFBQVEsRUFBQztnQkFDUCxLQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUE7Z0JBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUM7b0JBQ1YsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUU7b0JBQ25ELEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxFQUFFO2lCQUMvRSxDQUFDO3FCQUNELElBQUksQ0FBQyxVQUFBLE1BQU07b0JBQ1YsSUFBTSxLQUFLLEdBQVksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO29CQUNoQyxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUE7b0JBRTlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFlBQVksQ0FBQyxDQUFBO29CQUV6QyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7b0JBQzNCLEtBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFBO29CQUN2QywrRUFBK0U7b0JBQy9FLGdCQUFnQjtvQkFDaEIsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7Z0JBQzdDLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsVUFBQSxNQUFNO29CQUNYLEtBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUN4QyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUMsVUFBVSxFQUFFLEtBQUksQ0FBQyxjQUFjLEVBQUMsQ0FBQyxDQUFBO2dCQUNsRSxDQUFDLENBQUM7cUJBQ0QsT0FBTyxDQUFDO29CQUNQLEtBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQTtnQkFDaEMsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO1lBQ0QsV0FBVyxFQUFDLFVBQUMsV0FBa0I7Z0JBRTdCLElBQUksQ0FBQyxXQUFXLEVBQUU7b0JBQ2hCLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQTtvQkFDaEUsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQTtvQkFDaEUsT0FBTTtpQkFDUDtnQkFFRCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxXQUFXLENBQUE7Z0JBRTdCLElBQUksV0FBVyxDQUFDLGNBQWMsRUFBRTtvQkFDOUIsS0FBSSxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsR0FBRyxDQUFDLEVBQUMsRUFBRSxFQUFFLFdBQVcsQ0FBQyxjQUFjLEVBQUMsQ0FBQyxDQUFBO29CQUN4RSxLQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLFdBQVcsQ0FBQyxjQUFjLENBQUE7aUJBQzVEO2dCQUVELElBQUksdUJBQXVCLEdBQVcsU0FBUyxDQUFBO2dCQUMvQyxJQUFJLGNBQWMsR0FBdUIsU0FBUyxDQUFBO2dCQUVsRCxLQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQTtnQkFFbEMsSUFBSSxXQUFXLENBQUMscUJBQXFCLElBQUksV0FBVyxDQUFDLHFCQUFxQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3JGLDBEQUEwRDtvQkFDMUQsa0NBQWtDO29CQUNsQyxXQUFXLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRzt3QkFDM0MsSUFBSSx1QkFBdUIsS0FBSyxHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsRUFBRTs0QkFDbkQsa0NBQWtDOzRCQUNsQyx1QkFBdUIsR0FBRyxHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQTs0QkFDN0MsSUFBSSxjQUFjLEVBQUU7Z0NBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7NkJBQUU7NEJBQzFFLGNBQWMsR0FBRztnQ0FDZixjQUFjLEVBQUUsR0FBRyxDQUFDLFlBQVksQ0FBQyxFQUFFO2dDQUNuQyxZQUFZLEVBQUUsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJO2dDQUNuQyxLQUFLLEVBQUUsRUFBRTtnQ0FDVCxxQkFBcUIsRUFBRSxFQUFFOzZCQUMxQixDQUFBO3lCQUNGO3dCQUNELGNBQWMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7d0JBQ3hDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7b0JBQ2hELENBQUMsQ0FBQyxDQUFBO2lCQUNIO2dCQUVELG1CQUFtQjtnQkFDbkIsSUFBSSxjQUFjLEVBQUU7b0JBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUE7aUJBQUU7Z0JBRTFFLHdFQUF3RTtnQkFDeEUsb0JBQW9CO2dCQUNwQixLQUFJLENBQUMsTUFBTSxDQUFDLDRCQUE0QixHQUFHLEVBQUUsQ0FBQTtnQkFDN0MsS0FBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHO29CQUN2QyxLQUFJLENBQUMsTUFBTSxDQUFDLDRCQUE0QixDQUFDLElBQUksQ0FBQzt3QkFDNUMsRUFBRSxFQUFFLEdBQUcsQ0FBQyxjQUFjO3dCQUN0QixJQUFJLEVBQUUsR0FBRyxDQUFDLFlBQVk7cUJBQ3ZCLENBQUMsQ0FBQTtnQkFDSixDQUFDLENBQUMsQ0FBQTtZQUVKLENBQUM7WUFDRCxlQUFlLEVBQUM7Z0JBQ2QsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFBO2dCQUM5QixLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztxQkFDakUsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxVQUFBLFdBQVc7b0JBQ2YsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUE7Z0JBQ3hDLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsVUFBQSxNQUFNO29CQUNYLEtBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUN4QyxtRUFBbUU7Z0JBQ3JFLENBQUMsQ0FBQztxQkFDRCxPQUFPLENBQUM7b0JBQ1AsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFBO2dCQUNqQyxDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUM7U0FDRixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLEtBQUssRUFBRSxvQkFBb0I7WUFDM0IsV0FBVyxFQUFFLEtBQUs7WUFDbEIsWUFBWSxFQUFFLEtBQUs7WUFDbkIsU0FBUyxFQUFFLEtBQUs7WUFDaEIsSUFBSSxFQUFTLFNBQVM7WUFDdEIsd0JBQXdCLEVBQW1CLFNBQVM7WUFDcEQsSUFBSSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUMzQixLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZDLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzlFLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDM0MsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzNDLENBQUM7WUFDRixRQUFRLEVBQWEsU0FBUztZQUM5Qiw2QkFBNkIsRUFBMkIsU0FBUztZQUNqRSxrQkFBa0IsRUFBd0IsRUFBRTtZQUM1QyxvQkFBb0IsRUFBRSxDQUFDLFFBQVEsRUFBRSxjQUFjLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQztZQUNqRSxRQUFRLEVBQVcsU0FBUztTQUM3QixDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRztZQUNoQixRQUFRLEVBQUM7Z0JBQ1AsSUFBTSxVQUFVLEdBQVUsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBO2dCQUMvQyxVQUFVLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLENBQUE7Z0JBRTFELElBQUksQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7b0JBQzFCLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsb0RBQW9ELENBQUMsQ0FBQTtvQkFDdkYsWUFBWSxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUE7b0JBQy9DLE9BQU07aUJBQ1A7Z0JBRUQsSUFBSSxLQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsRUFBRTtvQkFDeEIsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO3lCQUNoRCxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLFVBQUMsT0FBYzt3QkFDbkIsS0FBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFDLGtCQUFrQixFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7d0JBQ3BGLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUE7d0JBQy9CLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLCtCQUErQixDQUFBO3dCQUNsRCxLQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7d0JBQzVCLEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUE7b0JBQzFCLENBQUMsQ0FBQzt5QkFDRCxLQUFLLENBQUMsVUFBQSxNQUFNO3dCQUNYLEtBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFBO29CQUMxQyxDQUFDLENBQUMsQ0FBQTtpQkFDSDtxQkFBTTtvQkFDTCxVQUFVLENBQUMsRUFBRSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQTtvQkFFbEMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDO3lCQUNoRCxTQUFTLEVBQUU7eUJBQ1gsSUFBSSxDQUFDLFVBQUEsTUFBTTt3QkFDVixLQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLG1CQUFtQixFQUFFLEVBQUMsa0JBQWtCLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtvQkFDdEYsQ0FBQyxDQUFDO3lCQUNELEtBQUssQ0FBQyxVQUFBLE1BQU07d0JBQ1gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQzFDLENBQUMsQ0FBQyxDQUFBO2lCQUNIO1lBQ0gsQ0FBQztZQUNELFVBQVUsRUFBQztnQkFDVCxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFBO1lBQzFCLENBQUM7WUFDRCxVQUFVLEVBQUM7Z0JBQ1QsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztvQkFDOUIsUUFBUSxFQUFFLGlEQUFpRDtpQkFDNUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7b0JBQ1QsSUFBSSxHQUFHLEVBQUU7d0JBQ1AsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7NkJBQ3JELFNBQVMsRUFBRTs2QkFDWCxJQUFJLENBQUMsVUFBQSxNQUFNOzRCQUNWLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQTs0QkFDM0QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQTt3QkFDbEUsQ0FBQyxDQUFDLENBQUE7cUJBQ0g7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO1lBQ0QsZUFBZSxFQUFDO2dCQUNkLElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUE7Z0JBQy9DLElBQU0sdUJBQXVCLEdBQWtCLEVBQUUsQ0FBQTtnQkFDakQsSUFBSSxpQkFBaUIsR0FBRyxLQUFLLENBQUE7Z0JBQzdCLElBQUksT0FBTyxHQUFHLGdGQUEyRSxDQUFBO2dCQUN6RixPQUFPLEdBQUcsT0FBTyxHQUFHLE1BQU0sQ0FBQTtnQkFFMUIsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEVBQUU7b0JBQ2xCLGdEQUFnRDtvQkFDaEQsSUFBSSxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUU7d0JBQ2pCLGlCQUFpQixHQUFHLElBQUksQ0FBQTt3QkFDeEIsT0FBTyxHQUFHLE9BQU8sSUFBRyxTQUFPLEVBQUUsQ0FBQyxZQUFZLFVBQU8sQ0FBQSxDQUFBO3dCQUNqRCx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxDQUFBO3FCQUNoRDtnQkFDSCxDQUFDLENBQUMsQ0FBQTtnQkFFRixPQUFPLEdBQUcsT0FBTyxHQUFHLE9BQU8sQ0FBQTtnQkFHM0IsSUFBSSxpQkFBaUIsRUFBRTtvQkFDckIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQzt3QkFDOUIsS0FBSyxFQUFFLGlCQUFpQjt3QkFDeEIsUUFBUSxFQUFFLE9BQU87cUJBQ2xCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO3dCQUNULElBQUksR0FBRyxFQUFFOzRCQUNQLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLHVCQUF1QixDQUFDO2lDQUN0RyxTQUFTLEVBQUU7aUNBQ1gsSUFBSSxDQUFDLFVBQUEsTUFBTTtnQ0FDVixLQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUE7Z0NBQ3RELEtBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxFQUFFLENBQUE7NEJBQ2pDLENBQUMsQ0FBQyxDQUFBO3lCQUNIO29CQUNILENBQUMsQ0FBQyxDQUFBO2lCQUNIO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsbURBQW1ELENBQUMsQ0FBQTtpQkFDdEY7WUFDSCxDQUFDO1lBQ0QsdUJBQXVCLEVBQUMsVUFBQyxRQUE2QjtnQkFDcEQsSUFBSSxxQkFBOEMsQ0FBQTtnQkFFbEQsSUFBSSxRQUFRLEVBQUU7b0JBQ1oscUJBQXFCLEdBQUcsUUFBUSxDQUFDLHFCQUFxQixDQUFBO2lCQUN2RDtnQkFFRCxJQUFNLFNBQVMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQywyQkFBMkIsRUFBRTtvQkFDakUsSUFBSSxFQUFFO3dCQUNKLEtBQUssRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVE7d0JBQzFCLHFCQUFxQixFQUFFLHFCQUFxQjt3QkFDNUMsMEJBQTBCLEVBQUUsS0FBSSxDQUFDLE1BQU0sQ0FBQyw0QkFBNEI7d0JBQ3BFLE1BQU0sRUFBRSxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO3FCQUMzQjtvQkFDRCxLQUFLLEVBQUUsT0FBTztvQkFDZCxNQUFNLEVBQUUscUJBQXFCO2lCQUM5QixDQUFDLENBQUE7Z0JBQ0YsU0FBUyxDQUFDLFdBQVcsRUFBRTtxQkFDdEIsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxVQUFBLGFBQWE7b0JBQ2pCLElBQUksYUFBYSxFQUFFO3dCQUNqQixLQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFBO3FCQUNoQztnQkFDSCxDQUFDLENBQUMsQ0FBQTtZQUNKLENBQUM7WUFDRCwrQkFBK0IsRUFBQyxVQUFDLGFBQThCO2dCQUM3RCxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDN0MsS0FBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFBO2lCQUNyRDtxQkFBTTtvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQTtpQkFDdEM7WUFDSCxDQUFDO1NBQ0YsQ0FBQTtJQTFQRCxDQUFDO0lBMlBELGlCQUFpQjtJQUNqQixtQ0FBUSxHQUFSO1FBQUEsaUJBY0M7UUFiQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQzNDLElBQU0sSUFBSSxHQUFPLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7WUFFbkMsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksS0FBSyxFQUFFO29CQUMvQixLQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUE7aUJBQzVCO3FCQUFNO29CQUNMLEtBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQTtvQkFDekIsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsK0JBQStCLENBQUE7b0JBQ2xELEtBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLENBQUE7aUJBQ3pCO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7b0ZBclJVLGdCQUFnQjt5REFBaEIsZ0JBQWdCO1lDL0I3QixtQ0FDRTtZQUFBLDBCQUFJO1lBQUEsWUFBaUI7WUFBQSxpQkFBSztZQUMxQiw4QkFDRTtZQUFBLGlDQUNrQztZQUFoQyw2RkFBUyx5QkFBcUIsSUFBQztZQUFDLHVCQUFPO1lBQUEsaUJBQVM7WUFDbEQsaUNBQ2tDO1lBQWhDLDZGQUFTLHlCQUFxQixJQUFDO1lBQUMseUJBQVM7WUFBQSxpQkFBUztZQUNwRCxpQ0FFRTtZQURBLDZGQUFTLHVCQUFtQixJQUFDO1lBQzdCLDJCQUFVO1lBQUEsaUJBQVM7WUFDdkIsaUJBQU07WUFDUixpQkFBVztZQUNYLCtCQUNFO1lBQUEsb0VBRUU7WUF5SEYsbUVBQ0U7WUFHSixpQkFBTTtZQUVOLHdDQUVvQjs7WUEvSWQsZUFBaUI7WUFBakIscUNBQWlCO1lBWWhCLGVBQTBCO1lBQTFCLDZDQUEwQjtZQTJIMUIsZUFBeUI7WUFBekIsNENBQXlCO1lBTzlCLGVBQXlCO1lBQXpCLDJDQUF5Qjs7MkJEL0kzQjtDQXFUQyxBQTNSRCxJQTJSQztTQXRSWSxnQkFBZ0I7a0RBQWhCLGdCQUFnQjtjQUw1QixTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsV0FBVyxFQUFFLDJCQUEyQjtnQkFDeEMsU0FBUyxFQUFFLENBQUMsMkJBQTJCLENBQUM7YUFDekMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vZm9mLXBlcm1pc3Npb24uc2VydmljZSdcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcclxuaW1wb3J0IHsgaVJvbGUsIGlVc2VyLCBpVXNlclJvbGVPcmdhbml6YXRpb24sIGlPcmdhbml6YXRpb24gfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycydcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlLCBmb3JrSm9pbiwgb2YgfSBmcm9tICdyeGpzJ1xyXG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1CdWlsZGVyLCBWYWxpZGF0b3JzICB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiXHJcbmltcG9ydCB7IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgRm9mRGlhbG9nU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLWRpYWxvZy5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBmb2ZVdGlsc0Zvcm0gfSBmcm9tICcuLi8uLi9jb3JlL2ZvZi11dGlscydcclxuaW1wb3J0IHsgTWF0RGlhbG9nLCBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZydcclxuaW1wb3J0IHsgRm9mVXNlclJvbGVzU2VsZWN0Q29tcG9uZW50IH0gZnJvbSAnLi4vZm9mLXVzZXItcm9sZXMtc2VsZWN0L2ZvZi11c2VyLXJvbGVzLXNlbGVjdC5jb21wb25lbnQnXHJcbmltcG9ydCB7IEZvZkVycm9yU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLWVycm9yLnNlcnZpY2UnXHJcblxyXG5pbnRlcmZhY2UgaVJvbGVVSSBleHRlbmRzIGlSb2xlIHtcclxuICBjaGVja2VkPzogYm9vbGVhblxyXG4gIHVzZXJSb2xlT3JnYW5pemF0aW9uPzogaVVzZXJSb2xlT3JnYW5pemF0aW9uXHJcbn1cclxuXHJcbmludGVyZmFjZSBpVXNlclJvbGVUb0Rpc3BsYXkge1xyXG4gIG9yZ2FuaXphdGlvbklkPzogbnVtYmVyLFxyXG4gIG9yZ2FuaXphdGlvbj86IHN0cmluZyxcclxuICByb2xlcz86IHN0cmluZ1tdXHJcbiAgdXNlclJvbGVPcmdhbml6YXRpb25zPzogaVVzZXJSb2xlT3JnYW5pemF0aW9uW11cclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2YtY29yZS1mb2YtdXNlcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZvZi11c2VyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9mb2YtdXNlci5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZVc2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9mUGVybWlzc2lvblNlcnZpY2U6IEZvZlBlcm1pc3Npb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIGZvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mRGlhbG9nU2VydmljZTogRm9mRGlhbG9nU2VydmljZSxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIG1hdERpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgcHJpdmF0ZSBmb2ZFcnJvclNlcnZpY2U6IEZvZkVycm9yU2VydmljZVxyXG4gICkgeyAgICAgXHJcbiAgfVxyXG5cclxuICAvLyBBbGwgcHJpdmF0ZSB2YXJpYWJsZXNcclxuICBwcml2YXRlIHByaVZhciA9IHtcclxuICAgIHVzZXJJZDogPG51bWJlcj51bmRlZmluZWQsXHJcbiAgICB1c2VyT3JnYW5pemF0aW9uSWQ6IDxudW1iZXI+dW5kZWZpbmVkLFxyXG4gICAgb3JnYW5pemF0aW9uQWxyZWFkeVdpdGhSb2xlczogPGlPcmdhbml6YXRpb25bXT51bmRlZmluZWRcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICAgIHVzZXJMb2FkOigpID0+IHtcclxuICAgICAgdGhpcy51aVZhci5sb2FkaW5nVXNlciA9IHRydWVcclxuICAgICAgUHJvbWlzZS5hbGwoW1xyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2Uucm9sZS5nZXRBbGwoKS50b1Byb21pc2UoKSxcclxuICAgICAgICB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIuZ2V0V2l0aFJvbGVCeUlkKHRoaXMucHJpVmFyLnVzZXJJZCkudG9Qcm9taXNlKCkgICAgICAgIFxyXG4gICAgICBdKVxyXG4gICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgIGNvbnN0IHJvbGVzOiBpUm9sZVtdID0gcmVzdWx0WzBdICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICBjb25zdCBjdXJyZW50VXNlcnMgPSByZXN1bHRbMV1cclxuXHJcbiAgICAgICAgY29uc29sZS5sb2coJ2N1cnJlbnRVc2VycycsIGN1cnJlbnRVc2VycylcclxuICAgICAgXHJcbiAgICAgICAgdGhpcy51aVZhci5hbGxSb2xlcyA9IHJvbGVzXHJcbiAgICAgICAgdGhpcy5wcml2RnVuYy51c2VyUmVmcmVzaChjdXJyZW50VXNlcnMpICAgICAgICAgIFxyXG4gICAgICAgIC8vIEhlcmUgZm9yIHByZXZlbnRpbmcgdG8gcmVmcmVzaCB0aGUgdXNlciBmb3JtIHdoZW4gcmVmcmVzaGluZyBvbmx5IHRoZSByb2xlcy5cclxuICAgICAgICAvLyB0b0RvOiBpbXByb3ZlXHJcbiAgICAgICAgdGhpcy51aVZhci5mb3JtLnBhdGNoVmFsdWUodGhpcy51aVZhci51c2VyKSAgICAgICAgICAgICAgICBcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7ICAgICAgICBcclxuICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShyZWFzb24pXHJcbiAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLi8nXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGV9KVxyXG4gICAgICB9KVxyXG4gICAgICAuZmluYWxseSgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy51aVZhci5sb2FkaW5nVXNlciA9IGZhbHNlXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG4gICAgdXNlclJlZnJlc2g6KGN1cnJlbnRVc2VyOiBpVXNlcikgPT4ge1xyXG4gICAgICBcclxuICAgICAgaWYgKCFjdXJyZW50VXNlcikge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5lcnJvcihgQ2UgdXRpbGlzYXRldXIgbidleGlzdGUgcGFzYClcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy4uLyddLCB7cmVsYXRpdmVUbzogdGhpcy5hY3RpdmF0ZWRSb3V0ZX0pXHJcbiAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMudWlWYXIudXNlciA9IGN1cnJlbnRVc2VyICAgICAgXHJcblxyXG4gICAgICBpZiAoY3VycmVudFVzZXIub3JnYW5pemF0aW9uSWQpIHtcclxuICAgICAgICB0aGlzLnVpVmFyLnVzZXJPcmdhbml6YXRpb25zRGlzcGxheSA9IFt7aWQ6IGN1cnJlbnRVc2VyLm9yZ2FuaXphdGlvbklkfV1cclxuICAgICAgICB0aGlzLnByaVZhci51c2VyT3JnYW5pemF0aW9uSWQgPSBjdXJyZW50VXNlci5vcmdhbml6YXRpb25JZFxyXG4gICAgICB9XHJcblxyXG4gICAgICBsZXQgX3ByZXZpb3VzT3JnYW5pc2F0aW9uSWQ6IG51bWJlciA9IHVuZGVmaW5lZFxyXG4gICAgICBsZXQgX3JvbGVUb0Rpc3BsYXk6IGlVc2VyUm9sZVRvRGlzcGxheSA9IHVuZGVmaW5lZFxyXG4gICAgICBcclxuICAgICAgdGhpcy51aVZhci51c2VyUm9sZXNUb0Rpc3BsYXkgPSBbXVxyXG5cclxuICAgICAgaWYgKGN1cnJlbnRVc2VyLnVzZXJSb2xlT3JnYW5pemF0aW9ucyAmJiBjdXJyZW50VXNlci51c2VyUm9sZU9yZ2FuaXphdGlvbnMubGVuZ3RoID4gMCkgeyAgICAgICAgICAgICAgICBcclxuICAgICAgICAvLyB3ZSB3aWxsIGRpc3BsYXkgdGhlIHVzZXIgcm9sZXMgZ3JvdXBlZCBieSBvcmdhbml6YXRpb24gXHJcbiAgICAgICAgLy8gd2l0aCBvbmUgb24gc2V2ZXJhbCByb2xlcyBvbiBpdFxyXG4gICAgICAgIGN1cnJlbnRVc2VyLnVzZXJSb2xlT3JnYW5pemF0aW9ucy5mb3JFYWNoKHVybyA9PiB7XHJcbiAgICAgICAgICBpZiAoX3ByZXZpb3VzT3JnYW5pc2F0aW9uSWQgIT09IHVyby5vcmdhbml6YXRpb24uaWQpIHtcclxuICAgICAgICAgICAgLy8gaXQncyBhIG5ldyBvcmdhbmlzYXRpb24sIHJlaW5pdFxyXG4gICAgICAgICAgICBfcHJldmlvdXNPcmdhbmlzYXRpb25JZCA9IHVyby5vcmdhbml6YXRpb24uaWRcclxuICAgICAgICAgICAgaWYgKF9yb2xlVG9EaXNwbGF5KSB7IHRoaXMudWlWYXIudXNlclJvbGVzVG9EaXNwbGF5LnB1c2goX3JvbGVUb0Rpc3BsYXkpIH1cclxuICAgICAgICAgICAgX3JvbGVUb0Rpc3BsYXkgPSB7IFxyXG4gICAgICAgICAgICAgIG9yZ2FuaXphdGlvbklkOiB1cm8ub3JnYW5pemF0aW9uLmlkLFxyXG4gICAgICAgICAgICAgIG9yZ2FuaXphdGlvbjogdXJvLm9yZ2FuaXphdGlvbi5uYW1lLFxyXG4gICAgICAgICAgICAgIHJvbGVzOiBbXSxcclxuICAgICAgICAgICAgICB1c2VyUm9sZU9yZ2FuaXphdGlvbnM6IFtdXHJcbiAgICAgICAgICAgIH0gICAgICAgICAgICBcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIF9yb2xlVG9EaXNwbGF5LnJvbGVzLnB1c2godXJvLnJvbGUuY29kZSlcclxuICAgICAgICAgIF9yb2xlVG9EaXNwbGF5LnVzZXJSb2xlT3JnYW5pemF0aW9ucy5wdXNoKHVybylcclxuICAgICAgICB9KSAgICAgICAgICAgIFxyXG4gICAgICB9XHJcblxyXG4gICAgICAvLyBhZGQgdGhlIGxhc3Qgb25lXHJcbiAgICAgIGlmIChfcm9sZVRvRGlzcGxheSkgeyB0aGlzLnVpVmFyLnVzZXJSb2xlc1RvRGlzcGxheS5wdXNoKF9yb2xlVG9EaXNwbGF5KSB9XHJcblxyXG4gICAgICAvLyB3ZSBkb24ndCB3YW50IHRoZSBmb2xsb3dpbmcgb3JnYW5pemF0aW9uIGNvdWxkIGJlIHNlbGVjdGViYWxlIGluIHRoZSBcclxuICAgICAgLy8gb3JnYW5pemF0aW9uIHRyZWVcclxuICAgICAgdGhpcy5wcmlWYXIub3JnYW5pemF0aW9uQWxyZWFkeVdpdGhSb2xlcyA9IFtdXHJcbiAgICAgIHRoaXMudWlWYXIudXNlclJvbGVzVG9EaXNwbGF5LmZvckVhY2gob3JnID0+IHtcclxuICAgICAgICB0aGlzLnByaVZhci5vcmdhbml6YXRpb25BbHJlYWR5V2l0aFJvbGVzLnB1c2goe1xyXG4gICAgICAgICAgaWQ6IG9yZy5vcmdhbml6YXRpb25JZCxcclxuICAgICAgICAgIG5hbWU6IG9yZy5vcmdhbml6YXRpb25cclxuICAgICAgICB9KVxyXG4gICAgICB9KVxyXG5cclxuICAgIH0sXHJcbiAgICB1c2VSb2xlc1JlZnJlc2g6KCkgPT4geyAgICAgIFxyXG4gICAgICB0aGlzLnVpVmFyLmxvYWRpbmdSb2xlcyA9IHRydWVcclxuICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS51c2VyLmdldFdpdGhSb2xlQnlJZCh0aGlzLnByaVZhci51c2VySWQpXHJcbiAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAudGhlbih1c2Vyc1Jlc3VsdCA9PiB7XHJcbiAgICAgICAgdGhpcy5wcml2RnVuYy51c2VyUmVmcmVzaCh1c2Vyc1Jlc3VsdCkgICAgICAgICAgICBcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxyXG4gICAgICAgIC8vIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLi4vJ10sIHtyZWxhdGl2ZVRvOiB0aGlzLmFjdGl2YXRlZFJvdXRlfSlcclxuICAgICAgfSlcclxuICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMudWlWYXIubG9hZGluZ1JvbGVzID0gZmFsc2VcclxuICAgICAgfSkgICAgICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgdGl0bGU6ICdOb3V2ZWwgdXRpbGlzYXRldXInLCAgICBcclxuICAgIGxvYWRpbmdVc2VyOiBmYWxzZSxcclxuICAgIGxvYWRpbmdSb2xlczogZmFsc2UsXHJcbiAgICB1c2VySXNOZXc6IGZhbHNlLFxyXG4gICAgdXNlcjogPGlVc2VyPnVuZGVmaW5lZCxcclxuICAgIHVzZXJPcmdhbml6YXRpb25zRGlzcGxheTogPGlPcmdhbml6YXRpb25bXT51bmRlZmluZWQsXHJcbiAgICBmb3JtOiB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHsgICAgICBcclxuICAgICAgbG9naW46IFsnJywgW1ZhbGlkYXRvcnMubWF4TGVuZ3RoKDMwKV1dLFxyXG4gICAgICBlbWFpbDogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5lbWFpbCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoNjApXV0sXHJcbiAgICAgIGZpcnN0TmFtZTogWycnLCBbVmFsaWRhdG9ycy5tYXhMZW5ndGgoMzApXV0sXHJcbiAgICAgIGxhc3ROYW1lOiBbJycsIFtWYWxpZGF0b3JzLm1heExlbmd0aCgzMCldXVxyXG4gICAgfSksXHJcbiAgICBhbGxSb2xlczogPGlSb2xlVUlbXT51bmRlZmluZWQsXHJcbiAgICBzZWxlY3RlZFVzZXJSb2xlT3JnYW5pemF0aW9uczogPGlVc2VyUm9sZU9yZ2FuaXphdGlvbltdPnVuZGVmaW5lZCxcclxuICAgIHVzZXJSb2xlc1RvRGlzcGxheTogPGlVc2VyUm9sZVRvRGlzcGxheVtdPltdLFxyXG4gICAgcm9sZURpc3BsYXllZENvbHVtbnM6IFsnZGVsZXRlJywgJ29yZ2FuaXphdGlvbicsICdyb2xlcycsICdpY29uJ10sXHJcbiAgICByb2xlc0FsbDogPGlSb2xlW10+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcclxuICAgIHVzZXJTYXZlOigpID0+IHsgICAgICBcclxuICAgICAgY29uc3QgdXNlclRvU2F2ZTogaVVzZXIgPSB0aGlzLnVpVmFyLmZvcm0udmFsdWVcclxuICAgICAgdXNlclRvU2F2ZS5vcmdhbml6YXRpb25JZCA9IHRoaXMucHJpVmFyLnVzZXJPcmdhbml6YXRpb25JZFxyXG5cclxuICAgICAgaWYgKCF0aGlzLnVpVmFyLmZvcm0udmFsaWQpIHtcclxuICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuZXJyb3IoJ1ZldWlsbGV6IGNvcnJpZ2VyIGxlcyBlcnJldXJzIGF2YW50IGRlIHNhdXZlZ2FyZGVyJylcclxuICAgICAgICBmb2ZVdGlsc0Zvcm0udmFsaWRhdGVBbGxGaWVsZHModGhpcy51aVZhci5mb3JtKVxyXG4gICAgICAgIHJldHVyblxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy51aVZhci51c2VySXNOZXcpIHsgICAgICAgIFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlci5jcmVhdGUodXNlclRvU2F2ZSlcclxuICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAudGhlbigobmV3VXNlcjogaVVzZXIpID0+IHtcclxuICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdVdGlsaXNhdGV1ciBzYXV2w6knLCB7bXVzdERpc2FwcGVhckFmdGVyOiAxMDAwfSlcclxuICAgICAgICAgIHRoaXMucHJpVmFyLnVzZXJJZCA9IG5ld1VzZXIuaWRcclxuICAgICAgICAgIHRoaXMudWlWYXIudGl0bGUgPSBgTW9kaWZpY2F0aW9uIGQndW4gdXRpbGlzYXRldXJgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnVzZXJJc05ldyA9IGZhbHNlXHJcbiAgICAgICAgICB0aGlzLnByaXZGdW5jLnVzZXJMb2FkKCkgICAgICAgICAgXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2gocmVhc29uID0+IHtcclxuICAgICAgICAgIHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmVycm9yTWFuYWdlKHJlYXNvbilcclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHVzZXJUb1NhdmUuaWQgPSB0aGlzLnVpVmFyLnVzZXIuaWQgXHJcbiAgICAgIFxyXG4gICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlci51cGRhdGUodXNlclRvU2F2ZSlcclxuICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLnN1Y2Nlc3MoJ1V0aWxpc2F0ZXVyIHNhdXbDqScsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IDEwMDB9KVxyXG4gICAgICAgIH0pIFxyXG4gICAgICAgIC5jYXRjaChyZWFzb24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICB1c2VyQ2FuY2VsOigpID0+IHsgICAgICBcclxuICAgICAgdGhpcy5wcml2RnVuYy51c2VyTG9hZCgpXHJcbiAgICB9LFxyXG4gICAgdXNlckRlbGV0ZTooKSA9PiB7XHJcbiAgICAgIHRoaXMuZm9mRGlhbG9nU2VydmljZS5vcGVuWWVzTm8oeyAgICAgICAgXHJcbiAgICAgICAgcXVlc3Rpb246IGBWb3VsZXogdm91cyB2cmFpbWVudCBzdXBwcmltZXIgbCcgdXRpbGlzYXRldXIgP2BcclxuICAgICAgfSkudGhlbih5ZXMgPT4ge1xyXG4gICAgICAgIGlmICh5ZXMpIHtcclxuICAgICAgICAgIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlci5kZWxldGUodGhpcy51aVZhci51c2VyKVxyXG4gICAgICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgICAgICAudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnVXRpbGlzYXRldXIgc3VwcHJpbcOpJylcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLi8nXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGV9KVxyXG4gICAgICAgICAgfSkgXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfSwgICAgICAgXHJcbiAgICB1c2VyUm9sZXNEZWxldGU6KCkgPT4ge1xyXG4gICAgICBjb25zdCB1c2VyUm9sZXMgPSB0aGlzLnVpVmFyLnVzZXJSb2xlc1RvRGlzcGxheVxyXG4gICAgICBjb25zdCBvcmdhbml6YXRpb25zSWRUb0RlbGV0ZTogQXJyYXk8bnVtYmVyPiA9IFtdXHJcbiAgICAgIGxldCBzb21ldGhpbmdUb0RlbGV0ZSA9IGZhbHNlXHJcbiAgICAgIGxldCBtZXNzYWdlID0gYFZvdWxleiB2b3VzIHZyYWltZW50IHN1cHByaW1lciBsZXMgcsO0bGVzIGRlcyBvcmdhbmlzYXRpb25zIHN1aXZhbnRlID88YnI+YFxyXG4gICAgICBtZXNzYWdlID0gbWVzc2FnZSArICc8dWw+J1xyXG5cclxuICAgICAgdXNlclJvbGVzLmZvckVhY2godXIgPT4ge1xyXG4gICAgICAgIC8vZG9uJ3Qgd2FudCB0byBjcmVhdGUgYW4gaW50ZXJmYWNlIGZvciAxIHBhcmFtKVxyXG4gICAgICAgIGlmICh1clsnY2hlY2tlZCddKSB7XHJcbiAgICAgICAgICBzb21ldGhpbmdUb0RlbGV0ZSA9IHRydWVcclxuICAgICAgICAgIG1lc3NhZ2UgPSBtZXNzYWdlICsgYDxsaT4ke3VyLm9yZ2FuaXphdGlvbn08L2xpPmBcclxuICAgICAgICAgIG9yZ2FuaXphdGlvbnNJZFRvRGVsZXRlLnB1c2godXIub3JnYW5pemF0aW9uSWQpXHJcbiAgICAgICAgfSAgICAgIFxyXG4gICAgICB9KVxyXG5cclxuICAgICAgbWVzc2FnZSA9IG1lc3NhZ2UgKyAnPC91bD4nICAgICAgXHJcblxyXG5cclxuICAgICAgaWYgKHNvbWV0aGluZ1RvRGVsZXRlKSB7XHJcbiAgICAgICAgdGhpcy5mb2ZEaWFsb2dTZXJ2aWNlLm9wZW5ZZXNObyh7ICAgICAgICBcclxuICAgICAgICAgIHRpdGxlOiAnU3VwcHJpbWVyIHLDtGxlcycsXHJcbiAgICAgICAgICBxdWVzdGlvbjogbWVzc2FnZVxyXG4gICAgICAgIH0pLnRoZW4oeWVzID0+IHtcclxuICAgICAgICAgIGlmICh5ZXMpIHsgICAgICAgICAgICBcclxuICAgICAgICAgICAgdGhpcy5mb2ZQZXJtaXNzaW9uU2VydmljZS51c2VyLmRlbGV0ZVVzZXJSb2xlT3JnYW5pemF0aW9ucyh0aGlzLnVpVmFyLnVzZXIuaWQsIG9yZ2FuaXphdGlvbnNJZFRvRGVsZXRlKVxyXG4gICAgICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnUsO0bGVzIHN1cHByaW3DqXMnKVxyXG4gICAgICAgICAgICAgIHRoaXMucHJpdkZ1bmMudXNlUm9sZXNSZWZyZXNoKCkgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9KSBcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5pbmZvKCdWb3VzIGRldmV6IHPDqWxlY3Rpb25uZXIgdWEgbW9pbnMgdW5lIG9yZ2FuaXNhdGlvbicpXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICByb2xlU2VsZWN0Q29tcG9uZW50T3BlbjoodXNlclJvbGU/OiBpVXNlclJvbGVUb0Rpc3BsYXkpID0+IHsgICAgICBcclxuICAgICAgbGV0IHVzZXJSb2xlT3JnYW5pemF0aW9uczogaVVzZXJSb2xlT3JnYW5pemF0aW9uW11cclxuXHJcbiAgICAgIGlmICh1c2VyUm9sZSkge1xyXG4gICAgICAgIHVzZXJSb2xlT3JnYW5pemF0aW9ucyA9IHVzZXJSb2xlLnVzZXJSb2xlT3JnYW5pemF0aW9uc1xyXG4gICAgICB9XHJcblxyXG4gICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLm1hdERpYWxvZy5vcGVuKEZvZlVzZXJSb2xlc1NlbGVjdENvbXBvbmVudCwge1xyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIHJvbGVzOiB0aGlzLnVpVmFyLmFsbFJvbGVzLFxyXG4gICAgICAgICAgdXNlclJvbGVPcmdhbml6YXRpb25zOiB1c2VyUm9sZU9yZ2FuaXphdGlvbnMsXHJcbiAgICAgICAgICBub3RTZWxlY3RhYmxlT3JnYW5pemF0aW9uczogdGhpcy5wcmlWYXIub3JnYW5pemF0aW9uQWxyZWFkeVdpdGhSb2xlcyxcclxuICAgICAgICAgIHVzZXJJZDogdGhpcy51aVZhci51c2VyLmlkXHJcbiAgICAgICAgfSxcclxuICAgICAgICB3aWR0aDogJzYwMHB4JyAsXHJcbiAgICAgICAgaGVpZ2h0OiAnY2FsYygxMDB2aCAtIDIwMHB4KScgICAgICBcclxuICAgICAgfSlcclxuICAgICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKClcclxuICAgICAgLnRvUHJvbWlzZSgpXHJcbiAgICAgIC50aGVuKG11c3RCZVJlZnJlc2QgPT4geyAgICAgICAgXHJcbiAgICAgICAgaWYgKG11c3RCZVJlZnJlc2QpIHtcclxuICAgICAgICAgIHRoaXMucHJpdkZ1bmMudXNlUm9sZXNSZWZyZXNoKClcclxuICAgICAgICB9ICAgICAgICBcclxuICAgICAgfSlcclxuICAgIH0sXHJcbiAgICBvcmdhbmlzYXRpb25NdWx0aVNlbGVjdGVkQ2hhbmdlOihvcmdhbml6YXRpb25zOiBpT3JnYW5pemF0aW9uW10pID0+IHsgICAgICBcclxuICAgICAgaWYgKG9yZ2FuaXphdGlvbnMgJiYgb3JnYW5pemF0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgdGhpcy5wcmlWYXIudXNlck9yZ2FuaXphdGlvbklkID0gb3JnYW5pemF0aW9uc1swXS5pZFxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMucHJpVmFyLnVzZXJPcmdhbml6YXRpb25JZCA9IG51bGxcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5wYXJhbU1hcC5zdWJzY3JpYmUocGFyYW1zID0+IHtcclxuICAgICAgY29uc3QgY29kZTphbnkgPSBwYXJhbXMuZ2V0KCdjb2RlJylcclxuICAgICAgXHJcbiAgICAgIGlmIChjb2RlKSB7XHJcbiAgICAgICAgaWYgKGNvZGUudG9Mb3dlckNhc2UoKSA9PSAnbmV3Jykge1xyXG4gICAgICAgICAgdGhpcy51aVZhci51c2VySXNOZXcgPSB0cnVlXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMucHJpVmFyLnVzZXJJZCA9IGNvZGVcclxuICAgICAgICAgIHRoaXMudWlWYXIudGl0bGUgPSBgTW9kaWZpY2F0aW9uIGQndW4gdXRpbGlzYXRldXJgXHJcbiAgICAgICAgICB0aGlzLnByaXZGdW5jLnVzZXJMb2FkKClcclxuICAgICAgICB9XHJcbiAgICAgIH0gICAgICBcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcbiIsIjxtYXQtY2FyZCBjbGFzcz1cImZvZi1oZWFkZXJcIj4gICAgICAgIFxyXG4gIDxoMz57eyB1aVZhci50aXRsZSB9fTwvaDM+IFxyXG4gIDxkaXYgY2xhc3M9XCJmb2YtdG9vbGJhclwiPlxyXG4gICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b25cclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVzZXJDYW5jZWwoKVwiPkFubnVsZXI8L2J1dHRvbj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwid2FyblwiXHJcbiAgICAgIChjbGljayk9XCJ1aUFjdGlvbi51c2VyRGVsZXRlKClcIj5TdXBwcmltZXI8L2J1dHRvbj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwiYWNjZW50XCJcclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVzZXJTYXZlKClcIj5cclxuICAgICAgRW5yZWdpc3RlcjwvYnV0dG9uPiAgICBcclxuICA8L2Rpdj4gXHJcbjwvbWF0LWNhcmQ+XHJcbjxkaXYgY2xhc3M9XCJtYWluXCI+ICBcclxuICA8ZGl2ICpuZ0lmPVwiIXVpVmFyLmxvYWRpbmdVc2VyXCIgY2xhc3M9XCJkZXRhaWwgZm9mLWZhZGUtaW5cIj4gICAgXHJcblxyXG4gICAgPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29sLW1kLTZcIj5cclxuXHJcbiAgICAgICAgPGZvcm0gW2Zvcm1Hcm91cF09XCJ1aVZhci5mb3JtXCI+XHJcblxyXG4gICAgICAgICAgPG1hdC1mb3JtLWZpZWxkPlxyXG4gICAgICAgICAgICA8aW5wdXQgbWF0SW5wdXQgXHJcbiAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwibG9naW5cIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwibG9naW5cIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgICAgICA8bWF0LWVycm9yICpuZ0lmPVwidWlWYXIuZm9ybS5nZXQoJ2xvZ2luJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICAgIExlIGxvZ2luIG5lIGRvaXQgcGFzIGV4Y8OpZGVyIDMwIGNhcmFjdMOocmVzXHJcbiAgICAgICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuXHJcbiAgICAgICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgICAgIDxpbnB1dCBtYXRJbnB1dCByZXF1aXJlZCBcclxuICAgICAgICAgICAgICB0eXBlPVwiZW1haWxcIlxyXG4gICAgICAgICAgICAgIGZvcm1Db250cm9sTmFtZT1cImVtYWlsXCJcclxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cIkVtYWlsXCIgdmFsdWU9XCJcIj5cclxuICAgICAgICAgICAgPG1hdC1lcnJvciAqbmdJZj1cInVpVmFyLmZvcm0uZ2V0KCdlbWFpbCcpLmludmFsaWRcIj5cclxuICAgICAgICAgICAgICBVbiBlbWFpbCB2YWxpZGUgZXN0IG9ibGlnYXRvaXJlXHJcbiAgICAgICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuXHJcbiAgICAgICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgICAgIDxpbnB1dCBtYXRJbnB1dCByZXF1aXJlZCBcclxuICAgICAgICAgICAgICB0eXBlPVwiZmlyc3ROYW1lXCJcclxuICAgICAgICAgICAgICBmb3JtQ29udHJvbE5hbWU9XCJmaXJzdE5hbWVcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiUHLDqW5vbVwiIHZhbHVlPVwiXCI+XHJcbiAgICAgICAgICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJ1aVZhci5mb3JtLmdldCgnZmlyc3ROYW1lJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICAgIExlIHByw6lub20gbmUgZG9pdCBwYXMgZXhjw6lkZXIgMzAgY2FyYWN0w6hyZXNcclxuICAgICAgICAgICAgPC9tYXQtZXJyb3I+XHJcbiAgICAgICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG5cclxuICAgICAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICAgICAgPGlucHV0IG1hdElucHV0IHJlcXVpcmVkIFxyXG4gICAgICAgICAgICAgIHR5cGU9XCJsYXN0TmFtZVwiXHJcbiAgICAgICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwibGFzdE5hbWVcIlxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiTm9tIGRlIGZhbWlsbGVcIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgICAgICA8bWF0LWVycm9yICpuZ0lmPVwidWlWYXIuZm9ybS5nZXQoJ2xhc3ROYW1lJykuaW52YWxpZFwiPlxyXG4gICAgICAgICAgICAgIExlIG5vbSBkZSBmYW1pbGxlIG5lIGRvaXQgcGFzIGV4Y8OpZGVyIDMwIGNhcmFjdMOocmVzXHJcbiAgICAgICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICAgICAgPC9tYXQtZm9ybS1maWVsZD4gICAgIFxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICA8Zm9mLWNvcmUtZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0XHJcbiAgICAgICAgICAgIFtwbGFjZUhvbGRlcl09XCInT3JnYW5pc2F0aW9uIGRlIHJhdHRhY2hlbWVudCdcIlxyXG4gICAgICAgICAgICBbbXVsdGlTZWxlY3RdPVwiZmFsc2VcIiAgICBcclxuICAgICAgICAgICAgW3NlbGVjdGVkT3JnYW5pc2F0aW9uc109XCJ1aVZhci51c2VyT3JnYW5pemF0aW9uc0Rpc3BsYXlcIiAgIFxyXG4gICAgICAgICAgICAoc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlKSA9IFwidWlBY3Rpb24ub3JnYW5pc2F0aW9uTXVsdGlTZWxlY3RlZENoYW5nZSgkZXZlbnQpXCIgICBcclxuICAgICAgICAgID48L2ZvZi1jb3JlLWZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdD5cclxuICAgICAgIFxyXG4gICAgICAgIDwvZm9ybT5cclxuICAgICAgXHJcbiAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgPGRpdiBjbGFzcz1cImNvbC1tZC02XCIgKm5nSWY9XCJ1aVZhci5hbGxSb2xlc1wiPlxyXG5cclxuICAgICAgICA8bWF0LWNhcmQ+XHJcbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiZm9mLWhlYWRlclwiPlxyXG4gICAgICAgICAgICA8aDQ+UsO0bGVzPC9oND5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImZvZi10b29sYmFyXCI+XHJcbiAgICAgICAgICAgICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJ3YXJuXCJcclxuICAgICAgICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi51c2VyUm9sZXNEZWxldGUoKVwiPlN1cHByaW1lciByw7RsZXM8L2J1dHRvbj5cclxuICAgICAgICAgICAgICA8YnV0dG9uIG1hdC1zdHJva2VkLWJ1dHRvblxyXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnJvbGVTZWxlY3RDb21wb25lbnRPcGVuKClcIj5Bam91dGVyIHVuIHLDtGxlPC9idXR0b24+ICAgICAgICAgICBcclxuICAgICAgICAgICAgPC9kaXY+IFxyXG4gICAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgICAgPGRpdiBjbGFzcz1cImZvZi10YWJsZS1jb250YWluZXJcIj5cclxuICAgICAgICAgICAgPGRpdiAqbmdJZj1cInVpVmFyLmxvYWRpbmdSb2xlc1wiIGNsYXNzPVwiZm9mLWxvYWRpbmcgZm9mLWxvYWRpbmctcm9sZXNcIj5cclxuICAgICAgICAgICAgICA8bWF0LXNwaW5uZXIgZGlhbWV0ZXI9MjA+PC9tYXQtc3Bpbm5lcj4gPHNwYW4+UmFmcmFpY2hpc3NlbWVudCBkZXMgcsO0bGVzLi4uPC9zcGFuPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgICAgIDx0YWJsZSBtYXQtdGFibGUgW2RhdGFTb3VyY2VdPVwidWlWYXIudXNlclJvbGVzVG9EaXNwbGF5XCIgY2xhc3M9XCJkYXRhLXRhYmxlXCI+XHJcblxyXG4gICAgICAgICAgICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwiZGVsZXRlXCI+XHJcbiAgICAgICAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPjwvdGg+XHJcbiAgICAgICAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+ICBcclxuICAgICAgICAgICAgICAgICAgPG1hdC1jaGVja2JveFxyXG4gICAgICAgICAgICAgICAgICAgIFsobmdNb2RlbCldPVwicm93LmNoZWNrZWRcIj5cclxuICAgICAgICAgICAgICAgICAgPC9tYXQtY2hlY2tib3g+XHJcbiAgICAgICAgICAgICAgICA8L3RkPlxyXG4gICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwib3JnYW5pemF0aW9uXCI+XHJcbiAgICAgICAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPk9yZ2FuaXNhdGlvbjwvdGg+XHJcbiAgICAgICAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCJcclxuICAgICAgICAgICAgICAgIChjbGljayk9XCJ1aUFjdGlvbi5yb2xlU2VsZWN0Q29tcG9uZW50T3Blbihyb3cpXCI+XHJcbiAgICAgICAgICAgICAgICB7e3Jvdy5vcmdhbml6YXRpb259fTwvdGQ+XHJcbiAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICBcclxuICAgICAgICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cInJvbGVzXCI+XHJcbiAgICAgICAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPlJvbGVzPC90aD5cclxuICAgICAgICAgICAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIlxyXG4gICAgICAgICAgICAgICAgICAoY2xpY2spPVwidWlBY3Rpb24ucm9sZVNlbGVjdENvbXBvbmVudE9wZW4ocm93KVwiPiAgXHJcbiAgICAgICAgICAgICAgICAgIDxkaXYgKm5nRm9yPVwibGV0IHJvbGUgb2Ygcm93LnJvbGVzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge3sgcm9sZSB9fVxyXG4gICAgICAgICAgICAgICAgICA8L2Rpdj4gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgICAgICAgICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwiaWNvblwiPlxyXG4gICAgICAgICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZj48L3RoPlxyXG4gICAgICAgICAgICAgICAgPHRkIGNsYXNzPVwiaWNvbi1zZWxlY3RcIiBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIlxyXG4gICAgICAgICAgICAgICAgICAoY2xpY2spPVwidWlBY3Rpb24ucm9sZVNlbGVjdENvbXBvbmVudE9wZW4ocm93KVwiPiAgXHJcbiAgICAgICAgICAgICAgICAgIDxtYXQtaWNvbj5jaGV2cm9uX3JpZ2h0PC9tYXQtaWNvbj5cclxuICAgICAgICAgICAgICAgIDwvdGQ+XHJcbiAgICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgICBcclxuICAgICAgICAgICAgICA8dHIgbWF0LWhlYWRlci1yb3cgKm1hdEhlYWRlclJvd0RlZj1cInVpVmFyLnJvbGVEaXNwbGF5ZWRDb2x1bW5zXCI+PC90cj5cclxuICAgICAgICAgICAgICA8dHIgbWF0LXJvdyBjbGFzcz1cImZvZi1lbGVtZW50LW92ZXJcIiBcclxuICAgICAgICAgICAgICAgICptYXRSb3dEZWY9XCJsZXQgcm93OyBjb2x1bW5zOiB1aVZhci5yb2xlRGlzcGxheWVkQ29sdW1ucztcIj48L3RyPlxyXG5cclxuICAgICAgICAgICAgPC90YWJsZT4gICAgICAgIFxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L21hdC1jYXJkPlxyXG5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuICA8ZGl2ICpuZ0lmPVwidWlWYXIubG9hZGluZ1VzZXJcIiBjbGFzcz1cImZvZi1sb2FkaW5nXCI+XHJcbiAgICA8bWF0LXNwaW5uZXIgZGlhbWV0ZXI9MjA+PC9tYXQtc3Bpbm5lcj4gPHNwYW4+Q2hhcmdlbWVudHMgZGVzIHV0aWxpc2F0ZXVycyBldCBkZXMgYXV0aG9yaXNhdGlvbnMuLi48L3NwYW4+XHJcbiAgPC9kaXY+XHJcbiAgICBcclxuPC9kaXY+XHJcblxyXG48Zm9mLWVudGl0eS1mb290ZXJcclxuICBbZW50aXR5QmFzZV09XCJ1aVZhci51c2VyXCI+XHJcbjwvZm9mLWVudGl0eS1mb290ZXI+Il19
import { Component, ViewChild, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import { FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
import * as i4 from "../../core/fof-error.service";
import * as i5 from "@angular/material/card";
import * as i6 from "../../components/fof-organizations-tree/fof-organizations-tree.component";
import * as i7 from "@angular/material/button";
import * as i8 from "@angular/router";
import * as i9 from "@angular/material/form-field";
import * as i10 from "@angular/material/input";
import * as i11 from "@angular/forms";
import * as i12 from "@angular/common";
import * as i13 from "@angular/material/table";
import * as i14 from "@angular/material/sort";
import * as i15 from "@angular/material/paginator";
import * as i16 from "@angular/material/progress-spinner";
import * as i17 from "@ngx-translate/core";
function FofUsersComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 21);
    i0.ɵɵelement(1, "mat-spinner", 22);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des utilisateurs...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function FofUsersComponent_th_24_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Email");
    i0.ɵɵelementEnd();
} }
function FofUsersComponent_td_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r232 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r232.email);
} }
function FofUsersComponent_th_27_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Login");
    i0.ɵɵelementEnd();
} }
function FofUsersComponent_td_28_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r233 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r233.login);
} }
function FofUsersComponent_th_30_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Pr\u00E9nom");
    i0.ɵɵelementEnd();
} }
function FofUsersComponent_td_31_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r234 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r234.firstName);
} }
function FofUsersComponent_th_33_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Nom");
    i0.ɵɵelementEnd();
} }
function FofUsersComponent_td_34_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r235 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r235.lastName);
} }
function FofUsersComponent_tr_35_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 25);
} }
function FofUsersComponent_tr_36_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 26);
} if (rf & 2) {
    var row_r236 = ctx.$implicit;
    i0.ɵɵproperty("routerLink", row_r236.id);
} }
var _c0 = function () { return [5, 10, 25, 100]; };
var FofUsersComponent = /** @class */ (function () {
    function FofUsersComponent(fofPermissionService, fofNotificationService, breakpointObserver, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
            filter: undefined,
            filterOrganizationsChange: new EventEmitter(),
            filterOrganizations: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: [],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true,
            searchUsersCtrl: new FormControl()
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: function (organization) {
                if (organization) {
                    _this.priVar.filterOrganizations = [organization.id];
                }
                else {
                    _this.priVar.filterOrganizations = null;
                }
                _this.priVar.filterOrganizationsChange.emit(organization);
            }
        };
    }
    // Angular events
    FofUsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.filter = this.uiVar.searchUsersCtrl.valueChanges
            .pipe(debounceTime(500), distinctUntilChanged(), filter(function (query) { return query.length >= 3 || query.length === 0; }));
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['email', 'login'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['email', 'login', 'firstName', 'lastName'];
            }
        });
    };
    FofUsersComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page, this.priVar.filter, this.priVar.filterOrganizationsChange)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.user.search(_this.uiVar.searchUsersCtrl.value, _this.priVar.filterOrganizations, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function (error) {
            _this.fofErrorService.errorManage(error);
            _this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(function (data) {
            _this.uiVar.data = data;
        });
    };
    FofUsersComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    FofUsersComponent.ɵfac = function FofUsersComponent_Factory(t) { return new (t || FofUsersComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver), i0.ɵɵdirectiveInject(i4.FofErrorService)); };
    FofUsersComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofUsersComponent, selectors: [["fof-core-fof-users"]], viewQuery: function FofUsersComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(MatPaginator, true);
            i0.ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 38, vars: 17, consts: [[1, "row"], [1, "col-md-3"], [1, "card-tree"], [3, "multiSelect", "selectedOrganizationsChange"], [1, "col-md-9"], [1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "filtres"], ["autofocus", "", "matInput", "", "placeholder", "email ou nom ou pr\u00E9nom ou login", 3, "formControl"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "email", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "email"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "login"], ["matColumnDef", "firstName"], ["matColumnDef", "lastName"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function FofUsersComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "div", 1);
            i0.ɵɵelementStart(2, "mat-card", 2);
            i0.ɵɵelementStart(3, "fof-organizations-tree", 3);
            i0.ɵɵlistener("selectedOrganizationsChange", function FofUsersComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(4, "div", 4);
            i0.ɵɵelementStart(5, "mat-card", 5);
            i0.ɵɵelementStart(6, "h3");
            i0.ɵɵtext(7);
            i0.ɵɵpipe(8, "translate");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "a", 6);
            i0.ɵɵelementStart(10, "span");
            i0.ɵɵtext(11);
            i0.ɵɵpipe(12, "translate");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(13, "mat-card", 7);
            i0.ɵɵelementStart(14, "mat-form-field");
            i0.ɵɵelementStart(15, "mat-label");
            i0.ɵɵtext(16, "Filtre");
            i0.ɵɵelementEnd();
            i0.ɵɵelement(17, "input", 8);
            i0.ɵɵelementStart(18, "mat-hint");
            i0.ɵɵtext(19, "Recherche \u00E0 partir de 3 caract\u00E8res saisies");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(20, "div", 9);
            i0.ɵɵtemplate(21, FofUsersComponent_div_21_Template, 4, 0, "div", 10);
            i0.ɵɵelementStart(22, "table", 11);
            i0.ɵɵelementContainerStart(23, 12);
            i0.ɵɵtemplate(24, FofUsersComponent_th_24_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(25, FofUsersComponent_td_25_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(26, 15);
            i0.ɵɵtemplate(27, FofUsersComponent_th_27_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(28, FofUsersComponent_td_28_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(29, 16);
            i0.ɵɵtemplate(30, FofUsersComponent_th_30_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(31, FofUsersComponent_td_31_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(32, 17);
            i0.ɵɵtemplate(33, FofUsersComponent_th_33_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(34, FofUsersComponent_td_34_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵtemplate(35, FofUsersComponent_tr_35_Template, 1, 0, "tr", 18);
            i0.ɵɵtemplate(36, FofUsersComponent_tr_36_Template, 1, 1, "tr", 19);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(37, "mat-paginator", 20);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("multiSelect", false);
            i0.ɵɵadvance(4);
            i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(8, 12, "admin.users.title-main"));
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("routerLink", "/admin/users/new");
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(i0.ɵɵpipeBind1(12, 14, "admin.users.btn-user-add"));
            i0.ɵɵadvance(6);
            i0.ɵɵproperty("formControl", ctx.uiVar.searchUsersCtrl);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("dataSource", ctx.uiVar.data);
            i0.ɵɵadvance(13);
            i0.ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", i0.ɵɵpureFunction0(16, _c0))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [i5.MatCard, i6.FofOrganizationsTreeComponent, i7.MatAnchor, i8.RouterLinkWithHref, i9.MatFormField, i9.MatLabel, i10.MatInput, i11.DefaultValueAccessor, i11.NgControlStatus, i11.FormControlDirective, i9.MatHint, i12.NgIf, i13.MatTable, i14.MatSort, i13.MatColumnDef, i13.MatHeaderCellDef, i13.MatCellDef, i13.MatHeaderRowDef, i13.MatRowDef, i15.MatPaginator, i16.MatSpinner, i13.MatHeaderCell, i14.MatSortHeader, i13.MatCell, i13.MatHeaderRow, i13.MatRow, i8.RouterLink], pipes: [i17.TranslatePipe], styles: [".filtres[_ngcontent-%COMP%]{margin-bottom:15px}.filtres[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.card-tree[_ngcontent-%COMP%]{height:100%}"] });
    return FofUsersComponent;
}());
export { FofUsersComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofUsersComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-users',
                templateUrl: './fof-users.component.html',
                styleUrls: ['./fof-users.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }, { type: i4.FofErrorService }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXVzZXJzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2FkbWluL2ZvZi11c2Vycy9mb2YtdXNlcnMuY29tcG9uZW50LnRzIiwibGliL2FkbWluL2ZvZi11c2Vycy9mb2YtdXNlcnMuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBbUMsU0FBUyxFQUNqRCxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFJaEQsT0FBTyxFQUFFLFlBQVksRUFBb0IsTUFBTSw2QkFBNkIsQ0FBQTtBQUM1RSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sd0JBQXdCLENBQUE7QUFDaEQsT0FBTyxFQUFFLEtBQUssRUFBYyxFQUFFLElBQUksWUFBWSxFQUFpQyxNQUFNLE1BQU0sQ0FBQTtBQUMzRixPQUFPLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFDMUQsb0JBQW9CLEVBQVksTUFBTSxFQUFFLE1BQU0sZ0JBQWdCLENBQUE7QUFFaEUsT0FBTyxFQUFzQixXQUFXLEVBQW1CLE1BQU0scUJBQXFCLENBQUE7QUFDdEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ21CdEMsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLCtDQUErQjtJQUFBLGlCQUFPO0lBQ3RGLGlCQUFNOzs7SUFNRiw4QkFBc0Q7SUFBQSxxQkFBSztJQUFBLGlCQUFLOzs7SUFDaEUsOEJBQW1DO0lBQUEsWUFBYTtJQUFBLGlCQUFLOzs7SUFBbEIsZUFBYTtJQUFiLG9DQUFhOzs7SUFJaEQsOEJBQXNEO0lBQUEscUJBQUs7SUFBQSxpQkFBSzs7O0lBQ2hFLDhCQUFtQztJQUFBLFlBQWE7SUFBQSxpQkFBSzs7O0lBQWxCLGVBQWE7SUFBYixvQ0FBYTs7O0lBSWhELDhCQUFzRDtJQUFBLDJCQUFNO0lBQUEsaUJBQUs7OztJQUNqRSw4QkFBbUM7SUFBQSxZQUFpQjtJQUFBLGlCQUFLOzs7SUFBdEIsZUFBaUI7SUFBakIsd0NBQWlCOzs7SUFJcEQsOEJBQXNEO0lBQUEsbUJBQUc7SUFBQSxpQkFBSzs7O0lBQzlELDhCQUFtQztJQUFBLFlBQWdCO0lBQUEsaUJBQUs7OztJQUFyQixlQUFnQjtJQUFoQix1Q0FBZ0I7OztJQUdyRCx5QkFBa0U7OztJQUNsRSx5QkFDOEQ7OztJQUR6Qix3Q0FBcUI7OztBRDFDbEU7SUFTRSwyQkFDVSxvQkFBMEMsRUFDMUMsc0JBQThDLEVBQzlDLGtCQUFzQyxFQUN0QyxlQUFnQztRQUoxQyxpQkFPQztRQU5TLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUsxQyx3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2YscUJBQXFCLEVBQWdCLFNBQVM7WUFDOUMsTUFBTSxFQUFzQixTQUFTO1lBQ3JDLHlCQUF5QixFQUErQixJQUFJLFlBQVksRUFBRTtZQUMxRSxtQkFBbUIsRUFBWSxTQUFTO1NBQ3pDLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHLEVBQ2xCLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsZ0JBQWdCLEVBQVksRUFBRTtZQUM5QixJQUFJLEVBQVcsRUFBRTtZQUNqQixhQUFhLEVBQUUsQ0FBQztZQUNoQixRQUFRLEVBQUUsQ0FBQztZQUNYLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsZUFBZSxFQUFFLElBQUksV0FBVyxFQUFFO1NBQ25DLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHO1lBQ2hCLCtCQUErQixFQUFDLFVBQUMsWUFBMkI7Z0JBQzFELElBQUksWUFBWSxFQUFFO29CQUNoQixLQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO2lCQUNwRDtxQkFBTTtvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQTtpQkFDdkM7Z0JBQ0QsS0FBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDMUQsQ0FBQztTQUNGLENBQUE7SUEvQkQsQ0FBQztJQWdDRCxpQkFBaUI7SUFDakIsb0NBQVEsR0FBUjtRQUFBLGlCQWtCQztRQWpCQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxZQUFZO2FBQ3pELElBQUksQ0FDSCxZQUFZLENBQUMsR0FBRyxDQUFDLEVBQ2pCLG9CQUFvQixFQUFFLEVBQ3RCLE1BQU0sQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUF2QyxDQUF1QyxDQUFDLENBQ3pELENBQUE7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzthQUN0RixTQUFTLENBQUMsVUFBQyxLQUFzQjtZQUNoQyxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ2pCLFNBQVM7Z0JBQ1QsS0FBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQTthQUNqRDtpQkFBTTtnQkFDTCxXQUFXO2dCQUNYLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQTthQUMxRTtRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELDJDQUFlLEdBQWY7UUFBQSxpQkE4QkM7UUE3QkMsb0VBQW9FO1FBQ3BFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxFQUE1QixDQUE0QixDQUFDLENBQUE7UUFFbEUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUM7YUFDeEcsSUFBSSxDQUNILFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixTQUFTLENBQUM7WUFDUixLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQTtZQUNsQyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQTtZQUU3QyxPQUFPLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUMxQyxLQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQ2hDLEtBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQy9CLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNuQixLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3BFLENBQUMsQ0FBQyxFQUNGLEdBQUcsQ0FBQyxVQUFDLE1BQXlCO1lBQzVCLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLEtBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUE7WUFDdkMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxVQUFBLEtBQUs7WUFDZCxLQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUN2QyxLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQTtZQUNuQyxPQUFPLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUN6QixDQUFDLENBQUMsQ0FDSCxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDZCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7UUFDeEIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsdUNBQVcsR0FBWDtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRTtZQUFFLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUE7U0FBRTtJQUM1RixDQUFDO3NGQWxHVSxpQkFBaUI7MERBQWpCLGlCQUFpQjsyQkFDakIsWUFBWTsyQkFDWixPQUFPOzs7Ozs7WUN2QnBCLDhCQUNFO1lBQUEsOEJBQ0U7WUFBQSxtQ0FDRTtZQUFBLGlEQUd5QjtZQUZ2QixnS0FBK0Isb0RBQWdELElBQUM7WUFFbEYsaUJBQXlCO1lBQzNCLGlCQUFXO1lBQ2IsaUJBQU07WUFDTiw4QkFFRTtZQUFBLG1DQUNFO1lBQUEsMEJBQUk7WUFBQSxZQUEwQzs7WUFBQSxpQkFBSztZQUNuRCw0QkFFRTtZQUFBLDZCQUFNO1lBQUEsYUFBNEM7O1lBQUEsaUJBQU87WUFDM0QsaUJBQUk7WUFDTixpQkFBVztZQUVYLG9DQUNFO1lBQUEsdUNBQ0U7WUFBQSxrQ0FBVztZQUFBLHVCQUFNO1lBQUEsaUJBQVk7WUFDN0IsNEJBRUE7WUFBQSxpQ0FBVTtZQUFBLHFFQUEwQztZQUFBLGlCQUFXO1lBQ2pFLGlCQUFpQjtZQUNuQixpQkFBVztZQUVYLCtCQUVFO1lBQUEscUVBQ0U7WUFHRixrQ0FHRTtZQUFBLGtDQUNFO1lBQUEsbUVBQXNEO1lBQ3RELG1FQUFtQztZQUNyQywwQkFBZTtZQUVmLGtDQUNFO1lBQUEsbUVBQXNEO1lBQ3RELG1FQUFtQztZQUNyQywwQkFBZTtZQUVmLGtDQUNFO1lBQUEsbUVBQXNEO1lBQ3RELG1FQUFtQztZQUNyQywwQkFBZTtZQUVmLGtDQUNFO1lBQUEsbUVBQXNEO1lBQ3RELG1FQUFtQztZQUNyQywwQkFBZTtZQUVmLG1FQUE2RDtZQUM3RCxtRUFDeUQ7WUFDM0QsaUJBQVE7WUFFUixxQ0FFOEM7WUFFaEQsaUJBQU07WUFFUixpQkFBTTtZQUNSLGlCQUFNOztZQWhFRSxlQUFxQjtZQUFyQixtQ0FBcUI7WUFPbkIsZUFBMEM7WUFBMUMscUVBQTBDO1lBRTVDLGVBQWlDO1lBQWpDLCtDQUFpQztZQUMzQixlQUE0QztZQUE1Qyx3RUFBNEM7WUFRaEQsZUFBcUM7WUFBckMsdURBQXFDO1lBT0ksZUFBOEI7WUFBOUIsaURBQThCO1lBSTFELGVBQXlCO1lBQXpCLDJDQUF5QjtZQXVCckIsZ0JBQXlDO1lBQXpDLDREQUF5QztZQUUxRCxlQUFzRDtZQUF0RCw2REFBc0Q7WUFHM0MsZUFBOEI7WUFBOUIsZ0RBQThCLGdEQUFBLGdDQUFBOzs0QkQvRG5EO0NBMEhDLEFBekdELElBeUdDO1NBcEdZLGlCQUFpQjtrREFBakIsaUJBQWlCO2NBTDdCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixXQUFXLEVBQUUsNEJBQTRCO2dCQUN6QyxTQUFTLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQzthQUMxQzs7a0JBRUUsU0FBUzttQkFBQyxZQUFZOztrQkFDdEIsU0FBUzttQkFBQyxPQUFPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIFxyXG4gIE9uRGVzdHJveSwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgaVVzZXIgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFBhZ2luYXRvckludGwgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InXHJcbmltcG9ydCB7IE1hdFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0J1xyXG5pbXBvcnQgeyBtZXJnZSwgT2JzZXJ2YWJsZSwgb2YgYXMgb2JzZXJ2YWJsZU9mLCBTdWJzY3JpcHRpb24sIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnXHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCwgc3RhcnRXaXRoLCBzd2l0Y2hNYXAsIGRlYm91bmNlVGltZSwgdGFwLCBcclxuICBkaXN0aW5jdFVudGlsQ2hhbmdlZCwgZmluYWxpemUsIGZpbHRlciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJ1xyXG5pbXBvcnQgeyBpZm9mU2VhcmNoIH0gZnJvbSAnLi4vLi4vY29yZS9jb3JlLmludGVyZmFjZSdcclxuaW1wb3J0IHsgQnJlYWtwb2ludE9ic2VydmVyLCBCcmVha3BvaW50cywgQnJlYWtwb2ludFN0YXRlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2xheW91dCdcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3JtcydcclxuaW1wb3J0IHsgRm9mRXJyb3JTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtZXJyb3Iuc2VydmljZSdcclxuaW1wb3J0IHsgaU9yZ2FuaXphdGlvbiB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IE9ic2VydmVyc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9vYnNlcnZlcnMnXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1jb3JlLWZvZi11c2VycycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZvZi11c2Vycy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9mLXVzZXJzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZlVzZXJzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xyXG4gIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yKSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvclxyXG4gIEBWaWV3Q2hpbGQoTWF0U29ydCkgc29ydDogTWF0U29ydFxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9mUGVybWlzc2lvblNlcnZpY2U6IEZvZlBlcm1pc3Npb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBicmVha3BvaW50T2JzZXJ2ZXI6IEJyZWFrcG9pbnRPYnNlcnZlcixcclxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2VcclxuICApIHsgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgYnJlYWtwb2ludE9ic2VydmVyU3ViOiA8U3Vic2NyaXB0aW9uPnVuZGVmaW5lZCwgICAgXHJcbiAgICBmaWx0ZXI6IDxPYnNlcnZhYmxlPHN0cmluZz4+dW5kZWZpbmVkLFxyXG4gICAgZmlsdGVyT3JnYW5pemF0aW9uc0NoYW5nZTogPEV2ZW50RW1pdHRlcjxpT3JnYW5pemF0aW9uPj5uZXcgRXZlbnRFbWl0dGVyKCksXHJcbiAgICBmaWx0ZXJPcmdhbml6YXRpb25zOiA8bnVtYmVyW10+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0geyAgICBcclxuICAgIGRpc3BsYXllZENvbHVtbnM6IDxzdHJpbmdbXT5bXSxcclxuICAgIGRhdGE6IDxpVXNlcltdPltdLFxyXG4gICAgcmVzdWx0c0xlbmd0aDogMCxcclxuICAgIHBhZ2VTaXplOiA1LFxyXG4gICAgaXNMb2FkaW5nUmVzdWx0czogdHJ1ZSxcclxuICAgIHNlYXJjaFVzZXJzQ3RybDogbmV3IEZvcm1Db250cm9sKCkgICAgXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcclxuICAgIG9yZ2FuaXNhdGlvbk11bHRpU2VsZWN0ZWRDaGFuZ2U6KG9yZ2FuaXphdGlvbjogaU9yZ2FuaXphdGlvbikgPT4geyAgICAgIFxyXG4gICAgICBpZiAob3JnYW5pemF0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9ucyA9IFtvcmdhbml6YXRpb24uaWRdXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9ucyA9IG51bGxcclxuICAgICAgfSAgICAgIFxyXG4gICAgICB0aGlzLnByaVZhci5maWx0ZXJPcmdhbml6YXRpb25zQ2hhbmdlLmVtaXQob3JnYW5pemF0aW9uKSAgICAgIFxyXG4gICAgfVxyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5wcmlWYXIuZmlsdGVyID0gdGhpcy51aVZhci5zZWFyY2hVc2Vyc0N0cmwudmFsdWVDaGFuZ2VzXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGRlYm91bmNlVGltZSg1MDApLFxyXG4gICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKCksICAgICAgICBcclxuICAgICAgICBmaWx0ZXIocXVlcnkgPT4gcXVlcnkubGVuZ3RoID49IDMgfHwgcXVlcnkubGVuZ3RoID09PSAwKSAgICAgICAgICAgICBcclxuICAgICAgKSBcclxuICAgIFxyXG4gICAgdGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViID0gdGhpcy5icmVha3BvaW50T2JzZXJ2ZXIub2JzZXJ2ZShCcmVha3BvaW50cy5YU21hbGwpXHJcbiAgICAuc3Vic2NyaWJlKChzdGF0ZTogQnJlYWtwb2ludFN0YXRlKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmIChzdGF0ZS5tYXRjaGVzKSB7XHJcbiAgICAgICAgLy8gWFNtYWxsXHJcbiAgICAgICAgdGhpcy51aVZhci5kaXNwbGF5ZWRDb2x1bW5zID0gWydlbWFpbCcsICdsb2dpbiddXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gPiBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ2VtYWlsJywgJ2xvZ2luJywgJ2ZpcnN0TmFtZScsICdsYXN0TmFtZSddXHJcbiAgICAgIH1cclxuICAgIH0pICAgXHJcbiAgfSAgXHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHsgICAgXHJcbiAgICAvLyBJZiB0aGUgdXNlciBjaGFuZ2VzIHRoZSBzb3J0IG9yZGVyLCByZXNldCBiYWNrIHRvIHRoZSBmaXJzdCBwYWdlLlxyXG4gICAgdGhpcy5zb3J0LnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCA9IDApXHJcblxyXG4gICAgbWVyZ2UodGhpcy5zb3J0LnNvcnRDaGFuZ2UsIHRoaXMucGFnaW5hdG9yLnBhZ2UsIHRoaXMucHJpVmFyLmZpbHRlciwgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9uc0NoYW5nZSlcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgc3RhcnRXaXRoKHt9KSxcclxuICAgICAgICBzd2l0Y2hNYXAoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gdHJ1ZSAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMudWlWYXIucGFnZVNpemUgPSB0aGlzLnBhZ2luYXRvci5wYWdlU2l6ZVxyXG5cclxuICAgICAgICAgIHJldHVybiB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnVzZXIuc2VhcmNoKCAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLnVpVmFyLnNlYXJjaFVzZXJzQ3RybC52YWx1ZSxcclxuICAgICAgICAgICAgdGhpcy5wcmlWYXIuZmlsdGVyT3JnYW5pemF0aW9ucyxcclxuICAgICAgICAgICAgdGhpcy51aVZhci5wYWdlU2l6ZSwgXHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCwgdGhpcy5zb3J0LmFjdGl2ZSwgdGhpcy5zb3J0LmRpcmVjdGlvbilcclxuICAgICAgICB9KSxcclxuICAgICAgICBtYXAoKHNlYXJjaDogaWZvZlNlYXJjaDxpVXNlcj4pID0+IHsgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMudWlWYXIucmVzdWx0c0xlbmd0aCA9IHNlYXJjaC50b3RhbCAgICAgICAgICBcclxuICAgICAgICAgIHJldHVybiBzZWFyY2guZGF0YVxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGNhdGNoRXJyb3IoZXJyb3IgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UoZXJyb3IpICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2UgICAgICAgICAgXHJcbiAgICAgICAgICByZXR1cm4gb2JzZXJ2YWJsZU9mKFtdKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICkuc3Vic2NyaWJlKGRhdGEgPT4geyAgICAgICAgXHJcbiAgICAgICAgdGhpcy51aVZhci5kYXRhID0gZGF0YVxyXG4gICAgICB9KVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViKSB7IHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1Yi51bnN1YnNjcmliZSgpIH1cclxuICB9XHJcblxyXG59XHJcbiIsIlxyXG48ZGl2IGNsYXNzPVwicm93XCI+XHJcbiAgPGRpdiBjbGFzcz1cImNvbC1tZC0zXCI+XHJcbiAgICA8bWF0LWNhcmQgY2xhc3M9XCJjYXJkLXRyZWVcIj5cclxuICAgICAgPGZvZi1vcmdhbml6YXRpb25zLXRyZWVcclxuICAgICAgICAoc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlKT1cInVpQWN0aW9uLm9yZ2FuaXNhdGlvbk11bHRpU2VsZWN0ZWRDaGFuZ2UoJGV2ZW50KVwiXHJcbiAgICAgICAgW211bHRpU2VsZWN0XT1cImZhbHNlXCI+XHJcbiAgICAgIDwvZm9mLW9yZ2FuaXphdGlvbnMtdHJlZT5cclxuICAgIDwvbWF0LWNhcmQ+XHJcbiAgPC9kaXY+XHJcbiAgPGRpdiBjbGFzcz1cImNvbC1tZC05XCI+XHJcblxyXG4gICAgPG1hdC1jYXJkIGNsYXNzPVwiZm9mLWhlYWRlclwiPlxyXG4gICAgICA8aDM+e3sgJ2FkbWluLnVzZXJzLnRpdGxlLW1haW4nIHwgdHJhbnNsYXRlIH19PC9oMz4gICBcclxuICAgICAgPGEgbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwiYWNjZW50XCIgXHJcbiAgICAgICAgW3JvdXRlckxpbmtdPVwiJy9hZG1pbi91c2Vycy9uZXcnXCI+ICAgIFxyXG4gICAgICAgIDxzcGFuPnt7ICdhZG1pbi51c2Vycy5idG4tdXNlci1hZGQnIHwgdHJhbnNsYXRlIH19PC9zcGFuPlxyXG4gICAgICA8L2E+ICAgIFxyXG4gICAgPC9tYXQtY2FyZD5cclxuICAgIFxyXG4gICAgPG1hdC1jYXJkIGNsYXNzPVwiZmlsdHJlc1wiPlxyXG4gICAgICA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgPG1hdC1sYWJlbD5GaWx0cmU8L21hdC1sYWJlbD5cclxuICAgICAgICA8aW5wdXQgYXV0b2ZvY3VzIG1hdElucHV0IHBsYWNlaG9sZGVyPVwiZW1haWwgb3Ugbm9tIG91IHByw6lub20gb3UgbG9naW5cIiBcclxuICAgICAgICAgIFtmb3JtQ29udHJvbF09XCJ1aVZhci5zZWFyY2hVc2Vyc0N0cmxcIj5cclxuICAgICAgICA8bWF0LWhpbnQ+UmVjaGVyY2hlIMOgIHBhcnRpciBkZSAzIGNhcmFjdMOocmVzIHNhaXNpZXM8L21hdC1oaW50PiAgICBcclxuICAgICAgPC9tYXQtZm9ybS1maWVsZD4gIFxyXG4gICAgPC9tYXQtY2FyZD4gIFxyXG4gICAgXHJcbiAgICA8ZGl2IGNsYXNzPVwiZm9mLXRhYmxlLWNvbnRhaW5lciBtYXQtZWxldmF0aW9uLXoyXCI+XHJcbiAgICBcclxuICAgICAgPGRpdiBjbGFzcz1cInRhYmxlLWxvYWRpbmctc2hhZGUgZm9mLWxvYWRpbmdcIiAqbmdJZj1cInVpVmFyLmlzTG9hZGluZ1Jlc3VsdHNcIj5cclxuICAgICAgICA8bWF0LXNwaW5uZXIgZGlhbWV0ZXI9MjA+PC9tYXQtc3Bpbm5lcj4gPHNwYW4+Q2hhcmdlbWVudHMgZGVzIHV0aWxpc2F0ZXVycy4uLjwvc3Bhbj5cclxuICAgICAgPC9kaXY+XHJcbiAgICBcclxuICAgICAgPHRhYmxlIG1hdC10YWJsZSBbZGF0YVNvdXJjZV09XCJ1aVZhci5kYXRhXCIgY2xhc3M9XCJkYXRhLXRhYmxlXCJcclxuICAgICAgICAgICAgbWF0U29ydCBtYXRTb3J0QWN0aXZlPVwiZW1haWxcIiBtYXRTb3J0RGlzYWJsZUNsZWFyIG1hdFNvcnREaXJlY3Rpb249XCJhc2NcIj5cclxuICAgICAgICBcclxuICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImVtYWlsXCI+XHJcbiAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsIG1hdC1zb3J0LWhlYWRlciAqbWF0SGVhZGVyQ2VsbERlZj5FbWFpbDwvdGg+XHJcbiAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cuZW1haWx9fTwvdGQ+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICBcclxuICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImxvZ2luXCI+XHJcbiAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsIG1hdC1zb3J0LWhlYWRlciAqbWF0SGVhZGVyQ2VsbERlZj5Mb2dpbjwvdGg+XHJcbiAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cubG9naW59fTwvdGQ+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICBcclxuICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImZpcnN0TmFtZVwiPlxyXG4gICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCBtYXQtc29ydC1oZWFkZXIgKm1hdEhlYWRlckNlbGxEZWY+UHLDqW5vbTwvdGg+XHJcbiAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cuZmlyc3ROYW1lfX08L3RkPlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgXHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJsYXN0TmFtZVwiPlxyXG4gICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCBtYXQtc29ydC1oZWFkZXIgKm1hdEhlYWRlckNlbGxEZWY+Tm9tPC90aD5cclxuICAgICAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIj57e3Jvdy5sYXN0TmFtZX19PC90ZD5cclxuICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgIFxyXG4gICAgICAgIDx0ciBtYXQtaGVhZGVyLXJvdyAqbWF0SGVhZGVyUm93RGVmPVwidWlWYXIuZGlzcGxheWVkQ29sdW1uc1wiPjwvdHI+XHJcbiAgICAgICAgPHRyIG1hdC1yb3cgY2xhc3M9XCJmb2YtZWxlbWVudC1vdmVyXCIgW3JvdXRlckxpbmtdPVwicm93LmlkXCJcclxuICAgICAgICAgICptYXRSb3dEZWY9XCJsZXQgcm93OyBjb2x1bW5zOiB1aVZhci5kaXNwbGF5ZWRDb2x1bW5zO1wiPjwvdHI+XHJcbiAgICAgIDwvdGFibGU+XHJcbiAgICBcclxuICAgICAgPG1hdC1wYWdpbmF0b3IgW2xlbmd0aF09XCJ1aVZhci5yZXN1bHRzTGVuZ3RoXCIgXHJcbiAgICAgICAgW3BhZ2VTaXplT3B0aW9uc109XCJbNSwgMTAsIDI1LCAxMDBdXCJcclxuICAgICAgICBbcGFnZVNpemVdPVwidWlWYXIucGFnZVNpemVcIj48L21hdC1wYWdpbmF0b3I+XHJcbiAgICBcclxuICAgIDwvZGl2PlxyXG4gICAgXHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG5cclxuXHJcblxyXG4iXX0=
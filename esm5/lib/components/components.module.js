import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FofOrganizationsTreeComponent } from './fof-organizations-tree/fof-organizations-tree.component';
import { ComponentsService } from './components.service';
import { FofPermissionModule } from '../permission/permission.module';
import { MaterialModule } from '../core/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FofOrganizationsMultiSelectComponent } from './fof-organizations-multi-select/fof-organizations-multi-select.component';
import { FofDatatableComponent } from './fof-datatable/fof-datatable.component';
import * as i0 from "@angular/core";
var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule.ɵmod = i0.ɵɵdefineNgModule({ type: ComponentsModule });
    ComponentsModule.ɵinj = i0.ɵɵdefineInjector({ factory: function ComponentsModule_Factory(t) { return new (t || ComponentsModule)(); }, providers: [
            ComponentsService
        ], imports: [[
                CommonModule,
                FofPermissionModule,
                MaterialModule,
                FormsModule,
                ReactiveFormsModule
            ]] });
    return ComponentsModule;
}());
export { ComponentsModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(ComponentsModule, { declarations: [FofOrganizationsTreeComponent,
        FofOrganizationsMultiSelectComponent,
        FofDatatableComponent], imports: [CommonModule,
        FofPermissionModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule], exports: [FofOrganizationsTreeComponent,
        FofOrganizationsMultiSelectComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(ComponentsModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    FofOrganizationsTreeComponent,
                    FofOrganizationsMultiSelectComponent,
                    FofDatatableComponent
                ],
                imports: [
                    CommonModule,
                    FofPermissionModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                providers: [
                    ComponentsService
                ],
                exports: [
                    FofOrganizationsTreeComponent,
                    FofOrganizationsMultiSelectComponent
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tcG9uZW50cy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2NvbXBvbmVudHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDJEQUEyRCxDQUFBO0FBQ3pHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFBO0FBQ3hELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFBO0FBQ3JFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLG9DQUFvQyxFQUFFLE1BQU0sMkVBQTJFLENBQUM7QUFDakksT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seUNBQXlDLENBQUE7O0FBRS9FO0lBQUE7S0FxQmlDO3dEQUFwQixnQkFBZ0I7bUhBQWhCLGdCQUFnQixtQkFSaEI7WUFDVCxpQkFBaUI7U0FDbEIsWUFUUTtnQkFDUCxZQUFZO2dCQUNaLG1CQUFtQjtnQkFDbkIsY0FBYztnQkFDZCxXQUFXO2dCQUNYLG1CQUFtQjthQUNwQjsyQkF0Qkg7Q0ErQmlDLEFBckJqQyxJQXFCaUM7U0FBcEIsZ0JBQWdCO3dGQUFoQixnQkFBZ0IsbUJBbkJ6Qiw2QkFBNkI7UUFDN0Isb0NBQW9DO1FBQ3BDLHFCQUFxQixhQUdyQixZQUFZO1FBQ1osbUJBQW1CO1FBQ25CLGNBQWM7UUFDZCxXQUFXO1FBQ1gsbUJBQW1CLGFBTW5CLDZCQUE2QjtRQUM3QixvQ0FBb0M7a0RBRzNCLGdCQUFnQjtjQXJCNUIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWiw2QkFBNkI7b0JBQzdCLG9DQUFvQztvQkFDcEMscUJBQXFCO2lCQUN0QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixtQkFBbUI7b0JBQ25CLGNBQWM7b0JBQ2QsV0FBVztvQkFDWCxtQkFBbUI7aUJBQ3BCO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxpQkFBaUI7aUJBQ2xCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCw2QkFBNkI7b0JBQzdCLG9DQUFvQztpQkFDckM7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJ1xyXG5pbXBvcnQgeyBGb2ZPcmdhbml6YXRpb25zVHJlZUNvbXBvbmVudCB9IGZyb20gJy4vZm9mLW9yZ2FuaXphdGlvbnMtdHJlZS9mb2Ytb3JnYW5pemF0aW9ucy10cmVlLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgQ29tcG9uZW50c1NlcnZpY2UgfSBmcm9tICcuL2NvbXBvbmVudHMuc2VydmljZSdcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvbk1vZHVsZSB9IGZyb20gJy4uL3Blcm1pc3Npb24vcGVybWlzc2lvbi5tb2R1bGUnXHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vY29yZS9tYXRlcmlhbC5tb2R1bGUnXHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBGb2ZPcmdhbml6YXRpb25zTXVsdGlTZWxlY3RDb21wb25lbnQgfSBmcm9tICcuL2ZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdC9mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3QuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9mRGF0YXRhYmxlQ29tcG9uZW50IH0gZnJvbSAnLi9mb2YtZGF0YXRhYmxlL2ZvZi1kYXRhdGFibGUuY29tcG9uZW50J1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIEZvZk9yZ2FuaXphdGlvbnNUcmVlQ29tcG9uZW50LFxyXG4gICAgRm9mT3JnYW5pemF0aW9uc011bHRpU2VsZWN0Q29tcG9uZW50LFxyXG4gICAgRm9mRGF0YXRhYmxlQ29tcG9uZW50XHJcbiAgXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBGb2ZQZXJtaXNzaW9uTW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSwgXHJcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlXHJcbiAgXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIENvbXBvbmVudHNTZXJ2aWNlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBGb2ZPcmdhbml6YXRpb25zVHJlZUNvbXBvbmVudCxcclxuICAgIEZvZk9yZ2FuaXphdGlvbnNNdWx0aVNlbGVjdENvbXBvbmVudFxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbXBvbmVudHNNb2R1bGUgeyB9XHJcbiJdfQ==
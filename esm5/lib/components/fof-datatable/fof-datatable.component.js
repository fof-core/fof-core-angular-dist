import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
var FofDatatableComponent = /** @class */ (function () {
    function FofDatatableComponent(fofPermissionService, fofNotificationService, breakpointObserver) {
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['code', 'description'],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    FofDatatableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['code'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['code', 'description'];
            }
        });
    };
    FofDatatableComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.role.search(null, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function () {
            _this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(function (data) { return _this.uiVar.data = data; });
    };
    FofDatatableComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    FofDatatableComponent.ɵfac = function FofDatatableComponent_Factory(t) { return new (t || FofDatatableComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver)); };
    FofDatatableComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofDatatableComponent, selectors: [["fof-core-fof-datatable"]], viewQuery: function FofDatatableComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(MatPaginator, true);
            i0.ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 2, vars: 0, template: function FofDatatableComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "p");
            i0.ɵɵtext(1, "fof-datatable works!");
            i0.ɵɵelementEnd();
        } }, styles: [""] });
    return FofDatatableComponent;
}());
export { FofDatatableComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofDatatableComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-datatable',
                templateUrl: './fof-datatable.component.html',
                styleUrls: ['./fof-datatable.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWRhdGF0YWJsZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2ZvZi1kYXRhdGFibGUvZm9mLWRhdGF0YWJsZS5jb21wb25lbnQudHMiLCJsaWIvY29tcG9uZW50cy9mb2YtZGF0YXRhYmxlL2ZvZi1kYXRhdGFibGUuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBbUMsU0FBUyxFQUE0QixNQUFNLGVBQWUsQ0FBQTtBQUkvRyxPQUFPLEVBQUUsWUFBWSxFQUFvQixNQUFNLDZCQUE2QixDQUFBO0FBQzVFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQTtBQUNoRCxPQUFPLEVBQUUsS0FBSyxFQUFjLEVBQUUsSUFBSSxZQUFZLEVBQWdCLE1BQU0sTUFBTSxDQUFBO0FBQzFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUV0RSxPQUFPLEVBQXNCLFdBQVcsRUFBbUIsTUFBTSxxQkFBcUIsQ0FBQTs7Ozs7QUFFdEY7SUFTRSwrQkFDVSxvQkFBMEMsRUFDMUMsc0JBQThDLEVBQzlDLGtCQUFzQztRQUZ0Qyx5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUtoRCx3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2YscUJBQXFCLEVBQWdCLFNBQVM7U0FDL0MsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUcsRUFDbEIsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixnQkFBZ0IsRUFBWSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUM7WUFDbkQsSUFBSSxFQUFXLEVBQUU7WUFDakIsYUFBYSxFQUFFLENBQUM7WUFDaEIsUUFBUSxFQUFFLENBQUM7WUFDWCxnQkFBZ0IsRUFBRSxJQUFJO1NBQ3ZCLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHLEVBQ2pCLENBQUE7SUFuQkQsQ0FBQztJQW9CRCxpQkFBaUI7SUFDakIsd0NBQVEsR0FBUjtRQUFBLGlCQVdDO1FBVkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDdEYsU0FBUyxDQUFDLFVBQUMsS0FBc0I7WUFDaEMsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO2dCQUNqQixTQUFTO2dCQUNULEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTthQUN2QztpQkFBTTtnQkFDTCxXQUFXO2dCQUNYLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsYUFBYSxDQUFDLENBQUE7YUFDdEQ7UUFDSCxDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCwrQ0FBZSxHQUFmO1FBQUEsaUJBdUJDO1FBdEJDLG9FQUFvRTtRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLENBQUMsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFBO1FBRWxFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzthQUM3QyxJQUFJLENBQ0gsU0FBUyxDQUFDLEVBQUUsQ0FBQyxFQUNiLFNBQVMsQ0FBQztZQUNSLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFBO1lBQ2xDLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFBO1lBQzdDLE9BQU8sS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUUsSUFBSSxFQUFFLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNyRSxLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3BFLENBQUMsQ0FBQyxFQUNGLEdBQUcsQ0FBQyxVQUFDLE1BQXlCO1lBQzVCLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLEtBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUE7WUFDdkMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQztZQUNULEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLE9BQU8sWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO1FBQ3pCLENBQUMsQ0FBQyxDQUNILENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxFQUF0QixDQUFzQixDQUFDLENBQUE7SUFDL0MsQ0FBQztJQUVELDJDQUFXLEdBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLEVBQUU7WUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQUU7SUFDNUYsQ0FBQzs4RkF2RVUscUJBQXFCOzhEQUFyQixxQkFBcUI7MkJBQ3JCLFlBQVk7MkJBQ1osT0FBTzs7Ozs7O1lDbEJwQix5QkFBRztZQUFBLG9DQUFvQjtZQUFBLGlCQUFJOztnQ0RBM0I7Q0F3RkMsQUE3RUQsSUE2RUM7U0F4RVkscUJBQXFCO2tEQUFyQixxQkFBcUI7Y0FMakMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSx3QkFBd0I7Z0JBQ2xDLFdBQVcsRUFBRSxnQ0FBZ0M7Z0JBQzdDLFNBQVMsRUFBRSxDQUFDLGdDQUFnQyxDQUFDO2FBQzlDOztrQkFFRSxTQUFTO21CQUFDLFlBQVk7O2tCQUN0QixTQUFTO21CQUFDLE9BQU8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIFZpZXdDaGlsZCwgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgaVJvbGUgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFBhZ2luYXRvckludGwgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InXHJcbmltcG9ydCB7IE1hdFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0J1xyXG5pbXBvcnQgeyBtZXJnZSwgT2JzZXJ2YWJsZSwgb2YgYXMgb2JzZXJ2YWJsZU9mLCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJ1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHN0YXJ0V2l0aCwgc3dpdGNoTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IGlmb2ZTZWFyY2ggfSBmcm9tICcuLi8uLi9jb3JlL2NvcmUuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBCcmVha3BvaW50T2JzZXJ2ZXIsIEJyZWFrcG9pbnRzLCBCcmVha3BvaW50U3RhdGUgfSBmcm9tICdAYW5ndWxhci9jZGsvbGF5b3V0J1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2YtY29yZS1mb2YtZGF0YXRhYmxlJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZm9mLWRhdGF0YWJsZS5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9mLWRhdGF0YWJsZS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZEYXRhdGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yKSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvclxyXG4gIEBWaWV3Q2hpbGQoTWF0U29ydCkgc29ydDogTWF0U29ydFxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9mUGVybWlzc2lvblNlcnZpY2U6IEZvZlBlcm1pc3Npb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBicmVha3BvaW50T2JzZXJ2ZXI6IEJyZWFrcG9pbnRPYnNlcnZlclxyXG4gICkgeyBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICBicmVha3BvaW50T2JzZXJ2ZXJTdWI6IDxTdWJzY3JpcHRpb24+dW5kZWZpbmVkLFxyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gIH1cclxuICAvLyBBbGwgdmFyaWFibGVzIHNoYXJlZCB3aXRoIFVJIFxyXG4gIHB1YmxpYyB1aVZhciA9IHsgICAgXHJcbiAgICBkaXNwbGF5ZWRDb2x1bW5zOiA8c3RyaW5nW10+Wydjb2RlJywgJ2Rlc2NyaXB0aW9uJ10sXHJcbiAgICBkYXRhOiA8aVJvbGVbXT5bXSxcclxuICAgIHJlc3VsdHNMZW5ndGg6IDAsXHJcbiAgICBwYWdlU2l6ZTogNSxcclxuICAgIGlzTG9hZGluZ1Jlc3VsdHM6IHRydWVcclxuICB9XHJcbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpQWN0aW9uID0ge1xyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViID0gdGhpcy5icmVha3BvaW50T2JzZXJ2ZXIub2JzZXJ2ZShCcmVha3BvaW50cy5YU21hbGwpXHJcbiAgICAuc3Vic2NyaWJlKChzdGF0ZTogQnJlYWtwb2ludFN0YXRlKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmIChzdGF0ZS5tYXRjaGVzKSB7XHJcbiAgICAgICAgLy8gWFNtYWxsXHJcbiAgICAgICAgdGhpcy51aVZhci5kaXNwbGF5ZWRDb2x1bW5zID0gWydjb2RlJ11cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyA+IFhTbWFsbFxyXG4gICAgICAgIHRoaXMudWlWYXIuZGlzcGxheWVkQ29sdW1ucyA9IFsnY29kZScsICdkZXNjcmlwdGlvbiddXHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgfSAgXHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHsgICAgXHJcbiAgICAvLyBJZiB0aGUgdXNlciBjaGFuZ2VzIHRoZSBzb3J0IG9yZGVyLCByZXNldCBiYWNrIHRvIHRoZSBmaXJzdCBwYWdlLlxyXG4gICAgdGhpcy5zb3J0LnNvcnRDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHRoaXMucGFnaW5hdG9yLnBhZ2VJbmRleCA9IDApXHJcblxyXG4gICAgbWVyZ2UodGhpcy5zb3J0LnNvcnRDaGFuZ2UsIHRoaXMucGFnaW5hdG9yLnBhZ2UpXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIHN0YXJ0V2l0aCh7fSksXHJcbiAgICAgICAgc3dpdGNoTWFwKCgpID0+IHtcclxuICAgICAgICAgIHRoaXMudWlWYXIuaXNMb2FkaW5nUmVzdWx0cyA9IHRydWUgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnBhZ2VTaXplID0gdGhpcy5wYWdpbmF0b3IucGFnZVNpemVcclxuICAgICAgICAgIHJldHVybiB0aGlzLmZvZlBlcm1pc3Npb25TZXJ2aWNlLnJvbGUuc2VhcmNoIChudWxsLCB0aGlzLnVpVmFyLnBhZ2VTaXplLCBcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4LCB0aGlzLnNvcnQuYWN0aXZlLCB0aGlzLnNvcnQuZGlyZWN0aW9uKVxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIG1hcCgoc2VhcmNoOiBpZm9mU2VhcmNoPGlSb2xlPikgPT4geyAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSBmYWxzZVxyXG4gICAgICAgICAgdGhpcy51aVZhci5yZXN1bHRzTGVuZ3RoID0gc2VhcmNoLnRvdGFsXHJcbiAgICAgICAgICByZXR1cm4gc2VhcmNoLmRhdGFcclxuICAgICAgICB9KSxcclxuICAgICAgICBjYXRjaEVycm9yKCgpID0+IHtcclxuICAgICAgICAgIHRoaXMudWlWYXIuaXNMb2FkaW5nUmVzdWx0cyA9IGZhbHNlICAgICAgICAgIFxyXG4gICAgICAgICAgcmV0dXJuIG9ic2VydmFibGVPZihbXSlcclxuICAgICAgICB9KVxyXG4gICAgICApLnN1YnNjcmliZShkYXRhID0+IHRoaXMudWlWYXIuZGF0YSA9IGRhdGEpXHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnByaVZhci5icmVha3BvaW50T2JzZXJ2ZXJTdWIpIHsgdGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViLnVuc3Vic2NyaWJlKCkgfVxyXG4gIH1cclxufVxyXG4iLCI8cD5mb2YtZGF0YXRhYmxlIHdvcmtzITwvcD5cclxuIl19
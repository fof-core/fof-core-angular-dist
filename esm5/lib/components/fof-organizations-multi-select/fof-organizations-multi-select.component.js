import { Component, Output, EventEmitter, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/form-field";
import * as i2 from "@angular/material/chips";
import * as i3 from "@angular/common";
import * as i4 from "@angular/material/input";
import * as i5 from "../fof-organizations-tree/fof-organizations-tree.component";
import * as i6 from "@angular/material/icon";
function FofOrganizationsMultiSelectComponent_mat_chip_4_Template(rf, ctx) { if (rf & 1) {
    var _r193 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "mat-chip", 7);
    i0.ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_click_0_listener() { i0.ɵɵrestoreView(_r193); var ctx_r192 = i0.ɵɵnextContext(); return ctx_r192.uiAction.openTree(); })("removed", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_removed_0_listener() { i0.ɵɵrestoreView(_r193); var organisation_r191 = ctx.$implicit; var ctx_r194 = i0.ɵɵnextContext(); return ctx_r194.uiAction.organisationRemove(organisation_r191); });
    i0.ɵɵtext(1);
    i0.ɵɵelementStart(2, "mat-icon", 8);
    i0.ɵɵtext(3, "cancel");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var organisation_r191 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1(" ", organisation_r191.name, " ");
} }
// https://stackblitz.com/edit/menu-overlay?file=src%2Fapp%2Fselect-input.component.html
var FofOrganizationsMultiSelectComponent = /** @class */ (function () {
    function FofOrganizationsMultiSelectComponent(elementRef) {
        var _this = this;
        this.elementRef = elementRef;
        this.multiSelect = true;
        this.selectedOrganisations = [];
        this.placeHolder = 'Organisations';
        this.selectedOrganizationsChange = new EventEmitter();
        // All private variables
        this.priVar = {};
        // All private functions
        this.privFunc = {
            refreshOuputs: function () {
                _this.selectedOrganizationsChange.emit(_this.selectedOrganisations);
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            treeMenuVisible: false,
        };
        // All actions shared with UI 
        this.uiAction = {
            openTree: function () {
                _this.uiVar.treeMenuVisible = !_this.uiVar.treeMenuVisible;
            },
            selectedOrganizationsChange: function (selectedOrganisations) {
                if (!selectedOrganisations) {
                    _this.selectedOrganisations = null;
                    return;
                }
                if (Array.isArray(selectedOrganisations)) {
                    _this.selectedOrganisations = selectedOrganisations;
                }
                else {
                    _this.selectedOrganisations = [selectedOrganisations];
                }
                _this.privFunc.refreshOuputs();
            },
            // Remove organisation from chip
            organisationRemove: function (organisationToRemove) {
                _this.selectedOrganisations = _this.selectedOrganisations.filter(function (organisation) { return organisationToRemove.id !== organisation.id; });
                _this.privFunc.refreshOuputs();
            }
        };
    }
    FofOrganizationsMultiSelectComponent.prototype.onDocumentClick = function (event) {
        if (!this.elementRef.nativeElement.contains(event.target)) {
            this.uiVar.treeMenuVisible = false;
        }
    };
    // Angular events
    FofOrganizationsMultiSelectComponent.prototype.ngOnInit = function () {
    };
    FofOrganizationsMultiSelectComponent.ɵfac = function FofOrganizationsMultiSelectComponent_Factory(t) { return new (t || FofOrganizationsMultiSelectComponent)(i0.ɵɵdirectiveInject(i0.ElementRef)); };
    FofOrganizationsMultiSelectComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofOrganizationsMultiSelectComponent, selectors: [["fof-core-fof-organizations-multi-select"]], hostBindings: function FofOrganizationsMultiSelectComponent_HostBindings(rf, ctx) { if (rf & 1) {
            i0.ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_click_HostBindingHandler($event) { return ctx.onDocumentClick($event); }, false, i0.ɵɵresolveDocument);
        } }, inputs: { multiSelect: "multiSelect", selectedOrganisations: "selectedOrganisations", notSelectableOrganizations: "notSelectableOrganizations", placeHolder: "placeHolder" }, outputs: { selectedOrganizationsChange: "selectedOrganizationsChange" }, decls: 9, vars: 8, consts: [[1, "fof-menu-custom"], ["chipList", ""], [3, "click", "removed", 4, "ngFor", "ngForOf"], ["matInput", "", "readonly", "", 3, "placeholder", "matChipInputFor", "click"], [1, "cdk-overlay-pane"], [1, "organization-menu", "mat-menu-panel", "mat-elevation-z4"], [3, "multiSelect", "selectedNodesSynchronize", "notSelectableOrganizations", "selectedOrganizationsChange"], [3, "click", "removed"], ["matChipRemove", ""]], template: function FofOrganizationsMultiSelectComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "mat-form-field");
            i0.ɵɵelementStart(2, "mat-chip-list", null, 1);
            i0.ɵɵtemplate(4, FofOrganizationsMultiSelectComponent_mat_chip_4_Template, 4, 1, "mat-chip", 2);
            i0.ɵɵelementStart(5, "input", 3);
            i0.ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_Template_input_click_5_listener() { return ctx.uiAction.openTree(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 4);
            i0.ɵɵelementStart(7, "div", 5);
            i0.ɵɵelementStart(8, "fof-organizations-tree", 6);
            i0.ɵɵlistener("selectedOrganizationsChange", function FofOrganizationsMultiSelectComponent_Template_fof_organizations_tree_selectedOrganizationsChange_8_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            var _r189 = i0.ɵɵreference(3);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("ngForOf", ctx.selectedOrganisations);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("placeholder", ctx.placeHolder)("matChipInputFor", _r189);
            i0.ɵɵadvance(1);
            i0.ɵɵclassProp("tree-display", ctx.uiVar.treeMenuVisible);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("multiSelect", ctx.multiSelect)("selectedNodesSynchronize", ctx.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
        } }, directives: [i1.MatFormField, i2.MatChipList, i3.NgForOf, i4.MatInput, i2.MatChipInput, i5.FofOrganizationsTreeComponent, i2.MatChip, i6.MatIcon, i2.MatChipRemove], styles: [".fof-menu-custom[_ngcontent-%COMP%]{position:relative}.fof-menu-custom[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]{width:100%;position:absolute;top:60px;display:none}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane.tree-display[_ngcontent-%COMP%]{display:block}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu[_ngcontent-%COMP%]{min-width:112px;overflow:auto;-webkit-overflow-scrolling:touch;border-radius:4px;height:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu.mat-menu-panel[_ngcontent-%COMP%]{max-width:100%;max-height:unset}"] });
    return FofOrganizationsMultiSelectComponent;
}());
export { FofOrganizationsMultiSelectComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofOrganizationsMultiSelectComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-organizations-multi-select',
                templateUrl: './fof-organizations-multi-select.component.html',
                styleUrls: ['./fof-organizations-multi-select.component.scss'],
                host: {
                    '(document:click)': 'onDocumentClick($event)',
                }
            }]
    }], function () { return [{ type: i0.ElementRef }]; }, { multiSelect: [{
            type: Input
        }], selectedOrganisations: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }], placeHolder: [{
            type: Input
        }], selectedOrganizationsChange: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0L2ZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdC5jb21wb25lbnQudHMiLCJsaWIvY29tcG9uZW50cy9mb2Ytb3JnYW5pemF0aW9ucy1tdWx0aS1zZWxlY3QvZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0LmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQXNCLE1BQU0sRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFBOzs7Ozs7Ozs7O0lDR3ZGLG1DQUdDO0lBRkEsMExBQVMsNEJBQW1CLElBQUMsd05BQ2xCLHVEQUF5QyxJQUR2QjtJQUU3QixZQUNBO0lBQUEsbUNBQXdCO0lBQUEsc0JBQU07SUFBQSxpQkFBVztJQUMxQyxpQkFBVzs7O0lBRlYsZUFDQTtJQURBLHVEQUNBOztBREpKLHdGQUF3RjtBQUV4RjtJQXNCRSw4Q0FDVSxVQUFzQjtRQURoQyxpQkFJQztRQUhTLGVBQVUsR0FBVixVQUFVLENBQVk7UUFQdkIsZ0JBQVcsR0FBWSxJQUFJLENBQUE7UUFDM0IsMEJBQXFCLEdBQW9CLEVBQUUsQ0FBQTtRQUUzQyxnQkFBVyxHQUFXLGVBQWUsQ0FBQTtRQUNwQyxnQ0FBMkIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFBO1FBUTFELHdCQUF3QjtRQUNoQixXQUFNLEdBQUcsRUFFaEIsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUc7WUFDakIsYUFBYSxFQUFDO2dCQUNaLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLHFCQUFxQixDQUFDLENBQUE7WUFDbkUsQ0FBQztTQUNGLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsZUFBZSxFQUFFLEtBQUs7U0FFdkIsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsUUFBUSxFQUFDO2dCQUNQLEtBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUE7WUFDMUQsQ0FBQztZQUNELDJCQUEyQixFQUFFLFVBQUMscUJBQXNEO2dCQUNsRixJQUFJLENBQUMscUJBQXFCLEVBQUU7b0JBQzFCLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUE7b0JBQ2pDLE9BQU07aUJBQ1A7Z0JBRUQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFFLEVBQUU7b0JBQ3pDLEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxxQkFBcUIsQ0FBQTtpQkFDbkQ7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLHFCQUFxQixHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQTtpQkFDckQ7Z0JBRUQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsQ0FBQTtZQUMvQixDQUFDO1lBQ0QsZ0NBQWdDO1lBQ2hDLGtCQUFrQixFQUFDLFVBQUMsb0JBQW1DO2dCQUNyRCxLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FDNUQsVUFBQSxZQUFZLElBQUksT0FBQSxvQkFBb0IsQ0FBQyxFQUFFLEtBQUssWUFBWSxDQUFDLEVBQUUsRUFBM0MsQ0FBMkMsQ0FBRSxDQUFBO2dCQUMvRCxLQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFBO1lBQy9CLENBQUM7U0FDRixDQUFBO0lBMUNELENBQUM7SUFoQkQsOERBQWUsR0FBZixVQUFnQixLQUFLO1FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3pELElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQTtTQUNuQztJQUNILENBQUM7SUF1REQsaUJBQWlCO0lBQ2pCLHVEQUFRLEdBQVI7SUFFQSxDQUFDOzRIQWhFVSxvQ0FBb0M7NkVBQXBDLG9DQUFvQzs7O1lDYmpELDhCQUNDO1lBQUEsc0NBQ0M7WUFBQSw4Q0FDQztZQUFBLCtGQUdDO1lBR0QsZ0NBSUQ7WUFERSxnSEFBUyx1QkFBbUIsSUFBQztZQUg5QixpQkFJRDtZQUFBLGlCQUFnQjtZQUNqQixpQkFBaUI7WUFDakIsOEJBRUM7WUFBQSw4QkFDQztZQUFBLGlEQUsyQjtZQUQxQixtTEFBK0IsZ0RBQTRDLElBQUM7WUFDM0UsaUJBQXlCO1lBQzVCLGlCQUFNO1lBQ1AsaUJBQU07WUFDUCxpQkFBTTs7O1lBdkJPLGVBQWtEO1lBQWxELG1EQUFrRDtZQU01QyxlQUEyQjtZQUEzQiw2Q0FBMkIsMEJBQUE7WUFPNUMsZUFBNEM7WUFBNUMseURBQTRDO1lBRzFDLGVBQTJCO1lBQTNCLDZDQUEyQix1REFBQSw4REFBQTs7K0NEbkIvQjtDQThFQyxBQXpFRCxJQXlFQztTQWpFWSxvQ0FBb0M7a0RBQXBDLG9DQUFvQztjQVJoRCxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLHlDQUF5QztnQkFDbkQsV0FBVyxFQUFFLGlEQUFpRDtnQkFDOUQsU0FBUyxFQUFFLENBQUMsaURBQWlELENBQUM7Z0JBQzlELElBQUksRUFBRTtvQkFDSixrQkFBa0IsRUFBRSx5QkFBeUI7aUJBQzlDO2FBQ0Y7O2tCQVNFLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgRWxlbWVudFJlZiwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgaU9yZ2FuaXphdGlvbiB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IEZvZlBlcm1pc3Npb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9mb2YtcGVybWlzc2lvbi5zZXJ2aWNlJ1xyXG4vLyBodHRwczovL3N0YWNrYmxpdHouY29tL2VkaXQvbWVudS1vdmVybGF5P2ZpbGU9c3JjJTJGYXBwJTJGc2VsZWN0LWlucHV0LmNvbXBvbmVudC5odG1sXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1jb3JlLWZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZvZi1vcmdhbml6YXRpb25zLW11bHRpLXNlbGVjdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZm9mLW9yZ2FuaXphdGlvbnMtbXVsdGktc2VsZWN0LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgaG9zdDoge1xyXG4gICAgJyhkb2N1bWVudDpjbGljayknOiAnb25Eb2N1bWVudENsaWNrKCRldmVudCknLFxyXG4gIH1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvZk9yZ2FuaXphdGlvbnNNdWx0aVNlbGVjdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIG9uRG9jdW1lbnRDbGljayhldmVudCkge1xyXG4gICAgaWYgKCF0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5jb250YWlucyhldmVudC50YXJnZXQpKSB7XHJcbiAgICAgIHRoaXMudWlWYXIudHJlZU1lbnVWaXNpYmxlID0gZmFsc2VcclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBJbnB1dCgpIG11bHRpU2VsZWN0OiBib29sZWFuID0gdHJ1ZVxyXG4gIEBJbnB1dCgpIHNlbGVjdGVkT3JnYW5pc2F0aW9uczogaU9yZ2FuaXphdGlvbltdID0gW11cclxuICBASW5wdXQoKSBub3RTZWxlY3RhYmxlT3JnYW5pemF0aW9uczogaU9yZ2FuaXphdGlvbltdXHJcbiAgQElucHV0KCkgcGxhY2VIb2xkZXI6IHN0cmluZyA9ICdPcmdhbmlzYXRpb25zJ1xyXG4gIEBPdXRwdXQoKSBzZWxlY3RlZE9yZ2FuaXphdGlvbnNDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyKClcclxuICBcclxuICBjb25zdHJ1Y3RvciAoXHJcbiAgICBwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWZcclxuICApIHsgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0geyBcclxuICAgIFxyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gICAgcmVmcmVzaE91cHV0czooKSA9PiB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlLmVtaXQodGhpcy5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMpXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgdHJlZU1lbnVWaXNpYmxlOiBmYWxzZSwgICAgXHJcbiAgICAvLyBzZWxlY3RlZE9yZ2FuaXNhdGlvbnM6IDxpT3JnYW5pemF0aW9uW10+W11cclxuICB9XHJcbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpQWN0aW9uID0ge1xyXG4gICAgb3BlblRyZWU6KCkgPT4ge1xyXG4gICAgICB0aGlzLnVpVmFyLnRyZWVNZW51VmlzaWJsZSA9ICF0aGlzLnVpVmFyLnRyZWVNZW51VmlzaWJsZVxyXG4gICAgfSxcclxuICAgIHNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZTogKHNlbGVjdGVkT3JnYW5pc2F0aW9uczogaU9yZ2FuaXphdGlvbltdIHwgaU9yZ2FuaXphdGlvbikgPT4geyAgICAgIFxyXG4gICAgICBpZiAoIXNlbGVjdGVkT3JnYW5pc2F0aW9ucykge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcmdhbmlzYXRpb25zID0gbnVsbFxyXG4gICAgICAgIHJldHVyblxyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheShzZWxlY3RlZE9yZ2FuaXNhdGlvbnMgKSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRPcmdhbmlzYXRpb25zID0gc2VsZWN0ZWRPcmdhbmlzYXRpb25zICBcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkT3JnYW5pc2F0aW9ucyA9IFtzZWxlY3RlZE9yZ2FuaXNhdGlvbnNdXHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIHRoaXMucHJpdkZ1bmMucmVmcmVzaE91cHV0cygpXHJcbiAgICB9LCAgIFxyXG4gICAgLy8gUmVtb3ZlIG9yZ2FuaXNhdGlvbiBmcm9tIGNoaXBcclxuICAgIG9yZ2FuaXNhdGlvblJlbW92ZToob3JnYW5pc2F0aW9uVG9SZW1vdmU6IGlPcmdhbml6YXRpb24pID0+IHtcclxuICAgICAgdGhpcy5zZWxlY3RlZE9yZ2FuaXNhdGlvbnMgPSB0aGlzLnNlbGVjdGVkT3JnYW5pc2F0aW9ucy5maWx0ZXIgKFxyXG4gICAgICAgIG9yZ2FuaXNhdGlvbiA9PiBvcmdhbmlzYXRpb25Ub1JlbW92ZS5pZCAhPT0gb3JnYW5pc2F0aW9uLmlkIClcclxuICAgICAgdGhpcy5wcml2RnVuYy5yZWZyZXNoT3VwdXRzKClcclxuICAgIH1cclxuICB9XHJcbiAgLy8gQW5ndWxhciBldmVudHNcclxuICBuZ09uSW5pdCgpIHtcclxuICBcclxuICB9XHJcbn1cclxuIiwiPGRpdiBjbGFzcz1cImZvZi1tZW51LWN1c3RvbVwiPlx0XHJcblx0PG1hdC1mb3JtLWZpZWxkPlxyXG5cdFx0PG1hdC1jaGlwLWxpc3QgI2NoaXBMaXN0Plx0XHJcblx0XHRcdDxtYXQtY2hpcCAqbmdGb3I9XCJsZXQgb3JnYW5pc2F0aW9uIG9mIHNlbGVjdGVkT3JnYW5pc2F0aW9uc1wiXHRcdFx0XHRcclxuXHRcdFx0XHQoY2xpY2spPVwidWlBY3Rpb24ub3BlblRyZWUoKVwiXHRcdFx0XHRcclxuXHRcdFx0XHQocmVtb3ZlZCk9XCJ1aUFjdGlvbi5vcmdhbmlzYXRpb25SZW1vdmUob3JnYW5pc2F0aW9uKVwiPlxyXG5cdFx0XHRcdHt7IG9yZ2FuaXNhdGlvbi5uYW1lIH19XHJcblx0XHRcdFx0PG1hdC1pY29uIG1hdENoaXBSZW1vdmU+Y2FuY2VsPC9tYXQtaWNvbj5cclxuXHRcdFx0PC9tYXQtY2hpcD5cdFx0XHRcclxuXHRcdFx0PGlucHV0IG1hdElucHV0IFtwbGFjZWhvbGRlcl09XCJwbGFjZUhvbGRlclwiXHJcblx0XHRcdFx0cmVhZG9ubHlcclxuXHRcdFx0XHRbbWF0Q2hpcElucHV0Rm9yXT1cImNoaXBMaXN0XCJcclxuXHRcdFx0XHQoY2xpY2spPVwidWlBY3Rpb24ub3BlblRyZWUoKVwiPlxyXG5cdFx0PC9tYXQtY2hpcC1saXN0PlxyXG5cdDwvbWF0LWZvcm0tZmllbGQ+XHJcblx0PGRpdiBjbGFzcz1cImNkay1vdmVybGF5LXBhbmVcIlxyXG5cdFx0W2NsYXNzLnRyZWUtZGlzcGxheV09XCJ1aVZhci50cmVlTWVudVZpc2libGVcIj5cclxuXHRcdDxkaXYgY2xhc3M9XCJvcmdhbml6YXRpb24tbWVudSBtYXQtbWVudS1wYW5lbCBtYXQtZWxldmF0aW9uLXo0XCI+XHJcblx0XHRcdDxmb2Ytb3JnYW5pemF0aW9ucy10cmVlXHJcblx0XHRcdFx0W211bHRpU2VsZWN0XT1cIm11bHRpU2VsZWN0XCJcclxuXHRcdFx0XHRbc2VsZWN0ZWROb2Rlc1N5bmNocm9uaXplXT1cInNlbGVjdGVkT3JnYW5pc2F0aW9uc1wiXHJcblx0XHRcdFx0W25vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zXT1cIm5vdFNlbGVjdGFibGVPcmdhbml6YXRpb25zXCJcclxuXHRcdFx0XHQoc2VsZWN0ZWRPcmdhbml6YXRpb25zQ2hhbmdlKT1cInVpQWN0aW9uLnNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZSgkZXZlbnQpXCJcclxuXHRcdFx0XHQ+PC9mb2Ytb3JnYW5pemF0aW9ucy10cmVlPlxyXG5cdFx0PC9kaXY+XHJcblx0PC9kaXY+XHJcbjwvZGl2PiJdfQ==
import { __extends } from "tslib";
import { Injectable, ErrorHandler, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "./notification/notification.service";
/** Application-wide error handler that adds a UI notification to the error handling
 * provided by the default Angular ErrorHandler.
 */
var fofErrorHandler = /** @class */ (function (_super) {
    __extends(fofErrorHandler, _super);
    function fofErrorHandler(notificationsService, fofConfig) {
        var _this = _super.call(this) || this;
        _this.notificationsService = notificationsService;
        _this.fofConfig = fofConfig;
        return _this;
    }
    // handleError(error: Error | HttpErrorResponse | UnhandledRejection) {
    fofErrorHandler.prototype.handleError = function (exception) {
        var displayMessage = "Oops, nous avons une erreur...";
        var error;
        // if (exception instanceof HttpErrorResponse) {
        //   console.log('HttpErrorResponse', exception)
        //   error = exception.error
        //   if (error.fofCoreException) { 
        //     console.log('fof core exception', exception)
        //   }
        // }   
        // ToDO: manage promise unhandledrejection -> error in code
        // https://javascript.info/promise-error-handling
        if (exception.promise) {
            displayMessage = "<small>Technical notice</small><br>\n        Forgot to manage a promise rejection...<br>\n        <small>(yeah we know, it's funny ;)</small>";
            console.info("fofErrorHandler - Promises not catched!");
        }
        // console.error('fofErrorHandler - Common error', exception)
        this.notificationsService.error(displayMessage, { mustDisappearAfter: -1 });
        _super.prototype.handleError.call(this, exception);
    };
    fofErrorHandler.ɵfac = function fofErrorHandler_Factory(t) { return new (t || fofErrorHandler)(i0.ɵɵinject(i1.FofNotificationService), i0.ɵɵinject(CORE_CONFIG)); };
    fofErrorHandler.ɵprov = i0.ɵɵdefineInjectable({ token: fofErrorHandler, factory: fofErrorHandler.ɵfac });
    return fofErrorHandler;
}(ErrorHandler));
export { fofErrorHandler };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(fofErrorHandler, [{
        type: Injectable
    }], function () { return [{ type: i1.FofNotificationService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWVycm9yLWhhbmRsZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYXBwLWVycm9yLWhhbmRsZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBRWhFLE9BQU8sRUFBYyxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUE7OztBQUl2RDs7R0FFRztBQUNIO0lBQ3FDLG1DQUFZO0lBRS9DLHlCQUNVLG9CQUE0QyxFQUN2QixTQUFxQjtRQUZwRCxZQUlFLGlCQUFPLFNBQ1I7UUFKUywwQkFBb0IsR0FBcEIsb0JBQW9CLENBQXdCO1FBQ3ZCLGVBQVMsR0FBVCxTQUFTLENBQVk7O0lBR3BELENBQUM7SUFFRCx1RUFBdUU7SUFDdkUscUNBQVcsR0FBWCxVQUFZLFNBQWM7UUFDeEIsSUFBSSxjQUFjLEdBQUcsZ0NBQWdDLENBQUE7UUFDckQsSUFBSSxLQUFLLENBQUE7UUFFVCxnREFBZ0Q7UUFDaEQsZ0RBQWdEO1FBQ2hELDRCQUE0QjtRQUM1QixtQ0FBbUM7UUFDbkMsbURBQW1EO1FBQ25ELE1BQU07UUFDTixPQUFPO1FBR1AsMkRBQTJEO1FBQzNELGlEQUFpRDtRQUNqRCxJQUFJLFNBQVMsQ0FBQyxPQUFPLEVBQUU7WUFDckIsY0FBYyxHQUFHLCtJQUU2QixDQUFBO1lBQzlDLE9BQU8sQ0FBQyxJQUFJLENBQUMseUNBQXlDLENBQUMsQ0FBQTtTQUN4RDtRQUVELDZEQUE2RDtRQUU3RCxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxFQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQTtRQUV6RSxpQkFBTSxXQUFXLFlBQUMsU0FBUyxDQUFDLENBQUE7SUFDOUIsQ0FBQztrRkFyQ1UsZUFBZSxzREFJaEIsV0FBVzsyREFKVixlQUFlLFdBQWYsZUFBZTswQkFWNUI7Q0FnREMsQUF2Q0QsQ0FDcUMsWUFBWSxHQXNDaEQ7U0F0Q1ksZUFBZTtrREFBZixlQUFlO2NBRDNCLFVBQVU7O3NCQUtOLE1BQU07dUJBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEVycm9ySGFuZGxlciwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcclxuaW1wb3J0IHsgSWZvZkNvbmZpZywgQ09SRV9DT05GSUcgfSBmcm9tICcuLi9mb2YtY29uZmlnJ1xyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcblxyXG4vKiogQXBwbGljYXRpb24td2lkZSBlcnJvciBoYW5kbGVyIHRoYXQgYWRkcyBhIFVJIG5vdGlmaWNhdGlvbiB0byB0aGUgZXJyb3IgaGFuZGxpbmdcclxuICogcHJvdmlkZWQgYnkgdGhlIGRlZmF1bHQgQW5ndWxhciBFcnJvckhhbmRsZXIuXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBmb2ZFcnJvckhhbmRsZXIgZXh0ZW5kcyBFcnJvckhhbmRsZXIge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgbm90aWZpY2F0aW9uc1NlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBASW5qZWN0KENPUkVfQ09ORklHKSBwcml2YXRlIGZvZkNvbmZpZzogSWZvZkNvbmZpZ1xyXG4gICkgeyAgICBcclxuICAgIHN1cGVyKCkgICAgXHJcbiAgfVxyXG5cclxuICAvLyBoYW5kbGVFcnJvcihlcnJvcjogRXJyb3IgfCBIdHRwRXJyb3JSZXNwb25zZSB8IFVuaGFuZGxlZFJlamVjdGlvbikge1xyXG4gIGhhbmRsZUVycm9yKGV4Y2VwdGlvbjogYW55KSB7ICAgIFxyXG4gICAgbGV0IGRpc3BsYXlNZXNzYWdlID0gYE9vcHMsIG5vdXMgYXZvbnMgdW5lIGVycmV1ci4uLmBcclxuICAgIGxldCBlcnJvclxyXG5cclxuICAgIC8vIGlmIChleGNlcHRpb24gaW5zdGFuY2VvZiBIdHRwRXJyb3JSZXNwb25zZSkge1xyXG4gICAgLy8gICBjb25zb2xlLmxvZygnSHR0cEVycm9yUmVzcG9uc2UnLCBleGNlcHRpb24pXHJcbiAgICAvLyAgIGVycm9yID0gZXhjZXB0aW9uLmVycm9yXHJcbiAgICAvLyAgIGlmIChlcnJvci5mb2ZDb3JlRXhjZXB0aW9uKSB7IFxyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKCdmb2YgY29yZSBleGNlcHRpb24nLCBleGNlcHRpb24pXHJcbiAgICAvLyAgIH1cclxuICAgIC8vIH0gICBcclxuICAgIFxyXG4gICAgXHJcbiAgICAvLyBUb0RPOiBtYW5hZ2UgcHJvbWlzZSB1bmhhbmRsZWRyZWplY3Rpb24gLT4gZXJyb3IgaW4gY29kZVxyXG4gICAgLy8gaHR0cHM6Ly9qYXZhc2NyaXB0LmluZm8vcHJvbWlzZS1lcnJvci1oYW5kbGluZ1xyXG4gICAgaWYgKGV4Y2VwdGlvbi5wcm9taXNlKSB7IFxyXG4gICAgICBkaXNwbGF5TWVzc2FnZSA9IGA8c21hbGw+VGVjaG5pY2FsIG5vdGljZTwvc21hbGw+PGJyPlxyXG4gICAgICAgIEZvcmdvdCB0byBtYW5hZ2UgYSBwcm9taXNlIHJlamVjdGlvbi4uLjxicj5cclxuICAgICAgICA8c21hbGw+KHllYWggd2Uga25vdywgaXQncyBmdW5ueSA7KTwvc21hbGw+YFxyXG4gICAgICBjb25zb2xlLmluZm8oYGZvZkVycm9ySGFuZGxlciAtIFByb21pc2VzIG5vdCBjYXRjaGVkIWApXHJcbiAgICB9XHJcblxyXG4gICAgLy8gY29uc29sZS5lcnJvcignZm9mRXJyb3JIYW5kbGVyIC0gQ29tbW9uIGVycm9yJywgZXhjZXB0aW9uKVxyXG5cclxuICAgIHRoaXMubm90aWZpY2F0aW9uc1NlcnZpY2UuZXJyb3IoZGlzcGxheU1lc3NhZ2UsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IC0xfSlcclxuXHJcbiAgICBzdXBlci5oYW5kbGVFcnJvcihleGNlcHRpb24pXHJcbiAgfVxyXG59XHJcbiJdfQ==
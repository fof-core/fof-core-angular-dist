import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG, eAuth } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./auth.service";
import * as i3 from "./notification/notification.service";
var FofAuthGuard = /** @class */ (function () {
    function FofAuthGuard(router, foFAuthService, fofNotificationService, fofConfig) {
        this.router = router;
        this.foFAuthService = foFAuthService;
        this.fofNotificationService = fofNotificationService;
        this.fofConfig = fofConfig;
    }
    FofAuthGuard.prototype.canActivate = function (next, state) {
        // for permissions in route 
        // https://jasonwatmore.com/post/2019/08/06/angular-8-role-based-authorization-tutorial-with-example#tsconfig-json
        var currentUser = this.foFAuthService.currentUser;
        var notAuthentifiedRoute = '/login';
        if (this.fofConfig.authentication) {
            if (this.fofConfig.authentication.type == eAuth.windows) {
                notAuthentifiedRoute = 'loading';
            }
        }
        if (currentUser) {
            // check if route is restricted by permission      
            // get the deepest root data
            // https://stackoverflow.com/questions/43806188/how-can-i-access-an-activated-child-routes-data-from-the-parent-routes-compone
            var route = next;
            while (route.firstChild) {
                route = route.firstChild;
            }
            var data_1 = route.data;
            if (data_1.permissions) {
                var found = Object.values(currentUser.permissions).some(function (r) { return data_1.permissions.indexOf(r) >= 0; });
                if (!found) {
                    this.fofNotificationService.error("Vous n'avez pas les droit suffisant<br>\n            pour naviguer vers " + state.url);
                    this.router.navigate(['/']);
                    return false;
                }
            }
            // authorised
            return true;
        }
        // not logged in. Redirect to then notAuthentified page with the url to return on
        this.router.navigate([notAuthentifiedRoute], { queryParams: { returnUrl: state.url } });
        return false;
    };
    FofAuthGuard.ɵfac = function FofAuthGuard_Factory(t) { return new (t || FofAuthGuard)(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.FoFAuthService), i0.ɵɵinject(i3.FofNotificationService), i0.ɵɵinject(CORE_CONFIG)); };
    FofAuthGuard.ɵprov = i0.ɵɵdefineInjectable({ token: FofAuthGuard, factory: FofAuthGuard.ɵfac, providedIn: 'root' });
    return FofAuthGuard;
}());
export { FofAuthGuard };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofAuthGuard, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.Router }, { type: i2.FoFAuthService }, { type: i3.FofNotificationService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5ndWFyZC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvYXV0aC5ndWFyZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUtuRCxPQUFPLEVBQWMsV0FBVyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQTs7Ozs7QUFFOUQ7SUFLRSxzQkFDVSxNQUFjLEVBQ2QsY0FBOEIsRUFDOUIsc0JBQThDLEVBQ3pCLFNBQXFCO1FBSDFDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUN6QixjQUFTLEdBQVQsU0FBUyxDQUFZO0lBQ2hELENBQUM7SUFFTCxrQ0FBVyxHQUFYLFVBQ0UsSUFBNEIsRUFDNUIsS0FBMEI7UUFDMUIsNEJBQTRCO1FBQzVCLGtIQUFrSDtRQUNsSCxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQTtRQUNuRCxJQUFJLG9CQUFvQixHQUFHLFFBQVEsQ0FBQTtRQUduQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFO1lBQ2pDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ3ZELG9CQUFvQixHQUFHLFNBQVMsQ0FBQTthQUNqQztTQUNGO1FBRUQsSUFBSSxXQUFXLEVBQUU7WUFDZixtREFBbUQ7WUFDbkQsNEJBQTRCO1lBQzVCLDhIQUE4SDtZQUM5SCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7WUFBQyxPQUFPLEtBQUssQ0FBQyxVQUFVLEVBQUU7Z0JBQUUsS0FBSyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUE7YUFBRTtZQUN2RSxJQUFNLE1BQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFBO1lBRXZCLElBQUksTUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDcEIsSUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBUyxJQUFJLE9BQUEsTUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFoQyxDQUFnQyxDQUFDLENBQUE7Z0JBQ3pHLElBQUksQ0FBQyxLQUFLLEVBQUU7b0JBQ1YsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyw2RUFDWCxLQUFLLENBQUMsR0FBSyxDQUFDLENBQUE7b0JBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtvQkFDM0IsT0FBTyxLQUFLLENBQUE7aUJBQ2I7YUFDRjtZQUNELGFBQWE7WUFDYixPQUFPLElBQUksQ0FBQTtTQUNaO1FBRUQsaUZBQWlGO1FBQ2pGLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsb0JBQW9CLENBQUMsRUFBRSxFQUFFLFdBQVcsRUFBRSxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFBO1FBQ3ZGLE9BQU8sS0FBSyxDQUFBO0lBQ2QsQ0FBQzs0RUEvQ1UsWUFBWSw4R0FNYixXQUFXO3dEQU5WLFlBQVksV0FBWixZQUFZLG1CQUZYLE1BQU07dUJBUnBCO0NBMkRDLEFBcERELElBb0RDO1NBakRZLFlBQVk7a0RBQVosWUFBWTtjQUh4QixVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7O3NCQU9JLE1BQU07dUJBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgVXJsVHJlZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UgfSBmcm9tICcuL2F1dGguc2VydmljZSdcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBJZm9mQ29uZmlnLCBDT1JFX0NPTkZJRywgZUF1dGggfSBmcm9tICcuLi9mb2YtY29uZmlnJ1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9mQXV0aEd1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIGZvRkF1dGhTZXJ2aWNlOiBGb0ZBdXRoU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIEBJbmplY3QoQ09SRV9DT05GSUcpIHByaXZhdGUgZm9mQ29uZmlnOiBJZm9mQ29uZmlnXHJcbiAgKSB7IH1cclxuXHJcbiAgY2FuQWN0aXZhdGUoXHJcbiAgICBuZXh0OiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCAgICBcclxuICAgIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogT2JzZXJ2YWJsZTxib29sZWFuIHwgVXJsVHJlZT4gfCBQcm9taXNlPGJvb2xlYW4gfCBVcmxUcmVlPiB8IGJvb2xlYW4gfCBVcmxUcmVlIHtcclxuICAgIC8vIGZvciBwZXJtaXNzaW9ucyBpbiByb3V0ZSBcclxuICAgIC8vIGh0dHBzOi8vamFzb253YXRtb3JlLmNvbS9wb3N0LzIwMTkvMDgvMDYvYW5ndWxhci04LXJvbGUtYmFzZWQtYXV0aG9yaXphdGlvbi10dXRvcmlhbC13aXRoLWV4YW1wbGUjdHNjb25maWctanNvblxyXG4gICAgY29uc3QgY3VycmVudFVzZXIgPSB0aGlzLmZvRkF1dGhTZXJ2aWNlLmN1cnJlbnRVc2VyXHJcbiAgICBsZXQgbm90QXV0aGVudGlmaWVkUm91dGUgPSAnL2xvZ2luJ1xyXG4gICAgXHJcbiAgICBcclxuICAgIGlmICh0aGlzLmZvZkNvbmZpZy5hdXRoZW50aWNhdGlvbikge1xyXG4gICAgICBpZiAodGhpcy5mb2ZDb25maWcuYXV0aGVudGljYXRpb24udHlwZSA9PSBlQXV0aC53aW5kb3dzKSB7XHJcbiAgICAgICAgbm90QXV0aGVudGlmaWVkUm91dGUgPSAnbG9hZGluZydcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmIChjdXJyZW50VXNlcikge1xyXG4gICAgICAvLyBjaGVjayBpZiByb3V0ZSBpcyByZXN0cmljdGVkIGJ5IHBlcm1pc3Npb24gICAgICBcclxuICAgICAgLy8gZ2V0IHRoZSBkZWVwZXN0IHJvb3QgZGF0YVxyXG4gICAgICAvLyBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL3F1ZXN0aW9ucy80MzgwNjE4OC9ob3ctY2FuLWktYWNjZXNzLWFuLWFjdGl2YXRlZC1jaGlsZC1yb3V0ZXMtZGF0YS1mcm9tLXRoZS1wYXJlbnQtcm91dGVzLWNvbXBvbmVcclxuICAgICAgbGV0IHJvdXRlID0gbmV4dDsgd2hpbGUgKHJvdXRlLmZpcnN0Q2hpbGQpIHsgcm91dGUgPSByb3V0ZS5maXJzdENoaWxkIH1cclxuICAgICAgY29uc3QgZGF0YSA9IHJvdXRlLmRhdGFcclxuXHJcbiAgICAgIGlmIChkYXRhLnBlcm1pc3Npb25zKSB7XHJcbiAgICAgICAgY29uc3QgZm91bmQgPSBPYmplY3QudmFsdWVzKGN1cnJlbnRVc2VyLnBlcm1pc3Npb25zKS5zb21lKChyOiBzdHJpbmcpPT4gZGF0YS5wZXJtaXNzaW9ucy5pbmRleE9mKHIpID49IDApXHJcbiAgICAgICAgaWYgKCFmb3VuZCkge1xyXG4gICAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKGBWb3VzIG4nYXZleiBwYXMgbGVzIGRyb2l0IHN1ZmZpc2FudDxicj5cclxuICAgICAgICAgICAgcG91ciBuYXZpZ3VlciB2ZXJzICR7c3RhdGUudXJsfWApXHJcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy8nXSlcclxuICAgICAgICAgIHJldHVybiBmYWxzZVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICAvLyBhdXRob3Jpc2VkXHJcbiAgICAgIHJldHVybiB0cnVlXHJcbiAgICB9XHJcblxyXG4gICAgLy8gbm90IGxvZ2dlZCBpbi4gUmVkaXJlY3QgdG8gdGhlbiBub3RBdXRoZW50aWZpZWQgcGFnZSB3aXRoIHRoZSB1cmwgdG8gcmV0dXJuIG9uXHJcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbbm90QXV0aGVudGlmaWVkUm91dGVdLCB7IHF1ZXJ5UGFyYW1zOiB7IHJldHVyblVybDogc3RhdGUudXJsIH0gfSlcclxuICAgIHJldHVybiBmYWxzZVxyXG4gIH1cclxuICBcclxufVxyXG4iXX0=
import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "@angular/router";
var FoFAuthService = /** @class */ (function () {
    function FoFAuthService(fofConfig, httpClient, router) {
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.router = router;
        this.environment = this.fofConfig.environment;
        this.currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser$ = this.currentUserSubject.asObservable();
    }
    Object.defineProperty(FoFAuthService.prototype, "currentUser", {
        get: function () {
            if (this.currentUserSubject) {
                return this.currentUserSubject.value;
            }
            return null;
        },
        enumerable: true,
        configurable: true
    });
    FoFAuthService.prototype.refreshByCookie = function () {
        var _this = this;
        return this.httpClient.get(this.environment.apiPath + "/auth/getUser")
            .pipe(map(function (user) {
            if (user && user.accessToken) {
                _this.currentUserSubject.next(user);
                // this.currentUserSubject.complete()         
            }
            return user;
        }));
    };
    FoFAuthService.prototype.loginKerberos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.httpClient.get(_this.environment.authPath + "/auth/winlogin", { withCredentials: true })
                .toPromise()
                .then(function (user) {
                // give the temporary windows token to do the next get        
                if (user && user.accessToken) {
                    _this.currentUserSubject.next(user);
                }
                _this.httpClient.get(_this.environment.apiPath + "/auth/winUserValidate")
                    .toPromise()
                    .then(function (user) {
                    // get the final user. 
                    // Can work only if the windows user are registered into the App.
                    // if not, it means the user is just authentified against MS AD
                    if (user && user.accessToken) {
                        _this.currentUserSubject.next(user);
                    }
                    _this.currentUserSubject.complete();
                    resolve(user);
                })
                    .catch(function (error) {
                    reject(error);
                });
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    FoFAuthService.prototype.login = function (username, password) {
        var _this = this;
        var data = {
            username: username,
            password: password
        };
        return this.httpClient.post(this.environment.apiPath + "/auth/login", data)
            .pipe(map(function (user) {
            // login successful if there's a jwt token in the response         
            if (user && user.accessToken) {
                // There is a cookie with the jwt inside for managing web browser refresh            
                _this.currentUserSubject.next(user);
            }
            return user;
        }));
    };
    FoFAuthService.prototype.logOut = function () {
        var _this = this;
        // remove cookie to log user out    
        return this.httpClient.get(this.environment.apiPath + "/auth/logout")
            .pipe(map(function (cookieDeleted) {
            if (cookieDeleted) {
                _this.currentUserSubject.next(null);
                _this.router.navigate(['/login']);
            }
            return cookieDeleted;
        }));
        // .subscribe(
        //   cookieDeleted => {
        //     if (cookieDeleted) {
        //       this.currentUserSubject.next(null)    
        //       this.router.navigate(['/login'])        
        //     }
        //   },
        //   error => {
        //     throw error
        //   })    
    };
    FoFAuthService.ɵfac = function FoFAuthService_Factory(t) { return new (t || FoFAuthService)(i0.ɵɵinject(CORE_CONFIG), i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.Router)); };
    FoFAuthService.ɵprov = i0.ɵɵdefineInjectable({ token: FoFAuthService, factory: FoFAuthService.ɵfac, providedIn: 'root' });
    return FoFAuthService;
}());
export { FoFAuthService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFAuthService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: i1.HttpClient }, { type: i2.Router }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9hdXRoLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFHbEQsT0FBTyxFQUFFLGVBQWUsRUFBYyxNQUFNLE1BQU0sQ0FBQTtBQUNsRCxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUE7QUFDcEMsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7OztBQUd2RDtJQUtFLHdCQUMrQixTQUFxQixFQUMxQyxVQUFzQixFQUN0QixNQUFjO1FBRk8sY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUMxQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFFdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQTtRQUM3QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxlQUFlLENBQWEsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUMxRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtJQUM1RCxDQUFDO0lBTUQsc0JBQVcsdUNBQVc7YUFBdEI7WUFDRSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDM0IsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFBO2FBQ3JDO1lBQ0QsT0FBTyxJQUFJLENBQUE7UUFDYixDQUFDOzs7T0FBQTtJQUVELHdDQUFlLEdBQWY7UUFBQSxpQkFTQztRQVJDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWdCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxrQkFBZSxDQUFDO2FBQy9FLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFnQjtZQUN6QixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUM1QixLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO2dCQUNsQyw4Q0FBOEM7YUFDL0M7WUFDRCxPQUFPLElBQUksQ0FBQTtRQUNiLENBQUMsQ0FBQyxDQUFDLENBQUE7SUFDUCxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUFBLGlCQTZCQztRQTVCQyxPQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDakMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUksS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLG1CQUFnQixFQUFFLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRSxDQUFDO2lCQUMzRixTQUFTLEVBQUU7aUJBQ1gsSUFBSSxDQUFDLFVBQUMsSUFBZ0I7Z0JBQ3JCLDhEQUE4RDtnQkFDOUQsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtvQkFDNUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtpQkFDbkM7Z0JBQ0QsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWdCLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTywwQkFBdUIsQ0FBQztxQkFDbEYsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxVQUFDLElBQWdCO29CQUNyQix1QkFBdUI7b0JBQ3ZCLGlFQUFpRTtvQkFDakUsK0RBQStEO29CQUMvRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO3dCQUM1QixLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO3FCQUNuQztvQkFDRCxLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLENBQUE7b0JBQ2xDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDZixDQUFDLENBQUM7cUJBQ0QsS0FBSyxDQUFDLFVBQUEsS0FBSztvQkFDVixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7Z0JBQ2YsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDLENBQUM7aUJBQ0QsS0FBSyxDQUFDLFVBQUEsS0FBSztnQkFDVixNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDZixDQUFDLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUdELDhCQUFLLEdBQUwsVUFBTSxRQUFnQixFQUFFLFFBQWdCO1FBQXhDLGlCQWlCQztRQWZDLElBQU0sSUFBSSxHQUFHO1lBQ1gsUUFBUSxFQUFFLFFBQVE7WUFDbEIsUUFBUSxFQUFFLFFBQVE7U0FDbkIsQ0FBQTtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQWdCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxnQkFBYSxFQUFFLElBQUksQ0FBQzthQUNwRixJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBZ0I7WUFDekIsbUVBQW1FO1lBQ25FLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQzVCLHFGQUFxRjtnQkFDckYsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTthQUNuQztZQUVELE9BQU8sSUFBSSxDQUFBO1FBQ2IsQ0FBQyxDQUFDLENBQUMsQ0FBQTtJQUNQLENBQUM7SUFFRCwrQkFBTSxHQUFOO1FBQUEsaUJBc0JDO1FBckJDLG9DQUFvQztRQUNwQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFhLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxpQkFBYyxDQUFDO2FBQ3JFLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQyxhQUFzQjtZQUMvQixJQUFJLGFBQWEsRUFBRTtnQkFDakIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtnQkFDbEMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFBO2FBQ2pDO1lBQ0QsT0FBTyxhQUFhLENBQUE7UUFDdEIsQ0FBQyxDQUFDLENBQUMsQ0FBQTtRQUdYLGNBQWM7UUFDZCx1QkFBdUI7UUFDdkIsMkJBQTJCO1FBQzNCLCtDQUErQztRQUMvQyxpREFBaUQ7UUFDakQsUUFBUTtRQUNSLE9BQU87UUFDUCxlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLFdBQVc7SUFDYixDQUFDO2dGQTNHVSxjQUFjLGNBR2YsV0FBVzswREFIVixjQUFjLFdBQWQsY0FBYyxtQkFGYixNQUFNO3lCQVRwQjtDQXNJQyxBQTlIRCxJQThIQztTQTNIWSxjQUFjO2tEQUFkLGNBQWM7Y0FIMUIsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COztzQkFJSSxNQUFNO3VCQUFDLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IElmb2ZDb25maWcsIENPUkVfQ09ORklHIH0gZnJvbSAnLi4vZm9mLWNvbmZpZydcclxuaW1wb3J0IHsgaVVzZXJMb2dpbiB9IGZyb20gJy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb0ZBdXRoU2VydmljZSB7XHJcbiAgXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KENPUkVfQ09ORklHKSBwcml2YXRlIGZvZkNvbmZpZzogSWZvZkNvbmZpZyxcclxuICAgIHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcclxuICApIHsgXHJcbiAgICB0aGlzLmVudmlyb25tZW50ID0gdGhpcy5mb2ZDb25maWcuZW52aXJvbm1lbnRcclxuICAgIHRoaXMuY3VycmVudFVzZXJTdWJqZWN0ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxpVXNlckxvZ2luPihKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdjdXJyZW50VXNlcicpKSlcclxuICAgIHRoaXMuY3VycmVudFVzZXIkID0gdGhpcy5jdXJyZW50VXNlclN1YmplY3QuYXNPYnNlcnZhYmxlKCkgICAgXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGN1cnJlbnRVc2VyU3ViamVjdDogQmVoYXZpb3JTdWJqZWN0PGlVc2VyTG9naW4+XHJcbiAgcHJpdmF0ZSBlbnZpcm9ubWVudDphbnlcclxuICBwdWJsaWMgY3VycmVudFVzZXIkOiBPYnNlcnZhYmxlPGlVc2VyTG9naW4+XHJcblxyXG4gIHB1YmxpYyBnZXQgY3VycmVudFVzZXIoKTogaVVzZXJMb2dpbiB7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50VXNlclN1YmplY3QpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY3VycmVudFVzZXJTdWJqZWN0LnZhbHVlXHJcbiAgICB9XHJcbiAgICByZXR1cm4gbnVsbFxyXG4gIH1cclxuXHJcbiAgcmVmcmVzaEJ5Q29va2llKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8aVVzZXJMb2dpbj4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS9hdXRoL2dldFVzZXJgKVxyXG4gICAgICAucGlwZShtYXAoKHVzZXI6IGlVc2VyTG9naW4pID0+IHsgICAgICAgICAgICAgIFxyXG4gICAgICAgIGlmICh1c2VyICYmIHVzZXIuYWNjZXNzVG9rZW4pIHsgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QubmV4dCh1c2VyKSAgIFxyXG4gICAgICAgICAgLy8gdGhpcy5jdXJyZW50VXNlclN1YmplY3QuY29tcGxldGUoKSAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdXNlclxyXG4gICAgICB9KSlcclxuICB9XHJcblxyXG4gIGxvZ2luS2VyYmVyb3MoKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICB0aGlzLmh0dHBDbGllbnQuZ2V0KGAke3RoaXMuZW52aXJvbm1lbnQuYXV0aFBhdGh9L2F1dGgvd2lubG9naW5gLCB7IHdpdGhDcmVkZW50aWFsczogdHJ1ZSB9KVxyXG4gICAgICAudG9Qcm9taXNlKClcclxuICAgICAgLnRoZW4oKHVzZXI6IGlVc2VyTG9naW4pID0+IHtcclxuICAgICAgICAvLyBnaXZlIHRoZSB0ZW1wb3Jhcnkgd2luZG93cyB0b2tlbiB0byBkbyB0aGUgbmV4dCBnZXQgICAgICAgIFxyXG4gICAgICAgIGlmICh1c2VyICYmIHVzZXIuYWNjZXNzVG9rZW4pIHsgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QubmV4dCh1c2VyKSAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmh0dHBDbGllbnQuZ2V0PGlVc2VyTG9naW4+KGAke3RoaXMuZW52aXJvbm1lbnQuYXBpUGF0aH0vYXV0aC93aW5Vc2VyVmFsaWRhdGVgKSAgICAgIFxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKCh1c2VyOiBpVXNlckxvZ2luKSA9PiB7XHJcbiAgICAgICAgICAvLyBnZXQgdGhlIGZpbmFsIHVzZXIuIFxyXG4gICAgICAgICAgLy8gQ2FuIHdvcmsgb25seSBpZiB0aGUgd2luZG93cyB1c2VyIGFyZSByZWdpc3RlcmVkIGludG8gdGhlIEFwcC5cclxuICAgICAgICAgIC8vIGlmIG5vdCwgaXQgbWVhbnMgdGhlIHVzZXIgaXMganVzdCBhdXRoZW50aWZpZWQgYWdhaW5zdCBNUyBBRFxyXG4gICAgICAgICAgaWYgKHVzZXIgJiYgdXNlci5hY2Nlc3NUb2tlbikgeyAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJTdWJqZWN0Lm5leHQodXNlcikgICAgICAgICAgICBcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJTdWJqZWN0LmNvbXBsZXRlKClcclxuICAgICAgICAgIHJlc29sdmUodXNlcikgICAgICAgICAgXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuY2F0Y2goZXJyb3IgPT4ge1xyXG4gICAgICAgICAgcmVqZWN0KGVycm9yKVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0pXHJcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XHJcbiAgICAgICAgcmVqZWN0KGVycm9yKVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9XHJcbiAgXHJcblxyXG4gIGxvZ2luKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpIHtcclxuICAgIFxyXG4gICAgY29uc3QgZGF0YSA9IHtcclxuICAgICAgdXNlcm5hbWU6IHVzZXJuYW1lLCBcclxuICAgICAgcGFzc3dvcmQ6IHBhc3N3b3JkXHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0PGlVc2VyTG9naW4+KGAke3RoaXMuZW52aXJvbm1lbnQuYXBpUGF0aH0vYXV0aC9sb2dpbmAsIGRhdGEpXHJcbiAgICAgIC5waXBlKG1hcCgodXNlcjogaVVzZXJMb2dpbikgPT4geyAgICAgICBcclxuICAgICAgICAvLyBsb2dpbiBzdWNjZXNzZnVsIGlmIHRoZXJlJ3MgYSBqd3QgdG9rZW4gaW4gdGhlIHJlc3BvbnNlICAgICAgICAgXHJcbiAgICAgICAgaWYgKHVzZXIgJiYgdXNlci5hY2Nlc3NUb2tlbikge1xyXG4gICAgICAgICAgLy8gVGhlcmUgaXMgYSBjb29raWUgd2l0aCB0aGUgand0IGluc2lkZSBmb3IgbWFuYWdpbmcgd2ViIGJyb3dzZXIgcmVmcmVzaCAgICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QubmV4dCh1c2VyKSAgICAgICAgICAgIFxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHVzZXJcclxuICAgICAgfSkpXHJcbiAgfVxyXG5cclxuICBsb2dPdXQoKSB7XHJcbiAgICAvLyByZW1vdmUgY29va2llIHRvIGxvZyB1c2VyIG91dCAgICBcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PGJvb2xlYW4+KGAke3RoaXMuZW52aXJvbm1lbnQuYXBpUGF0aH0vYXV0aC9sb2dvdXRgKVxyXG4gICAgICAgICAgICAucGlwZShtYXAoKGNvb2tpZURlbGV0ZWQ6IGJvb2xlYW4pID0+IHsgXHJcbiAgICAgICAgICAgICAgaWYgKGNvb2tpZURlbGV0ZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFVzZXJTdWJqZWN0Lm5leHQobnVsbCkgICAgXHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9sb2dpbiddKSAgICAgICAgXHJcbiAgICAgICAgICAgICAgfSAgICAgIFxyXG4gICAgICAgICAgICAgIHJldHVybiBjb29raWVEZWxldGVkXHJcbiAgICAgICAgICAgIH0pKVxyXG5cclxuXHJcbiAgICAvLyAuc3Vic2NyaWJlKFxyXG4gICAgLy8gICBjb29raWVEZWxldGVkID0+IHtcclxuICAgIC8vICAgICBpZiAoY29va2llRGVsZXRlZCkge1xyXG4gICAgLy8gICAgICAgdGhpcy5jdXJyZW50VXNlclN1YmplY3QubmV4dChudWxsKSAgICBcclxuICAgIC8vICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2xvZ2luJ10pICAgICAgICBcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgIH0sXHJcbiAgICAvLyAgIGVycm9yID0+IHtcclxuICAgIC8vICAgICB0aHJvdyBlcnJvclxyXG4gICAgLy8gICB9KSAgICBcclxuICB9XHJcblxyXG4gIC8vIGFzeW5jIGxvZ091dCgpIHtcclxuICAvLyAgIC8vIHJlbW92ZSBjb29raWUgdG8gbG9nIHVzZXIgb3V0ICAgIFxyXG4gIC8vICAgdGhpcy5odHRwQ2xpZW50LmdldDxib29sZWFuPihgJHt0aGlzLmVudmlyb25tZW50LmFwaVBhdGh9L2F1dGgvbG9nb3V0YClcclxuICAvLyAgIC5zdWJzY3JpYmUoXHJcbiAgLy8gICAgIGNvb2tpZURlbGV0ZWQgPT4ge1xyXG4gIC8vICAgICAgIGlmIChjb29raWVEZWxldGVkKSB7XHJcbiAgLy8gICAgICAgICB0aGlzLmN1cnJlbnRVc2VyU3ViamVjdC5uZXh0KG51bGwpICAgIFxyXG4gIC8vICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvbG9naW4nXSkgICAgICAgIFxyXG4gIC8vICAgICAgIH1cclxuICAvLyAgICAgfSxcclxuICAvLyAgICAgZXJyb3IgPT4ge1xyXG4gIC8vICAgICAgIHRocm93IGVycm9yXHJcbiAgLy8gICAgIH0pICAgIFxyXG4gIC8vIH1cclxufVxyXG4iXX0=
// Angular
import { Component, EventEmitter, Input, Output, ViewEncapsulation, Inject, Optional } from '@angular/core';
// SPS Services
// SPS Models
// SPS Components
// Vendors & utils
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
function FofCoreDialogYesNoComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "button", 5);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r205 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r205.uiVar.noLabel);
} }
var FofCoreDialogYesNoComponent = /** @class */ (function () {
    function FofCoreDialogYesNoComponent(data) {
        this.data = data;
        // All input var
        this.anInput = undefined;
        // All output notification
        this.anOuputEvent = new EventEmitter();
        // All variables private to the class
        this.priVar = {};
        // All private functions
        this.privFunc = {};
        // All variables shared with UI HTML interface
        this.uiVar = {
            title: 'fof à une question',
            question: undefined,
            yesLabel: 'oui',
            noLabel: 'non',
            informationOnly: false
        };
        // All actions accessible by user (could be used internaly too)
        this.uiAction = {};
        // this.matDialogRef.updatePosition({ top: '20px', left: '50px' });
        this.uiVar.question = data.question;
        if (data.title) {
            this.uiVar.title = data.title;
        }
        if (data.yesLabel) {
            this.uiVar.yesLabel = data.yesLabel;
        }
        if (data.noLabel) {
            this.uiVar.noLabel = data.noLabel;
        }
        if (data.informationOnly) {
            this.uiVar.informationOnly = data.informationOnly;
        }
    }
    // Angular events
    FofCoreDialogYesNoComponent.prototype.ngOnInit = function () {
    };
    FofCoreDialogYesNoComponent.prototype.ngAfterViewInit = function () {
    };
    FofCoreDialogYesNoComponent.ɵfac = function FofCoreDialogYesNoComponent_Factory(t) { return new (t || FofCoreDialogYesNoComponent)(i0.ɵɵdirectiveInject(MAT_DIALOG_DATA, 8)); };
    FofCoreDialogYesNoComponent.ɵcmp = i0.ɵɵdefineComponent({ type: FofCoreDialogYesNoComponent, selectors: [["core-dialog-yes-no"]], inputs: { anInput: "anInput" }, outputs: { anOuputEvent: "anOuputEvent" }, decls: 9, vars: 5, consts: [[1, "view-mad--core--yes-no"], ["mat-dialog-title", ""], [3, "innerHTML"], ["mat-button", "", "mat-dialog-close", "", 4, "ngIf"], ["mat-button", "", 3, "mat-dialog-close"], ["mat-button", "", "mat-dialog-close", ""]], template: function FofCoreDialogYesNoComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "h2", 1);
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "mat-dialog-content");
            i0.ɵɵelement(4, "div", 2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(5, "mat-dialog-actions");
            i0.ɵɵtemplate(6, FofCoreDialogYesNoComponent_button_6_Template, 2, 1, "button", 3);
            i0.ɵɵelementStart(7, "button", 4);
            i0.ɵɵtext(8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.uiVar.title);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("innerHTML", ctx.uiVar.question, i0.ɵɵsanitizeHtml);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", !ctx.uiVar.informationOnly);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("mat-dialog-close", true);
            i0.ɵɵadvance(1);
            i0.ɵɵtextInterpolate(ctx.uiVar.yesLabel);
        } }, directives: [i1.MatDialogTitle, i1.MatDialogContent, i1.MatDialogActions, i2.NgIf, i3.MatButton, i1.MatDialogClose], styles: [".view-mad--core--yes-no .mat-dialog-actions{margin-top:1rem;display:flex;justify-content:flex-end}"], encapsulation: 2 });
    return FofCoreDialogYesNoComponent;
}());
export { FofCoreDialogYesNoComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofCoreDialogYesNoComponent, [{
        type: Component,
        args: [{
                selector: 'core-dialog-yes-no',
                templateUrl: './core-dialog-yes-no.component.html',
                styleUrls: ['./core-dialog-yes-no.component.scss'],
                encapsulation: ViewEncapsulation.None
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Optional
            }, {
                type: Inject,
                args: [MAT_DIALOG_DATA]
            }] }]; }, { anInput: [{
            type: Input
        }], anOuputEvent: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvY29yZS1kaWFsb2cteWVzLW5vL2NvcmUtZGlhbG9nLXllcy1uby5jb21wb25lbnQudHMiLCJsaWIvY29yZS9jb3JlLWRpYWxvZy15ZXMtbm8vY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLFVBQVU7QUFDVixPQUFPLEVBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLGlCQUFpQixFQUN4RCxNQUFNLEVBQUUsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFBO0FBQ3ZELGVBQWU7QUFDZixhQUFhO0FBQ2IsaUJBQWlCO0FBQ2pCLGtCQUFrQjtBQUNsQixPQUFPLEVBQUMsZUFBZSxFQUFnQixNQUFNLDBCQUEwQixDQUFBOzs7Ozs7SUNBbkUsaUNBQ2lDO0lBQUEsWUFBaUI7SUFBQSxpQkFBUzs7O0lBQTFCLGVBQWlCO0lBQWpCLDRDQUFpQjs7QURpQnREO0lBWUUscUNBQzhDLElBQWU7UUFBZixTQUFJLEdBQUosSUFBSSxDQUFXO1FBTjdELGdCQUFnQjtRQUNQLFlBQU8sR0FBUSxTQUFTLENBQUE7UUFDakMsMEJBQTBCO1FBQ2hCLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQTtRQXlCaEQscUNBQXFDO1FBQzdCLFdBQU0sR0FBRyxFQUNoQixDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRyxFQUNsQixDQUFBO1FBQ0QsOENBQThDO1FBQ3ZDLFVBQUssR0FBRztZQUNiLEtBQUssRUFBRSxvQkFBb0I7WUFDM0IsUUFBUSxFQUFFLFNBQVM7WUFDbkIsUUFBUSxFQUFFLEtBQUs7WUFDZixPQUFPLEVBQUUsS0FBSztZQUNkLGVBQWUsRUFBRSxLQUFLO1NBQ3ZCLENBQUE7UUFDRCwrREFBK0Q7UUFDeEQsYUFBUSxHQUFHLEVBQ2pCLENBQUE7UUFqQ0MsbUVBQW1FO1FBQ25FLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUE7UUFFbkMsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2QsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQTtTQUM5QjtRQUNELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFBO1NBQ3BDO1FBQ0QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUE7U0FDbEM7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQTtTQUNsRDtJQUNILENBQUM7SUFtQkQsaUJBQWlCO0lBQ2pCLDhDQUFRLEdBQVI7SUFFQSxDQUFDO0lBQ0QscURBQWUsR0FBZjtJQUVBLENBQUM7MEdBcERVLDJCQUEyQix1QkFPaEIsZUFBZTtvRUFQMUIsMkJBQTJCO1lDL0J4Qyw4QkFDRTtZQUFBLDZCQUFxQjtZQUFBLFlBQWU7WUFBQSxpQkFBSztZQUN6QywwQ0FDRTtZQUFBLHlCQUF3QztZQUUxQyxpQkFBcUI7WUFDckIsMENBQ0U7WUFBQSxrRkFDaUM7WUFFakMsaUNBQTZDO1lBQUEsWUFBa0I7WUFBQSxpQkFBUztZQUMxRSxpQkFBcUI7WUFDdkIsaUJBQU07O1lBWGlCLGVBQWU7WUFBZixxQ0FBZTtZQUU3QixlQUE0QjtZQUE1QixpRUFBNEI7WUFLL0IsZUFBOEI7WUFBOUIsaURBQThCO1lBRWIsZUFBeUI7WUFBekIsdUNBQXlCO1lBQUMsZUFBa0I7WUFBbEIsd0NBQWtCOztzQ0RWbkU7Q0FvRkMsQUEzREQsSUEyREM7U0FyRFksMkJBQTJCO2tEQUEzQiwyQkFBMkI7Y0FOdkMsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLFdBQVcsRUFBRSxxQ0FBcUM7Z0JBQ2xELFNBQVMsRUFBRSxDQUFDLHFDQUFxQyxDQUFDO2dCQUNsRCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTthQUN0Qzs7c0JBUUksUUFBUTs7c0JBQUksTUFBTTt1QkFBQyxlQUFlOztrQkFMcEMsS0FBSzs7a0JBRUwsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbIi8vIEFuZ3VsYXJcclxuaW1wb3J0IHtDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBWaWV3RW5jYXBzdWxhdGlvbiwgXHJcbiAgQWZ0ZXJWaWV3SW5pdCwgSW5qZWN0LCBPcHRpb25hbH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuLy8gU1BTIFNlcnZpY2VzXHJcbi8vIFNQUyBNb2RlbHNcclxuLy8gU1BTIENvbXBvbmVudHNcclxuLy8gVmVuZG9ycyAmIHV0aWxzXHJcbmltcG9ydCB7TUFUX0RJQUxPR19EQVRBLCBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIGlJbmZvcm1hdGlvbiB7XHJcbiAgdGl0bGU/IDogc3RyaW5nLFxyXG4gIHF1ZXN0aW9uOiBzdHJpbmcsXHJcbiAgeWVzTGFiZWw/IDogc3RyaW5nLCAgXHJcbiAgd2lkdGg/OiBzdHJpbmcsXHJcbiAgaGVpZ2h0Pzogc3RyaW5nICBcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBpWWVzTm8gZXh0ZW5kcyBpSW5mb3JtYXRpb24gIHtcclxuICBub0xhYmVsPzogc3RyaW5nLFxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIGlJbnRlcm5hbCBleHRlbmRzIGlZZXNObyAge1xyXG4gIGluZm9ybWF0aW9uT25seTogYm9vbGVhblxyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2NvcmUtZGlhbG9nLXllcy1ubycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvcmUtZGlhbG9nLXllcy1uby5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29yZS1kaWFsb2cteWVzLW5vLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9mQ29yZURpYWxvZ1llc05vQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAvLyBBbGwgaW5wdXQgdmFyXHJcbiAgQElucHV0KCkgYW5JbnB1dDogYW55ID0gdW5kZWZpbmVkXHJcbiAgLy8gQWxsIG91dHB1dCBub3RpZmljYXRpb25cclxuICBAT3V0cHV0KCkgYW5PdXB1dEV2ZW50ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KClcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBAT3B0aW9uYWwoKSBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGlJbnRlcm5hbCxcclxuICAgIC8vIHB1YmxpYyBtYXREaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxhbnk+XHJcbiAgICAvLyBwcml2YXRlIHJlYWRvbmx5IGNvcmVBbGVydFNlcnZpY2U6IENvcmVBbGVydFNlcnZpY2UsXHJcbiAgICAvLyBwcml2YXRlIHJlYWRvbmx5IGNvcmVFcnJvclNlcnZpY2U6IENvcmVFcnJvclNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIHRoaXMubWF0RGlhbG9nUmVmLnVwZGF0ZVBvc2l0aW9uKHsgdG9wOiAnMjBweCcsIGxlZnQ6ICc1MHB4JyB9KTtcclxuICAgIHRoaXMudWlWYXIucXVlc3Rpb24gPSBkYXRhLnF1ZXN0aW9uXHJcblxyXG4gICAgaWYgKGRhdGEudGl0bGUpIHtcclxuICAgICAgdGhpcy51aVZhci50aXRsZSA9IGRhdGEudGl0bGVcclxuICAgIH0gICAgXHJcbiAgICBpZiAoZGF0YS55ZXNMYWJlbCkge1xyXG4gICAgICB0aGlzLnVpVmFyLnllc0xhYmVsID0gZGF0YS55ZXNMYWJlbFxyXG4gICAgfSAgICBcclxuICAgIGlmIChkYXRhLm5vTGFiZWwpIHtcclxuICAgICAgdGhpcy51aVZhci5ub0xhYmVsID0gZGF0YS5ub0xhYmVsXHJcbiAgICB9ICAgIFxyXG4gICAgaWYgKGRhdGEuaW5mb3JtYXRpb25Pbmx5KSB7XHJcbiAgICAgIHRoaXMudWlWYXIuaW5mb3JtYXRpb25Pbmx5ID0gZGF0YS5pbmZvcm1hdGlvbk9ubHlcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgcHJpdmF0ZSB0byB0aGUgY2xhc3NcclxuICBwcml2YXRlIHByaVZhciA9IHtcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICB9XHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBIVE1MIGludGVyZmFjZVxyXG4gIHB1YmxpYyB1aVZhciA9IHtcclxuICAgIHRpdGxlOiAnZm9mIMOgIHVuZSBxdWVzdGlvbicsXHJcbiAgICBxdWVzdGlvbjogdW5kZWZpbmVkLFxyXG4gICAgeWVzTGFiZWw6ICdvdWknLFxyXG4gICAgbm9MYWJlbDogJ25vbicsXHJcbiAgICBpbmZvcm1hdGlvbk9ubHk6IGZhbHNlXHJcbiAgfVxyXG4gIC8vIEFsbCBhY3Rpb25zIGFjY2Vzc2libGUgYnkgdXNlciAoY291bGQgYmUgdXNlZCBpbnRlcm5hbHkgdG9vKVxyXG4gIHB1YmxpYyB1aUFjdGlvbiA9IHtcclxuICB9XHJcbiAgLy8gQW5ndWxhciBldmVudHNcclxuICBuZ09uSW5pdCgpIHtcclxuICAgXHJcbiAgfVxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgIFxyXG4gIH1cclxufVxyXG4iLCI8ZGl2IGNsYXNzPVwidmlldy1tYWQtLWNvcmUtLXllcy1ub1wiPlxyXG4gIDxoMiBtYXQtZGlhbG9nLXRpdGxlPnt7dWlWYXIudGl0bGV9fTwvaDI+XHJcbiAgPG1hdC1kaWFsb2ctY29udGVudD5cclxuICAgIDxkaXYgW2lubmVySFRNTF09XCJ1aVZhci5xdWVzdGlvblwiPjwvZGl2PlxyXG4gICAgPCEtLSB7e3VpVmFyLnF1ZXN0aW9ufX0gLS0+XHJcbiAgPC9tYXQtZGlhbG9nLWNvbnRlbnQ+XHJcbiAgPG1hdC1kaWFsb2ctYWN0aW9ucz5cclxuICAgIDxidXR0b24gbWF0LWJ1dHRvbiBtYXQtZGlhbG9nLWNsb3NlXHJcbiAgICAgICpuZ0lmPVwiIXVpVmFyLmluZm9ybWF0aW9uT25seVwiPnt7dWlWYXIubm9MYWJlbH19PC9idXR0b24+XHJcbiAgICA8IS0tIFRoZSBtYXQtZGlhbG9nLWNsb3NlIGRpcmVjdGl2ZSBvcHRpb25hbGx5IGFjY2VwdHMgYSB2YWx1ZSBhcyBhIHJlc3VsdCBmb3IgdGhlIGRpYWxvZy4gLS0+XHJcbiAgICA8YnV0dG9uIG1hdC1idXR0b24gW21hdC1kaWFsb2ctY2xvc2VdPVwidHJ1ZVwiPnt7dWlWYXIueWVzTGFiZWx9fTwvYnV0dG9uPiAgICBcclxuICA8L21hdC1kaWFsb2ctYWN0aW9ucz5cclxuPC9kaXY+Il19
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FofLocalstorageService } from './fof-localstorage.service';
import { fofErrorHandler } from './app-error-handler.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FofAuthGuard } from './auth.guard';
import { FoFAuthService } from './auth.service';
import { FofErrorInterceptor } from './http-error.interceptor';
import { FoFJwtInterceptor } from './jwt.interceptor';
import { FoFCoreService } from './core.service';
import { FofNotificationService } from './notification/notification.service';
import { notificationComponent } from './notification/notification/notification.component';
import { MaterialModule } from '../core/material.module';
import { FofCoreDialogYesNoComponent } from './core-dialog-yes-no/core-dialog-yes-no.component';
import { FofDialogService } from './fof-dialog.service';
import { FofErrorService } from './fof-error.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SharedModule } from '../shared/shared.module';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
// import { ComponentsModule } from '../components/components.module'
// import { ModuleTranslateLoader, IModuleTranslationOptions } from '@larscom/ngx-translate-module-loader'
// import { registerLocaleData } from '@angular/common'
// import localeFr from '@angular/common/locales/fr'
// import localeFrExtra from '@angular/common/locales/extra/fr'
// registerLocaleData(localeFr)
// AoT requires an exported function for factories
export function HttpLoaderFactory(http) {
    return new TranslateHttpLoader(http);
}
// https://github.com/larscom/ngx-translate-module-loader
// export function moduleHttpLoaderFactory(http: HttpClient) {
//   const baseTranslateUrl = './assets/i18n';
//   const options: IModuleTranslationOptions = {
//     modules: [
//       // final url: ./assets/i18n/en.json
//       { baseTranslateUrl },
//       // final url: ./assets/i18n/admin/en.json
//       { moduleName: 'admin', baseTranslateUrl },
//       // final url: ./assets/i18n/feature2/en.json
//       { moduleName: 'feature2', baseTranslateUrl }
//     ]
//   };
//   return new ModuleTranslateLoader(http, options);
// }
var FoFCoreModule = /** @class */ (function () {
    function FoFCoreModule() {
    }
    FoFCoreModule.ɵmod = i0.ɵɵdefineNgModule({ type: FoFCoreModule });
    FoFCoreModule.ɵinj = i0.ɵɵdefineInjector({ factory: function FoFCoreModule_Factory(t) { return new (t || FoFCoreModule)(); }, providers: [
            FofLocalstorageService,
            // { provide: LOCALE_ID, useValue: 'fr' },
            { provide: ErrorHandler, useClass: fofErrorHandler },
            { provide: HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
            { provide: HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
            FofAuthGuard,
            FoFAuthService,
            FoFCoreService,
            FofNotificationService,
            FofDialogService,
            FofErrorService
        ], imports: [[
                CommonModule,
                SharedModule,
                MaterialModule,
                HttpClientModule,
                // ComponentsModule,
                TranslateModule.forRoot({
                    defaultLanguage: 'fr',
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ]] });
    return FoFCoreModule;
}());
export { FoFCoreModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(FoFCoreModule, { declarations: [notificationComponent,
        FofCoreDialogYesNoComponent], imports: [CommonModule,
        SharedModule,
        MaterialModule,
        HttpClientModule, i1.TranslateModule], exports: [notificationComponent,
        FofCoreDialogYesNoComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFCoreModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    notificationComponent,
                    FofCoreDialogYesNoComponent
                ],
                imports: [
                    CommonModule,
                    SharedModule,
                    MaterialModule,
                    HttpClientModule,
                    // ComponentsModule,
                    TranslateModule.forRoot({
                        defaultLanguage: 'fr',
                        loader: {
                            provide: TranslateLoader,
                            useFactory: HttpLoaderFactory,
                            deps: [HttpClient]
                        }
                    })
                ],
                entryComponents: [
                    FofCoreDialogYesNoComponent
                ],
                providers: [
                    FofLocalstorageService,
                    // { provide: LOCALE_ID, useValue: 'fr' },
                    { provide: ErrorHandler, useClass: fofErrorHandler },
                    { provide: HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
                    { provide: HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
                    FofAuthGuard,
                    FoFAuthService,
                    FoFCoreService,
                    FofNotificationService,
                    FofDialogService,
                    FofErrorService
                ],
                exports: [
                    notificationComponent,
                    FofCoreDialogYesNoComponent,
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsWUFBWSxFQUFhLE1BQU0sZUFBZSxDQUFBO0FBQ2pFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUE7QUFDOUMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUE7QUFDbkUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFBO0FBQzdELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHNCQUFzQixDQUFBO0FBQ3hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUE7QUFDM0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBQy9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDBCQUEwQixDQUFBO0FBQzlELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1CQUFtQixDQUFBO0FBQ3JELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUMvQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQTtBQUM1RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQTtBQUMxRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUE7QUFDeEQsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sbURBQW1ELENBQUE7QUFDL0YsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUE7QUFDdkQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFBO0FBQ3JELE9BQU8sRUFBRSxlQUFlLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUE7QUFDdEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNEJBQTRCLENBQUE7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHlCQUF5QixDQUFBOzs7QUFDdEQscUVBQXFFO0FBQ3JFLDBHQUEwRztBQUUxRyx1REFBdUQ7QUFDdkQsb0RBQW9EO0FBQ3BELCtEQUErRDtBQUMvRCwrQkFBK0I7QUFFL0Isa0RBQWtEO0FBQ2xELE1BQU0sVUFBVSxpQkFBaUIsQ0FBQyxJQUFnQjtJQUNoRCxPQUFPLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDdEMsQ0FBQztBQUVELHlEQUF5RDtBQUN6RCw4REFBOEQ7QUFDOUQsOENBQThDO0FBRTlDLGlEQUFpRDtBQUNqRCxpQkFBaUI7QUFDakIsNENBQTRDO0FBQzVDLDhCQUE4QjtBQUM5QixrREFBa0Q7QUFDbEQsbURBQW1EO0FBQ25ELHFEQUFxRDtBQUNyRCxxREFBcUQ7QUFDckQsUUFBUTtBQUNSLE9BQU87QUFDUCxxREFBcUQ7QUFDckQsSUFBSTtBQUdKO0lBQUE7S0EwQzhCO3FEQUFqQixhQUFhOzZHQUFiLGFBQWEsbUJBbkJiO1lBQ1Qsc0JBQXNCO1lBQ3RCLDBDQUEwQztZQUMxQyxFQUFFLE9BQU8sRUFBRSxZQUFZLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRTtZQUNwRCxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRTtZQUN4RSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRTtZQUMxRSxZQUFZO1lBQ1osY0FBYztZQUNkLGNBQWM7WUFDZCxzQkFBc0I7WUFDdEIsZ0JBQWdCO1lBQ2hCLGVBQWU7U0FDaEIsWUE5QlE7Z0JBQ1AsWUFBWTtnQkFDWixZQUFZO2dCQUNaLGNBQWM7Z0JBQ2QsZ0JBQWdCO2dCQUNoQixvQkFBb0I7Z0JBQ3BCLGVBQWUsQ0FBQyxPQUFPLENBQUM7b0JBQ3RCLGVBQWUsRUFBRSxJQUFJO29CQUNyQixNQUFNLEVBQUU7d0JBQ04sT0FBTyxFQUFFLGVBQWU7d0JBQ3hCLFVBQVUsRUFBRSxpQkFBaUI7d0JBQzdCLElBQUksRUFBRSxDQUFDLFVBQVUsQ0FBQztxQkFDbkI7aUJBQ0YsQ0FBQzthQUNIO3dCQXRFSDtDQTZGOEIsQUExQzlCLElBMEM4QjtTQUFqQixhQUFhO3dGQUFiLGFBQWEsbUJBeEN0QixxQkFBcUI7UUFDckIsMkJBQTJCLGFBRzNCLFlBQVk7UUFDWixZQUFZO1FBQ1osY0FBYztRQUNkLGdCQUFnQixpQ0E0QmhCLHFCQUFxQjtRQUNyQiwyQkFBMkI7a0RBSWxCLGFBQWE7Y0ExQ3pCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUU7b0JBQ1oscUJBQXFCO29CQUNyQiwyQkFBMkI7aUJBQzVCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxnQkFBZ0I7b0JBQ2hCLG9CQUFvQjtvQkFDcEIsZUFBZSxDQUFDLE9BQU8sQ0FBQzt3QkFDdEIsZUFBZSxFQUFFLElBQUk7d0JBQ3JCLE1BQU0sRUFBRTs0QkFDTixPQUFPLEVBQUUsZUFBZTs0QkFDeEIsVUFBVSxFQUFFLGlCQUFpQjs0QkFDN0IsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDO3lCQUNuQjtxQkFDRixDQUFDO2lCQUNIO2dCQUNELGVBQWUsRUFBRTtvQkFDZiwyQkFBMkI7aUJBQzVCO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxzQkFBc0I7b0JBQ3RCLDBDQUEwQztvQkFDMUMsRUFBRSxPQUFPLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUU7b0JBQ3BELEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxpQkFBaUIsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFO29CQUN4RSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRTtvQkFDMUUsWUFBWTtvQkFDWixjQUFjO29CQUNkLGNBQWM7b0JBQ2Qsc0JBQXNCO29CQUN0QixnQkFBZ0I7b0JBQ2hCLGVBQWU7aUJBQ2hCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxxQkFBcUI7b0JBQ3JCLDJCQUEyQjtpQkFFNUI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBFcnJvckhhbmRsZXIsIExPQ0FMRV9JRCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUsIEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJ1xyXG5pbXBvcnQgeyBGb2ZMb2NhbHN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi9mb2YtbG9jYWxzdG9yYWdlLnNlcnZpY2UnXHJcbmltcG9ydCB7IGZvZkVycm9ySGFuZGxlciB9IGZyb20gJy4vYXBwLWVycm9yLWhhbmRsZXIuc2VydmljZSdcclxuaW1wb3J0IHsgSFRUUF9JTlRFUkNFUFRPUlMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcclxuaW1wb3J0IHsgRm9mQXV0aEd1YXJkIH0gZnJvbSAnLi9hdXRoLmd1YXJkJ1xyXG5pbXBvcnQgeyBGb0ZBdXRoU2VydmljZSB9IGZyb20gJy4vYXV0aC5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZFcnJvckludGVyY2VwdG9yIH0gZnJvbSAnLi9odHRwLWVycm9yLmludGVyY2VwdG9yJ1xyXG5pbXBvcnQgeyBGb0ZKd3RJbnRlcmNlcHRvciB9IGZyb20gJy4vand0LmludGVyY2VwdG9yJ1xyXG5pbXBvcnQgeyBGb0ZDb3JlU2VydmljZSB9IGZyb20gJy4vY29yZS5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IG5vdGlmaWNhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50J1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL2NvcmUvbWF0ZXJpYWwubW9kdWxlJ1xyXG5pbXBvcnQgeyBGb2ZDb3JlRGlhbG9nWWVzTm9Db21wb25lbnQgfSBmcm9tICcuL2NvcmUtZGlhbG9nLXllcy1uby9jb3JlLWRpYWxvZy15ZXMtbm8uY29tcG9uZW50J1xyXG5pbXBvcnQgeyBGb2ZEaWFsb2dTZXJ2aWNlIH0gZnJvbSAnLi9mb2YtZGlhbG9nLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZkVycm9yU2VydmljZSB9IGZyb20gJy4vZm9mLWVycm9yLnNlcnZpY2UnXHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSwgVHJhbnNsYXRlTG9hZGVyIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSdcclxuaW1wb3J0IHsgVHJhbnNsYXRlSHR0cExvYWRlciB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2h0dHAtbG9hZGVyJ1xyXG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi9zaGFyZWQvc2hhcmVkLm1vZHVsZSdcclxuLy8gaW1wb3J0IHsgQ29tcG9uZW50c01vZHVsZSB9IGZyb20gJy4uL2NvbXBvbmVudHMvY29tcG9uZW50cy5tb2R1bGUnXHJcbi8vIGltcG9ydCB7IE1vZHVsZVRyYW5zbGF0ZUxvYWRlciwgSU1vZHVsZVRyYW5zbGF0aW9uT3B0aW9ucyB9IGZyb20gJ0BsYXJzY29tL25neC10cmFuc2xhdGUtbW9kdWxlLWxvYWRlcidcclxuXHJcbi8vIGltcG9ydCB7IHJlZ2lzdGVyTG9jYWxlRGF0YSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbidcclxuLy8gaW1wb3J0IGxvY2FsZUZyIGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9sb2NhbGVzL2ZyJ1xyXG4vLyBpbXBvcnQgbG9jYWxlRnJFeHRyYSBmcm9tICdAYW5ndWxhci9jb21tb24vbG9jYWxlcy9leHRyYS9mcidcclxuLy8gcmVnaXN0ZXJMb2NhbGVEYXRhKGxvY2FsZUZyKVxyXG5cclxuLy8gQW9UIHJlcXVpcmVzIGFuIGV4cG9ydGVkIGZ1bmN0aW9uIGZvciBmYWN0b3JpZXNcclxuZXhwb3J0IGZ1bmN0aW9uIEh0dHBMb2FkZXJGYWN0b3J5KGh0dHA6IEh0dHBDbGllbnQpIHtcclxuICByZXR1cm4gbmV3IFRyYW5zbGF0ZUh0dHBMb2FkZXIoaHR0cClcclxufVxyXG5cclxuLy8gaHR0cHM6Ly9naXRodWIuY29tL2xhcnNjb20vbmd4LXRyYW5zbGF0ZS1tb2R1bGUtbG9hZGVyXHJcbi8vIGV4cG9ydCBmdW5jdGlvbiBtb2R1bGVIdHRwTG9hZGVyRmFjdG9yeShodHRwOiBIdHRwQ2xpZW50KSB7XHJcbi8vICAgY29uc3QgYmFzZVRyYW5zbGF0ZVVybCA9ICcuL2Fzc2V0cy9pMThuJztcclxuXHJcbi8vICAgY29uc3Qgb3B0aW9uczogSU1vZHVsZVRyYW5zbGF0aW9uT3B0aW9ucyA9IHtcclxuLy8gICAgIG1vZHVsZXM6IFtcclxuLy8gICAgICAgLy8gZmluYWwgdXJsOiAuL2Fzc2V0cy9pMThuL2VuLmpzb25cclxuLy8gICAgICAgeyBiYXNlVHJhbnNsYXRlVXJsIH0sXHJcbi8vICAgICAgIC8vIGZpbmFsIHVybDogLi9hc3NldHMvaTE4bi9hZG1pbi9lbi5qc29uXHJcbi8vICAgICAgIHsgbW9kdWxlTmFtZTogJ2FkbWluJywgYmFzZVRyYW5zbGF0ZVVybCB9LFxyXG4vLyAgICAgICAvLyBmaW5hbCB1cmw6IC4vYXNzZXRzL2kxOG4vZmVhdHVyZTIvZW4uanNvblxyXG4vLyAgICAgICB7IG1vZHVsZU5hbWU6ICdmZWF0dXJlMicsIGJhc2VUcmFuc2xhdGVVcmwgfVxyXG4vLyAgICAgXVxyXG4vLyAgIH07XHJcbi8vICAgcmV0dXJuIG5ldyBNb2R1bGVUcmFuc2xhdGVMb2FkZXIoaHR0cCwgb3B0aW9ucyk7XHJcbi8vIH1cclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gICAgbm90aWZpY2F0aW9uQ29tcG9uZW50LFxyXG4gICAgRm9mQ29yZURpYWxvZ1llc05vQ29tcG9uZW50ICAgIFxyXG4gIF0sXHJcbiAgaW1wb3J0czogWyAgICBcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIFNoYXJlZE1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgSHR0cENsaWVudE1vZHVsZSxcclxuICAgIC8vIENvbXBvbmVudHNNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUuZm9yUm9vdCh7XHJcbiAgICAgIGRlZmF1bHRMYW5ndWFnZTogJ2ZyJyxcclxuICAgICAgbG9hZGVyOiB7XHJcbiAgICAgICAgcHJvdmlkZTogVHJhbnNsYXRlTG9hZGVyLFxyXG4gICAgICAgIHVzZUZhY3Rvcnk6IEh0dHBMb2FkZXJGYWN0b3J5LFxyXG4gICAgICAgIGRlcHM6IFtIdHRwQ2xpZW50XVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIF0sXHJcbiAgZW50cnlDb21wb25lbnRzOiBbXHJcbiAgICBGb2ZDb3JlRGlhbG9nWWVzTm9Db21wb25lbnRcclxuICBdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgRm9mTG9jYWxzdG9yYWdlU2VydmljZSxcclxuICAgIC8vIHsgcHJvdmlkZTogTE9DQUxFX0lELCB1c2VWYWx1ZTogJ2ZyJyB9LFxyXG4gICAgeyBwcm92aWRlOiBFcnJvckhhbmRsZXIsIHVzZUNsYXNzOiBmb2ZFcnJvckhhbmRsZXIgfSxcclxuICAgIHsgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsIHVzZUNsYXNzOiBGb0ZKd3RJbnRlcmNlcHRvciwgbXVsdGk6IHRydWUgfSxcclxuICAgIHsgcHJvdmlkZTogSFRUUF9JTlRFUkNFUFRPUlMsIHVzZUNsYXNzOiBGb2ZFcnJvckludGVyY2VwdG9yLCBtdWx0aTogdHJ1ZSB9LFxyXG4gICAgRm9mQXV0aEd1YXJkLFxyXG4gICAgRm9GQXV0aFNlcnZpY2UsXHJcbiAgICBGb0ZDb3JlU2VydmljZSxcclxuICAgIEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBGb2ZEaWFsb2dTZXJ2aWNlLFxyXG4gICAgRm9mRXJyb3JTZXJ2aWNlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbICAgIFxyXG4gICAgbm90aWZpY2F0aW9uQ29tcG9uZW50LFxyXG4gICAgRm9mQ29yZURpYWxvZ1llc05vQ29tcG9uZW50LFxyXG4gICAgLy8gQ29tcG9uZW50c01vZHVsZVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvRkNvcmVNb2R1bGUgeyB9XHJcbiJdfQ==
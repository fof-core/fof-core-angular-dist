import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, forkJoin } from 'rxjs';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "@angular/router";
import * as i3 from "./auth.service";
var FoFCoreService = /** @class */ (function () {
    function FoFCoreService(fofConfig, httpClient, router, foFAuthService) {
        var _this = this;
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.router = router;
        this.foFAuthService = foFAuthService;
        this._initializationReadySubject = new BehaviorSubject(false);
        this.initializationReady$ = this._initializationReadySubject.asObservable();
        this._environment = this.fofConfig.environment;
        forkJoin(this.foFAuthService.currentUser$).subscribe({
            // next: value => console.log('value', value),
            complete: function () {
                _this._initializationReadySubject.next(true);
            }
        });
    }
    Object.defineProperty(FoFCoreService.prototype, "environment", {
        get: function () {
            return this._environment;
        },
        enumerable: true,
        configurable: true
    });
    FoFCoreService.ɵfac = function FoFCoreService_Factory(t) { return new (t || FoFCoreService)(i0.ɵɵinject(CORE_CONFIG), i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.Router), i0.ɵɵinject(i3.FoFAuthService)); };
    FoFCoreService.ɵprov = i0.ɵɵdefineInjectable({ token: FoFCoreService, factory: FoFCoreService.ɵfac, providedIn: 'root' });
    return FoFCoreService;
}());
export { FoFCoreService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFCoreService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: i1.HttpClient }, { type: i2.Router }, { type: i3.FoFAuthService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9jb3JlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFHbEQsT0FBTyxFQUFFLGVBQWUsRUFBYyxRQUFRLEVBQU0sTUFBTSxNQUFNLENBQUE7QUFFaEUsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7Ozs7QUFJdkQ7SUFLRSx3QkFDK0IsU0FBcUIsRUFDMUMsVUFBc0IsRUFDdEIsTUFBYyxFQUNkLGNBQThCO1FBSnhDLGlCQWdCQztRQWY4QixjQUFTLEdBQVQsU0FBUyxDQUFZO1FBQzFDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQWNoQyxnQ0FBMkIsR0FBNkIsSUFBSSxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDbkYseUJBQW9CLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLFlBQVksRUFBRSxDQUFBO1FBYjNFLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUE7UUFFOUMsUUFBUSxDQUNOLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUNqQyxDQUFDLFNBQVMsQ0FBQztZQUNWLDhDQUE4QztZQUM5QyxRQUFRLEVBQUU7Z0JBQ1IsS0FBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtZQUM3QyxDQUFDO1NBQ0YsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQU1ELHNCQUFXLHVDQUFXO2FBQXRCO1lBQ0UsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFBO1FBQzFCLENBQUM7OztPQUFBO2dGQTFCVSxjQUFjLGNBR2YsV0FBVzswREFIVixjQUFjLFdBQWQsY0FBYyxtQkFGYixNQUFNO3lCQVZwQjtDQXdDQyxBQS9CRCxJQStCQztTQTVCWSxjQUFjO2tEQUFkLGNBQWM7Y0FIMUIsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COztzQkFJSSxNQUFNO3VCQUFDLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlLCBmb3JrSm9pbiwgb2YgfSBmcm9tICdyeGpzJ1xyXG5pbXBvcnQgeyBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycydcclxuaW1wb3J0IHsgSWZvZkNvbmZpZywgQ09SRV9DT05GSUcgfSBmcm9tICcuLi9mb2YtY29uZmlnJ1xyXG4vLyBpbXBvcnQgeyBpVXNlciB9IGZyb20gJy4uL3NoYXJlZC91c2VyLmludGVyZmFjZSdcclxuaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UgfSBmcm9tICcuL2F1dGguc2VydmljZSdcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvRkNvcmVTZXJ2aWNlIHtcclxuICBcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIEBJbmplY3QoQ09SRV9DT05GSUcpIHByaXZhdGUgZm9mQ29uZmlnOiBJZm9mQ29uZmlnLFxyXG4gICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgZm9GQXV0aFNlcnZpY2U6IEZvRkF1dGhTZXJ2aWNlXHJcbiAgKSB7IFxyXG4gICAgdGhpcy5fZW52aXJvbm1lbnQgPSB0aGlzLmZvZkNvbmZpZy5lbnZpcm9ubWVudFxyXG4gICBcclxuICAgIGZvcmtKb2luKFxyXG4gICAgICB0aGlzLmZvRkF1dGhTZXJ2aWNlLmN1cnJlbnRVc2VyJCAgICAgIFxyXG4gICAgKS5zdWJzY3JpYmUoe1xyXG4gICAgICAvLyBuZXh0OiB2YWx1ZSA9PiBjb25zb2xlLmxvZygndmFsdWUnLCB2YWx1ZSksXHJcbiAgICAgIGNvbXBsZXRlOiAoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5faW5pdGlhbGl6YXRpb25SZWFkeVN1YmplY3QubmV4dCh0cnVlKVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBfaW5pdGlhbGl6YXRpb25SZWFkeVN1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPiA9IG5ldyBCZWhhdmlvclN1YmplY3QoZmFsc2UpXHJcbiAgcHVibGljIGluaXRpYWxpemF0aW9uUmVhZHkkID0gdGhpcy5faW5pdGlhbGl6YXRpb25SZWFkeVN1YmplY3QuYXNPYnNlcnZhYmxlKClcclxuXHJcbiAgcHJpdmF0ZSBfZW52aXJvbm1lbnQ6YW55XHJcbiAgcHVibGljIGdldCBlbnZpcm9ubWVudCgpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2Vudmlyb25tZW50XHJcbiAgfVxyXG4gIFxyXG59XHJcbiJdfQ==
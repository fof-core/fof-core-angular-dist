import { Injectable } from '@angular/core';
import { FofCoreDialogYesNoComponent } from './core-dialog-yes-no/core-dialog-yes-no.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
var FofDialogService = /** @class */ (function () {
    function FofDialogService(matDialog) {
        this.matDialog = matDialog;
    }
    FofDialogService.prototype.openYesNo = function (options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _options = options;
            var width = options.width || '300px';
            var height = options.height || undefined;
            _options.informationOnly = _options.informationOnly || false;
            var dialogRef = _this.matDialog.open(FofCoreDialogYesNoComponent, {
                data: _options,
                width: width,
                height: height,
                // to have a look on, bug
                position: {
                    top: '150px'
                }
            });
            dialogRef.afterClosed()
                .toPromise()
                .then(function (result) {
                resolve(result);
            });
        });
    };
    FofDialogService.prototype.openInformation = function (options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _options = options;
            _options.informationOnly = true;
            _options.title = _options.title || 'fof à une information';
            _options.yesLabel = _options.yesLabel || 'ok';
            _this.openYesNo(_options)
                .then(function (result) {
                resolve(result);
            });
        });
    };
    FofDialogService.ɵfac = function FofDialogService_Factory(t) { return new (t || FofDialogService)(i0.ɵɵinject(i1.MatDialog)); };
    FofDialogService.ɵprov = i0.ɵɵdefineInjectable({ token: FofDialogService, factory: FofDialogService.ɵfac, providedIn: 'root' });
    return FofDialogService;
}());
export { FofDialogService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofDialogService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.MatDialog }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWRpYWxvZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9mb2YtZGlhbG9nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQTtBQUMxQyxPQUFPLEVBQUUsMkJBQTJCLEVBQW1DLE1BQU0sbURBQW1ELENBQUE7OztBQUloSTtJQUtFLDBCQUNtQixTQUFvQjtRQUFwQixjQUFTLEdBQVQsU0FBUyxDQUFXO0lBQ25DLENBQUM7SUFFRSxvQ0FBUyxHQUFoQixVQUFpQixPQUFlO1FBQWhDLGlCQXlCQztRQXhCQyxPQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFFakMsSUFBSSxRQUFRLEdBQXlCLE9BQU8sQ0FBQTtZQUU1QyxJQUFJLEtBQUssR0FBRyxPQUFPLENBQUMsS0FBSyxJQUFJLE9BQU8sQ0FBQTtZQUNwQyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxJQUFJLFNBQVMsQ0FBQTtZQUV4QyxRQUFRLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxlQUFlLElBQUksS0FBSyxDQUFBO1lBRTVELElBQU0sU0FBUyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLDJCQUEyQixFQUFFO2dCQUNqRSxJQUFJLEVBQUUsUUFBUTtnQkFDZCxLQUFLLEVBQUUsS0FBSztnQkFDWixNQUFNLEVBQUUsTUFBTTtnQkFDZCx5QkFBeUI7Z0JBQ3pCLFFBQVEsRUFBRTtvQkFDUixHQUFHLEVBQUUsT0FBTztpQkFDYjthQUNGLENBQUMsQ0FBQTtZQUNGLFNBQVMsQ0FBQyxXQUFXLEVBQUU7aUJBQ3RCLFNBQVMsRUFBRTtpQkFDWCxJQUFJLENBQUMsVUFBQSxNQUFNO2dCQUNWLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQTtZQUNqQixDQUFDLENBQUMsQ0FBQTtRQUNKLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVNLDBDQUFlLEdBQXRCLFVBQXVCLE9BQXFCO1FBQTVDLGlCQWFDO1FBWkMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBRWpDLElBQUksUUFBUSxHQUF5QixPQUFPLENBQUE7WUFDNUMsUUFBUSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUE7WUFDL0IsUUFBUSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxJQUFJLHVCQUF1QixDQUFBO1lBQzFELFFBQVEsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUE7WUFFN0MsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7aUJBQ3ZCLElBQUksQ0FBQyxVQUFBLE1BQU07Z0JBQ1YsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1lBQ2pCLENBQUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO29GQTlDVSxnQkFBZ0I7NERBQWhCLGdCQUFnQixXQUFoQixnQkFBZ0IsbUJBRmYsTUFBTTsyQkFOcEI7Q0F1REMsQUFsREQsSUFrREM7U0EvQ1ksZ0JBQWdCO2tEQUFoQixnQkFBZ0I7Y0FINUIsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IEZvZkNvcmVEaWFsb2dZZXNOb0NvbXBvbmVudCwgaUludGVybmFsLCBpWWVzTm8sIGlJbmZvcm1hdGlvbiB9IGZyb20gJy4vY29yZS1kaWFsb2cteWVzLW5vL2NvcmUtZGlhbG9nLXllcy1uby5jb21wb25lbnQnXHJcbmltcG9ydCB7IE1hdERpYWxvZywgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnXHJcblxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9mRGlhbG9nU2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSByZWFkb25seSBtYXREaWFsb2c6IE1hdERpYWxvZ1xyXG4gICkgeyB9XHJcblxyXG4gIHB1YmxpYyBvcGVuWWVzTm8ob3B0aW9uczogaVllc05vKSB7XHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG5cclxuICAgICAgbGV0IF9vcHRpb25zOiBpSW50ZXJuYWwgPSA8aUludGVybmFsPm9wdGlvbnNcclxuXHJcbiAgICAgIGxldCB3aWR0aCA9IG9wdGlvbnMud2lkdGggfHwgJzMwMHB4JyBcclxuICAgICAgbGV0IGhlaWdodCA9IG9wdGlvbnMuaGVpZ2h0IHx8IHVuZGVmaW5lZFxyXG5cclxuICAgICAgX29wdGlvbnMuaW5mb3JtYXRpb25Pbmx5ID0gX29wdGlvbnMuaW5mb3JtYXRpb25Pbmx5IHx8IGZhbHNlXHJcblxyXG4gICAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLm1hdERpYWxvZy5vcGVuKEZvZkNvcmVEaWFsb2dZZXNOb0NvbXBvbmVudCwge1xyXG4gICAgICAgIGRhdGE6IF9vcHRpb25zLCAgICAgICAgXHJcbiAgICAgICAgd2lkdGg6IHdpZHRoLFxyXG4gICAgICAgIGhlaWdodDogaGVpZ2h0LFxyXG4gICAgICAgIC8vIHRvIGhhdmUgYSBsb29rIG9uLCBidWdcclxuICAgICAgICBwb3NpdGlvbjoge1xyXG4gICAgICAgICAgdG9wOiAnMTUwcHgnXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgICAudG9Qcm9taXNlKClcclxuICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICByZXNvbHZlKHJlc3VsdClcclxuICAgICAgfSlcclxuICAgIH0pXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb3BlbkluZm9ybWF0aW9uKG9wdGlvbnM6IGlJbmZvcm1hdGlvbikge1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuXHJcbiAgICAgIGxldCBfb3B0aW9uczogaUludGVybmFsID0gPGlJbnRlcm5hbD5vcHRpb25zXHJcbiAgICAgIF9vcHRpb25zLmluZm9ybWF0aW9uT25seSA9IHRydWVcclxuICAgICAgX29wdGlvbnMudGl0bGUgPSBfb3B0aW9ucy50aXRsZSB8fCAnZm9mIMOgIHVuZSBpbmZvcm1hdGlvbidcclxuICAgICAgX29wdGlvbnMueWVzTGFiZWwgPSBfb3B0aW9ucy55ZXNMYWJlbCB8fCAnb2snXHJcblxyXG4gICAgICB0aGlzLm9wZW5ZZXNObyhfb3B0aW9ucylcclxuICAgICAgLnRoZW4ocmVzdWx0ID0+IHtcclxuICAgICAgICByZXNvbHZlKHJlc3VsdClcclxuICAgICAgfSlcclxuICAgIH0pXHJcbiAgfVxyXG59XHJcbiJdfQ==
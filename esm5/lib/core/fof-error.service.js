import { __assign } from "tslib";
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "./notification/notification.service";
var FofErrorService = /** @class */ (function () {
    function FofErrorService(fofNotificationService) {
        this.fofNotificationService = fofNotificationService;
    }
    /** Only for the core service, the error should be already cleaned */
    FofErrorService.prototype.cleanError = function (errorToManage) {
        var errorToReturn = errorToManage;
        var message;
        var isConstraintError = false;
        // console.log('errorToManage', errorToManage)
        if (errorToManage.error instanceof ErrorEvent) {
            // client-side error
            console.log('toDo: manage error');
            console.log('errorToManage', errorToManage);
            return errorToManage;
        }
        else {
            // HTTP or server-side error
            if (errorToManage.error) {
                if (errorToManage.error instanceof Object) {
                    if (errorToManage.error.fofCoreException) {
                        if (errorToManage.error.message instanceof Object) {
                            // it's an http rest error
                            var _error = errorToManage.error.message;
                            if (_error.error) {
                                errorToManage.error.statusText = _error.error;
                                errorToManage.error.message = _error.message;
                            }
                        }
                        else {
                            message = errorToManage.error.message;
                            if (message.search('constraint') > -1) {
                                isConstraintError = true;
                            }
                        }
                        return __assign({ isConstraintError: isConstraintError }, errorToManage.error);
                    }
                    else {
                        if (errorToManage.status == 0) {
                            console.error('FOF-UNKNOWN-ERROR', errorToManage.error);
                            errorToManage.error = errorToManage.message;
                            errorToManage.message = 'Impossible to contact the server';
                        }
                    }
                }
                else {
                    // the error to manage is comming from the httpClient
                    return errorToManage;
                }
            }
        }
        return errorToReturn;
    };
    FofErrorService.prototype.errorManage = function (errorToManage) {
        var message = errorToManage.message;
        if (errorToManage.error) {
            message = message + ("<br>" + errorToManage.error);
        }
        if (errorToManage.status) {
            switch (errorToManage.status) {
                case 401:
                case 403:
                    message = "Vous n'avez pas le droit d'acc\u00E9der \u00E0 ces ressources";
                    break;
                default:
                    message = "Oups, nous avons une erreur<br>\n          <small>" + message + "</small><br>";
                    break;
            }
        }
        this.fofNotificationService.error(message, { mustDisappearAfter: -1 });
    };
    FofErrorService.ɵfac = function FofErrorService_Factory(t) { return new (t || FofErrorService)(i0.ɵɵinject(i1.FofNotificationService)); };
    FofErrorService.ɵprov = i0.ɵɵdefineInjectable({ token: FofErrorService, factory: FofErrorService.ɵfac, providedIn: 'root' });
    return FofErrorService;
}());
export { FofErrorService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofErrorService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: i1.FofNotificationService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWVycm9yLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2ZvZi1lcnJvci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFBOzs7QUFTMUM7SUFLRSx5QkFDVSxzQkFBOEM7UUFBOUMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtJQUNwRCxDQUFDO0lBRUwscUVBQXFFO0lBQ3JFLG9DQUFVLEdBQVYsVUFBWSxhQUFrQjtRQUM1QixJQUFJLGFBQWEsR0FBdUIsYUFBYSxDQUFBO1FBQ3JELElBQUksT0FBZSxDQUFBO1FBQ25CLElBQUksaUJBQWlCLEdBQVksS0FBSyxDQUFBO1FBRXRDLDhDQUE4QztRQUU5QyxJQUFJLGFBQWEsQ0FBQyxLQUFLLFlBQVksVUFBVSxFQUFFO1lBQzdDLG9CQUFvQjtZQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLENBQUE7WUFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsYUFBYSxDQUFDLENBQUE7WUFDM0MsT0FBTyxhQUFhLENBQUE7U0FDckI7YUFBTTtZQUNMLDRCQUE0QjtZQUM1QixJQUFJLGFBQWEsQ0FBQyxLQUFLLEVBQUU7Z0JBQ3ZCLElBQUksYUFBYSxDQUFDLEtBQUssWUFBWSxNQUFNLEVBQUU7b0JBQ3pDLElBQUksYUFBYSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsRUFBRTt3QkFDeEMsSUFBSSxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sWUFBWSxNQUFNLEVBQUU7NEJBQ2xELDBCQUEwQjs0QkFDMUIsSUFBTSxNQUFNLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUE7NEJBQzFDLElBQUksTUFBTSxDQUFDLEtBQUssRUFBRTtnQ0FDakIsYUFBYSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQTtnQ0FDN0MsYUFBYSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQTs2QkFDNUM7eUJBQ0Q7NkJBQU07NEJBQ0wsT0FBTyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFBOzRCQUNyQyxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUc7Z0NBQ3RDLGlCQUFpQixHQUFHLElBQUksQ0FBQTs2QkFDekI7eUJBQ0Y7d0JBQ0Qsa0JBQ0UsaUJBQWlCLG1CQUFBLElBQ2QsYUFBYSxDQUFDLEtBQUssRUFDdkI7cUJBQ0Y7eUJBQU07d0JBQ0wsSUFBSSxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTs0QkFDN0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxtQkFBbUIsRUFBRSxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7NEJBQ3ZELGFBQWEsQ0FBQyxLQUFLLEdBQUcsYUFBYSxDQUFDLE9BQU8sQ0FBQTs0QkFDM0MsYUFBYSxDQUFDLE9BQU8sR0FBRyxrQ0FBa0MsQ0FBQTt5QkFDM0Q7cUJBQ0Y7aUJBQ0Y7cUJBQU07b0JBQ0wscURBQXFEO29CQUNyRCxPQUFPLGFBQWEsQ0FBQTtpQkFDckI7YUFDRjtTQUNGO1FBRUQsT0FBTyxhQUFhLENBQUE7SUFDdEIsQ0FBQztJQUVELHFDQUFXLEdBQVgsVUFBWSxhQUFpQztRQUMzQyxJQUFJLE9BQU8sR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFBO1FBQ25DLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUN2QixPQUFPLEdBQUcsT0FBTyxJQUFHLFNBQU8sYUFBYSxDQUFDLEtBQU8sQ0FBQSxDQUFBO1NBQ2pEO1FBQ0QsSUFBSSxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQ3hCLFFBQVEsYUFBYSxDQUFDLE1BQU0sRUFBRTtnQkFDNUIsS0FBSyxHQUFHLENBQUM7Z0JBQ1QsS0FBSyxHQUFHO29CQUNOLE9BQU8sR0FBRywrREFBcUQsQ0FBQTtvQkFDL0QsTUFBSztnQkFDUDtvQkFDRSxPQUFPLEdBQUcsdURBQ0QsT0FBTyxpQkFBYyxDQUFBO29CQUM5QixNQUFLO2FBQ1I7U0FDRjtRQUNELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFLEVBQUMsa0JBQWtCLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFBO0lBQ3RFLENBQUM7a0ZBNUVVLGVBQWU7MkRBQWYsZUFBZSxXQUFmLGVBQWUsbUJBRmQsTUFBTTswQkFWcEI7Q0F5RkMsQUFoRkQsSUFnRkM7U0E3RVksZUFBZTtrREFBZixlQUFlO2NBSDNCLFVBQVU7ZUFBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBpRm9mSHR0cEV4Y2VwdGlvbiB9IGZyb20gJy4vY29yZS5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZSdcclxuaW1wb3J0IHsgZXJyb3IgfSBmcm9tICdwcm90cmFjdG9yJ1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBpRm9mQ2xlYW5FeGNlcHRpb24gZXh0ZW5kcyBpRm9mSHR0cEV4Y2VwdGlvbiB7XHJcbiAgaXNDb25zdHJhaW50RXJyb3I6IGJvb2xlYW5cclxufVxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9mRXJyb3JTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2VcclxuICApIHsgfVxyXG5cclxuICAvKiogT25seSBmb3IgdGhlIGNvcmUgc2VydmljZSwgdGhlIGVycm9yIHNob3VsZCBiZSBhbHJlYWR5IGNsZWFuZWQgKi9cclxuICBjbGVhbkVycm9yIChlcnJvclRvTWFuYWdlOiBhbnkpOiBpRm9mQ2xlYW5FeGNlcHRpb24ge1xyXG4gICAgbGV0IGVycm9yVG9SZXR1cm46IGlGb2ZDbGVhbkV4Y2VwdGlvbiA9IGVycm9yVG9NYW5hZ2VcclxuICAgIGxldCBtZXNzYWdlOiBzdHJpbmdcclxuICAgIGxldCBpc0NvbnN0cmFpbnRFcnJvcjogYm9vbGVhbiA9IGZhbHNlXHJcbiAgICBcclxuICAgIC8vIGNvbnNvbGUubG9nKCdlcnJvclRvTWFuYWdlJywgZXJyb3JUb01hbmFnZSlcclxuXHJcbiAgICBpZiAoZXJyb3JUb01hbmFnZS5lcnJvciBpbnN0YW5jZW9mIEVycm9yRXZlbnQpIHtcclxuICAgICAgLy8gY2xpZW50LXNpZGUgZXJyb3JcclxuICAgICAgY29uc29sZS5sb2coJ3RvRG86IG1hbmFnZSBlcnJvcicpXHJcbiAgICAgIGNvbnNvbGUubG9nKCdlcnJvclRvTWFuYWdlJywgZXJyb3JUb01hbmFnZSlcclxuICAgICAgcmV0dXJuIGVycm9yVG9NYW5hZ2VcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIEhUVFAgb3Igc2VydmVyLXNpZGUgZXJyb3JcclxuICAgICAgaWYgKGVycm9yVG9NYW5hZ2UuZXJyb3IpIHtcclxuICAgICAgICBpZiAoZXJyb3JUb01hbmFnZS5lcnJvciBpbnN0YW5jZW9mIE9iamVjdCkge1xyXG4gICAgICAgICAgaWYgKGVycm9yVG9NYW5hZ2UuZXJyb3IuZm9mQ29yZUV4Y2VwdGlvbikge1xyXG4gICAgICAgICAgICBpZiAoZXJyb3JUb01hbmFnZS5lcnJvci5tZXNzYWdlIGluc3RhbmNlb2YgT2JqZWN0KSB7XHJcbiAgICAgICAgICAgICAvLyBpdCdzIGFuIGh0dHAgcmVzdCBlcnJvclxyXG4gICAgICAgICAgICAgY29uc3QgX2Vycm9yID0gZXJyb3JUb01hbmFnZS5lcnJvci5tZXNzYWdlXHJcbiAgICAgICAgICAgICBpZiAoX2Vycm9yLmVycm9yKSB7ICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgZXJyb3JUb01hbmFnZS5lcnJvci5zdGF0dXNUZXh0ID0gX2Vycm9yLmVycm9yXHJcbiAgICAgICAgICAgICAgZXJyb3JUb01hbmFnZS5lcnJvci5tZXNzYWdlID0gX2Vycm9yLm1lc3NhZ2VcclxuICAgICAgICAgICAgIH0gICAgICAgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIG1lc3NhZ2UgPSBlcnJvclRvTWFuYWdlLmVycm9yLm1lc3NhZ2VcclxuICAgICAgICAgICAgICBpZiAobWVzc2FnZS5zZWFyY2goJ2NvbnN0cmFpbnQnKSA+IC0xICkge1xyXG4gICAgICAgICAgICAgICAgaXNDb25zdHJhaW50RXJyb3IgPSB0cnVlXHJcbiAgICAgICAgICAgICAgfSAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfSAgICAgICAgICBcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICBpc0NvbnN0cmFpbnRFcnJvcixcclxuICAgICAgICAgICAgICAuLi5lcnJvclRvTWFuYWdlLmVycm9yXHJcbiAgICAgICAgICAgIH0gICAgICAgICAgICBcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmIChlcnJvclRvTWFuYWdlLnN0YXR1cyA9PSAwKSB7ICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKCdGT0YtVU5LTk9XTi1FUlJPUicsIGVycm9yVG9NYW5hZ2UuZXJyb3IpXHJcbiAgICAgICAgICAgICAgZXJyb3JUb01hbmFnZS5lcnJvciA9IGVycm9yVG9NYW5hZ2UubWVzc2FnZVxyXG4gICAgICAgICAgICAgIGVycm9yVG9NYW5hZ2UubWVzc2FnZSA9ICdJbXBvc3NpYmxlIHRvIGNvbnRhY3QgdGhlIHNlcnZlcidcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAvLyB0aGUgZXJyb3IgdG8gbWFuYWdlIGlzIGNvbW1pbmcgZnJvbSB0aGUgaHR0cENsaWVudFxyXG4gICAgICAgICAgcmV0dXJuIGVycm9yVG9NYW5hZ2VcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gIFxyXG5cclxuICAgIHJldHVybiBlcnJvclRvUmV0dXJuXHJcbiAgfVxyXG5cclxuICBlcnJvck1hbmFnZShlcnJvclRvTWFuYWdlOiBpRm9mQ2xlYW5FeGNlcHRpb24pIHtcclxuICAgIGxldCBtZXNzYWdlID0gZXJyb3JUb01hbmFnZS5tZXNzYWdlXHJcbiAgICBpZiAoZXJyb3JUb01hbmFnZS5lcnJvcikge1xyXG4gICAgICBtZXNzYWdlID0gbWVzc2FnZSArIGA8YnI+JHtlcnJvclRvTWFuYWdlLmVycm9yfWBcclxuICAgIH1cclxuICAgIGlmIChlcnJvclRvTWFuYWdlLnN0YXR1cykge1xyXG4gICAgICBzd2l0Y2ggKGVycm9yVG9NYW5hZ2Uuc3RhdHVzKSB7XHJcbiAgICAgICAgY2FzZSA0MDE6XHJcbiAgICAgICAgY2FzZSA0MDM6XHJcbiAgICAgICAgICBtZXNzYWdlID0gYFZvdXMgbidhdmV6IHBhcyBsZSBkcm9pdCBkJ2FjY8OpZGVyIMOgIGNlcyByZXNzb3VyY2VzYFxyXG4gICAgICAgICAgYnJlYWtcclxuICAgICAgICBkZWZhdWx0OiAgICAgICAgICBcclxuICAgICAgICAgIG1lc3NhZ2UgPSBgT3Vwcywgbm91cyBhdm9ucyB1bmUgZXJyZXVyPGJyPlxyXG4gICAgICAgICAgPHNtYWxsPiR7bWVzc2FnZX08L3NtYWxsPjxicj5gICAgICAgICAgIFxyXG4gICAgICAgICAgYnJlYWtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKG1lc3NhZ2UsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IC0xfSlcclxuICB9XHJcbn1cclxuIl19
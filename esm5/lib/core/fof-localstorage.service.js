import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
var FofLocalstorageService = /** @class */ (function () {
    function FofLocalstorageService(fofConfig) {
        this.fofConfig = fofConfig;
        this.appShortName = this.fofConfig.appName.technical.toLocaleUpperCase() + "-";
    }
    FofLocalstorageService.prototype.setItem = function (key, value) {
        localStorage.setItem("" + this.appShortName + key, JSON.stringify(value));
    };
    FofLocalstorageService.prototype.getItem = function (key) {
        return JSON.parse(localStorage.getItem("" + this.appShortName + key));
    };
    FofLocalstorageService.prototype.removeItem = function (key) {
        localStorage.removeItem("" + this.appShortName + key);
    };
    FofLocalstorageService.ɵfac = function FofLocalstorageService_Factory(t) { return new (t || FofLocalstorageService)(i0.ɵɵinject(CORE_CONFIG)); };
    FofLocalstorageService.ɵprov = i0.ɵɵdefineInjectable({ token: FofLocalstorageService, factory: FofLocalstorageService.ɵfac, providedIn: 'root' });
    return FofLocalstorageService;
}());
export { FofLocalstorageService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofLocalstorageService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLWxvY2Fsc3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9mb2YtbG9jYWxzdG9yYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDbEQsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7QUFFdkQ7SUFLRSxnQ0FDK0IsU0FBcUI7UUFBckIsY0FBUyxHQUFULFNBQVMsQ0FBWTtRQUVsRCxJQUFJLENBQUMsWUFBWSxHQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxNQUFHLENBQUE7SUFDaEYsQ0FBQztJQUlELHdDQUFPLEdBQVAsVUFBUSxHQUFXLEVBQUUsS0FBVTtRQUM3QixZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUcsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFLLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFBO0lBQzNFLENBQUM7SUFFRCx3Q0FBTyxHQUFQLFVBQVEsR0FBVztRQUNqQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxLQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBSyxDQUFDLENBQUMsQ0FBQTtJQUN2RSxDQUFDO0lBRUQsMkNBQVUsR0FBVixVQUFXLEdBQVc7UUFDcEIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxLQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBSyxDQUFDLENBQUE7SUFDdkQsQ0FBQztnR0FwQlUsc0JBQXNCLGNBR3ZCLFdBQVc7a0VBSFYsc0JBQXNCLFdBQXRCLHNCQUFzQixtQkFGckIsTUFBTTtpQ0FKcEI7Q0EyQkMsQUF4QkQsSUF3QkM7U0FyQlksc0JBQXNCO2tEQUF0QixzQkFBc0I7Y0FIbEMsVUFBVTtlQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COztzQkFJSSxNQUFNO3VCQUFDLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBJZm9mQ29uZmlnLCBDT1JFX0NPTkZJRyB9IGZyb20gJy4uL2ZvZi1jb25maWcnXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZMb2NhbHN0b3JhZ2VTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KENPUkVfQ09ORklHKSBwcml2YXRlIGZvZkNvbmZpZzogSWZvZkNvbmZpZ1xyXG4gICkgeyBcclxuICAgIHRoaXMuYXBwU2hvcnROYW1lID0gYCR7dGhpcy5mb2ZDb25maWcuYXBwTmFtZS50ZWNobmljYWwudG9Mb2NhbGVVcHBlckNhc2UoKX0tYFxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBhcHBTaG9ydE5hbWU6IHN0cmluZ1xyXG5cclxuICBzZXRJdGVtKGtleTogc3RyaW5nLCB2YWx1ZTogYW55KSB7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShgJHt0aGlzLmFwcFNob3J0TmFtZX0ke2tleX1gLCBKU09OLnN0cmluZ2lmeSh2YWx1ZSkpXHJcbiAgfVxyXG5cclxuICBnZXRJdGVtKGtleTogc3RyaW5nKSB7XHJcbiAgICByZXR1cm4gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShgJHt0aGlzLmFwcFNob3J0TmFtZX0ke2tleX1gKSlcclxuICB9XHJcblxyXG4gIHJlbW92ZUl0ZW0oa2V5OiBzdHJpbmcpIHtcclxuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKGAke3RoaXMuYXBwU2hvcnROYW1lfSR7a2V5fWApXHJcbiAgfVxyXG59XHJcbiJdfQ==
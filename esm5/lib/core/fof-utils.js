var _this = this;
import { __assign } from "tslib";
import { FormGroup, FormControl } from '@angular/forms';
export var fofUtilsForm = {
    validateAllFields: function (formGroup) {
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    }
};
export var fofError = {
    clean: function (errorToManage) {
        var error;
        var message;
        var isConstraintError = false;
        if (errorToManage.error) {
            error = errorToManage.error;
            if (error.fofCoreException) {
                message = error.exceptionDetail.message;
                if (message.search('constraint') > -1) {
                    isConstraintError = true;
                }
                return __assign({ isConstraintError: isConstraintError }, error);
            }
        }
    },
    cleanAndMange: function (errorToManage) {
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9mLXV0aWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9mb2YtdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsaUJBNENBOztBQTVDQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBT3ZELE1BQU0sQ0FBQyxJQUFNLFlBQVksR0FBRztJQUMxQixpQkFBaUIsRUFBQyxVQUFDLFNBQW9CO1FBQ3JDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7WUFDM0MsSUFBTSxPQUFPLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQyxJQUFJLE9BQU8sWUFBWSxXQUFXLEVBQUU7Z0JBQ2xDLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQzthQUMzQztpQkFBTSxJQUFJLE9BQU8sWUFBWSxTQUFTLEVBQUU7Z0JBQ3ZDLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNyQztRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztDQUNGLENBQUE7QUFFRCxNQUFNLENBQUMsSUFBTSxRQUFRLEdBQUc7SUFDdEIsS0FBSyxFQUFDLFVBQUMsYUFBa0I7UUFDdkIsSUFBSSxLQUFLLENBQUE7UUFDVCxJQUFJLE9BQWUsQ0FBQTtRQUNuQixJQUFJLGlCQUFpQixHQUFZLEtBQUssQ0FBQTtRQUN0QyxJQUFJLGFBQWEsQ0FBQyxLQUFLLEVBQUU7WUFDdkIsS0FBSyxHQUFzQixhQUFhLENBQUMsS0FBSyxDQUFBO1lBQzlDLElBQUksS0FBSyxDQUFDLGdCQUFnQixFQUFFO2dCQUMxQixPQUFPLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUE7Z0JBQ3ZDLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRztvQkFDdEMsaUJBQWlCLEdBQUcsSUFBSSxDQUFBO2lCQUN6QjtnQkFDRCxrQkFDRSxpQkFBaUIsbUJBQUEsSUFDZCxLQUFLLEVBQ1Q7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUNELGFBQWEsRUFBQyxVQUFDLGFBQWtCO0lBRWpDLENBQUM7Q0FDRixDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJ1xyXG5pbXBvcnQgeyBpRm9mSHR0cEV4Y2VwdGlvbiB9IGZyb20gJy4vY29yZS5pbnRlcmZhY2UnXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIGlGb2ZDbGVhbkV4Y2VwdGlvbiBleHRlbmRzIGlGb2ZIdHRwRXhjZXB0aW9uIHtcclxuICBpc0NvbnN0cmFpbnRFcnJvcjogYm9vbGVhblxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgZm9mVXRpbHNGb3JtID0ge1xyXG4gIHZhbGlkYXRlQWxsRmllbGRzOihmb3JtR3JvdXA6IEZvcm1Hcm91cCkgPT4geyAgICAgICAgIFxyXG4gICAgT2JqZWN0LmtleXMoZm9ybUdyb3VwLmNvbnRyb2xzKS5mb3JFYWNoKGZpZWxkID0+IHsgIFxyXG4gICAgICBjb25zdCBjb250cm9sID0gZm9ybUdyb3VwLmdldChmaWVsZCk7ICAgICAgICAgICAgIFxyXG4gICAgICBpZiAoY29udHJvbCBpbnN0YW5jZW9mIEZvcm1Db250cm9sKSB7ICAgICAgICAgICAgIFxyXG4gICAgICAgIGNvbnRyb2wubWFya0FzVG91Y2hlZCh7IG9ubHlTZWxmOiB0cnVlIH0pO1xyXG4gICAgICB9IGVsc2UgaWYgKGNvbnRyb2wgaW5zdGFuY2VvZiBGb3JtR3JvdXApIHsgICAgICAgIFxyXG4gICAgICAgIHRoaXMudmFsaWRhdGVBbGxGb3JtRmllbGRzKGNvbnRyb2wpOyAgICAgICAgICAgIFxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IGZvZkVycm9yID0ge1xyXG4gIGNsZWFuOihlcnJvclRvTWFuYWdlOiBhbnkpOiBpRm9mQ2xlYW5FeGNlcHRpb24gPT4ge1xyXG4gICAgbGV0IGVycm9yXHJcbiAgICBsZXQgbWVzc2FnZTogc3RyaW5nXHJcbiAgICBsZXQgaXNDb25zdHJhaW50RXJyb3I6IGJvb2xlYW4gPSBmYWxzZVxyXG4gICAgaWYgKGVycm9yVG9NYW5hZ2UuZXJyb3IpIHtcclxuICAgICAgZXJyb3IgPSA8aUZvZkh0dHBFeGNlcHRpb24+ZXJyb3JUb01hbmFnZS5lcnJvclxyXG4gICAgICBpZiAoZXJyb3IuZm9mQ29yZUV4Y2VwdGlvbikge1xyXG4gICAgICAgIG1lc3NhZ2UgPSBlcnJvci5leGNlcHRpb25EZXRhaWwubWVzc2FnZVxyXG4gICAgICAgIGlmIChtZXNzYWdlLnNlYXJjaCgnY29uc3RyYWludCcpID4gLTEgKSB7XHJcbiAgICAgICAgICBpc0NvbnN0cmFpbnRFcnJvciA9IHRydWVcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgIGlzQ29uc3RyYWludEVycm9yLFxyXG4gICAgICAgICAgLi4uZXJyb3JcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9LFxyXG4gIGNsZWFuQW5kTWFuZ2U6KGVycm9yVG9NYW5hZ2U6IGFueSkgPT4ge1xyXG4gICAgXHJcbiAgfVxyXG59XHJcblxyXG4iXX0=
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./fof-error.service";
var FofErrorInterceptor = /** @class */ (function () {
    function FofErrorInterceptor(fofErrorService) {
        this.fofErrorService = fofErrorService;
    }
    FofErrorInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request)
            .pipe(catchError(function (httpError) {
            return throwError(_this.fofErrorService.cleanError(httpError));
        }));
    };
    FofErrorInterceptor.ɵfac = function FofErrorInterceptor_Factory(t) { return new (t || FofErrorInterceptor)(i0.ɵɵinject(i1.FofErrorService)); };
    FofErrorInterceptor.ɵprov = i0.ɵɵdefineInjectable({ token: FofErrorInterceptor, factory: FofErrorInterceptor.ɵfac });
    return FofErrorInterceptor;
}());
export { FofErrorInterceptor };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofErrorInterceptor, [{
        type: Injectable
    }], function () { return [{ type: i1.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC1lcnJvci5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvaHR0cC1lcnJvci5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBRTFDLE9BQU8sRUFBYyxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUE7QUFDN0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFBOzs7QUFHM0M7SUFFRSw2QkFDVSxlQUFnQztRQUFoQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7SUFDdEMsQ0FBQztJQUVMLHVDQUFTLEdBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO1FBQXRELGlCQU9DO1FBTkMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQzthQUN4QixJQUFJLENBQ0gsVUFBVSxDQUFDLFVBQUMsU0FBNEI7WUFDdEMsT0FBTyxVQUFVLENBQUMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQTtRQUMvRCxDQUFDLENBQUMsQ0FDSCxDQUFBO0lBQ0wsQ0FBQzswRkFaVSxtQkFBbUI7K0RBQW5CLG1CQUFtQixXQUFuQixtQkFBbUI7OEJBUGhDO0NBb0JDLEFBZEQsSUFjQztTQWJZLG1CQUFtQjtrREFBbkIsbUJBQW1CO2NBRC9CLFVBQVUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgSHR0cFJlcXVlc3QsIEh0dHBIYW5kbGVyLCBIdHRwRXZlbnQsIEh0dHBJbnRlcmNlcHRvciwgSHR0cEVycm9yUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCdcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnXHJcbmltcG9ydCB7IGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycydcclxuaW1wb3J0IHsgRm9mRXJyb3JTZXJ2aWNlIH0gZnJvbSAnLi9mb2YtZXJyb3Iuc2VydmljZSdcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEZvZkVycm9ySW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIGNvbnN0cnVjdG9yICggICAgXHJcbiAgICBwcml2YXRlIGZvZkVycm9yU2VydmljZTogRm9mRXJyb3JTZXJ2aWNlXHJcbiAgKSB7IH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4geyAgICBcclxuICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KVxyXG4gICAgICAucGlwZSggICAgICAgICAgICAgICAgXHJcbiAgICAgICAgY2F0Y2hFcnJvcigoaHR0cEVycm9yOiBIdHRwRXJyb3JSZXNwb25zZSkgPT4geyAgICAgICAgICBcclxuICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKHRoaXMuZm9mRXJyb3JTZXJ2aWNlLmNsZWFuRXJyb3IoaHR0cEVycm9yKSkgICAgICAgICAgXHJcbiAgICAgICAgfSlcclxuICAgICAgKSAgICAgIFxyXG4gIH0gXHJcbn0iXX0=
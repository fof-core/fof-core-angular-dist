import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "./auth.service";
var FoFJwtInterceptor = /** @class */ (function () {
    function FoFJwtInterceptor(foFAuthService, fofConfig) {
        this.foFAuthService = foFAuthService;
        this.fofConfig = fofConfig;
        this.environment = this.fofConfig.environment;
    }
    FoFJwtInterceptor.prototype.intercept = function (request, next) {
        // add auth header with jwt if user is logged in and request is to api url    
        var currentUser = this.foFAuthService.currentUser;
        var isLoggedIn = currentUser && currentUser.accessToken;
        var isApiUrl = request.url.startsWith(this.environment.apiPath);
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + currentUser.accessToken
                }
            });
        }
        return next.handle(request);
    };
    FoFJwtInterceptor.ɵfac = function FoFJwtInterceptor_Factory(t) { return new (t || FoFJwtInterceptor)(i0.ɵɵinject(i1.FoFAuthService), i0.ɵɵinject(CORE_CONFIG)); };
    FoFJwtInterceptor.ɵprov = i0.ɵɵdefineInjectable({ token: FoFJwtInterceptor, factory: FoFJwtInterceptor.ɵfac });
    return FoFJwtInterceptor;
}());
export { FoFJwtInterceptor };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FoFJwtInterceptor, [{
        type: Injectable
    }], function () { return [{ type: i1.FoFAuthService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvY29yZS9qd3QuaW50ZXJjZXB0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFLbEQsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7O0FBRXZEO0lBSUUsMkJBQ1UsY0FBOEIsRUFDVCxTQUFxQjtRQUQxQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDVCxjQUFTLEdBQVQsU0FBUyxDQUFZO1FBRWxELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUE7SUFDL0MsQ0FBQztJQUVELHFDQUFTLEdBQVQsVUFBVSxPQUF5QixFQUFFLElBQWlCO1FBQ3BELDhFQUE4RTtRQUM5RSxJQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQTtRQUNuRCxJQUFNLFVBQVUsR0FBRyxXQUFXLElBQUksV0FBVyxDQUFDLFdBQVcsQ0FBQTtRQUN6RCxJQUFNLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQ2pFLElBQUksVUFBVSxJQUFJLFFBQVEsRUFBRTtZQUN4QixPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFDcEIsVUFBVSxFQUFFO29CQUNWLGFBQWEsRUFBRSxZQUFVLFdBQVcsQ0FBQyxXQUFhO2lCQUNuRDthQUNKLENBQUMsQ0FBQTtTQUNMO1FBRUQsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO0lBQzdCLENBQUM7c0ZBeEJVLGlCQUFpQiw4Q0FLbEIsV0FBVzs2REFMVixpQkFBaUIsV0FBakIsaUJBQWlCOzRCQVI5QjtDQWlDQyxBQTFCRCxJQTBCQztTQXpCWSxpQkFBaUI7a0RBQWpCLGlCQUFpQjtjQUQ3QixVQUFVOztzQkFNTixNQUFNO3VCQUFDLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBIdHRwUmVxdWVzdCwgSHR0cEhhbmRsZXIsIEh0dHBFdmVudCwgSHR0cEludGVyY2VwdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnXHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJ1xyXG4vLyBpbXBvcnQgeyBpVXNlciB9IGZyb20gJy4uL3NoYXJlZC91c2VyLmludGVyZmFjZSdcclxuaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UgfSBmcm9tICcuL2F1dGguc2VydmljZSdcclxuaW1wb3J0IHsgSWZvZkNvbmZpZywgQ09SRV9DT05GSUcgfSBmcm9tICcuLi9mb2YtY29uZmlnJ1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgRm9GSnd0SW50ZXJjZXB0b3IgaW1wbGVtZW50cyBIdHRwSW50ZXJjZXB0b3Ige1xyXG4gIHByaXZhdGUgZW52aXJvbm1lbnQ6YW55XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmb0ZBdXRoU2VydmljZTogRm9GQXV0aFNlcnZpY2UsXHJcbiAgICBASW5qZWN0KENPUkVfQ09ORklHKSBwcml2YXRlIGZvZkNvbmZpZzogSWZvZkNvbmZpZ1xyXG4gICkgeyBcclxuICAgIHRoaXMuZW52aXJvbm1lbnQgPSB0aGlzLmZvZkNvbmZpZy5lbnZpcm9ubWVudFxyXG4gIH1cclxuXHJcbiAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG4gICAgLy8gYWRkIGF1dGggaGVhZGVyIHdpdGggand0IGlmIHVzZXIgaXMgbG9nZ2VkIGluIGFuZCByZXF1ZXN0IGlzIHRvIGFwaSB1cmwgICAgXHJcbiAgICBjb25zdCBjdXJyZW50VXNlciA9IHRoaXMuZm9GQXV0aFNlcnZpY2UuY3VycmVudFVzZXJcclxuICAgIGNvbnN0IGlzTG9nZ2VkSW4gPSBjdXJyZW50VXNlciAmJiBjdXJyZW50VXNlci5hY2Nlc3NUb2tlblxyXG4gICAgY29uc3QgaXNBcGlVcmwgPSByZXF1ZXN0LnVybC5zdGFydHNXaXRoKHRoaXMuZW52aXJvbm1lbnQuYXBpUGF0aClcclxuICAgIGlmIChpc0xvZ2dlZEluICYmIGlzQXBpVXJsKSB7XHJcbiAgICAgICAgcmVxdWVzdCA9IHJlcXVlc3QuY2xvbmUoe1xyXG4gICAgICAgICAgICBzZXRIZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke2N1cnJlbnRVc2VyLmFjY2Vzc1Rva2VufWBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpXHJcbiAgfVxyXG59Il19
var Notification = /** @class */ (function () {
    function Notification() {
    }
    return Notification;
}());
export { Notification };
export var NotificationType;
(function (NotificationType) {
    NotificationType[NotificationType["Success"] = 0] = "Success";
    NotificationType[NotificationType["Error"] = 1] = "Error";
    NotificationType[NotificationType["Info"] = 2] = "Info";
    NotificationType[NotificationType["Warning"] = 3] = "Warning";
})(NotificationType || (NotificationType = {}));
/** params for notification */
var NotificationParams = /** @class */ (function () {
    function NotificationParams() {
    }
    return NotificationParams;
}());
export { NotificationParams };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLXR5cGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24tdHlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUFBO0lBSUEsQ0FBQztJQUFELG1CQUFDO0FBQUQsQ0FBQyxBQUpELElBSUM7O0FBRUQsTUFBTSxDQUFOLElBQVksZ0JBS1g7QUFMRCxXQUFZLGdCQUFnQjtJQUMxQiw2REFBTyxDQUFBO0lBQ1AseURBQUssQ0FBQTtJQUNMLHVEQUFJLENBQUE7SUFDSiw2REFBTyxDQUFBO0FBQ1QsQ0FBQyxFQUxXLGdCQUFnQixLQUFoQixnQkFBZ0IsUUFLM0I7QUFFRCw4QkFBOEI7QUFDOUI7SUFBQTtJQU1BLENBQUM7SUFBRCx5QkFBQztBQUFELENBQUMsQUFORCxJQU1DIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbiB7XHJcbiAgdHlwZTogTm90aWZpY2F0aW9uVHlwZVxyXG4gIG1lc3NhZ2U6IHN0cmluZ1xyXG4gIG11c3REaXNhcHBlYXJBZnRlcj86IG51bWJlclxyXG59XHJcblxyXG5leHBvcnQgZW51bSBOb3RpZmljYXRpb25UeXBlIHtcclxuICBTdWNjZXNzLFxyXG4gIEVycm9yLFxyXG4gIEluZm8sXHJcbiAgV2FybmluZ1xyXG59XHJcblxyXG4vKiogcGFyYW1zIGZvciBub3RpZmljYXRpb24gKi9cclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvblBhcmFtcyB7XHJcbiAgLyoqIEtlZXAgdGhlIG5vdGlmaWNhdGlvbiBhY3RpdmUsIGV2ZW4gaWYgdGhlIHJvdXRlIGNoYW5nZSAqL1xyXG4gIGtlZXBBZnRlclJvdXRlQ2hhbmdlPzogYm9vbGVhblxyXG4gIC8qKiBOdW1iZXIgaW4gbWlsbGlzZWNvbmQuIEJ5IGRlZmF1bHQsIDMwMDAuIEZvciBwcmV2ZW50aW5nIHRoZSBub3RpZmljYXRpb24gdG8gXHJcbiAgICogZGlzZWFwZXIsIHNldCBpdCB3aXRoIGEgbmVnYXRpdmUgbnVtYmVyICovXHJcbiAgbXVzdERpc2FwcGVhckFmdGVyPzogbnVtYmVyXHJcbn0iXX0=
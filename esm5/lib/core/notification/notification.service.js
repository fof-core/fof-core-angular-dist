import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Notification, NotificationType } from './notification-type';
import * as i0 from "@angular/core";
var FofNotificationService = /** @class */ (function () {
    function FofNotificationService() {
        this.saveIsDoneNotification = new BehaviorSubject(undefined);
        this.subject = new Subject();
        this.keepAfterRouteChange = false;
        // const router = this.injector.get(Router)
        // clear notification messages on route change unless 'keepAfterRouteChange' flag is true
        // router.events.subscribe(event => {
        //   if (event instanceof NavigationStart) {
        //     if (this.keepAfterRouteChange) {
        //       // only keep for a single route change
        //       this.keepAfterRouteChange = false
        //     } else {
        //       // clear notification messages
        //       this.clear()
        //     }
        //   }
        // })
    }
    Object.defineProperty(FofNotificationService.prototype, "savedNotification", {
        get: function () {
            return this.saveIsDoneNotification.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    FofNotificationService.prototype.getnotification = function () {
        return this.subject.asObservable();
    };
    /** Notifiy a success
     * @param message
     * accept html
     */
    FofNotificationService.prototype.success = function (message, params) {
        this.notification(NotificationType.Success, message, params);
    };
    /** Notifiy an error
     * @param message
     * accept html
     */
    FofNotificationService.prototype.error = function (message, params) {
        this.notification(NotificationType.Error, message, params);
    };
    /** Notifiy an info
     * @param message
     * accept html
     */
    FofNotificationService.prototype.info = function (message, params) {
        this.notification(NotificationType.Info, message, params);
    };
    /** Notifiy a warning
     * @param message
     * accept html
     */
    FofNotificationService.prototype.warn = function (message, params) {
        this.notification(NotificationType.Warning, message, params);
    };
    FofNotificationService.prototype.notification = function (type, message, params) {
        var notification = new Notification();
        if (params) {
            if (params.keepAfterRouteChange) {
                this.keepAfterRouteChange = params.keepAfterRouteChange;
            }
            if (params.mustDisappearAfter) {
                if (params.mustDisappearAfter < 0) {
                    params.mustDisappearAfter = undefined;
                }
                else {
                    notification.mustDisappearAfter = params.mustDisappearAfter;
                }
            }
            else {
                notification.mustDisappearAfter = 3000;
            }
        }
        else {
            notification.mustDisappearAfter = 3000;
        }
        notification.type = type;
        notification.message = message;
        this.subject.next(notification);
    };
    FofNotificationService.prototype.saveIsDone = function () {
        this.saveIsDoneNotification.next(true);
    };
    FofNotificationService.prototype.clear = function () {
        // clear notifications
        this.subject.next();
    };
    FofNotificationService.ɵfac = function FofNotificationService_Factory(t) { return new (t || FofNotificationService)(); };
    FofNotificationService.ɵprov = i0.ɵɵdefineInjectable({ token: FofNotificationService, factory: FofNotificationService.ɵfac, providedIn: 'root' });
    return FofNotificationService;
}());
export { FofNotificationService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofNotificationService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFZLE1BQU0sZUFBZSxDQUFBO0FBRXBELE9BQU8sRUFBRSxlQUFlLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFBO0FBQzNELE9BQU8sRUFBRSxZQUFZLEVBQXNCLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUE7O0FBRXhGO0lBU0U7UUFMTywyQkFBc0IsR0FBRyxJQUFJLGVBQWUsQ0FBTSxTQUFTLENBQUMsQ0FBQTtRQUUzRCxZQUFPLEdBQUcsSUFBSSxPQUFPLEVBQWdCLENBQUE7UUFDckMseUJBQW9CLEdBQUcsS0FBSyxDQUFBO1FBTWxDLDJDQUEyQztRQUMzQyx5RkFBeUY7UUFDekYscUNBQXFDO1FBQ3JDLDRDQUE0QztRQUM1Qyx1Q0FBdUM7UUFDdkMsK0NBQStDO1FBQy9DLDBDQUEwQztRQUMxQyxlQUFlO1FBQ2YsdUNBQXVDO1FBQ3ZDLHFCQUFxQjtRQUNyQixRQUFRO1FBQ1IsTUFBTTtRQUNOLEtBQUs7SUFDUCxDQUFDO0lBRUQsc0JBQUkscURBQWlCO2FBQXJCO1lBQ0UsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxFQUFFLENBQUE7UUFDbkQsQ0FBQzs7O09BQUE7SUFFRCxnREFBZSxHQUFmO1FBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFBO0lBQ3BDLENBQUM7SUFFRDs7O09BR0c7SUFDSCx3Q0FBTyxHQUFQLFVBQVEsT0FBZSxFQUFFLE1BQTJCO1FBQ2xELElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQTtJQUM5RCxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsc0NBQUssR0FBTCxVQUFNLE9BQWUsRUFBRSxNQUEyQjtRQUNoRCxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFDNUQsQ0FBQztJQUVEOzs7T0FHRztJQUNILHFDQUFJLEdBQUosVUFBSyxPQUFlLEVBQUUsTUFBMkI7UUFDL0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFBO0lBQzNELENBQUM7SUFFRDs7O09BR0c7SUFDSCxxQ0FBSSxHQUFKLFVBQUssT0FBZSxFQUFFLE1BQTJCO1FBQy9DLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQTtJQUM5RCxDQUFDO0lBR0QsNkNBQVksR0FBWixVQUFhLElBQXNCLEVBQUUsT0FBZSxFQUFFLE1BQTJCO1FBQy9FLElBQU0sWUFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUE7UUFFdkMsSUFBSSxNQUFNLEVBQUU7WUFDVixJQUFJLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQTthQUN4RDtZQUNELElBQUksTUFBTSxDQUFDLGtCQUFrQixFQUFFO2dCQUM3QixJQUFJLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxDQUFDLEVBQUU7b0JBQ2pDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLENBQUE7aUJBQ3RDO3FCQUFNO29CQUNMLFlBQVksQ0FBQyxrQkFBa0IsR0FBRyxNQUFNLENBQUMsa0JBQWtCLENBQUE7aUJBQzVEO2FBQ0Y7aUJBQU07Z0JBQ0wsWUFBWSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQTthQUN2QztTQUNGO2FBQU07WUFDTCxZQUFZLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFBO1NBQ3ZDO1FBRUQsWUFBWSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7UUFDeEIsWUFBWSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUE7UUFFOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7SUFDakMsQ0FBQztJQUVELDJDQUFVLEdBQVY7UUFDRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO0lBQ3hDLENBQUM7SUFFRCxzQ0FBSyxHQUFMO1FBQ0Usc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUE7SUFDckIsQ0FBQztnR0FuR1Usc0JBQXNCO2tFQUF0QixzQkFBc0IsV0FBdEIsc0JBQXNCLG1CQUZyQixNQUFNO2lDQU5wQjtDQTRHQyxBQXZHRCxJQXVHQztTQXBHWSxzQkFBc0I7a0RBQXRCLHNCQUFzQjtjQUhsQyxVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IE5hdmlnYXRpb25TdGFydCwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUsIFN1YmplY3QgfSBmcm9tICdyeGpzJ1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb24sIE5vdGlmaWNhdGlvblBhcmFtcywgTm90aWZpY2F0aW9uVHlwZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uLXR5cGUnXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIHtcclxuICBwdWJsaWMgc2F2ZUlzRG9uZU5vdGlmaWNhdGlvbiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55Pih1bmRlZmluZWQpXHJcblxyXG4gIHByaXZhdGUgc3ViamVjdCA9IG5ldyBTdWJqZWN0PE5vdGlmaWNhdGlvbj4oKVxyXG4gIHByaXZhdGUga2VlcEFmdGVyUm91dGVDaGFuZ2UgPSBmYWxzZVxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIC8vIHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcclxuICAgIC8vIHByaXZhdGUgaW5qZWN0b3I6IEluamVjdG9yICAgIFxyXG4gICkge1xyXG4gICAgLy8gY29uc3Qgcm91dGVyID0gdGhpcy5pbmplY3Rvci5nZXQoUm91dGVyKVxyXG4gICAgLy8gY2xlYXIgbm90aWZpY2F0aW9uIG1lc3NhZ2VzIG9uIHJvdXRlIGNoYW5nZSB1bmxlc3MgJ2tlZXBBZnRlclJvdXRlQ2hhbmdlJyBmbGFnIGlzIHRydWVcclxuICAgIC8vIHJvdXRlci5ldmVudHMuc3Vic2NyaWJlKGV2ZW50ID0+IHtcclxuICAgIC8vICAgaWYgKGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvblN0YXJ0KSB7XHJcbiAgICAvLyAgICAgaWYgKHRoaXMua2VlcEFmdGVyUm91dGVDaGFuZ2UpIHtcclxuICAgIC8vICAgICAgIC8vIG9ubHkga2VlcCBmb3IgYSBzaW5nbGUgcm91dGUgY2hhbmdlXHJcbiAgICAvLyAgICAgICB0aGlzLmtlZXBBZnRlclJvdXRlQ2hhbmdlID0gZmFsc2VcclxuICAgIC8vICAgICB9IGVsc2Uge1xyXG4gICAgLy8gICAgICAgLy8gY2xlYXIgbm90aWZpY2F0aW9uIG1lc3NhZ2VzXHJcbiAgICAvLyAgICAgICB0aGlzLmNsZWFyKClcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgIH1cclxuICAgIC8vIH0pXHJcbiAgfVxyXG5cclxuICBnZXQgc2F2ZWROb3RpZmljYXRpb24gKCkge1xyXG4gICAgcmV0dXJuIHRoaXMuc2F2ZUlzRG9uZU5vdGlmaWNhdGlvbi5hc09ic2VydmFibGUoKVxyXG4gIH1cclxuXHJcbiAgZ2V0bm90aWZpY2F0aW9uKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zdWJqZWN0LmFzT2JzZXJ2YWJsZSgpXHJcbiAgfVxyXG5cclxuICAvKiogTm90aWZpeSBhIHN1Y2Nlc3MgXHJcbiAgICogQHBhcmFtIG1lc3NhZ2VcclxuICAgKiBhY2NlcHQgaHRtbFxyXG4gICAqL1xyXG4gIHN1Y2Nlc3MobWVzc2FnZTogc3RyaW5nLCBwYXJhbXM/OiBOb3RpZmljYXRpb25QYXJhbXMpIHtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvblR5cGUuU3VjY2VzcywgbWVzc2FnZSwgcGFyYW1zKVxyXG4gIH1cclxuXHJcbiAgLyoqIE5vdGlmaXkgYW4gZXJyb3IgXHJcbiAgICogQHBhcmFtIG1lc3NhZ2VcclxuICAgKiBhY2NlcHQgaHRtbFxyXG4gICAqL1xyXG4gIGVycm9yKG1lc3NhZ2U6IHN0cmluZywgcGFyYW1zPzogTm90aWZpY2F0aW9uUGFyYW1zKSB7XHJcbiAgICB0aGlzLm5vdGlmaWNhdGlvbihOb3RpZmljYXRpb25UeXBlLkVycm9yLCBtZXNzYWdlLCBwYXJhbXMpXHJcbiAgfVxyXG5cclxuICAvKiogTm90aWZpeSBhbiBpbmZvXHJcbiAgICogQHBhcmFtIG1lc3NhZ2VcclxuICAgKiBhY2NlcHQgaHRtbFxyXG4gICAqL1xyXG4gIGluZm8obWVzc2FnZTogc3RyaW5nLCBwYXJhbXM/OiBOb3RpZmljYXRpb25QYXJhbXMpIHtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvblR5cGUuSW5mbywgbWVzc2FnZSwgcGFyYW1zKVxyXG4gIH1cclxuXHJcbiAgLyoqIE5vdGlmaXkgYSB3YXJuaW5nXHJcbiAgICogQHBhcmFtIG1lc3NhZ2VcclxuICAgKiBhY2NlcHQgaHRtbFxyXG4gICAqL1xyXG4gIHdhcm4obWVzc2FnZTogc3RyaW5nLCBwYXJhbXM/OiBOb3RpZmljYXRpb25QYXJhbXMpIHtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvblR5cGUuV2FybmluZywgbWVzc2FnZSwgcGFyYW1zKVxyXG4gIH1cclxuXHJcblxyXG4gIG5vdGlmaWNhdGlvbih0eXBlOiBOb3RpZmljYXRpb25UeXBlLCBtZXNzYWdlOiBzdHJpbmcsIHBhcmFtcz86IE5vdGlmaWNhdGlvblBhcmFtcykge1xyXG4gICAgY29uc3Qgbm90aWZpY2F0aW9uID0gbmV3IE5vdGlmaWNhdGlvbigpXHJcblxyXG4gICAgaWYgKHBhcmFtcykge1xyXG4gICAgICBpZiAocGFyYW1zLmtlZXBBZnRlclJvdXRlQ2hhbmdlKSB7XHJcbiAgICAgICAgdGhpcy5rZWVwQWZ0ZXJSb3V0ZUNoYW5nZSA9IHBhcmFtcy5rZWVwQWZ0ZXJSb3V0ZUNoYW5nZVxyXG4gICAgICB9XHJcbiAgICAgIGlmIChwYXJhbXMubXVzdERpc2FwcGVhckFmdGVyKSB7XHJcbiAgICAgICAgaWYgKHBhcmFtcy5tdXN0RGlzYXBwZWFyQWZ0ZXIgPCAwKSB7XHJcbiAgICAgICAgICBwYXJhbXMubXVzdERpc2FwcGVhckFmdGVyID0gdW5kZWZpbmVkXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIG5vdGlmaWNhdGlvbi5tdXN0RGlzYXBwZWFyQWZ0ZXIgPSBwYXJhbXMubXVzdERpc2FwcGVhckFmdGVyXHJcbiAgICAgICAgfSAgICAgICAgXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbm90aWZpY2F0aW9uLm11c3REaXNhcHBlYXJBZnRlciA9IDMwMDBcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgbm90aWZpY2F0aW9uLm11c3REaXNhcHBlYXJBZnRlciA9IDMwMDBcclxuICAgIH1cclxuXHJcbiAgICBub3RpZmljYXRpb24udHlwZSA9IHR5cGVcclxuICAgIG5vdGlmaWNhdGlvbi5tZXNzYWdlID0gbWVzc2FnZVxyXG5cclxuICAgIHRoaXMuc3ViamVjdC5uZXh0KG5vdGlmaWNhdGlvbilcclxuICB9XHJcblxyXG4gIHNhdmVJc0RvbmUoKSB7XHJcbiAgICB0aGlzLnNhdmVJc0RvbmVOb3RpZmljYXRpb24ubmV4dCh0cnVlKVxyXG4gIH1cclxuXHJcbiAgY2xlYXIoKSB7XHJcbiAgICAvLyBjbGVhciBub3RpZmljYXRpb25zXHJcbiAgICB0aGlzLnN1YmplY3QubmV4dCgpXHJcbiAgfVxyXG59XHJcbiJdfQ==
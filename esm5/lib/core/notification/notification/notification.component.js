// Angular
import { Component } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
// Models
import { NotificationType } from '../notification-type';
import * as i0 from "@angular/core";
import * as i1 from "../notification.service";
import * as i2 from "@angular/common";
function notificationComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    var _r304 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div");
    i0.ɵɵelementStart(1, "button", 2);
    i0.ɵɵlistener("click", function notificationComponent_div_1_Template_button_click_1_listener() { i0.ɵɵrestoreView(_r304); var notification_r302 = ctx.$implicit; var ctx_r303 = i0.ɵɵnextContext(); return ctx_r303.uiAction.removenotification(notification_r302); });
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "\u00D7");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelement(4, "div", 3);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var notification_r302 = ctx.$implicit;
    var ctx_r300 = i0.ɵɵnextContext();
    i0.ɵɵclassMapInterpolate1("", ctx_r300.uiAction.cssClass(notification_r302), " notification-dismissable");
    i0.ɵɵproperty("@inOut", undefined);
    i0.ɵɵadvance(4);
    i0.ɵɵproperty("innerHTML", notification_r302.message, i0.ɵɵsanitizeHtml);
} }
function notificationComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 4);
    i0.ɵɵelement(1, "span");
    i0.ɵɵelementEnd();
} }
// how to
// https://angular.io/guide/animations
// https://github.com/PointInside/ng2-toastr/blob/master/src/toast-container.component.ts
var notificationComponent = /** @class */ (function () {
    function notificationComponent(fofNotificationService, ngZone) {
        var _this = this;
        this.fofNotificationService = fofNotificationService;
        this.ngZone = ngZone;
        // All private variables
        this.priVar = {
            getnotificationSub: undefined,
            savedNotificationSub: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            notifications: [],
            savedActive: false
        };
        // All actions shared with UI 
        this.uiAction = {
            removenotification: function (notification) {
                _this.uiVar.notifications = _this.uiVar.notifications.filter(function (x) { return x !== notification; });
            },
            cssClass: function (notification) {
                if (!notification) {
                    return;
                }
                // return css class based on notification type
                switch (notification.type) {
                    case NotificationType.Success:
                        return 'notification notification-success';
                    case NotificationType.Error:
                        return 'notification notification-danger';
                    case NotificationType.Info:
                        return 'notification notification-info';
                    case NotificationType.Warning:
                        return 'notification notification-warning';
                }
            }
        };
    }
    // Angular events
    notificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        var template = this;
        this.priVar.getnotificationSub = this.fofNotificationService.getnotification()
            .subscribe(function (notification) {
            if (!notification) {
                // clear notifications when an empty notification is received
                _this.uiVar.notifications = [];
                return;
            }
            if (notification.mustDisappearAfter) {
                setTimeout(function () {
                    template.uiAction.removenotification(notification);
                }, notification.mustDisappearAfter);
            }
            // ensure the notif will be displayed even if received from a promise pipe
            _this.ngZone.run(function () {
                // push or unshift depend if there are on the top or bottom
                // this.notifications.push(notification)
                _this.uiVar.notifications.unshift(notification);
            });
        });
        this.fofNotificationService.savedNotification
            .subscribe(function (saved) {
            if (!saved) {
                return;
            }
            _this.uiVar.savedActive = true;
            setTimeout(function () {
                _this.uiVar.savedActive = false;
            }, 300);
        });
    };
    notificationComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.getnotificationSub) {
            this.priVar.getnotificationSub.unsubscribe();
        }
        if (this.priVar.savedNotificationSub) {
            this.priVar.savedNotificationSub.unsubscribe();
        }
    };
    notificationComponent.ɵfac = function notificationComponent_Factory(t) { return new (t || notificationComponent)(i0.ɵɵdirectiveInject(i1.FofNotificationService), i0.ɵɵdirectiveInject(i0.NgZone)); };
    notificationComponent.ɵcmp = i0.ɵɵdefineComponent({ type: notificationComponent, selectors: [["fof-notification"]], decls: 3, vars: 2, consts: [[3, "class", 4, "ngFor", "ngForOf"], ["class", "saved-notification", 4, "ngIf"], ["type", "button", "data-dismiss", "notification", 1, "close", 3, "click"], [3, "innerHTML"], [1, "saved-notification"]], template: function notificationComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div");
            i0.ɵɵtemplate(1, notificationComponent_div_1_Template, 5, 5, "div", 0);
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(2, notificationComponent_div_2_Template, 2, 0, "div", 1);
        } if (rf & 2) {
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx.uiVar.notifications);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.uiVar.savedActive);
        } }, directives: [i2.NgForOf, i2.NgIf], styles: ["[_nghost-%COMP%]{position:fixed;z-index:999999;bottom:12px;right:12px;width:300px}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]{padding:.5rem;margin-top:.5rem;position:relative}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]:hover   .close[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]{position:absolute;border:none;background-color:transparent;right:-5px;top:-13px;font-size:25px;cursor:pointer;opacity:.3}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]{position:fixed;z-index:9999999;bottom:15px;right:15px;width:25px;height:25px;border-radius:50px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{position:absolute;left:-3px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after, [_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{box-sizing:border-box;content:\"\";position:absolute}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{border-style:solid;border-width:7px 2px 1px;border-radius:1px;height:16px;left:8px;top:5px;width:16px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after{border-style:solid;border-width:1px 1px 1px 4px;height:5px;left:13px;top:5px;width:7px}"], data: { animation: [
                trigger('inOut', [
                    transition(':enter', [
                        style({
                            opacity: 0,
                            transform: 'translateX(100%)'
                        }),
                        animate('0.2s ease-in')
                    ]),
                    transition(':leave', [
                        animate('0.2s 10ms ease-out', style({
                            opacity: 0,
                            transform: 'translateX(100%)'
                        }))
                    ])
                ])
            ] } });
    return notificationComponent;
}());
export { notificationComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(notificationComponent, [{
        type: Component,
        args: [{
                selector: 'fof-notification',
                templateUrl: './notification.component.html',
                styleUrls: ['./notification.component.scss'],
                animations: [
                    trigger('inOut', [
                        transition(':enter', [
                            style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }),
                            animate('0.2s ease-in')
                        ]),
                        transition(':leave', [
                            animate('0.2s 10ms ease-out', style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }))
                        ])
                    ])
                ]
            }]
    }], function () { return [{ type: i1.FofNotificationService }, { type: i0.NgZone }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50LnRzIiwibGliL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi9ub3RpZmljYXRpb24uY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsVUFBVTtBQUNWLE9BQU8sRUFBQyxTQUFTLEVBQTRCLE1BQU0sZUFBZSxDQUFBO0FBQ2xFLE9BQU8sRUFBQyxPQUFPLEVBQVMsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUMsTUFBTSxxQkFBcUIsQ0FBQTtBQUc5RSxTQUFTO0FBQ1QsT0FBTyxFQUFnQixnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFBOzs7Ozs7SUNMbkUsMkJBR0U7SUFBQSxpQ0FDRTtJQURNLDJNQUFTLHVEQUF5QyxJQUFDO0lBQ3pELDRCQUFPO0lBQUEsc0JBQU87SUFBQSxpQkFBTztJQUN2QixpQkFBUztJQUNULHlCQUE4QztJQUNoRCxpQkFBTTs7OztJQUxBLHlHQUFzRTtJQUR4RSxrQ0FBUTtJQUtMLGVBQWtDO0lBQWxDLHdFQUFrQzs7O0lBSzNDLDhCQUNFO0lBQUEsdUJBQWE7SUFDZixpQkFBTTs7QURITixTQUFTO0FBQ1Qsc0NBQXNDO0FBQ3RDLHlGQUF5RjtBQUV6RjtJQXVCRSwrQkFDVSxzQkFBOEMsRUFDOUMsTUFBYztRQUZ4QixpQkFHSztRQUZLLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUd4Qix3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2Ysa0JBQWtCLEVBQWdCLFNBQVM7WUFDM0Msb0JBQW9CLEVBQWdCLFNBQVM7U0FDOUMsQ0FBQTtRQUNELHdCQUF3QjtRQUNoQixhQUFRLEdBQUcsRUFDbEIsQ0FBQTtRQUNELGdDQUFnQztRQUN6QixVQUFLLEdBQUc7WUFDYixhQUFhLEVBQXNCLEVBQUU7WUFDckMsV0FBVyxFQUFFLEtBQUs7U0FDbkIsQ0FBQTtRQUNELDhCQUE4QjtRQUN2QixhQUFRLEdBQUc7WUFDaEIsa0JBQWtCLEVBQUMsVUFBQyxZQUEwQjtnQkFDNUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFLLFlBQVksRUFBbEIsQ0FBa0IsQ0FBQyxDQUFBO1lBQ3JGLENBQUM7WUFDRCxRQUFRLEVBQUMsVUFBQyxZQUEwQjtnQkFDbEMsSUFBSSxDQUFDLFlBQVksRUFBRTtvQkFDZixPQUFNO2lCQUNUO2dCQUVELDhDQUE4QztnQkFDOUMsUUFBUSxZQUFZLENBQUMsSUFBSSxFQUFFO29CQUN6QixLQUFLLGdCQUFnQixDQUFDLE9BQU87d0JBQ3pCLE9BQU8sbUNBQW1DLENBQUE7b0JBQzlDLEtBQUssZ0JBQWdCLENBQUMsS0FBSzt3QkFDdkIsT0FBTyxrQ0FBa0MsQ0FBQTtvQkFDN0MsS0FBSyxnQkFBZ0IsQ0FBQyxJQUFJO3dCQUN0QixPQUFPLGdDQUFnQyxDQUFBO29CQUMzQyxLQUFLLGdCQUFnQixDQUFDLE9BQU87d0JBQ3pCLE9BQU8sbUNBQW1DLENBQUE7aUJBQy9DO1lBQ0gsQ0FBQztTQUNGLENBQUE7SUFyQ0csQ0FBQztJQXNDTCxpQkFBaUI7SUFDakIsd0NBQVEsR0FBUjtRQUFBLGlCQWlDQztRQWhDQyxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUE7UUFFckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxFQUFFO2FBQzdFLFNBQVMsQ0FBQyxVQUFDLFlBQTBCO1lBQ3BDLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ2YsNkRBQTZEO2dCQUM3RCxLQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUE7Z0JBQzdCLE9BQU07YUFDVDtZQUVELElBQUksWUFBWSxDQUFDLGtCQUFrQixFQUFFO2dCQUNuQyxVQUFVLENBQUM7b0JBQ1QsUUFBUSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQTtnQkFDcEQsQ0FBQyxFQUFFLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO2FBQ3BDO1lBRUQsMEVBQTBFO1lBQzFFLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUNkLDJEQUEyRDtnQkFDM0Qsd0NBQXdDO2dCQUN4QyxLQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDaEQsQ0FBQyxDQUFDLENBQUE7UUFDSixDQUFDLENBQUMsQ0FBQTtRQUVGLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxpQkFBaUI7YUFDNUMsU0FBUyxDQUFDLFVBQUMsS0FBSztZQUNmLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQUUsT0FBTTthQUFFO1lBQ3RCLEtBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQTtZQUM3QixVQUFVLENBQUM7Z0JBQ1QsS0FBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFBO1lBQ2hDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtRQUNULENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUNELDJDQUFXLEdBQVg7UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUU7WUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFBO1NBQUU7UUFDcEYsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFO1lBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUFFO0lBQzFGLENBQUM7OEZBaEZVLHFCQUFxQjs4REFBckIscUJBQXFCO1lDckNsQywyQkFDRTtZQUFBLHNFQUdFO1lBS0osaUJBQU07WUFHTixzRUFDRTs7WUFaSyxlQUFnRDtZQUFoRCxpREFBZ0Q7WUFXbEQsZUFBeUI7WUFBekIsNENBQXlCO3c1Q0RPaEI7Z0JBQ1YsT0FBTyxDQUFDLE9BQU8sRUFBRTtvQkFDZixVQUFVLENBQUMsUUFBUSxFQUFFO3dCQUNuQixLQUFLLENBQUM7NEJBQ0osT0FBTyxFQUFFLENBQUM7NEJBQ1YsU0FBUyxFQUFFLGtCQUFrQjt5QkFDOUIsQ0FBQzt3QkFDRixPQUFPLENBQUMsY0FBYyxDQUFDO3FCQUN4QixDQUFDO29CQUNGLFVBQVUsQ0FBQyxRQUFRLEVBQUU7d0JBQ25CLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUM7NEJBQ2xDLE9BQU8sRUFBRSxDQUFDOzRCQUNWLFNBQVMsRUFBRSxrQkFBa0I7eUJBQzlCLENBQUMsQ0FBQztxQkFDSixDQUFDO2lCQUNILENBQUM7YUFDSDtnQ0FuQ0g7Q0FzSEMsQUF2R0QsSUF1R0M7U0FqRlkscUJBQXFCO2tEQUFyQixxQkFBcUI7Y0F0QmpDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixXQUFXLEVBQUUsK0JBQStCO2dCQUM1QyxTQUFTLEVBQUUsQ0FBQywrQkFBK0IsQ0FBQztnQkFDNUMsVUFBVSxFQUFFO29CQUNWLE9BQU8sQ0FBQyxPQUFPLEVBQUU7d0JBQ2YsVUFBVSxDQUFDLFFBQVEsRUFBRTs0QkFDbkIsS0FBSyxDQUFDO2dDQUNKLE9BQU8sRUFBRSxDQUFDO2dDQUNWLFNBQVMsRUFBRSxrQkFBa0I7NkJBQzlCLENBQUM7NEJBQ0YsT0FBTyxDQUFDLGNBQWMsQ0FBQzt5QkFDeEIsQ0FBQzt3QkFDRixVQUFVLENBQUMsUUFBUSxFQUFFOzRCQUNuQixPQUFPLENBQUMsb0JBQW9CLEVBQUUsS0FBSyxDQUFDO2dDQUNsQyxPQUFPLEVBQUUsQ0FBQztnQ0FDVixTQUFTLEVBQUUsa0JBQWtCOzZCQUM5QixDQUFDLENBQUM7eUJBQ0osQ0FBQztxQkFDSCxDQUFDO2lCQUNIO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBBbmd1bGFyXHJcbmltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveSwgTmdab25lfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQge2FuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlcn0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucydcclxuLy8gU2VydmljZXNcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG4vLyBNb2RlbHNcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uLCBOb3RpZmljYXRpb25UeXBlIH0gZnJvbSAnLi4vbm90aWZpY2F0aW9uLXR5cGUnXHJcbi8vIENvbXBvbmVudHNcclxuLy8gVmVuZG9ycyAmIHV0aWxzXHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnXHJcblxyXG4vLyBob3cgdG9cclxuLy8gaHR0cHM6Ly9hbmd1bGFyLmlvL2d1aWRlL2FuaW1hdGlvbnNcclxuLy8gaHR0cHM6Ly9naXRodWIuY29tL1BvaW50SW5zaWRlL25nMi10b2FzdHIvYmxvYi9tYXN0ZXIvc3JjL3RvYXN0LWNvbnRhaW5lci5jb21wb25lbnQudHNcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZm9mLW5vdGlmaWNhdGlvbicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbm90aWZpY2F0aW9uLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgYW5pbWF0aW9uczogW1xyXG4gICAgdHJpZ2dlcignaW5PdXQnLCBbICAgICBcclxuICAgICAgdHJhbnNpdGlvbignOmVudGVyJywgW1xyXG4gICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgIG9wYWNpdHk6IDAsXHJcbiAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDEwMCUpJ1xyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIGFuaW1hdGUoJzAuMnMgZWFzZS1pbicpXHJcbiAgICAgIF0pLFxyXG4gICAgICB0cmFuc2l0aW9uKCc6bGVhdmUnLCBbXHJcbiAgICAgICAgYW5pbWF0ZSgnMC4ycyAxMG1zIGVhc2Utb3V0Jywgc3R5bGUoe1xyXG4gICAgICAgICAgb3BhY2l0eTogMCxcclxuICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMTAwJSknXHJcbiAgICAgICAgfSkpXHJcbiAgICAgIF0pXHJcbiAgICBdKSAgICBcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBub3RpZmljYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG5nWm9uZTogTmdab25lXHJcbiAgKSB7IH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICBnZXRub3RpZmljYXRpb25TdWI6IDxTdWJzY3JpcHRpb24+dW5kZWZpbmVkLFxyXG4gICAgc2F2ZWROb3RpZmljYXRpb25TdWI6IDxTdWJzY3JpcHRpb24+dW5kZWZpbmVkXHJcbiAgfVxyXG4gIC8vIEFsbCBwcml2YXRlIGZ1bmN0aW9uc1xyXG4gIHByaXZhdGUgcHJpdkZ1bmMgPSB7XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgbm90aWZpY2F0aW9uczo8QXJyYXk8Tm90aWZpY2F0aW9uPj5bXSxcclxuICAgIHNhdmVkQWN0aXZlOiBmYWxzZVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgICByZW1vdmVub3RpZmljYXRpb246KG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uKSA9PiB7XHJcbiAgICAgIHRoaXMudWlWYXIubm90aWZpY2F0aW9ucyA9IHRoaXMudWlWYXIubm90aWZpY2F0aW9ucy5maWx0ZXIoeCA9PiB4ICE9PSBub3RpZmljYXRpb24pXHJcbiAgICB9LFxyXG4gICAgY3NzQ2xhc3M6KG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uKSA9PiB7XHJcbiAgICAgIGlmICghbm90aWZpY2F0aW9uKSB7XHJcbiAgICAgICAgICByZXR1cm5cclxuICAgICAgfVxyXG5cclxuICAgICAgLy8gcmV0dXJuIGNzcyBjbGFzcyBiYXNlZCBvbiBub3RpZmljYXRpb24gdHlwZVxyXG4gICAgICBzd2l0Y2ggKG5vdGlmaWNhdGlvbi50eXBlKSB7XHJcbiAgICAgICAgY2FzZSBOb3RpZmljYXRpb25UeXBlLlN1Y2Nlc3M6XHJcbiAgICAgICAgICAgIHJldHVybiAnbm90aWZpY2F0aW9uIG5vdGlmaWNhdGlvbi1zdWNjZXNzJ1xyXG4gICAgICAgIGNhc2UgTm90aWZpY2F0aW9uVHlwZS5FcnJvcjpcclxuICAgICAgICAgICAgcmV0dXJuICdub3RpZmljYXRpb24gbm90aWZpY2F0aW9uLWRhbmdlcidcclxuICAgICAgICBjYXNlIE5vdGlmaWNhdGlvblR5cGUuSW5mbzpcclxuICAgICAgICAgICAgcmV0dXJuICdub3RpZmljYXRpb24gbm90aWZpY2F0aW9uLWluZm8nXHJcbiAgICAgICAgY2FzZSBOb3RpZmljYXRpb25UeXBlLldhcm5pbmc6XHJcbiAgICAgICAgICAgIHJldHVybiAnbm90aWZpY2F0aW9uIG5vdGlmaWNhdGlvbi13YXJuaW5nJ1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICBjb25zdCB0ZW1wbGF0ZSA9IHRoaXNcclxuXHJcbiAgICB0aGlzLnByaVZhci5nZXRub3RpZmljYXRpb25TdWIgPSB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2UuZ2V0bm90aWZpY2F0aW9uKClcclxuICAgIC5zdWJzY3JpYmUoKG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmICghbm90aWZpY2F0aW9uKSB7XHJcbiAgICAgICAgICAvLyBjbGVhciBub3RpZmljYXRpb25zIHdoZW4gYW4gZW1wdHkgbm90aWZpY2F0aW9uIGlzIHJlY2VpdmVkXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLm5vdGlmaWNhdGlvbnMgPSBbXVxyXG4gICAgICAgICAgcmV0dXJuXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChub3RpZmljYXRpb24ubXVzdERpc2FwcGVhckFmdGVyKSB7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICB0ZW1wbGF0ZS51aUFjdGlvbi5yZW1vdmVub3RpZmljYXRpb24obm90aWZpY2F0aW9uKVxyXG4gICAgICAgIH0sIG5vdGlmaWNhdGlvbi5tdXN0RGlzYXBwZWFyQWZ0ZXIpXHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIC8vIGVuc3VyZSB0aGUgbm90aWYgd2lsbCBiZSBkaXNwbGF5ZWQgZXZlbiBpZiByZWNlaXZlZCBmcm9tIGEgcHJvbWlzZSBwaXBlXHJcbiAgICAgIHRoaXMubmdab25lLnJ1bigoKSA9PiB7XHJcbiAgICAgICAgLy8gcHVzaCBvciB1bnNoaWZ0IGRlcGVuZCBpZiB0aGVyZSBhcmUgb24gdGhlIHRvcCBvciBib3R0b21cclxuICAgICAgICAvLyB0aGlzLm5vdGlmaWNhdGlvbnMucHVzaChub3RpZmljYXRpb24pXHJcbiAgICAgICAgdGhpcy51aVZhci5ub3RpZmljYXRpb25zLnVuc2hpZnQobm90aWZpY2F0aW9uKVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuXHJcbiAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc2F2ZWROb3RpZmljYXRpb25cclxuICAgIC5zdWJzY3JpYmUoKHNhdmVkKSA9PiB7ICAgICAgICAgIFxyXG4gICAgICBpZiAoIXNhdmVkKSB7IHJldHVybiB9XHJcbiAgICAgIHRoaXMudWlWYXIuc2F2ZWRBY3RpdmUgPSB0cnVlXHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMudWlWYXIuc2F2ZWRBY3RpdmUgPSBmYWxzZVxyXG4gICAgICB9LCAzMDApXHJcbiAgICB9KVxyXG4gIH1cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnByaVZhci5nZXRub3RpZmljYXRpb25TdWIpIHsgdGhpcy5wcmlWYXIuZ2V0bm90aWZpY2F0aW9uU3ViLnVuc3Vic2NyaWJlKCkgfVxyXG4gICAgaWYgKHRoaXMucHJpVmFyLnNhdmVkTm90aWZpY2F0aW9uU3ViKSB7IHRoaXMucHJpVmFyLnNhdmVkTm90aWZpY2F0aW9uU3ViLnVuc3Vic2NyaWJlKCkgfVxyXG4gIH1cclxufVxyXG4iLCI8ZGl2ID4gICAgXHJcbiAgPGRpdiAqbmdGb3I9XCJsZXQgbm90aWZpY2F0aW9uIG9mIHVpVmFyLm5vdGlmaWNhdGlvbnNcIlxyXG4gICAgICBbQGluT3V0XVxyXG4gICAgICAgIGNsYXNzPVwie3sgdWlBY3Rpb24uY3NzQ2xhc3Mobm90aWZpY2F0aW9uKSB9fSBub3RpZmljYXRpb24tZGlzbWlzc2FibGVcIj5cclxuICAgIDxidXR0b24gKGNsaWNrKT1cInVpQWN0aW9uLnJlbW92ZW5vdGlmaWNhdGlvbihub3RpZmljYXRpb24pXCIgdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiBkYXRhLWRpc21pc3M9XCJub3RpZmljYXRpb25cIj5cclxuICAgICAgPHNwYW4gPiZ0aW1lczs8L3NwYW4+XHJcbiAgICA8L2J1dHRvbj5cclxuICAgIDxkaXYgW2lubmVySFRNTF09XCJub3RpZmljYXRpb24ubWVzc2FnZVwiPjwvZGl2PlxyXG4gIDwvZGl2PlxyXG48L2Rpdj5cclxuXHJcblxyXG48ZGl2ICpuZ0lmPVwidWlWYXIuc2F2ZWRBY3RpdmVcIiAgY2xhc3M9XCJzYXZlZC1ub3RpZmljYXRpb25cIiA+XHJcbiAgPHNwYW4+PC9zcGFuPlxyXG48L2Rpdj4iXX0=
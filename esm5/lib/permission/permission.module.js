import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FofPermissionService } from './fof-permission.service';
import * as i0 from "@angular/core";
var FofPermissionModule = /** @class */ (function () {
    function FofPermissionModule() {
    }
    FofPermissionModule.ɵmod = i0.ɵɵdefineNgModule({ type: FofPermissionModule });
    FofPermissionModule.ɵinj = i0.ɵɵdefineInjector({ factory: function FofPermissionModule_Factory(t) { return new (t || FofPermissionModule)(); }, providers: [
            FofPermissionService
        ], imports: [[
                CommonModule
            ]] });
    return FofPermissionModule;
}());
export { FofPermissionModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(FofPermissionModule, { imports: [CommonModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(FofPermissionModule, [{
        type: NgModule,
        args: [{
                declarations: [],
                imports: [
                    CommonModule
                ],
                providers: [
                    FofPermissionService
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVybWlzc2lvbi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9wZXJtaXNzaW9uL3Blcm1pc3Npb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFBOztBQUUvRDtJQUFBO0tBV29DOzJEQUF2QixtQkFBbUI7eUhBQW5CLG1CQUFtQixtQkFKbkI7WUFDVCxvQkFBb0I7U0FDckIsWUFMUTtnQkFDUCxZQUFZO2FBQ2I7OEJBVkg7Q0Flb0MsQUFYcEMsSUFXb0M7U0FBdkIsbUJBQW1CO3dGQUFuQixtQkFBbUIsY0FONUIsWUFBWTtrREFNSCxtQkFBbUI7Y0FYL0IsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxFQUViO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO2lCQUNiO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxvQkFBb0I7aUJBQ3JCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbidcclxuaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1xyXG4gIFxyXG4gIF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlXHJcbiAgXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIEZvZlBlcm1pc3Npb25TZXJ2aWNlXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9mUGVybWlzc2lvbk1vZHVsZSB7IH1cclxuIl19
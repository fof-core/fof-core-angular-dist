import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
var fofTranslate = /** @class */ (function () {
    function fofTranslate() {
    }
    fofTranslate.prototype.transform = function (content) {
        return "<b>" + content + "</b>";
    };
    fofTranslate.ɵfac = function fofTranslate_Factory(t) { return new (t || fofTranslate)(); };
    fofTranslate.ɵpipe = i0.ɵɵdefinePipe({ name: "fofTranslate", type: fofTranslate, pure: true });
    return fofTranslate;
}());
export { fofTranslate };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(fofTranslate, [{
        type: Pipe,
        args: [{ name: 'fofTranslate' }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm8tdHJhbnNsYXRlLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi9zaGFyZWQvcGlwZXMvZm8tdHJhbnNsYXRlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7O0FBRXBEO0lBQUE7S0FLQztJQUhDLGdDQUFTLEdBQVQsVUFBVSxPQUFPO1FBQ2YsT0FBTyxRQUFNLE9BQU8sU0FBTSxDQUFDO0lBQzdCLENBQUM7NEVBSFUsWUFBWTt1RUFBWixZQUFZO3VCQUh6QjtDQU9DLEFBTEQsSUFLQztTQUpZLFlBQVk7a0RBQVosWUFBWTtjQUR4QixJQUFJO2VBQUMsRUFBQyxJQUFJLEVBQUUsY0FBYyxFQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQFBpcGUoe25hbWU6ICdmb2ZUcmFuc2xhdGUnfSlcclxuZXhwb3J0IGNsYXNzIGZvZlRyYW5zbGF0ZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG4gIHRyYW5zZm9ybShjb250ZW50KSB7XHJcbiAgICByZXR1cm4gYDxiPiR7Y29udGVudH08L2I+YDtcclxuICB9XHJcbn0iXX0=
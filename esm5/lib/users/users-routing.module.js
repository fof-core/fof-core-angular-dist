import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FofAuthGuard } from '../core/auth.guard';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
var routes = [
    { path: '', canActivate: [FofAuthGuard],
        children: [
            { path: '',
                children: [
                    { path: '', component: UsersComponent },
                    { path: ':code', component: UserComponent }
                ]
            },
        ]
    }
];
var UsersRoutingModule = /** @class */ (function () {
    function UsersRoutingModule() {
    }
    UsersRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: UsersRoutingModule });
    UsersRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UsersRoutingModule_Factory(t) { return new (t || UsersRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
            RouterModule] });
    return UsersRoutingModule;
}());
export { UsersRoutingModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UsersRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UsersRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnMtcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi91c2Vycy91c2Vycy1yb3V0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsWUFBWSxFQUFDLE1BQU0sb0JBQW9CLENBQUE7QUFDaEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFBO0FBQ3hELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQTs7O0FBRXJELElBQU0sTUFBTSxHQUFXO0lBQ3JCLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsQ0FBQyxZQUFZLENBQUM7UUFDckMsUUFBUSxFQUFFO1lBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRTtnQkFDUixRQUFRLEVBQUU7b0JBQ1IsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUU7b0JBQ3ZDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFO2lCQUM1QzthQUNGO1NBQ0Y7S0FDRjtDQUNGLENBQUE7QUFFRDtJQUFBO0tBSW1DOzBEQUF0QixrQkFBa0I7dUhBQWxCLGtCQUFrQixrQkFIcEIsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlCLFlBQVk7NkJBckJ4QjtDQXVCbUMsQUFKbkMsSUFJbUM7U0FBdEIsa0JBQWtCO3dGQUFsQixrQkFBa0IsMENBRm5CLFlBQVk7a0RBRVgsa0JBQWtCO2NBSjlCLFFBQVE7ZUFBQztnQkFDUixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4QyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDeEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IFJvdXRlcywgUm91dGVyTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJ1xyXG5pbXBvcnQgeyBGb2ZBdXRoR3VhcmR9IGZyb20gJy4uL2NvcmUvYXV0aC5ndWFyZCdcclxuaW1wb3J0IHsgVXNlcnNDb21wb25lbnQgfSBmcm9tICcuL3VzZXJzL3VzZXJzLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgVXNlckNvbXBvbmVudCB9IGZyb20gJy4vdXNlci91c2VyLmNvbXBvbmVudCdcclxuXHJcbmNvbnN0IHJvdXRlczogUm91dGVzID0gW1xyXG4gIHsgcGF0aDogJycsIGNhbkFjdGl2YXRlOiBbRm9mQXV0aEd1YXJkXSxcclxuICAgIGNoaWxkcmVuOiBbXHJcbiAgICAgIHsgcGF0aDogJycsXHJcbiAgICAgICAgY2hpbGRyZW46IFtcclxuICAgICAgICAgIHsgcGF0aDogJycsIGNvbXBvbmVudDogVXNlcnNDb21wb25lbnQgfSwgICAgICAgICAgICBcclxuICAgICAgICAgIHsgcGF0aDogJzpjb2RlJywgY29tcG9uZW50OiBVc2VyQ29tcG9uZW50IH1cclxuICAgICAgICBdXHJcbiAgICAgIH0sXHJcbiAgICBdXHJcbiAgfSAgIFxyXG5dXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGltcG9ydHM6IFtSb3V0ZXJNb2R1bGUuZm9yQ2hpbGQocm91dGVzKV0sXHJcbiAgZXhwb3J0czogW1JvdXRlck1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFVzZXJzUm91dGluZ01vZHVsZSB7IH1cclxuIl19
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../core/material.module';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '../components/components.module';
import { UservicesModule } from '../uservices/uservices.module';
import * as i0 from "@angular/core";
var UsersModule = /** @class */ (function () {
    function UsersModule() {
    }
    UsersModule.ɵmod = i0.ɵɵdefineNgModule({ type: UsersModule });
    UsersModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UsersModule_Factory(t) { return new (t || UsersModule)(); }, imports: [[
                CommonModule,
                UsersRoutingModule,
                MaterialModule,
                FormsModule,
                ReactiveFormsModule,
                ComponentsModule,
                UservicesModule
            ]] });
    return UsersModule;
}());
export { UsersModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UsersModule, { declarations: [UsersComponent,
        UserComponent], imports: [CommonModule,
        UsersRoutingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        ComponentsModule,
        UservicesModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UsersModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    UsersComponent,
                    UserComponent
                ],
                imports: [
                    CommonModule,
                    UsersRoutingModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    ComponentsModule,
                    UservicesModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvdXNlcnMvdXNlcnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQTtBQUMzRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUE7QUFDeEQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVCQUF1QixDQUFBO0FBQ3JELE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTtBQUNqRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sK0JBQStCLENBQUE7O0FBRS9EO0lBQUE7S0FlNEI7bURBQWYsV0FBVzt5R0FBWCxXQUFXLGtCQVZiO2dCQUNQLFlBQVk7Z0JBQ1osa0JBQWtCO2dCQUNsQixjQUFjO2dCQUNkLFdBQVc7Z0JBQ1gsbUJBQW1CO2dCQUNuQixnQkFBZ0I7Z0JBQ2hCLGVBQWU7YUFDaEI7c0JBdkJIO0NBeUI0QixBQWY1QixJQWU0QjtTQUFmLFdBQVc7d0ZBQVgsV0FBVyxtQkFicEIsY0FBYztRQUNkLGFBQWEsYUFHYixZQUFZO1FBQ1osa0JBQWtCO1FBQ2xCLGNBQWM7UUFDZCxXQUFXO1FBQ1gsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixlQUFlO2tEQUdOLFdBQVc7Y0FmdkIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixjQUFjO29CQUNkLGFBQWE7aUJBQ2Q7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osa0JBQWtCO29CQUNsQixjQUFjO29CQUNkLFdBQVc7b0JBQ1gsbUJBQW1CO29CQUNuQixnQkFBZ0I7b0JBQ2hCLGVBQWU7aUJBQ2hCO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbidcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9jb3JlL21hdGVyaWFsLm1vZHVsZSdcclxuaW1wb3J0IHsgVXNlcnNSb3V0aW5nTW9kdWxlIH0gZnJvbSAnLi91c2Vycy1yb3V0aW5nLm1vZHVsZSdcclxuaW1wb3J0IHsgVXNlcnNDb21wb25lbnQgfSBmcm9tICcuL3VzZXJzL3VzZXJzLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgVXNlckNvbXBvbmVudCB9IGZyb20gJy4vdXNlci91c2VyLmNvbXBvbmVudCdcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3JtcydcclxuaW1wb3J0IHsgQ29tcG9uZW50c01vZHVsZSB9IGZyb20gJy4uL2NvbXBvbmVudHMvY29tcG9uZW50cy5tb2R1bGUnXHJcbmltcG9ydCB7IFVzZXJ2aWNlc01vZHVsZSB9IGZyb20gJy4uL3VzZXJ2aWNlcy91c2VydmljZXMubW9kdWxlJ1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIFVzZXJzQ29tcG9uZW50LCBcclxuICAgIFVzZXJDb21wb25lbnRcclxuICBdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIFVzZXJzUm91dGluZ01vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgRm9ybXNNb2R1bGUsXHJcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgQ29tcG9uZW50c01vZHVsZSxcclxuICAgIFVzZXJ2aWNlc01vZHVsZVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFVzZXJzTW9kdWxlIHsgfVxyXG4iXX0=
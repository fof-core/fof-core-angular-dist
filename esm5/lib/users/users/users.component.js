import { Component, ViewChild, EventEmitter } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap, debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import { FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "../../permission/fof-permission.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
import * as i4 from "../../core/fof-error.service";
import * as i5 from "@angular/material/card";
import * as i6 from "../../components/fof-organizations-tree/fof-organizations-tree.component";
import * as i7 from "@angular/material/button";
import * as i8 from "@angular/router";
import * as i9 from "@angular/material/form-field";
import * as i10 from "@angular/material/input";
import * as i11 from "@angular/forms";
import * as i12 from "@angular/common";
import * as i13 from "@angular/material/table";
import * as i14 from "@angular/material/sort";
import * as i15 from "@angular/material/paginator";
import * as i16 from "@angular/material/progress-spinner";
function UsersComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 21);
    i0.ɵɵelement(1, "mat-spinner", 22);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des collaborateurs...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function UsersComponent_th_22_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Email");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_23_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r316 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r316.email);
} }
function UsersComponent_th_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Login");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_26_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r317 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r317.login);
} }
function UsersComponent_th_28_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Pr\u00E9nom");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_29_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r318 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r318.firstName);
} }
function UsersComponent_th_31_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 23);
    i0.ɵɵtext(1, "Nom");
    i0.ɵɵelementEnd();
} }
function UsersComponent_td_32_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 24);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r319 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r319.lastName);
} }
function UsersComponent_tr_33_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 25);
} }
function UsersComponent_tr_34_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 26);
} if (rf & 2) {
    var row_r320 = ctx.$implicit;
    i0.ɵɵproperty("routerLink", row_r320.id);
} }
var _c0 = function () { return [5, 10, 25, 100]; };
var UsersComponent = /** @class */ (function () {
    function UsersComponent(fofPermissionService, fofNotificationService, breakpointObserver, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
            filter: undefined,
            filterOrganizationsChange: new EventEmitter(),
            filterOrganizations: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: [],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true,
            searchUsersCtrl: new FormControl()
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: function (organization) {
                if (organization) {
                    _this.priVar.filterOrganizations = [organization.id];
                }
                else {
                    _this.priVar.filterOrganizations = null;
                }
                _this.priVar.filterOrganizationsChange.emit(organization);
            }
        };
    }
    // Angular events
    UsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.filter = this.uiVar.searchUsersCtrl.valueChanges
            .pipe(debounceTime(500), distinctUntilChanged(), filter(function (query) { return query.length >= 3 || query.length === 0; }));
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['email', 'login'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['email', 'login', 'firstName', 'lastName'];
            }
        });
    };
    UsersComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page, this.priVar.filter, this.priVar.filterOrganizationsChange)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.user.search(_this.uiVar.searchUsersCtrl.value, _this.priVar.filterOrganizations, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function (error) {
            _this.fofErrorService.errorManage(error);
            _this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(function (data) {
            _this.uiVar.data = data;
        });
    };
    UsersComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    UsersComponent.ɵfac = function UsersComponent_Factory(t) { return new (t || UsersComponent)(i0.ɵɵdirectiveInject(i1.FofPermissionService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver), i0.ɵɵdirectiveInject(i4.FofErrorService)); };
    UsersComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UsersComponent, selectors: [["fof-core-users"]], viewQuery: function UsersComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(MatPaginator, true);
            i0.ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 36, vars: 11, consts: [[1, "row"], [1, "col-md-3"], [1, "card-tree"], [3, "multiSelect", "selectedOrganizationsChange"], [1, "col-md-9"], [1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "filtres"], ["autofocus", "", "matInput", "", "placeholder", "email ou nom ou pr\u00E9nom ou login", 3, "formControl"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "email", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "email"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "login"], ["matColumnDef", "firstName"], ["matColumnDef", "lastName"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UsersComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "div", 1);
            i0.ɵɵelementStart(2, "mat-card", 2);
            i0.ɵɵelementStart(3, "fof-organizations-tree", 3);
            i0.ɵɵlistener("selectedOrganizationsChange", function UsersComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(4, "div", 4);
            i0.ɵɵelementStart(5, "mat-card", 5);
            i0.ɵɵelementStart(6, "h3");
            i0.ɵɵtext(7, "Gestion des collaborateurs");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "a", 6);
            i0.ɵɵelementStart(9, "span");
            i0.ɵɵtext(10, "Ajouter un collaborateur");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(11, "mat-card", 7);
            i0.ɵɵelementStart(12, "mat-form-field");
            i0.ɵɵelementStart(13, "mat-label");
            i0.ɵɵtext(14, "Filtre");
            i0.ɵɵelementEnd();
            i0.ɵɵelement(15, "input", 8);
            i0.ɵɵelementStart(16, "mat-hint");
            i0.ɵɵtext(17, "Recherche \u00E0 partir de 3 caract\u00E8res saisies");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(18, "div", 9);
            i0.ɵɵtemplate(19, UsersComponent_div_19_Template, 4, 0, "div", 10);
            i0.ɵɵelementStart(20, "table", 11);
            i0.ɵɵelementContainerStart(21, 12);
            i0.ɵɵtemplate(22, UsersComponent_th_22_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(23, UsersComponent_td_23_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(24, 15);
            i0.ɵɵtemplate(25, UsersComponent_th_25_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(26, UsersComponent_td_26_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(27, 16);
            i0.ɵɵtemplate(28, UsersComponent_th_28_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(29, UsersComponent_td_29_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(30, 17);
            i0.ɵɵtemplate(31, UsersComponent_th_31_Template, 2, 0, "th", 13);
            i0.ɵɵtemplate(32, UsersComponent_td_32_Template, 2, 1, "td", 14);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵtemplate(33, UsersComponent_tr_33_Template, 1, 0, "tr", 18);
            i0.ɵɵtemplate(34, UsersComponent_tr_34_Template, 1, 1, "tr", 19);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(35, "mat-paginator", 20);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("multiSelect", false);
            i0.ɵɵadvance(5);
            i0.ɵɵproperty("routerLink", "/admin/users/new");
            i0.ɵɵadvance(7);
            i0.ɵɵproperty("formControl", ctx.uiVar.searchUsersCtrl);
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("dataSource", ctx.uiVar.data);
            i0.ɵɵadvance(13);
            i0.ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", i0.ɵɵpureFunction0(10, _c0))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [i5.MatCard, i6.FofOrganizationsTreeComponent, i7.MatAnchor, i8.RouterLinkWithHref, i9.MatFormField, i9.MatLabel, i10.MatInput, i11.DefaultValueAccessor, i11.NgControlStatus, i11.FormControlDirective, i9.MatHint, i12.NgIf, i13.MatTable, i14.MatSort, i13.MatColumnDef, i13.MatHeaderCellDef, i13.MatCellDef, i13.MatHeaderRowDef, i13.MatRowDef, i15.MatPaginator, i16.MatSpinner, i13.MatHeaderCell, i14.MatSortHeader, i13.MatCell, i13.MatHeaderRow, i13.MatRow, i8.RouterLink], styles: [".filtres[_ngcontent-%COMP%]{margin-bottom:15px}.filtres[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.card-tree[_ngcontent-%COMP%]{height:100%}"] });
    return UsersComponent;
}());
export { UsersComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UsersComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-users',
                templateUrl: './users.component.html',
                styleUrls: ['./users.component.scss']
            }]
    }], function () { return [{ type: i1.FofPermissionService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }, { type: i4.FofErrorService }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvdXNlcnMvdXNlcnMvdXNlcnMuY29tcG9uZW50LnRzIiwibGliL3VzZXJzL3VzZXJzL3VzZXJzLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQW1DLFNBQVMsRUFDakQsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBSWhELE9BQU8sRUFBRSxZQUFZLEVBQW9CLE1BQU0sNkJBQTZCLENBQUE7QUFDNUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBQ2hELE9BQU8sRUFBRSxLQUFLLEVBQWMsRUFBRSxJQUFJLFlBQVksRUFBaUMsTUFBTSxNQUFNLENBQUE7QUFDM0YsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQzFELG9CQUFvQixFQUFZLE1BQU0sRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBRWhFLE9BQU8sRUFBc0IsV0FBVyxFQUFtQixNQUFNLHFCQUFxQixDQUFBO0FBQ3RGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ2tCdEMsK0JBQ0U7SUFBQSxrQ0FBdUM7SUFBQyw0QkFBTTtJQUFBLGlEQUFpQztJQUFBLGlCQUFPO0lBQ3hGLGlCQUFNOzs7SUFNRiw4QkFBc0Q7SUFBQSxxQkFBSztJQUFBLGlCQUFLOzs7SUFDaEUsOEJBQW1DO0lBQUEsWUFBYTtJQUFBLGlCQUFLOzs7SUFBbEIsZUFBYTtJQUFiLG9DQUFhOzs7SUFJaEQsOEJBQXNEO0lBQUEscUJBQUs7SUFBQSxpQkFBSzs7O0lBQ2hFLDhCQUFtQztJQUFBLFlBQWE7SUFBQSxpQkFBSzs7O0lBQWxCLGVBQWE7SUFBYixvQ0FBYTs7O0lBSWhELDhCQUFzRDtJQUFBLDJCQUFNO0lBQUEsaUJBQUs7OztJQUNqRSw4QkFBbUM7SUFBQSxZQUFpQjtJQUFBLGlCQUFLOzs7SUFBdEIsZUFBaUI7SUFBakIsd0NBQWlCOzs7SUFJcEQsOEJBQXNEO0lBQUEsbUJBQUc7SUFBQSxpQkFBSzs7O0lBQzlELDhCQUFtQztJQUFBLFlBQWdCO0lBQUEsaUJBQUs7OztJQUFyQixlQUFnQjtJQUFoQix1Q0FBZ0I7OztJQUdyRCx5QkFBa0U7OztJQUNsRSx5QkFDOEQ7OztJQUR6Qix3Q0FBcUI7OztBRHpDbEU7SUFTRSx3QkFDVSxvQkFBMEMsRUFDMUMsc0JBQThDLEVBQzlDLGtCQUFzQyxFQUN0QyxlQUFnQztRQUoxQyxpQkFPQztRQU5TLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUsxQyx3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2YscUJBQXFCLEVBQWdCLFNBQVM7WUFDOUMsTUFBTSxFQUFzQixTQUFTO1lBQ3JDLHlCQUF5QixFQUErQixJQUFJLFlBQVksRUFBRTtZQUMxRSxtQkFBbUIsRUFBWSxTQUFTO1NBQ3pDLENBQUE7UUFDRCx3QkFBd0I7UUFDaEIsYUFBUSxHQUFHLEVBQ2xCLENBQUE7UUFDRCxnQ0FBZ0M7UUFDekIsVUFBSyxHQUFHO1lBQ2IsZ0JBQWdCLEVBQVksRUFBRTtZQUM5QixJQUFJLEVBQVcsRUFBRTtZQUNqQixhQUFhLEVBQUUsQ0FBQztZQUNoQixRQUFRLEVBQUUsQ0FBQztZQUNYLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsZUFBZSxFQUFFLElBQUksV0FBVyxFQUFFO1NBQ25DLENBQUE7UUFDRCw4QkFBOEI7UUFDdkIsYUFBUSxHQUFHO1lBQ2hCLCtCQUErQixFQUFDLFVBQUMsWUFBMkI7Z0JBQzFELElBQUksWUFBWSxFQUFFO29CQUNoQixLQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixHQUFHLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFBO2lCQUNwRDtxQkFBTTtvQkFDTCxLQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQTtpQkFDdkM7Z0JBQ0QsS0FBSSxDQUFDLE1BQU0sQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDMUQsQ0FBQztTQUNGLENBQUE7SUEvQkQsQ0FBQztJQWdDRCxpQkFBaUI7SUFDakIsaUNBQVEsR0FBUjtRQUFBLGlCQWtCQztRQWpCQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxZQUFZO2FBQ3pELElBQUksQ0FDSCxZQUFZLENBQUMsR0FBRyxDQUFDLEVBQ2pCLG9CQUFvQixFQUFFLEVBQ3RCLE1BQU0sQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUF2QyxDQUF1QyxDQUFDLENBQ3pELENBQUE7UUFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzthQUN0RixTQUFTLENBQUMsVUFBQyxLQUFzQjtZQUNoQyxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7Z0JBQ2pCLFNBQVM7Z0JBQ1QsS0FBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQTthQUNqRDtpQkFBTTtnQkFDTCxXQUFXO2dCQUNYLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUMsQ0FBQTthQUMxRTtRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELHdDQUFlLEdBQWY7UUFBQSxpQkE4QkM7UUE3QkMsb0VBQW9FO1FBQ3BFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxFQUE1QixDQUE0QixDQUFDLENBQUE7UUFFbEUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMseUJBQXlCLENBQUM7YUFDeEcsSUFBSSxDQUNILFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixTQUFTLENBQUM7WUFDUixLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQTtZQUNsQyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQTtZQUU3QyxPQUFPLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUMxQyxLQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQ2hDLEtBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQy9CLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUNuQixLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQ3BFLENBQUMsQ0FBQyxFQUNGLEdBQUcsQ0FBQyxVQUFDLE1BQXlCO1lBQzVCLEtBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFBO1lBQ25DLEtBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUE7WUFDdkMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ3BCLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxVQUFBLEtBQUs7WUFDZCxLQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUN2QyxLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQTtZQUNuQyxPQUFPLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUN6QixDQUFDLENBQUMsQ0FDSCxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDZCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUE7UUFDeEIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsb0NBQVcsR0FBWDtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsRUFBRTtZQUFFLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUE7U0FBRTtJQUM1RixDQUFDO2dGQWxHVSxjQUFjO3VEQUFkLGNBQWM7MkJBQ2QsWUFBWTsyQkFDWixPQUFPOzs7Ozs7WUN4QnBCLDhCQUNFO1lBQUEsOEJBQ0U7WUFBQSxtQ0FDRTtZQUFBLGlEQUd5QjtZQUZ2Qiw2SkFBK0Isb0RBQWdELElBQUM7WUFFbEYsaUJBQXlCO1lBQzNCLGlCQUFXO1lBQ2IsaUJBQU07WUFDTiw4QkFFRTtZQUFBLG1DQUNFO1lBQUEsMEJBQUk7WUFBQSwwQ0FBMEI7WUFBQSxpQkFBSztZQUNuQyw0QkFFRTtZQUFBLDRCQUFNO1lBQUEseUNBQXdCO1lBQUEsaUJBQU87WUFDdkMsaUJBQUk7WUFDTixpQkFBVztZQUVYLG9DQUNFO1lBQUEsdUNBQ0U7WUFBQSxrQ0FBVztZQUFBLHVCQUFNO1lBQUEsaUJBQVk7WUFDN0IsNEJBRUE7WUFBQSxpQ0FBVTtZQUFBLHFFQUEwQztZQUFBLGlCQUFXO1lBQ2pFLGlCQUFpQjtZQUNuQixpQkFBVztZQUVYLCtCQUVFO1lBQUEsa0VBQ0U7WUFHRixrQ0FHRTtZQUFBLGtDQUNFO1lBQUEsZ0VBQXNEO1lBQ3RELGdFQUFtQztZQUNyQywwQkFBZTtZQUVmLGtDQUNFO1lBQUEsZ0VBQXNEO1lBQ3RELGdFQUFtQztZQUNyQywwQkFBZTtZQUVmLGtDQUNFO1lBQUEsZ0VBQXNEO1lBQ3RELGdFQUFtQztZQUNyQywwQkFBZTtZQUVmLGtDQUNFO1lBQUEsZ0VBQXNEO1lBQ3RELGdFQUFtQztZQUNyQywwQkFBZTtZQUVmLGdFQUE2RDtZQUM3RCxnRUFDeUQ7WUFDM0QsaUJBQVE7WUFFUixxQ0FFOEM7WUFFaEQsaUJBQU07WUFFUixpQkFBTTtZQUNSLGlCQUFNOztZQWhFRSxlQUFxQjtZQUFyQixtQ0FBcUI7WUFTckIsZUFBaUM7WUFBakMsK0NBQWlDO1lBUy9CLGVBQXFDO1lBQXJDLHVEQUFxQztZQU9JLGVBQThCO1lBQTlCLGlEQUE4QjtZQUkxRCxlQUF5QjtZQUF6QiwyQ0FBeUI7WUF1QnJCLGdCQUF5QztZQUF6Qyw0REFBeUM7WUFFMUQsZUFBc0Q7WUFBdEQsNkRBQXNEO1lBRzNDLGVBQThCO1lBQTlCLGdEQUE4QixnREFBQSxnQ0FBQTs7eUJEOURuRDtDQTBIQyxBQXpHRCxJQXlHQztTQXBHWSxjQUFjO2tEQUFkLGNBQWM7Y0FMMUIsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLFdBQVcsRUFBRSx3QkFBd0I7Z0JBQ3JDLFNBQVMsRUFBRSxDQUFDLHdCQUF3QixDQUFDO2FBQ3RDOztrQkFFRSxTQUFTO21CQUFDLFlBQVk7O2tCQUN0QixTQUFTO21CQUFDLE9BQU8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIFZpZXdDaGlsZCwgQWZ0ZXJWaWV3SW5pdCwgXHJcbiAgT25EZXN0cm95LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJ1xyXG5pbXBvcnQgeyBGb2ZQZXJtaXNzaW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vZm9mLXBlcm1pc3Npb24uc2VydmljZSdcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBpVXNlciB9IGZyb20gJy4uLy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IE1hdFBhZ2luYXRvciwgTWF0UGFnaW5hdG9ySW50bCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3BhZ2luYXRvcidcclxuaW1wb3J0IHsgTWF0U29ydCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NvcnQnXHJcbmltcG9ydCB7IG1lcmdlLCBPYnNlcnZhYmxlLCBvZiBhcyBvYnNlcnZhYmxlT2YsIFN1YnNjcmlwdGlvbiwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCBzdGFydFdpdGgsIHN3aXRjaE1hcCwgZGVib3VuY2VUaW1lLCB0YXAsIFxyXG4gIGRpc3RpbmN0VW50aWxDaGFuZ2VkLCBmaW5hbGl6ZSwgZmlsdGVyIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IGlmb2ZTZWFyY2ggfSBmcm9tICcuLi8uLi9jb3JlL2NvcmUuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBCcmVha3BvaW50T2JzZXJ2ZXIsIEJyZWFrcG9pbnRzLCBCcmVha3BvaW50U3RhdGUgfSBmcm9tICdAYW5ndWxhci9jZGsvbGF5b3V0J1xyXG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJ1xyXG5pbXBvcnQgeyBGb2ZFcnJvclNlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL2ZvZi1lcnJvci5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBpT3JnYW5pemF0aW9uIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgT2JzZXJ2ZXJzTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL29ic2VydmVycydcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnZm9mLWNvcmUtdXNlcnMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi91c2Vycy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vdXNlcnMuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlcnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB7XHJcbiAgQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yXHJcbiAgQFZpZXdDaGlsZChNYXRTb3J0KSBzb3J0OiBNYXRTb3J0XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBmb2ZQZXJtaXNzaW9uU2VydmljZTogRm9mUGVybWlzc2lvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGZvZk5vdGlmaWNhdGlvblNlcnZpY2U6IEZvZk5vdGlmaWNhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGJyZWFrcG9pbnRPYnNlcnZlcjogQnJlYWtwb2ludE9ic2VydmVyLFxyXG4gICAgcHJpdmF0ZSBmb2ZFcnJvclNlcnZpY2U6IEZvZkVycm9yU2VydmljZVxyXG4gICkgeyBcclxuICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICBicmVha3BvaW50T2JzZXJ2ZXJTdWI6IDxTdWJzY3JpcHRpb24+dW5kZWZpbmVkLCAgICBcclxuICAgIGZpbHRlcjogPE9ic2VydmFibGU8c3RyaW5nPj51bmRlZmluZWQsXHJcbiAgICBmaWx0ZXJPcmdhbml6YXRpb25zQ2hhbmdlOiA8RXZlbnRFbWl0dGVyPGlPcmdhbml6YXRpb24+Pm5ldyBFdmVudEVtaXR0ZXIoKSxcclxuICAgIGZpbHRlck9yZ2FuaXphdGlvbnM6IDxudW1iZXJbXT51bmRlZmluZWRcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICB9XHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlWYXIgPSB7ICAgIFxyXG4gICAgZGlzcGxheWVkQ29sdW1uczogPHN0cmluZ1tdPltdLFxyXG4gICAgZGF0YTogPGlVc2VyW10+W10sXHJcbiAgICByZXN1bHRzTGVuZ3RoOiAwLFxyXG4gICAgcGFnZVNpemU6IDUsXHJcbiAgICBpc0xvYWRpbmdSZXN1bHRzOiB0cnVlLFxyXG4gICAgc2VhcmNoVXNlcnNDdHJsOiBuZXcgRm9ybUNvbnRyb2woKSAgICBcclxuICB9XHJcbiAgLy8gQWxsIGFjdGlvbnMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpQWN0aW9uID0ge1xyXG4gICAgb3JnYW5pc2F0aW9uTXVsdGlTZWxlY3RlZENoYW5nZToob3JnYW5pemF0aW9uOiBpT3JnYW5pemF0aW9uKSA9PiB7ICAgICAgXHJcbiAgICAgIGlmIChvcmdhbml6YXRpb24pIHtcclxuICAgICAgICB0aGlzLnByaVZhci5maWx0ZXJPcmdhbml6YXRpb25zID0gW29yZ2FuaXphdGlvbi5pZF1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnByaVZhci5maWx0ZXJPcmdhbml6YXRpb25zID0gbnVsbFxyXG4gICAgICB9ICAgICAgXHJcbiAgICAgIHRoaXMucHJpVmFyLmZpbHRlck9yZ2FuaXphdGlvbnNDaGFuZ2UuZW1pdChvcmdhbml6YXRpb24pICAgICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnByaVZhci5maWx0ZXIgPSB0aGlzLnVpVmFyLnNlYXJjaFVzZXJzQ3RybC52YWx1ZUNoYW5nZXNcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZGVib3VuY2VUaW1lKDUwMCksXHJcbiAgICAgICAgZGlzdGluY3RVbnRpbENoYW5nZWQoKSwgICAgICAgIFxyXG4gICAgICAgIGZpbHRlcihxdWVyeSA9PiBxdWVyeS5sZW5ndGggPj0gMyB8fCBxdWVyeS5sZW5ndGggPT09IDApICAgICAgICAgICAgIFxyXG4gICAgICApIFxyXG4gICAgXHJcbiAgICB0aGlzLnByaVZhci5icmVha3BvaW50T2JzZXJ2ZXJTdWIgPSB0aGlzLmJyZWFrcG9pbnRPYnNlcnZlci5vYnNlcnZlKEJyZWFrcG9pbnRzLlhTbWFsbClcclxuICAgIC5zdWJzY3JpYmUoKHN0YXRlOiBCcmVha3BvaW50U3RhdGUpID0+IHsgICAgICBcclxuICAgICAgaWYgKHN0YXRlLm1hdGNoZXMpIHtcclxuICAgICAgICAvLyBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ2VtYWlsJywgJ2xvZ2luJ11cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyA+IFhTbWFsbFxyXG4gICAgICAgIHRoaXMudWlWYXIuZGlzcGxheWVkQ29sdW1ucyA9IFsnZW1haWwnLCAnbG9naW4nLCAnZmlyc3ROYW1lJywgJ2xhc3ROYW1lJ11cclxuICAgICAgfVxyXG4gICAgfSkgICBcclxuICB9ICBcclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCkgeyAgICBcclxuICAgIC8vIElmIHRoZSB1c2VyIGNoYW5nZXMgdGhlIHNvcnQgb3JkZXIsIHJlc2V0IGJhY2sgdG8gdGhlIGZpcnN0IHBhZ2UuXHJcbiAgICB0aGlzLnNvcnQuc29ydENoYW5nZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4ID0gMClcclxuXHJcbiAgICBtZXJnZSh0aGlzLnNvcnQuc29ydENoYW5nZSwgdGhpcy5wYWdpbmF0b3IucGFnZSwgdGhpcy5wcmlWYXIuZmlsdGVyLCB0aGlzLnByaVZhci5maWx0ZXJPcmdhbml6YXRpb25zQ2hhbmdlKVxyXG4gICAgICAucGlwZShcclxuICAgICAgICBzdGFydFdpdGgoe30pLFxyXG4gICAgICAgIHN3aXRjaE1hcCgoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSB0cnVlICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5wYWdlU2l6ZSA9IHRoaXMucGFnaW5hdG9yLnBhZ2VTaXplXHJcblxyXG4gICAgICAgICAgcmV0dXJuIHRoaXMuZm9mUGVybWlzc2lvblNlcnZpY2UudXNlci5zZWFyY2goICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMudWlWYXIuc2VhcmNoVXNlcnNDdHJsLnZhbHVlLFxyXG4gICAgICAgICAgICB0aGlzLnByaVZhci5maWx0ZXJPcmdhbml6YXRpb25zLFxyXG4gICAgICAgICAgICB0aGlzLnVpVmFyLnBhZ2VTaXplLCBcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4LCB0aGlzLnNvcnQuYWN0aXZlLCB0aGlzLnNvcnQuZGlyZWN0aW9uKVxyXG4gICAgICAgIH0pLFxyXG4gICAgICAgIG1hcCgoc2VhcmNoOiBpZm9mU2VhcmNoPGlVc2VyPikgPT4geyAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSBmYWxzZVxyXG4gICAgICAgICAgdGhpcy51aVZhci5yZXN1bHRzTGVuZ3RoID0gc2VhcmNoLnRvdGFsICAgICAgICAgIFxyXG4gICAgICAgICAgcmV0dXJuIHNlYXJjaC5kYXRhXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgY2F0Y2hFcnJvcihlcnJvciA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZkVycm9yU2VydmljZS5lcnJvck1hbmFnZShlcnJvcikgICAgICAgICAgXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSBmYWxzZSAgICAgICAgICBcclxuICAgICAgICAgIHJldHVybiBvYnNlcnZhYmxlT2YoW10pXHJcbiAgICAgICAgfSlcclxuICAgICAgKS5zdWJzY3JpYmUoZGF0YSA9PiB7ICAgICAgICBcclxuICAgICAgICB0aGlzLnVpVmFyLmRhdGEgPSBkYXRhXHJcbiAgICAgIH0pXHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIGlmICh0aGlzLnByaVZhci5icmVha3BvaW50T2JzZXJ2ZXJTdWIpIHsgdGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViLnVuc3Vic2NyaWJlKCkgfVxyXG4gIH1cclxuXHJcbn1cclxuIiwiPGRpdiBjbGFzcz1cInJvd1wiPlxyXG4gIDxkaXYgY2xhc3M9XCJjb2wtbWQtM1wiPlxyXG4gICAgPG1hdC1jYXJkIGNsYXNzPVwiY2FyZC10cmVlXCI+XHJcbiAgICAgIDxmb2Ytb3JnYW5pemF0aW9ucy10cmVlXHJcbiAgICAgICAgKHNlbGVjdGVkT3JnYW5pemF0aW9uc0NoYW5nZSk9XCJ1aUFjdGlvbi5vcmdhbmlzYXRpb25NdWx0aVNlbGVjdGVkQ2hhbmdlKCRldmVudClcIlxyXG4gICAgICAgIFttdWx0aVNlbGVjdF09XCJmYWxzZVwiPlxyXG4gICAgICA8L2ZvZi1vcmdhbml6YXRpb25zLXRyZWU+XHJcbiAgICA8L21hdC1jYXJkPlxyXG4gIDwvZGl2PlxyXG4gIDxkaXYgY2xhc3M9XCJjb2wtbWQtOVwiPlxyXG5cclxuICAgIDxtYXQtY2FyZCBjbGFzcz1cImZvZi1oZWFkZXJcIj5cclxuICAgICAgPGgzPkdlc3Rpb24gZGVzIGNvbGxhYm9yYXRldXJzPC9oMz4gICBcclxuICAgICAgPGEgbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwiYWNjZW50XCIgXHJcbiAgICAgICAgW3JvdXRlckxpbmtdPVwiJy9hZG1pbi91c2Vycy9uZXcnXCI+ICAgIFxyXG4gICAgICAgIDxzcGFuPkFqb3V0ZXIgdW4gY29sbGFib3JhdGV1cjwvc3Bhbj5cclxuICAgICAgPC9hPiAgICBcclxuICAgIDwvbWF0LWNhcmQ+XHJcbiAgICBcclxuICAgIDxtYXQtY2FyZCBjbGFzcz1cImZpbHRyZXNcIj5cclxuICAgICAgPG1hdC1mb3JtLWZpZWxkPlxyXG4gICAgICAgIDxtYXQtbGFiZWw+RmlsdHJlPC9tYXQtbGFiZWw+XHJcbiAgICAgICAgPGlucHV0IGF1dG9mb2N1cyBtYXRJbnB1dCBwbGFjZWhvbGRlcj1cImVtYWlsIG91IG5vbSBvdSBwcsOpbm9tIG91IGxvZ2luXCIgXHJcbiAgICAgICAgICBbZm9ybUNvbnRyb2xdPVwidWlWYXIuc2VhcmNoVXNlcnNDdHJsXCI+XHJcbiAgICAgICAgPG1hdC1oaW50PlJlY2hlcmNoZSDDoCBwYXJ0aXIgZGUgMyBjYXJhY3TDqHJlcyBzYWlzaWVzPC9tYXQtaGludD4gICAgXHJcbiAgICAgIDwvbWF0LWZvcm0tZmllbGQ+ICBcclxuICAgIDwvbWF0LWNhcmQ+ICBcclxuICAgIFxyXG4gICAgPGRpdiBjbGFzcz1cImZvZi10YWJsZS1jb250YWluZXIgbWF0LWVsZXZhdGlvbi16MlwiPlxyXG4gICAgXHJcbiAgICAgIDxkaXYgY2xhc3M9XCJ0YWJsZS1sb2FkaW5nLXNoYWRlIGZvZi1sb2FkaW5nXCIgKm5nSWY9XCJ1aVZhci5pc0xvYWRpbmdSZXN1bHRzXCI+XHJcbiAgICAgICAgPG1hdC1zcGlubmVyIGRpYW1ldGVyPTIwPjwvbWF0LXNwaW5uZXI+IDxzcGFuPkNoYXJnZW1lbnRzIGRlcyBjb2xsYWJvcmF0ZXVycy4uLjwvc3Bhbj5cclxuICAgICAgPC9kaXY+XHJcbiAgICBcclxuICAgICAgPHRhYmxlIG1hdC10YWJsZSBbZGF0YVNvdXJjZV09XCJ1aVZhci5kYXRhXCIgY2xhc3M9XCJkYXRhLXRhYmxlXCJcclxuICAgICAgICAgICAgbWF0U29ydCBtYXRTb3J0QWN0aXZlPVwiZW1haWxcIiBtYXRTb3J0RGlzYWJsZUNsZWFyIG1hdFNvcnREaXJlY3Rpb249XCJhc2NcIj5cclxuICAgICAgICBcclxuICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImVtYWlsXCI+XHJcbiAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsIG1hdC1zb3J0LWhlYWRlciAqbWF0SGVhZGVyQ2VsbERlZj5FbWFpbDwvdGg+XHJcbiAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cuZW1haWx9fTwvdGQ+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICBcclxuICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImxvZ2luXCI+XHJcbiAgICAgICAgICA8dGggbWF0LWhlYWRlci1jZWxsIG1hdC1zb3J0LWhlYWRlciAqbWF0SGVhZGVyQ2VsbERlZj5Mb2dpbjwvdGg+XHJcbiAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cubG9naW59fTwvdGQ+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICBcclxuICAgICAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImZpcnN0TmFtZVwiPlxyXG4gICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCBtYXQtc29ydC1oZWFkZXIgKm1hdEhlYWRlckNlbGxEZWY+UHLDqW5vbTwvdGg+XHJcbiAgICAgICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cuZmlyc3ROYW1lfX08L3RkPlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgXHJcbiAgICAgICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJsYXN0TmFtZVwiPlxyXG4gICAgICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCBtYXQtc29ydC1oZWFkZXIgKm1hdEhlYWRlckNlbGxEZWY+Tm9tPC90aD5cclxuICAgICAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIj57e3Jvdy5sYXN0TmFtZX19PC90ZD5cclxuICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgIFxyXG4gICAgICAgIDx0ciBtYXQtaGVhZGVyLXJvdyAqbWF0SGVhZGVyUm93RGVmPVwidWlWYXIuZGlzcGxheWVkQ29sdW1uc1wiPjwvdHI+XHJcbiAgICAgICAgPHRyIG1hdC1yb3cgY2xhc3M9XCJmb2YtZWxlbWVudC1vdmVyXCIgW3JvdXRlckxpbmtdPVwicm93LmlkXCJcclxuICAgICAgICAgICptYXRSb3dEZWY9XCJsZXQgcm93OyBjb2x1bW5zOiB1aVZhci5kaXNwbGF5ZWRDb2x1bW5zO1wiPjwvdHI+XHJcbiAgICAgIDwvdGFibGU+XHJcbiAgICBcclxuICAgICAgPG1hdC1wYWdpbmF0b3IgW2xlbmd0aF09XCJ1aVZhci5yZXN1bHRzTGVuZ3RoXCIgXHJcbiAgICAgICAgW3BhZ2VTaXplT3B0aW9uc109XCJbNSwgMTAsIDI1LCAxMDBdXCJcclxuICAgICAgICBbcGFnZVNpemVdPVwidWlWYXIucGFnZVNpemVcIj48L21hdC1wYWdpbmF0b3I+XHJcbiAgICBcclxuICAgIDwvZGl2PlxyXG4gICAgXHJcbiAgPC9kaXY+XHJcbjwvZGl2PlxyXG4iXX0=
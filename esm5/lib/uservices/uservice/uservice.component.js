import { Component } from '@angular/core';
import { Validators } from "@angular/forms";
import { fofUtilsForm } from '../../core/fof-utils';
import * as i0 from "@angular/core";
import * as i1 from "../uservices.service";
import * as i2 from "@angular/router";
import * as i3 from "@angular/forms";
import * as i4 from "../../core/notification/notification.service";
import * as i5 from "../../core/fof-dialog.service";
import * as i6 from "../../core/fof-error.service";
import * as i7 from "@angular/material/card";
import * as i8 from "@angular/material/button";
import * as i9 from "@angular/common";
import * as i10 from "@angular/material/form-field";
import * as i11 from "@angular/material/input";
import * as i12 from "@angular/material/checkbox";
import * as i13 from "@angular/material/progress-spinner";
function UserviceComponent_div_11_mat_error_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le nom technique est obligatoire et doit \u00EAtre compos\u00E9 de 3 \u00E0 40 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function UserviceComponent_div_11_mat_error_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " Le nom est obligatoire et doit \u00EAtre compos\u00E9 de moins de 30 caract\u00E8res ");
    i0.ɵɵelementEnd();
} }
function UserviceComponent_div_11_mat_error_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " L'url doit \u00EAtre valide ");
    i0.ɵɵelementEnd();
} }
function UserviceComponent_div_11_mat_error_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1, " L'url doit \u00EAtre valide ");
    i0.ɵɵelementEnd();
} }
function UserviceComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 8);
    i0.ɵɵelementStart(1, "form", 9);
    i0.ɵɵelementStart(2, "mat-form-field");
    i0.ɵɵelement(3, "input", 10);
    i0.ɵɵtemplate(4, UserviceComponent_div_11_mat_error_4_Template, 2, 0, "mat-error", 11);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(5, "mat-form-field");
    i0.ɵɵelement(6, "input", 12);
    i0.ɵɵtemplate(7, UserviceComponent_div_11_mat_error_7_Template, 2, 0, "mat-error", 11);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(8, "mat-form-field");
    i0.ɵɵelement(9, "input", 13);
    i0.ɵɵtemplate(10, UserviceComponent_div_11_mat_error_10_Template, 2, 0, "mat-error", 11);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(11, "mat-form-field");
    i0.ɵɵelement(12, "input", 14);
    i0.ɵɵtemplate(13, UserviceComponent_div_11_mat_error_13_Template, 2, 0, "mat-error", 11);
    i0.ɵɵelementEnd();
    i0.ɵɵelementStart(14, "mat-checkbox", 15);
    i0.ɵɵtext(15, "Accessible aux utilisateurs ?");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r506 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("formGroup", ctx_r506.uiVar.form);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("technicalName").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("name").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("backUrl").invalid);
    i0.ɵɵadvance(3);
    i0.ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("frontUrl").invalid);
} }
function UserviceComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵelement(1, "mat-spinner", 17);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des uServices et des authorisations...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
var UserviceComponent = /** @class */ (function () {
    function UserviceComponent(uUservicesService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, fofErrorService) {
        var _this = this;
        this.uUservicesService = uUservicesService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            uServiceId: undefined,
            URL_REGEXP: /^[A-Za-z][A-Za-z\d.+-]*:\/*(?:\w+(?::\w+)?@)?[^\s/]+(?::\d+)?(?:\/[\w#!:.?+=&%@\-/]*)?$/
        };
        // All private functions
        this.privFunc = {
            uServiceLoad: function () {
                _this.uiVar.loading = true;
                _this.uUservicesService.uServices.getOne(_this.priVar.uServiceId)
                    .toPromise()
                    .then(function (uservice) {
                    _this.uiVar.uService = uservice;
                    _this.uiVar.form.patchValue(_this.uiVar.uService);
                })
                    .catch(function (reason) {
                    _this.fofErrorService.errorManage(reason);
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                })
                    .finally(function () {
                    _this.uiVar.loading = false;
                });
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouveau rôle',
            permissionGroups: undefined,
            loading: false,
            uServiceIsNew: false,
            uService: undefined,
            form: this.formBuilder.group({
                technicalName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
                name: ['', [Validators.required, Validators.maxLength(30)]],
                frontUrl: ['', [Validators.pattern(this.priVar.URL_REGEXP)]],
                backUrl: ['', [Validators.pattern(this.priVar.URL_REGEXP)]],
                availableForUsers: ['']
            })
        };
        // All actions shared with UI 
        this.uiAction = {
            uServiceSave: function () {
                var uServiceToSave = _this.uiVar.form.value;
                console.log('uServiceToSave', uServiceToSave);
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                if (_this.uiVar.uServiceIsNew) {
                    _this.uUservicesService.uServices.create(uServiceToSave)
                        .toPromise()
                        .then(function (newUservice) {
                        _this.fofNotificationService.success('Mini-service sauvé', { mustDisappearAfter: 1000 });
                        _this.priVar.uServiceId = newUservice.id;
                        _this.uiVar.title = 'Modification de mini-service';
                        _this.uiVar.uServiceIsNew = false;
                        _this.privFunc.uServiceLoad();
                    });
                }
                else {
                    uServiceToSave.id = _this.uiVar.uService.id;
                    _this.uUservicesService.uServices.update(uServiceToSave)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.success('Mini-service sauvé', { mustDisappearAfter: 1000 });
                    });
                }
            },
            uServiceCancel: function () {
                _this.privFunc.uServiceLoad();
            },
            uServiceDelete: function () {
                _this.fofDialogService.openYesNo({
                    question: 'Voulez vous vraiment supprimer le mini-service ?'
                }).then(function (yes) {
                    if (yes) {
                        _this.uUservicesService.uServices.delete(_this.uiVar.uService)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Mini-service supprimé');
                            _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        });
                    }
                });
            }
        };
    }
    // Angular events
    UserviceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.paramMap.subscribe(function (params) {
            var id = params.get('id');
            _this.priVar.uServiceId = id;
            if (id) {
                if (id.toLowerCase() == 'new') {
                    _this.uiVar.uServiceIsNew = true;
                }
                else {
                    _this.uiVar.title = 'Modification de mini-service';
                    _this.privFunc.uServiceLoad();
                }
            }
        });
    };
    UserviceComponent.ɵfac = function UserviceComponent_Factory(t) { return new (t || UserviceComponent)(i0.ɵɵdirectiveInject(i1.UservicesService), i0.ɵɵdirectiveInject(i2.ActivatedRoute), i0.ɵɵdirectiveInject(i3.FormBuilder), i0.ɵɵdirectiveInject(i4.FofNotificationService), i0.ɵɵdirectiveInject(i5.FofDialogService), i0.ɵɵdirectiveInject(i2.Router), i0.ɵɵdirectiveInject(i6.FofErrorService)); };
    UserviceComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UserviceComponent, selectors: [["fof-core-uservice"]], decls: 13, vars: 3, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "fof-fade-in detail", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [1, "fof-fade-in", "detail"], [3, "formGroup"], ["matInput", "", "required", "", "formControlName", "technicalName", "placeholder", "Nom technique", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "formControlName", "name", "placeholder", "Nom", "value", ""], ["matInput", "", "type", "url", "formControlName", "backUrl", "placeholder", "Url du backend", "value", ""], ["matInput", "", "type", "url", "formControlName", "frontUrl", "placeholder", "Url du frontend", "value", ""], ["formControlName", "availableForUsers"], [1, "fof-loading"], ["diameter", "20"]], template: function UserviceComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "mat-card", 0);
            i0.ɵɵelementStart(1, "h3");
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "div", 1);
            i0.ɵɵelementStart(4, "button", 2);
            i0.ɵɵlistener("click", function UserviceComponent_Template_button_click_4_listener() { return ctx.uiAction.uServiceCancel(); });
            i0.ɵɵtext(5, "Annuler");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "button", 3);
            i0.ɵɵlistener("click", function UserviceComponent_Template_button_click_6_listener() { return ctx.uiAction.uServiceDelete(); });
            i0.ɵɵtext(7, "Supprimer");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(8, "button", 4);
            i0.ɵɵlistener("click", function UserviceComponent_Template_button_click_8_listener() { return ctx.uiAction.uServiceSave(); });
            i0.ɵɵtext(9, " Enregister");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(10, "div", 5);
            i0.ɵɵtemplate(11, UserviceComponent_div_11_Template, 16, 5, "div", 6);
            i0.ɵɵtemplate(12, UserviceComponent_div_12_Template, 4, 0, "div", 7);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.uiVar.title);
            i0.ɵɵadvance(9);
            i0.ɵɵproperty("ngIf", !ctx.uiVar.loading);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.uiVar.loading);
        } }, directives: [i7.MatCard, i8.MatButton, i9.NgIf, i3.ɵangular_packages_forms_forms_y, i3.NgControlStatusGroup, i3.FormGroupDirective, i10.MatFormField, i11.MatInput, i3.DefaultValueAccessor, i3.RequiredValidator, i3.NgControlStatus, i3.FormControlName, i12.MatCheckbox, i10.MatError, i13.MatSpinner], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
    return UserviceComponent;
}());
export { UserviceComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UserviceComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-uservice',
                templateUrl: './uservice.component.html',
                styleUrls: ['./uservice.component.scss']
            }]
    }], function () { return [{ type: i1.UservicesService }, { type: i2.ActivatedRoute }, { type: i3.FormBuilder }, { type: i4.FofNotificationService }, { type: i5.FofDialogService }, { type: i2.Router }, { type: i6.FofErrorService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2UuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvdXNlcnZpY2VzL3VzZXJ2aWNlL3VzZXJ2aWNlLmNvbXBvbmVudC50cyIsImxpYi91c2VydmljZXMvdXNlcnZpY2UvdXNlcnZpY2UuY29tcG9uZW50Lmh0bWwiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBbUMsTUFBTSxlQUFlLENBQUE7QUFRMUUsT0FBTyxFQUEwQixVQUFVLEVBQUcsTUFBTSxnQkFBZ0IsQ0FBQTtBQUdwRSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sc0JBQXNCLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7SUNTM0MsaUNBQ0U7SUFBQSxnSEFDRjtJQUFBLGlCQUFZOzs7SUFPWixpQ0FDRTtJQUFBLHNHQUNGO0lBQUEsaUJBQVk7OztJQVFaLGlDQUNFO0lBQUEsNkNBQ0Y7SUFBQSxpQkFBWTs7O0lBUVosaUNBQ0U7SUFBQSw2Q0FDRjtJQUFBLGlCQUFZOzs7SUFyQ2xCLDhCQUNFO0lBQUEsK0JBQ0U7SUFBQSxzQ0FDRTtJQUFBLDRCQUdBO0lBQUEsc0ZBQ0U7SUFFSixpQkFBaUI7SUFFakIsc0NBQ0U7SUFBQSw0QkFHQTtJQUFBLHNGQUNFO0lBRUosaUJBQWlCO0lBRWpCLHNDQUNFO0lBQUEsNEJBSUE7SUFBQSx3RkFDRTtJQUVKLGlCQUFpQjtJQUVqQix1Q0FDRTtJQUFBLDZCQUlBO0lBQUEsd0ZBQ0U7SUFFSixpQkFBaUI7SUFFakIseUNBRUc7SUFBQSw4Q0FBNkI7SUFBQSxpQkFBZTtJQWFqRCxpQkFBTztJQUNULGlCQUFNOzs7SUF2REUsZUFBd0I7SUFBeEIsK0NBQXdCO0lBS2YsZUFBK0M7SUFBL0MsdUVBQStDO0lBUy9DLGVBQXNDO0lBQXRDLDhEQUFzQztJQVV0QyxlQUF5QztJQUF6QyxpRUFBeUM7SUFVekMsZUFBMEM7SUFBMUMsa0VBQTBDOzs7SUFzQjNELCtCQUNFO0lBQUEsa0NBQXVDO0lBQUMsNEJBQU07SUFBQSxrRUFBa0Q7SUFBQSxpQkFBTztJQUN6RyxpQkFBTTs7QUQzRFI7SUFPRSwyQkFDVSxpQkFBbUMsRUFDbkMsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsc0JBQThDLEVBQzlDLGdCQUFrQyxFQUNsQyxNQUFjLEVBQ2QsZUFBZ0M7UUFQMUMsaUJBU0M7UUFSUyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ25DLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUkxQyx3QkFBd0I7UUFDaEIsV0FBTSxHQUFHO1lBQ2YsVUFBVSxFQUFVLFNBQVM7WUFDN0IsVUFBVSxFQUFFLHlGQUF5RjtTQUN0RyxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRztZQUNqQixZQUFZLEVBQUM7Z0JBQ1gsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFBO2dCQUV6QixLQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztxQkFDOUQsU0FBUyxFQUFFO3FCQUNYLElBQUksQ0FBQyxVQUFBLFFBQVE7b0JBQ1osS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFBO29CQUM5QixLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQTtnQkFDakQsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQyxVQUFBLE1BQU07b0JBQ1gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7b0JBQ3hDLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBQyxVQUFVLEVBQUUsS0FBSSxDQUFDLGNBQWMsRUFBQyxDQUFDLENBQUE7Z0JBQ2xFLENBQUMsQ0FBQztxQkFDRCxPQUFPLENBQUM7b0JBQ1AsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFBO2dCQUM1QixDQUFDLENBQUMsQ0FBQTtZQUVKLENBQUM7U0FDRixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLEtBQUssRUFBRSxjQUFjO1lBQ3JCLGdCQUFnQixFQUFPLFNBQVM7WUFDaEMsT0FBTyxFQUFFLEtBQUs7WUFDZCxhQUFhLEVBQUUsS0FBSztZQUNwQixRQUFRLEVBQWEsU0FBUztZQUM5QixJQUFJLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLGFBQWEsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQzdGLElBQUksRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUMzRCxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDNUQsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQzNELGlCQUFpQixFQUFFLENBQUMsRUFBRSxDQUFDO2FBQ3hCLENBQUM7U0FDSCxDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRztZQUNoQixZQUFZLEVBQUM7Z0JBQ1gsSUFBTSxjQUFjLEdBQWMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBO2dCQUV2RCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGNBQWMsQ0FBQyxDQUFBO2dCQUU3QyxJQUFJLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO29CQUMxQixLQUFJLENBQUMsc0JBQXNCLENBQUMsS0FBSyxDQUFDLG9EQUFvRCxDQUFDLENBQUE7b0JBQ3ZGLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFBO29CQUMvQyxPQUFNO2lCQUNQO2dCQUVELElBQUksS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUU7b0JBQzVCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQzt5QkFDdEQsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxVQUFDLFdBQXNCO3dCQUMzQixLQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLG9CQUFvQixFQUFFLEVBQUMsa0JBQWtCLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTt3QkFDckYsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQTt3QkFDdkMsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsOEJBQThCLENBQUE7d0JBQ2pELEtBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQTt3QkFDaEMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtvQkFDOUIsQ0FBQyxDQUFDLENBQUE7aUJBQ0g7cUJBQU07b0JBQ0wsY0FBYyxDQUFDLEVBQUUsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUE7b0JBRTFDLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQzt5QkFDdEQsU0FBUyxFQUFFO3lCQUNYLElBQUksQ0FBQyxVQUFBLE1BQU07d0JBQ1YsS0FBSSxDQUFDLHNCQUFzQixDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxFQUFDLGtCQUFrQixFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7b0JBQ3ZGLENBQUMsQ0FBQyxDQUFBO2lCQUNIO1lBQ0gsQ0FBQztZQUNELGNBQWMsRUFBQztnQkFDYixLQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksRUFBRSxDQUFBO1lBQzlCLENBQUM7WUFDRCxjQUFjLEVBQUM7Z0JBQ2IsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztvQkFDOUIsUUFBUSxFQUFFLGtEQUFrRDtpQkFDN0QsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFBLEdBQUc7b0JBQ1QsSUFBSSxHQUFHLEVBQUU7d0JBQ1AsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7NkJBQzNELFNBQVMsRUFBRTs2QkFDWCxJQUFJLENBQUMsVUFBQSxNQUFNOzRCQUNWLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQTs0QkFDNUQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsY0FBYyxFQUFDLENBQUMsQ0FBQTt3QkFDbEUsQ0FBQyxDQUFDLENBQUE7cUJBQ0g7Z0JBQ0gsQ0FBQyxDQUFDLENBQUE7WUFDSixDQUFDO1NBQ0YsQ0FBQTtJQTdGRCxDQUFDO0lBOEZELGlCQUFpQjtJQUNqQixvQ0FBUSxHQUFSO1FBQUEsaUJBY0M7UUFiQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQzNDLElBQU0sRUFBRSxHQUFRLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7WUFDaEMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFBO1lBRTNCLElBQUksRUFBRSxFQUFFO2dCQUNOLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxJQUFJLEtBQUssRUFBRTtvQkFDN0IsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFBO2lCQUNoQztxQkFBTTtvQkFDTCxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyw4QkFBOEIsQ0FBQTtvQkFDakQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtpQkFDN0I7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztzRkF4SFUsaUJBQWlCOzBEQUFqQixpQkFBaUI7WUNuQjlCLG1DQUNFO1lBQUEsMEJBQUk7WUFBQSxZQUFpQjtZQUFBLGlCQUFLO1lBQzFCLDhCQUNFO1lBQUEsaUNBQ3NDO1lBQXBDLDhGQUFTLDZCQUF5QixJQUFDO1lBQUMsdUJBQU87WUFBQSxpQkFBUztZQUN0RCxpQ0FDc0M7WUFBcEMsOEZBQVMsNkJBQXlCLElBQUM7WUFBQyx5QkFBUztZQUFBLGlCQUFTO1lBQ3hELGlDQUVFO1lBREEsOEZBQVMsMkJBQXVCLElBQUM7WUFDakMsMkJBQVU7WUFBQSxpQkFBUztZQUN2QixpQkFBTTtZQUNSLGlCQUFXO1lBRVgsK0JBQ0U7WUFBQSxxRUFDRTtZQXdERixvRUFDRTtZQUVKLGlCQUFNOztZQXpFQSxlQUFpQjtZQUFqQixxQ0FBaUI7WUFhaEIsZUFBc0I7WUFBdEIseUNBQXNCO1lBeUR0QixlQUFxQjtZQUFyQix3Q0FBcUI7OzRCRHZFNUI7Q0E0SUMsQUE5SEQsSUE4SEM7U0F6SFksaUJBQWlCO2tEQUFqQixpQkFBaUI7Y0FMN0IsU0FBUztlQUFDO2dCQUNULFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLFdBQVcsRUFBRSwyQkFBMkI7Z0JBQ3hDLFNBQVMsRUFBRSxDQUFDLDJCQUEyQixDQUFDO2FBQ3pDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuLy8gaW1wb3J0IHsgRm9mUGVybWlzc2lvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ZvZi1wZXJtaXNzaW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IFVzZXJ2aWNlc1NlcnZpY2UgfSBmcm9tICcuLi91c2VydmljZXMuc2VydmljZSdcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcidcclxuLy8gaW1wb3J0IHsgaVBlcm1pc3Npb24sIGlGb2Z1U2VydmljZSwgaVJvbGVQZXJtaXNzaW9uIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgaVVzZXJ2aWNlIH0gZnJvbSAnLi4vLi4vcGVybWlzc2lvbi9pbnRlcmZhY2VzL3Blcm1pc3Npb25zLmludGVyZmFjZSdcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgZm9ya0pvaW4sIG9mIH0gZnJvbSAncnhqcydcclxuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQnVpbGRlciwgVmFsaWRhdG9ycyAgfSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIlxyXG5pbXBvcnQgeyBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vY29yZS9ub3RpZmljYXRpb24vbm90aWZpY2F0aW9uLnNlcnZpY2UnXHJcbmltcG9ydCB7IEZvZkRpYWxvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9jb3JlL2ZvZi1kaWFsb2cuc2VydmljZSdcclxuaW1wb3J0IHsgZm9mVXRpbHNGb3JtIH0gZnJvbSAnLi4vLi4vY29yZS9mb2YtdXRpbHMnXHJcbmltcG9ydCB7IEZvZkVycm9yU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvZm9mLWVycm9yLnNlcnZpY2UnXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2ZvZi1jb3JlLXVzZXJ2aWNlJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vdXNlcnZpY2UuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3VzZXJ2aWNlLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFVzZXJ2aWNlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgY29uc3RydWN0b3IgKFxyXG4gICAgcHJpdmF0ZSB1VXNlcnZpY2VzU2VydmljZTogVXNlcnZpY2VzU2VydmljZSwgICAgXHJcbiAgICBwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSBmb2ZOb3RpZmljYXRpb25TZXJ2aWNlOiBGb2ZOb3RpZmljYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBmb2ZEaWFsb2dTZXJ2aWNlOiBGb2ZEaWFsb2dTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgZm9mRXJyb3JTZXJ2aWNlOiBGb2ZFcnJvclNlcnZpY2VcclxuICApIHsgICAgIFxyXG4gIH1cclxuXHJcbiAgLy8gQWxsIHByaXZhdGUgdmFyaWFibGVzXHJcbiAgcHJpdmF0ZSBwcmlWYXIgPSB7XHJcbiAgICB1U2VydmljZUlkOiA8bnVtYmVyPnVuZGVmaW5lZCwgICAgXHJcbiAgICBVUkxfUkVHRVhQOiAvXltBLVphLXpdW0EtWmEtelxcZC4rLV0qOlxcLyooPzpcXHcrKD86OlxcdyspP0ApP1teXFxzL10rKD86OlxcZCspPyg/OlxcL1tcXHcjITouPys9JiVAXFwtL10qKT8kL1xyXG4gIH1cclxuICAvLyBBbGwgcHJpdmF0ZSBmdW5jdGlvbnNcclxuICBwcml2YXRlIHByaXZGdW5jID0ge1xyXG4gICAgdVNlcnZpY2VMb2FkOigpID0+IHtcclxuICAgICAgdGhpcy51aVZhci5sb2FkaW5nID0gdHJ1ZVxyXG5cclxuICAgICAgdGhpcy51VXNlcnZpY2VzU2VydmljZS51U2VydmljZXMuZ2V0T25lKHRoaXMucHJpVmFyLnVTZXJ2aWNlSWQpXHJcbiAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAudGhlbih1c2VydmljZSA9PiB7XHJcbiAgICAgICAgdGhpcy51aVZhci51U2VydmljZSA9IHVzZXJ2aWNlXHJcbiAgICAgICAgdGhpcy51aVZhci5mb3JtLnBhdGNoVmFsdWUodGhpcy51aVZhci51U2VydmljZSlcclxuICAgICAgfSlcclxuICAgICAgLmNhdGNoKHJlYXNvbiA9PiB7XHJcbiAgICAgICAgdGhpcy5mb2ZFcnJvclNlcnZpY2UuZXJyb3JNYW5hZ2UocmVhc29uKVxyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLi4vJ10sIHtyZWxhdGl2ZVRvOiB0aGlzLmFjdGl2YXRlZFJvdXRlfSlcclxuICAgICAgfSlcclxuICAgICAgLmZpbmFsbHkoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMudWlWYXIubG9hZGluZyA9IGZhbHNlXHJcbiAgICAgIH0pXHJcbiAgICAgXHJcbiAgICB9XHJcbiAgfVxyXG4gIC8vIEFsbCB2YXJpYWJsZXMgc2hhcmVkIHdpdGggVUkgXHJcbiAgcHVibGljIHVpVmFyID0ge1xyXG4gICAgdGl0bGU6ICdOb3V2ZWF1IHLDtGxlJyxcclxuICAgIHBlcm1pc3Npb25Hcm91cHM6IDxhbnk+dW5kZWZpbmVkLFxyXG4gICAgbG9hZGluZzogZmFsc2UsXHJcbiAgICB1U2VydmljZUlzTmV3OiBmYWxzZSxcclxuICAgIHVTZXJ2aWNlOiA8aVVzZXJ2aWNlPnVuZGVmaW5lZCxcclxuICAgIGZvcm06IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoeyAgXHJcbiAgICAgIHRlY2huaWNhbE5hbWU6IFsnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMubWluTGVuZ3RoKDMpLCBWYWxpZGF0b3JzLm1heExlbmd0aCgzMCldXSxcclxuICAgICAgbmFtZTogWycnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5tYXhMZW5ndGgoMzApXV0sXHJcbiAgICAgIGZyb250VXJsOiBbJycsIFtWYWxpZGF0b3JzLnBhdHRlcm4odGhpcy5wcmlWYXIuVVJMX1JFR0VYUCldXSxcclxuICAgICAgYmFja1VybDogWycnLCBbVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMucHJpVmFyLlVSTF9SRUdFWFApXV0sXHJcbiAgICAgIGF2YWlsYWJsZUZvclVzZXJzOiBbJyddICAgICAgXHJcbiAgICB9KVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgICB1U2VydmljZVNhdmU6KCkgPT4geyAgICAgIFxyXG4gICAgICBjb25zdCB1U2VydmljZVRvU2F2ZTogaVVzZXJ2aWNlID0gdGhpcy51aVZhci5mb3JtLnZhbHVlXHJcblxyXG4gICAgICBjb25zb2xlLmxvZygndVNlcnZpY2VUb1NhdmUnLCB1U2VydmljZVRvU2F2ZSlcclxuXHJcbiAgICAgIGlmICghdGhpcy51aVZhci5mb3JtLnZhbGlkKSB7XHJcbiAgICAgICAgdGhpcy5mb2ZOb3RpZmljYXRpb25TZXJ2aWNlLmVycm9yKCdWZXVpbGxleiBjb3JyaWdlciBsZXMgZXJyZXVycyBhdmFudCBkZSBzYXV2ZWdhcmRlcicpXHJcbiAgICAgICAgZm9mVXRpbHNGb3JtLnZhbGlkYXRlQWxsRmllbGRzKHRoaXMudWlWYXIuZm9ybSlcclxuICAgICAgICByZXR1cm5cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMudWlWYXIudVNlcnZpY2VJc05ldykgeyAgICAgICAgXHJcbiAgICAgICAgdGhpcy51VXNlcnZpY2VzU2VydmljZS51U2VydmljZXMuY3JlYXRlKHVTZXJ2aWNlVG9TYXZlKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKChuZXdVc2VydmljZTogaVVzZXJ2aWNlKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnTWluaS1zZXJ2aWNlIHNhdXbDqScsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IDEwMDB9KVxyXG4gICAgICAgICAgdGhpcy5wcmlWYXIudVNlcnZpY2VJZCA9IG5ld1VzZXJ2aWNlLmlkXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnRpdGxlID0gJ01vZGlmaWNhdGlvbiBkZSBtaW5pLXNlcnZpY2UnXHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnVTZXJ2aWNlSXNOZXcgPSBmYWxzZVxyXG4gICAgICAgICAgdGhpcy5wcml2RnVuYy51U2VydmljZUxvYWQoKSAgICAgICAgICBcclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHVTZXJ2aWNlVG9TYXZlLmlkID0gdGhpcy51aVZhci51U2VydmljZS5pZCBcclxuICAgICAgXHJcbiAgICAgICAgdGhpcy51VXNlcnZpY2VzU2VydmljZS51U2VydmljZXMudXBkYXRlKHVTZXJ2aWNlVG9TYXZlKVxyXG4gICAgICAgIC50b1Byb21pc2UoKVxyXG4gICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvZk5vdGlmaWNhdGlvblNlcnZpY2Uuc3VjY2VzcygnTWluaS1zZXJ2aWNlIHNhdXbDqScsIHttdXN0RGlzYXBwZWFyQWZ0ZXI6IDEwMDB9KVxyXG4gICAgICAgIH0pIFxyXG4gICAgICB9XHJcbiAgICB9LFxyXG4gICAgdVNlcnZpY2VDYW5jZWw6KCkgPT4geyAgICAgIFxyXG4gICAgICB0aGlzLnByaXZGdW5jLnVTZXJ2aWNlTG9hZCgpXHJcbiAgICB9LFxyXG4gICAgdVNlcnZpY2VEZWxldGU6KCkgPT4ge1xyXG4gICAgICB0aGlzLmZvZkRpYWxvZ1NlcnZpY2Uub3Blblllc05vKHsgICAgICAgIFxyXG4gICAgICAgIHF1ZXN0aW9uOiAnVm91bGV6IHZvdXMgdnJhaW1lbnQgc3VwcHJpbWVyIGxlIG1pbmktc2VydmljZSA/J1xyXG4gICAgICB9KS50aGVuKHllcyA9PiB7XHJcbiAgICAgICAgaWYgKHllcykge1xyXG4gICAgICAgICAgdGhpcy51VXNlcnZpY2VzU2VydmljZS51U2VydmljZXMuZGVsZXRlKHRoaXMudWlWYXIudVNlcnZpY2UpXHJcbiAgICAgICAgICAudG9Qcm9taXNlKClcclxuICAgICAgICAgIC50aGVuKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9mTm90aWZpY2F0aW9uU2VydmljZS5zdWNjZXNzKCdNaW5pLXNlcnZpY2Ugc3VwcHJpbcOpJylcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLi8nXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGV9KVxyXG4gICAgICAgICAgfSkgXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gIH1cclxuICAvLyBBbmd1bGFyIGV2ZW50c1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5wYXJhbU1hcC5zdWJzY3JpYmUocGFyYW1zID0+IHtcclxuICAgICAgY29uc3QgaWQ6IGFueSA9IHBhcmFtcy5nZXQoJ2lkJylcclxuICAgICAgdGhpcy5wcmlWYXIudVNlcnZpY2VJZCA9IGlkXHJcbiAgICAgIFxyXG4gICAgICBpZiAoaWQpIHtcclxuICAgICAgICBpZiAoaWQudG9Mb3dlckNhc2UoKSA9PSAnbmV3Jykge1xyXG4gICAgICAgICAgdGhpcy51aVZhci51U2VydmljZUlzTmV3ID0gdHJ1ZVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLnRpdGxlID0gJ01vZGlmaWNhdGlvbiBkZSBtaW5pLXNlcnZpY2UnXHJcbiAgICAgICAgICB0aGlzLnByaXZGdW5jLnVTZXJ2aWNlTG9hZCgpXHJcbiAgICAgICAgfVxyXG4gICAgICB9ICAgICAgXHJcbiAgICB9KVxyXG4gIH1cclxufVxyXG4iLCI8bWF0LWNhcmQgY2xhc3M9XCJmb2YtaGVhZGVyXCI+ICAgICAgICBcclxuICA8aDM+e3sgdWlWYXIudGl0bGUgfX08L2gzPiBcclxuICA8ZGl2IGNsYXNzPVwiZm9mLXRvb2xiYXJcIj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uXHJcbiAgICAgIChjbGljayk9XCJ1aUFjdGlvbi51U2VydmljZUNhbmNlbCgpXCI+QW5udWxlcjwvYnV0dG9uPlxyXG4gICAgPGJ1dHRvbiBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJ3YXJuXCJcclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVTZXJ2aWNlRGVsZXRlKClcIj5TdXBwcmltZXI8L2J1dHRvbj5cclxuICAgIDxidXR0b24gbWF0LXN0cm9rZWQtYnV0dG9uIGNvbG9yPVwiYWNjZW50XCJcclxuICAgICAgKGNsaWNrKT1cInVpQWN0aW9uLnVTZXJ2aWNlU2F2ZSgpXCI+XHJcbiAgICAgIEVucmVnaXN0ZXI8L2J1dHRvbj4gICAgXHJcbiAgPC9kaXY+IFxyXG48L21hdC1jYXJkPlxyXG5cclxuPGRpdiBjbGFzcz1cIm1haW5cIj4gIFxyXG4gIDxkaXYgKm5nSWY9XCIhdWlWYXIubG9hZGluZ1wiIGNsYXNzPVwiZm9mLWZhZGUtaW4gZGV0YWlsXCI+ICAgIFxyXG4gICAgPGZvcm0gW2Zvcm1Hcm91cF09XCJ1aVZhci5mb3JtXCI+XHJcbiAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICA8aW5wdXQgbWF0SW5wdXQgcmVxdWlyZWRcclxuICAgICAgICAgIGZvcm1Db250cm9sTmFtZT1cInRlY2huaWNhbE5hbWVcIlxyXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJOb20gdGVjaG5pcXVlXCIgdmFsdWU9XCJcIj5cclxuICAgICAgICA8bWF0LWVycm9yICpuZ0lmPVwidWlWYXIuZm9ybS5nZXQoJ3RlY2huaWNhbE5hbWUnKS5pbnZhbGlkXCI+XHJcbiAgICAgICAgICBMZSBub20gdGVjaG5pcXVlIGVzdCBvYmxpZ2F0b2lyZSBldCBkb2l0IMOqdHJlIGNvbXBvc8OpIGRlIDMgw6AgNDAgY2FyYWN0w6hyZXNcclxuICAgICAgICA8L21hdC1lcnJvcj5cclxuICAgICAgPC9tYXQtZm9ybS1maWVsZD5cclxuXHJcbiAgICAgIDxtYXQtZm9ybS1maWVsZD5cclxuICAgICAgICA8aW5wdXQgbWF0SW5wdXQgcmVxdWlyZWRcclxuICAgICAgICAgIGZvcm1Db250cm9sTmFtZT1cIm5hbWVcIlxyXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJOb21cIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJ1aVZhci5mb3JtLmdldCgnbmFtZScpLmludmFsaWRcIj5cclxuICAgICAgICAgIExlIG5vbSBlc3Qgb2JsaWdhdG9pcmUgZXQgZG9pdCDDqnRyZSBjb21wb3PDqSBkZSBtb2lucyBkZSAzMCBjYXJhY3TDqHJlc1xyXG4gICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG5cclxuICAgICAgPG1hdC1mb3JtLWZpZWxkPlxyXG4gICAgICAgIDxpbnB1dCBtYXRJbnB1dCBcclxuICAgICAgICAgIHR5cGU9XCJ1cmxcIlxyXG4gICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiYmFja1VybFwiXHJcbiAgICAgICAgICBwbGFjZWhvbGRlcj1cIlVybCBkdSBiYWNrZW5kXCIgdmFsdWU9XCJcIj5cclxuICAgICAgICA8bWF0LWVycm9yICpuZ0lmPVwidWlWYXIuZm9ybS5nZXQoJ2JhY2tVcmwnKS5pbnZhbGlkXCI+XHJcbiAgICAgICAgICBMJ3VybCBkb2l0IMOqdHJlIHZhbGlkZVxyXG4gICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG5cclxuICAgICAgPG1hdC1mb3JtLWZpZWxkPlxyXG4gICAgICAgIDxpbnB1dCBtYXRJbnB1dCBcclxuICAgICAgICAgIHR5cGU9XCJ1cmxcIlxyXG4gICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiZnJvbnRVcmxcIlxyXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJVcmwgZHUgZnJvbnRlbmRcIiB2YWx1ZT1cIlwiPlxyXG4gICAgICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJ1aVZhci5mb3JtLmdldCgnZnJvbnRVcmwnKS5pbnZhbGlkXCI+XHJcbiAgICAgICAgICBMJ3VybCBkb2l0IMOqdHJlIHZhbGlkZVxyXG4gICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICA8L21hdC1mb3JtLWZpZWxkPlxyXG4gICAgICBcclxuICAgICAgPG1hdC1jaGVja2JveFxyXG4gICAgICAgIGZvcm1Db250cm9sTmFtZT1cImF2YWlsYWJsZUZvclVzZXJzXCJcclxuICAgICAgICA+QWNjZXNzaWJsZSBhdXggdXRpbGlzYXRldXJzID88L21hdC1jaGVja2JveD5cclxuICAgICAgPCEtLSA8bWF0LWZvcm0tZmllbGQ+XHJcbiAgICAgICAgPHRleHRhcmVhIG1hdElucHV0IFxyXG4gICAgICAgICAgZm9ybUNvbnRyb2xOYW1lPVwiZGVzY3JpcHRpb25cIlxyXG4gICAgICAgICAgcm93cz1cIjNcIlxyXG4gICAgICAgICAgcGxhY2Vob2xkZXI9XCJEZXNjcmlwdGlvblwiPjwvdGV4dGFyZWE+XHJcbiAgICAgICAgPG1hdC1oaW50PkTDqWNyaXZleiBzdWNjaW50ZW1lbnQgbGUgcsO0bGU8L21hdC1oaW50PlxyXG4gICAgICAgIDxtYXQtaGludCBhbGlnbj1cImVuZFwiPnt7dWlWYXIuZm9ybS5nZXQoJ2Rlc2NyaXB0aW9uJykudmFsdWU/Lmxlbmd0aH19IC9cclxuICAgICAgICAgIDIwMDwvbWF0LWhpbnQ+XHJcbiAgICAgICAgPG1hdC1lcnJvciAqbmdJZj1cInVpVmFyLmZvcm0uZ2V0KCdkZXNjcmlwdGlvbicpLmludmFsaWRcIj5cclxuICAgICAgICAgIExhIGRlc2NyaXB0aW9uIG5lIHBldXQgcGFzIGV4Y8OpZGVyIDIwMCBjYXJhY3TDqHJlc1xyXG4gICAgICAgIDwvbWF0LWVycm9yPlxyXG4gICAgICA8L21hdC1mb3JtLWZpZWxkPiAtLT5cclxuICAgIDwvZm9ybT5cclxuICA8L2Rpdj5cclxuICA8ZGl2ICpuZ0lmPVwidWlWYXIubG9hZGluZ1wiIGNsYXNzPVwiZm9mLWxvYWRpbmdcIj5cclxuICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5DaGFyZ2VtZW50cyBkZXMgdVNlcnZpY2VzIGV0IGRlcyBhdXRob3Jpc2F0aW9ucy4uLjwvc3Bhbj5cclxuICA8L2Rpdj4gXHJcbjwvZGl2PlxyXG5cclxuPCEtLSA8Zm9mLWVudGl0eS1mb290ZXJcclxuICBbZW50aXR5QmFzZV09XCJ1aVZhci51U2VydmljZVwiPlxyXG48L2ZvZi1lbnRpdHktZm9vdGVyPiAtLT4iXX0=
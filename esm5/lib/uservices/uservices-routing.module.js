import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FofAuthGuard } from '../core/auth.guard';
import { UservicesComponent } from './uservices/uservices.component';
import { UserviceComponent } from './uservice/uservice.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
var routes = [
    { path: '', canActivate: [FofAuthGuard],
        children: [
            { path: '',
                children: [
                    { path: '', component: UservicesComponent },
                    { path: ':id', component: UserviceComponent }
                ]
            },
        ]
    }
];
var UservicesRoutingModule = /** @class */ (function () {
    function UservicesRoutingModule() {
    }
    UservicesRoutingModule.ɵmod = i0.ɵɵdefineNgModule({ type: UservicesRoutingModule });
    UservicesRoutingModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UservicesRoutingModule_Factory(t) { return new (t || UservicesRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
            RouterModule] });
    return UservicesRoutingModule;
}());
export { UservicesRoutingModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UservicesRoutingModule, { imports: [i1.RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLXJvdXRpbmcubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGZvZi1hbmd1bGFyL2NvcmUvIiwic291cmNlcyI6WyJsaWIvdXNlcnZpY2VzL3VzZXJ2aWNlcy1yb3V0aW5nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFBO0FBQ3hDLE9BQU8sRUFBVSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQTtBQUN0RCxPQUFPLEVBQUUsWUFBWSxFQUFDLE1BQU0sb0JBQW9CLENBQUE7QUFDaEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0saUNBQWlDLENBQUE7QUFDcEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0JBQStCLENBQUE7OztBQUVqRSxJQUFNLE1BQU0sR0FBVztJQUNyQixFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsV0FBVyxFQUFFLENBQUMsWUFBWSxDQUFDO1FBQ3JDLFFBQVEsRUFBRTtZQUNSLEVBQUUsSUFBSSxFQUFFLEVBQUU7Z0JBQ1IsUUFBUSxFQUFFO29CQUNSLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsa0JBQWtCLEVBQUU7b0JBQzNDLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUU7aUJBQzlDO2FBQ0Y7U0FDRjtLQUNGO0NBQ0YsQ0FBQTtBQUVEO0lBQUE7S0FJdUM7OERBQTFCLHNCQUFzQjsrSEFBdEIsc0JBQXNCLGtCQUh4QixDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDOUIsWUFBWTtpQ0FyQnhCO0NBdUJ1QyxBQUp2QyxJQUl1QztTQUExQixzQkFBc0I7d0ZBQXRCLHNCQUFzQiwwQ0FGdkIsWUFBWTtrREFFWCxzQkFBc0I7Y0FKbEMsUUFBUTtlQUFDO2dCQUNSLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQzthQUN4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgUm91dGVzLCBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInXHJcbmltcG9ydCB7IEZvZkF1dGhHdWFyZH0gZnJvbSAnLi4vY29yZS9hdXRoLmd1YXJkJ1xyXG5pbXBvcnQgeyBVc2VydmljZXNDb21wb25lbnQgfSBmcm9tICcuL3VzZXJ2aWNlcy91c2VydmljZXMuY29tcG9uZW50J1xyXG5pbXBvcnQgeyBVc2VydmljZUNvbXBvbmVudCB9IGZyb20gJy4vdXNlcnZpY2UvdXNlcnZpY2UuY29tcG9uZW50J1xyXG5cclxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXHJcbiAgeyBwYXRoOiAnJywgY2FuQWN0aXZhdGU6IFtGb2ZBdXRoR3VhcmRdLFxyXG4gICAgY2hpbGRyZW46IFtcclxuICAgICAgeyBwYXRoOiAnJyxcclxuICAgICAgICBjaGlsZHJlbjogW1xyXG4gICAgICAgICAgeyBwYXRoOiAnJywgY29tcG9uZW50OiBVc2VydmljZXNDb21wb25lbnQgfSwgICAgICAgICAgICBcclxuICAgICAgICAgIHsgcGF0aDogJzppZCcsIGNvbXBvbmVudDogVXNlcnZpY2VDb21wb25lbnQgfVxyXG4gICAgICAgIF1cclxuICAgICAgfSxcclxuICAgIF1cclxuICB9ICAgXHJcbl1cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1JvdXRlck1vZHVsZS5mb3JDaGlsZChyb3V0ZXMpXSxcclxuICBleHBvcnRzOiBbUm91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlcnZpY2VzUm91dGluZ01vZHVsZSB7IH1cclxuIl19
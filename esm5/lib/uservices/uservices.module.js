import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../core/material.module';
import { UservicesRoutingModule } from './uservices-routing.module';
import { UservicesComponent } from './uservices/uservices.component';
import { UserviceComponent } from './uservice/uservice.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as i0 from "@angular/core";
var UservicesModule = /** @class */ (function () {
    function UservicesModule() {
    }
    UservicesModule.ɵmod = i0.ɵɵdefineNgModule({ type: UservicesModule });
    UservicesModule.ɵinj = i0.ɵɵdefineInjector({ factory: function UservicesModule_Factory(t) { return new (t || UservicesModule)(); }, imports: [[
                CommonModule,
                MaterialModule,
                FormsModule,
                ReactiveFormsModule,
                UservicesRoutingModule
            ]] });
    return UservicesModule;
}());
export { UservicesModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(UservicesModule, { declarations: [UservicesComponent, UserviceComponent], imports: [CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        UservicesRoutingModule] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesModule, [{
        type: NgModule,
        args: [{
                declarations: [UservicesComponent, UserviceComponent],
                imports: [
                    CommonModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    UservicesRoutingModule
                ]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL3VzZXJ2aWNlcy91c2VydmljZXMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFBO0FBQzlDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQTtBQUN4RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQTtBQUNuRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQTtBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQTtBQUNqRSxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUE7O0FBR2pFO0lBQUE7S0FVZ0M7dURBQW5CLGVBQWU7aUhBQWYsZUFBZSxrQkFSakI7Z0JBQ1AsWUFBWTtnQkFDWixjQUFjO2dCQUNkLFdBQVc7Z0JBQ1gsbUJBQW1CO2dCQUNuQixzQkFBc0I7YUFDdkI7MEJBakJIO0NBbUJnQyxBQVZoQyxJQVVnQztTQUFuQixlQUFlO3dGQUFmLGVBQWUsbUJBVFgsa0JBQWtCLEVBQUUsaUJBQWlCLGFBRWxELFlBQVk7UUFDWixjQUFjO1FBQ2QsV0FBVztRQUNYLG1CQUFtQjtRQUNuQixzQkFBc0I7a0RBR2IsZUFBZTtjQVYzQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsa0JBQWtCLEVBQUUsaUJBQWlCLENBQUM7Z0JBQ3JELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGNBQWM7b0JBQ2QsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLHNCQUFzQjtpQkFDdkI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSdcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJ1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL2NvcmUvbWF0ZXJpYWwubW9kdWxlJ1xyXG5pbXBvcnQgeyBVc2VydmljZXNSb3V0aW5nTW9kdWxlIH0gZnJvbSAnLi91c2VydmljZXMtcm91dGluZy5tb2R1bGUnXHJcbmltcG9ydCB7IFVzZXJ2aWNlc0NvbXBvbmVudCB9IGZyb20gJy4vdXNlcnZpY2VzL3VzZXJ2aWNlcy5jb21wb25lbnQnXHJcbmltcG9ydCB7IFVzZXJ2aWNlQ29tcG9uZW50IH0gZnJvbSAnLi91c2VydmljZS91c2VydmljZS5jb21wb25lbnQnXHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnXHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtVc2VydmljZXNDb21wb25lbnQsIFVzZXJ2aWNlQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLCBcclxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICBVc2VydmljZXNSb3V0aW5nTW9kdWxlXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlcnZpY2VzTW9kdWxlIHsgfVxyXG4iXX0=
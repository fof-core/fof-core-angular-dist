import { Injectable, Inject } from '@angular/core';
import { CORE_CONFIG } from '../fof-config';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var UservicesService = /** @class */ (function () {
    function UservicesService(fofConfig, httpClient) {
        var _this = this;
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.uServices = {
            create: function (uService) { return _this.httpClient.post(_this.environment.apiPath + "/uservices", uService); },
            update: function (uService) { return _this.httpClient.patch(_this.environment.apiPath + "/uservices/" + uService.id, uService); },
            delete: function (uService) { return _this.httpClient.delete(_this.environment.apiPath + "/uservices/" + uService.id); },
            getOne: function (id) { return _this.httpClient.get(_this.environment.apiPath + "/uservices/" + id); },
            getAll: function () { return _this.httpClient.get(_this.environment.apiPath + "/uservices"); }
        };
        this.environment = this.fofConfig.environment;
    }
    UservicesService.ɵfac = function UservicesService_Factory(t) { return new (t || UservicesService)(i0.ɵɵinject(CORE_CONFIG), i0.ɵɵinject(i1.HttpClient)); };
    UservicesService.ɵprov = i0.ɵɵdefineInjectable({ token: UservicesService, factory: UservicesService.ɵfac, providedIn: 'root' });
    return UservicesService;
}());
export { UservicesService };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: i1.HttpClient }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AZm9mLWFuZ3VsYXIvY29yZS8iLCJzb3VyY2VzIjpbImxpYi91c2VydmljZXMvdXNlcnZpY2VzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUE7QUFDbEQsT0FBTyxFQUFjLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQTs7O0FBWXZEO0lBS0UsMEJBQytCLFNBQXFCLEVBQzFDLFVBQXNCO1FBRmhDLGlCQUtDO1FBSjhCLGNBQVMsR0FBVCxTQUFTLENBQVk7UUFDMUMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQU96QixjQUFTLEdBQUc7WUFDakIsTUFBTSxFQUFDLFVBQUMsUUFBbUIsSUFBSyxPQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFlLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxlQUFZLEVBQUUsUUFBUSxDQUFDLEVBQWxGLENBQWtGO1lBQ2xILE1BQU0sRUFBQyxVQUFDLFFBQW1CLElBQUssT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBZSxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sbUJBQWMsUUFBUSxDQUFDLEVBQUksRUFBRSxRQUFRLENBQUMsRUFBbEcsQ0FBa0c7WUFDbEksTUFBTSxFQUFDLFVBQUMsUUFBbUIsSUFBSyxPQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFlLEtBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxtQkFBYyxRQUFRLENBQUMsRUFBSSxDQUFDLEVBQXpGLENBQXlGO1lBQ3pILE1BQU0sRUFBRSxVQUFDLEVBQVUsSUFBNEIsT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBZSxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sbUJBQWMsRUFBSSxDQUFDLEVBQTdFLENBQTZFO1lBQzVILE1BQU0sRUFBRSxjQUErQixPQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFzQixLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sZUFBWSxDQUFDLEVBQTlFLENBQThFO1NBQ3RILENBQUE7UUFYQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFBO0lBQy9DLENBQUM7b0ZBUFUsZ0JBQWdCLGNBR2pCLFdBQVc7NERBSFYsZ0JBQWdCLFdBQWhCLGdCQUFnQixtQkFGZixNQUFNOzJCQWRwQjtDQWtDQyxBQXJCRCxJQXFCQztTQWxCWSxnQkFBZ0I7a0RBQWhCLGdCQUFnQjtjQUg1QixVQUFVO2VBQUM7Z0JBQ1YsVUFBVSxFQUFFLE1BQU07YUFDbkI7O3NCQUlJLE1BQU07dUJBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IElmb2ZDb25maWcsIENPUkVfQ09ORklHIH0gZnJvbSAnLi4vZm9mLWNvbmZpZydcclxuLy8gaW1wb3J0IHsgZUNwIH0gZnJvbSAnLi4vc2hhcmVkL3Blcm1pc3Npb24uZW51bSdcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJ1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IsIHNoYXJlIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcydcclxuLy8gaW1wb3J0IHsgRm9GQXV0aFNlcnZpY2UgfSBmcm9tICcuLi9jb3JlL2F1dGguc2VydmljZSdcclxuaW1wb3J0IHsgRm9GQ29yZVNlcnZpY2UgfSBmcm9tICcuLi9jb3JlL2NvcmUuc2VydmljZSdcclxuLy8gaW1wb3J0IHsgaVVzZXIgfSBmcm9tICcuLi9zaGFyZWQvdXNlci5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IFJlcXVlc3RRdWVyeUJ1aWxkZXIsIENvbmRPcGVyYXRvciB9IGZyb20gJ0BuZXN0anN4L2NydWQtcmVxdWVzdCdcclxuaW1wb3J0IHsgaWZvZlNlYXJjaCB9IGZyb20gJy4uL2NvcmUvY29yZS5pbnRlcmZhY2UnXHJcbmltcG9ydCB7IGlVc2VydmljZSB9IGZyb20gJy4uL3Blcm1pc3Npb24vaW50ZXJmYWNlcy9wZXJtaXNzaW9ucy5pbnRlcmZhY2UnXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBVc2VydmljZXNTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBASW5qZWN0KENPUkVfQ09ORklHKSBwcml2YXRlIGZvZkNvbmZpZzogSWZvZkNvbmZpZyxcclxuICAgIHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudFxyXG4gICkgeyBcclxuICAgIHRoaXMuZW52aXJvbm1lbnQgPSB0aGlzLmZvZkNvbmZpZy5lbnZpcm9ubWVudFxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBlbnZpcm9ubWVudDphbnlcclxuXHJcbiAgcHVibGljIHVTZXJ2aWNlcyA9IHtcclxuICAgIGNyZWF0ZToodVNlcnZpY2U6IGlVc2VydmljZSkgPT4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8aVVzZXJ2aWNlPihgJHt0aGlzLmVudmlyb25tZW50LmFwaVBhdGh9L3VzZXJ2aWNlc2AsIHVTZXJ2aWNlKSxcclxuICAgIHVwZGF0ZToodVNlcnZpY2U6IGlVc2VydmljZSkgPT4gdGhpcy5odHRwQ2xpZW50LnBhdGNoPGlVc2VydmljZT4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS91c2VydmljZXMvJHt1U2VydmljZS5pZH1gLCB1U2VydmljZSksXHJcbiAgICBkZWxldGU6KHVTZXJ2aWNlOiBpVXNlcnZpY2UpID0+IHRoaXMuaHR0cENsaWVudC5kZWxldGU8aVVzZXJ2aWNlPihgJHt0aGlzLmVudmlyb25tZW50LmFwaVBhdGh9L3VzZXJ2aWNlcy8ke3VTZXJ2aWNlLmlkfWApLFxyXG4gICAgZ2V0T25lOiAoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8aVVzZXJ2aWNlPiA9PiB0aGlzLmh0dHBDbGllbnQuZ2V0PGlVc2VydmljZT4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS91c2VydmljZXMvJHtpZH1gKSwgICBcclxuICAgIGdldEFsbDogKCk6IE9ic2VydmFibGU8aVVzZXJ2aWNlW10+ID0+IHRoaXMuaHR0cENsaWVudC5nZXQ8QXJyYXk8aVVzZXJ2aWNlPj4oYCR7dGhpcy5lbnZpcm9ubWVudC5hcGlQYXRofS91c2VydmljZXNgKSAgIFxyXG4gIH1cclxufVxyXG4iXX0=
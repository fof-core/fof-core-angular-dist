import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Breakpoints } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
import * as i1 from "../uservices.service";
import * as i2 from "../../core/notification/notification.service";
import * as i3 from "@angular/cdk/layout";
import * as i4 from "@angular/material/card";
import * as i5 from "@angular/material/button";
import * as i6 from "@angular/router";
import * as i7 from "@angular/common";
import * as i8 from "@angular/material/table";
import * as i9 from "@angular/material/sort";
import * as i10 from "@angular/material/paginator";
import * as i11 from "@angular/material/progress-spinner";
function UservicesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 15);
    i0.ɵɵelement(1, "mat-spinner", 16);
    i0.ɵɵelementStart(2, "span");
    i0.ɵɵtext(3, "Chargements des \u03BCServices...");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function UservicesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Nom technique");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r500 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r500.technicalName);
} }
function UservicesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Nom");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r501 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r501.name);
} }
function UservicesComponent_th_16_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Url interface");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r502 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r502.frontUrl);
} }
function UservicesComponent_th_19_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Url backend");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_20_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r503 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r503.backUrl);
} }
function UservicesComponent_th_22_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "th", 17);
    i0.ɵɵtext(1, "Visible pour les utilisateurs");
    i0.ɵɵelementEnd();
} }
function UservicesComponent_td_23_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "td", 18);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var row_r504 = ctx.$implicit;
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(row_r504.availableForUsers);
} }
function UservicesComponent_tr_24_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 19);
} }
function UservicesComponent_tr_25_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "tr", 20);
} if (rf & 2) {
    var row_r505 = ctx.$implicit;
    i0.ɵɵproperty("routerLink", row_r505.id);
} }
var _c0 = function () { return [100]; };
var UservicesComponent = /** @class */ (function () {
    function UservicesComponent(uservicesService, fofNotificationService, breakpointObserver) {
        this.uservicesService = uservicesService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['technicalName', 'name', 'urlFront', 'urlBack', 'availableForUsers'],
            data: [],
            resultsLength: 0,
            pageSize: 100,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    UservicesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['technicalName', 'name'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['technicalName', 'name', 'frontUrl', 'backUrl', 'availableForUsers'];
            }
        });
    };
    UservicesComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.uservicesService.uServices.getAll();
        }), map(function (uServices) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = uServices.length;
            return uServices;
        }), catchError(function () {
            _this.uiVar.isLoadingResults = false;
            return observableOf([]);
        })).subscribe(function (data) { return _this.uiVar.data = data; });
    };
    UservicesComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    UservicesComponent.ɵfac = function UservicesComponent_Factory(t) { return new (t || UservicesComponent)(i0.ɵɵdirectiveInject(i1.UservicesService), i0.ɵɵdirectiveInject(i2.FofNotificationService), i0.ɵɵdirectiveInject(i3.BreakpointObserver)); };
    UservicesComponent.ɵcmp = i0.ɵɵdefineComponent({ type: UservicesComponent, selectors: [["fof-core-uservices"]], viewQuery: function UservicesComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(MatPaginator, true);
            i0.ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.paginator = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 27, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "technicalName", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "technicalName"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "name"], ["matColumnDef", "frontUrl"], ["matColumnDef", "backUrl"], ["matColumnDef", "availableForUsers"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UservicesComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "mat-card", 0);
            i0.ɵɵelementStart(1, "h3");
            i0.ɵɵtext(2, "Mini-services");
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(3, "a", 1);
            i0.ɵɵelementStart(4, "span");
            i0.ɵɵtext(5, "Ajouter un mini-service");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 2);
            i0.ɵɵtemplate(7, UservicesComponent_div_7_Template, 4, 0, "div", 3);
            i0.ɵɵelementStart(8, "table", 4);
            i0.ɵɵelementContainerStart(9, 5);
            i0.ɵɵtemplate(10, UservicesComponent_th_10_Template, 2, 0, "th", 6);
            i0.ɵɵtemplate(11, UservicesComponent_td_11_Template, 2, 1, "td", 7);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(12, 8);
            i0.ɵɵtemplate(13, UservicesComponent_th_13_Template, 2, 0, "th", 6);
            i0.ɵɵtemplate(14, UservicesComponent_td_14_Template, 2, 1, "td", 7);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(15, 9);
            i0.ɵɵtemplate(16, UservicesComponent_th_16_Template, 2, 0, "th", 6);
            i0.ɵɵtemplate(17, UservicesComponent_td_17_Template, 2, 1, "td", 7);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(18, 10);
            i0.ɵɵtemplate(19, UservicesComponent_th_19_Template, 2, 0, "th", 6);
            i0.ɵɵtemplate(20, UservicesComponent_td_20_Template, 2, 1, "td", 7);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵelementContainerStart(21, 11);
            i0.ɵɵtemplate(22, UservicesComponent_th_22_Template, 2, 0, "th", 6);
            i0.ɵɵtemplate(23, UservicesComponent_td_23_Template, 2, 1, "td", 7);
            i0.ɵɵelementContainerEnd();
            i0.ɵɵtemplate(24, UservicesComponent_tr_24_Template, 1, 0, "tr", 12);
            i0.ɵɵtemplate(25, UservicesComponent_tr_25_Template, 1, 1, "tr", 13);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(26, "mat-paginator", 14);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("routerLink", "new");
            i0.ɵɵadvance(4);
            i0.ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("dataSource", ctx.uiVar.data);
            i0.ɵɵadvance(16);
            i0.ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", i0.ɵɵpureFunction0(8, _c0))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [i4.MatCard, i5.MatAnchor, i6.RouterLinkWithHref, i7.NgIf, i8.MatTable, i9.MatSort, i8.MatColumnDef, i8.MatHeaderCellDef, i8.MatCellDef, i8.MatHeaderRowDef, i8.MatRowDef, i10.MatPaginator, i11.MatSpinner, i8.MatHeaderCell, i8.MatCell, i8.MatHeaderRow, i8.MatRow, i6.RouterLink], styles: [""] });
    return UservicesComponent;
}());
export { UservicesComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(UservicesComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-uservices',
                templateUrl: './uservices.component.html',
                styleUrls: ['./uservices.component.scss']
            }]
    }], function () { return [{ type: i1.UservicesService }, { type: i2.FofNotificationService }, { type: i3.BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlcnZpY2VzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bmb2YtYW5ndWxhci9jb3JlLyIsInNvdXJjZXMiOlsibGliL3VzZXJ2aWNlcy91c2VydmljZXMvdXNlcnZpY2VzLmNvbXBvbmVudC50cyIsImxpYi91c2VydmljZXMvdXNlcnZpY2VzL3VzZXJ2aWNlcy5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFtQyxTQUFTLEVBQTRCLE1BQU0sZUFBZSxDQUFBO0FBSS9HLE9BQU8sRUFBRSxZQUFZLEVBQW9CLE1BQU0sNkJBQTZCLENBQUE7QUFDNUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLHdCQUF3QixDQUFBO0FBQ2hELE9BQU8sRUFBRSxLQUFLLEVBQWMsRUFBRSxJQUFJLFlBQVksRUFBZ0IsTUFBTSxNQUFNLENBQUE7QUFDMUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFBO0FBRXRFLE9BQU8sRUFBc0IsV0FBVyxFQUFtQixNQUFNLHFCQUFxQixDQUFBOzs7Ozs7Ozs7Ozs7OztJQ0NwRiwrQkFDRTtJQUFBLGtDQUF1QztJQUFDLDRCQUFNO0lBQUEsaURBQTRCO0lBQUEsaUJBQU87SUFDbkYsaUJBQU07OztJQU1GLDhCQUFzQztJQUFBLDZCQUFhO0lBQUEsaUJBQUs7OztJQUN4RCw4QkFBbUM7SUFBQSxZQUFxQjtJQUFBLGlCQUFLOzs7SUFBMUIsZUFBcUI7SUFBckIsNENBQXFCOzs7SUFJeEQsOEJBQXNDO0lBQUEsbUJBQUc7SUFBQSxpQkFBSzs7O0lBQzlDLDhCQUFtQztJQUFBLFlBQVk7SUFBQSxpQkFBSzs7O0lBQWpCLGVBQVk7SUFBWixtQ0FBWTs7O0lBSS9DLDhCQUFzQztJQUFBLDZCQUFhO0lBQUEsaUJBQUs7OztJQUN4RCw4QkFBbUM7SUFBQSxZQUFnQjtJQUFBLGlCQUFLOzs7SUFBckIsZUFBZ0I7SUFBaEIsdUNBQWdCOzs7SUFJbkQsOEJBQXNDO0lBQUEsMkJBQVc7SUFBQSxpQkFBSzs7O0lBQ3RELDhCQUFtQztJQUFBLFlBQWU7SUFBQSxpQkFBSzs7O0lBQXBCLGVBQWU7SUFBZixzQ0FBZTs7O0lBSWxELDhCQUFzQztJQUFBLDZDQUE2QjtJQUFBLGlCQUFLOzs7SUFDeEUsOEJBQW1DO0lBQUEsWUFBeUI7SUFBQSxpQkFBSzs7O0lBQTlCLGVBQXlCO0lBQXpCLGdEQUF5Qjs7O0lBRzlELHlCQUFrRTs7O0lBQ2xFLHlCQUM4RDs7O0lBRHpCLHdDQUFxQjs7O0FEaEM5RDtJQVNFLDRCQUNVLGdCQUFrQyxFQUNsQyxzQkFBOEMsRUFDOUMsa0JBQXNDO1FBRnRDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBS2hELHdCQUF3QjtRQUNoQixXQUFNLEdBQUc7WUFDZixxQkFBcUIsRUFBZ0IsU0FBUztTQUMvQyxDQUFBO1FBQ0Qsd0JBQXdCO1FBQ2hCLGFBQVEsR0FBRyxFQUNsQixDQUFBO1FBQ0QsZ0NBQWdDO1FBQ3pCLFVBQUssR0FBRztZQUNiLGdCQUFnQixFQUFZLENBQUMsZUFBZSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLG1CQUFtQixDQUFDO1lBQ2pHLElBQUksRUFBZSxFQUFFO1lBQ3JCLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLFFBQVEsRUFBRSxHQUFHO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtTQUN2QixDQUFBO1FBQ0QsOEJBQThCO1FBQ3ZCLGFBQVEsR0FBRyxFQUNqQixDQUFBO0lBbkJELENBQUM7SUFvQkQsaUJBQWlCO0lBQ2pCLHFDQUFRLEdBQVI7UUFBQSxpQkFXQztRQVZDLElBQUksQ0FBQyxNQUFNLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO2FBQ3RGLFNBQVMsQ0FBQyxVQUFDLEtBQXNCO1lBQ2hDLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtnQkFDakIsU0FBUztnQkFDVCxLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxDQUFBO2FBQ3hEO2lCQUFNO2dCQUNMLFdBQVc7Z0JBQ1gsS0FBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLGVBQWUsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO2FBQ3BHO1FBQ0gsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsNENBQWUsR0FBZjtRQUFBLGlCQXNCQztRQXJCQyxvRUFBb0U7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxDQUFDLEVBQTVCLENBQTRCLENBQUMsQ0FBQTtRQUVsRSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7YUFDN0MsSUFBSSxDQUNILFNBQVMsQ0FBQyxFQUFFLENBQUMsRUFDYixTQUFTLENBQUM7WUFDUixLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQTtZQUNsQyxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQTtZQUM3QyxPQUFPLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUE7UUFDakQsQ0FBQyxDQUFDLEVBQ0YsR0FBRyxDQUFDLFVBQUMsU0FBc0I7WUFDekIsS0FBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUE7WUFDbkMsS0FBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQTtZQUMzQyxPQUFPLFNBQVMsQ0FBQTtRQUNsQixDQUFDLENBQUMsRUFDRixVQUFVLENBQUM7WUFDVCxLQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQTtZQUNuQyxPQUFPLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQTtRQUN6QixDQUFDLENBQUMsQ0FDSCxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksRUFBdEIsQ0FBc0IsQ0FBQyxDQUFBO0lBQy9DLENBQUM7SUFFRCx3Q0FBVyxHQUFYO1FBQ0UsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLHFCQUFxQixFQUFFO1lBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQTtTQUFFO0lBQzVGLENBQUM7d0ZBdEVVLGtCQUFrQjsyREFBbEIsa0JBQWtCOzJCQUNsQixZQUFZOzJCQUNaLE9BQU87Ozs7OztZQ2xCcEIsbUNBQ0U7WUFBQSwwQkFBSTtZQUFBLDZCQUFhO1lBQUEsaUJBQUs7WUFDdEIsNEJBRUU7WUFBQSw0QkFBTTtZQUFBLHVDQUF1QjtZQUFBLGlCQUFPO1lBQ3RDLGlCQUFJO1lBQ04saUJBQVc7WUFFWCw4QkFFRTtZQUFBLG1FQUNFO1lBR0YsZ0NBR0U7WUFBQSxnQ0FDRTtZQUFBLG1FQUFzQztZQUN0QyxtRUFBbUM7WUFDckMsMEJBQWU7WUFFZixpQ0FDRTtZQUFBLG1FQUFzQztZQUN0QyxtRUFBbUM7WUFDckMsMEJBQWU7WUFFZixpQ0FDRTtZQUFBLG1FQUFzQztZQUN0QyxtRUFBbUM7WUFDckMsMEJBQWU7WUFFZixrQ0FDRTtZQUFBLG1FQUFzQztZQUN0QyxtRUFBbUM7WUFDckMsMEJBQWU7WUFFZixrQ0FDRTtZQUFBLG1FQUFzQztZQUN0QyxtRUFBbUM7WUFDckMsMEJBQWU7WUFFZixvRUFBNkQ7WUFDN0Qsb0VBQ3lEO1lBQzNELGlCQUFRO1lBRVIscUNBRThDO1lBRWhELGlCQUFNOztZQWhERixlQUFvQjtZQUFwQixrQ0FBb0I7WUFPdUIsZUFBOEI7WUFBOUIsaURBQThCO1lBSTFELGVBQXlCO1lBQXpCLDJDQUF5QjtZQTRCckIsZ0JBQXlDO1lBQXpDLDREQUF5QztZQUUxRCxlQUFzRDtZQUF0RCw2REFBc0Q7WUFHM0MsZUFBOEI7WUFBOUIsZ0RBQThCLCtDQUFBLGdDQUFBOzs2QkQvQy9DO0NBd0ZDLEFBN0VELElBNkVDO1NBeEVZLGtCQUFrQjtrREFBbEIsa0JBQWtCO2NBTDlCLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixXQUFXLEVBQUUsNEJBQTRCO2dCQUN6QyxTQUFTLEVBQUUsQ0FBQyw0QkFBNEIsQ0FBQzthQUMxQzs7a0JBRUUsU0FBUzttQkFBQyxZQUFZOztrQkFDdEIsU0FBUzttQkFBQyxPQUFPIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnXHJcbmltcG9ydCB7IFVzZXJ2aWNlc1NlcnZpY2UgfSBmcm9tICcuLi91c2VydmljZXMuc2VydmljZSdcclxuaW1wb3J0IHsgRm9mTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL2NvcmUvbm90aWZpY2F0aW9uL25vdGlmaWNhdGlvbi5zZXJ2aWNlJ1xyXG5pbXBvcnQgeyBpVXNlcnZpY2UgfSBmcm9tICcuLi8uLi9wZXJtaXNzaW9uL2ludGVyZmFjZXMvcGVybWlzc2lvbnMuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFBhZ2luYXRvckludGwgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9wYWdpbmF0b3InXHJcbmltcG9ydCB7IE1hdFNvcnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zb3J0J1xyXG5pbXBvcnQgeyBtZXJnZSwgT2JzZXJ2YWJsZSwgb2YgYXMgb2JzZXJ2YWJsZU9mLCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJ1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHN0YXJ0V2l0aCwgc3dpdGNoTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnXHJcbmltcG9ydCB7IGlmb2ZTZWFyY2ggfSBmcm9tICcuLi8uLi9jb3JlL2NvcmUuaW50ZXJmYWNlJ1xyXG5pbXBvcnQgeyBCcmVha3BvaW50T2JzZXJ2ZXIsIEJyZWFrcG9pbnRzLCBCcmVha3BvaW50U3RhdGUgfSBmcm9tICdAYW5ndWxhci9jZGsvbGF5b3V0J1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdmb2YtY29yZS11c2VydmljZXMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi91c2VydmljZXMuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3VzZXJ2aWNlcy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBVc2VydmljZXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yKSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvclxyXG4gIEBWaWV3Q2hpbGQoTWF0U29ydCkgc29ydDogTWF0U29ydFxyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgdXNlcnZpY2VzU2VydmljZTogVXNlcnZpY2VzU2VydmljZSxcclxuICAgIHByaXZhdGUgZm9mTm90aWZpY2F0aW9uU2VydmljZTogRm9mTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgYnJlYWtwb2ludE9ic2VydmVyOiBCcmVha3BvaW50T2JzZXJ2ZXJcclxuICApIHsgXHJcbiAgICBcclxuICB9XHJcblxyXG4gIC8vIEFsbCBwcml2YXRlIHZhcmlhYmxlc1xyXG4gIHByaXZhdGUgcHJpVmFyID0ge1xyXG4gICAgYnJlYWtwb2ludE9ic2VydmVyU3ViOiA8U3Vic2NyaXB0aW9uPnVuZGVmaW5lZCxcclxuICB9XHJcbiAgLy8gQWxsIHByaXZhdGUgZnVuY3Rpb25zXHJcbiAgcHJpdmF0ZSBwcml2RnVuYyA9IHtcclxuICB9XHJcbiAgLy8gQWxsIHZhcmlhYmxlcyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlWYXIgPSB7ICAgIFxyXG4gICAgZGlzcGxheWVkQ29sdW1uczogPHN0cmluZ1tdPlsndGVjaG5pY2FsTmFtZScsICduYW1lJywgJ3VybEZyb250JywgJ3VybEJhY2snLCAnYXZhaWxhYmxlRm9yVXNlcnMnXSxcclxuICAgIGRhdGE6IDxpVXNlcnZpY2VbXT5bXSxcclxuICAgIHJlc3VsdHNMZW5ndGg6IDAsXHJcbiAgICBwYWdlU2l6ZTogMTAwLFxyXG4gICAgaXNMb2FkaW5nUmVzdWx0czogdHJ1ZVxyXG4gIH1cclxuICAvLyBBbGwgYWN0aW9ucyBzaGFyZWQgd2l0aCBVSSBcclxuICBwdWJsaWMgdWlBY3Rpb24gPSB7XHJcbiAgfVxyXG4gIC8vIEFuZ3VsYXIgZXZlbnRzXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnByaVZhci5icmVha3BvaW50T2JzZXJ2ZXJTdWIgPSB0aGlzLmJyZWFrcG9pbnRPYnNlcnZlci5vYnNlcnZlKEJyZWFrcG9pbnRzLlhTbWFsbClcclxuICAgIC5zdWJzY3JpYmUoKHN0YXRlOiBCcmVha3BvaW50U3RhdGUpID0+IHsgICAgICBcclxuICAgICAgaWYgKHN0YXRlLm1hdGNoZXMpIHtcclxuICAgICAgICAvLyBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ3RlY2huaWNhbE5hbWUnLCAnbmFtZSddXHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gPiBYU21hbGxcclxuICAgICAgICB0aGlzLnVpVmFyLmRpc3BsYXllZENvbHVtbnMgPSBbJ3RlY2huaWNhbE5hbWUnLCAnbmFtZScsICdmcm9udFVybCcsICdiYWNrVXJsJywgJ2F2YWlsYWJsZUZvclVzZXJzJ11cclxuICAgICAgfVxyXG4gICAgfSlcclxuICB9ICBcclxuXHJcbiAgbmdBZnRlclZpZXdJbml0KCkgeyAgICBcclxuICAgIC8vIElmIHRoZSB1c2VyIGNoYW5nZXMgdGhlIHNvcnQgb3JkZXIsIHJlc2V0IGJhY2sgdG8gdGhlIGZpcnN0IHBhZ2UuXHJcbiAgICB0aGlzLnNvcnQuc29ydENoYW5nZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5wYWdpbmF0b3IucGFnZUluZGV4ID0gMClcclxuXHJcbiAgICBtZXJnZSh0aGlzLnNvcnQuc29ydENoYW5nZSwgdGhpcy5wYWdpbmF0b3IucGFnZSlcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgc3RhcnRXaXRoKHt9KSxcclxuICAgICAgICBzd2l0Y2hNYXAoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gdHJ1ZSAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMudWlWYXIucGFnZVNpemUgPSB0aGlzLnBhZ2luYXRvci5wYWdlU2l6ZVxyXG4gICAgICAgICAgcmV0dXJuIHRoaXMudXNlcnZpY2VzU2VydmljZS51U2VydmljZXMuZ2V0QWxsKClcclxuICAgICAgICB9KSxcclxuICAgICAgICBtYXAoKHVTZXJ2aWNlczogaVVzZXJ2aWNlW10pID0+IHsgICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy51aVZhci5pc0xvYWRpbmdSZXN1bHRzID0gZmFsc2VcclxuICAgICAgICAgIHRoaXMudWlWYXIucmVzdWx0c0xlbmd0aCA9IHVTZXJ2aWNlcy5sZW5ndGggICAgICAgICAgXHJcbiAgICAgICAgICByZXR1cm4gdVNlcnZpY2VzXHJcbiAgICAgICAgfSksXHJcbiAgICAgICAgY2F0Y2hFcnJvcigoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnVpVmFyLmlzTG9hZGluZ1Jlc3VsdHMgPSBmYWxzZSAgICAgICAgICBcclxuICAgICAgICAgIHJldHVybiBvYnNlcnZhYmxlT2YoW10pXHJcbiAgICAgICAgfSlcclxuICAgICAgKS5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLnVpVmFyLmRhdGEgPSBkYXRhKVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICBpZiAodGhpcy5wcmlWYXIuYnJlYWtwb2ludE9ic2VydmVyU3ViKSB7IHRoaXMucHJpVmFyLmJyZWFrcG9pbnRPYnNlcnZlclN1Yi51bnN1YnNjcmliZSgpIH1cclxuICB9XHJcblxyXG59XHJcbiIsIjxtYXQtY2FyZCBjbGFzcz1cImZvZi1oZWFkZXJcIj4gICAgICAgIFxyXG4gIDxoMz5NaW5pLXNlcnZpY2VzPC9oMz4gICBcclxuICA8YSBtYXQtc3Ryb2tlZC1idXR0b24gY29sb3I9XCJhY2NlbnRcIiBcclxuICAgIFtyb3V0ZXJMaW5rXT1cIiduZXcnXCI+ICAgIFxyXG4gICAgPHNwYW4+QWpvdXRlciB1biBtaW5pLXNlcnZpY2U8L3NwYW4+XHJcbiAgPC9hPiAgICBcclxuPC9tYXQtY2FyZD5cclxuXHJcbjxkaXYgY2xhc3M9XCJmb2YtdGFibGUtY29udGFpbmVyIG1hdC1lbGV2YXRpb24tejJcIj5cclxuXHJcbiAgPGRpdiBjbGFzcz1cInRhYmxlLWxvYWRpbmctc2hhZGUgZm9mLWxvYWRpbmdcIiAqbmdJZj1cInVpVmFyLmlzTG9hZGluZ1Jlc3VsdHNcIj5cclxuICAgIDxtYXQtc3Bpbm5lciBkaWFtZXRlcj0yMD48L21hdC1zcGlubmVyPiA8c3Bhbj5DaGFyZ2VtZW50cyBkZXMgzrxTZXJ2aWNlcy4uLjwvc3Bhbj5cclxuICA8L2Rpdj5cclxuXHJcbiAgPHRhYmxlIG1hdC10YWJsZSBbZGF0YVNvdXJjZV09XCJ1aVZhci5kYXRhXCIgY2xhc3M9XCJkYXRhLXRhYmxlXCJcclxuICAgICAgICBtYXRTb3J0IG1hdFNvcnRBY3RpdmU9XCJ0ZWNobmljYWxOYW1lXCIgbWF0U29ydERpc2FibGVDbGVhciBtYXRTb3J0RGlyZWN0aW9uPVwiYXNjXCI+XHJcbiAgICBcclxuICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwidGVjaG5pY2FsTmFtZVwiPlxyXG4gICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPk5vbSB0ZWNobmlxdWU8L3RoPlxyXG4gICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cudGVjaG5pY2FsTmFtZX19PC90ZD5cclxuICAgIDwvbmctY29udGFpbmVyPlxyXG5cclxuICAgIDxuZy1jb250YWluZXIgbWF0Q29sdW1uRGVmPVwibmFtZVwiPlxyXG4gICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPk5vbTwvdGg+XHJcbiAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIj57e3Jvdy5uYW1lfX08L3RkPlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJmcm9udFVybFwiPlxyXG4gICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPlVybCBpbnRlcmZhY2U8L3RoPlxyXG4gICAgICA8dGQgbWF0LWNlbGwgKm1hdENlbGxEZWY9XCJsZXQgcm93XCI+e3tyb3cuZnJvbnRVcmx9fTwvdGQ+XHJcbiAgICA8L25nLWNvbnRhaW5lcj5cclxuXHJcbiAgICA8bmctY29udGFpbmVyIG1hdENvbHVtbkRlZj1cImJhY2tVcmxcIj5cclxuICAgICAgPHRoIG1hdC1oZWFkZXItY2VsbCAqbWF0SGVhZGVyQ2VsbERlZj5VcmwgYmFja2VuZDwvdGg+XHJcbiAgICAgIDx0ZCBtYXQtY2VsbCAqbWF0Q2VsbERlZj1cImxldCByb3dcIj57e3Jvdy5iYWNrVXJsfX08L3RkPlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgPG5nLWNvbnRhaW5lciBtYXRDb2x1bW5EZWY9XCJhdmFpbGFibGVGb3JVc2Vyc1wiPlxyXG4gICAgICA8dGggbWF0LWhlYWRlci1jZWxsICptYXRIZWFkZXJDZWxsRGVmPlZpc2libGUgcG91ciBsZXMgdXRpbGlzYXRldXJzPC90aD5cclxuICAgICAgPHRkIG1hdC1jZWxsICptYXRDZWxsRGVmPVwibGV0IHJvd1wiPnt7cm93LmF2YWlsYWJsZUZvclVzZXJzfX08L3RkPlxyXG4gICAgPC9uZy1jb250YWluZXI+XHJcblxyXG4gICAgPHRyIG1hdC1oZWFkZXItcm93ICptYXRIZWFkZXJSb3dEZWY9XCJ1aVZhci5kaXNwbGF5ZWRDb2x1bW5zXCI+PC90cj5cclxuICAgIDx0ciBtYXQtcm93IGNsYXNzPVwiZm9mLWVsZW1lbnQtb3ZlclwiIFtyb3V0ZXJMaW5rXT1cInJvdy5pZFwiXHJcbiAgICAgICptYXRSb3dEZWY9XCJsZXQgcm93OyBjb2x1bW5zOiB1aVZhci5kaXNwbGF5ZWRDb2x1bW5zO1wiPjwvdHI+XHJcbiAgPC90YWJsZT5cclxuXHJcbiAgPG1hdC1wYWdpbmF0b3IgW2xlbmd0aF09XCJ1aVZhci5yZXN1bHRzTGVuZ3RoXCIgXHJcbiAgICBbcGFnZVNpemVPcHRpb25zXT1cIlsxMDBdXCJcclxuICAgIFtwYWdlU2l6ZV09XCJ1aVZhci5wYWdlU2l6ZVwiPjwvbWF0LXBhZ2luYXRvcj5cclxuXHJcbjwvZGl2PlxyXG4iXX0=
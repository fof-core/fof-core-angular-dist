import { InjectionToken, ɵɵinject, ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, Inject, ɵɵgetCurrentView, ɵɵelementStart, ɵɵelement, ɵɵlistener, ɵɵrestoreView, ɵɵnextContext, ɵɵtext, ɵɵelementEnd, ɵɵadvance, ɵɵproperty, ɵɵtextInterpolate1, ɵɵelementContainer, ɵɵclassProp, EventEmitter, ɵɵdirectiveInject, ɵɵdefineComponent, ɵɵNgOnChangesFeature, ɵɵtemplate, Component, Input, Output, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule, ElementRef, ɵɵresolveDocument, ɵɵreference, ɵɵviewQuery, ɵɵqueryRefresh, ɵɵloadQuery, ViewChild, ɵɵtextInterpolate, ɵɵelementContainerStart, ɵɵelementContainerEnd, ɵɵpureFunction0, ɵɵsanitizeHtml, ViewEncapsulation, Optional, ɵɵpipe, ɵɵtextInterpolate2, ɵɵpipeBind2, ɵɵtemplateRefExtractor, ɵɵpipeBind1, ɵɵdefinePipe, Pipe, ɵɵclassMapInterpolate1, NgZone, ErrorHandler } from '@angular/core';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { HttpClient, HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BehaviorSubject, forkJoin, Subject, merge, of, throwError } from 'rxjs';
import { Router, RouterLinkWithHref, RouterLink, ActivatedRoute, RouterModule } from '@angular/router';
import { map, startWith, switchMap, catchError, debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { CommonModule, NgForOf, NgIf, DatePipe } from '@angular/common';
import { __assign, __extends } from 'tslib';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource, MatTree, MatTreeNodeDef, MatTreeNode, MatTreeNodeToggle, MatNestedTreeNode, MatTreeNodeOutlet, MatTreeModule } from '@angular/material/tree';
import { MatButton, MatButtonModule, MatAnchor } from '@angular/material/button';
import { MatCheckbox, MatCheckboxModule } from '@angular/material/checkbox';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { MatCardModule, MatCard, MatCardHeader, MatCardTitle, MatCardContent } from '@angular/material/card';
import { MatChipsModule, MatChipList, MatChipInput, MatChip, MatChipRemove } from '@angular/material/chips';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogTitle, MatDialogContent, MatDialogActions, MatDialogClose, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule, MatAccordion, MatExpansionPanel, MatExpansionPanelHeader } from '@angular/material/expansion';
import { MatInputModule, MatInput } from '@angular/material/input';
import { MatListModule, MatList } from '@angular/material/list';
import { MatPaginatorModule, MatPaginator } from '@angular/material/paginator';
import { MatProgressSpinnerModule, MatSpinner } from '@angular/material/progress-spinner';
import { MatSortModule, MatSort, MatSortHeader } from '@angular/material/sort';
import { MatTableModule, MatTable, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatHeaderCell, MatCell, MatHeaderRow, MatRow } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule, FormControl, FormGroup, Validators, FormBuilder, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, DefaultValueAccessor, RequiredValidator, NgControlStatus, FormControlName, NgModel, FormControlDirective } from '@angular/forms';
import { MatFormField, MatHint, MatError, MatLabel } from '@angular/material/form-field';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { TranslatePipe, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { trigger, transition, style, animate } from '@angular/animations';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

var eAuth;
(function (eAuth) {
    eAuth["basic"] = "basic";
    eAuth["windows"] = "windows";
})(eAuth || (eAuth = {}));
var CORE_CONFIG = new InjectionToken('CORE_CONFIG');

var FoFAuthService = /** @class */ (function () {
    function FoFAuthService(fofConfig, httpClient, router) {
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.router = router;
        this.environment = this.fofConfig.environment;
        this.currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser$ = this.currentUserSubject.asObservable();
    }
    Object.defineProperty(FoFAuthService.prototype, "currentUser", {
        get: function () {
            if (this.currentUserSubject) {
                return this.currentUserSubject.value;
            }
            return null;
        },
        enumerable: true,
        configurable: true
    });
    FoFAuthService.prototype.refreshByCookie = function () {
        var _this = this;
        return this.httpClient.get(this.environment.apiPath + "/auth/getUser")
            .pipe(map(function (user) {
            if (user && user.accessToken) {
                _this.currentUserSubject.next(user);
                // this.currentUserSubject.complete()         
            }
            return user;
        }));
    };
    FoFAuthService.prototype.loginKerberos = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.httpClient.get(_this.environment.authPath + "/auth/winlogin", { withCredentials: true })
                .toPromise()
                .then(function (user) {
                // give the temporary windows token to do the next get        
                if (user && user.accessToken) {
                    _this.currentUserSubject.next(user);
                }
                _this.httpClient.get(_this.environment.apiPath + "/auth/winUserValidate")
                    .toPromise()
                    .then(function (user) {
                    // get the final user. 
                    // Can work only if the windows user are registered into the App.
                    // if not, it means the user is just authentified against MS AD
                    if (user && user.accessToken) {
                        _this.currentUserSubject.next(user);
                    }
                    _this.currentUserSubject.complete();
                    resolve(user);
                })
                    .catch(function (error) {
                    reject(error);
                });
            })
                .catch(function (error) {
                reject(error);
            });
        });
    };
    FoFAuthService.prototype.login = function (username, password) {
        var _this = this;
        var data = {
            username: username,
            password: password
        };
        return this.httpClient.post(this.environment.apiPath + "/auth/login", data)
            .pipe(map(function (user) {
            // login successful if there's a jwt token in the response         
            if (user && user.accessToken) {
                // There is a cookie with the jwt inside for managing web browser refresh            
                _this.currentUserSubject.next(user);
            }
            return user;
        }));
    };
    FoFAuthService.prototype.logOut = function () {
        var _this = this;
        // remove cookie to log user out    
        return this.httpClient.get(this.environment.apiPath + "/auth/logout")
            .pipe(map(function (cookieDeleted) {
            if (cookieDeleted) {
                _this.currentUserSubject.next(null);
                _this.router.navigate(['/login']);
            }
            return cookieDeleted;
        }));
        // .subscribe(
        //   cookieDeleted => {
        //     if (cookieDeleted) {
        //       this.currentUserSubject.next(null)    
        //       this.router.navigate(['/login'])        
        //     }
        //   },
        //   error => {
        //     throw error
        //   })    
    };
    FoFAuthService.ɵfac = function FoFAuthService_Factory(t) { return new (t || FoFAuthService)(ɵɵinject(CORE_CONFIG), ɵɵinject(HttpClient), ɵɵinject(Router)); };
    FoFAuthService.ɵprov = ɵɵdefineInjectable({ token: FoFAuthService, factory: FoFAuthService.ɵfac, providedIn: 'root' });
    return FoFAuthService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FoFAuthService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: HttpClient }, { type: Router }]; }, null); })();

var FoFCoreService = /** @class */ (function () {
    function FoFCoreService(fofConfig, httpClient, router, foFAuthService) {
        var _this = this;
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.router = router;
        this.foFAuthService = foFAuthService;
        this._initializationReadySubject = new BehaviorSubject(false);
        this.initializationReady$ = this._initializationReadySubject.asObservable();
        this._environment = this.fofConfig.environment;
        forkJoin(this.foFAuthService.currentUser$).subscribe({
            // next: value => console.log('value', value),
            complete: function () {
                _this._initializationReadySubject.next(true);
            }
        });
    }
    Object.defineProperty(FoFCoreService.prototype, "environment", {
        get: function () {
            return this._environment;
        },
        enumerable: true,
        configurable: true
    });
    FoFCoreService.ɵfac = function FoFCoreService_Factory(t) { return new (t || FoFCoreService)(ɵɵinject(CORE_CONFIG), ɵɵinject(HttpClient), ɵɵinject(Router), ɵɵinject(FoFAuthService)); };
    FoFCoreService.ɵprov = ɵɵdefineInjectable({ token: FoFCoreService, factory: FoFCoreService.ɵfac, providedIn: 'root' });
    return FoFCoreService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FoFCoreService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: HttpClient }, { type: Router }, { type: FoFAuthService }]; }, null); })();

// toDO: change all relations! -> must NOT use an ID!
var FofPermissionService = /** @class */ (function () {
    function FofPermissionService(fofConfig, httpClient, coreService) {
        var _this = this;
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.coreService = coreService;
        this.user = {
            create: function (role) { return _this.httpClient.post(_this.environment.apiPath + "/users", role); },
            update: function (role) { return _this.httpClient.patch(_this.environment.apiPath + "/users/" + role.id, role); },
            delete: function (role) { return _this.httpClient.delete(_this.environment.apiPath + "/users/" + role.id); },
            getAll: function () { return _this.httpClient.get(_this.environment.apiPath + "/users"); },
            search: function (fullSearch, organizationsId, limit, page, sort, direction) {
                if (direction === void 0) { direction = 'asc'; }
                var query = "limit=" + limit + "&page=" + page + "&sort=" + sort + "&order=" + direction;
                if (organizationsId) {
                    query += "&organizationsId=" + organizationsId;
                }
                if (fullSearch) {
                    query += "&fullSearch=" + fullSearch;
                }
                return _this.httpClient.get(_this.environment.apiPath + "/users?" + query);
            },
            replaceUserRoleOrganization: function (userRoleOrganizations, userId, organisationId) {
                return _this.httpClient.put(_this.environment.apiPath + "/users/" + userId + "/organization/" + organisationId + "/roles", userRoleOrganizations);
            },
            deleteUserRoleOrganizations: function (userId, organisationsId) {
                return _this.httpClient.delete(_this.environment.apiPath + "/users/" + userId + "/organizations?ids=" + JSON.stringify(organisationsId));
            },
            getWithRoleById: function (id) {
                var qb = RequestQueryBuilder.create();
                qb.setJoin({ field: 'userRoleOrganizations', select: ['roleId', 'organizationId'] })
                    .setJoin({ field: 'userRoleOrganizations.role', select: ['code', 'description'] })
                    .setJoin({ field: 'userRoleOrganizations.organization', select: ['name', 'code'] })
                    .sortBy({ field: 'organization.name', order: 'DESC' });
                return _this.httpClient.get(_this.environment.apiPath + "/users/" + id + "?" + qb.query());
            },
            getWithUservicesById: function (id) {
                var qb = RequestQueryBuilder.create();
                qb.setJoin({ field: 'userUServices', select: ['uServiceId'] });
                return _this.httpClient.get(_this.environment.apiPath + "/users/" + id + "?" + qb.query());
            }
        };
        this.role = {
            create: function (role) { return _this.httpClient.post(_this.environment.apiPath + "/roles", role); },
            update: function (role) { return _this.httpClient.patch(_this.environment.apiPath + "/roles/" + role.id, role); },
            delete: function (role) { return _this.httpClient.delete(_this.environment.apiPath + "/roles/" + role.id); },
            getAll: function () { return _this.httpClient.get(_this.environment.apiPath + "/roles"); },
            search: function (query, limit, page, sort, direction) {
                if (direction === void 0) { direction = 'asc'; }
                var qb = RequestQueryBuilder.create();
                qb.setLimit(limit)
                    .setPage(page + 1)
                    .sortBy({ field: sort, order: direction.toUpperCase() });
                return _this.httpClient.get(_this.environment.apiPath + "/roles?" + qb.query());
            },
            getWithPermissionByRoleCode: function (roleCode) {
                // toDo: post a CR for edge browser?
                // https://github.com/nestjsx/crud/wiki/Requests#frontend-usage
                var qb = RequestQueryBuilder.create();
                // qb.select(['code', 'description'])
                qb.setJoin({ field: 'rolePermissions', select: ['permissionId'] })
                    .setJoin({ field: 'rolePermissions.permission', select: ['code'] })
                    // .setLimit(1)
                    // .setPage(1)
                    .sortBy({ field: 'permission.code', order: 'ASC' })
                    .search({ code: roleCode });
                return _this.httpClient.get(_this.environment.apiPath + "/roles?" + qb.query());
            }
        };
        this.permission = {
            getAll: function () {
                var qb = RequestQueryBuilder.create();
                qb.sortBy({ field: 'permission.code', order: 'ASC' });
                return _this.httpClient.get(_this.environment.apiPath + "/permissions?" + qb.query());
            }
        };
        this.organization = {
            create: function (organization) {
                return _this.httpClient.post(_this.environment.apiPath + "/organizations", organization);
            },
            update: function (organization) {
                return _this.httpClient.patch(_this.environment.apiPath + "/organizations/" + organization.id, organization);
            },
            delete: function (organization) {
                return _this.httpClient.delete(_this.environment.apiPath + "/organizations/" + organization.id);
            },
            getTreeView: function () {
                var qb = RequestQueryBuilder.create();
                qb.select(['code', 'name']);
                return _this.httpClient.get(_this.environment.apiPath + "/organizations/getTreeView?" + qb.query());
            }
        };
        this.rolePermission = {
            create: function (rolePermission) { return _this.httpClient.post(_this.environment.apiPath + "/rolePermissions", rolePermission); },
            delete: function (rolePermission) { return _this.httpClient.delete(_this.environment.apiPath + "/rolePermissions/" + rolePermission.id); }
        };
        this.userRoleOrganization = {
            create: function (userRoleOrganization) { return _this.httpClient.post(_this.environment.apiPath + "/userRoleOrganizations", userRoleOrganization); },
            delete: function (userRoleOrganization) { return _this.httpClient.delete(_this.environment.apiPath + "/userRoleOrganizations/" + userRoleOrganization.id); },
            bulkCreate: function (userRoleOrganizations) { return _this.httpClient.post(_this.environment.apiPath + "/userRoleOrganizations/bulk", { bulk: userRoleOrganizations }); }
        };
        this.userUservice = {
            create: function (userService) { return _this.httpClient.post(_this.environment.apiPath + "/userUservices", userService); },
            delete: function (id) { return _this.httpClient.delete(_this.environment.apiPath + "/userUservices/" + id); }
        };
        this.environment = this.fofConfig.environment;
    }
    FofPermissionService.ɵfac = function FofPermissionService_Factory(t) { return new (t || FofPermissionService)(ɵɵinject(CORE_CONFIG), ɵɵinject(HttpClient), ɵɵinject(FoFCoreService)); };
    FofPermissionService.ɵprov = ɵɵdefineInjectable({ token: FofPermissionService, factory: FofPermissionService.ɵfac, providedIn: 'root' });
    return FofPermissionService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofPermissionService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: HttpClient }, { type: FoFCoreService }]; }, null); })();

function FofOrganizationsTreeComponent_mat_tree_node_1_Template(rf, ctx) { if (rf & 1) {
    var _r185 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-tree-node", 3);
    ɵɵelementStart(1, "li", 4);
    ɵɵelement(2, "button", 5);
    ɵɵelementStart(3, "mat-checkbox", 6);
    ɵɵlistener("change", function FofOrganizationsTreeComponent_mat_tree_node_1_Template_mat_checkbox_change_3_listener($event) { ɵɵrestoreView(_r185); var node_r183 = ctx.$implicit; var ctx_r184 = ɵɵnextContext(); return ctx_r184.uiAction.treeViewNodeSelect(node_r183, $event); });
    ɵɵtext(4);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var node_r183 = ctx.$implicit;
    ɵɵadvance(3);
    ɵɵproperty("checked", node_r183.checked)("disabled", node_r183.notSelectable);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", node_r183.name, "");
} }
function FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template(rf, ctx) { if (rf & 1) {
    var _r188 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-nested-tree-node");
    ɵɵelementStart(1, "li");
    ɵɵelementStart(2, "div", 4);
    ɵɵelementStart(3, "button", 7);
    ɵɵelementStart(4, "mat-icon", 8);
    ɵɵtext(5);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(6, "mat-checkbox", 6);
    ɵɵlistener("change", function FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template_mat_checkbox_change_6_listener($event) { ɵɵrestoreView(_r188); var node_r186 = ctx.$implicit; var ctx_r187 = ɵɵnextContext(); return ctx_r187.uiAction.treeViewNodeSelect(node_r186, $event); });
    ɵɵtext(7);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(8, "ul", 9);
    ɵɵelementContainer(9, 10);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var node_r186 = ctx.$implicit;
    var ctx_r182 = ɵɵnextContext();
    ɵɵadvance(5);
    ɵɵtextInterpolate1(" ", ctx_r182.uiVar.organizations.treeControl.isExpanded(node_r186) ? "expand_more" : "chevron_right", " ");
    ɵɵadvance(1);
    ɵɵproperty("checked", node_r186.checked)("disabled", node_r186.notSelectable);
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", node_r186.name, "");
    ɵɵadvance(1);
    ɵɵclassProp("treeview-invisible", !ctx_r182.uiVar.organizations.treeControl.isExpanded(node_r186));
} }
var FofOrganizationsTreeComponent = /** @class */ (function () {
    function FofOrganizationsTreeComponent(fofPermissionService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        /** Can the user could select multiple organizations? */
        this.multiSelect = false;
        /** Submit all the organization selected everytime an orgnization is selected or unselected
         * AN objet or array, depending from the multiSelect param
        */
        this.selectedOrganizationsChange = new EventEmitter();
        // All private variables
        this.priVar = {
            currentNode: undefined,
            selectedNodesSynchronizeSave: undefined
        };
        // All private functions
        this.privFunc = {
            selectedNodeSynchronize: function () {
                var tree = _this.uiVar.organizations.dataSource.data;
                var selectedNodesSynchronize = _this.selectedNodesSynchronize;
                var areSomeNodeNotSelectable = false;
                if (_this.notSelectableOrganizations && _this.notSelectableOrganizations.length > 0) {
                    areSomeNodeNotSelectable = true;
                }
                var getSelectedNode = function (node) {
                    if (selectedNodesSynchronize) {
                        var foundedNode = selectedNodesSynchronize.filter(function (n) {
                            if (n.id === node.id) {
                                // ugly fix: toDo: to review
                                if (!n.name) {
                                    n.name = node.name;
                                }
                                return node;
                            }
                        });
                        if (foundedNode.length > 0) {
                            node.checked = true;
                        }
                        else {
                            node.checked = false;
                        }
                        // if (selectedNodesSynchronize && selectedNodesSynchronize.filter(n => n.id === node.id).length > 0) {
                        //   node.checked = true          
                        // } else {
                        //   node.checked = false
                        // }
                    }
                    if (areSomeNodeNotSelectable) {
                        if (_this.notSelectableOrganizations.filter(function (n) { return n.id === node.id; }).length > 0) {
                            // would prevent the user to check again the same organization 
                            if (_this.priVar.selectedNodesSynchronizeSave.filter(function (n) { return n.id === node.id; }).length === 0) {
                                node.notSelectable = true;
                            }
                        }
                        else {
                            node.notSelectable = false;
                        }
                    }
                    node.children.forEach(function (child) { return getSelectedNode(child); });
                };
                if (tree && tree.length > 0) {
                    getSelectedNode(tree[0]);
                }
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            loading: true,
            organizations: {
                treeControl: new NestedTreeControl(function (node) { return node.children; }),
                dataSource: new MatTreeNestedDataSource()
            },
        };
        // All actions shared with UI 
        this.uiAction = {
            OrganizationNodeHasChild: function (_, node) { return !!node.children && node.children.length > 0; },
            organizationTreeLoad: function () {
                _this.uiVar.loading = true;
                _this.fofPermissionService.organization.getTreeView()
                    .toPromise()
                    .then(function (organizations) {
                    _this.uiVar.organizations.dataSource.data = organizations;
                    if (organizations.length > 0) {
                        _this.uiVar.organizations.treeControl.expand(organizations[0]);
                        _this.privFunc.selectedNodeSynchronize();
                    }
                })
                    .finally(function () {
                    _this.uiVar.loading = false;
                });
            },
            treeViewNodeSelect: function (node, $event) {
                var tree = _this.uiVar.organizations.dataSource.data;
                if (_this.multiSelect) {
                    var nodes_1 = [];
                    node.checked = $event.checked;
                    var getSelectedNode_1 = function (node) {
                        if (node.checked) {
                            var selectNode = __assign({}, node);
                            selectNode.children = null;
                            nodes_1.push(selectNode);
                        }
                        node.children.forEach(function (child) { return getSelectedNode_1(child); });
                    };
                    if (tree && tree.length > 0) {
                        getSelectedNode_1(tree[0]);
                    }
                    _this.selectedOrganizationsChange.emit(nodes_1);
                }
                else {
                    var nodeUnSelect_1 = function (node) {
                        node.indeterminate = false;
                        node.checked = false;
                        if (!node.children) {
                            node.children = [];
                        }
                        node.children.forEach(function (child) { return nodeUnSelect_1(child); });
                    };
                    if (tree && tree.length > 0) {
                        nodeUnSelect_1(tree[0]);
                    }
                    node.checked = $event.checked;
                    if (node.checked) {
                        _this.priVar.currentNode = node;
                    }
                    else {
                        _this.priVar.currentNode = null;
                    }
                    _this.selectedOrganizationsChange.emit(_this.priVar.currentNode);
                }
            }
        };
        this.uiAction.organizationTreeLoad();
    }
    // Angular events
    FofOrganizationsTreeComponent.prototype.ngOnInit = function () {
    };
    FofOrganizationsTreeComponent.prototype.ngOnChanges = function () {
        var _this = this;
        if (this.nodeToDelete) {
            var spliceNode_1 = function (node) {
                if (node.id === _this.nodeToDelete.id)
                    return null;
                node.children.forEach(function (child, index) {
                    if (child.id === _this.nodeToDelete.id) {
                        node.children.splice(index, 1);
                        return;
                    }
                    spliceNode_1(child);
                });
            };
            var data = this.uiVar.organizations.dataSource.data.slice();
            spliceNode_1(data[0]);
            // toDO: Performance   
            // bug on tree refresh
            // https://github.com/angular/components/issues/11381   
            this.uiVar.organizations.dataSource.data = null;
            this.uiVar.organizations.dataSource.data = data;
            this.nodeToDelete = null;
            return;
        }
        if (this.nodeChanged) {
            this.priVar.currentNode = this.nodeChanged;
            // toDO: Performance   
            // bug on tree refresh
            // https://github.com/angular/components/issues/11381        
            var data = this.uiVar.organizations.dataSource.data.slice();
            this.uiVar.organizations.dataSource.data = null;
            this.uiVar.organizations.dataSource.data = data;
            this.nodeChanged = null;
        }
        if (this.selectedNodesSynchronize || this.notSelectableOrganizations) {
            if (!this.priVar.selectedNodesSynchronizeSave) {
                this.priVar.selectedNodesSynchronizeSave = this.selectedNodesSynchronize.slice();
            }
            this.privFunc.selectedNodeSynchronize();
        }
    };
    FofOrganizationsTreeComponent.ɵfac = function FofOrganizationsTreeComponent_Factory(t) { return new (t || FofOrganizationsTreeComponent)(ɵɵdirectiveInject(FofPermissionService)); };
    FofOrganizationsTreeComponent.ɵcmp = ɵɵdefineComponent({ type: FofOrganizationsTreeComponent, selectors: [["fof-organizations-tree"]], inputs: { nodeChanged: "nodeChanged", nodeToDelete: "nodeToDelete", multiSelect: "multiSelect", selectedNodesSynchronize: "selectedNodesSynchronize", notSelectableOrganizations: "notSelectableOrganizations" }, outputs: { selectedOrganizationsChange: "selectedOrganizationsChange" }, features: [ɵɵNgOnChangesFeature], decls: 3, vars: 3, consts: [[1, "treeview", 3, "dataSource", "treeControl"], ["matTreeNodeToggle", "", 4, "matTreeNodeDef"], [4, "matTreeNodeDef", "matTreeNodeDefWhen"], ["matTreeNodeToggle", ""], [1, "mat-tree-node"], ["mat-icon-button", "", "disabled", ""], [3, "checked", "disabled", "change"], ["mat-icon-button", "", "matTreeNodeToggle", "", 1, "fof-test-btn"], [1, "mat-icon-rtl-mirror"], [1, ""], ["matTreeNodeOutlet", ""]], template: function FofOrganizationsTreeComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-tree", 0);
            ɵɵtemplate(1, FofOrganizationsTreeComponent_mat_tree_node_1_Template, 5, 3, "mat-tree-node", 1);
            ɵɵtemplate(2, FofOrganizationsTreeComponent_mat_nested_tree_node_2_Template, 10, 6, "mat-nested-tree-node", 2);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵproperty("dataSource", ctx.uiVar.organizations.dataSource)("treeControl", ctx.uiVar.organizations.treeControl);
            ɵɵadvance(2);
            ɵɵproperty("matTreeNodeDefWhen", ctx.uiAction.OrganizationNodeHasChild);
        } }, directives: [MatTree, MatTreeNodeDef, MatTreeNode, MatTreeNodeToggle, MatButton, MatCheckbox, MatNestedTreeNode, MatIcon, MatTreeNodeOutlet], styles: [".mat-tree[_ngcontent-%COMP%]{height:100%;margin-bottom:15px}.mat-tree[_ngcontent-%COMP%]   .mat-tree-node[_ngcontent-%COMP%]{margin-right:15px}.treeview-invisible[_ngcontent-%COMP%]{display:none}.treeview[_ngcontent-%COMP%]   li[_ngcontent-%COMP%], .treeview[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{margin-top:0;margin-bottom:0;list-style-type:none}.treeview[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]{padding-left:25px}.mat-tree-node[_ngcontent-%COMP%]{min-height:25px}.mat-form-field[_ngcontent-%COMP%]{width:100%}"] });
    return FofOrganizationsTreeComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofOrganizationsTreeComponent, [{
        type: Component,
        args: [{
                selector: 'fof-organizations-tree',
                templateUrl: './fof-organizations-tree.component.html',
                styleUrls: ['./fof-organizations-tree.component.scss']
            }]
    }], function () { return [{ type: FofPermissionService }]; }, { nodeChanged: [{
            type: Input
        }], nodeToDelete: [{
            type: Input
        }], multiSelect: [{
            type: Input
        }], selectedNodesSynchronize: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }], selectedOrganizationsChange: [{
            type: Output
        }] }); })();

var ComponentsService = /** @class */ (function () {
    function ComponentsService() {
    }
    ComponentsService.ɵfac = function ComponentsService_Factory(t) { return new (t || ComponentsService)(); };
    ComponentsService.ɵprov = ɵɵdefineInjectable({ token: ComponentsService, factory: ComponentsService.ɵfac, providedIn: 'root' });
    return ComponentsService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(ComponentsService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

var FofPermissionModule = /** @class */ (function () {
    function FofPermissionModule() {
    }
    FofPermissionModule.ɵmod = ɵɵdefineNgModule({ type: FofPermissionModule });
    FofPermissionModule.ɵinj = ɵɵdefineInjector({ factory: function FofPermissionModule_Factory(t) { return new (t || FofPermissionModule)(); }, providers: [
            FofPermissionService
        ], imports: [[
                CommonModule
            ]] });
    return FofPermissionModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(FofPermissionModule, { imports: [CommonModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofPermissionModule, [{
        type: NgModule,
        args: [{
                declarations: [],
                imports: [
                    CommonModule
                ],
                providers: [
                    FofPermissionService
                ]
            }]
    }], null, null); })();

var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule.ɵmod = ɵɵdefineNgModule({ type: MaterialModule });
    MaterialModule.ɵinj = ɵɵdefineInjector({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [[
                // MatAutocompleteModule,
                // MatBadgeModule,
                // MatBottomSheetModule,
                MatButtonModule,
                // MatButtonToggleModule,
                MatCardModule,
                MatCheckboxModule,
                MatChipsModule,
                // MatDatepickerModule,
                MatDialogModule,
                MatDividerModule,
                MatExpansionModule,
                // MatGridListModule,
                MatIconModule,
                MatInputModule,
                MatListModule,
                // MatMenuModule,
                // MatNativeDateModule,
                MatPaginatorModule,
                // MatProgressBarModule,
                MatProgressSpinnerModule,
                // MatRadioModule,
                // MatRippleModule,
                // MatSelectModule,
                // MatSidenavModule,
                // MatSliderModule,
                // MatSlideToggleModule,
                // MatSnackBarModule,
                MatSortModule,
                // MatStepperModule,
                MatTableModule,
                // MatTabsModule,
                // MatToolbarModule,
                // MatTooltipModule,
                MatTreeModule
            ],
            // MatAutocompleteModule,
            // MatBadgeModule,
            // MatBottomSheetModule,
            MatButtonModule,
            // MatButtonToggleModule,
            MatCardModule,
            MatCheckboxModule,
            MatChipsModule,
            // MatDatepickerModule,
            MatDialogModule,
            MatDividerModule,
            MatExpansionModule,
            // MatGridListModule,
            MatIconModule,
            MatInputModule,
            MatListModule,
            // MatMenuModule,
            // MatNativeDateModule,
            MatPaginatorModule,
            // MatProgressBarModule,
            MatProgressSpinnerModule,
            // MatRadioModule,
            // MatRippleModule,
            // MatSelectModule,
            // MatSidenavModule,
            // MatSliderModule,
            // MatSlideToggleModule,
            // MatSnackBarModule,
            MatSortModule,
            // MatStepperModule,
            MatTableModule,
            // MatTabsModule,
            // MatToolbarModule,
            // MatTooltipModule,
            MatTreeModule] });
    return MaterialModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(MaterialModule, { imports: [
        // MatAutocompleteModule,
        // MatBadgeModule,
        // MatBottomSheetModule,
        MatButtonModule,
        // MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        // MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        // MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        // MatMenuModule,
        // MatNativeDateModule,
        MatPaginatorModule,
        // MatProgressBarModule,
        MatProgressSpinnerModule,
        // MatRadioModule,
        // MatRippleModule,
        // MatSelectModule,
        // MatSidenavModule,
        // MatSliderModule,
        // MatSlideToggleModule,
        // MatSnackBarModule,
        MatSortModule,
        // MatStepperModule,
        MatTableModule,
        // MatTabsModule,
        // MatToolbarModule,
        // MatTooltipModule,
        MatTreeModule], exports: [
        // MatAutocompleteModule,
        // MatBadgeModule,
        // MatBottomSheetModule,
        MatButtonModule,
        // MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        // MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        // MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        // MatMenuModule,
        // MatNativeDateModule,
        MatPaginatorModule,
        // MatProgressBarModule,
        MatProgressSpinnerModule,
        // MatRadioModule,
        // MatRippleModule,
        // MatSelectModule,
        // MatSidenavModule,
        // MatSliderModule,
        // MatSlideToggleModule,
        // MatSnackBarModule,
        MatSortModule,
        // MatStepperModule,
        MatTableModule,
        // MatTabsModule,
        // MatToolbarModule,
        // MatTooltipModule,
        MatTreeModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(MaterialModule, [{
        type: NgModule,
        args: [{
                imports: [
                    // MatAutocompleteModule,
                    // MatBadgeModule,
                    // MatBottomSheetModule,
                    MatButtonModule,
                    // MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    // MatDatepickerModule,
                    MatDialogModule,
                    MatDividerModule,
                    MatExpansionModule,
                    // MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    // MatMenuModule,
                    // MatNativeDateModule,
                    MatPaginatorModule,
                    // MatProgressBarModule,
                    MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    // MatSelectModule,
                    // MatSidenavModule,
                    // MatSliderModule,
                    // MatSlideToggleModule,
                    // MatSnackBarModule,
                    MatSortModule,
                    // MatStepperModule,
                    MatTableModule,
                    // MatTabsModule,
                    // MatToolbarModule,
                    // MatTooltipModule,
                    MatTreeModule
                ],
                exports: [
                    // MatAutocompleteModule,
                    // MatBadgeModule,
                    // MatBottomSheetModule,
                    MatButtonModule,
                    // MatButtonToggleModule,
                    MatCardModule,
                    MatCheckboxModule,
                    MatChipsModule,
                    // MatDatepickerModule,
                    MatDialogModule,
                    MatDividerModule,
                    MatExpansionModule,
                    // MatGridListModule,
                    MatIconModule,
                    MatInputModule,
                    MatListModule,
                    // MatMenuModule,
                    // MatNativeDateModule,
                    MatPaginatorModule,
                    // MatProgressBarModule,
                    MatProgressSpinnerModule,
                    // MatRadioModule,
                    // MatRippleModule,
                    // MatSelectModule,
                    // MatSidenavModule,
                    // MatSliderModule,
                    // MatSlideToggleModule,
                    // MatSnackBarModule,
                    MatSortModule,
                    // MatStepperModule,
                    MatTableModule,
                    // MatTabsModule,
                    // MatToolbarModule,
                    // MatTooltipModule,
                    MatTreeModule
                ],
                declarations: []
            }]
    }], null, null); })();

function FofOrganizationsMultiSelectComponent_mat_chip_4_Template(rf, ctx) { if (rf & 1) {
    var _r193 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-chip", 7);
    ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_click_0_listener() { ɵɵrestoreView(_r193); var ctx_r192 = ɵɵnextContext(); return ctx_r192.uiAction.openTree(); })("removed", function FofOrganizationsMultiSelectComponent_mat_chip_4_Template_mat_chip_removed_0_listener() { ɵɵrestoreView(_r193); var organisation_r191 = ctx.$implicit; var ctx_r194 = ɵɵnextContext(); return ctx_r194.uiAction.organisationRemove(organisation_r191); });
    ɵɵtext(1);
    ɵɵelementStart(2, "mat-icon", 8);
    ɵɵtext(3, "cancel");
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var organisation_r191 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", organisation_r191.name, " ");
} }
// https://stackblitz.com/edit/menu-overlay?file=src%2Fapp%2Fselect-input.component.html
var FofOrganizationsMultiSelectComponent = /** @class */ (function () {
    function FofOrganizationsMultiSelectComponent(elementRef) {
        var _this = this;
        this.elementRef = elementRef;
        this.multiSelect = true;
        this.selectedOrganisations = [];
        this.placeHolder = 'Organisations';
        this.selectedOrganizationsChange = new EventEmitter();
        // All private variables
        this.priVar = {};
        // All private functions
        this.privFunc = {
            refreshOuputs: function () {
                _this.selectedOrganizationsChange.emit(_this.selectedOrganisations);
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            treeMenuVisible: false,
        };
        // All actions shared with UI 
        this.uiAction = {
            openTree: function () {
                _this.uiVar.treeMenuVisible = !_this.uiVar.treeMenuVisible;
            },
            selectedOrganizationsChange: function (selectedOrganisations) {
                if (!selectedOrganisations) {
                    _this.selectedOrganisations = null;
                    return;
                }
                if (Array.isArray(selectedOrganisations)) {
                    _this.selectedOrganisations = selectedOrganisations;
                }
                else {
                    _this.selectedOrganisations = [selectedOrganisations];
                }
                _this.privFunc.refreshOuputs();
            },
            // Remove organisation from chip
            organisationRemove: function (organisationToRemove) {
                _this.selectedOrganisations = _this.selectedOrganisations.filter(function (organisation) { return organisationToRemove.id !== organisation.id; });
                _this.privFunc.refreshOuputs();
            }
        };
    }
    FofOrganizationsMultiSelectComponent.prototype.onDocumentClick = function (event) {
        if (!this.elementRef.nativeElement.contains(event.target)) {
            this.uiVar.treeMenuVisible = false;
        }
    };
    // Angular events
    FofOrganizationsMultiSelectComponent.prototype.ngOnInit = function () {
    };
    FofOrganizationsMultiSelectComponent.ɵfac = function FofOrganizationsMultiSelectComponent_Factory(t) { return new (t || FofOrganizationsMultiSelectComponent)(ɵɵdirectiveInject(ElementRef)); };
    FofOrganizationsMultiSelectComponent.ɵcmp = ɵɵdefineComponent({ type: FofOrganizationsMultiSelectComponent, selectors: [["fof-core-fof-organizations-multi-select"]], hostBindings: function FofOrganizationsMultiSelectComponent_HostBindings(rf, ctx) { if (rf & 1) {
            ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_click_HostBindingHandler($event) { return ctx.onDocumentClick($event); }, false, ɵɵresolveDocument);
        } }, inputs: { multiSelect: "multiSelect", selectedOrganisations: "selectedOrganisations", notSelectableOrganizations: "notSelectableOrganizations", placeHolder: "placeHolder" }, outputs: { selectedOrganizationsChange: "selectedOrganizationsChange" }, decls: 9, vars: 8, consts: [[1, "fof-menu-custom"], ["chipList", ""], [3, "click", "removed", 4, "ngFor", "ngForOf"], ["matInput", "", "readonly", "", 3, "placeholder", "matChipInputFor", "click"], [1, "cdk-overlay-pane"], [1, "organization-menu", "mat-menu-panel", "mat-elevation-z4"], [3, "multiSelect", "selectedNodesSynchronize", "notSelectableOrganizations", "selectedOrganizationsChange"], [3, "click", "removed"], ["matChipRemove", ""]], template: function FofOrganizationsMultiSelectComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "div", 0);
            ɵɵelementStart(1, "mat-form-field");
            ɵɵelementStart(2, "mat-chip-list", null, 1);
            ɵɵtemplate(4, FofOrganizationsMultiSelectComponent_mat_chip_4_Template, 4, 1, "mat-chip", 2);
            ɵɵelementStart(5, "input", 3);
            ɵɵlistener("click", function FofOrganizationsMultiSelectComponent_Template_input_click_5_listener() { return ctx.uiAction.openTree(); });
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(6, "div", 4);
            ɵɵelementStart(7, "div", 5);
            ɵɵelementStart(8, "fof-organizations-tree", 6);
            ɵɵlistener("selectedOrganizationsChange", function FofOrganizationsMultiSelectComponent_Template_fof_organizations_tree_selectedOrganizationsChange_8_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            var _r189 = ɵɵreference(3);
            ɵɵadvance(4);
            ɵɵproperty("ngForOf", ctx.selectedOrganisations);
            ɵɵadvance(1);
            ɵɵproperty("placeholder", ctx.placeHolder)("matChipInputFor", _r189);
            ɵɵadvance(1);
            ɵɵclassProp("tree-display", ctx.uiVar.treeMenuVisible);
            ɵɵadvance(2);
            ɵɵproperty("multiSelect", ctx.multiSelect)("selectedNodesSynchronize", ctx.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
        } }, directives: [MatFormField, MatChipList, NgForOf, MatInput, MatChipInput, FofOrganizationsTreeComponent, MatChip, MatIcon, MatChipRemove], styles: [".fof-menu-custom[_ngcontent-%COMP%]{position:relative}.fof-menu-custom[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]{width:100%;position:absolute;top:60px;display:none}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane.tree-display[_ngcontent-%COMP%]{display:block}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu[_ngcontent-%COMP%]{min-width:112px;overflow:auto;-webkit-overflow-scrolling:touch;border-radius:4px;height:100%}.fof-menu-custom[_ngcontent-%COMP%]   .cdk-overlay-pane[_ngcontent-%COMP%]   .organization-menu.mat-menu-panel[_ngcontent-%COMP%]{max-width:100%;max-height:unset}"] });
    return FofOrganizationsMultiSelectComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofOrganizationsMultiSelectComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-organizations-multi-select',
                templateUrl: './fof-organizations-multi-select.component.html',
                styleUrls: ['./fof-organizations-multi-select.component.scss'],
                host: {
                    '(document:click)': 'onDocumentClick($event)',
                }
            }]
    }], function () { return [{ type: ElementRef }]; }, { multiSelect: [{
            type: Input
        }], selectedOrganisations: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }], placeHolder: [{
            type: Input
        }], selectedOrganizationsChange: [{
            type: Output
        }] }); })();

var Notification = /** @class */ (function () {
    function Notification() {
    }
    return Notification;
}());
var NotificationType;
(function (NotificationType) {
    NotificationType[NotificationType["Success"] = 0] = "Success";
    NotificationType[NotificationType["Error"] = 1] = "Error";
    NotificationType[NotificationType["Info"] = 2] = "Info";
    NotificationType[NotificationType["Warning"] = 3] = "Warning";
})(NotificationType || (NotificationType = {}));
/** params for notification */
var NotificationParams = /** @class */ (function () {
    function NotificationParams() {
    }
    return NotificationParams;
}());

var FofNotificationService = /** @class */ (function () {
    function FofNotificationService() {
        this.saveIsDoneNotification = new BehaviorSubject(undefined);
        this.subject = new Subject();
        this.keepAfterRouteChange = false;
        // const router = this.injector.get(Router)
        // clear notification messages on route change unless 'keepAfterRouteChange' flag is true
        // router.events.subscribe(event => {
        //   if (event instanceof NavigationStart) {
        //     if (this.keepAfterRouteChange) {
        //       // only keep for a single route change
        //       this.keepAfterRouteChange = false
        //     } else {
        //       // clear notification messages
        //       this.clear()
        //     }
        //   }
        // })
    }
    Object.defineProperty(FofNotificationService.prototype, "savedNotification", {
        get: function () {
            return this.saveIsDoneNotification.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    FofNotificationService.prototype.getnotification = function () {
        return this.subject.asObservable();
    };
    /** Notifiy a success
     * @param message
     * accept html
     */
    FofNotificationService.prototype.success = function (message, params) {
        this.notification(NotificationType.Success, message, params);
    };
    /** Notifiy an error
     * @param message
     * accept html
     */
    FofNotificationService.prototype.error = function (message, params) {
        this.notification(NotificationType.Error, message, params);
    };
    /** Notifiy an info
     * @param message
     * accept html
     */
    FofNotificationService.prototype.info = function (message, params) {
        this.notification(NotificationType.Info, message, params);
    };
    /** Notifiy a warning
     * @param message
     * accept html
     */
    FofNotificationService.prototype.warn = function (message, params) {
        this.notification(NotificationType.Warning, message, params);
    };
    FofNotificationService.prototype.notification = function (type, message, params) {
        var notification = new Notification();
        if (params) {
            if (params.keepAfterRouteChange) {
                this.keepAfterRouteChange = params.keepAfterRouteChange;
            }
            if (params.mustDisappearAfter) {
                if (params.mustDisappearAfter < 0) {
                    params.mustDisappearAfter = undefined;
                }
                else {
                    notification.mustDisappearAfter = params.mustDisappearAfter;
                }
            }
            else {
                notification.mustDisappearAfter = 3000;
            }
        }
        else {
            notification.mustDisappearAfter = 3000;
        }
        notification.type = type;
        notification.message = message;
        this.subject.next(notification);
    };
    FofNotificationService.prototype.saveIsDone = function () {
        this.saveIsDoneNotification.next(true);
    };
    FofNotificationService.prototype.clear = function () {
        // clear notifications
        this.subject.next();
    };
    FofNotificationService.ɵfac = function FofNotificationService_Factory(t) { return new (t || FofNotificationService)(); };
    FofNotificationService.ɵprov = ɵɵdefineInjectable({ token: FofNotificationService, factory: FofNotificationService.ɵfac, providedIn: 'root' });
    return FofNotificationService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofNotificationService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

var FofDatatableComponent = /** @class */ (function () {
    function FofDatatableComponent(fofPermissionService, fofNotificationService, breakpointObserver) {
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['code', 'description'],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    FofDatatableComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['code'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['code', 'description'];
            }
        });
    };
    FofDatatableComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.role.search(null, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function () {
            _this.uiVar.isLoadingResults = false;
            return of([]);
        })).subscribe(function (data) { return _this.uiVar.data = data; });
    };
    FofDatatableComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    FofDatatableComponent.ɵfac = function FofDatatableComponent_Factory(t) { return new (t || FofDatatableComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(BreakpointObserver)); };
    FofDatatableComponent.ɵcmp = ɵɵdefineComponent({ type: FofDatatableComponent, selectors: [["fof-core-fof-datatable"]], viewQuery: function FofDatatableComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(MatPaginator, true);
            ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.paginator = _t.first);
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 2, vars: 0, template: function FofDatatableComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "p");
            ɵɵtext(1, "fof-datatable works!");
            ɵɵelementEnd();
        } }, styles: [""] });
    return FofDatatableComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofDatatableComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-datatable',
                templateUrl: './fof-datatable.component.html',
                styleUrls: ['./fof-datatable.component.scss']
            }]
    }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();

var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule.ɵmod = ɵɵdefineNgModule({ type: ComponentsModule });
    ComponentsModule.ɵinj = ɵɵdefineInjector({ factory: function ComponentsModule_Factory(t) { return new (t || ComponentsModule)(); }, providers: [
            ComponentsService
        ], imports: [[
                CommonModule,
                FofPermissionModule,
                MaterialModule,
                FormsModule,
                ReactiveFormsModule
            ]] });
    return ComponentsModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(ComponentsModule, { declarations: [FofOrganizationsTreeComponent,
        FofOrganizationsMultiSelectComponent,
        FofDatatableComponent], imports: [CommonModule,
        FofPermissionModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule], exports: [FofOrganizationsTreeComponent,
        FofOrganizationsMultiSelectComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(ComponentsModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    FofOrganizationsTreeComponent,
                    FofOrganizationsMultiSelectComponent,
                    FofDatatableComponent
                ],
                imports: [
                    CommonModule,
                    FofPermissionModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule
                ],
                providers: [
                    ComponentsService
                ],
                exports: [
                    FofOrganizationsTreeComponent,
                    FofOrganizationsMultiSelectComponent
                ]
            }]
    }], null, null); })();

function FofRolesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 13);
    ɵɵelement(1, "mat-spinner", 14);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des roles...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function FofRolesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 15);
    ɵɵtext(1, "Code");
    ɵɵelementEnd();
} }
function FofRolesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r202 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r202.code);
} }
function FofRolesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 17);
    ɵɵtext(1, "Description");
    ɵɵelementEnd();
} }
function FofRolesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 16);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r203 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r203.description);
} }
function FofRolesComponent_tr_15_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 18);
} }
function FofRolesComponent_tr_16_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 19);
} if (rf & 2) {
    var row_r204 = ctx.$implicit;
    ɵɵproperty("routerLink", row_r204.code);
} }
var _c0 = function () { return [5, 10, 25, 100]; };
var FofRolesComponent = /** @class */ (function () {
    function FofRolesComponent(fofPermissionService, fofNotificationService, breakpointObserver) {
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['code', 'description'],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    FofRolesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['code'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['code', 'description'];
            }
        });
    };
    FofRolesComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.role.search(null, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function () {
            _this.uiVar.isLoadingResults = false;
            return of([]);
        })).subscribe(function (data) { return _this.uiVar.data = data; });
    };
    FofRolesComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    FofRolesComponent.ɵfac = function FofRolesComponent_Factory(t) { return new (t || FofRolesComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(BreakpointObserver)); };
    FofRolesComponent.ɵcmp = ɵɵdefineComponent({ type: FofRolesComponent, selectors: [["fof-roles"]], viewQuery: function FofRolesComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(MatPaginator, true);
            ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.paginator = _t.first);
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 18, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "code", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "code"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "description"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function FofRolesComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-card", 0);
            ɵɵelementStart(1, "h3");
            ɵɵtext(2, "R\u00F4les");
            ɵɵelementEnd();
            ɵɵelementStart(3, "a", 1);
            ɵɵelementStart(4, "span");
            ɵɵtext(5, "Ajouter un r\u00F4le");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(6, "div", 2);
            ɵɵtemplate(7, FofRolesComponent_div_7_Template, 4, 0, "div", 3);
            ɵɵelementStart(8, "table", 4);
            ɵɵelementContainerStart(9, 5);
            ɵɵtemplate(10, FofRolesComponent_th_10_Template, 2, 0, "th", 6);
            ɵɵtemplate(11, FofRolesComponent_td_11_Template, 2, 1, "td", 7);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(12, 8);
            ɵɵtemplate(13, FofRolesComponent_th_13_Template, 2, 0, "th", 9);
            ɵɵtemplate(14, FofRolesComponent_td_14_Template, 2, 1, "td", 7);
            ɵɵelementContainerEnd();
            ɵɵtemplate(15, FofRolesComponent_tr_15_Template, 1, 0, "tr", 10);
            ɵɵtemplate(16, FofRolesComponent_tr_16_Template, 1, 1, "tr", 11);
            ɵɵelementEnd();
            ɵɵelement(17, "mat-paginator", 12);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(3);
            ɵɵproperty("routerLink", "/admin/roles/new");
            ɵɵadvance(4);
            ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            ɵɵadvance(1);
            ɵɵproperty("dataSource", ctx.uiVar.data);
            ɵɵadvance(7);
            ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", ɵɵpureFunction0(8, _c0))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [MatCard, MatAnchor, RouterLinkWithHref, NgIf, MatTable, MatSort, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatPaginator, MatSpinner, MatHeaderCell, MatSortHeader, MatCell, MatHeaderRow, MatRow, RouterLink], styles: [""] });
    return FofRolesComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofRolesComponent, [{
        type: Component,
        args: [{
                selector: 'fof-roles',
                templateUrl: './fof-roles.component.html',
                styleUrls: ['./fof-roles.component.scss'],
            }]
    }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();

var _this = this;
var fofUtilsForm = {
    validateAllFields: function (formGroup) {
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                _this.validateAllFormFields(control);
            }
        });
    }
};
var fofError = {
    clean: function (errorToManage) {
        var error;
        var message;
        var isConstraintError = false;
        if (errorToManage.error) {
            error = errorToManage.error;
            if (error.fofCoreException) {
                message = error.exceptionDetail.message;
                if (message.search('constraint') > -1) {
                    isConstraintError = true;
                }
                return __assign({ isConstraintError: isConstraintError }, error);
            }
        }
    },
    cleanAndMange: function (errorToManage) {
    }
};

// Angular
function FofCoreDialogYesNoComponent_button_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "button", 5);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r205 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate(ctx_r205.uiVar.noLabel);
} }
var FofCoreDialogYesNoComponent = /** @class */ (function () {
    function FofCoreDialogYesNoComponent(data) {
        this.data = data;
        // All input var
        this.anInput = undefined;
        // All output notification
        this.anOuputEvent = new EventEmitter();
        // All variables private to the class
        this.priVar = {};
        // All private functions
        this.privFunc = {};
        // All variables shared with UI HTML interface
        this.uiVar = {
            title: 'fof à une question',
            question: undefined,
            yesLabel: 'oui',
            noLabel: 'non',
            informationOnly: false
        };
        // All actions accessible by user (could be used internaly too)
        this.uiAction = {};
        // this.matDialogRef.updatePosition({ top: '20px', left: '50px' });
        this.uiVar.question = data.question;
        if (data.title) {
            this.uiVar.title = data.title;
        }
        if (data.yesLabel) {
            this.uiVar.yesLabel = data.yesLabel;
        }
        if (data.noLabel) {
            this.uiVar.noLabel = data.noLabel;
        }
        if (data.informationOnly) {
            this.uiVar.informationOnly = data.informationOnly;
        }
    }
    // Angular events
    FofCoreDialogYesNoComponent.prototype.ngOnInit = function () {
    };
    FofCoreDialogYesNoComponent.prototype.ngAfterViewInit = function () {
    };
    FofCoreDialogYesNoComponent.ɵfac = function FofCoreDialogYesNoComponent_Factory(t) { return new (t || FofCoreDialogYesNoComponent)(ɵɵdirectiveInject(MAT_DIALOG_DATA, 8)); };
    FofCoreDialogYesNoComponent.ɵcmp = ɵɵdefineComponent({ type: FofCoreDialogYesNoComponent, selectors: [["core-dialog-yes-no"]], inputs: { anInput: "anInput" }, outputs: { anOuputEvent: "anOuputEvent" }, decls: 9, vars: 5, consts: [[1, "view-mad--core--yes-no"], ["mat-dialog-title", ""], [3, "innerHTML"], ["mat-button", "", "mat-dialog-close", "", 4, "ngIf"], ["mat-button", "", 3, "mat-dialog-close"], ["mat-button", "", "mat-dialog-close", ""]], template: function FofCoreDialogYesNoComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "div", 0);
            ɵɵelementStart(1, "h2", 1);
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelementStart(3, "mat-dialog-content");
            ɵɵelement(4, "div", 2);
            ɵɵelementEnd();
            ɵɵelementStart(5, "mat-dialog-actions");
            ɵɵtemplate(6, FofCoreDialogYesNoComponent_button_6_Template, 2, 1, "button", 3);
            ɵɵelementStart(7, "button", 4);
            ɵɵtext(8);
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.uiVar.title);
            ɵɵadvance(2);
            ɵɵproperty("innerHTML", ctx.uiVar.question, ɵɵsanitizeHtml);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", !ctx.uiVar.informationOnly);
            ɵɵadvance(1);
            ɵɵproperty("mat-dialog-close", true);
            ɵɵadvance(1);
            ɵɵtextInterpolate(ctx.uiVar.yesLabel);
        } }, directives: [MatDialogTitle, MatDialogContent, MatDialogActions, NgIf, MatButton, MatDialogClose], styles: [".view-mad--core--yes-no .mat-dialog-actions{margin-top:1rem;display:flex;justify-content:flex-end}"], encapsulation: 2 });
    return FofCoreDialogYesNoComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofCoreDialogYesNoComponent, [{
        type: Component,
        args: [{
                selector: 'core-dialog-yes-no',
                templateUrl: './core-dialog-yes-no.component.html',
                styleUrls: ['./core-dialog-yes-no.component.scss'],
                encapsulation: ViewEncapsulation.None
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Optional
            }, {
                type: Inject,
                args: [MAT_DIALOG_DATA]
            }] }]; }, { anInput: [{
            type: Input
        }], anOuputEvent: [{
            type: Output
        }] }); })();

var FofDialogService = /** @class */ (function () {
    function FofDialogService(matDialog) {
        this.matDialog = matDialog;
    }
    FofDialogService.prototype.openYesNo = function (options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _options = options;
            var width = options.width || '300px';
            var height = options.height || undefined;
            _options.informationOnly = _options.informationOnly || false;
            var dialogRef = _this.matDialog.open(FofCoreDialogYesNoComponent, {
                data: _options,
                width: width,
                height: height,
                // to have a look on, bug
                position: {
                    top: '150px'
                }
            });
            dialogRef.afterClosed()
                .toPromise()
                .then(function (result) {
                resolve(result);
            });
        });
    };
    FofDialogService.prototype.openInformation = function (options) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _options = options;
            _options.informationOnly = true;
            _options.title = _options.title || 'fof à une information';
            _options.yesLabel = _options.yesLabel || 'ok';
            _this.openYesNo(_options)
                .then(function (result) {
                resolve(result);
            });
        });
    };
    FofDialogService.ɵfac = function FofDialogService_Factory(t) { return new (t || FofDialogService)(ɵɵinject(MatDialog)); };
    FofDialogService.ɵprov = ɵɵdefineInjectable({ token: FofDialogService, factory: FofDialogService.ɵfac, providedIn: 'root' });
    return FofDialogService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofDialogService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: MatDialog }]; }, null); })();

function FofEntityFooterComponent_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 4);
    ɵɵtext(1);
    ɵɵpipe(2, "date");
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r299 = ɵɵnextContext(2);
    ɵɵadvance(1);
    ɵɵtextInterpolate2(" Derni\u00E8re modification le ", ɵɵpipeBind2(2, 2, ctx_r299.entityBase._updatedDate, "short"), " par ", ctx_r299.entityBase._updatedByName, " ");
} }
function FofEntityFooterComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 3);
    ɵɵelementStart(1, "div", 4);
    ɵɵtext(2);
    ɵɵpipe(3, "date");
    ɵɵelementEnd();
    ɵɵtemplate(4, FofEntityFooterComponent_div_1_div_4_Template, 3, 5, "div", 5);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r296 = ɵɵnextContext();
    var _r297 = ɵɵreference(3);
    ɵɵadvance(2);
    ɵɵtextInterpolate2(" Cr\u00E9ation le ", ɵɵpipeBind2(3, 4, ctx_r296.entityBase._createdDate, "short"), " par ", ctx_r296.entityBase._createdByName, " ");
    ɵɵadvance(2);
    ɵɵproperty("ngIf", ctx_r296.entityBase._updatedByName)("ngIfElse", _r297);
} }
function FofEntityFooterComponent_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 4);
    ɵɵtext(1, " Aucune modification depuis la cr\u00E9ation ");
    ɵɵelementEnd();
} }
var FofEntityFooterComponent = /** @class */ (function () {
    function FofEntityFooterComponent() {
    }
    FofEntityFooterComponent.prototype.ngOnInit = function () {
    };
    FofEntityFooterComponent.ɵfac = function FofEntityFooterComponent_Factory(t) { return new (t || FofEntityFooterComponent)(); };
    FofEntityFooterComponent.ɵcmp = ɵɵdefineComponent({ type: FofEntityFooterComponent, selectors: [["fof-entity-footer"]], inputs: { entityBase: "entityBase" }, decls: 4, vars: 1, consts: [[1, "custom-card"], ["class", "row", 4, "ngIf"], ["elseBlock", ""], [1, "row"], [1, "col-md-6"], ["class", "col-md-6", 4, "ngIf", "ngIfElse"]], template: function FofEntityFooterComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "div", 0);
            ɵɵtemplate(1, FofEntityFooterComponent_div_1_Template, 5, 7, "div", 1);
            ɵɵtemplate(2, FofEntityFooterComponent_ng_template_2_Template, 2, 0, "ng-template", null, 2, ɵɵtemplateRefExtractor);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.entityBase);
        } }, directives: [NgIf], pipes: [DatePipe], styles: [".custom-card[_ngcontent-%COMP%]{font-size:smaller;padding-top:15px}"] });
    return FofEntityFooterComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofEntityFooterComponent, [{
        type: Component,
        args: [{
                selector: 'fof-entity-footer',
                templateUrl: './fof-entity-footer.component.html',
                styleUrls: ['./fof-entity-footer.component.scss']
            }]
    }], function () { return []; }, { entityBase: [{
            type: Input
        }] }); })();

function FofRoleComponent_div_11_mat_error_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le code du r\u00F4le est obligatoire et doit \u00EAtre compos\u00E9 de au moins 3 caract\u00E8res ");
    ɵɵelementEnd();
} }
function FofRoleComponent_div_11_mat_error_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " La description ne peut pas exc\u00E9der 200 caract\u00E8res ");
    ɵɵelementEnd();
} }
function FofRoleComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 10);
    ɵɵelementStart(1, "form", 11);
    ɵɵelementStart(2, "mat-form-field");
    ɵɵelement(3, "input", 12);
    ɵɵtemplate(4, FofRoleComponent_div_11_mat_error_4_Template, 2, 0, "mat-error", 13);
    ɵɵelementEnd();
    ɵɵelementStart(5, "mat-form-field");
    ɵɵelement(6, "textarea", 14);
    ɵɵelementStart(7, "mat-hint");
    ɵɵtext(8, "D\u00E9crivez succintement le r\u00F4le");
    ɵɵelementEnd();
    ɵɵelementStart(9, "mat-hint", 15);
    ɵɵtext(10);
    ɵɵelementEnd();
    ɵɵtemplate(11, FofRoleComponent_div_11_mat_error_11_Template, 2, 0, "mat-error", 13);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r206 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("formGroup", ctx_r206.uiVar.form);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r206.uiVar.form.get("code").invalid);
    ɵɵadvance(6);
    ɵɵtextInterpolate1("", ctx_r206.uiVar.form.get("description").value == null ? null : ctx_r206.uiVar.form.get("description").value.length, " / 200");
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r206.uiVar.form.get("description").invalid);
} }
function FofRoleComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 16);
    ɵɵelement(1, "mat-spinner", 17);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des roles et des authorisations...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template(rf, ctx) { if (rf & 1) {
    var _r218 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div");
    ɵɵelementStart(1, "mat-checkbox", 21);
    ɵɵlistener("change", function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template_mat_checkbox_change_1_listener() { ɵɵrestoreView(_r218); var permission_r216 = ctx.$implicit; var ctx_r217 = ɵɵnextContext(3); return ctx_r217.uiAction.rolePermissionSave(permission_r216); })("ngModelChange", function FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template_mat_checkbox_ngModelChange_1_listener($event) { ɵɵrestoreView(_r218); var permission_r216 = ctx.$implicit; return permission_r216.checked = $event; });
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var permission_r216 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngModel", permission_r216.checked)("checked", permission_r216.checked);
    ɵɵadvance(1);
    ɵɵtextInterpolate(permission_r216.code);
} }
function FofRoleComponent_mat_card_13_mat_card_content_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-card-content");
    ɵɵtemplate(1, FofRoleComponent_mat_card_13_mat_card_content_4_div_1_Template, 3, 3, "div", 20);
    ɵɵelementEnd();
} if (rf & 2) {
    var group_r211 = ɵɵnextContext().$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", group_r211.children);
} }
function FofRoleComponent_mat_card_13_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-spinner", 22);
} }
function FofRoleComponent_mat_card_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-card", 18);
    ɵɵelementStart(1, "mat-card-header");
    ɵɵelementStart(2, "mat-card-title");
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtemplate(4, FofRoleComponent_mat_card_13_mat_card_content_4_Template, 2, 1, "mat-card-content", 13);
    ɵɵtemplate(5, FofRoleComponent_mat_card_13_ng_template_5_Template, 1, 0, "ng-template", null, 19, ɵɵtemplateRefExtractor);
    ɵɵelementEnd();
} if (rf & 2) {
    var group_r211 = ctx.$implicit;
    ɵɵadvance(3);
    ɵɵtextInterpolate(group_r211.code);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", group_r211.children);
} }
var FofRoleComponent = /** @class */ (function () {
    function FofRoleComponent(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        // All private variables
        this.priVar = {
            roleCode: undefined,
            permissionRoluUpdating: false
        };
        // All private functions
        this.privFunc = {
            roleLoad: function () {
                _this.uiVar.loading = true;
                forkJoin(_this.fofPermissionService.permission.getAll(), _this.fofPermissionService.role.getWithPermissionByRoleCode(_this.priVar.roleCode)).subscribe({
                    next: function (result) {
                        var permissions = result[0];
                        var currentRoles = result[1];
                        var currentRole = undefined;
                        var _permissions = [];
                        if (currentRoles.length > 0) {
                            currentRole = result[1][0];
                        }
                        else {
                            _this.fofNotificationService.error("Cet r\u00F4le n'existe pas");
                            _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                            return;
                        }
                        _this.uiVar.role = currentRole;
                        _this.uiVar.form.patchValue(_this.uiVar.role);
                        var _permission;
                        var previousNameRoot = undefined;
                        permissions.forEach(function (permission) {
                            //split to upper case
                            var names = permission.code.split(/(?=[A-Z])/);
                            var nameRoot = names[0];
                            var found = false;
                            var rolePermission;
                            if (currentRole) {
                                rolePermission = currentRole.rolePermissions.find(function (item) { return item.permissionId == permission.id; });
                                if (rolePermission) {
                                    found = true;
                                }
                            }
                            var permissionCode = permission.code.slice(nameRoot.length);
                            if (nameRoot === previousNameRoot) {
                                _permission.children.push({
                                    id: permission.id,
                                    checked: found,
                                    rolePermission: rolePermission,
                                    code: permissionCode
                                });
                            }
                            else {
                                previousNameRoot = nameRoot;
                                if (_permission) {
                                    _permissions.push(_permission);
                                }
                                _permission = {};
                                _permission.code = nameRoot;
                                _permission.children = [];
                                _permission.children.push({
                                    id: permission.id,
                                    checked: found,
                                    rolePermission: rolePermission,
                                    code: permissionCode
                                });
                            }
                        });
                        if (_permission) {
                            _permissions.push(_permission);
                        }
                        _this.uiVar.permissionGroups = _permissions;
                        _this.uiVar.loading = false;
                        // console.log('currentRole', currentRole)
                    },
                    complete: function () {
                    }
                });
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouveau rôle',
            permissionGroups: undefined,
            loading: false,
            roleIsNew: false,
            role: undefined,
            form: this.formBuilder.group({
                code: ['', [Validators.required, Validators.minLength(3)]],
                description: ['', [Validators.maxLength(200)]]
            })
        };
        // All actions shared with UI 
        this.uiAction = {
            roleSave: function () {
                var roleToSave = _this.uiVar.form.value;
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                if (roleToSave.code.toLowerCase() == 'new') {
                    _this.fofNotificationService.error('Désolé, ce code est réservé');
                    return;
                }
                if (_this.uiVar.roleIsNew) {
                    _this.fofPermissionService.role.create(roleToSave)
                        .toPromise()
                        .then(function (newRole) {
                        _this.fofNotificationService.success('Rôle sauvé', { mustDisappearAfter: 1000 });
                        _this.priVar.roleCode = newRole.code;
                        _this.uiVar.title = 'Modification de rôle';
                        _this.uiVar.roleIsNew = false;
                        _this.privFunc.roleLoad();
                    });
                }
                else {
                    roleToSave.id = _this.uiVar.role.id;
                    _this.fofPermissionService.role.update(roleToSave)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.success('Rôle sauvé', { mustDisappearAfter: 1000 });
                    });
                }
            },
            roleCancel: function () {
                _this.uiVar.permissionGroups = null;
                _this.privFunc.roleLoad();
            },
            roleDelete: function () {
                _this.fofDialogService.openYesNo({
                    question: 'Voulez vous vraiment supprimer le rôle ?'
                }).then(function (yes) {
                    if (yes) {
                        _this.fofPermissionService.role.delete(_this.uiVar.role)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Rôle supprimé');
                            _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        });
                    }
                });
            },
            rolePermissionSave: function (permission) {
                var rolePermission = permission.rolePermission;
                // The rolePermission must be deleted
                if (rolePermission) {
                    _this.fofPermissionService.rolePermission.delete(rolePermission)
                        .toPromise()
                        .then(function (result) {
                        permission.rolePermission = null;
                        _this.fofNotificationService.saveIsDone();
                    });
                }
                else {
                    // The rolePermission must be created
                    var rolePermissionToSave = {
                        permissionId: permission.id,
                        roleId: _this.uiVar.role.id
                    };
                    _this.fofPermissionService.rolePermission.create(rolePermissionToSave)
                        .toPromise()
                        .then(function (result) {
                        _this.priVar.permissionRoluUpdating = false;
                        permission.rolePermission = result;
                        _this.fofNotificationService.saveIsDone();
                    });
                }
            }
        };
    }
    // Angular events
    FofRoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.paramMap.subscribe(function (params) {
            var code = params.get('code');
            _this.priVar.roleCode = code;
            if (code) {
                if (code.toLowerCase() == 'new') {
                    _this.uiVar.roleIsNew = true;
                }
                else {
                    _this.uiVar.title = 'Modification de rôle';
                    _this.privFunc.roleLoad();
                }
            }
        });
    };
    FofRoleComponent.ɵfac = function FofRoleComponent_Factory(t) { return new (t || FofRoleComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(ActivatedRoute), ɵɵdirectiveInject(FormBuilder), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(FofDialogService), ɵɵdirectiveInject(Router)); };
    FofRoleComponent.ɵcmp = ɵɵdefineComponent({ type: FofRoleComponent, selectors: [["fof-role"]], decls: 15, vars: 5, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "fof-fade-in detail", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], ["class", "group-card fof-fade-in", 4, "ngFor", "ngForOf"], [3, "entityBase"], [1, "fof-fade-in", "detail"], [3, "formGroup"], ["matInput", "", "required", "", "formControlName", "code", "placeholder", "Code du r\u00F4le", "value", ""], [4, "ngIf"], ["matInput", "", "formControlName", "description", "rows", "3", "placeholder", "Description"], ["align", "end"], [1, "fof-loading"], ["diameter", "20"], [1, "group-card", "fof-fade-in"], ["loading", ""], [4, "ngFor", "ngForOf"], [3, "ngModel", "checked", "change", "ngModelChange"], ["diameter", "30"]], template: function FofRoleComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-card", 0);
            ɵɵelementStart(1, "h3");
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelementStart(3, "div", 1);
            ɵɵelementStart(4, "button", 2);
            ɵɵlistener("click", function FofRoleComponent_Template_button_click_4_listener() { return ctx.uiAction.roleCancel(); });
            ɵɵtext(5, "Annuler");
            ɵɵelementEnd();
            ɵɵelementStart(6, "button", 3);
            ɵɵlistener("click", function FofRoleComponent_Template_button_click_6_listener() { return ctx.uiAction.roleDelete(); });
            ɵɵtext(7, "Supprimer");
            ɵɵelementEnd();
            ɵɵelementStart(8, "button", 4);
            ɵɵlistener("click", function FofRoleComponent_Template_button_click_8_listener() { return ctx.uiAction.roleSave(); });
            ɵɵtext(9, " Enregister");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(10, "div", 5);
            ɵɵtemplate(11, FofRoleComponent_div_11_Template, 12, 4, "div", 6);
            ɵɵtemplate(12, FofRoleComponent_div_12_Template, 4, 0, "div", 7);
            ɵɵtemplate(13, FofRoleComponent_mat_card_13_Template, 7, 2, "mat-card", 8);
            ɵɵelementEnd();
            ɵɵelement(14, "fof-entity-footer", 9);
        } if (rf & 2) {
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.uiVar.title);
            ɵɵadvance(9);
            ɵɵproperty("ngIf", !ctx.uiVar.loading);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.loading);
            ɵɵadvance(1);
            ɵɵproperty("ngForOf", ctx.uiVar.permissionGroups);
            ɵɵadvance(1);
            ɵɵproperty("entityBase", ctx.uiVar.role);
        } }, directives: [MatCard, MatButton, NgIf, NgForOf, FofEntityFooterComponent, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, MatFormField, MatInput, DefaultValueAccessor, RequiredValidator, NgControlStatus, FormControlName, MatHint, MatError, MatSpinner, MatCardHeader, MatCardTitle, MatCardContent, MatCheckbox, NgModel], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{margin:15px 15px 15px 0;width:18em}.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]   mat-card-title[_ngcontent-%COMP%]{text-transform:uppercase}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
    return FofRoleComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofRoleComponent, [{
        type: Component,
        args: [{
                selector: 'fof-role',
                templateUrl: './fof-role.component.html',
                styleUrls: ['./fof-role.component.scss'],
            }]
    }], function () { return [{ type: FofPermissionService }, { type: ActivatedRoute }, { type: FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: Router }]; }, null); })();

var FofErrorService = /** @class */ (function () {
    function FofErrorService(fofNotificationService) {
        this.fofNotificationService = fofNotificationService;
    }
    /** Only for the core service, the error should be already cleaned */
    FofErrorService.prototype.cleanError = function (errorToManage) {
        var errorToReturn = errorToManage;
        var message;
        var isConstraintError = false;
        // console.log('errorToManage', errorToManage)
        if (errorToManage.error instanceof ErrorEvent) {
            // client-side error
            console.log('toDo: manage error');
            console.log('errorToManage', errorToManage);
            return errorToManage;
        }
        else {
            // HTTP or server-side error
            if (errorToManage.error) {
                if (errorToManage.error instanceof Object) {
                    if (errorToManage.error.fofCoreException) {
                        if (errorToManage.error.message instanceof Object) {
                            // it's an http rest error
                            var _error = errorToManage.error.message;
                            if (_error.error) {
                                errorToManage.error.statusText = _error.error;
                                errorToManage.error.message = _error.message;
                            }
                        }
                        else {
                            message = errorToManage.error.message;
                            if (message.search('constraint') > -1) {
                                isConstraintError = true;
                            }
                        }
                        return __assign({ isConstraintError: isConstraintError }, errorToManage.error);
                    }
                    else {
                        if (errorToManage.status == 0) {
                            console.error('FOF-UNKNOWN-ERROR', errorToManage.error);
                            errorToManage.error = errorToManage.message;
                            errorToManage.message = 'Impossible to contact the server';
                        }
                    }
                }
                else {
                    // the error to manage is comming from the httpClient
                    return errorToManage;
                }
            }
        }
        return errorToReturn;
    };
    FofErrorService.prototype.errorManage = function (errorToManage) {
        var message = errorToManage.message;
        if (errorToManage.error) {
            message = message + ("<br>" + errorToManage.error);
        }
        if (errorToManage.status) {
            switch (errorToManage.status) {
                case 401:
                case 403:
                    message = "Vous n'avez pas le droit d'acc\u00E9der \u00E0 ces ressources";
                    break;
                default:
                    message = "Oups, nous avons une erreur<br>\n          <small>" + message + "</small><br>";
                    break;
            }
        }
        this.fofNotificationService.error(message, { mustDisappearAfter: -1 });
    };
    FofErrorService.ɵfac = function FofErrorService_Factory(t) { return new (t || FofErrorService)(ɵɵinject(FofNotificationService)); };
    FofErrorService.ɵprov = ɵɵdefineInjectable({ token: FofErrorService, factory: FofErrorService.ɵfac, providedIn: 'root' });
    return FofErrorService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofErrorService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: FofNotificationService }]; }, null); })();

function FofUsersComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 21);
    ɵɵelement(1, "mat-spinner", 22);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des utilisateurs...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function FofUsersComponent_th_24_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Email");
    ɵɵelementEnd();
} }
function FofUsersComponent_td_25_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r232 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r232.email);
} }
function FofUsersComponent_th_27_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Login");
    ɵɵelementEnd();
} }
function FofUsersComponent_td_28_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r233 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r233.login);
} }
function FofUsersComponent_th_30_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Pr\u00E9nom");
    ɵɵelementEnd();
} }
function FofUsersComponent_td_31_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r234 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r234.firstName);
} }
function FofUsersComponent_th_33_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Nom");
    ɵɵelementEnd();
} }
function FofUsersComponent_td_34_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r235 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r235.lastName);
} }
function FofUsersComponent_tr_35_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 25);
} }
function FofUsersComponent_tr_36_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 26);
} if (rf & 2) {
    var row_r236 = ctx.$implicit;
    ɵɵproperty("routerLink", row_r236.id);
} }
var _c0$1 = function () { return [5, 10, 25, 100]; };
var FofUsersComponent = /** @class */ (function () {
    function FofUsersComponent(fofPermissionService, fofNotificationService, breakpointObserver, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
            filter: undefined,
            filterOrganizationsChange: new EventEmitter(),
            filterOrganizations: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: [],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true,
            searchUsersCtrl: new FormControl()
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: function (organization) {
                if (organization) {
                    _this.priVar.filterOrganizations = [organization.id];
                }
                else {
                    _this.priVar.filterOrganizations = null;
                }
                _this.priVar.filterOrganizationsChange.emit(organization);
            }
        };
    }
    // Angular events
    FofUsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.filter = this.uiVar.searchUsersCtrl.valueChanges
            .pipe(debounceTime(500), distinctUntilChanged(), filter(function (query) { return query.length >= 3 || query.length === 0; }));
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['email', 'login'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['email', 'login', 'firstName', 'lastName'];
            }
        });
    };
    FofUsersComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page, this.priVar.filter, this.priVar.filterOrganizationsChange)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.user.search(_this.uiVar.searchUsersCtrl.value, _this.priVar.filterOrganizations, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function (error) {
            _this.fofErrorService.errorManage(error);
            _this.uiVar.isLoadingResults = false;
            return of([]);
        })).subscribe(function (data) {
            _this.uiVar.data = data;
        });
    };
    FofUsersComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    FofUsersComponent.ɵfac = function FofUsersComponent_Factory(t) { return new (t || FofUsersComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(BreakpointObserver), ɵɵdirectiveInject(FofErrorService)); };
    FofUsersComponent.ɵcmp = ɵɵdefineComponent({ type: FofUsersComponent, selectors: [["fof-core-fof-users"]], viewQuery: function FofUsersComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(MatPaginator, true);
            ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.paginator = _t.first);
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 38, vars: 17, consts: [[1, "row"], [1, "col-md-3"], [1, "card-tree"], [3, "multiSelect", "selectedOrganizationsChange"], [1, "col-md-9"], [1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "filtres"], ["autofocus", "", "matInput", "", "placeholder", "email ou nom ou pr\u00E9nom ou login", 3, "formControl"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "email", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "email"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "login"], ["matColumnDef", "firstName"], ["matColumnDef", "lastName"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function FofUsersComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "div", 0);
            ɵɵelementStart(1, "div", 1);
            ɵɵelementStart(2, "mat-card", 2);
            ɵɵelementStart(3, "fof-organizations-tree", 3);
            ɵɵlistener("selectedOrganizationsChange", function FofUsersComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(4, "div", 4);
            ɵɵelementStart(5, "mat-card", 5);
            ɵɵelementStart(6, "h3");
            ɵɵtext(7);
            ɵɵpipe(8, "translate");
            ɵɵelementEnd();
            ɵɵelementStart(9, "a", 6);
            ɵɵelementStart(10, "span");
            ɵɵtext(11);
            ɵɵpipe(12, "translate");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(13, "mat-card", 7);
            ɵɵelementStart(14, "mat-form-field");
            ɵɵelementStart(15, "mat-label");
            ɵɵtext(16, "Filtre");
            ɵɵelementEnd();
            ɵɵelement(17, "input", 8);
            ɵɵelementStart(18, "mat-hint");
            ɵɵtext(19, "Recherche \u00E0 partir de 3 caract\u00E8res saisies");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(20, "div", 9);
            ɵɵtemplate(21, FofUsersComponent_div_21_Template, 4, 0, "div", 10);
            ɵɵelementStart(22, "table", 11);
            ɵɵelementContainerStart(23, 12);
            ɵɵtemplate(24, FofUsersComponent_th_24_Template, 2, 0, "th", 13);
            ɵɵtemplate(25, FofUsersComponent_td_25_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(26, 15);
            ɵɵtemplate(27, FofUsersComponent_th_27_Template, 2, 0, "th", 13);
            ɵɵtemplate(28, FofUsersComponent_td_28_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(29, 16);
            ɵɵtemplate(30, FofUsersComponent_th_30_Template, 2, 0, "th", 13);
            ɵɵtemplate(31, FofUsersComponent_td_31_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(32, 17);
            ɵɵtemplate(33, FofUsersComponent_th_33_Template, 2, 0, "th", 13);
            ɵɵtemplate(34, FofUsersComponent_td_34_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵtemplate(35, FofUsersComponent_tr_35_Template, 1, 0, "tr", 18);
            ɵɵtemplate(36, FofUsersComponent_tr_36_Template, 1, 1, "tr", 19);
            ɵɵelementEnd();
            ɵɵelement(37, "mat-paginator", 20);
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(3);
            ɵɵproperty("multiSelect", false);
            ɵɵadvance(4);
            ɵɵtextInterpolate(ɵɵpipeBind1(8, 12, "admin.users.title-main"));
            ɵɵadvance(2);
            ɵɵproperty("routerLink", "/admin/users/new");
            ɵɵadvance(2);
            ɵɵtextInterpolate(ɵɵpipeBind1(12, 14, "admin.users.btn-user-add"));
            ɵɵadvance(6);
            ɵɵproperty("formControl", ctx.uiVar.searchUsersCtrl);
            ɵɵadvance(4);
            ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            ɵɵadvance(1);
            ɵɵproperty("dataSource", ctx.uiVar.data);
            ɵɵadvance(13);
            ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", ɵɵpureFunction0(16, _c0$1))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [MatCard, FofOrganizationsTreeComponent, MatAnchor, RouterLinkWithHref, MatFormField, MatLabel, MatInput, DefaultValueAccessor, NgControlStatus, FormControlDirective, MatHint, NgIf, MatTable, MatSort, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatPaginator, MatSpinner, MatHeaderCell, MatSortHeader, MatCell, MatHeaderRow, MatRow, RouterLink], pipes: [TranslatePipe], styles: [".filtres[_ngcontent-%COMP%]{margin-bottom:15px}.filtres[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.card-tree[_ngcontent-%COMP%]{height:100%}"] });
    return FofUsersComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofUsersComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-users',
                templateUrl: './fof-users.component.html',
                styleUrls: ['./fof-users.component.scss']
            }]
    }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: BreakpointObserver }, { type: FofErrorService }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();

function FofUserRolesSelectComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 6);
    ɵɵelement(1, "mat-spinner", 7);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Rafraichissement des r\u00F4les...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function FofUserRolesSelectComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    var _r241 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 8);
    ɵɵelementStart(1, "mat-checkbox", 9);
    ɵɵlistener("change", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_change_1_listener() { ɵɵrestoreView(_r241); var role_r239 = ctx.$implicit; var ctx_r240 = ɵɵnextContext(); return ctx_r240.uiAction.userRoleOrganizationSave(role_r239); })("ngModelChange", function FofUserRolesSelectComponent_div_7_Template_mat_checkbox_ngModelChange_1_listener($event) { ɵɵrestoreView(_r241); var role_r239 = ctx.$implicit; return role_r239.checked = $event; });
    ɵɵtext(2);
    ɵɵelementEnd();
    ɵɵelementStart(3, "div", 10);
    ɵɵtext(4);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var role_r239 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngModel", role_r239.checked)("checked", role_r239.checked);
    ɵɵadvance(1);
    ɵɵtextInterpolate(role_r239.code);
    ɵɵadvance(2);
    ɵɵtextInterpolate(role_r239.description);
} }
var FofUserRolesSelectComponent = /** @class */ (function () {
    function FofUserRolesSelectComponent(matDialogRef, fofPermissionService, fofNotificationService, fofErrorService, data) {
        var _this = this;
        this.matDialogRef = matDialogRef;
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.fofErrorService = fofErrorService;
        this.userRoleOrganizations = undefined;
        // All private variables
        this.priVar = {
            userId: undefined,
            organizationId: undefined
        };
        // All private functions
        this.privFunc = {
            componenentDataRefresh: function () {
                if (_this.userRoleOrganizations) {
                    // it's an update
                    _this.uiVar.isCreation = false;
                    _this.uiVar.title = "Modification des acc\u00E8s";
                    // Only one organization possible in update
                    _this.uiVar.organizationMultipleSelect = false;
                    if (_this.userRoleOrganizations.length > 0) {
                        // we get only the first one since it's impossible to have mutiple organisations
                        _this.priVar.organizationId = _this.userRoleOrganizations[0].organization.id;
                        _this.uiVar.selectedOrganisations = [];
                        _this.uiVar.selectedOrganisations.push(_this.userRoleOrganizations[0].organization);
                    }
                    if (_this.uiVar.allRoles) {
                        _this.uiVar.allRoles.forEach(function (role) {
                            var userRoleOrganization = _this.userRoleOrganizations.find(function (item) { return item.roleId == role.id; });
                            if (userRoleOrganization) {
                                role.userRoleOrganization = userRoleOrganization;
                                role.checked = true;
                            }
                        });
                    }
                }
                else {
                    // it's a creation
                    _this.uiVar.isCreation = true;
                    _this.uiVar.title = "Creation d'un acc\u00E8s";
                }
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            allRoles: undefined,
            organizationMultipleSelect: true,
            selectedOrganisations: [],
            title: "Creation d'un acc\u00E8s",
            isCreation: false,
            loading: false
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: function (organisations) {
                _this.uiVar.selectedOrganisations = organisations;
            },
            userRoleOrganizationSave: function (role) {
                var userRoleOrganization = role.userRoleOrganization;
                // if (userRoleOrganization) {
                //   this.fofPermissionService.userRoleOrganization.delete(userRoleOrganization)
                //   .toPromise()
                //   .then((result: any) => {          
                //     role.userRoleOrganization = null          
                //     this.fofNotificationService.saveIsDone()
                //   })      
                // } else {
                //   // The userRoleOrganization must be created
                //   const userRoleOrganizationToSave: iUserRoleOrganization = {        
                //     roleId: role.id,
                //     userId: this.uiVar.user.id
                //   }
                //   this.fofPermissionService.userRoleOrganization.create(userRoleOrganizationToSave)
                //   .toPromise()
                //   .then((result: any) => {          
                //     role.userRoleOrganization = result          
                //     this.fofNotificationService.saveIsDone()
                //   })      
                // }
            },
            save: function () {
                var userRoleOrganizations = [];
                var userRoleOrganization;
                if (!_this.priVar.userId) {
                    _this.fofNotificationService.error("\n          Probl\u00E8me avec les donn\u00E9es<br>\n          <small>Manque le user</small>\n        ");
                }
                _this.uiVar.selectedOrganisations.forEach(function (organisation) {
                    _this.uiVar.allRoles.forEach(function (role) {
                        if (role.checked) {
                            userRoleOrganization = {
                                userId: _this.priVar.userId,
                                organizationId: organisation.id,
                                roleId: role.id
                            };
                            userRoleOrganizations.push(userRoleOrganization);
                        }
                    });
                });
                if (userRoleOrganizations.length === 0) {
                    _this.fofNotificationService.error("\n          Vous devez s\u00E9lectionner au moins<br>\n          <ul>\n            <li>Une organisation</li>\n            <li>Un role</li>\n          </ul>\n        ", { mustDisappearAfter: -1 });
                    return;
                }
                _this.uiVar.loading = true;
                if (_this.uiVar.isCreation) {
                    _this.fofPermissionService.userRoleOrganization.bulkCreate(userRoleOrganizations)
                        .toPromise()
                        .then(function (result) {
                        _this.matDialogRef.close({ saved: true });
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                        // this.fofNotificationService.error(`
                        //   Erreur lors de la sauvegarde<br>
                        //   <small>${reason}</small>
                        // `)
                    })
                        .finally(function () {
                        _this.uiVar.loading = false;
                    });
                }
                else {
                    _this.fofPermissionService.user
                        .replaceUserRoleOrganization(userRoleOrganizations, _this.priVar.userId, _this.priVar.organizationId)
                        .toPromise()
                        .then(function (result) {
                        _this.matDialogRef.close({ saved: true });
                    })
                        .catch(function (reason) {
                        _this.fofNotificationService.error("\n            Erreur lors de la sauvegarde<br>\n            <small>" + reason + "</small>\n          ");
                    })
                        .finally(function () {
                        _this.uiVar.loading = false;
                    });
                }
            }
        };
        this.userRoleOrganizations = data.userRoleOrganizations;
        this.uiVar.allRoles = data.roles;
        this.priVar.userId = data.userId;
        if (data.notSelectableOrganizations) {
            this.notSelectableOrganizations = data.notSelectableOrganizations;
        }
        if (this.uiVar.allRoles && this.uiVar.allRoles.length) {
            this.uiVar.allRoles.forEach(function (role) {
                role.checked = false;
            });
        }
        this.privFunc.componenentDataRefresh();
    }
    // Angular events
    FofUserRolesSelectComponent.prototype.ngOnInit = function () {
    };
    FofUserRolesSelectComponent.prototype.ngOnChanges = function () {
        this.privFunc.componenentDataRefresh();
    };
    FofUserRolesSelectComponent.ɵfac = function FofUserRolesSelectComponent_Factory(t) { return new (t || FofUserRolesSelectComponent)(ɵɵdirectiveInject(MatDialogRef), ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(FofErrorService), ɵɵdirectiveInject(MAT_DIALOG_DATA)); };
    FofUserRolesSelectComponent.ɵcmp = ɵɵdefineComponent({ type: FofUserRolesSelectComponent, selectors: [["fof-user-roles-select"]], inputs: { userRoleOrganizations: "userRoleOrganizations", notSelectableOrganizations: "notSelectableOrganizations" }, features: [ɵɵNgOnChangesFeature], decls: 13, vars: 6, consts: [["class", "fof-loading", 4, "ngIf"], ["mat-dialog-title", ""], [3, "multiSelect", "selectedOrganisations", "notSelectableOrganizations", "selectedOrganizationsChange"], ["class", "fof-fade-in", 4, "ngFor", "ngForOf"], ["mat-flat-button", "", "mat-dialog-close", ""], ["mat-flat-button", "", "color", "primary", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in"], [3, "ngModel", "checked", "change", "ngModelChange"], [1, "role-hint"]], template: function FofUserRolesSelectComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵtemplate(0, FofUserRolesSelectComponent_div_0_Template, 4, 0, "div", 0);
            ɵɵelementStart(1, "h2", 1);
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelementStart(3, "mat-dialog-content");
            ɵɵelementStart(4, "fof-core-fof-organizations-multi-select", 2);
            ɵɵlistener("selectedOrganizationsChange", function FofUserRolesSelectComponent_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_4_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
            ɵɵelementEnd();
            ɵɵelementStart(5, "h4");
            ɵɵtext(6, "R\u00F4les");
            ɵɵelementEnd();
            ɵɵtemplate(7, FofUserRolesSelectComponent_div_7_Template, 5, 4, "div", 3);
            ɵɵelementEnd();
            ɵɵelementStart(8, "mat-dialog-actions");
            ɵɵelementStart(9, "button", 4);
            ɵɵtext(10, "Annuler");
            ɵɵelementEnd();
            ɵɵelementStart(11, "button", 5);
            ɵɵlistener("click", function FofUserRolesSelectComponent_Template_button_click_11_listener() { return ctx.uiAction.save(); });
            ɵɵtext(12, "Enregistrer");
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵproperty("ngIf", ctx.uiVar.loading);
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.uiVar.title);
            ɵɵadvance(2);
            ɵɵproperty("multiSelect", ctx.uiVar.organizationMultipleSelect)("selectedOrganisations", ctx.uiVar.selectedOrganisations)("notSelectableOrganizations", ctx.notSelectableOrganizations);
            ɵɵadvance(3);
            ɵɵproperty("ngForOf", ctx.uiVar.allRoles);
        } }, directives: [NgIf, MatDialogTitle, MatDialogContent, FofOrganizationsMultiSelectComponent, NgForOf, MatDialogActions, MatButton, MatDialogClose, MatSpinner, MatCheckbox, NgControlStatus, NgModel], styles: ["[_nghost-%COMP%]{position:relative}[_nghost-%COMP%]   .fof-loading[_ngcontent-%COMP%]{position:absolute;height:calc(100% + 15px);z-index:2}[_nghost-%COMP%]   .mat-dialog-actions[_ngcontent-%COMP%]{margin-top:1rem;display:flex;justify-content:flex-end}"] });
    return FofUserRolesSelectComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofUserRolesSelectComponent, [{
        type: Component,
        args: [{
                selector: 'fof-user-roles-select',
                templateUrl: './fof-user-roles-select.component.html',
                styleUrls: ['./fof-user-roles-select.component.scss']
            }]
    }], function () { return [{ type: MatDialogRef }, { type: FofPermissionService }, { type: FofNotificationService }, { type: FofErrorService }, { type: undefined, decorators: [{
                type: Inject,
                args: [MAT_DIALOG_DATA]
            }] }]; }, { userRoleOrganizations: [{
            type: Input
        }], notSelectableOrganizations: [{
            type: Input
        }] }); })();

function FofUserComponent_div_11_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le login ne doit pas exc\u00E9der 30 caract\u00E8res ");
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Un email valide est obligatoire ");
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le pr\u00E9nom ne doit pas exc\u00E9der 30 caract\u00E8res ");
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_mat_error_15_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le nom de famille ne doit pas exc\u00E9der 30 caract\u00E8res ");
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_div_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 33);
    ɵɵelement(1, "mat-spinner", 34);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Rafraichissement des r\u00F4les...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_th_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "th", 35);
} }
function FofUserComponent_div_11_div_17_td_15_Template(rf, ctx) { if (rf & 1) {
    var _r263 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "td", 36);
    ɵɵelementStart(1, "mat-checkbox", 37);
    ɵɵlistener("ngModelChange", function FofUserComponent_div_11_div_17_td_15_Template_mat_checkbox_ngModelChange_1_listener($event) { ɵɵrestoreView(_r263); var row_r261 = ctx.$implicit; return row_r261.checked = $event; });
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r261 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngModel", row_r261.checked);
} }
function FofUserComponent_div_11_div_17_th_17_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 35);
    ɵɵtext(1, "Organisation");
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_td_18_Template(rf, ctx) { if (rf & 1) {
    var _r266 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "td", 38);
    ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_18_Template_td_click_0_listener() { ɵɵrestoreView(_r266); var row_r264 = ctx.$implicit; var ctx_r265 = ɵɵnextContext(3); return ctx_r265.uiAction.roleSelectComponentOpen(row_r264); });
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r264 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", row_r264.organization, "");
} }
function FofUserComponent_div_11_div_17_th_20_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 35);
    ɵɵtext(1, "Roles");
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_td_21_div_1_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var role_r269 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate1(" ", role_r269, " ");
} }
function FofUserComponent_div_11_div_17_td_21_Template(rf, ctx) { if (rf & 1) {
    var _r271 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "td", 38);
    ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_21_Template_td_click_0_listener() { ɵɵrestoreView(_r271); var row_r267 = ctx.$implicit; var ctx_r270 = ɵɵnextContext(3); return ctx_r270.uiAction.roleSelectComponentOpen(row_r267); });
    ɵɵtemplate(1, FofUserComponent_div_11_div_17_td_21_div_1_Template, 2, 1, "div", 39);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r267 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngForOf", row_r267.roles);
} }
function FofUserComponent_div_11_div_17_th_23_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "th", 35);
} }
function FofUserComponent_div_11_div_17_td_24_Template(rf, ctx) { if (rf & 1) {
    var _r274 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "td", 40);
    ɵɵlistener("click", function FofUserComponent_div_11_div_17_td_24_Template_td_click_0_listener() { ɵɵrestoreView(_r274); var row_r272 = ctx.$implicit; var ctx_r273 = ɵɵnextContext(3); return ctx_r273.uiAction.roleSelectComponentOpen(row_r272); });
    ɵɵelementStart(1, "mat-icon");
    ɵɵtext(2, "chevron_right");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function FofUserComponent_div_11_div_17_tr_25_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 41);
} }
function FofUserComponent_div_11_div_17_tr_26_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 42);
} }
function FofUserComponent_div_11_div_17_Template(rf, ctx) { if (rf & 1) {
    var _r277 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 11);
    ɵɵelementStart(1, "mat-card");
    ɵɵelementStart(2, "div", 0);
    ɵɵelementStart(3, "h4");
    ɵɵtext(4, "R\u00F4les");
    ɵɵelementEnd();
    ɵɵelementStart(5, "div", 1);
    ɵɵelementStart(6, "button", 3);
    ɵɵlistener("click", function FofUserComponent_div_11_div_17_Template_button_click_6_listener() { ɵɵrestoreView(_r277); var ctx_r276 = ɵɵnextContext(2); return ctx_r276.uiAction.userRolesDelete(); });
    ɵɵtext(7, "Supprimer r\u00F4les");
    ɵɵelementEnd();
    ɵɵelementStart(8, "button", 2);
    ɵɵlistener("click", function FofUserComponent_div_11_div_17_Template_button_click_8_listener() { ɵɵrestoreView(_r277); var ctx_r278 = ɵɵnextContext(2); return ctx_r278.uiAction.roleSelectComponentOpen(); });
    ɵɵtext(9, "Ajouter un r\u00F4le");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(10, "div", 20);
    ɵɵtemplate(11, FofUserComponent_div_11_div_17_div_11_Template, 4, 0, "div", 21);
    ɵɵelementStart(12, "table", 22);
    ɵɵelementContainerStart(13, 23);
    ɵɵtemplate(14, FofUserComponent_div_11_div_17_th_14_Template, 1, 0, "th", 24);
    ɵɵtemplate(15, FofUserComponent_div_11_div_17_td_15_Template, 2, 1, "td", 25);
    ɵɵelementContainerEnd();
    ɵɵelementContainerStart(16, 26);
    ɵɵtemplate(17, FofUserComponent_div_11_div_17_th_17_Template, 2, 0, "th", 24);
    ɵɵtemplate(18, FofUserComponent_div_11_div_17_td_18_Template, 2, 1, "td", 27);
    ɵɵelementContainerEnd();
    ɵɵelementContainerStart(19, 28);
    ɵɵtemplate(20, FofUserComponent_div_11_div_17_th_20_Template, 2, 0, "th", 24);
    ɵɵtemplate(21, FofUserComponent_div_11_div_17_td_21_Template, 2, 1, "td", 27);
    ɵɵelementContainerEnd();
    ɵɵelementContainerStart(22, 29);
    ɵɵtemplate(23, FofUserComponent_div_11_div_17_th_23_Template, 1, 0, "th", 24);
    ɵɵtemplate(24, FofUserComponent_div_11_div_17_td_24_Template, 3, 0, "td", 30);
    ɵɵelementContainerEnd();
    ɵɵtemplate(25, FofUserComponent_div_11_div_17_tr_25_Template, 1, 0, "tr", 31);
    ɵɵtemplate(26, FofUserComponent_div_11_div_17_tr_26_Template, 1, 0, "tr", 32);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r249 = ɵɵnextContext(2);
    ɵɵadvance(11);
    ɵɵproperty("ngIf", ctx_r249.uiVar.loadingRoles);
    ɵɵadvance(1);
    ɵɵproperty("dataSource", ctx_r249.uiVar.userRolesToDisplay);
    ɵɵadvance(13);
    ɵɵproperty("matHeaderRowDef", ctx_r249.uiVar.roleDisplayedColumns);
    ɵɵadvance(1);
    ɵɵproperty("matRowDefColumns", ctx_r249.uiVar.roleDisplayedColumns);
} }
function FofUserComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    var _r280 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 9);
    ɵɵelementStart(1, "div", 10);
    ɵɵelementStart(2, "div", 11);
    ɵɵelementStart(3, "form", 12);
    ɵɵelementStart(4, "mat-form-field");
    ɵɵelement(5, "input", 13);
    ɵɵtemplate(6, FofUserComponent_div_11_mat_error_6_Template, 2, 0, "mat-error", 14);
    ɵɵelementEnd();
    ɵɵelementStart(7, "mat-form-field");
    ɵɵelement(8, "input", 15);
    ɵɵtemplate(9, FofUserComponent_div_11_mat_error_9_Template, 2, 0, "mat-error", 14);
    ɵɵelementEnd();
    ɵɵelementStart(10, "mat-form-field");
    ɵɵelement(11, "input", 16);
    ɵɵtemplate(12, FofUserComponent_div_11_mat_error_12_Template, 2, 0, "mat-error", 14);
    ɵɵelementEnd();
    ɵɵelementStart(13, "mat-form-field");
    ɵɵelement(14, "input", 17);
    ɵɵtemplate(15, FofUserComponent_div_11_mat_error_15_Template, 2, 0, "mat-error", 14);
    ɵɵelementEnd();
    ɵɵelementStart(16, "fof-core-fof-organizations-multi-select", 18);
    ɵɵlistener("selectedOrganizationsChange", function FofUserComponent_div_11_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_16_listener($event) { ɵɵrestoreView(_r280); var ctx_r279 = ɵɵnextContext(); return ctx_r279.uiAction.organisationMultiSelectedChange($event); });
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtemplate(17, FofUserComponent_div_11_div_17_Template, 27, 4, "div", 19);
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r243 = ɵɵnextContext();
    ɵɵadvance(3);
    ɵɵproperty("formGroup", ctx_r243.uiVar.form);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("login").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("email").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("firstName").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r243.uiVar.form.get("lastName").invalid);
    ɵɵadvance(1);
    ɵɵproperty("placeHolder", "Organisation de rattachement")("multiSelect", false)("selectedOrganisations", ctx_r243.uiVar.userOrganizationsDisplay);
    ɵɵadvance(1);
    ɵɵproperty("ngIf", ctx_r243.uiVar.allRoles);
} }
function FofUserComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 43);
    ɵɵelement(1, "mat-spinner", 34);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des utilisateurs et des authorisations...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
var FofUserComponent = /** @class */ (function () {
    function FofUserComponent(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, matDialog, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        this.matDialog = matDialog;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            userId: undefined,
            userOrganizationId: undefined,
            organizationAlreadyWithRoles: undefined
        };
        // All private functions
        this.privFunc = {
            userLoad: function () {
                _this.uiVar.loadingUser = true;
                Promise.all([
                    _this.fofPermissionService.role.getAll().toPromise(),
                    _this.fofPermissionService.user.getWithRoleById(_this.priVar.userId).toPromise()
                ])
                    .then(function (result) {
                    var roles = result[0];
                    var currentUsers = result[1];
                    console.log('currentUsers', currentUsers);
                    _this.uiVar.allRoles = roles;
                    _this.privFunc.userRefresh(currentUsers);
                    // Here for preventing to refresh the user form when refreshing only the roles.
                    // toDo: improve
                    _this.uiVar.form.patchValue(_this.uiVar.user);
                })
                    .catch(function (reason) {
                    _this.fofErrorService.errorManage(reason);
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                })
                    .finally(function () {
                    _this.uiVar.loadingUser = false;
                });
            },
            userRefresh: function (currentUser) {
                if (!currentUser) {
                    _this.fofNotificationService.error("Ce utilisateur n'existe pas");
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                    return;
                }
                _this.uiVar.user = currentUser;
                if (currentUser.organizationId) {
                    _this.uiVar.userOrganizationsDisplay = [{ id: currentUser.organizationId }];
                    _this.priVar.userOrganizationId = currentUser.organizationId;
                }
                var _previousOrganisationId = undefined;
                var _roleToDisplay = undefined;
                _this.uiVar.userRolesToDisplay = [];
                if (currentUser.userRoleOrganizations && currentUser.userRoleOrganizations.length > 0) {
                    // we will display the user roles grouped by organization 
                    // with one on several roles on it
                    currentUser.userRoleOrganizations.forEach(function (uro) {
                        if (_previousOrganisationId !== uro.organization.id) {
                            // it's a new organisation, reinit
                            _previousOrganisationId = uro.organization.id;
                            if (_roleToDisplay) {
                                _this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                            }
                            _roleToDisplay = {
                                organizationId: uro.organization.id,
                                organization: uro.organization.name,
                                roles: [],
                                userRoleOrganizations: []
                            };
                        }
                        _roleToDisplay.roles.push(uro.role.code);
                        _roleToDisplay.userRoleOrganizations.push(uro);
                    });
                }
                // add the last one
                if (_roleToDisplay) {
                    _this.uiVar.userRolesToDisplay.push(_roleToDisplay);
                }
                // we don't want the following organization could be selectebale in the 
                // organization tree
                _this.priVar.organizationAlreadyWithRoles = [];
                _this.uiVar.userRolesToDisplay.forEach(function (org) {
                    _this.priVar.organizationAlreadyWithRoles.push({
                        id: org.organizationId,
                        name: org.organization
                    });
                });
            },
            useRolesRefresh: function () {
                _this.uiVar.loadingRoles = true;
                _this.fofPermissionService.user.getWithRoleById(_this.priVar.userId)
                    .toPromise()
                    .then(function (usersResult) {
                    _this.privFunc.userRefresh(usersResult);
                })
                    .catch(function (reason) {
                    _this.fofErrorService.errorManage(reason);
                    // this.router.navigate(['../'], {relativeTo: this.activatedRoute})
                })
                    .finally(function () {
                    _this.uiVar.loadingRoles = false;
                });
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouvel utilisateur',
            loadingUser: false,
            loadingRoles: false,
            userIsNew: false,
            user: undefined,
            userOrganizationsDisplay: undefined,
            form: this.formBuilder.group({
                login: ['', [Validators.maxLength(30)]],
                email: ['', [Validators.required, Validators.email, Validators.maxLength(60)]],
                firstName: ['', [Validators.maxLength(30)]],
                lastName: ['', [Validators.maxLength(30)]]
            }),
            allRoles: undefined,
            selectedUserRoleOrganizations: undefined,
            userRolesToDisplay: [],
            roleDisplayedColumns: ['delete', 'organization', 'roles', 'icon'],
            rolesAll: undefined
        };
        // All actions shared with UI 
        this.uiAction = {
            userSave: function () {
                var userToSave = _this.uiVar.form.value;
                userToSave.organizationId = _this.priVar.userOrganizationId;
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                if (_this.uiVar.userIsNew) {
                    _this.fofPermissionService.user.create(userToSave)
                        .toPromise()
                        .then(function (newUser) {
                        _this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                        _this.priVar.userId = newUser.id;
                        _this.uiVar.title = "Modification d'un utilisateur";
                        _this.uiVar.userIsNew = false;
                        _this.privFunc.userLoad();
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    userToSave.id = _this.uiVar.user.id;
                    _this.fofPermissionService.user.update(userToSave)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.success('Utilisateur sauvé', { mustDisappearAfter: 1000 });
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
            },
            userCancel: function () {
                _this.privFunc.userLoad();
            },
            userDelete: function () {
                _this.fofDialogService.openYesNo({
                    question: "Voulez vous vraiment supprimer l' utilisateur ?"
                }).then(function (yes) {
                    if (yes) {
                        _this.fofPermissionService.user.delete(_this.uiVar.user)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Utilisateur supprimé');
                            _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        });
                    }
                });
            },
            userRolesDelete: function () {
                var userRoles = _this.uiVar.userRolesToDisplay;
                var organizationsIdToDelete = [];
                var somethingToDelete = false;
                var message = "Voulez vous vraiment supprimer les r\u00F4les des organisations suivante ?<br>";
                message = message + '<ul>';
                userRoles.forEach(function (ur) {
                    //don't want to create an interface for 1 param)
                    if (ur['checked']) {
                        somethingToDelete = true;
                        message = message + ("<li>" + ur.organization + "</li>");
                        organizationsIdToDelete.push(ur.organizationId);
                    }
                });
                message = message + '</ul>';
                if (somethingToDelete) {
                    _this.fofDialogService.openYesNo({
                        title: 'Supprimer rôles',
                        question: message
                    }).then(function (yes) {
                        if (yes) {
                            _this.fofPermissionService.user.deleteUserRoleOrganizations(_this.uiVar.user.id, organizationsIdToDelete)
                                .toPromise()
                                .then(function (result) {
                                _this.fofNotificationService.success('Rôles supprimés');
                                _this.privFunc.useRolesRefresh();
                            });
                        }
                    });
                }
                else {
                    _this.fofNotificationService.info('Vous devez sélectionner ua moins une organisation');
                }
            },
            roleSelectComponentOpen: function (userRole) {
                var userRoleOrganizations;
                if (userRole) {
                    userRoleOrganizations = userRole.userRoleOrganizations;
                }
                var dialogRef = _this.matDialog.open(FofUserRolesSelectComponent, {
                    data: {
                        roles: _this.uiVar.allRoles,
                        userRoleOrganizations: userRoleOrganizations,
                        notSelectableOrganizations: _this.priVar.organizationAlreadyWithRoles,
                        userId: _this.uiVar.user.id
                    },
                    width: '600px',
                    height: 'calc(100vh - 200px)'
                });
                dialogRef.afterClosed()
                    .toPromise()
                    .then(function (mustBeRefresd) {
                    if (mustBeRefresd) {
                        _this.privFunc.useRolesRefresh();
                    }
                });
            },
            organisationMultiSelectedChange: function (organizations) {
                if (organizations && organizations.length > 0) {
                    _this.priVar.userOrganizationId = organizations[0].id;
                }
                else {
                    _this.priVar.userOrganizationId = null;
                }
            }
        };
    }
    // Angular events
    FofUserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.paramMap.subscribe(function (params) {
            var code = params.get('code');
            if (code) {
                if (code.toLowerCase() == 'new') {
                    _this.uiVar.userIsNew = true;
                }
                else {
                    _this.priVar.userId = code;
                    _this.uiVar.title = "Modification d'un utilisateur";
                    _this.privFunc.userLoad();
                }
            }
        });
    };
    FofUserComponent.ɵfac = function FofUserComponent_Factory(t) { return new (t || FofUserComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(ActivatedRoute), ɵɵdirectiveInject(FormBuilder), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(FofDialogService), ɵɵdirectiveInject(Router), ɵɵdirectiveInject(MatDialog), ɵɵdirectiveInject(FofErrorService)); };
    FofUserComponent.ɵcmp = ɵɵdefineComponent({ type: FofUserComponent, selectors: [["fof-core-fof-user"]], decls: 14, vars: 4, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "detail fof-fade-in", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [3, "entityBase"], [1, "detail", "fof-fade-in"], [1, "row"], [1, "col-md-6"], [3, "formGroup"], ["matInput", "", "formControlName", "login", "placeholder", "login", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "type", "email", "formControlName", "email", "placeholder", "Email", "value", ""], ["matInput", "", "required", "", "type", "firstName", "formControlName", "firstName", "placeholder", "Pr\u00E9nom", "value", ""], ["matInput", "", "required", "", "type", "lastName", "formControlName", "lastName", "placeholder", "Nom de famille", "value", ""], [3, "placeHolder", "multiSelect", "selectedOrganisations", "selectedOrganizationsChange"], ["class", "col-md-6", 4, "ngIf"], [1, "fof-table-container"], ["class", "fof-loading fof-loading-roles", 4, "ngIf"], ["mat-table", "", 1, "data-table", 3, "dataSource"], ["matColumnDef", "delete"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "organization"], ["mat-cell", "", 3, "click", 4, "matCellDef"], ["matColumnDef", "roles"], ["matColumnDef", "icon"], ["class", "icon-select", "mat-cell", "", 3, "click", 4, "matCellDef"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 4, "matRowDef", "matRowDefColumns"], [1, "fof-loading", "fof-loading-roles"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], [3, "ngModel", "ngModelChange"], ["mat-cell", "", 3, "click"], [4, "ngFor", "ngForOf"], ["mat-cell", "", 1, "icon-select", 3, "click"], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over"], [1, "fof-loading"]], template: function FofUserComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-card", 0);
            ɵɵelementStart(1, "h3");
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelementStart(3, "div", 1);
            ɵɵelementStart(4, "button", 2);
            ɵɵlistener("click", function FofUserComponent_Template_button_click_4_listener() { return ctx.uiAction.userCancel(); });
            ɵɵtext(5, "Annuler");
            ɵɵelementEnd();
            ɵɵelementStart(6, "button", 3);
            ɵɵlistener("click", function FofUserComponent_Template_button_click_6_listener() { return ctx.uiAction.userDelete(); });
            ɵɵtext(7, "Supprimer");
            ɵɵelementEnd();
            ɵɵelementStart(8, "button", 4);
            ɵɵlistener("click", function FofUserComponent_Template_button_click_8_listener() { return ctx.uiAction.userSave(); });
            ɵɵtext(9, " Enregister");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(10, "div", 5);
            ɵɵtemplate(11, FofUserComponent_div_11_Template, 18, 9, "div", 6);
            ɵɵtemplate(12, FofUserComponent_div_12_Template, 4, 0, "div", 7);
            ɵɵelementEnd();
            ɵɵelement(13, "fof-entity-footer", 8);
        } if (rf & 2) {
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.uiVar.title);
            ɵɵadvance(9);
            ɵɵproperty("ngIf", !ctx.uiVar.loadingUser);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.loadingUser);
            ɵɵadvance(1);
            ɵɵproperty("entityBase", ctx.uiVar.user);
        } }, directives: [MatCard, MatButton, NgIf, FofEntityFooterComponent, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, MatFormField, MatInput, DefaultValueAccessor, NgControlStatus, FormControlName, RequiredValidator, FofOrganizationsMultiSelectComponent, MatError, MatTable, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatSpinner, MatHeaderCell, MatCell, MatCheckbox, NgModel, NgForOf, MatIcon, MatHeaderRow, MatRow], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row;margin-bottom:15px}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .fof-loading-roles[_ngcontent-%COMP%]{position:absolute}.main[_ngcontent-%COMP%]   .role-hint[_ngcontent-%COMP%]{padding-left:25px;font-size:smaller}.main[_ngcontent-%COMP%]   .icon-select[_ngcontent-%COMP%]{text-align:right}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
    return FofUserComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofUserComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-user',
                templateUrl: './fof-user.component.html',
                styleUrls: ['./fof-user.component.scss']
            }]
    }], function () { return [{ type: FofPermissionService }, { type: ActivatedRoute }, { type: FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: Router }, { type: MatDialog }, { type: FofErrorService }]; }, null); })();

function FofOrganizationsComponent_button_9_Template(rf, ctx) { if (rf & 1) {
    var _r287 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 11);
    ɵɵlistener("click", function FofOrganizationsComponent_button_9_Template_button_click_0_listener() { ɵɵrestoreView(_r287); var ctx_r286 = ɵɵnextContext(); return ctx_r286.uiAction.organisationCancel(); });
    ɵɵtext(1, "Annuler");
    ɵɵelementEnd();
} }
function FofOrganizationsComponent_button_10_Template(rf, ctx) { if (rf & 1) {
    var _r289 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 12);
    ɵɵlistener("click", function FofOrganizationsComponent_button_10_Template_button_click_0_listener() { ɵɵrestoreView(_r289); var ctx_r288 = ɵɵnextContext(); return ctx_r288.uiAction.organisationDelete(); });
    ɵɵtext(1, "Supprimer");
    ɵɵelementEnd();
} }
function FofOrganizationsComponent_button_11_Template(rf, ctx) { if (rf & 1) {
    var _r291 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 13);
    ɵɵlistener("click", function FofOrganizationsComponent_button_11_Template_button_click_0_listener() { ɵɵrestoreView(_r291); var ctx_r290 = ɵɵnextContext(); return ctx_r290.uiAction.organisationSave(); });
    ɵɵtext(1, " Enregistrer");
    ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 14);
    ɵɵelement(1, "mat-spinner", 15);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des organisations...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_button_5_Template(rf, ctx) { if (rf & 1) {
    var _r295 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "button", 22);
    ɵɵlistener("click", function FofOrganizationsComponent_div_13_button_5_Template_button_click_0_listener() { ɵɵrestoreView(_r295); var ctx_r294 = ɵɵnextContext(2); return ctx_r294.uiAction.organisationAdd(); });
    ɵɵtext(1, "Ajouter une organisation enfant");
    ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le nom ne doit pas \u00EAtre nul et faire plus de 100 cararct\u00E8res ");
    ɵɵelementEnd();
} }
function FofOrganizationsComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 16);
    ɵɵelementStart(1, "div", 17);
    ɵɵelementStart(2, "h4");
    ɵɵtext(3);
    ɵɵelementEnd();
    ɵɵelementStart(4, "div", 5);
    ɵɵtemplate(5, FofOrganizationsComponent_div_13_button_5_Template, 2, 0, "button", 18);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(6, "form", 19);
    ɵɵelementStart(7, "mat-form-field");
    ɵɵelement(8, "input", 20);
    ɵɵtemplate(9, FofOrganizationsComponent_div_13_mat_error_9_Template, 2, 0, "mat-error", 21);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r285 = ɵɵnextContext();
    ɵɵadvance(3);
    ɵɵtextInterpolate(ctx_r285.uiVar.actionTitle);
    ɵɵadvance(2);
    ɵɵproperty("ngIf", !ctx_r285.uiVar.organizationIsNew);
    ɵɵadvance(1);
    ɵɵproperty("formGroup", ctx_r285.uiVar.form);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r285.uiVar.form.get("name").invalid);
} }
var FofOrganizationsComponent = /** @class */ (function () {
    function FofOrganizationsComponent(fofPermissionService, formBuilder, fofNotificationService, fofDialogService, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            currentNode: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            loading: false,
            organizationIsNew: false,
            nodeForm: false,
            form: this.formBuilder.group({
                name: ['', [Validators.required, Validators.maxLength(100)]],
                description: ['', [Validators.maxLength(200)]],
            }),
            actionTitle: "Modification de l'organisation",
            mainTitle: "G\u00E9rer l'organisation",
            nodeChanged: undefined,
            nodeToDelete: undefined
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationAdd: function () {
                if (!_this.priVar.currentNode) {
                    return;
                }
                _this.uiVar.form.reset();
                _this.uiVar.organizationIsNew = true;
                _this.uiVar.actionTitle = "Ajouter une organisation enfant";
            },
            organisationDelete: function () {
                var nodeToDelete = _this.priVar.currentNode;
                _this.fofDialogService.openYesNo({
                    title: "Supprimer une organisation",
                    question: "Voulez vous vraiment supprimer \n          " + nodeToDelete.name + " ?"
                }).then(function (yes) {
                    if (yes) {
                        _this.fofPermissionService.organization.delete(nodeToDelete)
                            .toPromise()
                            .then(function (result) {
                            _this.uiVar.nodeToDelete = nodeToDelete;
                        })
                            .catch(function (reason) {
                            if (reason.isConstraintError) {
                                _this.fofNotificationService.info("Vous ne pouvez pas supprimer l'organisation !<br>\n                <small>Vous devez supprimer tous les objets rattach\u00E9s \u00E0 l'organisation ou contacter un admninistrateur</small>", { mustDisappearAfter: -1 });
                            }
                            else {
                                _this.fofErrorService.errorManage(reason);
                            }
                        });
                    }
                });
                _this.uiVar.organizationIsNew = false;
            },
            organisationSave: function () {
                var nodeToSave = _this.uiVar.form.value;
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                // New organization
                if (_this.uiVar.organizationIsNew) {
                    nodeToSave.parentId = _this.priVar.currentNode.id;
                    _this.fofPermissionService.organization.create({
                        id: nodeToSave.id,
                        name: nodeToSave.name
                    })
                        .toPromise()
                        .then(function (nodeResult) {
                        _this.fofNotificationService.saveIsDone();
                        _this.priVar.currentNode.children.push(nodeResult);
                        // deep copy for pushing the tree component to refresh the object
                        _this.uiVar.nodeChanged = JSON.parse(JSON.stringify(_this.priVar.currentNode));
                        _this.uiAction.selectedOrganizationsChange(_this.priVar.currentNode);
                        _this.uiVar.organizationIsNew = false;
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    // Update an organization
                    _this.priVar.currentNode.name = nodeToSave.name;
                    _this.fofPermissionService.organization.update({
                        id: _this.priVar.currentNode.id,
                        name: _this.priVar.currentNode.name
                    })
                        .toPromise()
                        .then(function (nodeResult) {
                        _this.uiVar.nodeChanged = nodeResult;
                        _this.fofNotificationService.saveIsDone();
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
            },
            organisationCancel: function () {
                _this.uiVar.actionTitle = "Modification de l'organisation";
                _this.uiVar.nodeForm = false;
                _this.uiVar.organizationIsNew = false;
            },
            selectedOrganizationsChange: function (node) {
                if (node) {
                    _this.uiVar.nodeForm = node.checked;
                    _this.uiVar.form.patchValue(node);
                }
                else {
                    _this.uiVar.nodeForm = false;
                    _this.uiVar.form.reset();
                }
                if (node && node.checked) {
                    _this.priVar.currentNode = node;
                    _this.uiVar.mainTitle = node.name;
                }
                else {
                    _this.priVar.currentNode = null;
                    _this.uiVar.mainTitle = "G\u00E9rer l'organisation";
                }
            }
        };
    }
    // Angular events
    FofOrganizationsComponent.prototype.ngOnInit = function () {
    };
    FofOrganizationsComponent.ɵfac = function FofOrganizationsComponent_Factory(t) { return new (t || FofOrganizationsComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(FormBuilder), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(FofDialogService), ɵɵdirectiveInject(FofErrorService)); };
    FofOrganizationsComponent.ɵcmp = ɵɵdefineComponent({ type: FofOrganizationsComponent, selectors: [["fof-core-fof-organizations"]], decls: 14, vars: 8, consts: [[1, "row", "main"], [1, "col-md-6"], [1, "card-tree"], [3, "nodeChanged", "nodeToDelete", "selectedOrganizationsChange"], [1, "fof-header", "card-detail"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "warn", 3, "click", 4, "ngIf"], ["mat-stroked-button", "", "color", "accent", 3, "click", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], ["class", "fof-fade-in card-detail", 4, "ngIf"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "fof-loading"], ["diameter", "20"], [1, "fof-fade-in", "card-detail"], [1, "fof-header"], ["mat-stroked-button", "", "class", "child-add", "color", "primary", 3, "click", 4, "ngIf"], [3, "formGroup"], ["matInput", "", "required", "", "type", "name", "formControlName", "name", "placeholder", "Nom de l'organisation", "value", ""], [4, "ngIf"], ["mat-stroked-button", "", "color", "primary", 1, "child-add", 3, "click"]], template: function FofOrganizationsComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "div", 0);
            ɵɵelementStart(1, "div", 1);
            ɵɵelementStart(2, "mat-card", 2);
            ɵɵelementStart(3, "fof-organizations-tree", 3);
            ɵɵlistener("selectedOrganizationsChange", function FofOrganizationsComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.selectedOrganizationsChange($event); });
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(4, "div", 1);
            ɵɵelementStart(5, "mat-card", 4);
            ɵɵelementStart(6, "h3");
            ɵɵtext(7);
            ɵɵelementEnd();
            ɵɵelementStart(8, "div", 5);
            ɵɵtemplate(9, FofOrganizationsComponent_button_9_Template, 2, 0, "button", 6);
            ɵɵtemplate(10, FofOrganizationsComponent_button_10_Template, 2, 0, "button", 7);
            ɵɵtemplate(11, FofOrganizationsComponent_button_11_Template, 2, 0, "button", 8);
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵtemplate(12, FofOrganizationsComponent_div_12_Template, 4, 0, "div", 9);
            ɵɵtemplate(13, FofOrganizationsComponent_div_13_Template, 10, 4, "div", 10);
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(3);
            ɵɵproperty("nodeChanged", ctx.uiVar.nodeChanged)("nodeToDelete", ctx.uiVar.nodeToDelete);
            ɵɵadvance(4);
            ɵɵtextInterpolate(ctx.uiVar.mainTitle);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.uiVar.nodeForm);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.nodeForm && !ctx.uiVar.organizationIsNew);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.nodeForm);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.loading);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", !ctx.uiVar.loading && ctx.uiVar.nodeForm);
        } }, directives: [MatCard, FofOrganizationsTreeComponent, NgIf, MatButton, MatSpinner, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, MatFormField, MatInput, DefaultValueAccessor, RequiredValidator, NgControlStatus, FormControlName, MatError], styles: [".row.main[_ngcontent-%COMP%], .row.main[_ngcontent-%COMP%]   .card-tree[_ngcontent-%COMP%]{height:100%}.row.main[_ngcontent-%COMP%]   .card-detail[_ngcontent-%COMP%]{position:-webkit-sticky;position:sticky;top:0}.mat-form-field[_ngcontent-%COMP%]{width:100%}.child-add[_ngcontent-%COMP%]{margin-bottom:15px}"] });
    return FofOrganizationsComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofOrganizationsComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-fof-organizations',
                templateUrl: './fof-organizations.component.html',
                styleUrls: ['./fof-organizations.component.scss']
            }]
    }], function () { return [{ type: FofPermissionService }, { type: FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: FofErrorService }]; }, null); })();

var FofAuthGuard = /** @class */ (function () {
    function FofAuthGuard(router, foFAuthService, fofNotificationService, fofConfig) {
        this.router = router;
        this.foFAuthService = foFAuthService;
        this.fofNotificationService = fofNotificationService;
        this.fofConfig = fofConfig;
    }
    FofAuthGuard.prototype.canActivate = function (next, state) {
        // for permissions in route 
        // https://jasonwatmore.com/post/2019/08/06/angular-8-role-based-authorization-tutorial-with-example#tsconfig-json
        var currentUser = this.foFAuthService.currentUser;
        var notAuthentifiedRoute = '/login';
        if (this.fofConfig.authentication) {
            if (this.fofConfig.authentication.type == eAuth.windows) {
                notAuthentifiedRoute = 'loading';
            }
        }
        if (currentUser) {
            // check if route is restricted by permission      
            // get the deepest root data
            // https://stackoverflow.com/questions/43806188/how-can-i-access-an-activated-child-routes-data-from-the-parent-routes-compone
            var route = next;
            while (route.firstChild) {
                route = route.firstChild;
            }
            var data_1 = route.data;
            if (data_1.permissions) {
                var found = Object.values(currentUser.permissions).some(function (r) { return data_1.permissions.indexOf(r) >= 0; });
                if (!found) {
                    this.fofNotificationService.error("Vous n'avez pas les droit suffisant<br>\n            pour naviguer vers " + state.url);
                    this.router.navigate(['/']);
                    return false;
                }
            }
            // authorised
            return true;
        }
        // not logged in. Redirect to then notAuthentified page with the url to return on
        this.router.navigate([notAuthentifiedRoute], { queryParams: { returnUrl: state.url } });
        return false;
    };
    FofAuthGuard.ɵfac = function FofAuthGuard_Factory(t) { return new (t || FofAuthGuard)(ɵɵinject(Router), ɵɵinject(FoFAuthService), ɵɵinject(FofNotificationService), ɵɵinject(CORE_CONFIG)); };
    FofAuthGuard.ɵprov = ɵɵdefineInjectable({ token: FofAuthGuard, factory: FofAuthGuard.ɵfac, providedIn: 'root' });
    return FofAuthGuard;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofAuthGuard, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: Router }, { type: FoFAuthService }, { type: FofNotificationService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();

var eCp;
(function (eCp) {
    eCp["userCreate"] = "userCreate";
    eCp["userUpdate"] = "userUpdate";
    eCp["userRead"] = "userRead";
    eCp["userDelete"] = "userDelete";
    eCp["roleCreate"] = "roleCreate";
    eCp["roleUpdate"] = "roleUpdate";
    eCp["roleRead"] = "roleRead";
    eCp["roleDelete"] = "roleDelete";
    eCp["userRoleCreate"] = "userRoleCreate";
    eCp["userRoleUpdate"] = "userRoleUpdate";
    eCp["userRoleRead"] = "userRoleRead";
    eCp["userRoleDelete"] = "userRoleDelete";
    eCp["organizationCreate"] = "organizationCreate";
    eCp["organizationRead"] = "organizationRead";
    eCp["organizationUpdate"] = "organizationUpdate";
    eCp["organizationDelete"] = "organizationDelete";
    eCp["coreAdminAccess"] = "coreAdminAccess";
})(eCp || (eCp = {}));

var routes = [
    { path: 'admin', canActivate: [FofAuthGuard], data: { permissions: [eCp.coreAdminAccess] },
        children: [
            { path: '', component: FofRolesComponent },
            /**
             * if a child doesn't have permissions, it will get that ones
             * if it has some, it will override this ones
             * e.g. first child (path: '') get that ones
             */
            // { path: 'roles', data: {permissions: [eCp.roleRead]},
            { path: 'roles',
                children: [
                    { path: '', component: FofRolesComponent },
                    // { path: 'new', component: FofRoleComponent, data: {permissions: [eCp.roleCreate]} }
                    // { path: 'new', component: FofRoleComponent },
                    { path: ':code', component: FofRoleComponent }
                ]
            },
            { path: 'users',
                children: [
                    { path: '', component: FofUsersComponent },
                    { path: ':code', component: FofUserComponent }
                ]
            },
            { path: 'organizations', data: { permissions: [eCp.organizationRead] },
                children: [
                    { path: '', component: FofOrganizationsComponent }
                ]
            }
        ]
    }
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule.ɵmod = ɵɵdefineNgModule({ type: AdminRoutingModule });
    AdminRoutingModule.ɵinj = ɵɵdefineInjector({ factory: function AdminRoutingModule_Factory(t) { return new (t || AdminRoutingModule)(); }, imports: [[RouterModule.forChild(routes)],
            RouterModule] });
    return AdminRoutingModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(AdminRoutingModule, { imports: [RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(AdminRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes)],
                exports: [RouterModule]
            }]
    }], null, null); })();

var fofTranslate = /** @class */ (function () {
    function fofTranslate() {
    }
    fofTranslate.prototype.transform = function (content) {
        return "<b>" + content + "</b>";
    };
    fofTranslate.ɵfac = function fofTranslate_Factory(t) { return new (t || fofTranslate)(); };
    fofTranslate.ɵpipe = ɵɵdefinePipe({ name: "fofTranslate", type: fofTranslate, pure: true });
    return fofTranslate;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(fofTranslate, [{
        type: Pipe,
        args: [{ name: 'fofTranslate' }]
    }], null, null); })();

var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule.ɵmod = ɵɵdefineNgModule({ type: SharedModule });
    SharedModule.ɵinj = ɵɵdefineInjector({ factory: function SharedModule_Factory(t) { return new (t || SharedModule)(); }, imports: [[
                CommonModule
            ]] });
    return SharedModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(SharedModule, { declarations: [fofTranslate], imports: [CommonModule], exports: [fofTranslate] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(SharedModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    fofTranslate
                ],
                imports: [
                    CommonModule
                ],
                exports: [
                    fofTranslate
                ]
            }]
    }], null, null); })();

var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule.ɵmod = ɵɵdefineNgModule({ type: AdminModule });
    AdminModule.ɵinj = ɵɵdefineInjector({ factory: function AdminModule_Factory(t) { return new (t || AdminModule)(); }, imports: [[
                CommonModule,
                SharedModule,
                AdminRoutingModule,
                ComponentsModule,
                FofPermissionModule,
                MaterialModule,
                FormsModule,
                ReactiveFormsModule,
                TranslateModule //.forChild()
            ]] });
    return AdminModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(AdminModule, { declarations: [FofRolesComponent,
        FofRoleComponent,
        FofUsersComponent,
        FofUserComponent,
        FofEntityFooterComponent,
        FofOrganizationsComponent,
        FofUserRolesSelectComponent], imports: [CommonModule,
        SharedModule,
        AdminRoutingModule,
        ComponentsModule,
        FofPermissionModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule //.forChild()
    ] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(AdminModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    FofRolesComponent,
                    FofRoleComponent,
                    FofUsersComponent,
                    FofUserComponent,
                    FofEntityFooterComponent,
                    FofOrganizationsComponent,
                    FofUserRolesSelectComponent
                ],
                imports: [
                    CommonModule,
                    SharedModule,
                    AdminRoutingModule,
                    ComponentsModule,
                    FofPermissionModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    TranslateModule //.forChild()
                ]
            }]
    }], null, null); })();

var FofLocalstorageService = /** @class */ (function () {
    function FofLocalstorageService(fofConfig) {
        this.fofConfig = fofConfig;
        this.appShortName = this.fofConfig.appName.technical.toLocaleUpperCase() + "-";
    }
    FofLocalstorageService.prototype.setItem = function (key, value) {
        localStorage.setItem("" + this.appShortName + key, JSON.stringify(value));
    };
    FofLocalstorageService.prototype.getItem = function (key) {
        return JSON.parse(localStorage.getItem("" + this.appShortName + key));
    };
    FofLocalstorageService.prototype.removeItem = function (key) {
        localStorage.removeItem("" + this.appShortName + key);
    };
    FofLocalstorageService.ɵfac = function FofLocalstorageService_Factory(t) { return new (t || FofLocalstorageService)(ɵɵinject(CORE_CONFIG)); };
    FofLocalstorageService.ɵprov = ɵɵdefineInjectable({ token: FofLocalstorageService, factory: FofLocalstorageService.ɵfac, providedIn: 'root' });
    return FofLocalstorageService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofLocalstorageService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();

// Angular
function notificationComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    var _r304 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div");
    ɵɵelementStart(1, "button", 2);
    ɵɵlistener("click", function notificationComponent_div_1_Template_button_click_1_listener() { ɵɵrestoreView(_r304); var notification_r302 = ctx.$implicit; var ctx_r303 = ɵɵnextContext(); return ctx_r303.uiAction.removenotification(notification_r302); });
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "\u00D7");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelement(4, "div", 3);
    ɵɵelementEnd();
} if (rf & 2) {
    var notification_r302 = ctx.$implicit;
    var ctx_r300 = ɵɵnextContext();
    ɵɵclassMapInterpolate1("", ctx_r300.uiAction.cssClass(notification_r302), " notification-dismissable");
    ɵɵproperty("@inOut", undefined);
    ɵɵadvance(4);
    ɵɵproperty("innerHTML", notification_r302.message, ɵɵsanitizeHtml);
} }
function notificationComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 4);
    ɵɵelement(1, "span");
    ɵɵelementEnd();
} }
// how to
// https://angular.io/guide/animations
// https://github.com/PointInside/ng2-toastr/blob/master/src/toast-container.component.ts
var notificationComponent = /** @class */ (function () {
    function notificationComponent(fofNotificationService, ngZone) {
        var _this = this;
        this.fofNotificationService = fofNotificationService;
        this.ngZone = ngZone;
        // All private variables
        this.priVar = {
            getnotificationSub: undefined,
            savedNotificationSub: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            notifications: [],
            savedActive: false
        };
        // All actions shared with UI 
        this.uiAction = {
            removenotification: function (notification) {
                _this.uiVar.notifications = _this.uiVar.notifications.filter(function (x) { return x !== notification; });
            },
            cssClass: function (notification) {
                if (!notification) {
                    return;
                }
                // return css class based on notification type
                switch (notification.type) {
                    case NotificationType.Success:
                        return 'notification notification-success';
                    case NotificationType.Error:
                        return 'notification notification-danger';
                    case NotificationType.Info:
                        return 'notification notification-info';
                    case NotificationType.Warning:
                        return 'notification notification-warning';
                }
            }
        };
    }
    // Angular events
    notificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        var template = this;
        this.priVar.getnotificationSub = this.fofNotificationService.getnotification()
            .subscribe(function (notification) {
            if (!notification) {
                // clear notifications when an empty notification is received
                _this.uiVar.notifications = [];
                return;
            }
            if (notification.mustDisappearAfter) {
                setTimeout(function () {
                    template.uiAction.removenotification(notification);
                }, notification.mustDisappearAfter);
            }
            // ensure the notif will be displayed even if received from a promise pipe
            _this.ngZone.run(function () {
                // push or unshift depend if there are on the top or bottom
                // this.notifications.push(notification)
                _this.uiVar.notifications.unshift(notification);
            });
        });
        this.fofNotificationService.savedNotification
            .subscribe(function (saved) {
            if (!saved) {
                return;
            }
            _this.uiVar.savedActive = true;
            setTimeout(function () {
                _this.uiVar.savedActive = false;
            }, 300);
        });
    };
    notificationComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.getnotificationSub) {
            this.priVar.getnotificationSub.unsubscribe();
        }
        if (this.priVar.savedNotificationSub) {
            this.priVar.savedNotificationSub.unsubscribe();
        }
    };
    notificationComponent.ɵfac = function notificationComponent_Factory(t) { return new (t || notificationComponent)(ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(NgZone)); };
    notificationComponent.ɵcmp = ɵɵdefineComponent({ type: notificationComponent, selectors: [["fof-notification"]], decls: 3, vars: 2, consts: [[3, "class", 4, "ngFor", "ngForOf"], ["class", "saved-notification", 4, "ngIf"], ["type", "button", "data-dismiss", "notification", 1, "close", 3, "click"], [3, "innerHTML"], [1, "saved-notification"]], template: function notificationComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "div");
            ɵɵtemplate(1, notificationComponent_div_1_Template, 5, 5, "div", 0);
            ɵɵelementEnd();
            ɵɵtemplate(2, notificationComponent_div_2_Template, 2, 0, "div", 1);
        } if (rf & 2) {
            ɵɵadvance(1);
            ɵɵproperty("ngForOf", ctx.uiVar.notifications);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.savedActive);
        } }, directives: [NgForOf, NgIf], styles: ["[_nghost-%COMP%]{position:fixed;z-index:999999;bottom:12px;right:12px;width:300px}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]{padding:.5rem;margin-top:.5rem;position:relative}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]:hover   .close[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   .notification[_ngcontent-%COMP%]   .close[_ngcontent-%COMP%]{position:absolute;border:none;background-color:transparent;right:-5px;top:-13px;font-size:25px;cursor:pointer;opacity:.3}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]{position:fixed;z-index:9999999;bottom:15px;right:15px;width:25px;height:25px;border-radius:50px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{position:absolute;left:-3px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after, [_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{box-sizing:border-box;content:\"\";position:absolute}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:before{border-style:solid;border-width:7px 2px 1px;border-radius:1px;height:16px;left:8px;top:5px;width:16px}[_nghost-%COMP%]   .saved-notification[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]:after{border-style:solid;border-width:1px 1px 1px 4px;height:5px;left:13px;top:5px;width:7px}"], data: { animation: [
                trigger('inOut', [
                    transition(':enter', [
                        style({
                            opacity: 0,
                            transform: 'translateX(100%)'
                        }),
                        animate('0.2s ease-in')
                    ]),
                    transition(':leave', [
                        animate('0.2s 10ms ease-out', style({
                            opacity: 0,
                            transform: 'translateX(100%)'
                        }))
                    ])
                ])
            ] } });
    return notificationComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(notificationComponent, [{
        type: Component,
        args: [{
                selector: 'fof-notification',
                templateUrl: './notification.component.html',
                styleUrls: ['./notification.component.scss'],
                animations: [
                    trigger('inOut', [
                        transition(':enter', [
                            style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }),
                            animate('0.2s ease-in')
                        ]),
                        transition(':leave', [
                            animate('0.2s 10ms ease-out', style({
                                opacity: 0,
                                transform: 'translateX(100%)'
                            }))
                        ])
                    ])
                ]
            }]
    }], function () { return [{ type: FofNotificationService }, { type: NgZone }]; }, null); })();

/** Application-wide error handler that adds a UI notification to the error handling
 * provided by the default Angular ErrorHandler.
 */
var fofErrorHandler = /** @class */ (function (_super) {
    __extends(fofErrorHandler, _super);
    function fofErrorHandler(notificationsService, fofConfig) {
        var _this = _super.call(this) || this;
        _this.notificationsService = notificationsService;
        _this.fofConfig = fofConfig;
        return _this;
    }
    // handleError(error: Error | HttpErrorResponse | UnhandledRejection) {
    fofErrorHandler.prototype.handleError = function (exception) {
        var displayMessage = "Oops, nous avons une erreur...";
        var error;
        // if (exception instanceof HttpErrorResponse) {
        //   console.log('HttpErrorResponse', exception)
        //   error = exception.error
        //   if (error.fofCoreException) { 
        //     console.log('fof core exception', exception)
        //   }
        // }   
        // ToDO: manage promise unhandledrejection -> error in code
        // https://javascript.info/promise-error-handling
        if (exception.promise) {
            displayMessage = "<small>Technical notice</small><br>\n        Forgot to manage a promise rejection...<br>\n        <small>(yeah we know, it's funny ;)</small>";
            console.info("fofErrorHandler - Promises not catched!");
        }
        // console.error('fofErrorHandler - Common error', exception)
        this.notificationsService.error(displayMessage, { mustDisappearAfter: -1 });
        _super.prototype.handleError.call(this, exception);
    };
    fofErrorHandler.ɵfac = function fofErrorHandler_Factory(t) { return new (t || fofErrorHandler)(ɵɵinject(FofNotificationService), ɵɵinject(CORE_CONFIG)); };
    fofErrorHandler.ɵprov = ɵɵdefineInjectable({ token: fofErrorHandler, factory: fofErrorHandler.ɵfac });
    return fofErrorHandler;
}(ErrorHandler));
/*@__PURE__*/ (function () { ɵsetClassMetadata(fofErrorHandler, [{
        type: Injectable
    }], function () { return [{ type: FofNotificationService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();

var FofErrorInterceptor = /** @class */ (function () {
    function FofErrorInterceptor(fofErrorService) {
        this.fofErrorService = fofErrorService;
    }
    FofErrorInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request)
            .pipe(catchError(function (httpError) {
            return throwError(_this.fofErrorService.cleanError(httpError));
        }));
    };
    FofErrorInterceptor.ɵfac = function FofErrorInterceptor_Factory(t) { return new (t || FofErrorInterceptor)(ɵɵinject(FofErrorService)); };
    FofErrorInterceptor.ɵprov = ɵɵdefineInjectable({ token: FofErrorInterceptor, factory: FofErrorInterceptor.ɵfac });
    return FofErrorInterceptor;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FofErrorInterceptor, [{
        type: Injectable
    }], function () { return [{ type: FofErrorService }]; }, null); })();

var FoFJwtInterceptor = /** @class */ (function () {
    function FoFJwtInterceptor(foFAuthService, fofConfig) {
        this.foFAuthService = foFAuthService;
        this.fofConfig = fofConfig;
        this.environment = this.fofConfig.environment;
    }
    FoFJwtInterceptor.prototype.intercept = function (request, next) {
        // add auth header with jwt if user is logged in and request is to api url    
        var currentUser = this.foFAuthService.currentUser;
        var isLoggedIn = currentUser && currentUser.accessToken;
        var isApiUrl = request.url.startsWith(this.environment.apiPath);
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + currentUser.accessToken
                }
            });
        }
        return next.handle(request);
    };
    FoFJwtInterceptor.ɵfac = function FoFJwtInterceptor_Factory(t) { return new (t || FoFJwtInterceptor)(ɵɵinject(FoFAuthService), ɵɵinject(CORE_CONFIG)); };
    FoFJwtInterceptor.ɵprov = ɵɵdefineInjectable({ token: FoFJwtInterceptor, factory: FoFJwtInterceptor.ɵfac });
    return FoFJwtInterceptor;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(FoFJwtInterceptor, [{
        type: Injectable
    }], function () { return [{ type: FoFAuthService }, { type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }]; }, null); })();

// import { ComponentsModule } from '../components/components.module'
// import { ModuleTranslateLoader, IModuleTranslationOptions } from '@larscom/ngx-translate-module-loader'
// import { registerLocaleData } from '@angular/common'
// import localeFr from '@angular/common/locales/fr'
// import localeFrExtra from '@angular/common/locales/extra/fr'
// registerLocaleData(localeFr)
// AoT requires an exported function for factories
function HttpLoaderFactory(http) {
    return new TranslateHttpLoader(http);
}
// https://github.com/larscom/ngx-translate-module-loader
// export function moduleHttpLoaderFactory(http: HttpClient) {
//   const baseTranslateUrl = './assets/i18n';
//   const options: IModuleTranslationOptions = {
//     modules: [
//       // final url: ./assets/i18n/en.json
//       { baseTranslateUrl },
//       // final url: ./assets/i18n/admin/en.json
//       { moduleName: 'admin', baseTranslateUrl },
//       // final url: ./assets/i18n/feature2/en.json
//       { moduleName: 'feature2', baseTranslateUrl }
//     ]
//   };
//   return new ModuleTranslateLoader(http, options);
// }
var FoFCoreModule = /** @class */ (function () {
    function FoFCoreModule() {
    }
    FoFCoreModule.ɵmod = ɵɵdefineNgModule({ type: FoFCoreModule });
    FoFCoreModule.ɵinj = ɵɵdefineInjector({ factory: function FoFCoreModule_Factory(t) { return new (t || FoFCoreModule)(); }, providers: [
            FofLocalstorageService,
            // { provide: LOCALE_ID, useValue: 'fr' },
            { provide: ErrorHandler, useClass: fofErrorHandler },
            { provide: HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
            { provide: HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
            FofAuthGuard,
            FoFAuthService,
            FoFCoreService,
            FofNotificationService,
            FofDialogService,
            FofErrorService
        ], imports: [[
                CommonModule,
                SharedModule,
                MaterialModule,
                HttpClientModule,
                // ComponentsModule,
                TranslateModule.forRoot({
                    defaultLanguage: 'fr',
                    loader: {
                        provide: TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [HttpClient]
                    }
                })
            ]] });
    return FoFCoreModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(FoFCoreModule, { declarations: [notificationComponent,
        FofCoreDialogYesNoComponent], imports: [CommonModule,
        SharedModule,
        MaterialModule,
        HttpClientModule, TranslateModule], exports: [notificationComponent,
        FofCoreDialogYesNoComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(FoFCoreModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    notificationComponent,
                    FofCoreDialogYesNoComponent
                ],
                imports: [
                    CommonModule,
                    SharedModule,
                    MaterialModule,
                    HttpClientModule,
                    // ComponentsModule,
                    TranslateModule.forRoot({
                        defaultLanguage: 'fr',
                        loader: {
                            provide: TranslateLoader,
                            useFactory: HttpLoaderFactory,
                            deps: [HttpClient]
                        }
                    })
                ],
                entryComponents: [
                    FofCoreDialogYesNoComponent
                ],
                providers: [
                    FofLocalstorageService,
                    // { provide: LOCALE_ID, useValue: 'fr' },
                    { provide: ErrorHandler, useClass: fofErrorHandler },
                    { provide: HTTP_INTERCEPTORS, useClass: FoFJwtInterceptor, multi: true },
                    { provide: HTTP_INTERCEPTORS, useClass: FofErrorInterceptor, multi: true },
                    FofAuthGuard,
                    FoFAuthService,
                    FoFCoreService,
                    FofNotificationService,
                    FofDialogService,
                    FofErrorService
                ],
                exports: [
                    notificationComponent,
                    FofCoreDialogYesNoComponent,
                ]
            }]
    }], null, null); })();

function UsersComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 21);
    ɵɵelement(1, "mat-spinner", 22);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des collaborateurs...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function UsersComponent_th_22_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Email");
    ɵɵelementEnd();
} }
function UsersComponent_td_23_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r316 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r316.email);
} }
function UsersComponent_th_25_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Login");
    ɵɵelementEnd();
} }
function UsersComponent_td_26_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r317 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r317.login);
} }
function UsersComponent_th_28_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Pr\u00E9nom");
    ɵɵelementEnd();
} }
function UsersComponent_td_29_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r318 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r318.firstName);
} }
function UsersComponent_th_31_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 23);
    ɵɵtext(1, "Nom");
    ɵɵelementEnd();
} }
function UsersComponent_td_32_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 24);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r319 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r319.lastName);
} }
function UsersComponent_tr_33_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 25);
} }
function UsersComponent_tr_34_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 26);
} if (rf & 2) {
    var row_r320 = ctx.$implicit;
    ɵɵproperty("routerLink", row_r320.id);
} }
var _c0$2 = function () { return [5, 10, 25, 100]; };
var UsersComponent = /** @class */ (function () {
    function UsersComponent(fofPermissionService, fofNotificationService, breakpointObserver, fofErrorService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
            filter: undefined,
            filterOrganizationsChange: new EventEmitter(),
            filterOrganizations: undefined
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: [],
            data: [],
            resultsLength: 0,
            pageSize: 5,
            isLoadingResults: true,
            searchUsersCtrl: new FormControl()
        };
        // All actions shared with UI 
        this.uiAction = {
            organisationMultiSelectedChange: function (organization) {
                if (organization) {
                    _this.priVar.filterOrganizations = [organization.id];
                }
                else {
                    _this.priVar.filterOrganizations = null;
                }
                _this.priVar.filterOrganizationsChange.emit(organization);
            }
        };
    }
    // Angular events
    UsersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.filter = this.uiVar.searchUsersCtrl.valueChanges
            .pipe(debounceTime(500), distinctUntilChanged(), filter(function (query) { return query.length >= 3 || query.length === 0; }));
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['email', 'login'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['email', 'login', 'firstName', 'lastName'];
            }
        });
    };
    UsersComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page, this.priVar.filter, this.priVar.filterOrganizationsChange)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.fofPermissionService.user.search(_this.uiVar.searchUsersCtrl.value, _this.priVar.filterOrganizations, _this.uiVar.pageSize, _this.paginator.pageIndex, _this.sort.active, _this.sort.direction);
        }), map(function (search) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = search.total;
            return search.data;
        }), catchError(function (error) {
            _this.fofErrorService.errorManage(error);
            _this.uiVar.isLoadingResults = false;
            return of([]);
        })).subscribe(function (data) {
            _this.uiVar.data = data;
        });
    };
    UsersComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    UsersComponent.ɵfac = function UsersComponent_Factory(t) { return new (t || UsersComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(BreakpointObserver), ɵɵdirectiveInject(FofErrorService)); };
    UsersComponent.ɵcmp = ɵɵdefineComponent({ type: UsersComponent, selectors: [["fof-core-users"]], viewQuery: function UsersComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(MatPaginator, true);
            ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.paginator = _t.first);
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 36, vars: 11, consts: [[1, "row"], [1, "col-md-3"], [1, "card-tree"], [3, "multiSelect", "selectedOrganizationsChange"], [1, "col-md-9"], [1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "filtres"], ["autofocus", "", "matInput", "", "placeholder", "email ou nom ou pr\u00E9nom ou login", 3, "formControl"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "email", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "email"], ["mat-header-cell", "", "mat-sort-header", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "login"], ["matColumnDef", "firstName"], ["matColumnDef", "lastName"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", "", "mat-sort-header", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UsersComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "div", 0);
            ɵɵelementStart(1, "div", 1);
            ɵɵelementStart(2, "mat-card", 2);
            ɵɵelementStart(3, "fof-organizations-tree", 3);
            ɵɵlistener("selectedOrganizationsChange", function UsersComponent_Template_fof_organizations_tree_selectedOrganizationsChange_3_listener($event) { return ctx.uiAction.organisationMultiSelectedChange($event); });
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(4, "div", 4);
            ɵɵelementStart(5, "mat-card", 5);
            ɵɵelementStart(6, "h3");
            ɵɵtext(7, "Gestion des collaborateurs");
            ɵɵelementEnd();
            ɵɵelementStart(8, "a", 6);
            ɵɵelementStart(9, "span");
            ɵɵtext(10, "Ajouter un collaborateur");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(11, "mat-card", 7);
            ɵɵelementStart(12, "mat-form-field");
            ɵɵelementStart(13, "mat-label");
            ɵɵtext(14, "Filtre");
            ɵɵelementEnd();
            ɵɵelement(15, "input", 8);
            ɵɵelementStart(16, "mat-hint");
            ɵɵtext(17, "Recherche \u00E0 partir de 3 caract\u00E8res saisies");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(18, "div", 9);
            ɵɵtemplate(19, UsersComponent_div_19_Template, 4, 0, "div", 10);
            ɵɵelementStart(20, "table", 11);
            ɵɵelementContainerStart(21, 12);
            ɵɵtemplate(22, UsersComponent_th_22_Template, 2, 0, "th", 13);
            ɵɵtemplate(23, UsersComponent_td_23_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(24, 15);
            ɵɵtemplate(25, UsersComponent_th_25_Template, 2, 0, "th", 13);
            ɵɵtemplate(26, UsersComponent_td_26_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(27, 16);
            ɵɵtemplate(28, UsersComponent_th_28_Template, 2, 0, "th", 13);
            ɵɵtemplate(29, UsersComponent_td_29_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(30, 17);
            ɵɵtemplate(31, UsersComponent_th_31_Template, 2, 0, "th", 13);
            ɵɵtemplate(32, UsersComponent_td_32_Template, 2, 1, "td", 14);
            ɵɵelementContainerEnd();
            ɵɵtemplate(33, UsersComponent_tr_33_Template, 1, 0, "tr", 18);
            ɵɵtemplate(34, UsersComponent_tr_34_Template, 1, 1, "tr", 19);
            ɵɵelementEnd();
            ɵɵelement(35, "mat-paginator", 20);
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(3);
            ɵɵproperty("multiSelect", false);
            ɵɵadvance(5);
            ɵɵproperty("routerLink", "/admin/users/new");
            ɵɵadvance(7);
            ɵɵproperty("formControl", ctx.uiVar.searchUsersCtrl);
            ɵɵadvance(4);
            ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            ɵɵadvance(1);
            ɵɵproperty("dataSource", ctx.uiVar.data);
            ɵɵadvance(13);
            ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", ɵɵpureFunction0(10, _c0$2))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [MatCard, FofOrganizationsTreeComponent, MatAnchor, RouterLinkWithHref, MatFormField, MatLabel, MatInput, DefaultValueAccessor, NgControlStatus, FormControlDirective, MatHint, NgIf, MatTable, MatSort, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatPaginator, MatSpinner, MatHeaderCell, MatSortHeader, MatCell, MatHeaderRow, MatRow, RouterLink], styles: [".filtres[_ngcontent-%COMP%]{margin-bottom:15px}.filtres[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.card-tree[_ngcontent-%COMP%]{height:100%}"] });
    return UsersComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UsersComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-users',
                templateUrl: './users.component.html',
                styleUrls: ['./users.component.scss']
            }]
    }], function () { return [{ type: FofPermissionService }, { type: FofNotificationService }, { type: BreakpointObserver }, { type: FofErrorService }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();

var UservicesService = /** @class */ (function () {
    function UservicesService(fofConfig, httpClient) {
        var _this = this;
        this.fofConfig = fofConfig;
        this.httpClient = httpClient;
        this.uServices = {
            create: function (uService) { return _this.httpClient.post(_this.environment.apiPath + "/uservices", uService); },
            update: function (uService) { return _this.httpClient.patch(_this.environment.apiPath + "/uservices/" + uService.id, uService); },
            delete: function (uService) { return _this.httpClient.delete(_this.environment.apiPath + "/uservices/" + uService.id); },
            getOne: function (id) { return _this.httpClient.get(_this.environment.apiPath + "/uservices/" + id); },
            getAll: function () { return _this.httpClient.get(_this.environment.apiPath + "/uservices"); }
        };
        this.environment = this.fofConfig.environment;
    }
    UservicesService.ɵfac = function UservicesService_Factory(t) { return new (t || UservicesService)(ɵɵinject(CORE_CONFIG), ɵɵinject(HttpClient)); };
    UservicesService.ɵprov = ɵɵdefineInjectable({ token: UservicesService, factory: UservicesService.ɵfac, providedIn: 'root' });
    return UservicesService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UservicesService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: Inject,
                args: [CORE_CONFIG]
            }] }, { type: HttpClient }]; }, null); })();

function UserComponent_div_11_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le login ne doit pas exc\u00E9der 30 caract\u00E8res ");
    ɵɵelementEnd();
} }
function UserComponent_div_11_mat_error_9_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Un email valide est obligatoire ");
    ɵɵelementEnd();
} }
function UserComponent_div_11_mat_error_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le pr\u00E9nom ne doit pas exc\u00E9der 30 caract\u00E8res ");
    ɵɵelementEnd();
} }
function UserComponent_div_11_mat_error_15_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le nom de famille ne doit pas exc\u00E9der 30 caract\u00E8res ");
    ɵɵelementEnd();
} }
function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template(rf, ctx) { if (rf & 1) {
    var _r332 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "mat-expansion-panel");
    ɵɵelementStart(1, "mat-expansion-panel-header", 20);
    ɵɵelementStart(2, "mat-checkbox", 21);
    ɵɵlistener("click", function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template_mat_checkbox_click_2_listener($event) { ɵɵrestoreView(_r332); return $event.stopPropagation(); })("change", function UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template_mat_checkbox_change_2_listener() { ɵɵrestoreView(_r332); var uService_r328 = ɵɵnextContext().$implicit; var ctx_r333 = ɵɵnextContext(2); return ctx_r333.uiAction.userUserviceSave(uService_r328); });
    ɵɵelementEnd();
    ɵɵelementStart(3, "div", 22);
    ɵɵtext(4);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵtext(5, " This the expansion 2 content ");
    ɵɵelementEnd();
} if (rf & 2) {
    var uService_r328 = ɵɵnextContext().$implicit;
    ɵɵadvance(2);
    ɵɵproperty("checked", uService_r328.checked);
    ɵɵadvance(2);
    ɵɵtextInterpolate(uService_r328.name);
} }
function UserComponent_div_11_ng_container_22_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementContainerStart(0);
    ɵɵtemplate(1, UserComponent_div_11_ng_container_22_mat_expansion_panel_1_Template, 6, 2, "mat-expansion-panel", 13);
    ɵɵelementContainerEnd();
} if (rf & 2) {
    var uService_r328 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵproperty("ngIf", uService_r328.availableForUsers);
} }
function UserComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    var _r337 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "div", 8);
    ɵɵelementStart(1, "div", 9);
    ɵɵelementStart(2, "div", 10);
    ɵɵelementStart(3, "form", 11);
    ɵɵelementStart(4, "mat-form-field");
    ɵɵelement(5, "input", 12);
    ɵɵtemplate(6, UserComponent_div_11_mat_error_6_Template, 2, 0, "mat-error", 13);
    ɵɵelementEnd();
    ɵɵelementStart(7, "mat-form-field");
    ɵɵelement(8, "input", 14);
    ɵɵtemplate(9, UserComponent_div_11_mat_error_9_Template, 2, 0, "mat-error", 13);
    ɵɵelementEnd();
    ɵɵelementStart(10, "mat-form-field");
    ɵɵelement(11, "input", 15);
    ɵɵtemplate(12, UserComponent_div_11_mat_error_12_Template, 2, 0, "mat-error", 13);
    ɵɵelementEnd();
    ɵɵelementStart(13, "mat-form-field");
    ɵɵelement(14, "input", 16);
    ɵɵtemplate(15, UserComponent_div_11_mat_error_15_Template, 2, 0, "mat-error", 13);
    ɵɵelementEnd();
    ɵɵelementStart(16, "fof-core-fof-organizations-multi-select", 17);
    ɵɵlistener("selectedOrganizationsChange", function UserComponent_div_11_Template_fof_core_fof_organizations_multi_select_selectedOrganizationsChange_16_listener($event) { ɵɵrestoreView(_r337); var ctx_r336 = ɵɵnextContext(); return ctx_r336.uiAction.organisationMultiSelectedChange($event); });
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementStart(17, "div", 10);
    ɵɵelementStart(18, "h3");
    ɵɵtext(19, "Acc\u00E8s du collaborateur aux mini-services");
    ɵɵelementEnd();
    ɵɵelementStart(20, "mat-list");
    ɵɵelementStart(21, "mat-accordion", 18);
    ɵɵtemplate(22, UserComponent_div_11_ng_container_22_Template, 2, 1, "ng-container", 19);
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r321 = ɵɵnextContext();
    ɵɵadvance(3);
    ɵɵproperty("formGroup", ctx_r321.uiVar.form);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r321.uiVar.form.get("login").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r321.uiVar.form.get("email").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r321.uiVar.form.get("firstName").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r321.uiVar.form.get("lastName").invalid);
    ɵɵadvance(1);
    ɵɵproperty("placeHolder", "Organisation de rattachement")("multiSelect", false)("selectedOrganisations", ctx_r321.uiVar.userOrganizationsDisplay);
    ɵɵadvance(6);
    ɵɵproperty("ngForOf", ctx_r321.uiVar.uServicesAll);
} }
function UserComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 23);
    ɵɵelement(1, "mat-spinner", 24);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des utilisateurs et des authorisations...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
var UserComponent = /** @class */ (function () {
    function UserComponent(fofPermissionService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, matDialog, fofErrorService, uservicesService) {
        var _this = this;
        this.fofPermissionService = fofPermissionService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        this.matDialog = matDialog;
        this.fofErrorService = fofErrorService;
        this.uservicesService = uservicesService;
        // All private variables
        this.priVar = {
            userId: undefined,
            userOrganizationId: undefined,
            organizationAlreadyWithRoles: undefined
        };
        // All private functions
        this.privFunc = {
            userLoad: function () {
                _this.uiVar.loadingUser = true;
                Promise.all([
                    // Just need to ensure the uServices and user are both loaded the first time
                    _this.uiVar.uServicesAll ? null : _this.uservicesService.uServices.getAll().toPromise(),
                    _this.fofPermissionService.user.getWithUservicesById(_this.priVar.userId).toPromise()
                ])
                    .then(function (result) {
                    var uServices = result[0];
                    var currentUsers = result[1];
                    if (uServices) {
                        _this.uiVar.uServicesAll = uServices;
                    }
                    if (currentUsers.userUServices && currentUsers.userUServices.length > 0) {
                        uServices.forEach(function (service) {
                            var result = currentUsers.userUServices.filter(function (userService) { return userService.uServiceId == service.id; });
                            if (result && result.length > 0) {
                                service.checked = true;
                                service.userUserviceId = result[0].id;
                            }
                        });
                    }
                    _this.privFunc.userRefresh(currentUsers);
                    _this.uiVar.form.patchValue(_this.uiVar.user);
                })
                    .catch(function (reason) {
                    _this.fofErrorService.errorManage(reason);
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                })
                    .finally(function () {
                    _this.uiVar.loadingUser = false;
                });
            },
            userRefresh: function (currentUser) {
                if (!currentUser) {
                    _this.fofNotificationService.error("Ce collaborateur n'existe pas");
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                    return;
                }
                _this.uiVar.user = currentUser;
                if (currentUser.organizationId) {
                    _this.uiVar.userOrganizationsDisplay = [{ id: currentUser.organizationId }];
                    _this.priVar.userOrganizationId = currentUser.organizationId;
                }
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouveau collaborateur',
            loadingUser: false,
            uServicesAll: undefined,
            userIsNew: false,
            user: undefined,
            userOrganizationsDisplay: undefined,
            form: this.formBuilder.group({
                login: ['', [Validators.maxLength(30)]],
                email: ['', [Validators.required, Validators.email, Validators.maxLength(60)]],
                firstName: ['', [Validators.maxLength(30)]],
                lastName: ['', [Validators.maxLength(30)]]
            })
        };
        // All actions shared with UI 
        this.uiAction = {
            userSave: function () {
                var userToSave = _this.uiVar.form.value;
                userToSave.organizationId = _this.priVar.userOrganizationId;
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                if (_this.uiVar.userIsNew) {
                    _this.fofPermissionService.user.create(userToSave)
                        .toPromise()
                        .then(function (newUser) {
                        _this.fofNotificationService.success('collaborateur sauvé', { mustDisappearAfter: 1000 });
                        _this.priVar.userId = newUser.id;
                        _this.uiVar.title = "Modification d'un collaborateur";
                        _this.uiVar.userIsNew = false;
                        _this.privFunc.userLoad();
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    userToSave.id = _this.uiVar.user.id;
                    _this.fofPermissionService.user.update(userToSave)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.success('collaborateur sauvé', { mustDisappearAfter: 1000 });
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
            },
            userCancel: function () {
                _this.privFunc.userLoad();
            },
            userDelete: function () {
                _this.fofDialogService.openYesNo({
                    question: "Voulez vous vraiment supprimer le collaborateur ?"
                }).then(function (yes) {
                    if (yes) {
                        _this.fofPermissionService.user.delete(_this.uiVar.user)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('collaborateur supprimé');
                            _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        });
                    }
                });
            },
            userUserviceSave: function (uService) {
                if (uService.userUserviceId) {
                    _this.fofPermissionService.userUservice.delete(uService.userUserviceId)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.saveIsDone();
                        uService.checked = false;
                        uService.userUserviceId = null;
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
                else {
                    var userService = {
                        userId: _this.priVar.userId,
                        uServiceId: uService.id
                    };
                    _this.fofPermissionService.userUservice.create(userService)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.saveIsDone();
                        uService.checked = true;
                        uService.userUserviceId = result.id;
                    })
                        .catch(function (reason) {
                        _this.fofErrorService.errorManage(reason);
                    });
                }
            },
            organisationMultiSelectedChange: function (organizations) {
                if (organizations && organizations.length > 0) {
                    _this.priVar.userOrganizationId = organizations[0].id;
                }
                else {
                    _this.priVar.userOrganizationId = null;
                }
            }
        };
    }
    // Angular events
    UserComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.paramMap.subscribe(function (params) {
            var code = params.get('code');
            if (code) {
                if (code.toLowerCase() == 'new') {
                    _this.uiVar.userIsNew = true;
                }
                else {
                    _this.priVar.userId = code;
                    _this.uiVar.title = "Modification d'un collaborateur";
                    _this.privFunc.userLoad();
                }
            }
        });
    };
    UserComponent.ɵfac = function UserComponent_Factory(t) { return new (t || UserComponent)(ɵɵdirectiveInject(FofPermissionService), ɵɵdirectiveInject(ActivatedRoute), ɵɵdirectiveInject(FormBuilder), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(FofDialogService), ɵɵdirectiveInject(Router), ɵɵdirectiveInject(MatDialog), ɵɵdirectiveInject(FofErrorService), ɵɵdirectiveInject(UservicesService)); };
    UserComponent.ɵcmp = ɵɵdefineComponent({ type: UserComponent, selectors: [["fof-core-user"]], decls: 13, vars: 3, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "detail fof-fade-in", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [1, "detail", "fof-fade-in"], [1, "row"], [1, "col-md-6"], [3, "formGroup"], ["matInput", "", "formControlName", "login", "placeholder", "login", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "type", "email", "formControlName", "email", "placeholder", "Email", "value", ""], ["matInput", "", "required", "", "type", "firstName", "formControlName", "firstName", "placeholder", "Pr\u00E9nom", "value", ""], ["matInput", "", "required", "", "type", "lastName", "formControlName", "lastName", "placeholder", "Nom de famille", "value", ""], [3, "placeHolder", "multiSelect", "selectedOrganisations", "selectedOrganizationsChange"], ["multi", "true"], [4, "ngFor", "ngForOf"], [1, "uservice-access-header"], [3, "checked", "click", "change"], [1, "check-label"], [1, "fof-loading"], ["diameter", "20"]], template: function UserComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-card", 0);
            ɵɵelementStart(1, "h3");
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelementStart(3, "div", 1);
            ɵɵelementStart(4, "button", 2);
            ɵɵlistener("click", function UserComponent_Template_button_click_4_listener() { return ctx.uiAction.userCancel(); });
            ɵɵtext(5, "Annuler");
            ɵɵelementEnd();
            ɵɵelementStart(6, "button", 3);
            ɵɵlistener("click", function UserComponent_Template_button_click_6_listener() { return ctx.uiAction.userDelete(); });
            ɵɵtext(7, "Supprimer");
            ɵɵelementEnd();
            ɵɵelementStart(8, "button", 4);
            ɵɵlistener("click", function UserComponent_Template_button_click_8_listener() { return ctx.uiAction.userSave(); });
            ɵɵtext(9, " Enregister");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(10, "div", 5);
            ɵɵtemplate(11, UserComponent_div_11_Template, 23, 9, "div", 6);
            ɵɵtemplate(12, UserComponent_div_12_Template, 4, 0, "div", 7);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.uiVar.title);
            ɵɵadvance(9);
            ɵɵproperty("ngIf", !ctx.uiVar.loadingUser);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.loadingUser);
        } }, directives: [MatCard, MatButton, NgIf, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, MatFormField, MatInput, DefaultValueAccessor, NgControlStatus, FormControlName, RequiredValidator, FofOrganizationsMultiSelectComponent, MatList, MatAccordion, NgForOf, MatError, MatExpansionPanel, MatExpansionPanelHeader, MatCheckbox, MatSpinner], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row;margin-bottom:15px}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .uservice-access-header[_ngcontent-%COMP%]   .check-label[_ngcontent-%COMP%]{padding-left:10px;margin-top:.1em}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
    return UserComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UserComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-user',
                templateUrl: './user.component.html',
                styleUrls: ['./user.component.scss']
            }]
    }], function () { return [{ type: FofPermissionService }, { type: ActivatedRoute }, { type: FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: Router }, { type: MatDialog }, { type: FofErrorService }, { type: UservicesService }]; }, null); })();

var routes$1 = [
    { path: '', canActivate: [FofAuthGuard],
        children: [
            { path: '',
                children: [
                    { path: '', component: UsersComponent },
                    { path: ':code', component: UserComponent }
                ]
            },
        ]
    }
];
var UsersRoutingModule = /** @class */ (function () {
    function UsersRoutingModule() {
    }
    UsersRoutingModule.ɵmod = ɵɵdefineNgModule({ type: UsersRoutingModule });
    UsersRoutingModule.ɵinj = ɵɵdefineInjector({ factory: function UsersRoutingModule_Factory(t) { return new (t || UsersRoutingModule)(); }, imports: [[RouterModule.forChild(routes$1)],
            RouterModule] });
    return UsersRoutingModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(UsersRoutingModule, { imports: [RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(UsersRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes$1)],
                exports: [RouterModule]
            }]
    }], null, null); })();

function UservicesComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 15);
    ɵɵelement(1, "mat-spinner", 16);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des \u03BCServices...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function UservicesComponent_th_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 17);
    ɵɵtext(1, "Nom technique");
    ɵɵelementEnd();
} }
function UservicesComponent_td_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r500 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r500.technicalName);
} }
function UservicesComponent_th_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 17);
    ɵɵtext(1, "Nom");
    ɵɵelementEnd();
} }
function UservicesComponent_td_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r501 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r501.name);
} }
function UservicesComponent_th_16_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 17);
    ɵɵtext(1, "Url interface");
    ɵɵelementEnd();
} }
function UservicesComponent_td_17_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r502 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r502.frontUrl);
} }
function UservicesComponent_th_19_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 17);
    ɵɵtext(1, "Url backend");
    ɵɵelementEnd();
} }
function UservicesComponent_td_20_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r503 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r503.backUrl);
} }
function UservicesComponent_th_22_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "th", 17);
    ɵɵtext(1, "Visible pour les utilisateurs");
    ɵɵelementEnd();
} }
function UservicesComponent_td_23_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "td", 18);
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var row_r504 = ctx.$implicit;
    ɵɵadvance(1);
    ɵɵtextInterpolate(row_r504.availableForUsers);
} }
function UservicesComponent_tr_24_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 19);
} }
function UservicesComponent_tr_25_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "tr", 20);
} if (rf & 2) {
    var row_r505 = ctx.$implicit;
    ɵɵproperty("routerLink", row_r505.id);
} }
var _c0$3 = function () { return [100]; };
var UservicesComponent = /** @class */ (function () {
    function UservicesComponent(uservicesService, fofNotificationService, breakpointObserver) {
        this.uservicesService = uservicesService;
        this.fofNotificationService = fofNotificationService;
        this.breakpointObserver = breakpointObserver;
        // All private variables
        this.priVar = {
            breakpointObserverSub: undefined,
        };
        // All private functions
        this.privFunc = {};
        // All variables shared with UI 
        this.uiVar = {
            displayedColumns: ['technicalName', 'name', 'urlFront', 'urlBack', 'availableForUsers'],
            data: [],
            resultsLength: 0,
            pageSize: 100,
            isLoadingResults: true
        };
        // All actions shared with UI 
        this.uiAction = {};
    }
    // Angular events
    UservicesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.priVar.breakpointObserverSub = this.breakpointObserver.observe(Breakpoints.XSmall)
            .subscribe(function (state) {
            if (state.matches) {
                // XSmall
                _this.uiVar.displayedColumns = ['technicalName', 'name'];
            }
            else {
                // > XSmall
                _this.uiVar.displayedColumns = ['technicalName', 'name', 'frontUrl', 'backUrl', 'availableForUsers'];
            }
        });
    };
    UservicesComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        // If the user changes the sort order, reset back to the first page.
        this.sort.sortChange.subscribe(function () { return _this.paginator.pageIndex = 0; });
        merge(this.sort.sortChange, this.paginator.page)
            .pipe(startWith({}), switchMap(function () {
            _this.uiVar.isLoadingResults = true;
            _this.uiVar.pageSize = _this.paginator.pageSize;
            return _this.uservicesService.uServices.getAll();
        }), map(function (uServices) {
            _this.uiVar.isLoadingResults = false;
            _this.uiVar.resultsLength = uServices.length;
            return uServices;
        }), catchError(function () {
            _this.uiVar.isLoadingResults = false;
            return of([]);
        })).subscribe(function (data) { return _this.uiVar.data = data; });
    };
    UservicesComponent.prototype.ngOnDestroy = function () {
        if (this.priVar.breakpointObserverSub) {
            this.priVar.breakpointObserverSub.unsubscribe();
        }
    };
    UservicesComponent.ɵfac = function UservicesComponent_Factory(t) { return new (t || UservicesComponent)(ɵɵdirectiveInject(UservicesService), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(BreakpointObserver)); };
    UservicesComponent.ɵcmp = ɵɵdefineComponent({ type: UservicesComponent, selectors: [["fof-core-uservices"]], viewQuery: function UservicesComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(MatPaginator, true);
            ɵɵviewQuery(MatSort, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.paginator = _t.first);
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.sort = _t.first);
        } }, decls: 27, vars: 9, consts: [[1, "fof-header"], ["mat-stroked-button", "", "color", "accent", 3, "routerLink"], [1, "fof-table-container", "mat-elevation-z2"], ["class", "table-loading-shade fof-loading", 4, "ngIf"], ["mat-table", "", "matSort", "", "matSortActive", "technicalName", "matSortDisableClear", "", "matSortDirection", "asc", 1, "data-table", 3, "dataSource"], ["matColumnDef", "technicalName"], ["mat-header-cell", "", 4, "matHeaderCellDef"], ["mat-cell", "", 4, "matCellDef"], ["matColumnDef", "name"], ["matColumnDef", "frontUrl"], ["matColumnDef", "backUrl"], ["matColumnDef", "availableForUsers"], ["mat-header-row", "", 4, "matHeaderRowDef"], ["mat-row", "", "class", "fof-element-over", 3, "routerLink", 4, "matRowDef", "matRowDefColumns"], [3, "length", "pageSizeOptions", "pageSize"], [1, "table-loading-shade", "fof-loading"], ["diameter", "20"], ["mat-header-cell", ""], ["mat-cell", ""], ["mat-header-row", ""], ["mat-row", "", 1, "fof-element-over", 3, "routerLink"]], template: function UservicesComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-card", 0);
            ɵɵelementStart(1, "h3");
            ɵɵtext(2, "Mini-services");
            ɵɵelementEnd();
            ɵɵelementStart(3, "a", 1);
            ɵɵelementStart(4, "span");
            ɵɵtext(5, "Ajouter un mini-service");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(6, "div", 2);
            ɵɵtemplate(7, UservicesComponent_div_7_Template, 4, 0, "div", 3);
            ɵɵelementStart(8, "table", 4);
            ɵɵelementContainerStart(9, 5);
            ɵɵtemplate(10, UservicesComponent_th_10_Template, 2, 0, "th", 6);
            ɵɵtemplate(11, UservicesComponent_td_11_Template, 2, 1, "td", 7);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(12, 8);
            ɵɵtemplate(13, UservicesComponent_th_13_Template, 2, 0, "th", 6);
            ɵɵtemplate(14, UservicesComponent_td_14_Template, 2, 1, "td", 7);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(15, 9);
            ɵɵtemplate(16, UservicesComponent_th_16_Template, 2, 0, "th", 6);
            ɵɵtemplate(17, UservicesComponent_td_17_Template, 2, 1, "td", 7);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(18, 10);
            ɵɵtemplate(19, UservicesComponent_th_19_Template, 2, 0, "th", 6);
            ɵɵtemplate(20, UservicesComponent_td_20_Template, 2, 1, "td", 7);
            ɵɵelementContainerEnd();
            ɵɵelementContainerStart(21, 11);
            ɵɵtemplate(22, UservicesComponent_th_22_Template, 2, 0, "th", 6);
            ɵɵtemplate(23, UservicesComponent_td_23_Template, 2, 1, "td", 7);
            ɵɵelementContainerEnd();
            ɵɵtemplate(24, UservicesComponent_tr_24_Template, 1, 0, "tr", 12);
            ɵɵtemplate(25, UservicesComponent_tr_25_Template, 1, 1, "tr", 13);
            ɵɵelementEnd();
            ɵɵelement(26, "mat-paginator", 14);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(3);
            ɵɵproperty("routerLink", "new");
            ɵɵadvance(4);
            ɵɵproperty("ngIf", ctx.uiVar.isLoadingResults);
            ɵɵadvance(1);
            ɵɵproperty("dataSource", ctx.uiVar.data);
            ɵɵadvance(16);
            ɵɵproperty("matHeaderRowDef", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("matRowDefColumns", ctx.uiVar.displayedColumns);
            ɵɵadvance(1);
            ɵɵproperty("length", ctx.uiVar.resultsLength)("pageSizeOptions", ɵɵpureFunction0(8, _c0$3))("pageSize", ctx.uiVar.pageSize);
        } }, directives: [MatCard, MatAnchor, RouterLinkWithHref, NgIf, MatTable, MatSort, MatColumnDef, MatHeaderCellDef, MatCellDef, MatHeaderRowDef, MatRowDef, MatPaginator, MatSpinner, MatHeaderCell, MatCell, MatHeaderRow, MatRow, RouterLink], styles: [""] });
    return UservicesComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UservicesComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-uservices',
                templateUrl: './uservices.component.html',
                styleUrls: ['./uservices.component.scss']
            }]
    }], function () { return [{ type: UservicesService }, { type: FofNotificationService }, { type: BreakpointObserver }]; }, { paginator: [{
            type: ViewChild,
            args: [MatPaginator]
        }], sort: [{
            type: ViewChild,
            args: [MatSort]
        }] }); })();

function UserviceComponent_div_11_mat_error_4_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le nom technique est obligatoire et doit \u00EAtre compos\u00E9 de 3 \u00E0 40 caract\u00E8res ");
    ɵɵelementEnd();
} }
function UserviceComponent_div_11_mat_error_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " Le nom est obligatoire et doit \u00EAtre compos\u00E9 de moins de 30 caract\u00E8res ");
    ɵɵelementEnd();
} }
function UserviceComponent_div_11_mat_error_10_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " L'url doit \u00EAtre valide ");
    ɵɵelementEnd();
} }
function UserviceComponent_div_11_mat_error_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1, " L'url doit \u00EAtre valide ");
    ɵɵelementEnd();
} }
function UserviceComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 8);
    ɵɵelementStart(1, "form", 9);
    ɵɵelementStart(2, "mat-form-field");
    ɵɵelement(3, "input", 10);
    ɵɵtemplate(4, UserviceComponent_div_11_mat_error_4_Template, 2, 0, "mat-error", 11);
    ɵɵelementEnd();
    ɵɵelementStart(5, "mat-form-field");
    ɵɵelement(6, "input", 12);
    ɵɵtemplate(7, UserviceComponent_div_11_mat_error_7_Template, 2, 0, "mat-error", 11);
    ɵɵelementEnd();
    ɵɵelementStart(8, "mat-form-field");
    ɵɵelement(9, "input", 13);
    ɵɵtemplate(10, UserviceComponent_div_11_mat_error_10_Template, 2, 0, "mat-error", 11);
    ɵɵelementEnd();
    ɵɵelementStart(11, "mat-form-field");
    ɵɵelement(12, "input", 14);
    ɵɵtemplate(13, UserviceComponent_div_11_mat_error_13_Template, 2, 0, "mat-error", 11);
    ɵɵelementEnd();
    ɵɵelementStart(14, "mat-checkbox", 15);
    ɵɵtext(15, "Accessible aux utilisateurs ?");
    ɵɵelementEnd();
    ɵɵelementEnd();
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r506 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵproperty("formGroup", ctx_r506.uiVar.form);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("technicalName").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("name").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("backUrl").invalid);
    ɵɵadvance(3);
    ɵɵproperty("ngIf", ctx_r506.uiVar.form.get("frontUrl").invalid);
} }
function UserviceComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 16);
    ɵɵelement(1, "mat-spinner", 17);
    ɵɵelementStart(2, "span");
    ɵɵtext(3, "Chargements des uServices et des authorisations...");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
var UserviceComponent = /** @class */ (function () {
    function UserviceComponent(uUservicesService, activatedRoute, formBuilder, fofNotificationService, fofDialogService, router, fofErrorService) {
        var _this = this;
        this.uUservicesService = uUservicesService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.fofNotificationService = fofNotificationService;
        this.fofDialogService = fofDialogService;
        this.router = router;
        this.fofErrorService = fofErrorService;
        // All private variables
        this.priVar = {
            uServiceId: undefined,
            URL_REGEXP: /^[A-Za-z][A-Za-z\d.+-]*:\/*(?:\w+(?::\w+)?@)?[^\s/]+(?::\d+)?(?:\/[\w#!:.?+=&%@\-/]*)?$/
        };
        // All private functions
        this.privFunc = {
            uServiceLoad: function () {
                _this.uiVar.loading = true;
                _this.uUservicesService.uServices.getOne(_this.priVar.uServiceId)
                    .toPromise()
                    .then(function (uservice) {
                    _this.uiVar.uService = uservice;
                    _this.uiVar.form.patchValue(_this.uiVar.uService);
                })
                    .catch(function (reason) {
                    _this.fofErrorService.errorManage(reason);
                    _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                })
                    .finally(function () {
                    _this.uiVar.loading = false;
                });
            }
        };
        // All variables shared with UI 
        this.uiVar = {
            title: 'Nouveau rôle',
            permissionGroups: undefined,
            loading: false,
            uServiceIsNew: false,
            uService: undefined,
            form: this.formBuilder.group({
                technicalName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
                name: ['', [Validators.required, Validators.maxLength(30)]],
                frontUrl: ['', [Validators.pattern(this.priVar.URL_REGEXP)]],
                backUrl: ['', [Validators.pattern(this.priVar.URL_REGEXP)]],
                availableForUsers: ['']
            })
        };
        // All actions shared with UI 
        this.uiAction = {
            uServiceSave: function () {
                var uServiceToSave = _this.uiVar.form.value;
                console.log('uServiceToSave', uServiceToSave);
                if (!_this.uiVar.form.valid) {
                    _this.fofNotificationService.error('Veuillez corriger les erreurs avant de sauvegarder');
                    fofUtilsForm.validateAllFields(_this.uiVar.form);
                    return;
                }
                if (_this.uiVar.uServiceIsNew) {
                    _this.uUservicesService.uServices.create(uServiceToSave)
                        .toPromise()
                        .then(function (newUservice) {
                        _this.fofNotificationService.success('Mini-service sauvé', { mustDisappearAfter: 1000 });
                        _this.priVar.uServiceId = newUservice.id;
                        _this.uiVar.title = 'Modification de mini-service';
                        _this.uiVar.uServiceIsNew = false;
                        _this.privFunc.uServiceLoad();
                    });
                }
                else {
                    uServiceToSave.id = _this.uiVar.uService.id;
                    _this.uUservicesService.uServices.update(uServiceToSave)
                        .toPromise()
                        .then(function (result) {
                        _this.fofNotificationService.success('Mini-service sauvé', { mustDisappearAfter: 1000 });
                    });
                }
            },
            uServiceCancel: function () {
                _this.privFunc.uServiceLoad();
            },
            uServiceDelete: function () {
                _this.fofDialogService.openYesNo({
                    question: 'Voulez vous vraiment supprimer le mini-service ?'
                }).then(function (yes) {
                    if (yes) {
                        _this.uUservicesService.uServices.delete(_this.uiVar.uService)
                            .toPromise()
                            .then(function (result) {
                            _this.fofNotificationService.success('Mini-service supprimé');
                            _this.router.navigate(['../'], { relativeTo: _this.activatedRoute });
                        });
                    }
                });
            }
        };
    }
    // Angular events
    UserviceComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.paramMap.subscribe(function (params) {
            var id = params.get('id');
            _this.priVar.uServiceId = id;
            if (id) {
                if (id.toLowerCase() == 'new') {
                    _this.uiVar.uServiceIsNew = true;
                }
                else {
                    _this.uiVar.title = 'Modification de mini-service';
                    _this.privFunc.uServiceLoad();
                }
            }
        });
    };
    UserviceComponent.ɵfac = function UserviceComponent_Factory(t) { return new (t || UserviceComponent)(ɵɵdirectiveInject(UservicesService), ɵɵdirectiveInject(ActivatedRoute), ɵɵdirectiveInject(FormBuilder), ɵɵdirectiveInject(FofNotificationService), ɵɵdirectiveInject(FofDialogService), ɵɵdirectiveInject(Router), ɵɵdirectiveInject(FofErrorService)); };
    UserviceComponent.ɵcmp = ɵɵdefineComponent({ type: UserviceComponent, selectors: [["fof-core-uservice"]], decls: 13, vars: 3, consts: [[1, "fof-header"], [1, "fof-toolbar"], ["mat-stroked-button", "", 3, "click"], ["mat-stroked-button", "", "color", "warn", 3, "click"], ["mat-stroked-button", "", "color", "accent", 3, "click"], [1, "main"], ["class", "fof-fade-in detail", 4, "ngIf"], ["class", "fof-loading", 4, "ngIf"], [1, "fof-fade-in", "detail"], [3, "formGroup"], ["matInput", "", "required", "", "formControlName", "technicalName", "placeholder", "Nom technique", "value", ""], [4, "ngIf"], ["matInput", "", "required", "", "formControlName", "name", "placeholder", "Nom", "value", ""], ["matInput", "", "type", "url", "formControlName", "backUrl", "placeholder", "Url du backend", "value", ""], ["matInput", "", "type", "url", "formControlName", "frontUrl", "placeholder", "Url du frontend", "value", ""], ["formControlName", "availableForUsers"], [1, "fof-loading"], ["diameter", "20"]], template: function UserviceComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-card", 0);
            ɵɵelementStart(1, "h3");
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelementStart(3, "div", 1);
            ɵɵelementStart(4, "button", 2);
            ɵɵlistener("click", function UserviceComponent_Template_button_click_4_listener() { return ctx.uiAction.uServiceCancel(); });
            ɵɵtext(5, "Annuler");
            ɵɵelementEnd();
            ɵɵelementStart(6, "button", 3);
            ɵɵlistener("click", function UserviceComponent_Template_button_click_6_listener() { return ctx.uiAction.uServiceDelete(); });
            ɵɵtext(7, "Supprimer");
            ɵɵelementEnd();
            ɵɵelementStart(8, "button", 4);
            ɵɵlistener("click", function UserviceComponent_Template_button_click_8_listener() { return ctx.uiAction.uServiceSave(); });
            ɵɵtext(9, " Enregister");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(10, "div", 5);
            ɵɵtemplate(11, UserviceComponent_div_11_Template, 16, 5, "div", 6);
            ɵɵtemplate(12, UserviceComponent_div_12_Template, 4, 0, "div", 7);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.uiVar.title);
            ɵɵadvance(9);
            ɵɵproperty("ngIf", !ctx.uiVar.loading);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.uiVar.loading);
        } }, directives: [MatCard, MatButton, NgIf, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, MatFormField, MatInput, DefaultValueAccessor, RequiredValidator, NgControlStatus, FormControlName, MatCheckbox, MatError, MatSpinner], styles: [".main[_ngcontent-%COMP%]{display:flex;flex-wrap:wrap;align-items:flex-start;flex-direction:row}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]{width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]{min-width:150px;max-width:500px;width:100%}.main[_ngcontent-%COMP%]   .detail[_ngcontent-%COMP%]   form[_ngcontent-%COMP%]   .mat-form-field[_ngcontent-%COMP%]{width:100%}@media (max-width:768px){.main[_ngcontent-%COMP%]   .group-card[_ngcontent-%COMP%]{width:100%;margin-right:0}}"] });
    return UserviceComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(UserviceComponent, [{
        type: Component,
        args: [{
                selector: 'fof-core-uservice',
                templateUrl: './uservice.component.html',
                styleUrls: ['./uservice.component.scss']
            }]
    }], function () { return [{ type: UservicesService }, { type: ActivatedRoute }, { type: FormBuilder }, { type: FofNotificationService }, { type: FofDialogService }, { type: Router }, { type: FofErrorService }]; }, null); })();

var routes$2 = [
    { path: '', canActivate: [FofAuthGuard],
        children: [
            { path: '',
                children: [
                    { path: '', component: UservicesComponent },
                    { path: ':id', component: UserviceComponent }
                ]
            },
        ]
    }
];
var UservicesRoutingModule = /** @class */ (function () {
    function UservicesRoutingModule() {
    }
    UservicesRoutingModule.ɵmod = ɵɵdefineNgModule({ type: UservicesRoutingModule });
    UservicesRoutingModule.ɵinj = ɵɵdefineInjector({ factory: function UservicesRoutingModule_Factory(t) { return new (t || UservicesRoutingModule)(); }, imports: [[RouterModule.forChild(routes$2)],
            RouterModule] });
    return UservicesRoutingModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(UservicesRoutingModule, { imports: [RouterModule], exports: [RouterModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(UservicesRoutingModule, [{
        type: NgModule,
        args: [{
                imports: [RouterModule.forChild(routes$2)],
                exports: [RouterModule]
            }]
    }], null, null); })();

var UservicesModule = /** @class */ (function () {
    function UservicesModule() {
    }
    UservicesModule.ɵmod = ɵɵdefineNgModule({ type: UservicesModule });
    UservicesModule.ɵinj = ɵɵdefineInjector({ factory: function UservicesModule_Factory(t) { return new (t || UservicesModule)(); }, imports: [[
                CommonModule,
                MaterialModule,
                FormsModule,
                ReactiveFormsModule,
                UservicesRoutingModule
            ]] });
    return UservicesModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(UservicesModule, { declarations: [UservicesComponent, UserviceComponent], imports: [CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        UservicesRoutingModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(UservicesModule, [{
        type: NgModule,
        args: [{
                declarations: [UservicesComponent, UserviceComponent],
                imports: [
                    CommonModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    UservicesRoutingModule
                ]
            }]
    }], null, null); })();

var UsersModule = /** @class */ (function () {
    function UsersModule() {
    }
    UsersModule.ɵmod = ɵɵdefineNgModule({ type: UsersModule });
    UsersModule.ɵinj = ɵɵdefineInjector({ factory: function UsersModule_Factory(t) { return new (t || UsersModule)(); }, imports: [[
                CommonModule,
                UsersRoutingModule,
                MaterialModule,
                FormsModule,
                ReactiveFormsModule,
                ComponentsModule,
                UservicesModule
            ]] });
    return UsersModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(UsersModule, { declarations: [UsersComponent,
        UserComponent], imports: [CommonModule,
        UsersRoutingModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        ComponentsModule,
        UservicesModule] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(UsersModule, [{
        type: NgModule,
        args: [{
                declarations: [
                    UsersComponent,
                    UserComponent
                ],
                imports: [
                    CommonModule,
                    UsersRoutingModule,
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    ComponentsModule,
                    UservicesModule
                ]
            }]
    }], null, null); })();

/*
 * Public API Surface of fof-angular-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AdminModule, CORE_CONFIG, ComponentsModule, FoFAuthService, FoFCoreModule, FofAuthGuard, FofCoreDialogYesNoComponent, FofDialogService, FofErrorService, FofLocalstorageService, FofNotificationService, FofOrganizationsMultiSelectComponent, FofOrganizationsTreeComponent, FofPermissionService, HttpLoaderFactory, UsersModule, UservicesModule, eAuth, notificationComponent };
//# sourceMappingURL=fof-angular-core.js.map

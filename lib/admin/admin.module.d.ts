import * as i0 from "@angular/core";
import * as i1 from "./fof-roles/fof-roles.component";
import * as i2 from "./fof-role/fof-role.component";
import * as i3 from "./fof-users/fof-users.component";
import * as i4 from "./fof-user/fof-user.component";
import * as i5 from "./fof-entity-footer/fof-entity-footer.component";
import * as i6 from "./fof-organizations/fof-organizations.component";
import * as i7 from "./fof-user-roles-select/fof-user-roles-select.component";
import * as i8 from "@angular/common";
import * as i9 from "../shared/shared.module";
import * as i10 from "./admin-routing.module";
import * as i11 from "../components/components.module";
import * as i12 from "../permission/permission.module";
import * as i13 from "../core/material.module";
import * as i14 from "@angular/forms";
import * as i15 from "@ngx-translate/core";
export declare class AdminModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<AdminModule, [typeof i1.FofRolesComponent, typeof i2.FofRoleComponent, typeof i3.FofUsersComponent, typeof i4.FofUserComponent, typeof i5.FofEntityFooterComponent, typeof i6.FofOrganizationsComponent, typeof i7.FofUserRolesSelectComponent], [typeof i8.CommonModule, typeof i9.SharedModule, typeof i10.AdminRoutingModule, typeof i11.ComponentsModule, typeof i12.FofPermissionModule, typeof i13.MaterialModule, typeof i14.FormsModule, typeof i14.ReactiveFormsModule, typeof i15.TranslateModule], never>;
    static ɵinj: i0.ɵɵInjectorDef<AdminModule>;
}

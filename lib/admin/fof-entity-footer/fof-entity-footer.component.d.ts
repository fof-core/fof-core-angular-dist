import { OnInit } from '@angular/core';
import { iFofEntityCore } from '../../core/entityCore.interface';
import * as i0 from "@angular/core";
export declare class FofEntityFooterComponent implements OnInit {
    entityBase: iFofEntityCore;
    constructor();
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofEntityFooterComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofEntityFooterComponent, "fof-entity-footer", never, { "entityBase": "entityBase"; }, {}, never>;
}

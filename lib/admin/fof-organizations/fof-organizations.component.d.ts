import { OnInit } from '@angular/core';
import { FofPermissionService } from '../../permission/fof-permission.service';
import { iOrganization } from '../../permission/interfaces/permissions.interface';
import { FormBuilder } from "@angular/forms";
import { FofNotificationService } from '../../core/notification/notification.service';
import { FofDialogService } from '../../core/fof-dialog.service';
import { FofErrorService } from '../../core/fof-error.service';
import * as i0 from "@angular/core";
interface iOrgUI extends iOrganization {
    checked?: boolean;
    indeterminate?: boolean;
    children?: iOrgUI[];
}
export declare class FofOrganizationsComponent implements OnInit {
    private fofPermissionService;
    private formBuilder;
    private fofNotificationService;
    private fofDialogService;
    private fofErrorService;
    constructor(fofPermissionService: FofPermissionService, formBuilder: FormBuilder, fofNotificationService: FofNotificationService, fofDialogService: FofDialogService, fofErrorService: FofErrorService);
    private priVar;
    private privFunc;
    uiVar: {
        loading: boolean;
        organizationIsNew: boolean;
        nodeForm: boolean;
        form: import("@angular/forms").FormGroup;
        actionTitle: string;
        mainTitle: string;
        nodeChanged: iOrganization;
        nodeToDelete: iOrganization;
    };
    uiAction: {
        organisationAdd: () => void;
        organisationDelete: () => void;
        organisationSave: () => void;
        organisationCancel: () => void;
        selectedOrganizationsChange: (node: iOrgUI) => void;
    };
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofOrganizationsComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofOrganizationsComponent, "fof-core-fof-organizations", never, {}, {}, never>;
}
export {};

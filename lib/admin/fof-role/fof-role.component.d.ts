import { OnInit } from '@angular/core';
import { FofPermissionService } from '../../permission/fof-permission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { iRole } from '../../permission/interfaces/permissions.interface';
import { FormGroup, FormBuilder } from "@angular/forms";
import { FofNotificationService } from '../../core/notification/notification.service';
import { FofDialogService } from '../../core/fof-dialog.service';
import * as i0 from "@angular/core";
export declare class FofRoleComponent implements OnInit {
    private fofPermissionService;
    private activatedRoute;
    private formBuilder;
    private fofNotificationService;
    private fofDialogService;
    private router;
    constructor(fofPermissionService: FofPermissionService, activatedRoute: ActivatedRoute, formBuilder: FormBuilder, fofNotificationService: FofNotificationService, fofDialogService: FofDialogService, router: Router);
    private priVar;
    private privFunc;
    uiVar: {
        title: string;
        permissionGroups: any;
        loading: boolean;
        roleIsNew: boolean;
        role: iRole;
        form: FormGroup;
    };
    uiAction: {
        roleSave: () => void;
        roleCancel: () => void;
        roleDelete: () => void;
        rolePermissionSave: (permission: any) => void;
    };
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofRoleComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofRoleComponent, "fof-role", never, {}, {}, never>;
}

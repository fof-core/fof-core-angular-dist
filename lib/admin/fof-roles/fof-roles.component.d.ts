import { OnInit, AfterViewInit } from '@angular/core';
import { FofPermissionService } from '../../permission/fof-permission.service';
import { FofNotificationService } from '../../core/notification/notification.service';
import { iRole } from '../../permission/interfaces/permissions.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BreakpointObserver } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
export declare class FofRolesComponent implements OnInit, AfterViewInit {
    private fofPermissionService;
    private fofNotificationService;
    private breakpointObserver;
    paginator: MatPaginator;
    sort: MatSort;
    constructor(fofPermissionService: FofPermissionService, fofNotificationService: FofNotificationService, breakpointObserver: BreakpointObserver);
    private priVar;
    private privFunc;
    uiVar: {
        displayedColumns: string[];
        data: iRole[];
        resultsLength: number;
        pageSize: number;
        isLoadingResults: boolean;
    };
    uiAction: {};
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofRolesComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofRolesComponent, "fof-roles", never, {}, {}, never>;
}

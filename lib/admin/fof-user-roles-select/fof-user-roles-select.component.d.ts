import { OnInit, OnChanges } from '@angular/core';
import { iRole, iUserRoleOrganization, iOrganization } from '../../permission/interfaces/permissions.interface';
import { FofPermissionService } from '../../permission/fof-permission.service';
import { MatDialogRef } from '@angular/material/dialog';
import { FofNotificationService } from '../../core/notification/notification.service';
import { FofErrorService } from '../../core/fof-error.service';
import * as i0 from "@angular/core";
interface iRoleUI extends iRole {
    checked?: boolean;
    userRoleOrganization?: iUserRoleOrganization;
}
export declare class FofUserRolesSelectComponent implements OnInit, OnChanges {
    private matDialogRef;
    private fofPermissionService;
    private fofNotificationService;
    private fofErrorService;
    userRoleOrganizations: iUserRoleOrganization[];
    notSelectableOrganizations: iOrganization[];
    constructor(matDialogRef: MatDialogRef<FofUserRolesSelectComponent>, fofPermissionService: FofPermissionService, fofNotificationService: FofNotificationService, fofErrorService: FofErrorService, data: {
        roles: iRole[];
        userRoleOrganizations: iUserRoleOrganization[];
        userId: number;
        notSelectableOrganizations: iOrganization[];
    });
    private priVar;
    private privFunc;
    uiVar: {
        allRoles: iRoleUI[];
        organizationMultipleSelect: boolean;
        selectedOrganisations: iOrganization[];
        title: string;
        isCreation: boolean;
        loading: boolean;
    };
    uiAction: {
        organisationMultiSelectedChange: (organisations: iOrganization[]) => void;
        userRoleOrganizationSave: (role: iRoleUI) => void;
        save: () => void;
    };
    ngOnInit(): void;
    ngOnChanges(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofUserRolesSelectComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofUserRolesSelectComponent, "fof-user-roles-select", never, { "userRoleOrganizations": "userRoleOrganizations"; "notSelectableOrganizations": "notSelectableOrganizations"; }, {}, never>;
}
export {};

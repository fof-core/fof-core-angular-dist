import { OnInit } from '@angular/core';
import { FofPermissionService } from '../../permission/fof-permission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { iRole, iUser, iUserRoleOrganization, iOrganization } from '../../permission/interfaces/permissions.interface';
import { FormGroup, FormBuilder } from "@angular/forms";
import { FofNotificationService } from '../../core/notification/notification.service';
import { FofDialogService } from '../../core/fof-dialog.service';
import { MatDialog } from '@angular/material/dialog';
import { FofErrorService } from '../../core/fof-error.service';
import * as i0 from "@angular/core";
interface iRoleUI extends iRole {
    checked?: boolean;
    userRoleOrganization?: iUserRoleOrganization;
}
interface iUserRoleToDisplay {
    organizationId?: number;
    organization?: string;
    roles?: string[];
    userRoleOrganizations?: iUserRoleOrganization[];
}
export declare class FofUserComponent implements OnInit {
    private fofPermissionService;
    private activatedRoute;
    private formBuilder;
    private fofNotificationService;
    private fofDialogService;
    private router;
    private matDialog;
    private fofErrorService;
    constructor(fofPermissionService: FofPermissionService, activatedRoute: ActivatedRoute, formBuilder: FormBuilder, fofNotificationService: FofNotificationService, fofDialogService: FofDialogService, router: Router, matDialog: MatDialog, fofErrorService: FofErrorService);
    private priVar;
    private privFunc;
    uiVar: {
        title: string;
        loadingUser: boolean;
        loadingRoles: boolean;
        userIsNew: boolean;
        user: iUser;
        userOrganizationsDisplay: iOrganization[];
        form: FormGroup;
        allRoles: iRoleUI[];
        selectedUserRoleOrganizations: iUserRoleOrganization[];
        userRolesToDisplay: iUserRoleToDisplay[];
        roleDisplayedColumns: string[];
        rolesAll: iRole[];
    };
    uiAction: {
        userSave: () => void;
        userCancel: () => void;
        userDelete: () => void;
        userRolesDelete: () => void;
        roleSelectComponentOpen: (userRole?: iUserRoleToDisplay) => void;
        organisationMultiSelectedChange: (organizations: iOrganization[]) => void;
    };
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofUserComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofUserComponent, "fof-core-fof-user", never, {}, {}, never>;
}
export {};

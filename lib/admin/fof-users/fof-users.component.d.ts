import { OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { FofPermissionService } from '../../permission/fof-permission.service';
import { FofNotificationService } from '../../core/notification/notification.service';
import { iUser } from '../../permission/interfaces/permissions.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BreakpointObserver } from '@angular/cdk/layout';
import { FormControl } from '@angular/forms';
import { FofErrorService } from '../../core/fof-error.service';
import { iOrganization } from '../../permission/interfaces/permissions.interface';
import * as i0 from "@angular/core";
export declare class FofUsersComponent implements OnInit, AfterViewInit, OnDestroy {
    private fofPermissionService;
    private fofNotificationService;
    private breakpointObserver;
    private fofErrorService;
    paginator: MatPaginator;
    sort: MatSort;
    constructor(fofPermissionService: FofPermissionService, fofNotificationService: FofNotificationService, breakpointObserver: BreakpointObserver, fofErrorService: FofErrorService);
    private priVar;
    private privFunc;
    uiVar: {
        displayedColumns: string[];
        data: iUser[];
        resultsLength: number;
        pageSize: number;
        isLoadingResults: boolean;
        searchUsersCtrl: FormControl;
    };
    uiAction: {
        organisationMultiSelectedChange: (organization: iOrganization) => void;
    };
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofUsersComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofUsersComponent, "fof-core-fof-users", never, {}, {}, never>;
}

import * as i0 from "@angular/core";
import * as i1 from "./fof-organizations-tree/fof-organizations-tree.component";
import * as i2 from "./fof-organizations-multi-select/fof-organizations-multi-select.component";
import * as i3 from "./fof-datatable/fof-datatable.component";
import * as i4 from "@angular/common";
import * as i5 from "../permission/permission.module";
import * as i6 from "../core/material.module";
import * as i7 from "@angular/forms";
export declare class ComponentsModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<ComponentsModule, [typeof i1.FofOrganizationsTreeComponent, typeof i2.FofOrganizationsMultiSelectComponent, typeof i3.FofDatatableComponent], [typeof i4.CommonModule, typeof i5.FofPermissionModule, typeof i6.MaterialModule, typeof i7.FormsModule, typeof i7.ReactiveFormsModule], [typeof i1.FofOrganizationsTreeComponent, typeof i2.FofOrganizationsMultiSelectComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<ComponentsModule>;
}

import { OnInit, ElementRef, EventEmitter } from '@angular/core';
import { iOrganization } from '../../permission/interfaces/permissions.interface';
import * as i0 from "@angular/core";
export declare class FofOrganizationsMultiSelectComponent implements OnInit {
    private elementRef;
    onDocumentClick(event: any): void;
    multiSelect: boolean;
    selectedOrganisations: iOrganization[];
    notSelectableOrganizations: iOrganization[];
    placeHolder: string;
    selectedOrganizationsChange: EventEmitter<any>;
    constructor(elementRef: ElementRef);
    private priVar;
    private privFunc;
    uiVar: {
        treeMenuVisible: boolean;
    };
    uiAction: {
        openTree: () => void;
        selectedOrganizationsChange: (selectedOrganisations: iOrganization | iOrganization[]) => void;
        organisationRemove: (organisationToRemove: iOrganization) => void;
    };
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofOrganizationsMultiSelectComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofOrganizationsMultiSelectComponent, "fof-core-fof-organizations-multi-select", never, { "multiSelect": "multiSelect"; "selectedOrganisations": "selectedOrganisations"; "notSelectableOrganizations": "notSelectableOrganizations"; "placeHolder": "placeHolder"; }, { "selectedOrganizationsChange": "selectedOrganizationsChange"; }, never>;
}

import { EventEmitter, OnInit } from '@angular/core';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { iOrganization } from '../../permission/interfaces/permissions.interface';
import { FofPermissionService } from '../../permission/fof-permission.service';
import * as i0 from "@angular/core";
export interface iOrgUI extends iOrganization {
    checked?: boolean;
    indeterminate?: boolean;
    notSelectable?: boolean;
    children?: iOrgUI[];
}
export declare class FofOrganizationsTreeComponent implements OnInit {
    private fofPermissionService;
    /** Change the current organizations */
    nodeChanged: iOrgUI;
    /** Notifiy the tree to delete an organizations */
    nodeToDelete: iOrgUI;
    /** Can the user could select multiple organizations? */
    multiSelect: boolean;
    /** Organization we want to be already checked */
    selectedNodesSynchronize: iOrgUI[];
    /** Prevent to check some organizations */
    notSelectableOrganizations: iOrgUI[];
    /** Submit all the organization selected everytime an orgnization is selected or unselected
     * AN objet or array, depending from the multiSelect param
    */
    selectedOrganizationsChange: EventEmitter<iOrgUI | iOrgUI[]>;
    constructor(fofPermissionService: FofPermissionService);
    private priVar;
    private privFunc;
    uiVar: {
        loading: boolean;
        organizations: {
            treeControl: NestedTreeControl<iOrgUI>;
            dataSource: MatTreeNestedDataSource<iOrgUI>;
        };
    };
    uiAction: {
        OrganizationNodeHasChild: (_: number, node: iOrgUI) => boolean;
        organizationTreeLoad: () => void;
        treeViewNodeSelect: (node: iOrgUI, $event: any) => void;
    };
    ngOnInit(): void;
    ngOnChanges(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofOrganizationsTreeComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofOrganizationsTreeComponent, "fof-organizations-tree", never, { "nodeChanged": "nodeChanged"; "nodeToDelete": "nodeToDelete"; "multiSelect": "multiSelect"; "selectedNodesSynchronize": "selectedNodesSynchronize"; "notSelectableOrganizations": "notSelectableOrganizations"; }, { "selectedOrganizationsChange": "selectedOrganizationsChange"; }, never>;
}

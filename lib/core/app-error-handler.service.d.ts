import { ErrorHandler } from '@angular/core';
import { IfofConfig } from '../fof-config';
import { FofNotificationService } from './notification/notification.service';
import * as i0 from "@angular/core";
/** Application-wide error handler that adds a UI notification to the error handling
 * provided by the default Angular ErrorHandler.
 */
export declare class fofErrorHandler extends ErrorHandler {
    private notificationsService;
    private fofConfig;
    constructor(notificationsService: FofNotificationService, fofConfig: IfofConfig);
    handleError(exception: any): void;
    static ɵfac: i0.ɵɵFactoryDef<fofErrorHandler>;
    static ɵprov: i0.ɵɵInjectableDef<fofErrorHandler>;
}

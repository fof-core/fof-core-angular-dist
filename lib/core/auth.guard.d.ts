import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FoFAuthService } from './auth.service';
import { FofNotificationService } from './notification/notification.service';
import { IfofConfig } from '../fof-config';
import * as i0 from "@angular/core";
export declare class FofAuthGuard implements CanActivate {
    private router;
    private foFAuthService;
    private fofNotificationService;
    private fofConfig;
    constructor(router: Router, foFAuthService: FoFAuthService, fofNotificationService: FofNotificationService, fofConfig: IfofConfig);
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;
    static ɵfac: i0.ɵɵFactoryDef<FofAuthGuard>;
    static ɵprov: i0.ɵɵInjectableDef<FofAuthGuard>;
}

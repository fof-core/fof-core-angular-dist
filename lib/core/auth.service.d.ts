import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IfofConfig } from '../fof-config';
import { iUserLogin } from '../permission/interfaces/permissions.interface';
import * as i0 from "@angular/core";
export declare class FoFAuthService {
    private fofConfig;
    private httpClient;
    private router;
    constructor(fofConfig: IfofConfig, httpClient: HttpClient, router: Router);
    private currentUserSubject;
    private environment;
    currentUser$: Observable<iUserLogin>;
    get currentUser(): iUserLogin;
    refreshByCookie(): Observable<iUserLogin>;
    loginKerberos(): Promise<unknown>;
    login(username: string, password: string): Observable<iUserLogin>;
    logOut(): Observable<boolean>;
    static ɵfac: i0.ɵɵFactoryDef<FoFAuthService>;
    static ɵprov: i0.ɵɵInjectableDef<FoFAuthService>;
}

import { EventEmitter, OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export interface iInformation {
    title?: string;
    question: string;
    yesLabel?: string;
    width?: string;
    height?: string;
}
export interface iYesNo extends iInformation {
    noLabel?: string;
}
export interface iInternal extends iYesNo {
    informationOnly: boolean;
}
export declare class FofCoreDialogYesNoComponent implements OnInit {
    data: iInternal;
    anInput: any;
    anOuputEvent: EventEmitter<any>;
    constructor(data: iInternal);
    private priVar;
    private privFunc;
    uiVar: {
        title: string;
        question: any;
        yesLabel: string;
        noLabel: string;
        informationOnly: boolean;
    };
    uiAction: {};
    ngOnInit(): void;
    ngAfterViewInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofCoreDialogYesNoComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<FofCoreDialogYesNoComponent, "core-dialog-yes-no", never, { "anInput": "anInput"; }, { "anOuputEvent": "anOuputEvent"; }, never>;
}

export interface ifofSearch<T> {
    count: number;
    total: number;
    page: number;
    pageCount: number;
    data: T[];
}
export interface iUserError {
    id: string;
    email: string;
}
export interface iFofHttpException {
    fofCoreException: boolean;
    errorManaged: boolean;
    status: number;
    statusText?: string;
    timestamp: string;
    url: string;
    user?: iUserError;
    message?: string;
    error?: any;
    ok?: boolean;
}

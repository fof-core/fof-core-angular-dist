import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import * as i0 from "@angular/core";
import * as i1 from "./notification/notification/notification.component";
import * as i2 from "./core-dialog-yes-no/core-dialog-yes-no.component";
import * as i3 from "@angular/common";
import * as i4 from "../shared/shared.module";
import * as i5 from "./material.module";
import * as i6 from "@angular/common/http";
import * as i7 from "@ngx-translate/core";
export declare function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader;
export declare class FoFCoreModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<FoFCoreModule, [typeof i1.notificationComponent, typeof i2.FofCoreDialogYesNoComponent], [typeof i3.CommonModule, typeof i4.SharedModule, typeof i5.MaterialModule, typeof i6.HttpClientModule, typeof i7.TranslateModule], [typeof i1.notificationComponent, typeof i2.FofCoreDialogYesNoComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<FoFCoreModule>;
}

import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IfofConfig } from '../fof-config';
import { FoFAuthService } from './auth.service';
import * as i0 from "@angular/core";
export declare class FoFCoreService {
    private fofConfig;
    private httpClient;
    private router;
    private foFAuthService;
    constructor(fofConfig: IfofConfig, httpClient: HttpClient, router: Router, foFAuthService: FoFAuthService);
    private _initializationReadySubject;
    initializationReady$: Observable<boolean>;
    private _environment;
    get environment(): any;
    static ɵfac: i0.ɵɵFactoryDef<FoFCoreService>;
    static ɵprov: i0.ɵɵInjectableDef<FoFCoreService>;
}

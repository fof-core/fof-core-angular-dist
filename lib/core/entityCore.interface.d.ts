export interface iFofEntityCore {
    id?: number;
    _createdDate?: string;
    _createdById?: number;
    _createdByName?: string;
    _updatedDate?: string;
    _updatedById?: number;
    _updatedByName?: string;
    _version?: number;
    organizationId?: number;
}

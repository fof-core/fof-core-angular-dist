import { iYesNo, iInformation } from './core-dialog-yes-no/core-dialog-yes-no.component';
import { MatDialog } from '@angular/material/dialog';
import * as i0 from "@angular/core";
export declare class FofDialogService {
    private readonly matDialog;
    constructor(matDialog: MatDialog);
    openYesNo(options: iYesNo): Promise<unknown>;
    openInformation(options: iInformation): Promise<unknown>;
    static ɵfac: i0.ɵɵFactoryDef<FofDialogService>;
    static ɵprov: i0.ɵɵInjectableDef<FofDialogService>;
}

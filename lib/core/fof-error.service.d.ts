import { iFofHttpException } from './core.interface';
import { FofNotificationService } from './notification/notification.service';
import * as i0 from "@angular/core";
export interface iFofCleanException extends iFofHttpException {
    isConstraintError: boolean;
}
export declare class FofErrorService {
    private fofNotificationService;
    constructor(fofNotificationService: FofNotificationService);
    /** Only for the core service, the error should be already cleaned */
    cleanError(errorToManage: any): iFofCleanException;
    errorManage(errorToManage: iFofCleanException): void;
    static ɵfac: i0.ɵɵFactoryDef<FofErrorService>;
    static ɵprov: i0.ɵɵInjectableDef<FofErrorService>;
}

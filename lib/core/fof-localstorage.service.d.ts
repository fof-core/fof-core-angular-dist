import { IfofConfig } from '../fof-config';
import * as i0 from "@angular/core";
export declare class FofLocalstorageService {
    private fofConfig;
    constructor(fofConfig: IfofConfig);
    private appShortName;
    setItem(key: string, value: any): void;
    getItem(key: string): any;
    removeItem(key: string): void;
    static ɵfac: i0.ɵɵFactoryDef<FofLocalstorageService>;
    static ɵprov: i0.ɵɵInjectableDef<FofLocalstorageService>;
}

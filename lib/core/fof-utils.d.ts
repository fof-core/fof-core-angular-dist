import { FormGroup } from '@angular/forms';
import { iFofHttpException } from './core.interface';
export interface iFofCleanException extends iFofHttpException {
    isConstraintError: boolean;
}
export declare const fofUtilsForm: {
    validateAllFields: (formGroup: FormGroup) => void;
};
export declare const fofError: {
    clean: (errorToManage: any) => iFofCleanException;
    cleanAndMange: (errorToManage: any) => void;
};

import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FofErrorService } from './fof-error.service';
import * as i0 from "@angular/core";
export declare class FofErrorInterceptor implements HttpInterceptor {
    private fofErrorService;
    constructor(fofErrorService: FofErrorService);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
    static ɵfac: i0.ɵɵFactoryDef<FofErrorInterceptor>;
    static ɵprov: i0.ɵɵInjectableDef<FofErrorInterceptor>;
}

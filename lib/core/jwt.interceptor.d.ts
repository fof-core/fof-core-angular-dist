import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FoFAuthService } from './auth.service';
import { IfofConfig } from '../fof-config';
import * as i0 from "@angular/core";
export declare class FoFJwtInterceptor implements HttpInterceptor {
    private foFAuthService;
    private fofConfig;
    private environment;
    constructor(foFAuthService: FoFAuthService, fofConfig: IfofConfig);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
    static ɵfac: i0.ɵɵFactoryDef<FoFJwtInterceptor>;
    static ɵprov: i0.ɵɵInjectableDef<FoFJwtInterceptor>;
}

import * as i0 from "@angular/core";
import * as i1 from "@angular/material/button";
import * as i2 from "@angular/material/card";
import * as i3 from "@angular/material/checkbox";
import * as i4 from "@angular/material/chips";
import * as i5 from "@angular/material/dialog";
import * as i6 from "@angular/material/divider";
import * as i7 from "@angular/material/expansion";
import * as i8 from "@angular/material/icon";
import * as i9 from "@angular/material/input";
import * as i10 from "@angular/material/list";
import * as i11 from "@angular/material/paginator";
import * as i12 from "@angular/material/progress-spinner";
import * as i13 from "@angular/material/sort";
import * as i14 from "@angular/material/table";
import * as i15 from "@angular/material/tree";
export declare class MaterialModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<MaterialModule, never, [typeof i1.MatButtonModule, typeof i2.MatCardModule, typeof i3.MatCheckboxModule, typeof i4.MatChipsModule, typeof i5.MatDialogModule, typeof i6.MatDividerModule, typeof i7.MatExpansionModule, typeof i8.MatIconModule, typeof i9.MatInputModule, typeof i10.MatListModule, typeof i11.MatPaginatorModule, typeof i12.MatProgressSpinnerModule, typeof i13.MatSortModule, typeof i14.MatTableModule, typeof i15.MatTreeModule], [typeof i1.MatButtonModule, typeof i2.MatCardModule, typeof i3.MatCheckboxModule, typeof i4.MatChipsModule, typeof i5.MatDialogModule, typeof i6.MatDividerModule, typeof i7.MatExpansionModule, typeof i8.MatIconModule, typeof i9.MatInputModule, typeof i10.MatListModule, typeof i11.MatPaginatorModule, typeof i12.MatProgressSpinnerModule, typeof i13.MatSortModule, typeof i14.MatTableModule, typeof i15.MatTreeModule]>;
    static ɵinj: i0.ɵɵInjectorDef<MaterialModule>;
}

export declare class Notification {
    type: NotificationType;
    message: string;
    mustDisappearAfter?: number;
}
export declare enum NotificationType {
    Success = 0,
    Error = 1,
    Info = 2,
    Warning = 3
}
/** params for notification */
export declare class NotificationParams {
    /** Keep the notification active, even if the route change */
    keepAfterRouteChange?: boolean;
    /** Number in millisecond. By default, 3000. For preventing the notification to
     * diseaper, set it with a negative number */
    mustDisappearAfter?: number;
}

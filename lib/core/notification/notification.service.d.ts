import { BehaviorSubject, Observable } from 'rxjs';
import { NotificationParams, NotificationType } from './notification-type';
import * as i0 from "@angular/core";
export declare class FofNotificationService {
    saveIsDoneNotification: BehaviorSubject<any>;
    private subject;
    private keepAfterRouteChange;
    constructor();
    get savedNotification(): Observable<any>;
    getnotification(): Observable<any>;
    /** Notifiy a success
     * @param message
     * accept html
     */
    success(message: string, params?: NotificationParams): void;
    /** Notifiy an error
     * @param message
     * accept html
     */
    error(message: string, params?: NotificationParams): void;
    /** Notifiy an info
     * @param message
     * accept html
     */
    info(message: string, params?: NotificationParams): void;
    /** Notifiy a warning
     * @param message
     * accept html
     */
    warn(message: string, params?: NotificationParams): void;
    notification(type: NotificationType, message: string, params?: NotificationParams): void;
    saveIsDone(): void;
    clear(): void;
    static ɵfac: i0.ɵɵFactoryDef<FofNotificationService>;
    static ɵprov: i0.ɵɵInjectableDef<FofNotificationService>;
}

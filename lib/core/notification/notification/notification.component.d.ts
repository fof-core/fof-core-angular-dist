import { OnInit, OnDestroy, NgZone } from '@angular/core';
import { FofNotificationService } from '../notification.service';
import { Notification } from '../notification-type';
import * as i0 from "@angular/core";
export declare class notificationComponent implements OnInit, OnDestroy {
    private fofNotificationService;
    private ngZone;
    constructor(fofNotificationService: FofNotificationService, ngZone: NgZone);
    private priVar;
    private privFunc;
    uiVar: {
        notifications: Notification[];
        savedActive: boolean;
    };
    uiAction: {
        removenotification: (notification: Notification) => void;
        cssClass: (notification: Notification) => "notification notification-success" | "notification notification-danger" | "notification notification-info" | "notification notification-warning";
    };
    ngOnInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<notificationComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<notificationComponent, "fof-notification", never, {}, {}, never>;
}

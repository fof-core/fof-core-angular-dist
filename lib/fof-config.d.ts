import { InjectionToken } from '@angular/core';
export interface IfofNavigation {
    url?: string;
    external?: true;
    label: string;
    icon?: string;
    childs?: IfofNavigation[];
}
export declare enum eAuth {
    basic = "basic",
    windows = "windows"
}
/**
 * Interface for fof-core/angular package
 */
export interface IfofConfig {
    appName: {
        /** Give the friendly name for your app. */
        long: string;
        /** Will be used in all places where a technical name is necessary. (e.g.  localstorage key root name)*/
        technical: string;
    };
    mainTitle: {
        /** Will be used as main title for devices up to xl*/
        long: string;
        /** Will be used as main title for mobile devices */
        short: string;
    };
    /** An enum with your permission actions
     * @example
     * export enum eCp = { invoiceCreate = 'invoiceCreate' }
    */
    permissions?: any;
    /** your environment object (not the path of the file, the object itself) */
    environment: any;
    /**
     * the main navigation with the default label
     * @example
     * navigationMain: [{ link: 'settings', label: 'A propos' }]
     */
    navigationMain: Array<IfofNavigation>;
    /** By default, the authentication is basic */
    authentication?: {
        type: eAuth;
        /** root url of the fof-nestjs-winauth service. Must be set when auth = windows */
        authUrl?: string;
        /** could all users from MS Domain can log in? Could be set when auth = windows. Default = false */
        allDomainUsers?: boolean;
    };
}
export declare const CORE_CONFIG: InjectionToken<unknown>;

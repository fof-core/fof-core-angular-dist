import { IfofConfig } from '../fof-config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FoFCoreService } from '../core/core.service';
import { iPermission, iRole, iRolePermission, iUserRoleOrganization, iOrganization, iUser, iUserUservice } from './interfaces/permissions.interface';
import { ifofSearch } from '../core/core.interface';
import * as i0 from "@angular/core";
export declare class FofPermissionService {
    private fofConfig;
    private httpClient;
    private coreService;
    constructor(fofConfig: IfofConfig, httpClient: HttpClient, coreService: FoFCoreService);
    private environment;
    user: {
        create: (role: iUser) => Observable<iUser>;
        update: (role: iUser) => Observable<iUser>;
        delete: (role: iUser) => Observable<iUser>;
        getAll: () => Observable<iUser[]>;
        search: (fullSearch: string, organizationsId: number[], limit: number, page: number, sort: string, direction?: any) => Observable<ifofSearch<iUser>>;
        replaceUserRoleOrganization: (userRoleOrganizations: iUserRoleOrganization[], userId: number, organisationId: number) => Observable<iUserRoleOrganization[]>;
        deleteUserRoleOrganizations: (userId: number, organisationsId: number[]) => Observable<iUserRoleOrganization[]>;
        getWithRoleById: (id: number) => Observable<iUser>;
        getWithUservicesById: (id: number) => Observable<iUser>;
    };
    role: {
        create: (role: iRole) => Observable<iRole>;
        update: (role: iRole) => Observable<iRole>;
        delete: (role: iRole) => Observable<iRole>;
        getAll: () => Observable<iRole[]>;
        search: (query: string, limit: number, page: number, sort: string, direction?: any) => Observable<ifofSearch<iRole>>;
        getWithPermissionByRoleCode: (roleCode: string) => Observable<iRole[]>;
    };
    permission: {
        getAll: () => Observable<iPermission[]>;
    };
    organization: {
        create: (organization: iOrganization) => Observable<iOrganization>;
        update: (organization: iOrganization) => Observable<iOrganization>;
        delete: (organization: iOrganization) => Observable<iOrganization>;
        getTreeView: () => Observable<iOrganization[]>;
    };
    rolePermission: {
        create: (rolePermission: iRolePermission) => Observable<iRolePermission>;
        delete: (rolePermission: iRolePermission) => Observable<iRolePermission>;
    };
    userRoleOrganization: {
        create: (userRoleOrganization: iUserRoleOrganization) => Observable<iUserRoleOrganization>;
        delete: (userRoleOrganization: iUserRoleOrganization) => Observable<iUserRoleOrganization>;
        bulkCreate: (userRoleOrganizations: iUserRoleOrganization[]) => Observable<iUserRoleOrganization[]>;
    };
    userUservice: {
        create: (userService: iUserUservice) => Observable<iUserUservice>;
        delete: (id: number) => Observable<number>;
    };
    static ɵfac: i0.ɵɵFactoryDef<FofPermissionService>;
    static ɵprov: i0.ɵɵInjectableDef<FofPermissionService>;
}

import { iFofEntityCore } from '../../core/entityCore.interface';
export interface iPermission extends iFofEntityCore {
    code: string;
    description?: string;
    rolePermissions?: iRolePermission[];
}
export interface iRole extends iFofEntityCore {
    code: string;
    description_: string;
    userRoleOrganizations?: iUserRoleOrganization[];
    rolePermissions?: iRolePermission[];
}
export interface iUser extends iFofEntityCore {
    email: string;
    login?: string;
    firstName?: string;
    lastName?: string;
    userRoleOrganizations?: iUserRoleOrganization[];
    userUServices?: iUserUservice[];
}
export interface iOrganization extends iFofEntityCore {
    code?: string;
    name?: string;
    description?: string;
    children?: iOrganization[];
    parentId?: number;
    parent?: iOrganization[];
}
export interface iUserLogin extends iUser {
    windowAuthentification?: boolean;
    accessToken?: string;
    permissions?: Array<string>;
}
export interface iRolePermission extends iFofEntityCore {
    permissionId?: number;
    roleId?: number;
    order?: number;
    permission?: iPermission;
    role?: iRole;
}
export interface iUserRoleOrganization extends iFofEntityCore {
    userId: number;
    roleId: number;
    organizationId?: number;
    order?: number;
    user?: iUser;
    role?: iRole;
    organization?: iOrganization;
}
export interface iUservice extends iFofEntityCore {
    technicalName: string;
    name?: string;
    frontUrl?: string;
    backUrl?: string;
    availableForUsers?: boolean;
}
export interface iUserUservice extends iFofEntityCore {
    userId?: number;
    uServiceId?: number;
}

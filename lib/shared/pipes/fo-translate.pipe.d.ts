import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class fofTranslate implements PipeTransform {
    transform(content: any): string;
    static ɵfac: i0.ɵɵFactoryDef<fofTranslate>;
    static ɵpipe: i0.ɵɵPipeDefWithMeta<fofTranslate, "fofTranslate">;
}

import * as i0 from "@angular/core";
import * as i1 from "./pipes/fo-translate.pipe";
import * as i2 from "@angular/common";
export declare class SharedModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<SharedModule, [typeof i1.fofTranslate], [typeof i2.CommonModule], [typeof i1.fofTranslate]>;
    static ɵinj: i0.ɵɵInjectorDef<SharedModule>;
}

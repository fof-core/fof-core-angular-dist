import { OnInit } from '@angular/core';
import { FofPermissionService } from '../../permission/fof-permission.service';
import { Router, ActivatedRoute } from '@angular/router';
import { iUser, iOrganization } from '../../permission/interfaces/permissions.interface';
import { FormGroup, FormBuilder } from "@angular/forms";
import { FofNotificationService } from '../../core/notification/notification.service';
import { FofDialogService } from '../../core/fof-dialog.service';
import { MatDialog } from '@angular/material/dialog';
import { FofErrorService } from '../../core/fof-error.service';
import { UservicesService } from '../../uservices/uservices.service';
import { iUservice } from '../../permission/interfaces/permissions.interface';
import * as i0 from "@angular/core";
interface iUserviceUI extends iUservice {
    checked?: boolean;
    userUserviceId?: number;
}
export declare class UserComponent implements OnInit {
    private fofPermissionService;
    private activatedRoute;
    private formBuilder;
    private fofNotificationService;
    private fofDialogService;
    private router;
    private matDialog;
    private fofErrorService;
    private uservicesService;
    constructor(fofPermissionService: FofPermissionService, activatedRoute: ActivatedRoute, formBuilder: FormBuilder, fofNotificationService: FofNotificationService, fofDialogService: FofDialogService, router: Router, matDialog: MatDialog, fofErrorService: FofErrorService, uservicesService: UservicesService);
    private priVar;
    private privFunc;
    uiVar: {
        title: string;
        loadingUser: boolean;
        uServicesAll: iUservice[];
        userIsNew: boolean;
        user: iUser;
        userOrganizationsDisplay: iOrganization[];
        form: FormGroup;
    };
    uiAction: {
        userSave: () => void;
        userCancel: () => void;
        userDelete: () => void;
        userUserviceSave: (uService: iUserviceUI) => void;
        organisationMultiSelectedChange: (organizations: iOrganization[]) => void;
    };
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<UserComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<UserComponent, "fof-core-user", never, {}, {}, never>;
}
export {};

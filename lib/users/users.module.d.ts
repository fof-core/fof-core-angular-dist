import * as i0 from "@angular/core";
import * as i1 from "./users/users.component";
import * as i2 from "./user/user.component";
import * as i3 from "@angular/common";
import * as i4 from "./users-routing.module";
import * as i5 from "../core/material.module";
import * as i6 from "@angular/forms";
import * as i7 from "../components/components.module";
import * as i8 from "../uservices/uservices.module";
export declare class UsersModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<UsersModule, [typeof i1.UsersComponent, typeof i2.UserComponent], [typeof i3.CommonModule, typeof i4.UsersRoutingModule, typeof i5.MaterialModule, typeof i6.FormsModule, typeof i6.ReactiveFormsModule, typeof i7.ComponentsModule, typeof i8.UservicesModule], never>;
    static ɵinj: i0.ɵɵInjectorDef<UsersModule>;
}

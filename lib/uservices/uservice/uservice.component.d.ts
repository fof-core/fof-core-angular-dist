import { OnInit } from '@angular/core';
import { UservicesService } from '../uservices.service';
import { Router, ActivatedRoute } from '@angular/router';
import { iUservice } from '../../permission/interfaces/permissions.interface';
import { FormGroup, FormBuilder } from "@angular/forms";
import { FofNotificationService } from '../../core/notification/notification.service';
import { FofDialogService } from '../../core/fof-dialog.service';
import { FofErrorService } from '../../core/fof-error.service';
import * as i0 from "@angular/core";
export declare class UserviceComponent implements OnInit {
    private uUservicesService;
    private activatedRoute;
    private formBuilder;
    private fofNotificationService;
    private fofDialogService;
    private router;
    private fofErrorService;
    constructor(uUservicesService: UservicesService, activatedRoute: ActivatedRoute, formBuilder: FormBuilder, fofNotificationService: FofNotificationService, fofDialogService: FofDialogService, router: Router, fofErrorService: FofErrorService);
    private priVar;
    private privFunc;
    uiVar: {
        title: string;
        permissionGroups: any;
        loading: boolean;
        uServiceIsNew: boolean;
        uService: iUservice;
        form: FormGroup;
    };
    uiAction: {
        uServiceSave: () => void;
        uServiceCancel: () => void;
        uServiceDelete: () => void;
    };
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDef<UserviceComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<UserviceComponent, "fof-core-uservice", never, {}, {}, never>;
}

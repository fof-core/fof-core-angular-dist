import * as i0 from "@angular/core";
import * as i1 from "./uservices/uservices.component";
import * as i2 from "./uservice/uservice.component";
import * as i3 from "@angular/common";
import * as i4 from "../core/material.module";
import * as i5 from "@angular/forms";
import * as i6 from "./uservices-routing.module";
export declare class UservicesModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<UservicesModule, [typeof i1.UservicesComponent, typeof i2.UserviceComponent], [typeof i3.CommonModule, typeof i4.MaterialModule, typeof i5.FormsModule, typeof i5.ReactiveFormsModule, typeof i6.UservicesRoutingModule], never>;
    static ɵinj: i0.ɵɵInjectorDef<UservicesModule>;
}

import { IfofConfig } from '../fof-config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { iUservice } from '../permission/interfaces/permissions.interface';
import * as i0 from "@angular/core";
export declare class UservicesService {
    private fofConfig;
    private httpClient;
    constructor(fofConfig: IfofConfig, httpClient: HttpClient);
    private environment;
    uServices: {
        create: (uService: iUservice) => Observable<iUservice>;
        update: (uService: iUservice) => Observable<iUservice>;
        delete: (uService: iUservice) => Observable<iUservice>;
        getOne: (id: number) => Observable<iUservice>;
        getAll: () => Observable<iUservice[]>;
    };
    static ɵfac: i0.ɵɵFactoryDef<UservicesService>;
    static ɵprov: i0.ɵɵInjectableDef<UservicesService>;
}

import { OnInit } from '@angular/core';
import { UservicesService } from '../uservices.service';
import { FofNotificationService } from '../../core/notification/notification.service';
import { iUservice } from '../../permission/interfaces/permissions.interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BreakpointObserver } from '@angular/cdk/layout';
import * as i0 from "@angular/core";
export declare class UservicesComponent implements OnInit {
    private uservicesService;
    private fofNotificationService;
    private breakpointObserver;
    paginator: MatPaginator;
    sort: MatSort;
    constructor(uservicesService: UservicesService, fofNotificationService: FofNotificationService, breakpointObserver: BreakpointObserver);
    private priVar;
    private privFunc;
    uiVar: {
        displayedColumns: string[];
        data: iUservice[];
        resultsLength: number;
        pageSize: number;
        isLoadingResults: boolean;
    };
    uiAction: {};
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    static ɵfac: i0.ɵɵFactoryDef<UservicesComponent>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<UservicesComponent, "fof-core-uservices", never, {}, {}, never>;
}

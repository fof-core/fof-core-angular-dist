echo -/ copy proxy-conf.json
cp "node_modules\@fof-angular\core\ressources\proxy-conf.json" proxy-conf.json
echo -/ replace browserslist
cp "node_modules\@fof-angular\core\ressources\browserslist" browserslist
echo -/ install boostrap
npm install bootstrap --save
echo -/ install @nestjsx/crud-request
npm install --save @nestjsx/crud-request
echo -/ install @ngx-translate/core
npm install @ngx-translate/core --save
echo -/ install @ngx-translate/http-loader
npm install @ngx-translate/http-loader --save
echo -/ install @angular/material
ng add @angular/material 
########## Pour app uniquement
# echo -/ install locally material-design-icons
# npm install material-design-icons --save
echo -/ --------------------------------------------------------
echo -/ please, have a look to the package readme file and 
echo -/ follow the instruction for finishing the manual install
echo -/ --------------------------------------------------------
read -p "automatic install is finished. Press [Enter] key to close..."